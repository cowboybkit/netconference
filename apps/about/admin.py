from django.contrib import admin
from about.models import Optout

class OptoutAdmin(admin.ModelAdmin):
    list_display = ('username', 'optout', 'timestamp')

admin.site.register(Optout, OptoutAdmin)