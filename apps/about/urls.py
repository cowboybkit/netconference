from django.conf.urls.defaults import *
from django.views.generic.simple import direct_to_template

urlpatterns = patterns('',
    url(r'^aboutus/$', direct_to_template, 
        {"template": "about/newer_aboutus.html",
         'extra_context' : {'nav_current' : 'whats_netc_logout_nav',
                            'subnav_current' : 'about_us'}
         },
         name="aboutus"),
	url(r'^howitworks/$', direct_to_template, {"template": "about/howitworks.html",
         'extra_context' : {'nav_current' : 'whats_netc_logout_nav',
                            'subnav_current' : 'how_it_works'}
         },
         name="howitworks"),
	url(r'^articles/$', direct_to_template, {"template": "about/articles.html",
         'extra_context' : {'nav_current' : 'whats_netc_logout_nav',
                            'subnav_current' : 'articles'}
         },
         name="articles"),
	url(r'^netconference/$', direct_to_template, {"template": "about/newer_netconference.html",
         'extra_context' : {'nav_current' : 'whats_netc_logout_nav',
                            'subnav_current' : 'whats_netc'}
         },
         name="netconference"),
	url(r'^video/$', direct_to_template, {"template": "about/video.html",
         'extra_context' : {'nav_current' : 'whats_netc_logout_nav',
                            }
         },
         name="video"),
    
    # url(r'^products/$', direct_to_template, {"template": "about/products.html"}, name="products"),
    #   url(r'^services/$', direct_to_template, {"template": "about/services.html"}, name="services"),
    #   url(r'^clients/$', direct_to_template, {"template": "about/clients.html"}, name="clients"),
    #     
    url(r'^terms/$', direct_to_template, {"template": "about/newer_terms.html"}, name="terms"),
    url(r'^privacy/$', direct_to_template, {"template": "about/newer_privacy.html"}, name="privacy"),
    url(r'^optout/$', 'about.views.optout', name="optout"),
    # url(r'^dmca/$', direct_to_template, {"template": "about/dmca.html"}, name="dmca"),
    
    url(r'^what_next/$', direct_to_template, {"template": "about/what_next.html"}, name="what_next"),
    
    # url(r'^products/svod/$', direct_to_template, {"template": "about/products_svod.html"}, name="svod"),
    #  url(r'^products/video-chat/$', direct_to_template, {"template": "about/products_video_chat.html"}, name="video_chat"),
    #  url(r'^products/messaging/$', direct_to_template, {"template": "about/products_vmail.html"}, name="products_vmail"),
    #  url(r'^products/v-moticons/$', direct_to_template, {"template": "about/products_vmoticons.html"}, name="products_vmoticons"),
    #  url(r'^products/group-chat/$', direct_to_template, {"template": "about/products_group_chat.html"}, name="products_group_chat"),
    #  url(r'^products/educational/$', direct_to_template, {"template": "about/products_educational.html"}, name="products_educational"),
    #  url(r'^products/uploader/$', direct_to_template, {"template": "about/products_uploader.html"}, name="products_uploader"),
    #  url(r'^products/site-builder/$', direct_to_template, {"template": "about/products_site_builder.html"}, name="products_site_builder"),
    #  url(r'^products/event-scheduler/$', direct_to_template, {"template": "about/products_event_scheduler.html"}, name="products_event_scheduler"),
    #  url(r'^products/ratings-comments/$', direct_to_template, {"template": "about/products_ratings.html"}, name="products_ratings"),
    #  url(r'^products/dvd-burner/$', direct_to_template, {"template": "about/products_dvd_burner.html"}, name="products_dvd_burner"),
    #  url(r'^products/video-comments/$', direct_to_template, {"template": "about/products_video_comments.html"}, name="products_video_comments"),
    #  
    #  url(r'^services/create/$', direct_to_template, {"template": "about/services_create.html"}, name="services_create"),
    #  url(r'^services/deliver/$', direct_to_template, {"template": "about/services_deliver.html"}, name="services_deliver"),
    #  url(r'^services/publish/$', direct_to_template, {"template": "about/services_publish.html"}, name="services_publish"),
    #  url(r'^services/advertise/$', direct_to_template, {"template": "about/services_advertise.html"}, name="services_advertise"),
     
    url(r'^help/$', direct_to_template, {"template": "about/help.html"}, name="help"),
    # url(r'^help/video/1/$', direct_to_template, {"template": "about/help_vid1.html"}, name="helpvid1"),
    # url(r'^help/video/2/$', direct_to_template, {"template": "about/help_vid2.html"}, name="helpvid2"),
    # url(r'^help/video/3/$', direct_to_template, {"template": "about/help_vid3.html"}, name="helpvid3"),
)
