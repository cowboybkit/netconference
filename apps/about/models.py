from django.db import models
from django.contrib.auth.models import User

class Optout(models.Model):
    """
    People who have opted out of disclosures on the privacy policy.
    """
    username = models.ForeignKey(User)
    optout = models.BooleanField()
    timestamp = models.DateTimeField(auto_now_add=True, blank=True)
    
    def __unicode__(self):
        return self.username.username