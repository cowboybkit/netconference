from django.views.generic.simple import direct_to_template
from about.models import Optout
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
import logging
from django.utils.translation import ugettext_lazy
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth import authenticate, login
from django.http import HttpResponseRedirect

from account.forms import SignupForm
from profiles.models import Profile

from dateutil.relativedelta import relativedelta
from subscription.models import Subscription, UserSubscription
import datetime
from django.core.urlresolvers import reverse

from account.utils import populate_overview_files

from account.views import register_email_autoresponder

_ = lambda x: unicode(ugettext_lazy(x))

def optout(request, optout=False):
    username = request.user
    if request.method == 'POST' and 'username' in request.POST and 'optout' in request.POST:
        user_request = request.user.username
        user_given = request.POST['username']
        if user_request == user_given:
            optout = request.POST['optout']
            optout_add, created = Optout.objects.get_or_create(username = username, defaults={'optout': optout})
            optout_add.save()
            request.user.message_set.create(message=_("You have successfully opted out of disclosures."))
            return direct_to_template(request, 'about/optout.html', extra_context={'username': username, 'optout': True})
        else:
            request.user.message_set.create(message=_("There was an error. Please refresh this page and try again."))
            return direct_to_template(request, 'about/optout.html', extra_context={'username': username, 'optout': False})
    else:
        if request.method == 'POST':
            request.user.message_set.create(message=_("You did not opt out of disclosures. The box before your statement must be checked to opt out."))
            return direct_to_template(request, 'about/optout.html', extra_context={'username': username, 'optout': False})
        else:
            try:
                optnames = Optout.objects.get(username=username)
                return direct_to_template(request, 'about/optout.html', extra_context={'username': username, 'optout': True})
            except ObjectDoesNotExist:
                return direct_to_template(request, 'about/optout.html', extra_context={'username': username, 'optout': False})
optout = login_required(optout)

def followup(request):
    signup_form = SignupForm()
    if request.method=='POST':
        signup_form = SignupForm(request.POST)
        #import ipdb
        #ipdb.set_trace()
        if signup_form.is_valid():
            cleaned_data = signup_form.cleaned_data
            username = cleaned_data['username']
            password = cleaned_data['password1']
            email = cleaned_data['email']
            phone = cleaned_data['phone_number']
            user = User.objects.create_user(username, email, password)
            profile = Profile.objects.get(user=user)
            profile.phone = phone
            profile.save()
            user = authenticate(username=username, password=password)
            create_pro_subscription(user)
            populate_overview_files(user)
            register_email_autoresponder(user, True)
            print user
            login(request, user)
            return HttpResponseRedirect(reverse('conferencing'))
    return render_to_response("follow_up.html", {"form": signup_form}, context_instance=RequestContext(request)) 

def create_pro_subscription(user):
    if not user.usersubscription_set.all():
        subscription = Subscription.objects.get(name='pro')
        expires = datetime.date.today() + relativedelta(months=1)
        user.usersubscription_set.create(user=user, subscription=subscription, expires=expires)
