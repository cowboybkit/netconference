from django.http import Http404, HttpResponseRedirect, HttpResponse
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.template.loader import render_to_string
from ccall_pins.models import ConferencePin
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.utils.translation import ugettext as _
from django.utils.translation import ugettext_noop
from django.core.urlresolvers import reverse
from django.db import IntegrityError
from django.conf import settings
from uuid import uuid4

from vmail.models import Vmail, VmailAttachment
from vmail.forms import ComposeForm
from vmail.utils import format_quote
from friends_app.models import ContactGroup
from files.models import UserFile
from files.views import sorted_object_list
from netsign.models import NetSignDocument, NetSignInvitation, NetSignSignor, DeadlinePassed
from user_theme.models import UserTheme
from schedule.models import *
from schedule.periods import *
from schedule.forms import RsvpForm
from timezones.utils import adjust_datetime_to_timezone
from account.models import Account
from time import strptime
import logging, pytz, datetime

from netconf_utils.encode_decode import *
from django.contrib.sites.models import Site
from django.views.decorators.http import require_POST
from ccall_pins.models import ConferenceBridgeNumber

if "notification" in settings.INSTALLED_APPS:
    from notification import models as notification
else:
    notification = None
    
# favour django-mailer but fall back to django.core.mail
if "mailer" in settings.INSTALLED_APPS:
    from mailer import send_mail
else:
    from django.core.mail import send_mail

def inbox(request, template_name='vmail/inbox.html'):
    """
    Displays a list of received messages for the current user.
    Optional Arguments:
        ``template_name``: name of the template to use.
    """
    message_list = Vmail.objects.inbox_for(request.user)
    return render_to_response(template_name, {
        'message_list': message_list,
    }, context_instance=RequestContext(request))
inbox = login_required(inbox)

def outbox(request, template_name='vmail/outbox.html'):
    """
    Displays a list of sent messages by the current user.
    Optional arguments:
        ``template_name``: name of the template to use.
    """
    message_list = Vmail.objects.outbox_for(request.user)
    return sorted_object_list(request, 
            template_name=template_name, 
            queryset=message_list,
            extra_context={"DATETIME_FORMAT": settings.DATETIME_FORMAT})
outbox = login_required(outbox)

def trash(request, template_name='vmail/trash.html'):
    """
    Displays a list of deleted messages. 
    Optional arguments:
        ``template_name``: name of the template to use
    Hint: A Cron-Job could periodicly clean up old messages, which are deleted
    by sender and recipient.
    """
    message_list = Vmail.objects.trash_for(request.user)
    return sorted_object_list(request,
                              template_name, message_list,
                              extra_context = {
            'DATETIME_FORMAT': settings.DATETIME_FORMAT,
    })
trash = login_required(trash)

#@check_message_send_limit
from restrictions.decorators import compose_page_decorator
from django.shortcuts import redirect
@compose_page_decorator
def compose(request, recipient=None, form_class=ComposeForm,
        template_name='vmail/compose.html', success_url=None, 
        recipient_filter=None, to_emails=None, extra_context=None):
    """
    Displays and handles the ``form_class`` form to compose new messages.
    Required Arguments: None
    Optional Arguments:
        ``recipient``: username of a `django.contrib.auth` User, who should
                       receive the message, optionally multiple usernames
                       could be separated by a ','
        ``form_class``: the form-class to use
        ``template_name``: the template to use
        ``success_url``: where to redirect after successfull submission
    """
    if extra_context is None:
        extra_context = {}
    obj = None
    invite_preset = None
    netsign_preset = None
    file_preset = None
    calendar = get_object_or_404(Calendar, id=1)
    month = calendar.get_my_month(user=request.user)
    # A hack to override the events for the particular date and show the general conference
    if Event.objects.filter(creator=request.user, type="G").count():
        week_my_conf = Week([Event.objects.filter(creator=request.user, type="G")[0]])
        week_my_conf.get_days = [ii for ii in week_my_conf.get_days() if ii.start.date() == datetime.date.today()]
        month.get_weeks = [week_my_conf] + month.get_weeks()

    try:
        user_timezone = request.user.account.get().timezone
    except Exception:
        user_timezone = settings.TIME_ZONE
    user_account = Account.objects.get(user=request.user)
    if request.method == "POST":
        if request.POST.get("invite_attached", None) == "true":
            invite_preset = request.POST["event_id"]
        if request.POST.get("netsign_attached", None) == "true":
            netsign_preset = request.POST["netsign_file_id"][8:]
        if request.POST.get("file_attached", None) == "true":
            file_preset = map(lambda x: x[7:], 
                              filter(lambda x: x, 
                                     request.POST.getlist("file_id")))
        if extra_context.get("message_disallow"):
            return HttpResponseRedirect(reverse("subscription_list"))
        sender = request.user
        groups = request.POST.getlist('groups')
        instance = request.POST['instance']
        recipient_list = request.POST.getlist("recipient")
        recipients = ""
        for recipient in recipient_list:
            recipients += ",".join(map(lambda x: x.strip(), recipient.split(",")))
        recipients = ",".join(filter(lambda x: x, list(set(recipients.split(",")))))
        for g in groups:
            try:
                group = ContactGroup.objects.get(owner=request.user, id=g)
            except ContactGroup.DoesNotExist:
                continue
            for contact in group.contact_groups.all():
                recipients += "%s," % contact.email
        if recipients and recipients[-1] == ",":
            recipients = recipients[:-1]
            recipients = ",".join(list(set(recipients.split(","))))
        data = request.POST.copy()
        data.update({"recipient": recipients})
        form = form_class(data, recipient_filter=recipient_filter)
        if form.is_valid():
            attachment_exists = request.POST.get('file_attached', None)
            attachments = {}
            if attachment_exists == "true":
                file_type = request.POST.getlist('file_type')
                file_id = request.POST.getlist('file_id')
                for ftype, fid in zip(file_type, file_id):
                    if not ftype or not fid:
                        continue
                    file_obj = UserFile.objects.get(id=fid[7:])
                    if not attachments.get(ftype):
                        attachments[ftype] = []
                    attachments[ftype].append(file_obj.title)
            if request.POST['vmail_attached'] == "true":
                attachments["vmail_recording"] = request.POST['vmail_preset']
            invitation_exists = request.POST.get('invite_attached', None)
            if invitation_exists == "true":
                event_id = request.POST['event_id']
                obj = Event.objects.get(id=event_id)
            netsign_exists = request.POST.get('netsign_attached', None)
            if netsign_exists == "true":
                if extra_context.get("netsign_disallow"):
                    return HttpResponseRedirect(reverse("subscription_list"))
                netsign_id = request.POST['netsign_file_id'][8:]
                ns_obj = NetSignDocument.objects.get(id=netsign_id)
                attachments["NetSign Request"] = ns_obj.title
                attachments["netsign_hash"] = {}
                attachments["signor_uuid"] = {}
                deadline_date = request.POST.get("deadline_date", "").strip()
                deadline_time = request.POST.get("deadline_time", "").strip()
                if not deadline_date and not deadline_time:
                    deadline = None
                else:
                    deadline = "%s %s" %(deadline_date, deadline_time)
                if deadline:
                    try:
                        deadline = datetime.datetime(*strptime(deadline, "%Y-%m-%d %H:%M")[:6])
                        deadline = adjust_datetime_to_timezone(deadline, user_timezone, settings.TIME_ZONE)
                        deadline = deadline.astimezone(pytz.timezone(settings.TIME_ZONE)).replace(tzinfo=None)
                    except ValueError:
                        request.user.message_set.create(message=_("Invalid deadline date."))
                        deadline = None
                    if deadline and deadline < datetime.datetime.today():
                        request.user.message_set.create(message=_("NetSign Deadline cannot be in the past."))
                        deadline = None
                    if not deadline:
                        netsign = NetSignDocument.objects.filter(creator=request.user).order_by('-saved_at')
                        files = UserFile.active_objects.filter(user=request.user).order_by('-timestamp')
                        vmail_preset = instance
                        vmail_action = _("Record")
                        payload = {
                            'form': form,
                            'instance': instance,
                            'recipients': recipients,
                            'sender': request.user,
                            'files': files,
                            'netsign': netsign,
                            'invite_preset': invite_preset,
                            'netsign_preset': netsign_preset,
                            'file_preset': file_preset,
                            'timezone': user_timezone,
                            'vmail_preset': vmail_preset,
                            'vmail_action': vmail_action,
                            'groups': groups,
                            'account': user_account,
                            'calendar': calendar,
                            'month': month,
                            }    
                        payload.update(extra_context)
                        return render_to_response(template_name, payload, context_instance=RequestContext(request))
                for recipient in recipients.split(","):
                    try:
                        nsi = NetSignInvitation.objects.create(document_id=int(netsign_id),
                                                               from_user=sender,
                                                               to_email=recipient,
                                                               message="",
                                                               status="2",
                                                               deadline=deadline)
                    except DeadlinePassed:
                        request.user.message_set.create(message=_("Deadline should be set atleast 1 day ahead of today."))
                        return HttpResponseRedirect(request.META.get("HTTP_REFERER", reverse("vmail_compose")))
                    try:
                        signor_uuid = NetSignSignor.objects.get(document__id=netsign_id,
                                                                email=recipient)
                        attachments["signor_uuid"].update({recipient: signor_uuid.uuid})
                    except NetSignSignor.DoesNotExist:
                        pass
                    attachments["netsign_hash"].update({recipient: nsi})
            try:
                form.save(sender=request.user, instance=instance, recipient=recipients, attachments=attachments, event=obj)            
            except IntegrityError:
                request.user.message_set.create(message=_("This message has already been sent."))
                return HttpResponseRedirect(reverse("vmail_outbox"))
            message_id = form.data['instance']
            my_message = Vmail.objects.get(instance=message_id)            
            if request.POST.get('file_attached', None) == "true":
                for ftype, fid in zip(file_type, file_id):
                    if fid:
                        save_attachment = my_message.attached_to.create(
                            message=my_message,
                            file=UserFile.objects.get(id=fid[7:])
                            )
            if request.POST['vmail_attached'] == "true":
                save_attachment = my_message.attached_to.create(
                    message=my_message,
                    vmail_recording=request.POST['vmail_preset']
                )
            if invitation_exists == "true":
                save_invitation = my_message.attached_to.create(
                    message=my_message,
                    invite=Event.objects.get(id=event_id)
                    )
            if netsign_exists == "true":
                for receiver in recipients.split(","):
                    nsi = attachments["netsign_hash"][receiver]
                    nsi.message = my_message.body
                    nsi.save()
                    save_netsign = my_message.attached_to.create(
                        message=my_message,
                        netsign=nsi
                        )
            if success_url is None:
                success_url = reverse('vmail_outbox')
            if request.POST.get("next", None):
                success_url = request.POST['next']
            request.user.message_set.create(message=_("Message sent to: %s" % recipients))
            return HttpResponseRedirect(success_url)
    else:
        form = form_class()
        recipients = request.GET.get('recipient', None)
        if recipients is not None:
            recipient_list = [u for u in User.objects.filter(username__in=[r.strip() for r in recipients.split(',')])]
            form.initial['recipient'] = ",".join(recipient_list or [recipients])

    groups = ContactGroup.objects.filter(owner=request.user)

    instance = str(uuid4())
    vmail_preset = instance
    vmail_action = _("Record")
    files = UserFile.active_objects.filter(user=request.user).order_by('-timestamp')
    netsign = NetSignDocument.objects.filter(creator=request.user).order_by('-saved_at')

    if request.method == "GET":
        if "invite" in request.GET:
            invite_id = request.GET['invite']
            try:
                invite_preset = int(uri_b64decode(str(invite_id))) / 42
            except ValueError:
                request.user.message_set.create(message=_("Invalid conference ID."))
                return HttpResponseRedirect(reverse("vmail_compose"))
            try:
                event = Event.objects.get(id=invite_preset)
                form = form_class(initial={"subject": event.title, "body": event.description})
            except Event.DoesNotExist:
                event = None
                form = None
            if event:
                month = calendar.get_my_month(date=event.start, user=request.user)
                if event.type == "G":
                    week_my_conf = Week([event])
                    week_my_conf.get_days = [ii for ii in week_my_conf.get_days() if ii.start.date() == datetime.date.today()]
                    month.get_weeks = [week_my_conf] + month.get_weeks()
        if "netsign" in request.GET:
            netsign_id = request.GET['netsign']
            netsign_preset = int(uri_b64decode(str(netsign_id))) / 42
            signors = NetSignSignor.objects.filter(document__id=netsign_preset)
            if signors.count():
                recipients = ",".join(signors.values_list("email", flat=True))
                form.initial["recipient"] = recipients
        if "file" in request.GET:
            file_id = request.GET.getlist('file')
            file_preset = []
            for fid in file_id:
                file_preset.append(int(uri_b64decode(str(fid))) / 42)
        if "vmail" in request.GET:
            vmail_preset = instance
            vmail_action = _("Send")
    payload = {
        'form': form,
        'instance': instance,
        'recipients': recipients,
        'sender': request.user,
        'files': files,
        'netsign': netsign,
        'month': month,
        'calendar': calendar,
        'invite_preset': invite_preset,
        'netsign_preset': netsign_preset,
        'file_preset': file_preset,
        'timezone': user_timezone,
        'vmail_preset': vmail_preset,
        'vmail_action': vmail_action,
        'groups': groups,
        'account': user_account,
    }    
    payload.update(extra_context)
    return render_to_response(template_name, payload, context_instance=RequestContext(request))
compose = login_required(compose)

def reply(request, message_id, form_class=ComposeForm,
        template_name='vmail/compose.html', success_url=None, recipient_filter=None):
    """
    Prepares the ``form_class`` form for writing a reply to a given message
    (specified via ``message_id``). Uses the ``format_quote`` helper from
    ``messages.utils`` to pre-format the quote.
    """
    parent = get_object_or_404(Vmail, id=message_id)
    if request.method == "POST":
        sender = request.user
        form = form_class(request.POST, recipient_filter=recipient_filter)
        if form.is_valid():
            form.save(sender=request.user, parent_msg=parent)
            request.user.message_set.create(
                message=_(u"Vmail successfully sent."))
            if success_url is None:
                success_url = reverse('vmail_inbox')
            return HttpResponseRedirect(success_url)
    else:
        form = form_class({
            'body': _(u"%(sender)s wrote:\n%(body)s") % {
                'sender': parent.sender,
                'body': format_quote(parent.body)
                },
            'subject': _(u"Re: %(subject)s") % {'subject': parent.subject},
            'recipient': [parent.sender, ]
            })
    return render_to_response(template_name, {
        'form': form,
    }, context_instance=RequestContext(request))
reply = login_required(reply)

@login_required
@require_POST
def delete_selected(request):
    selected_vmails = request.POST.getlist("message_ids[]")
    for vmail in selected_vmails:
        delete(request, vmail)
    request.user.message_set.create(message=_(u"Vmail successfully deleted."))
    return HttpResponse("")

def delete(request, message_id, success_url=None):
    """
    Marks a message as deleted by sender or recipient. The message is not
    really removed from the database, because two users must delete a message
    before it's save to remove it completely. 
    A cron-job should prune the database and remove old messages which are 
    deleted by both users.
    As a side effect, this makes it easy to implement a trash with undelete.
    
    You can pass ?next=/foo/bar/ via the url to redirect the user to a different
    page (e.g. `/foo/bar/`) than ``success_url`` after deletion of the message.
    """
    user = request.user
    now = datetime.datetime.now()
    message = get_object_or_404(Vmail, id=message_id)
    deleted = False
    if success_url is None:
        success_url = reverse('vmail_outbox')
    if request.GET.has_key('next'):
        success_url = request.GET['next']
    if message.sender == user:
        message.sender_deleted_at = now
        deleted = True
    if message.recipient == user:
        message.recipient_deleted_at = now
        deleted = True
    if deleted:
        message.save()
        # if notification:
        #     notification.send([user], "vmail_deleted", {'message': message,})
        return 
    raise Http404

def undelete(request, message_id, success_url=None):
    """
    Recovers a message from trash. This is achieved by removing the
    ``(sender|recipient)_deleted_at`` from the model.
    """
    user = request.user
    message = get_object_or_404(Vmail, id=message_id)
    undeleted = False
    if success_url is None:
        success_url = reverse('vmail_outbox')
    if request.GET.has_key('next'):
        success_url = request.GET['next']
    if message.sender == user:
        message.sender_deleted_at = None
        undeleted = True
    if message.recipient == user:
        message.recipient_deleted_at = None
        undeleted = True
    if undeleted:
        message.save()
        user.message_set.create(message=_(u"Vmail successfully recovered."))
        # if notification:
        #     notification.send([user], "vmail_recovered", {'message': message,})
        return HttpResponseRedirect(success_url)
    raise Http404
undelete = login_required(undelete)

def view_netsign(request, instance, nsi_hash, template_name='vmail/view.html'):
    from netconf_utils.encode_decode import get_object_from_hash
    nsi = get_object_from_hash(NetSignInvitation, nsi_hash)
    extra_context = {'nsi_for_sign':nsi}
    return view(request, instance, extra_context=extra_context)

def view_rsvp(request, instance):
    data = request.POST.copy()
    name = data["name"]
    email = data["email"]
    phone = data["phone"]
    message = get_object_or_404(Vmail, instance=instance)
    attachments = message.attached_to.all()
    for o in attachments:
        if o.invite:
            invite_attached = True
            event =  o.invite
    Rsvp.objects.create(name = name, email = email, phone = phone, event = event)
    return HttpResponseRedirect(reverse("vmail_detail_rsvp", args=[instance]))





def view(request, instance, template_name='vmail/view.html', extra_context=None):
    """
    Shows a single message.``message_id`` argument is required.
    If the message is unread, ``read_at`` is set to the current datetime.
    """
    url_hash = request.GET.get("url_hash", "")
    signor_uuid = request.GET.get("uuid", "")
    user = request.user
    now = datetime.datetime.now()
    message = get_object_or_404(Vmail, instance=instance)
    if message.sender_deleted_at is not None:
        raise Http404
    sender = message.sender
    try:
        user_theme = UserTheme.objects.get(user=sender)
    except UserTheme.DoesNotExist:
        user_theme = None
    attachments = message.attached_to.all()
    
    vmail_attached = False
    file_attached = False
    invite_attached = False
    netsign_attached = False
    invite_obj = None
    vmid = message.instance
    for o in attachments:
        if o.vmail_recording:
            vmail_attached = True
            vmid = o.vmail_recording
        if o.file:
            file_attached = True
        if o.invite:
            invite_attached = True
            invite_obj = o.invite
        if o.netsign:
            netsign_attached = True

    # if (message.sender != user) and (message.recipient != user):
    #     raise Http404
    #if message.read_at is None and message.recipient == user:
    if message.read_at is None:
        message.read_at = now
        message.save()

    payload = { 'user_theme': user_theme,
                'message': message,
                'attachments': attachments,
                'vmail_attached': vmail_attached,
                'url_hash': url_hash,
                'signor_uuid': signor_uuid,
                'file_attached': file_attached,
                'invite_attached': invite_attached,
                'netsign_attached': netsign_attached,
                'thumb_url': settings.THUMBNAIL_URL,
                'vmid': vmid,
                "DATETIME_FORMAT": settings.DATETIME_FORMAT,
            }

    if invite_attached:
        urlhash = uri_b64encode(str(invite_obj.id * 42))
        payload["event"] =  invite_obj
        payload["urlhash"] = urlhash
        current_domain = Site.objects.get_current().domain
        payload["site_url"] = 'http://www.%s' % current_domain
        payload.update({"form": RsvpForm(initial={"event": invite_obj.id })})

        template_name =   'vmail/view_invite.html'

    if extra_context:
        payload.update(extra_context)
    
    if not request.session.get("instance", None):
        request.session["instance"] = []

    if not instance in request.session["instance"]:
        request.session["instance"].append(instance)
    request.session.set_expiry(0)

    return render_to_response(template_name, payload, context_instance=RequestContext(request))

from django.views.generic.simple import direct_to_template

def save_vmail(request, vmail_hash=None):
    if request.POST.get('save_vmail', ''):
        vmail = request.session.get('vmail')
        #Create a vmail with the given uuid
        vmail = get_object_or_404(Vmail, instance=vmail)
        UserFile.objects.create(title="Recorded vmail: %s"%vmail.subject,
                                user=vmail.sender,
                                uuid=vmail.instance,
                                source_type="vmail",
                                current_type="vmail",
                                duration=0,
                                processed=1,
                                file_size=0,
                                is_active=True
                                )
        return redirect("my_files")
        
    return direct_to_template(request,
                              template='vmail/vmail_save.html',
                              extra_context={'subnav_current_add':'Save Vmail'}
                              )

@require_POST    
def preview(request):
    data = request.POST.copy()
    data["message"] = data.get("body", "")
    data["vmail_url"] = None
    data["attachments"] = {}
    file_type = request.POST.getlist('file_type')
    file_id = request.POST.getlist('file_id')
    for ftype, fid in zip(file_type, file_id):
        if not ftype or not fid:
            continue
        file_obj = UserFile.objects.get(id=fid[7:])
        if not data["attachments"].get(ftype):
            data["attachments"][ftype] = []
        data["attachments"][ftype].append(file_obj.title)
    if data["netsign_attached"] == "true":
        ns_obj = NetSignDocument.objects.get(id=data["netsign_file_id"][8:])
        data["attachments"]["NetSign Request"] = ns_obj.title
    if data["vmail_attached"] == "true":
        data["attachments"]["vmail_recording"] = data["vmail_preset"]
        data["video_email"] = True
    if data["event_id"]:
        data["event"] = Event.objects.get(id=data["event_id"])
        data["conf_message"] = True
        data["voice"] = True
        data["ccall_pin"] = ConferencePin.objects.get(id=request.user.id)
        data["conftime"] = data["event"].start
        data["timezone"] = data["event"].timezone
        data["site"] = unicode(Site.objects.get_current())
        try:
            data["full_name"] = data["event"].creator.get_full_name()
        except:
            data["full_name"] = data["event"].creator.username  
    return render_to_response("vmail/delivery/vmail.html",
                              data,
                              context_instance=RequestContext(request))

@login_required
@require_POST
def resend_selected(request):
    selected_vmails = request.POST.getlist('message_ids[]')
    for vmail in selected_vmails:
        resend(request, message_id=vmail)
    request.user.message_set.create(message=_("Selected Vmails have been successfully resent."))
    return HttpResponse("")
    

def resend(request, message_id=None):
    vmail = get_object_or_404(Vmail, id=message_id)
    event = None
    show_netsign = False
    attachments = {}
    if vmail.sender != request.user:
        return None
    for attachment in vmail.attached_to.all():
        try:
            netsign_invite_attached = attachment.netsign
        except NetSignInvitation.DoesNotExist:
            netsign_invite_attached = None
        if netsign_invite_attached:
            show_netsign = True
            attachments["NetSign Request"] = netsign_invite_attached.document.title
            attachments["netsign_hash"] = {}
            attachments["netsign_hash"][netsign_invite_attached.to_email] = netsign_invite_attached
            attachments["signor_uuid"] = {}
            try:
                attachments["signor_uuid"][netsign_invite_attached.to_email] = netsign_invite_attached.document.netsign_signors.get(email=netsign_invite_attached.to_email).uuid
            except NetSignSignor.DoesNotExist:
                pass
        try:
            event_attached = attachment.invite
        except Event.DoesNotExist:
            event_attached = None
        if event_attached:
            event = event_attached
        try:
            vmail_attached = attachment.vmail_recording
        except Vmail.DoesNotExist:
            vmail_attached = None
        if vmail_attached:
            attachments["vmail_recording"] = vmail.subject
        try:
            file_attached = attachment.file
        except UserFile.DoesNotExist:
            file_attached = None
        if file_attached:
            attachments[file_attached.id] = [file_attached.title,]
    context = {"user": vmail.sender,
               "message": vmail.body,
               "vmail_url": "http://www.%s%s" % (Site.objects.get_current().domain, vmail.get_absolute_url()),
               "attachments": attachments,
               "show_netsign": show_netsign,
               "event": event}
    for recipient in vmail.recipient.split(","):
        if show_netsign:
            try:
                context.update({"url_hash": attachments["netsign_hash"][recipient].uhash,
                                "signor_uuid": attachments["signor_uuid"][recipient]})
            except KeyError:
                continue
        Vmail.objects.send_multipart_mail("vmail/delivery/vmail.html",
                                          context,
                                          vmail.subject,
                                          recipient,
                                          vmail.sender.email,
                                          event=event)
