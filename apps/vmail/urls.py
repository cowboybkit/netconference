from django.conf.urls.defaults import *
from django.views.generic.simple import direct_to_template, redirect_to
from vmail.views import *


urlpatterns = patterns('',
    url(r'^$', redirect_to, {'url': 'inbox/'}),
    url(r'^inbox/$', inbox, name='vmail_inbox'),
    url(r'^outbox/$', outbox, name='vmail_outbox'),
    url(r'^compose/$', compose, name='vmail_compose'),
    url(r'^compose/(?P<recipient>[\+\w]+)/$', compose, name='vmail_compose_to'),
    url(r'^reply/(?P<message_id>[\d]+)/$', reply, name='vmail_reply'),
    url(r'^view/(?P<instance>[-0-9a-f]{36})/rsvp/$', view_rsvp, name='vmail_detail_rsvp'),
    url(r'^view/(?P<instance>[-0-9a-f]{36})/(?P<nsi_hash>\w+)/$', view_netsign, name='vmail_detail_netsign'),
    url(r'^save_vmail/$', save_vmail,  name='vmail_save'),
    url(r'^view/(?P<instance>[-0-9a-f]{36})/$', view, name='vmail_detail'),
    url(r'^preview/$', preview, name='vmail_preview'),
    url(r'^resend/$', resend_selected, name='vmail_resend'),

    url(r'^delete/$', delete_selected, name='vmail_delete'),
    url(r'^undelete/(?P<message_id>[\d]+)/$', undelete, name='vmail_undelete'),
    url(r'^trash/$', trash, name='vmail_trash'),
)
