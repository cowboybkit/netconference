import datetime
from django.db import models
from django.conf import settings
from django.db.models import signals
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from uuid import uuid4
from files.models import UserFile
from schedule.models import Event
from netsign.models import NetSignInvitation
from django.core.mail import EmailMultiAlternatives
from django.template import loader, Context
from django.db.models import Count
from ccall_pins.models import ConferencePin
from django.template.loader import render_to_string
from mailer import send_mail, send_html_mail
from django.contrib.sites.models import Site
from ccall_pins.models import ConferenceBridgeNumber
class VmailManager(models.Manager):

    def inbox_for(self, user):
        """
        Returns all messages that were received by the given user and are not
        marked as deleted.
        """
        return self.filter(
            recipient=user,
            recipient_deleted_at__isnull=True,
        )

    def outbox_for(self, user):
        """
        Returns all messages that were sent by the given user and are not
        marked as deleted.
        """
        return self.filter(
            sender=user,
            sender_deleted_at__isnull=True,
        )

    def outbox_with_netsign_for(self,user):
        """
        Returns all messages that were sent by the given user and are not
        marked as deleted. and have netsign attachements
        """
        return self.outbox_for(user=user).annotate(has_netsign=Count('attached_to')).filter(has_netsign__gte=1)
        
    
    def trash_for(self, user):
        """
        Returns all messages that were either received or sent by the given
        user and are marked as deleted.
        """
        return self.filter(
            recipient=user,
            recipient_deleted_at__isnull=False,
        ) | self.filter(
            sender=user,
            sender_deleted_at__isnull=False,
        )
        
    def send_vmail(self, from_user, to_email, subject, message, instance):
        contact, created = Contact.objects.get_or_create(email=to_email, user=from_user)
        vmail_url = u"http://%s%s" % (
            unicode(Site.objects.get_current()),
            reverse("vmail_detail", args=(instance,)),
        )
        subject = render_to_string("vmail/vmail_subject.txt")
        email_message = render_to_string("vmail/vmail_message.txt", {
            "user": from_user,
            "message": message,
            "vmail_url": vmail_url,
        })
        send_mail(subject, email_message, settings.DEFAULT_FROM_EMAIL, [to_email], fail_silently=False)   
    
    def send_multipart_mail(self, template_name, template_context, subject, recipients, sender=None, fail_silently=False, event=None):
        """
        This function will send a multi-part e-mail with both HTML and
        Text parts.

        template_name must NOT contain an extension. Both HTML (.html) and TEXT
            (.txt) versions must exist, eg 'emails/public_submit' will use both
            public_submit.html and public_submit.txt.

        email_context should be a plain python dictionary. It is applied against
            both the email messages (templates) & the subject.

        subject can be plain text or a Django template string, eg:
            New Job: {{ job.id }} {{ job.title }}

        recipients can be either a string, eg 'a@b.com' or a list, eg:
            ['a@b.com', 'c@d.com']. Type conversion is done if needed.

        sender can be an e-mail, 'Name <email>' or None. If unspecified, the
            DEFAULT_FROM_EMAIL will be used.

        """
        if not sender:
            sender = settings.DEFAULT_FROM_EMAIL

        template_context.update({'site': Site.objects.get().domain})
        context = Context(template_context)
        
        text_part = loader.get_template('vmail/delivery/vmail.txt').render(context)
        subject_part = loader.get_template_from_string(subject).render(context)

        if type(recipients) != list:
            recipients = [recipients,]
        #Requirement to display a different text for the conference email
        if context.has_key('event') and context['event']:
            context['timezone'] = context['event'].timezone
            context['conftime'] = context['event'].start
            #context['ccall_pin'] = ConferencePin.objects.get(id=context['user'].id)
            context['conf_message'] = True
            context['voice'] = True
            #context["conftime"] = context["event"].start
            context["timezone"] = context["event"].timezone
            context["site"] = unicode(Site.objects.get_current())

        html_part = loader.get_template('vmail/delivery/vmail.html').render(context)
        return send_html_mail(subject_part, text_part, html_part, sender, recipients)

class Vmail(models.Model):
    """
    A vmail message
    """
    instance = models.CharField(unique=True, max_length=36)
    subject = models.CharField(_("Subject"), max_length=120)
    body = models.TextField(_("Body"))
    sender = models.ForeignKey(User, related_name='sent_vmails', verbose_name=_("Sender"))
    #recipient = models.ForeignKey(User, related_name='received_vmails', null=True, blank=True, verbose_name=_("Recipient"))
    recipient = models.TextField(_("To: Email Address"))
    parent_msg = models.ForeignKey('self', related_name='next_vmails', null=True, blank=True, verbose_name=_("Parent v-mail"))
    sent_at = models.DateTimeField(_("sent at"), null=True, blank=True)
    read_at = models.DateTimeField(_("read at"), null=True, blank=True)
    replied_at = models.DateTimeField(_("replied at"), null=True, blank=True)
    sender_deleted_at = models.DateTimeField(_("Sender deleted at"), null=True, blank=True)
    recipient_deleted_at = models.DateTimeField(_("Recipient deleted at"), null=True, blank=True)
    
    objects = VmailManager()
    
    def new(self):
        """returns whether the recipient has read the message or not"""
        if self.read_at is not None:
            return False
        return True

    def replied(self):
        """returns whether the recipient has written a reply to this message"""
        if self.replied_at is not None:
            return True
        return False
    
    def __unicode__(self):
        return self.subject
        return '%s, %s: %s' %(self.sender, self.sent_at, self.subject)

    def get_recipient_list(self):
        return self.recipient.split(",")
    
    def get_absolute_url(self):
        return ('vmail_detail', (), {
            'instance': self.instance,
        })
    get_absolute_url = models.permalink(get_absolute_url)
        
    def save(self, force_insert=False, force_update=False):
        if not self.id:
            self.sent_at = datetime.datetime.now()
        # if not self.instance:
        #     self.instance = str(uuid4())  # random so it can't be easily guessed
        super(Vmail, self).save(force_insert, force_update) 
    
    class Meta:
        ordering = ['-sent_at']
        verbose_name = _("Vmail")
        verbose_name_plural = _("Vmails")

class VmailAttachment(models.Model):
    """
    Can be attached to any vmail message.  optionally contains vmail, video, file upload, or event invitation.
    """
    message = models.ForeignKey(Vmail, verbose_name=_("Attached To"), related_name='attached_to')
    vmail_recording = models.TextField(null=True, blank=True)
    file = models.ForeignKey(UserFile, verbose_name=_("File Attachment"), related_name='attachment_file', null=True, blank=True)
    invite = models.ForeignKey(Event, verbose_name=_("Event Invitation"), related_name='attachment_event', null=True, blank=True)
    netsign = models.ForeignKey(NetSignInvitation, verbose_name=_("NetSign Request"), related_name='attachment_netsign', null=True, blank=True)
    timestamp = models.DateTimeField(blank=True, default=datetime.datetime.now, auto_now_add=True)

def inbox_count_for(user):
    """
    returns the number of unread messages for the given user but does not
    mark them seen
    """
    return Vmail.objects.filter(recipient=user, read_at__isnull=True, recipient_deleted_at__isnull=True).count()

# fallback for email notification if django-notification could not be found
if "notification" not in settings.INSTALLED_APPS:
    from messages.utils import new_message_email
    signals.post_save.connect(new_message_email, sender=Vmail)
