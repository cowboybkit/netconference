import datetime
from django import forms
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.utils.translation import ugettext_noop
from django.contrib.auth.models import User
from django.utils.safestring import mark_safe
import re, smtplib

from friends.models import *
from django.contrib.sites.models import Site

from django.forms.fields import email_re

if "notification" in settings.INSTALLED_APPS:
    from notification import models as notification
else:
    notification = None

from vmail.models import Vmail, VmailAttachment

# from vmail.fields import MultiEmailField

# favour django-mailer but fall back to django.core.mail
# if "mailer" in settings.INSTALLED_APPS:
#     from mailer import send_mail
# else:
#     from django.core.mail import send_mail

class ComposeForm(forms.Form):
    """
    A simple default form for private messages.
    """
    recipient = forms.CharField(label=_(u"Recipient"))
    subject = forms.CharField(label=_(u"Subject"))
    body = forms.CharField(label=_(u"Body"), widget=forms.Textarea())

    def clean_subject(self):
        subject = self.cleaned_data["subject"]
        if not subject.strip():
            raise forms.ValidationError(_("Please enter a valid value."))
        return subject.strip()
    
    def clean_body(self):
        body = self.cleaned_data["body"]
        if not body.strip():
            raise forms.ValidationError(_("Please enter a valid value."))
        return body.strip()
    
    def __init__(self, *args, **kwargs):
        recipient_filter = kwargs.pop('recipient_filter', None)
        attachments = kwargs.pop('attachments', None)
        super(ComposeForm, self).__init__(*args, **kwargs)
        if recipient_filter is not None:
            self.fields['recipient']._recipient_filter = recipient_filter

    def clean_recipient(self):
        recipients = self.cleaned_data["recipient"]
        if not recipients:
            raise forms.ValidationError(_("There are no recipients entered or selected."))
        for recipient in recipients.split(","):
            if not email_re.match(recipient):
                raise forms.ValidationError(_("%(recipient)s is not a valid email." % {"recipient": recipient}))
        return recipients
                
    def save(self, sender, instance=None, parent_msg=None, 
             recipient=None, attachments=None, commit=True,event=None):
        # from_email = sender.email
        if sender.get_profile().name:
            from_string = "%s <%s>" % (sender.get_profile().name, sender.email)
        else:
            from_string = sender.email
        subject = self.cleaned_data['subject']
        body = self.cleaned_data['body']
        # recipient = self.cleaned_data['recipient']
        recipient_string = str(recipient)
        if recipient_string[-1:] == ',':
            recipient_string = recipient_string[0:-1]
        else:
            recipient_string = recipient_string
        message_list = []
        msg = Vmail(
            sender = sender,
            recipient = recipient_string,
            subject = subject,
            body = body,
            instance = instance,
        )
        current_site = Site.objects.get_current()
        
        vmail_url = u"http://%s%s" % (current_site.domain,
            reverse("vmail_detail", args=(instance,)),
        )
        
        # add linebreaks and spaces to vmail body
        esc = lambda x: x
        spaced = mark_safe(re.sub('[ \t\r\f\v]', '&'+'nbsp;', esc(body)))
        body_breaksandspaces = mark_safe(re.sub('\n', '<br />', esc(spaced)))
        
        template_name = 'vmail/delivery/vmail'
        
        show_netsign = attachments and 'NetSign Request' in attachments
        video_email = attachments and 'vmail_recording' in attachments
            
        template_context = {
            "user": sender,
            "message": body_breaksandspaces,
            "vmail_url": vmail_url,
            "attachments": attachments,
            "show_netsign":show_netsign,
            'event':event,
            'video_email': video_email,
        }
        if parent_msg is not None:
            msg.parent_msg = parent_msg
            parent_msg.replied_at = datetime.datetime.now()
            parent_msg.save()
        msg.save()
        message_list.append(msg)
        for r in recipient_string.split(','):
            if show_netsign:
                try:
                    signor_email = attachments["signor_uuid"][r]
                except KeyError:
                    continue
                try:
                    signor_netsign = attachments["netsign_hash"][r]
                except KeyError:
                    continue
                template_context.update({"url_hash": signor_netsign.uhash,
                                         "signor_uuid": signor_email})
            try:
                print "sending mail"
                send_vmail = Vmail.objects.send_multipart_mail(template_name, template_context, subject, r, from_string)
            except smtplib.SMTPRecipientsRefused:
                continue
        return message_list
