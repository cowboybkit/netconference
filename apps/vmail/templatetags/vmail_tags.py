from django.template import Library
from django.template.defaultfilters import stringfilter
from django.utils.html import conditional_escape
from django.utils.safestring import mark_safe
import re

register = Library()

@stringfilter
def breaksandspaces(value, autoescape=None):
    if autoescape:
        esc = conditional_escape
    else:
        esc = lambda x: x
    spaced = mark_safe(re.sub('[ \t\r\f\v]', '&'+'nbsp;', esc(value)))
    return mark_safe(re.sub('\n', '<br />', esc(spaced)))
breaksandspaces.needs_autoescape = True
register.filter(breaksandspaces)