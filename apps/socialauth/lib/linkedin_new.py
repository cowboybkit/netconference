import oauth2 as oauth
import urlparse
from xml.dom.minidom import parseString

class LinkedIn(object):

    LI_API_URL = "https://api.linkedin.com"
    REQUEST_TOKEN_URL = LI_API_URL + "/uas/oauth/requestToken"
    ACCESS_TOKEN_URL = LI_API_URL + "/uas/oauth/accessToken"
    AUTHORIZE_URL = LI_API_URL + "/uas/oauth/authorize"
    
    def __init__(self, key, secret):
        self.key = key
        self.secret = secret
    
    def get_request_token(self):
        consumer = oauth.Consumer(self.key, self.secret)
        client = oauth.Client(consumer)
        resp, content = client.request(self.REQUEST_TOKEN_URL, "POST")
        if resp['status'] != '200':
            raise Exception("Invalid response")
        request_token = dict(urlparse.parse_qsl(content))
        return request_token

    def get_access_token(self, request_token, oauth_verifier):
        token = oauth.Token(request_token['oauth_token'], request_token['oauth_token_secret'])
        token.set_verifier(oauth_verifier)
        consumer = oauth.Consumer(self.key, self.secret)
        client = oauth.Client(consumer, token)
        resp, content = client.request(self.ACCESS_TOKEN_URL, "POST")
        access_token = oauth.Token.from_string(content)
        return access_token

    def get_authorization_url(self):
        return self.AUTHORIZE_URL

class LinkedInApi(object):
    
    DEFAULT_PROFILE = "http://api.linkedin.com/v1/people/~"
    MY_PROFILE = "http://api.linkedin.com/v1/people/~:(id,first-name,last-name)"

    def __init__(self, key, secret):
        self.key = key
        self.secret = secret
        self.consumer = oauth.Consumer(key=self.key, secret=self.secret)

class MyProfileApi(LinkedInApi):
    
    def __init(self, key, secret):
        super(MyProfileApi, self).__init__(key, secret)

    def get_my_profile(self, token):
        client = oauth.Client(self.consumer, token)
        resp, content = client.request(self.MY_PROFILE)
        return self.create_person(content)

    def create_person(self, xml):
        dom = parseString(xml)
        personDom = dom.getElementsByTagName('person')
        p = personDom[0]
        id = p.getElementsByTagName('id')[0].firstChild.nodeValue
        fn = p.getElementsByTagName('first-name')[0].firstChild.nodeValue
        ln = p.getElementsByTagName('last-name')[0].firstChild.nodeValue
        person = Person()
        person.id = id
        person.firstname = fn
        person.lastname = ln
        return person

class Person(object):
    pass
