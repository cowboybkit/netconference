from django.db import models
from files.models import UserFile
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify
from django.utils.translation import ugettext_lazy as _

store_object_choices = (
_("Sales and Marketing"),
_("Self Help / Inspirational"),
_("Home Improvement"),
_("Accounting / Finance"),
_("Arts & Entertainment"),
_("Business"),
_("Careers & Work"),
_("Cars"),
_("Computers"),
_("Culture & Society"),
_("Education"),
_("Electronics"),
_("Fashion, Style & Personal Care"),
_("Food & Drink"),
_("Health"),
_("Hobbies, Games & Toys"),
_("Holidays & Celebrations"),
_("Home & Garden"),
_("Internet"),
_("Legal"),
_("Parenting"),
_("Parties & Entertaining"),
_("Personal Finance"),
_("Pets & Animals"),
_("Relationships & Family"),
_("Sports & Fitness"),
_("Travel"),
_("Weddings"),)

slugified_store_object_choices = dict([(slugify(el), el) for el in sorted(map(lambda x: unicode(x), store_object_choices))])
store_object_choices = sorted(map(lambda x: (unicode(x), unicode(x)), store_object_choices))


class StoreObject(models.Model):
    user = models.ForeignKey(User)
    file = models.ForeignKey(UserFile, unique=True)
    category = models.CharField(max_length = 100, choices=store_object_choices, null=True, blank=True)
    thumbnail = models.ImageField(upload_to = "store_thumbnails/")
    
    price = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=10)
    
    created = models.DateTimeField(auto_now_add = True)
    updated = models.DateTimeField(auto_now = True)
    
    @property
    def urlized_category(self):
        return slugify(self.category)
    
class UserPaypalDetail(models.Model):
    user = models.ForeignKey(User)
    email = models.EmailField()
    
    
class BoughtStoreObject(models.Model):
    store_object = models.ForeignKey(StoreObject)
    price_paid = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=10)
    is_bought = models.BooleanField(default = True)
    
    #Paypal data this is set after the IPN
    email = models.EmailField(null=True, blank=True)
    pay_key = models.CharField(max_length = 100, null=True, blank=True)
    ipn_response_string =  models.TextField(null=True, blank=True)
    
     
    
    created = models.DateTimeField(auto_now_add = True)
    updated = models.DateTimeField(auto_now = True)
