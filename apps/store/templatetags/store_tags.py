from django import template
from django.core.urlresolvers import reverse

import random
from paypal.standard.forms import PayPalPaymentsForm

register = template.Library()

@register.inclusion_tag('store/website_standard_form.html', takes_context=True)            
def store_paypal_form(context, store_object, paypal_rec_email):
    request = context["request"]
    store_owner =  context["store_owner"]
    ufile = store_object.file
    return_url = request.build_absolute_uri(reverse("store_file_sold", args=[ufile.urlhash, 0]))
    ipn_url = request.build_absolute_uri(reverse("store_file_paypal_ipn", args=[ufile.urlhash, 0]))
    cancel_url = request.build_absolute_uri(reverse("store_buy_user_files" , args=[store_owner.username, ], ))
    rand = random.randint(1000, 9999,)
    paypal_dict = {
        "business": paypal_rec_email,
        "amount": store_object.price,
        "item_name": store_object.file.title,
        "invoice": "%s-%s" % (store_object.pk, rand),
        "notify_url": ipn_url,
        "return_url": return_url,
        "cancel_return": cancel_url,

    }
    form = PayPalPaymentsForm(initial=paypal_dict)
    return {"form": form}

