from django.shortcuts import render_to_response, get_object_or_404
from django.core.urlresolvers import reverse
from django.conf import settings
from django.template.context import RequestContext
from django.http import HttpResponseRedirect, Http404, HttpResponse
from django.views.generic.list_detail import object_list
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required

from store.forms import SetPriceForm, UserPaypalForm
from libs.adaptive import get_adaptive_payment_url
from store.models import UserPaypalDetail, BoughtStoreObject, StoreObject
from store.utils import get_paypal_email, get_store_object_for_file
from netconf_utils.encode_decode import get_object_from_hash
from files.models import UserFile
from store.models import slugified_store_object_choices
from subscription.models import Subscription
from django.utils.translation import ugettext

_ = lambda x: unicode(ugettext(x))

@login_required
def index(request):
    return HttpResponseRedirect(reverse("store_buy_user_files", args=[request.user.username]))

def buy_user_files(request, username, category = None):
    is_adaptive_payment = getattr(settings, "IS_ADAPTIVE_PAYMENT", False)
    if category:
        category = slugified_store_object_choices.get(category.lower(), None)
    user = get_object_or_404(User, username = username)
    paypal_email = get_paypal_email(user)
    store_objects = StoreObject.objects.filter(user = user)
    if category:
        store_objects = store_objects.filter(category = category)

    extra_context =  {"store_owner": user, 
                      "paypal_email": paypal_email,
                      "is_adaptive_payment": is_adaptive_payment,
                      "categories": sorted(slugified_store_object_choices.items()),
                      "selected_category": category}
    return object_list(request, template_name="store/buy_user_files.html", queryset=store_objects, extra_context=extra_context)
    
@login_required
def file_sell(request, urlhash):
    #If a person is in a free Subscription
    free_sub = Subscription.objects.get_free()
    if free_sub == request.user.get_subscription():
        return render_to_response("store/free_account.html", {}, RequestContext(request))
    ufile = get_object_from_hash(UserFile, urlhash)
    
    if not ufile.user == request.user:
        raise Http404
    if ufile.is_selling():
        ufile.toggle_sale()
        request.user.message_set.create(message=_("The file has been successfully taken off the sale."))
        return HttpResponseRedirect(reverse("store_index"))
    else:
        form = SetPriceForm(ufile=ufile)
        if request.method == "POST":
            form = SetPriceForm(ufile=ufile, data=request.POST, files=request.FILES)
            if form.is_valid():
                form.save()
                request.user.message_set.create(message="You are now selling %s. This will be available in your store." % ufile.title)
                return HttpResponseRedirect(reverse("my_files"))
        payload = {"form": form, "ufile": ufile}
            
        return render_to_response("store/file_sell.html", payload, RequestContext(request))
    
def get_website_standard_payment_url(**kwargs):
    import urllib
    vars = urllib.urlencode(kwargs)
    paypal_url = "https://www.paypal.com/cgi-bin/webscr?cmd=_xclick&%s" % vars
    return paypal_url
    
@login_required
def file_paypal(request, urlhash):
    ufile = get_object_from_hash(UserFile, urlhash)
    cancel_url = request.build_absolute_uri(reverse("store_buy_user_files", args=[request.user.username]))
    paypal_email = get_paypal_email(ufile.user)
    store_object = store_obj = get_store_object_for_file(ufile)
    bought = BoughtStoreObject.objects.create(store_object = store_object, price_paid = store_object.price)
    return_url = request.build_absolute_uri(reverse("store_file_sold", args=[ufile.urlhash, bought.pk]))
    ipn_url = request.build_absolute_uri(reverse("store_file_paypal_ipn", args=[ufile.urlhash, bought.pk]))
    #We are making a Chained payment.
    #The file owner is the primary receiever
    #Nc takes split
    
    nc_split = store_obj.price * (settings.NC_STORE_SPLIT)/100
    owner_split = store_obj.price - nc_split
    recepients = [{"email":paypal_email, "amount": "%s"%owner_split, "primary": True}, {"email":settings.NC_PAYPAL_EMAIL, "amount": "%s"%nc_split, "primary": False}],
    
    if paypal_email and store_obj:
        is_adaptive_payment = getattr(settings, "IS_ADAPTIVE_PAYMENT", False)
        if is_adaptive_payment:
            redir_url = get_adaptive_payment_url(return_url, cancel_url, recepients)
        else:
            redir_url = get_website_standard_payment_url(return_url=return_url, 
                                                         cancel_url=cancel_url, 
                                                         business=paypal_email, 
                                                         item_name=store_obj.file.title,
                                                         amount=store_obj.price)
        if redir_url:
            return HttpResponseRedirect(redir_url)
        
        else:
            #We might have come here when we tried to make a Chained unilateral payments
            #lets try a Parallel payment.
            recepients = [{"email":paypal_email, "amount": "%s"%owner_split,}, {"email":settings.NC_PAYPAL_EMAIL, "amount": "%s"%nc_split,}],
            redir_url = get_adaptive_payment_url(return_url, cancel_url, recepients, ipnNotificationUrl=ipn_url)
            #TODO Handle this better
            if redir_url:
                return HttpResponseRedirect(redir_url)
        return render_to_response("store/paypal_error.html", {}, RequestContext(request))
            
    payload = {"paypal_email": paypal_email, "store_obj": store_obj, "ufile": ufile}
    return render_to_response("store/file_paypal.html", payload, RequestContext(request))
    
@login_required
def set_paypal(request):
    try:
        usr_pp = UserPaypalDetail.objects.get(user = request.user)
        form = UserPaypalForm(initial={"paypal_id":usr_pp.email})
    except UserPaypalDetail.DoesNotExist:
        form = UserPaypalForm()
    
    if request.method == "POST":
        form = UserPaypalForm(data = request.POST)
        if form.is_valid():
            email = form.cleaned_data["paypal_id"]
            try:
                pp_details = UserPaypalDetail.objects.get(user = request.user)
                pp_details.email = email
                pp_details.save()
            except UserPaypalDetail.DoesNotExist:
                UserPaypalDetail.objects.create(user = request.user, email = email)
            request.user.message_set.create(message="Your Paypal details have been set.")
            return HttpResponseRedirect(".")
    payload = {"form": form}
    return render_to_response("store/set_paypal.html", payload, RequestContext(request))

def file_sold(request, urlhash, bought_pk):
    "The file has been sold successfully"
    ufile = get_object_from_hash(UserFile, urlhash)
    store_object = get_object_or_404(StoreObject, file=ufile)
    try:
        bought = BoughtStoreObject.objects.get(pk = bought_pk)
    except BoughtStoreObject.DoesNotExist:
        try:
            bought = BoughtStoreObject.objects.filter(store_object = store_object)[0]
        except IndexError:
            bought = BoughtStoreObject.objects.create(store_object = store_object, 
                                                      price_paid = store_object.price)
    bought.is_bought = True
    bought.save()
    payload = {"ufile": ufile, "bought": bought, "store_object": store_object}
    return render_to_response("store/file_sold.html", payload, RequestContext(request))
    

def file_paypal_ipn(request, urlhash, bought_pk):
    "Paypal will ipn here."
    ufile = get_object_from_hash(UserFile, urlhash)
    store_object = get_object_or_404(StoreObject, file=ufile)
    try:
        bought = BoughtStoreObject.objects.get(pk = bought_pk)
    except BoughtStoreObject.DoesNotExist:
        try:
            bought = BoughtStoreObject.objects.filter(store_object = store_object)[0]
        except IndexError:
            bought = BoughtStoreObject.objects.create(store_object = store_object, price_paid = store_object.price)
    bought.ipn_response_string = str(request.GET.items())
    bought.save()
    payload = {"ufile": ufile, "bought": bought, "store_object": store_object}
    return HttpResponse("OK")

@login_required
def file_sold_list(request):
    "A list of the users sold files"
    sales = BoughtStoreObject.objects.filter(store_object__user = request.user)
    return object_list(request, template_name="store/file_sold_list.html", queryset=sales)
    
    
