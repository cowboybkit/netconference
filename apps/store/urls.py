from django.conf.urls.defaults import *
from django.views.generic.simple import direct_to_template
from django.views.generic.list_detail import object_list, object_detail

urlpatterns = patterns('store.views',
    url(r'^$', 'index', name='store_index'),
    url(r'^set-paypal/$', 'set_paypal', name='store_set_paypal'),
    
    url(r'^buy/(?P<urlhash>[-0-9A-Za-z_-]+)/$', 'file_sell',  name='store_file_sell'),
    url(r'^success/(?P<urlhash>[-0-9A-Za-z_-]+)/(?P<bought_pk>\d+)/$', 'file_sold',  name='store_file_sold'),
    url(r'^sold/$', 'file_sold_list',  name='store_file_sold_list'),
    url(r'^paypal/(?P<urlhash>[-0-9A-Za-z_-]+)/$', 'file_paypal',  name='store_file_paypal'),
    url(r'^paypal/ipn/(?P<urlhash>[-0-9A-Za-z_-]+)/(?P<bought_pk>\d+)/$', 'file_paypal_ipn',  name='store_file_paypal_ipn'),
    
    #Should be last
    url(r'^(?P<username>[\w\._-]+)/$', 'buy_user_files', name="store_buy_user_files"),
    url(r'^(?P<username>[\w\._-]+)/(?P<category>[\w\._-]+)/$', 'buy_user_files', name="store_buy_user_files_category"),
)