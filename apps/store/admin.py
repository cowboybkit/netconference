from django.contrib import admin

from store.models import StoreObject, UserPaypalDetail, BoughtStoreObject

admin.site.register([StoreObject, UserPaypalDetail, BoughtStoreObject])