from django.contrib.auth.models import User
from apps.store.models import UserPaypalDetail
from store.models import StoreObject


def get_paypal_email(user):
    try:
        pp_details = UserPaypalDetail.objects.get(user = user)
        return pp_details.email
    except UserPaypalDetail.DoesNotExist:
        return None
    
def get_store_object_for_file(ufile):
    try:
        return StoreObject.objects.get(file = ufile)
    except StoreObject.DoesNotExist:
        return None
            