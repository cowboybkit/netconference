from django import forms
from store.models import StoreObject
from django.utils.translation import ugettext_lazy as _

class SetPriceForm(forms.ModelForm):
    price = forms.DecimalField(max_digits=10)

    class Meta:
        model = StoreObject
        fields = ["price", "category", "thumbnail"]
    
    def __init__(self, ufile, *args, **kwargs):
        super(SetPriceForm, self).__init__(*args, **kwargs)
        self.ufile = ufile
        
    def save(self, *args, **kwargs):
        store_item = super(SetPriceForm, self).save(commit=False, *args, **kwargs)
        store_item.price = self.cleaned_data["price"]
        store_item.file = self.ufile
        store_item.user = self.ufile.user
        store_item.save()
        return store_item

    def clean_price(self):
        price = self.cleaned_data["price"]
        if price < 0:
            raise forms.ValidationError(_("Please provide a positive value."))
        return price
        
        
class UserPaypalForm(forms.Form):
    paypal_id = forms.EmailField()
    
        

