
import os
import datetime
import gzip
import subprocess

from django.conf import settings
from django.core.management.base import NoArgsCommand

from apps.storage import S3

class Command(NoArgsCommand):
    help = 'Take Media Backup and copy the dump to S3'
    
    def handle_noargs(self, **options):
        media_directory = getattr(settings, "MEDIA_ROOT")
        BACKUP_DIR = media_directory#settings.MEDIA_BACKUP_DIR
        
        backup_date = datetime.date.today().strftime('%Y-%m-%d')
        file_name = '%s_mediabackup_%s.tar.gz' % ("gqoupquality", backup_date)
        file_path =  os.path.join(BACKUP_DIR, file_name)
        
        #dump_cmd = 'pg_dump -s --username=%s %s' % (settings.DATABASE_USER, DATABASE_NAME)
        archive_cmd = 'tar -zcf %s %s' % (file_path, media_directory)
        print archive_cmd
        os.system(archive_cmd)
        #sub_process = subprocess.Popen(archive_cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        
        #outfile = open(file_path, "wb")
        #outfile.write(sub_process.communicate()[0])
        #outfile.close()
        
        self.upload_to_s3(file_name, file_path)
        
    def upload_to_s3(self, file_name, file_path):
        print "uploading " + file_name + " to Amazon S3..............."
    
        conn = S3.AWSAuthConnection(settings.AWS_ACCESS_KEY_ID, settings.AWS_SECRET_ACCESS_KEY)
        #get all buckets from amazon S3
        response =  conn.list_all_my_buckets()
        buckets = response.entries
        #is the bucket which you have specified is already there
        flag = False
        for bucket in buckets:
            if bucket.name == settings.BACKUP_BUCKET_NAME:
                flag = True
        
        #if there is no bucket with that name     
        if flag == False:
            print "There is no bucket with name " + settings.BACKUP_BUCKET_NAME + " in your Amazon S3 account"
            print "Error : Please enter an appropriate bucket name and re-run the script"
            return
        
        #upload file to Amazon S3    
        tardata = open(file_path, "rb").read()
        response = conn.put(settings.BACKUP_BUCKET_NAME, file_name, S3.S3Object(tardata))
        
        if response.http_response.status == 200 :
            print "sucessfully uploaded the archive to Amazon S3"
        else:
            print "Uploading database dump to Amazon S3 is not successful" 
            print "Error : " + response.message 
    
            