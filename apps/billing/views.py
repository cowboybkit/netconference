from django.shortcuts import render_to_response
from django.core import serializers
from django.utils import simplejson
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse

from billing.forms import CardForm, BillingInfoUpdateForm
from billing import passkey
from checkout.use_recurly import update_billing_info as rec_billing_info

@login_required
def add_card(request):
    if request.method == 'POST':
        post_data = request.POST.copy()
        # convert the POST variables into JSON format
        post_data.__delitem__('csrfmiddlewaretoken')
        json_data = simplejson.dumps(post_data)
        # encrypt the JSON
        encrypted_json = passkey.encrypt(json_data)
        # retrieve the encrypted JSON
        #decrypted_json = passkey.decrypt(encrypted_json)
        # convert the decrypted JSON into a dictionary
        #decrypted_data = simplejson.loads(decrypted_json)
        
        # store the newly encrypted data as a Card instance
        form = CardForm(post_data)
        card = form.save(commit=False)
        card.user = request.user
        card.num = post_data.get('card_number')[-4:]
        card.data = encrypted_json
        card.save()
    else:
        form = CardForm()
    if not request.user.is_staff:
        raise Http404
    else:
        return render_to_response("billing/add_card.html", locals(), context_instance=RequestContext(request))

@login_required
def update_billing_info(request):
    form_class = BillingInfoUpdateForm
    form = form_class()
    show_cc_form=True
    if request.method == 'POST':
        form = form_class(data=request.POST)
        if form.is_valid():
            acc_code = request.user.pk
            data = form.cleaned_data
            first_name = data['first_name']
            last_name = data['last_name']
            user_profile = request.user.profile_set.get()
            address = data['address']
            city = data['city']
            state = data['state']
            if state == '-':
                state = ''
            country = data['country']
            postal_code = data['postal_code']
            cc_number = data['credit_card_number']
            cc_cvv = data['credit_card_cvv']
            cc_exp_month = data['credit_card_expire_month']
            cc_exp_year = data['credit_card_expire_year']             
            message = rec_billing_info(acc_code, first_name, last_name, address, city, state, country, postal_code, cc_number, cc_cvv, cc_exp_month, cc_exp_year)
            if message:
                request.user.message_set.create(message="There seems to be some problem with your credit card")
            else:
                request.user.message_set.create(message='Credit card info successfully updated.')
            return HttpResponseRedirect(reverse('update_billing_info'))
    return render_to_response('billing/update_billing_info.html', {'form':form, 'show_cc_form':show_cc_form}, context_instance=RequestContext(request))
