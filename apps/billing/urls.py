from django.conf.urls.defaults import *
import views

urlpatterns = patterns('billing.views',
    (r'^add_card/$', 'add_card'),
    url(r'^update_billing_info/$', 'update_billing_info', name='update_billing_info'),
)
