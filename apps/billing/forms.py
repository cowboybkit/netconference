from django.db import models
from billing.models import Card
from django import forms
from datetime import datetime
from operator import itemgetter
from account.utils import COUNTRIES, STATES
from checkout.forms import CARD_TYPES, cc_expire_months, cc_expire_years, cardLuhnChecksumIsValid, strip_non_numbers
from django.utils.translation import ugettext_lazy as _, ugettext
import re

month_choice = []
# month_choice.append(('','- Month -'))
for i in range(1,13):
    if len(str(i)) == 1:
        numeric = '0' + str(i)
    else:
        numeric = str(i)
    month_choice.append((numeric, datetime(2009, i, 1).strftime('%B')))
MONTHS = tuple(month_choice)


calendar_years = []
# calendar_years.append(('','- Year -'))
for i in range(datetime.now().year, datetime.now().year+10):
    calendar_years.append((i,i))
YEARS = tuple(calendar_years)

class CardForm(forms.ModelForm):
    CARD_TYPES = (('Visa', 'Visa'),
                ('Amex', 'Amex'),
                ('Discover', 'Discover'),
                ('Mastercard', 'Mastercard'),)
    class Meta:
        model = Card
        exclude = ('data','num', 'user')
       
    cardholder_name = forms.CharField(max_length=100)
    card_number = forms.CharField(max_length=20)
    card_type = forms.ChoiceField(choices=CARD_TYPES)
    card_expire_month = forms.ChoiceField(choices=MONTHS)
    card_expire_year = forms.ChoiceField(choices=YEARS)

class BillingInfoUpdateForm(forms.Form):
    first_name = forms.CharField(widget=forms.TextInput({'class':'textfield_login', 'size':30}))
    last_name = forms.CharField(widget=forms.TextInput({'class':'textfield_login', 'size':30}))  
    address = forms.CharField(widget=forms.TextInput({'class':'textfield_login', 'size':30}))
    city = forms.CharField(widget=forms.TextInput({'class':'textfield_login', 'size':30})) 
    country = forms.CharField(widget=forms.Select({'size':1, 'style':'width:200px;'},choices=sorted(COUNTRIES, key=itemgetter(1))))
    state = forms.CharField(widget=forms.Select(choices=sorted(STATES, key=itemgetter(1))))
    postal_code = forms.CharField(widget=forms.TextInput({'class':'textfield_login', 'size':30}))  
    credit_card_number = forms.RegexField(widget=forms.TextInput({"class": "textfield_login"}),
                                          regex="\d{16}")
    credit_card_type = forms.CharField(widget=forms.Select(choices=CARD_TYPES))
    credit_card_expire_month = forms.IntegerField(widget=forms.Select(choices=cc_expire_months()),
                                                  initial=datetime.now().strftime('%m'))
    credit_card_expire_year = forms.IntegerField(widget=forms.Select(choices=cc_expire_years()),
                                                 initial=datetime.now().strftime('%Y'))
    credit_card_cvv = forms.RegexField(widget=forms.TextInput({"class": "textfield_login"}),
                                       regex="\d{3,4}")

    def clean_country(self):
        country = self.cleaned_data["country"].strip()
        if not country or country == u"-":
            raise forms.ValidationError(_("This field is required."))
        return country

    def clean_postal_code(self):
        postal_code = self.cleaned_data['postal_code']
        validity = re.match("^[a-zA-Z0-9 -]+$", postal_code)
        try:
            validity.group()
            return postal_code
        except:
            raise forms.ValidationError("The postal code must be a number or an alphanumeric.")

    def clean_credit_card_number(self):
        """ validate credit card number if valid per Luhn algorithm """
        cc_number = self.cleaned_data['credit_card_number']
        stripped_cc_number = strip_non_numbers(cc_number)
        if len(stripped_cc_number) != 16:
            raise forms.ValidationError(_("The credit card number you entered is invalid."))
        if not cardLuhnChecksumIsValid(stripped_cc_number):
            raise forms.ValidationError(_('The credit card number you entered is invalid.'))
        return cc_number

    def clean_credit_card_cvv(self):
        cvv = self.cleaned_data["credit_card_cvv"]
        if len(cvv) != 3 and len(cvv) !=4:
            raise forms.ValidationError(_("CVV can have a maximum of 4 digits."))
        return cvv

    def  clean(self):
        cleaned_data = self.cleaned_data
        expiry_month = cleaned_data.get("credit_card_expire_month")
        expiry_year = cleaned_data.get("credit_card_expire_year")
        expiry_date = datetime(expiry_year, expiry_month, 1)
        if expiry_date < datetime.today():
            raise forms.ValidationError(_("Credit Card is beyond expiry date."))
        return cleaned_data
