from django.contrib import admin

from signup_codes.models import SignupCode, SignupCodeResult

class SignupCodeAdmin(admin.ModelAdmin):
    list_display = ("code", "inviter", "max_uses", "use_count", "expiry", "created")
    list_filter = ("created",)

admin.site.register(SignupCode, SignupCodeAdmin)

class SignupCodeResultAdmin(admin.ModelAdmin):
    list_display = ("signup_code", "user", "timestamp")
    list_filter = ("timestamp",)

admin.site.register(SignupCodeResult, SignupCodeResultAdmin)