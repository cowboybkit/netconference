from datetime import datetime, timedelta

from django import forms
from django.conf import settings
from django.template.loader import render_to_string
from django.utils.hashcompat import sha_constructor
from django.utils.translation import ugettext_lazy as _, ugettext

from django.contrib.sites.models import Site

from mailer import send_html_mail as  send_mail

from account.forms import UserDetailsSignupForm as BaseSignupForm
from signup_codes.models import SignupCode, check_signup_code

from netconf_utils.multiemail_field import MultiEmailField

class SignupForm(BaseSignupForm):
    signup_code = forms.CharField(max_length=40, required=False, widget=forms.HiddenInput())
    
    def clean_signup_code(self):
        code = self.cleaned_data.get("signup_code")
        signup_code = check_signup_code(code)
        if signup_code:
            return signup_code
        raise forms.ValidationError("Signup code was not valid.")


class InviteUserForm(forms.Form):
    email = MultiEmailField()
    notes = forms.CharField(widget=forms.Textarea, label="Personal message:")
    def create_signup_code(self, commit=True, recipient=None, inviter=None, notes=None):
        if recipient is None:
            email = self.cleaned_data["email"]
        else:
            email = recipient
        if notes is not None:
            notes = self.cleaned_data["notes"]
        expiry = datetime.now() + timedelta(days=30)
        code = sha_constructor("%s%s%s%s" % (
            settings.SECRET_KEY,
            email,
            str(expiry),
            settings.SECRET_KEY,
        )).hexdigest()
        signup_code = SignupCode(code=code, email=email, max_uses=1, expiry=expiry, inviter=inviter, notes=notes)
        if commit:
            signup_code.save()
        return signup_code
    
    def send_signup_code(self, name=None, inviter=None, from_email="noreply@netconference.com", notes=None, req=None, generate_mail=None):
        
        current_site = Site.objects.get_current()
        domain = unicode(current_site.domain)
        
        subject = ugettext("A FREE Subscription to NetConference.com")
        
        for r in self.cleaned_data['email']:
            signup_code = self.create_signup_code(recipient=r, inviter=inviter, notes=notes)
            message = render_to_string("signup_codes/invite_user.txt", {
                "signup_code": signup_code,
                "domain": domain,
                "name": name,
                "notes": notes,
                "site": current_site,
                "MEDIA_URL": settings.MEDIA_URL,
            })
            if req is None:
                send_mail(subject, "", message, from_email, [r])
            else:
                if generate_mail is True:
                    return message
                else:
                    send_mail(subject, "", message, from_email, [r, req.user.email])
        return None
    
    def __init__(self, *args, **kwargs):
        super(InviteUserForm, self).__init__(*args, **kwargs)
        self.fields['email'].label ="Email Address(es) - separate multiple with commas"
 
        
