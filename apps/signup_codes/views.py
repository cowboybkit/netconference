from django.conf import settings
from django.shortcuts import render_to_response,redirect
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login
from django.utils.translation import ugettext as _
from django.core.urlresolvers import reverse
from django.contrib.admin.views.decorators import staff_member_required
import datetime

from account.utils import get_default_redirect
from signup_codes.models import check_signup_code
from netconference.apps.signup_codes.forms import SignupForm, InviteUserForm
from netconference.apps.subscription.models import Subscription, UserSubscription

def signup(request, form_class=SignupForm,
        template_name="account/signup.html", success_url=None):
    if success_url is None:
        success_url = reverse('conferencing')
    
    code = request.GET.get("code", None) or request.POST.get("signup_code", None)
    form = form_class(initial={"signup_code": code})
    
    if request.method == "POST":
        form = form_class(request.POST)
        if form.is_valid():
            username, password = form.save()
            user = authenticate(username=username, password=password)
            
            signup_code = form.cleaned_data["signup_code"]
            signup_code.use(user)
            
            auth_login(request, user)
            request.user.message_set.create(
                message=_("Successfully logged in as %(username)s.") % {
                'username': user.username
            })
            
            # making user active, seems like I shouldn't have to but here we are...
            request.user.is_active = True
            request.user.save()

            try:
                subscription = Subscription.objects.get(name="Pro")
            except Subscription.DoesNotExist:
                subscription = Subscription.objects.filter().order_by('-price')
                if subscription.count():
                    subscription = subscription[0]
            user_sub, created = UserSubscription.objects.get_or_create(user=request.user, subscription=subscription)
            # Set subscription variables
            user_sub.cancelled = 0
            user_sub.is_active = 1
            # user_sub.subscription = mo.product
            user_sub.extend(timedelta=datetime.timedelta(days=365))
            user_sub.fix()
            # Save subscription
            user_sub.save()
            request.user.message_set.create(message=_('Congrats! Your complimentary invite for 1 year premium membership is activated!'))
            
            return HttpResponseRedirect(success_url)

    #request.user.message_set.create(message='On signing up, you will get a free one year premium subscription!')
    return render_to_response(template_name, {
            "code": code,
            "form": form,
            "show_cc_form": False,
            "post_url":"/account/signup/promo/"
            }, context_instance=RequestContext(request))


@staff_member_required
def admin_invite_user(request, form_class=InviteUserForm,
        template_name="signup_codes/admin_invite_user.html"):
    """
    This view, by default, works inside the Django admin.
    """
    if request.method == "POST":
        print "req post is ", request.POST
        is_generate = request.POST['isGenerate']
        if is_generate:
            gen_mail = True
        else:
            gen_mail = False
        print "gen mail", gen_mail
        form = form_class(request.POST)
        if form.is_valid():
            if request.user.get_profile().name:
                name = request.user.get_profile().name
            else:
                name = request.user.username
            email = form.cleaned_data["email"]
            notes = form.cleaned_data["notes"]
            if request.user.email:
                from_email = request.user.email
            else:
                from_email = "noreply@netconference.com"
            message = form.send_signup_code(name=name, inviter=request.user, from_email=from_email, notes=notes, req=request, generate_mail=gen_mail)
            if message:
                #return render_to_response("signup_codes/generated_email.html", {'message':message})
                request.session['generated_message']=message
                return HttpResponseRedirect(reverse("generated_email"))
            request.user.message_set.create(message=_("An e-mail has been sent to %(email)s.") % {"email": email})
            form = form_class() # reset
    else:
        form = form_class()
    return render_to_response(template_name, {
        "title": _("Invite user"),
        "form": form,
    }, context_instance=RequestContext(request))


def promo(request):
    promo_code = request.GET.get('code')
    signup_code = check_signup_code(promo_code)
    if signup_code:
        form = SignupForm(initial={"signup_code": signup_code})
        
    else:
        request.user.message_set.create(message=_('This code is invalid or used up'))
        return redirect('home')
        
def show_generated_email(request):
    message = request.session.get('generated_message','')
    #del request.session['generated_message']
    return render_to_response("signup_codes/generated_email.html", {'message':message}, context_instance=RequestContext(request))
