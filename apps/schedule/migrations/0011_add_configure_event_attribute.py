# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding field 'Event.is_telephone'
        db.add_column('schedule_event', 'is_telephone', self.gf('django.db.models.fields.BooleanField')(default=False), keep_default=False)

        # Adding field 'Event.password_required'
        db.add_column('schedule_event', 'password_required', self.gf('django.db.models.fields.CharField')(default=None, max_length=255, null=True), keep_default=False)

        # Adding field 'Event.host_must_present'
        db.add_column('schedule_event', 'host_must_present', self.gf('django.db.models.fields.BooleanField')(default=False), keep_default=False)

        # Adding field 'Event.guest_have_approval'
        db.add_column('schedule_event', 'guest_have_approval', self.gf('django.db.models.fields.BooleanField')(default=False), keep_default=False)

        # Adding field 'Event.host_control_layout'
        db.add_column('schedule_event', 'host_control_layout', self.gf('django.db.models.fields.BooleanField')(default=True), keep_default=False)

        # Adding field 'Event.layout_type'
        db.add_column('schedule_event', 'layout_type', self.gf('django.db.models.fields.CharField')(default='presentation', max_length=255), keep_default=False)

        # Adding field 'Event.view_guest_list'
        db.add_column('schedule_event', 'view_guest_list', self.gf('django.db.models.fields.BooleanField')(default=True), keep_default=False)

        # Adding field 'Event.broadcast_video'
        db.add_column('schedule_event', 'broadcast_video', self.gf('django.db.models.fields.BooleanField')(default=True), keep_default=False)

        # Adding field 'Event.broadcast_audio'
        db.add_column('schedule_event', 'broadcast_audio', self.gf('django.db.models.fields.BooleanField')(default=True), keep_default=False)

        # Adding field 'Event.chat_with_host'
        db.add_column('schedule_event', 'chat_with_host', self.gf('django.db.models.fields.BooleanField')(default=True), keep_default=False)

        # Adding field 'Event.chat_with_guest'
        db.add_column('schedule_event', 'chat_with_guest', self.gf('django.db.models.fields.BooleanField')(default=True), keep_default=False)


    def backwards(self, orm):
        
        # Deleting field 'Event.is_telephone'
        db.delete_column('schedule_event', 'is_telephone')

        # Deleting field 'Event.password_required'
        db.delete_column('schedule_event', 'password_required')

        # Deleting field 'Event.host_must_present'
        db.delete_column('schedule_event', 'host_must_present')

        # Deleting field 'Event.guest_have_approval'
        db.delete_column('schedule_event', 'guest_have_approval')

        # Deleting field 'Event.host_control_layout'
        db.delete_column('schedule_event', 'host_control_layout')

        # Deleting field 'Event.layout_type'
        db.delete_column('schedule_event', 'layout_type')

        # Deleting field 'Event.view_guest_list'
        db.delete_column('schedule_event', 'view_guest_list')

        # Deleting field 'Event.broadcast_video'
        db.delete_column('schedule_event', 'broadcast_video')

        # Deleting field 'Event.broadcast_audio'
        db.delete_column('schedule_event', 'broadcast_audio')

        # Deleting field 'Event.chat_with_host'
        db.delete_column('schedule_event', 'chat_with_host')

        # Deleting field 'Event.chat_with_guest'
        db.delete_column('schedule_event', 'chat_with_guest')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'schedule.calendar': {
            'Meta': {'object_name': 'Calendar'},
            'events': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['schedule.Event']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '200', 'db_index': 'True'})
        },
        'schedule.calendarrelation': {
            'Meta': {'object_name': 'CalendarRelation'},
            'calendar': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['schedule.Calendar']"}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'distinction': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inheritable': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'object_id': ('django.db.models.fields.IntegerField', [], {})
        },
        'schedule.event': {
            'Meta': {'object_name': 'Event'},
            'broadcast_audio': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'broadcast_video': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'chat_with_guest': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'chat_with_host': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'creator': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']", 'null': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'end': ('django.db.models.fields.DateTimeField', [], {}),
            'end_recurring_period': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'force_registration': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'guest_have_approval': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'host_control_layout': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'host_must_present': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_recorded': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_recurring': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_telephone': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'layout_type': ('django.db.models.fields.CharField', [], {'default': "'presentation'", 'max_length': '255'}),
            'monthly': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'password_required': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '255', 'null': 'True'}),
            'private': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'reminder_interval': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1440'}),
            'reminder_sent': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'rule': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['schedule.Rule']", 'null': 'True', 'blank': 'True'}),
            'start': ('django.db.models.fields.DateTimeField', [], {}),
            'timezone': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'type': ('django.db.models.fields.CharField', [], {'default': "'S'", 'max_length': '1'}),
            'view_guest_list': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'weekly': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'})
        },
        'schedule.eventinvite': {
            'Meta': {'ordering': "['-sent_at']", 'object_name': 'EventInvite'},
            'body': ('django.db.models.fields.TextField', [], {}),
            'event': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'event_for'", 'to': "orm['schedule.Event']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'read_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'recipient': ('django.db.models.fields.TextField', [], {}),
            'sender': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'sent_invites'", 'to': "orm['auth.User']"}),
            'sender_deleted_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'sent_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'subject': ('django.db.models.fields.CharField', [], {'max_length': '120'})
        },
        'schedule.eventrelation': {
            'Meta': {'object_name': 'EventRelation'},
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'distinction': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True'}),
            'event': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['schedule.Event']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object_id': ('django.db.models.fields.IntegerField', [], {})
        },
        'schedule.rsvp': {
            'Meta': {'unique_together': "[('event', 'email_field')]", 'object_name': 'Rsvp'},
            'email_field': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'event': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'rsvped'", 'to': "orm['schedule.Event']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'schedule.rule': {
            'Meta': {'object_name': 'Rule'},
            'description': ('django.db.models.fields.TextField', [], {}),
            'frequency': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'params': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['schedule']
