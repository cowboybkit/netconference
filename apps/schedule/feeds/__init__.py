from schedule.models import Calendar, Event
from django.contrib.syndication.feeds import FeedDoesNotExist
from django.core.exceptions import ObjectDoesNotExist
from django.conf import settings
from schedule.feeds.atom import Feed
from schedule.feeds.icalendar import ICalendarFeed
from django.http import HttpResponse
import datetime, itertools
from django.contrib.sites.models import Site
from base64 import urlsafe_b64encode, urlsafe_b64decode
from timezones.utils import adjust_datetime_to_timezone

class UpcomingEventsFeed(Feed):
    feed_id = "upcoming"
    
    def feed_title(self, obj):
        return "Upcoming Events for %s" % obj.name
    
    def get_object(self, bits):
        if len(bits) != 1:
            raise ObjectDoesNotExist
        return Calendar.objects.get(pk=bits[0])
    
    def link(self, obj):
        if not obj:
            raise FeedDoesNotExist
        return obj.get_absolute_url()
    
    def items(self, obj):
        return itertools.islice(obj.occurrences_after(datetime.datetime.now()), 
            getattr(settings, "FEED_LIST_LENGTH", 10))
    
    def item_id(self, item):
        return str(item.id)
    
    def item_title(self, item):
        return item.event.title
    
    def item_authors(self, item):
        if item.event.creator is None:
            return [{'name': ''}]
        return [{"name": item.event.creator.username}]
    
    def item_updated(self, item):
        return item.event.created_on
    
    def item_content(self, item):
        return "%s \n %s" % (item.event.title, item.event.description)


class CalendarICalendar(ICalendarFeed):
    def items(self):
        cal_id = self.args[1]
        cal = Calendar.objects.get(pk=cal_id)
        
        return cal.events.all()

    def item_uid(self, item):
        return str(item.id)

    def item_start(self, item):
        return item.start

    def item_end(self, item):
        return item.end

    def item_summary(self, item):
        return item.title
        
    def item_created(self, item):
        return item.created_on
        

def uri_b64encode(s):
     return urlsafe_b64encode(s).strip('=')

def uri_b64decode(s):
    return urlsafe_b64decode(s + '=' * (4 - len(s) % 4))


class EventICalendar(ICalendarFeed):

    def items(self):
        event_id = int(uri_b64decode(str(self.args[0].GET['event']))) / 42
        cal = Event.objects.get(pk=event_id)
        cal.start = adjust_datetime_to_timezone(cal.start, settings.TIME_ZONE, cal.timezone)
        cal.end = adjust_datetime_to_timezone(cal.end, settings.TIME_ZONE, cal.timezone)
        return cal

    def item_uid(self, item):
        uid = uri_b64encode(str(int(item.id) * 42))
        return str(uid)

    def item_start(self, item):
        return item.start

    def item_end(self, item):
        return item.end

    def item_summary(self, item):
        return "NetConference: " + item.title

    def item_created(self, item):
        return item.created_on
    
    def item_rrule(self, item):
        return cal.rule.frequency
    # 
    # def item_url(self, item):
    #     current_site = Site.objects.get_current()
    #     return "http://www." + current_site.domain + item.get_absolute_url()

    def item_description(self, item):
        description = item.description
        request = self.args[0]
        url = "http://" + request.get_host() + item.get_absolute_url()  
        if item.type == 'G':
            return description + "\nClick on the link below and join the conference:\n" + url
        else:
            event_date = item.start.strftime("%B %d, %Y(%A)")
            event_time = item.start.strftime("%I:%M:%S %p")
            event_timezone = item.timezone
            return description + "\nLink for the conference is:\n" + url +"\nConference details:\n" +"Date: " + event_date + "\nTime: " + event_time + "\nTime Zone: " + event_timezone 
