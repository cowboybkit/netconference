import vobject
from django.http import HttpResponse, Http404
from base64 import urlsafe_b64encode, urlsafe_b64decode
from schedule.models import Event

def uri_b64encode(s):
     return urlsafe_b64encode(s).strip('=')

def uri_b64decode(s):
    return urlsafe_b64decode(s + '=' * (4 - len(s) % 4))

EVENT_ITEMS = (
    ('uid', 'uid'),
    ('dtstart', 'start'),
    ('dtend', 'end'),
    ('summary', 'summary'),
    ('description', 'description'),
    ('location', 'location'),
    ('last_modified', 'last_modified'),
    ('created', 'created'),
)

class ICalendarFeed(object):
     def __init__(self):
          self.args = []
          self.kwargs = {}

     def __call__(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs
         
        cal = vobject.iCalendar()
        request = self.args[0]

        if 'event' in request.GET:
             try:
                  item = self.items()
             except:
                  raise Http404
             event = cal.add('vevent')
             for vkey, key in EVENT_ITEMS:
                  value = getattr(self, 'item_' + key)(item)
                  if value:
                       event.add(vkey).value = value
             event_id = int(uri_b64decode(str(request.GET['event']))) / 42
             try:
                  ev_obj = Event.objects.get(pk=event_id)
             except Event.DoesNotExist:
                  raise Http404
             event_url = "http://" + request.get_host() + ev_obj.get_absolute_url()
             event.add('url').value = event_url
             if ev_obj.rule:
                  event_freq = "FREQ=" + ev_obj.rule.frequency
                  event.add('rrule').value = event_freq
        else:
             raise Http404

        response = HttpResponse(cal.serialize())
        response['Content-Type'] = 'text/calendar'
        return response

     def items(self):
        return []

     def item_uid(self, item):
        pass

     def item_start(self, item):
        pass

     def item_end(self, item):
        pass
    
     def item_rrule(self, item):
        pass

     def item_summary(self, item):
        return item.title

     def item_location(self, item):
        pass

     def item_last_modified(self, item):
        pass

     def item_created(self, item):
        pass

     def item_description(self, item):
         return item.description
