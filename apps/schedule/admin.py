from django.contrib import admin

from schedule.models import Calendar, Event, CalendarRelation, Rule, EventInvite, Rsvp

class CalendarAdminOptions(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}
    search_fields = ['name']
    
# class EventAdmin(admin.ModelAdmin):
#     search_fields = ['title','creator']
#     date_hierarchy = 'start'
    # prepopulated_fields = {"slug": ("title",)}


admin.site.register(Calendar, CalendarAdminOptions)
admin.site.register(Event)
admin.site.register(Rsvp)
admin.site.register([Rule, CalendarRelation])
admin.site.register(EventInvite)
