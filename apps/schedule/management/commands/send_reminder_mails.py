import time
from mailer import send_mail
from django.conf import settings
from schedule.models import Event, Rsvp
from datetime import datetime, timedelta
from vmail.models import VmailAttachment
from ccall_pins.models import ConferencePin
from django.template.loader import render_to_string
from timezones.utils import adjust_datetime_to_timezone
from django.core.management.base import NoArgsCommand

TIMEZONE_NAME_MAP = {"PST": "America/Los_Angeles",
                     "PDT": "America/Los_Angeles",
                     "UTC": "UTC",}

class Command(NoArgsCommand):
    help = "Send reminders for upcoming conferences"

    def handle_noargs(self, **options):
        now = datetime.now()
        events = Event.objects.filter(start__gte = now, start__lte= now + timedelta(days=2), reminder_sent=False)
        if not events.count():
            return None
        for event in events:
            event_time_in_local = adjust_datetime_to_timezone(event.start, event.timezone, settings.TIME_ZONE)
            now_time_in_local = adjust_datetime_to_timezone(now, TIMEZONE_NAME_MAP[time.tzname[0]], settings.TIME_ZONE)
            time_remaining = event_time_in_local - now_time_in_local
            if time_remaining <= timedelta(seconds=event.reminder_interval * 60) and event.reminder_sent == False:
                try:
                    ccall_pin = ConferencePin.objects.get(id=event.creator.id)
                except ConferencePin.DoesNotExist:
                    ccall_pin = None
                vmail_url = None
                vmail_attachment = list(VmailAttachment.objects.filter(invite=event))
                if len(vmail_attachment):
                    vmail_url = vmail_attachment[0].message.get_absolute_url()
                if vmail_url:
                    vmail_url = "http://www.netconference.com" + vmail_url
                message = render_to_string("vmail/delivery/vmail.html", {"conf_message": event.description,
                                                                         "user": event.creator,
                                                                         "conftime": event.start,
                                                                         "timezone": event.timezone,
                                                                         "ccall_pin": ccall_pin,
                                                                         "event": event,
                                                                         "message": event.description,
                                                                         "vmail_url": vmail_url,
                                                                         })
                if time_remaining == 0:
                    subject = "[Reminder] NetConference starting now at %s" %event.start
                else:
                    subject = "[Reminder] Upcoming NetConference at %s" %event.start
                from_email = settings.DEFAULT_FROM_EMAIL
                to_email = list(set(Rsvp.objects.filter(event=event).values_list('email_field', flat=True)))
                send_mail(subject, message, from_email, to_email,  priority="high")
                event.reminder_sent = True
                event.save()
