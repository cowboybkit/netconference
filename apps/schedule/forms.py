import datetime
import time
import re
import pytz

from django.utils.translation import ugettext_lazy as _
from django import forms
from django.core.urlresolvers import reverse
from django.utils.safestring import mark_safe
from django.contrib.sites.models import Site

from schedule.models import Event, Occurrence, EventInvite, Rsvp

from timezones.forms import TimeZoneField

PRETTY_TIMEZONE_CHOICES = []
for tz in pytz.common_timezones:
    now = datetime.datetime.now(pytz.timezone(tz))
    PRETTY_TIMEZONE_CHOICES.append((tz, _("GMT%(tz_offset)s : %(tz)s" % {"tz_offset": now.strftime("%z"), "tz": tz})))
PRETTY_TIMEZONE_CHOICES = sorted(PRETTY_TIMEZONE_CHOICES, key=lambda s: s[1].split(":")[0])

class GlobalSplitDateTimeWidget(forms.SplitDateTimeWidget):
    def __init__(self, render_tr=True, css_class="datetime", hour24=False, attrs=None, *args, **kwargs):
        """
        hour24 is a boolean. If set to true it will display like a normal
        SplitDateTimeWidget, else it will have a choice for am, pm.

        In reference to http://netconference.unfuddle.com/a#/projects/1/tickets/by_number/358
        modified format_output to render the table rows for all widgets. 
        """
        super(GlobalSplitDateTimeWidget, self).__init__(attrs, *args, **kwargs)
        if not hour24:
            self.widgets = [
                forms.TextInput(attrs=attrs),
                forms.TextInput(attrs=attrs),
                forms.Select(attrs=attrs, choices=[('AM',_('AM')),('PM',_('PM'))]),
            ]
        self.hour24 = hour24
        self.css_class = css_class
        
    def decompress(self, value):
        if value:
            if self.hour24:
                return super(GlobalSplitDateTimeWidget,self).decompress(value)
            else:
                return [value.date(), value.strftime("%I:%M"), value.strftime("%p")]
        return ""
    
    def value_from_datadict(self, data, files, name):
        if self.hour24:
            return super(GlobalSplitDateTimeWidget,self).value_from_datadict(data, files, name)
        formats = ['%s %%p' % s.replace('H', 'I') for s in forms.DEFAULT_DATETIME_INPUT_FORMATS]
        date_list = []
        try:
            date_list.extend([widget.value_from_datadict(data, files, name + '_%s' % i) for i, widget in enumerate(self.widgets)])
        except TypeError:
            return None
        if len(filter(None, date_list)) != len(self.widgets):
            return None
        date_str = ' '.join(date_list)
        for format in formats:
            try:
                return datetime.datetime(*time.strptime(date_str, format)[:6])
            except ValueError:
                continue
            
    def format_output(self, rendered_widgets):
        return """
                <tr class="%s">
                    <td>Date: </td>
                    <td>%s</td>
                </tr>
                <tr class="%s">
                    <td>Time: </td>
                    <td>%s%s</td>
                </tr>
               """ %(self.css_class,
                     rendered_widgets[0], 
                     self.css_class,
                     rendered_widgets[1],
                     rendered_widgets[2])

class SpanForm(forms.ModelForm):

    start = forms.DateTimeField(widget=forms.SplitDateTimeWidget)
    end = forms.DateTimeField(widget=forms.SplitDateTimeWidget, help_text = _("The end time must be later than start time."))

    def clean_end(self):
        if self.cleaned_data['end'] <= self.cleaned_data['start']:
            raise forms.ValidationError(_("The end time must be later than start time."))
        return self.cleaned_data['end']
    


class EventForm(forms.ModelForm):
    start = forms.DateTimeField(widget=GlobalSplitDateTimeWidget(), required=False)
    end_recurring_period = forms.DateTimeField(widget=GlobalSplitDateTimeWidget(css_class="hidden"), help_text = _("This date is ignored for one time only events."), required=False)
    
    timezone = TimeZoneField(label=_("Timezone"), required=True, choices=PRETTY_TIMEZONE_CHOICES)

    class Meta:
        model = Event
        exclude = ('creator', 'created_on', 'event_type', 'end', 'type')

    def clean_title(self):
        title = self.cleaned_data["title"]
        title = title.strip()
        if not title:
            raise forms.ValidationError(_("Please enter a value."))
        return title

    def clean_start(self):
        start = self.cleaned_data["start"]
        if not start:
            raise forms.ValidationError(_("Please provide both start date and time."))
        if start < datetime.datetime.today():
            raise forms.ValidationError(_("Start Date cannot be in past."))
        return start

    def clean_end(self):
        end = self.cleaned_data["end"]
        start = self.cleaned_data["start"]
        if end < start:
            raise forms.ValidationError(_("End Date cannot be less than Start Date."))
        return end

    def clean_end_recurring_period(self):
        end_recurring_period= self.cleaned_data["end_recurring_period"]
        start = self.cleaned_data.get("start", None)
        if start and end_recurring_period and end_recurring_period < start:
            raise forms.ValidationError(_("End Recurring Period cannot be less than Start Date."))
        return end_recurring_period

    def clean(self):
        cleaned_value = super(EventForm, self).clean()
        if not cleaned_value["is_recurring"] or not cleaned_value["rule"]:
            cleaned_value.pop("end_recurring_period", None)
        if cleaned_value["is_recurring"] and cleaned_value["rule"] and \
                not cleaned_value.get("end_recurring_period", None):
            raise forms.ValidationError(_("A recurring event must have an end date and time."))
        return cleaned_value

# class OccurrenceForm(SpanForm):
# 
#     class Meta:
#         model = Occurrence
#         exclude = ('original_start', 'original_end', 'event', 'cancelled')

class EventInviteForm(forms.Form):
    """
    A simple default form for email invitations to an event.
    """
    recipient = forms.CharField(label=_(u"Recipient (separate multiple email addresses with a comma):"))
    subject = forms.CharField(label=_(u"Subject:"))
    body = forms.CharField(label=_(u"Body:"), widget=forms.Textarea())

    def __init__(self, *args, **kwargs):
        recipient_filter = kwargs.pop('recipient_filter', None)
        recipient = kwargs.pop('recipient', None)
        super(EventInviteForm, self).__init__(*args, **kwargs)
        if recipient_filter is not None:
            self.fields['recipient']._recipient_filter = recipient_filter

    def save(self, sender, event=None, parent_msg=None, recipient=None):
        # from_email = sender.email
        if sender.first_name and sender.last_name:
            from_string = "%s %s <%s>" % (sender.first_name, sender.last_name, sender.email)
        else:
            from_string = sender.email
        subject = self.cleaned_data['subject']
        body = self.cleaned_data['body']
        # recipient = self.cleaned_data['recipient']
        recipient_string = str(recipient)
        if recipient_string[-1:] == ',':
            recipient_string = recipient_string[0:-1]
        else:
            recipient_string = recipient_string
        message_list = []
        msg = EventInvite(
            sender = sender,
            recipient = recipient_string,
            subject = subject,
            body = body,
            event = event,
        )
        current_site = Site.objects.get_current()
        site_domain = current_site.domain
        event_url = "http://%s%s" % (site_domain, event.get_absolute_url())
        
        # add linebreaks and spaces to vmail body
        esc = lambda x: x
        spaced = mark_safe(re.sub('[ \t\r\f\v]', '&'+'nbsp;', esc(body)))
        body_breaksandspaces = mark_safe(re.sub('\n', '<br />', esc(spaced)))
        
        template_name = 'schedule/delivery/invite'
        template_context = {
            "user": sender,
            "message": body_breaksandspaces,
            "event_url": event_url,
        }
        if parent_msg is not None:
            msg.parent_msg = parent_msg
            parent_msg.replied_at = datetime.datetime.now()
            parent_msg.save()
        msg.save()
        message_list.append(msg)
        for r in recipient_string.split(','):
            #the below line works but I'm commenting it out in lieu of the multipart method
            #send_vmail = Vmail.objects.send_vmail(sender, r, subject, body, instance)
            send_invite = EventInvite.objects.send_multipart_mail(template_name, template_context, subject, r, from_string)
            #user.message_set.create(message="Message sent to %s" % recipient_string)
        return message_list

class RsvpForm(forms.ModelForm):
    event = forms.IntegerField(widget=forms.HiddenInput)

    class Meta:
        model = Rsvp

    def clean_name(self):
        name = self.cleaned_data["name"].strip()
        if not name:
            raise forms.ValidationError(_("This field is required."))
        return name

    def clean_event(self):
        try:
            event = Event.objects.get(id=self.cleaned_data["event"])
        except Event.DoesNotExist:
            raise forms.ValidationError(_("Event does not exist."))
        return event
