from django.conf.urls.defaults import *
from django.views.generic.list_detail import object_list
from schedule.models import Calendar
from schedule.periods import Year, Month, Week, Day
from schedule.feeds import CalendarICalendar, EventICalendar
from apps.friends.models import *
from django.views.generic.simple import direct_to_template

info_dict = {
    'queryset': Calendar.objects.all(),
}

urlpatterns = patterns('',

    url(r'^$', 
        'schedule.views.calendar_by_periods2', 
        name = "my_events",
        kwargs={'periods': [Week], 'template_name': 'all_events.html'}),
    
    # By calendar_slug
    url(r'^calendar/year/(?P<calendar_slug>[-\w]+)/$', 'schedule.views.calendar_year', name="year_calendar"),
    url(r'^calendar/year/(?P<calendar_slug>[-\w]+)/(?P<year>\d+)/$', 'schedule.views.calendar_year', name="year_calendar_date"),
    url(r'^calendar/tri_month/(?P<calendar_slug>[-\w]+)/$', 'schedule.views.calendar_tri_month', name="tri_month_calendar"),
    url(r'^calendar/tri_month/(?P<calendar_slug>[-\w]+)/(?P<year>\d+)/(?P<month>\d+)/$', 'schedule.views.calendar_tri_month', name="tri_month_calendar_date"),
    url(r'^calendar/compact_month/(?P<calendar_slug>[-\w]+)/$', 'schedule.views.calendar_compact_month', name="c_calendar"),
    url(r'^calendar/compact_month/(?P<calendar_slug>[-\w]+)/(?P<year>\d+)/(?P<month>\d+)/$', 'schedule.views.calendar_compact_month', name = "c_calendar_date"),
    url(r'^calendar/month/(?P<calendar_slug>[-\w]+)/$', 'schedule.views.calendar_month', name="m_calendar"),
    #url(r'^calendar/month/(?P<calendar_slug>[-\w]+)/(?P<year>\d+)/(?P<month>\d+)/$', 'schedule.views.calendar', name = "m_calendar_date"),
    url(r'^calendar/month/(?P<year>\d+)/(?P<month>\d+)/$', 'schedule.views.calendar', name = "m_calendar_date"),
    url(r'^calendar/week/(?P<calendar_slug>[-\w]+)/$', 'schedule.views.calendar_week', name="w_calendar"),
    url(r'^calendar/week/(?P<calendar_slug>[-\w]+)/(?P<year>\d+)/(?P<month>\d+)/(?P<day>\d+)/$', 'schedule.views.calendar_week', name = "w_calendar_date"),
    url(r'^calendar/daily/(?P<calendar_slug>[-\w]+)/$', 'schedule.views.calendar_day', name="d_calendar"),
    #url(r'^calendar/daily/(?P<calendar_slug>[-\w]+)/(?P<year>\d+)/(?P<month>\d+)/(?P<day>\d+)/$', 'schedule.views.calendar_day', name = "d_calendar_all_date"),
    url(r'^calendar/daily/(?P<year>\d+)/(?P<month>\d+)/(?P<day>\d+)/$', 'schedule.views.calendar_day', name = "d_calendar_date"),
    url(r'^calendar/(?P<calendar_slug>[-\w]+)/$', 'schedule.views.calendar', name="s_calendar"),
    url(r'^calendar/$', 'schedule.views.calendar', name="my_calendar"),

# 
#    url(r'^calendar/(?P<calendar_slug>[-\w]+)/(?P<year>\d+)/(?P<month>\d+)/$', 'schedule.views.calendar', name = "s_calendar_date"),
    url(r'^calendar/(?P<username>[\w\._-]+)/(?P<year>\d+)/(?P<month>\d+)/$', 'schedule.views.calendar', name = "s_calendar_date"),
    url(r'^event/create/(?P<calendar_slug>[-\w]+)/(?P<year>\d+)/(?P<month>\d+)/(?P<day>\d+)/(?P<hour>\d+)/(?P<minute>\d+)/$', 'schedule.views.create_event', name='s_create_event_date'),

    #By calendar id
    url(r'^calendar/by_id/year/(?P<calendar_id>\d+)/$', 'schedule.views.calendar_year', name="year_calendar_by_id"),
    url(r'^calendar/by_id/year/(?P<calendar_id>\d+)/(?P<year>\d+)/$', 'schedule.views.calendar_year', name="year_calendar_by_id_date"),
    url(r'^calendar/by_id/tri_month/(?P<calendar_id>\d+)/$', 'schedule.views.calendar_tri_month', name="tri_month_calendar_by_id"),
    url(r'^calendar/by_id/tri_month/(?P<calendar_id>\d+)/(?P<year>\d+)/(?P<month>\d+)/$', 'schedule.views.calendar_tri_month', name="tri_month_calendar_by_id_date"),
    url(r'^calendar/by_id/compact_month/(?P<calendar_id>\d+)/$', 'schedule.views.calendar_compact_month', name="c_calendar_by_id"),
    url(r'^calendar/by_id/compact_month/(?P<calendar_id>\d+)/(?P<year>\d+)/(?P<month>\d+)/$', 'schedule.views.calendar_compact_month', name = "c_calendar_by_id_date"),
    url(r'^calendar/by_id/month/(?P<calendar_id>\d+)/$', 'schedule.views.calendar_month', name="m_calendar_by_id"),
    url(r'^calendar/by_month/month/(?P<calendar_id>\d+)/(?P<year>\d+)/(?P<month>\d+)/$', 'schedule.views.calendar_month', name = "m_calendar_by_id_date"),
    url(r'^calendar/week/(?P<calendar_id>\d+)/$', 'schedule.views.calendar_week', name="w_calendar_by_id"),
    url(r'^calendar/week/(?P<calendar_id>\d+)/(?P<year>\d+)/(?P<month>\d+)/(?P<day>\d+)/$', 'schedule.views.calendar_week', name = "w_calendar_date_by_id"),
    url(r'^calendar/by_id/daily/(?P<calendar_id>\d+)/$', 'schedule.views.calendar_day', name="d_calendar_by_id"),
    url(r'^calendar/by_id/daily/(?P<calendar_id>\d+)/(?P<year>\d+)/(?P<month>\d+)/(?P<day>\d+)/$', 'schedule.views.calendar_day', name = "d_calendar_by_id_date"),
    url(r'^calendar/by_id/(?P<calendar_id>\d+)/$', 'schedule.views.calendar', name="s_calendar_by_id"),
    url(r'^calendar/by_id/(?P<calendar_id>\d+)/(?P<year>\d+)/(?P<month>\d+)/$', 'schedule.views.calendar', name = "s_calendar_by_id_date"),
    url(r'^calendar/(?P<calendar_id>\d+)/create_event/$', 'schedule.views.create_or_edit_event', name="s_create_event_in_calendar"),
    url(r'^event/create/by_id/(?P<calendar_id>\d+)/(?P<year>\d+)/(?P<month>\d+)/(?P<day>\d+)/(?P<hour>\d+)/(?P<minute>\d+)/$', 'schedule.views.create_event', name='s_create_event_by_id_date'),

    url(r'^event/create/$', 'schedule.views.create_or_edit_event', name='s_create_event'),
#    url(r'^event/edit/(?P<urlhash>[-0-9A-Za-z_-]+)/$', 'conferencing.views.edit_conference', name='s_edit_event'),
    
    url(r'^event/delete/(?P<urlhash>[-0-9A-Za-z_-]+)/$', 'schedule.views.del_event', name="s_delete_event"),
    
    url(r'^invite/invite_email_ajax/$', "schedule.views.invite_email_ajax", name="invite_email_ajax"),
    url(r'^invite/invite_contacts_ajax/$', 'schedule.views.invite_contacts_ajax', name="invite_contacts_ajax"),
    url(r'^invite/invite_groups_ajax/$', 'schedule.views.invite_groups_ajax', name="invite_groups_ajax"),
    url(r'^invite/(?P<urlhash>[-0-9A-Za-z_-]+)/$', 'schedule.views.compose_event_invite', name="compose_event_invite"),

    url(r'^event/(?P<urlhash>[-0-9A-Za-z_-]+)/rsvp/$', 'schedule.views.rsvp_for_event', name="rsvp_for_event"),
    url(r'^event/(?P<urlhash>[-0-9A-Za-z_-]+)/stats/$', 'schedule.views.conf_stats', name="conference_statistics"),
    url(r'^event/get_guest/(?P<urlhash>[-0-9A-Za-z_-]+)/$', 'schedule.views.get_guests', name='get_guests'),
    url(r'^event/watch_recording/(?P<urlhash>[-0-9A-Za-z_-]+)/$', 'schedule.views.watch_recording', name='watch_recording'),
    
    #feed urls 
    # url(r'^feed/calendar/(.*)/$',
    #     'django.contrib.syndication.views.feed', 
    #     { "feed_dict": { "upcoming": UpcomingEventsFeed } }),

    url(r'^ical/calendar/(.*)/$', CalendarICalendar(), name="calendar_ical"),
    url(r'^ical/event/netconference.ics$', EventICalendar(), name="event_ical"),
    
    # Don't put anything after this line or you will waste hours of time and be very sorry.
    url(r'$', object_list, info_dict, name='schedule'),
)
