from django.shortcuts import render_to_response, get_object_or_404
from django.views.generic.create_update import delete_object
from django.views.generic.list_detail import object_list
from django.http import HttpResponseRedirect, Http404, HttpResponse
from django.template import RequestContext
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.contrib.auth.decorators import user_passes_test, login_required
from django.contrib.sites.models import Site
from django.utils.translation import ugettext_lazy as _
from django.views.decorators.http import require_POST
from django.conf import settings
import settings
from timezones.utils import adjust_datetime_to_timezone
from base64 import urlsafe_b64encode, urlsafe_b64decode
import gdata.calendar.service
import gdata.service
import os
import datetime
import logging
from schedule.forms import EventForm, EventInviteForm, RsvpForm
from schedule.models import *
from schedule.periods import *
from account.google import *
from conferencing.models import Attendees, TimezoneConference, ReminderConference,RecordedConference,\
    ConferenceFolder
from schedule.utils import check_event_permissions, coerce_date_dict
from tagging.models import Tag, TaggedItem
from account.models import Account
from subscription.models import UserSubscription, Subscription
from ccall_pins.models import ConferencePin
from netconference.apps.friends.models import Contact
from friends_app.models import ContactGroup
from friends.models import Friendship, FriendshipInvitation
from friends.forms import InviteFriendForm
from files.models import UserFile
from user_theme.models import UserTheme
from notification.models import *
from notification.decorators import basic_auth_required, simple_basic_auth_callback
from project.models import Project, SubscribedUser
from django.db.models import Q
from django.views.generic.simple import direct_to_template
from vmail.models import VmailAttachment
from store.models import StoreObject
from store.utils import get_paypal_email
from pytz import UnknownTimeZoneError
import pytz
from ccall_pins.models import ConferenceBridgeNumber
from netconf_utils.encode_decode import uri_b64encode, uri_b64decode, get_object_from_hash
from friends.models import Contact
from dateutil.relativedelta import relativedelta
from httplib import HTTP

MAX_FRIENDS = 11

if hasattr(settings,"SCHEDULE_EVENT_EDITOR_TEST"):
    test_user_function = getattr(settings,"SCHEDULE_EVENT_EDITOR_TEST") or (lambda u: u.is_authenticated())
else:
    test_user_function = lambda u: u.is_authenticated()

def calendar(request, calendar_id=None,calendar_slug=None, year=None, month=None, user_obj=None,
              username=None, periods=None, template='schedule/calendar.html'):
    if username:
        if username == request.user.username:
            is_public = False
        else:
            is_public = True
        user_obj = get_object_or_404(User, username=username)
    else:
        if request.user.is_anonymous():
            raise Http404
        else:
            is_public = False
            user_obj = request.user
    if calendar_id:
        calendar = get_object_or_404(Calendar, id = calendar_id)
    elif calendar_slug:
        calendar = get_object_or_404(Calendar, slug = calendar_slug)
    else:
        calendar = get_object_or_404(Calendar, id = 1)
    if year and month:
        if is_public:
            month = calendar.get_my_public_month(datetime.date(int(year),int(month),1), user=user_obj)
        else:
            month = calendar.get_my_month(datetime.date(int(year), int(month), 1), user=user_obj)
    else:
        if is_public:
            month = calendar.get_my_public_month(user=user_obj)
        else:
            month = calendar.get_my_month(user=user_obj)
    if is_public:
        filter = {"creator": user_obj.id, "private":False}
    else:
        filter = {"creator": user_obj.id}
    eventtags = Tag.objects.cloud_for_model(Event, filters=filter)
    start = datetime.datetime.now()
    end = start + datetime.timedelta(days=7)
    periods = [Week]
    if is_public:
        period_objects = dict([(period.__name__.lower(), period(calendar.events.filter(creator=user_obj, private=False), start)) for period in periods])
    else:
        period_objects = dict([(period.__name__.lower(), period(calendar.events.filter(creator=user_obj), start)) for period in periods])
    try:
        account = Account.objects.get(user=user_obj)
    except Account.DoesNotExist:
        account = None
    try:
        timezone = account.timezone
    except AttributeError:
        timezone = settings.TIME_ZONE
    ccall_pin = ConferencePin.objects.get(id=user_obj.id)
    
    user_id = user_obj.id
    paypal_email = get_paypal_email(user_obj)
    store_owner = user_obj
    if is_public:
        files = UserFile.active_objects.filter(user=user_id).filter(visibility='U').order_by('-timestamp')[:6]
        store_objects = StoreObject.objects.filter(user=user_id, file__visibility='U')[:6]
    else:
        files = UserFile.active_objects.filter(user=user_id).order_by('-timestamp')[:6]
        store_objects = StoreObject.objects.filter(user=user_id)[:6]
    
    try:
        user_theme = UserTheme.objects.get(user=user_obj)
    except UserTheme.DoesNotExist:
        user_theme = None
    other_friends = Friendship.objects.friends_for_user(user_obj)
    
    if request.user.is_authenticated():
        netsign_invites_received = user_obj.netsign_to.all()
        netsign_invites_sent = user_obj.netsign_from.all()
    else:
        netsign_invites_received = None
        netsign_invites_sent = None
    
    notices = Notice.objects.notices_for(user_obj, on_site=True)
    
    if request.user.is_authenticated():
        projects = [ii for ii in Project.objects.filter(owner=user_obj)] + [SubscribedUser.objects.filter(user=user_obj)]
    else:
        projects = Project.objects.filter(owner=user_obj)
                
        
        
    if request.user.is_authenticated():
        is_friend = Friendship.objects.are_friends(request.user, user_obj)
        if request.user == user_obj:
            is_me = True
        else:
            is_me = False
    else:
        is_friend = False
        is_me = False
    
    if is_friend:
        invite_form = None
        previous_invitations_to = None
        previous_invitations_from = None
        if request.method == "POST":
            if request.POST.get("action") == "remove": # @@@ perhaps the form should just post to friends and be redirected here
                Friendship.objects.remove(request.user, user_obj)
                request.user.message_set.create(message=_("You have removed %(from_user)s from friends") % {'from_user': user_obj})
                is_friend = False
                invite_form = InviteFriendForm(request.user, {
                    'to_user': username,
                    'message': ugettext("Let's be friends!"),
                })
    
    else:
        if request.user.is_authenticated() and request.method == "POST":
            if request.POST.get("action") == "invite": # @@@ perhaps the form should just post to friends and be redirected here
                invite_form = InviteFriendForm(request.user, request.POST)
                if invite_form.is_valid():
                    invite_form.save()
            else:
                invite_form = InviteFriendForm(request.user, {
                    'to_user': username,
                    'message': ugettext("Let's be friends!"),
                })
                invitation_id = request.POST.get("invitation", None)
                if request.POST.get("action") == "accept": # @@@ perhaps the form should just post to friends and be redirected here
                    try:
                        invitation = FriendshipInvitation.objects.get(id=invitation_id)
                        if invitation.to_user == request.user:
                            invitation.accept()
                            request.user.message_set.create(message=_("You have accepted the friendship request from %(from_user)s") % {'from_user': invitation.from_user})
                            is_friend = True
                    except FriendshipInvitation.DoesNotExist:
                        pass
                elif request.POST.get("action") == "decline": # @@@ perhaps the form should just post to friends and be redirected here
                    try:
                        invitation = FriendshipInvitation.objects.get(id=invitation_id)
                        if invitation.to_user == request.user:
                            invitation.decline()
                            request.user.message_set.create(message=_("You have declined the friendship request from %(from_user)s") % {'from_user': invitation.from_user})
                    except FriendshipInvitation.DoesNotExist:
                        pass
        elif request.user.is_authenticated():
            invite_form = InviteFriendForm(request.user, {
                'to_user': username,
                'message': ugettext("Let's be friends!"),
            })
        else:
            invite_form = None
    
            
    if request.user.is_authenticated():
        previous_invitations_to = FriendshipInvitation.objects.invitations(to_user=user_obj, from_user=request.user)
        previous_invitations_from = FriendshipInvitation.objects.invitations(to_user=request.user, from_user=user_obj)
    else:
        previous_invitations_to = None
        previous_invitations_from = None
    
    allow_join = True
    if not is_public:
        from restrictions.used_restrictions import ConferenceMinutesRestriction
        cmr = ConferenceMinutesRestriction(request.user)
        allow_join = cmr.allow()

    try:
        general_conference = Event.objects.get(creator=user_obj, type="G")
    except Event.DoesNotExist:
        general_conference = None
    
    conf_bridge = ConferenceBridgeNumber.objects.get_or_create(user_obj)
    conference_line = user_obj.get_conference_phonenumber()
    access_code = user_obj.get_conference_modcode()
    return render_to_response(template, {
        "projects": projects,
        "general_conference": general_conference,
        "calendar": calendar,
        "month": month,
        "day_names": weekday_abbrs,
        "user_obj": user_obj,
        "eventtags": eventtags,
        "periods": period_objects,
        "is_public": is_public,
        "timezone": timezone,
        "ccall_pin": ccall_pin,
        "files": files,
        "user_theme": user_theme,
        "other_friends": other_friends,
        "thumb_url": settings.THUMBNAIL_URL,
        "netsign_invites_received": netsign_invites_received,
        "netsign_invites_sent": netsign_invites_sent,
        "notices": notices,
        "is_me": is_me,
        "is_friend": is_friend,
        "other_user": user_obj,
        "invite_form": invite_form,
        "previous_invitations_to": previous_invitations_to,
        "previous_invitations_from": previous_invitations_from,
        'allow_join': allow_join,
        'paypal_email': paypal_email,
        'store_owner': store_owner,
        'store_objects': store_objects,
        'conf_bridge' : conf_bridge,
        'conference_line' : conference_line,
        'access_code' : access_code,
    }, context_instance=RequestContext(request))

from restrictions.views import get_context
from restrictions.used_restrictions import ConferenceMinutesRestriction

@login_required
def launcher(request, calendar_id=None,calendar_slug=None, year=None, month=None, user_obj=None,
              username=None, periods=None, template='schedule/launcher3.html'):
    user_obj = request.user
    run_joan = True
    if datetime.datetime.now() - user_obj.date_joined > datetime.timedelta(days=7):
        run_joan = False
    cmr = ConferenceMinutesRestriction(request.user)
    allow_join = cmr.allow()
    usub = user_obj.get_usersubscription()
    sub = usub.subscription
    biggest_sub =  Subscription.objects.order_by("-price")[0]
    show_upgrade = True
    try:
        general_conference = Event.objects.get(creator=request.user, type="G")
    except Event.DoesNotExist:
        general_conference = None
    if sub == biggest_sub:
        show_upgrade = False
    calendar = get_object_or_404(Calendar, id = 1)
    if year and month:
        month = calendar.get_my_month(datetime.date(int(year),int(month),1), user=user_obj)
    else:
        month = calendar.get_my_month(user=user_obj)
    filter = {"creator": user_obj.id}
    # eventtags = Tag.objects.cloud_for_model(Event, filters=filter)
    start = datetime.datetime.now()
    end = start + datetime.timedelta(days=7)
    periods = [Week]
    period_objects = dict([(period.__name__.lower(), period(calendar.events.filter(creator=user_obj), start)) for period in periods])
    account = Account.objects.get(user=user_obj)
    try:
        timezone = account.timezone
    except:
        timezone = "America/Los_Angeles"
    ccall_pin = ConferencePin.objects.get(id=user_obj.id)
    user_id = user_obj.id
    files = UserFile.active_objects.filter(user=user_id).order_by('-timestamp')[:6]
    try:
        user_theme = UserTheme.objects.get(user=user_obj)
    except:
        user_theme = None
    other_friends = Friendship.objects.friends_for_user(user_obj)
    home_page_url = request.build_absolute_uri(reverse("user_dashboard", args=[user_obj.username]))
    affiliate_page_url = request.build_absolute_uri(reverse("affiliate_signup", kwargs={"username": user_obj.username}))
    payload = {
        "calendar": calendar,
        "general_conference": general_conference,
        "month": month,
        "day_names": weekday_abbrs,
        "user_obj": user_obj,
        # "eventtags": eventtags,
        "periods": period_objects,
        "is_public": False,
        "timezone": timezone,
        "ccall_pin": ccall_pin,
        "files": files,
        "user_theme": user_theme,
        "other_friends": other_friends,
        "thumb_url": settings.THUMBNAIL_URL,
        "home_page_url": home_page_url,
        "nav_current":'dashboard_nav',
        'allow_join':allow_join,
        "show_upgrade": show_upgrade,
        "affiliate_page_url":affiliate_page_url,
        "run_joan": run_joan,
        "is_me": True,
        "instant":request.user.get_instant_conference(),
    }
    extra_context = get_context(request.user)
    payload.update(extra_context)
    return render_to_response(template, payload, context_instance=RequestContext(request))

#@login_required
#def event(request, template='schedule/schedule_info.html', urlhash=None, gcal_list=None):
#    event = get_object_from_hash(Event, urlhash)
#    current_site = Site.objects.get_current()
#    next_url_prefix = "http://" + current_site.domain
#    user = request.user
#    
#    back_url = request.META.get('HTTP_REFERER', '/')
#    current_domain = Site.objects.get_current().domain
#    try:
#        account = Account.objects.get(user=user)
#        logged_in = True
#    except:
#        account = Account.objects.get(user=event.creator)
#        logged_in = False
#    apps_hosted = ""
#    if user.is_authenticated():
#        apps_domain = user.account.all()[0].apps_domain
#        use_apps_calendar = user.account.all()[0].use_apps_calendar
#        if apps_domain and use_apps_calendar:
#            apps_hosted = "hosted/%s/" %apps_domain
#    timezone = event.timezone
#    try:
#        cal = event.calendar_set.get()
#    except:
#        cal = None
#    user_owner = event.creator
#    try:
#        user_theme = UserTheme.objects.get(user=user_owner)
#    except:
#        user_theme = None
#
#    allow_join = True
#    if request.user == event.creator:
#        from restrictions.used_restrictions import ConferenceMinutesRestriction
#        cmr = ConferenceMinutesRestriction(request.user)
#        allow_join = cmr.allow()
#    start = event.start
#    end = event.end
#    try:
#        start_utc = adjust_datetime_to_timezone(start, settings.TIME_ZONE, "UTC").strftime("%Y%m%dT%H%M%SZ")
#        end_utc = adjust_datetime_to_timezone(end,settings.TIME_ZONE, "UTC").strftime("%Y%m%dT%H%M%SZ")
#    except UnknownTimeZoneError:
#        start_utc = adjust_datetime_to_timezone(start, "UTC", "UTC").strftime("%Y%m%dT%H%M%SZ")
#        end_utc = adjust_datetime_to_timezone(end, "UTC", "UTC").strftime("%Y%m%dT%H%M%SZ")
#    utc_str = start_utc + "/" + end_utc
#    if 'approved' in request.GET:
#        authsub_url = None
#        calendar_service = gdata.calendar.service.CalendarService()
#        # calendar_service.auth_token = request.session['authsub_token']
#        authsub_token = request.session['authsub_token']
#        calendar_service.SetAuthSubToken(authsub_token)
#        feed = calendar_service.GetCalendarListFeed()
#        gcal_list = {}
#        for i, a_calendar in enumerate(feed.entry):
#            gcal_list[i] = a_calendar.title.text
#    else:
#        current_site = Site.objects.get_current()
#        next = reverse('s_event', args=[urlhash])
#        next_url = next_url_prefix + next
#        authsub_url = GetAuthSubUrl(post_next=next_url)
#    start = adjust_datetime_to_timezone(event.start, event.timezone, timezone)
#    tz = event.timezone
#    now = datetime.datetime.now(pytz.timezone(tz))
#    tz = (tz, _("GMT%(tz_offset)s : %(tz)s" % {"tz_offset": now.strftime("%z"), "tz": tz}))
#    tz_display = {'value':tz[0], 'title':tz[1].strip()}
#    
#    return render_to_response(template, {
#        "event": event,
#        "back_url" : back_url,
#        "calendar" : cal,
#        "user": user,
#        "site_url": 'http://%s' % current_domain,
#        "timezone": timezone,
#        "logged_in": logged_in,
#        "user_theme": user_theme,
#        "urlhash": urlhash,
#        "authsub_url": authsub_url,
#        "gcal_list": gcal_list,
#        "utc_str": utc_str,
#        'allow_join':allow_join,
#        'start': start,
#        'apps_hosted': apps_hosted,
#        'tz_display':tz_display
#    }, context_instance=RequestContext(request))


def convert_name_weekday_to_num(name):
    name = name.strip()
    if name == "Sunday":
        return 6
    elif name == "Monday":
        return 0
    elif name == "Tuesday":
        return 1
    elif name == "Wednesday":
        return 2
    elif name == "Thursday":
        return 3
    elif name == "Friday":
        return 4
    else: return 5

def event(request, template='schedule/schedule_info.html', urlhash=None, gcal_list=None):
    event = get_object_from_hash(Event, urlhash)
    if event.is_valid:
        current_site = Site.objects.get_current()
        next_url_prefix = "http://" + current_site.domain
        user = request.user
        user_host = event.creator
        is_host = False
        if user == user_host:
            is_host = True
        back_url = request.META.get('HTTP_REFERER', '/')
        current_domain = Site.objects.get_current().domain
        try:
            account = Account.objects.get(user=user)
            logged_in = True
        except:
            account = Account.objects.get(user=event.creator)
            logged_in = False
        apps_hosted = ""
        if user.is_authenticated():
            apps_domain = user.account.all()[0].apps_domain
            use_apps_calendar = user.account.all()[0].use_apps_calendar
            if apps_domain and use_apps_calendar:
                apps_hosted = "hosted/%s/" %apps_domain
        timezone = event.timezone
        try:
            cal = event.calendar_set.get()
        except:
            cal = None
        user_owner = event.creator
        try:
            user_theme = UserTheme.objects.get(user=user_owner)
        except:
            user_theme = None
    
        allow_join = True
        if request.user == event.creator:
            from restrictions.used_restrictions import ConferenceMinutesRestriction
            cmr = ConferenceMinutesRestriction(request.user)
            allow_join = cmr.allow()
        start = event.start
        end = event.end
        try:
            start_utc = adjust_datetime_to_timezone(start, settings.TIME_ZONE, "UTC").strftime("%Y%m%dT%H%M%SZ")
            end_utc = adjust_datetime_to_timezone(end,settings.TIME_ZONE, "UTC").strftime("%Y%m%dT%H%M%SZ")
        except UnknownTimeZoneError:
            start_utc = adjust_datetime_to_timezone(start, "UTC", "UTC").strftime("%Y%m%dT%H%M%SZ")
            end_utc = adjust_datetime_to_timezone(end, "UTC", "UTC").strftime("%Y%m%dT%H%M%SZ")
        utc_str = start_utc + "/" + end_utc
        if 'approved' in request.GET:
            authsub_url = None
            calendar_service = gdata.calendar.service.CalendarService()
            # calendar_service.auth_token = request.session['authsub_token']
            authsub_token = request.session['authsub_token']
            calendar_service.SetAuthSubToken(authsub_token)
            feed = calendar_service.GetCalendarListFeed()
            gcal_list = {}
            for i, a_calendar in enumerate(feed.entry):
                gcal_list[i] = a_calendar.title.text
        else:
            current_site = Site.objects.get_current()
            next = reverse('s_event', args=[urlhash])
            next_url = next_url_prefix + next
            authsub_url = GetAuthSubUrl(post_next=next_url)
            
        start = adjust_datetime_to_timezone(event.start, event.timezone, timezone)
        
        tz = event.timezone
        now = datetime.datetime.now(pytz.timezone(tz))
        tz = (tz, _("%(tz)s: (UTC%(tz_offset)s)" % {"tz_offset": now.strftime("%z"), "tz": tz}))
        tz_display = {'value':tz[0], 'title':tz[1].strip()}
        
        invites = {}
        
        total_invited = 0
        emails = []
        
        attendees = Attendees.objects.filter(event=event)
        for a in attendees:
            total_invited = total_invited + 1
            temp = {}
            temp['email'] = a.email
            contact = None
            try:
                contact = Contact.objects.get(user = request.user, email = a.email)
            except:
                pass
            if contact:
                temp['name'] = contact.get_contact_name()
            else:
                temp['name'] = None
            emails.append(temp)
    
        
        attendees_yes = Attendees.objects.filter(event=event, status='yes')
        yes = attendees_yes.count()
        
        attendees_no = Attendees.objects.filter(event=event, status='no')
        no = attendees_no.count()
        
        attendees_maybe = Attendees.objects.filter(event=event, status='maybe')
        maybe = attendees_maybe.count()
        
        attendees_rsvp = Attendees.objects.filter(event=event, status='not_defined')
        rsvp = attendees_rsvp.count()
        
        invites['total'] = total_invited
        invites['yes'] = yes
        invites['no'] = no
        invites['maybe'] = maybe
        invites['rsvp'] = rsvp
        
        is_conference_end = False
        
        now = datetime.datetime.now()
        
        end = event.end
        
        if now > end:
            is_conference_end = True
        
        tz = event.timezone
        now = datetime.datetime.now(pytz.timezone(tz))
        tz = (tz, _("%(tz)s: (UTC%(tz_offset)s)" % {"tz_offset": now.strftime("%z"), "tz": tz}))
        event.timezone = {'value':tz[0], 'title':tz[1].strip()}
        
        extra_timezones = TimezoneConference.objects.filter(event = event)
        
        for extra_tz in extra_timezones:
            tz = extra_tz.timezone
            now = datetime.datetime.now(pytz.timezone(tz))
            tz = (tz, _("%(tz)s: (UTC%(tz_offset)s)" % {"tz_offset": now.strftime("%z"), "tz": tz}))
            extra_tz.timezone = {'value':tz[0], 'title':tz[1].strip()}
        
        
        reminders = ReminderConference.objects.filter(event=event)
        
        if request.user.is_authenticated():
            root_folder = ConferenceFolder.objects.filter(user=user,parent_folder=None,is_root=True)[0]
            if not root_folder:
                root_folder = ConferenceFolder(name='Conferences',user = user, parent_folder = None, is_root = True )
                root_folder.save()
                root_folder = FileFolder.objects.filter(user=user, parent_folder = None, is_root = True)[0]
        else:
            root_folder = None
        ''' If conference end then get related conferences information '''
        
#        int[] week_days= [Date];
#        
#        int index = 1;
#        while (true)
#        {
#             for (int i = 0; i < week_days.length; i++)
#             {
#                  if (week_days[i].add(7*index) < end_recur)
#                  {
#                  } else
#              {
#               return
#              }
#              
#              index++
#         }

        status_response = None
        if 'status_response' in request.GET:
            status_response = request.GET['status_response']
        ralated_conference = []    
        weekly = []
        sort = None
        if "sort" in request.GET:
            sort = request.GET['sort']
        if event.rule:
            if event.rule.frequency == "WEEKLY":
                weekly_start = []
                end_weekday = event.start.weekday()
                week_days = event.weekly.split(',')
                for w in week_days:
                    day = convert_name_weekday_to_num(w)
                    if day == 6:
                        if end_weekday == 0:
                            delta = -1
                        elif end_weekday == 1:
                            delta = -2
                        elif end_weekday == 2:
                            delta = -3
                        elif end_weekday == 3:
                            delta = -4
                        elif end_weekday == 4:
                            delta = -5
                        elif end_weekday == 5:
                            delta = -6
                        elif end_weekday == 6:
                            delta = 0
                    else:
                        delta = day - end_weekday
                    weekly_start.append(event.start + datetime.timedelta(days= delta))
                for next in weekly_start:
                    delta = 7
                    if next >= event.start:
                        weekly.append(next)
                        
                    while next <= event.end_recurring_period:
                        next = next + datetime.timedelta(days=delta)
                        if next <= event.end_recurring_period:
                            weekly.append(next)
                ralated_conference = weekly
                if not sort or sort == "asc":
                    ralated_conference.sort()
                else: ralated_conference.reverse()
            monthly = []
            
            if event.rule.frequency == "MONTHLY":
                start_day = event.start.day
                month_val = event.monthly
                if month_val != "endmonth":
                    day_of_month = int(event.monthly)
                    next = event.start + datetime.timedelta(days = day_of_month-start_day)
                    
                    if day_of_month > start_day:
                        monthly.append(next)
                    month_start = 1
                    while next <= event.end_recurring_period:
                        next = event.start + datetime.timedelta(days = day_of_month-start_day)
                        next = next + relativedelta(months = month_start)
                        month_start = month_start + 1
                        if next <= event.end_recurring_period:
                            if next.day == day_of_month:
                                monthly.append(next)
                else:
                    
                    last_day = get_last_day_of_the_month(event.start.year, event.start.month)
                    last_date = datetime.datetime(event.start.year, event.start.month, last_day)
                    if last_date < event.start:
                        monthly.append(last_date)
                        
                    while last_date <= event.end_recurring_period:
                        last_date = last_date + relativedelta(months = 1)
                        nm = datetime.datetime(last_date.year, last_date.month, 15) + datetime.timedelta(days=31)
                        # now force us to the beginning of next month minus one day
                        eom = datetime.datetime(nm.year, nm.month, 1) - datetime.timedelta(days=1)   
                        
                        if eom <= event.end_recurring_period:
                            monthly.append(eom)
                ralated_conference = monthly
                if not sort or sort == "asc":
                    ralated_conference.sort()
                else: ralated_conference.reverse()
            daily = []     
            if event.rule.frequency == "DAILY":      
                next = event.start
                while next <= event.end_recurring_period:
                    next = next + datetime.timedelta(days = 1)
                    if next <= event.end_recurring_period:
                        daily.append(next)
                ralated_conference = daily
                if not sort or sort == "asc":
                    ralated_conference.sort()
                else: ralated_conference.reverse()
        if sort:
            if sort == "desc":
                sort = "asc"
            else: sort = "desc"
        
        
        is_terminal = False
        if is_conference_end and event.end_recurring_period and (datetime.datetime.now() > event.end_recurring_period):
            is_terminal = True
            
        
        return render_to_response(template, {
            "status_response":status_response,
            "event": event,
            "back_url" : back_url,
            "calendar" : cal,
            "user": user,
            "site_url": 'http://%s' % current_domain,
            "timezone": timezone,
            "logged_in": logged_in,
            "user_theme": user_theme,
            "urlhash": urlhash,
            "authsub_url": authsub_url,
            "gcal_list": gcal_list,
            "utc_str": utc_str,
            'allow_join':allow_join,
            'start': start,
            'apps_hosted': apps_hosted,
            'tz_display':tz_display,
            'emails': emails,
            'invites': invites,
            'bucket':unicode(Site.objects.get_current()),
            'is_host':is_host,
            'is_conference_end':is_conference_end,
            'extra_timezones':extra_timezones,
            'reminders':reminders,
            'root_folder':root_folder,
            'ralated_conference': ralated_conference,
            'sort':sort,
            'is_terminal':is_terminal,
        }, context_instance=RequestContext(request))

    else:
        return HttpResponseRedirect('/conferencing/list/',)
    

    
def get_last_day_of_the_month(y, m):
    '''
    Returns an integer representing the last day of the month, given
    a year and a month.
    '''
    
    # Algorithm: Take the first day of the next month, then count back
    # ward one day, that will be the last day of a given month. The
    # advantage of this algorithm is we don't have to determine the
    # leap year.
    
    m += 1
    if m == 13:
        m = 1
        y += 1
    
    first_of_next_month = datetime.date(y, m, 1)
    last_of_this_month = first_of_next_month + datetime.timedelta(-1)
    return last_of_this_month.day
    
@login_required
def get_guests(request, urlhash = None):
    user = request.user
    event = get_object_from_hash(Event, urlhash)
    user_host = event.creator
    is_host = False
    if user == user_host:
        is_host = True
    is_conference_end = False
    now = datetime.datetime.now()
    end = event.end
    if now > end:
        is_conference_end = True
    
    
    
    guest = {}
    guest['invited'] = {}
    guest['attended'] = {}
    guest['did_not_attend'] = {}
    
    emails_invited = []
    
    attendees = Attendees.objects.filter(event=event)
    total_invited = attendees.count()
    for a in attendees:
        temp = {}
        temp['email'] = a.email
        contact = None
        try:
            contact = Contact.objects.get(user = request.user, email = a.email)
        except:
            pass
        if contact:
            temp['name'] = contact.get_contact_name()
        else:
             temp['name'] = None
        emails_invited.append(temp)
    

        
    guest['invited']['total'] = total_invited
    guest['invited']['emails'] = emails_invited
    
#    text = ''
#    for e in guest['invited']['emails']:
#        text = text + e['email']
#    return HttpResponse(text)
    attendees_yes = Attendees.objects.filter(event=event, status='yes')
    yes = attendees_yes.count()
    
    emails_yes = []
    for a in attendees_yes:
        temp = {}
        temp['email'] = a.email
        contact = None
        try:
            contact = Contact.objects.get(user = request.user, email = a.email)
        except:
            pass
        if contact:
            temp['name'] = contact.get_contact_name()
        else:
             temp['name'] = None
        emails_yes.append(temp)
        
    guest['attended']['total'] = yes
    guest['attended']['emails'] = emails_yes
    
    attendees_no = Attendees.objects.filter(event=event, status='no')
    no = attendees_no.count()
    emails_no = []
    for a in emails_no:
        temp = {}
        temp['email'] = a.email
        contact = None
        try:
            contact = Contact.objects.get(user = request.user, email = a.email)
        except:
            pass
        if contact:
            temp['name'] = contact.get_contact_name()
        else:
             temp['name'] = None
        emails_no.append(temp)
        
    guest['did_not_attend']['total'] = no
    guest['did_not_attend']['emails'] = emails_no
    
    return direct_to_template(request, template = "schedule/guests.html",extra_context={'event':event,'is_conference_end':is_conference_end,'is_host':is_host, 'guest':guest})

@user_passes_test(test_func=test_user_function)
def create_or_edit_event(request, calendar_id=None, urlhash=None, next=None,
                          template_name="conferencing/create_conference.html", form_class=EventForm):
    """
    This function, if it receives a GET request or if given an invalid form in a
    POST request it will generate the following response

    * Template: schedule/create_event.html
    * Context:
        * form: an instance of EventForm
        * calendar: a Calendar with id=calendar_id

    If this form receives an event_id it will edit the event with that id, if it
    recieves a calendar_id and it is creating a new event it will add that event
    to the calendar with the id calendar_id

    If it is given a valid form in a POST request it will redirect with one of
    three options, in this order

    # Try to find a 'next' GET variable
    # If the key word argument redirect is set
    # Lastly redirect to the event detail of the recently create event
    """
    page_title = _("Create Conference")
    date = coerce_date_dict(request.GET)
    initial_data = {}
    ccall_pin = ConferencePin.objects.get(id=request.user.id)
    if date:
        try:
            start = datetime.datetime(**date)
            initial_data.update({
                "start": start,
                "end": start + datetime.timedelta(minutes=30)
            })
        except (TypeError, ValueError):
            raise Http404

    instance = None
    
    # set time zone info
    account = Account.objects.get(user=request.user)
    user_timezone = account.timezone
    server_timezone = 'America/Los_Angeles'
    
    if urlhash is not None:
        event_id = int(uri_b64decode(str(urlhash))) / 42
        instance = get_object_or_404(Event, id=event_id)
        end_recur = instance.end + datetime.timedelta(days=8)
        initial_data.update({
            "start": instance.start,
            "end": instance.start + datetime.timedelta(minutes=30),
            "end_recurring_period" : end_recur,
        })
        page_title = "Change Conference"
    calendar = get_object_or_404(Calendar, id=1)
    
    if instance is None:
        initial_data["timezone"] = user_timezone
    form = form_class(data=request.POST or None, instance=instance, initial=initial_data)
    
    if form.is_valid():
        event = form.save(commit=False)
        if instance is None:
            event.creator = request.user
        event.end = event.start + datetime.timedelta(minutes=30)
        event.save()
        if instance is None:
            calendar.events.add(event)
        next = next or reverse('s_event', args=[uri_b64encode(str(event.id * 42))])
        if 'next' in request.GET:
            next = _check_next_url(request.GET['next']) or next
        return HttpResponseRedirect(next)
    else:
        pass
        # logging.debug(form.errors)
    return render_to_response(template_name, {
        "form": form,
        "calendar": calendar,
        "event": instance,
        "page_title": page_title,
        "nav_current":'conf_nav',
        "ccall_pin": ccall_pin,
    }, context_instance=RequestContext(request))

@user_passes_test(test_func=test_user_function)
def create_event(request, calendar_id=None, calendar_slug=None, year=None, month=None, day=None, hour=None, minute=None, redirect=None):
    if calendar_id:
        calendar = get_object_or_404(Calendar, id=calendar_id)
    elif calendar_slug:
        calendar = get_object_or_404(Calendar, slug=calendar_slug)
    starttime = datetime.datetime(year=int(year),month=int(month),day=int(day),hour=int(hour),minute=int(minute))
    endtime = starttime + datetime.timedelta(minutes=30)    
    end_recur = endtime + datetime.timedelta(days=8)
    init_values = {
        'start' : starttime,
        'end' : endtime,
        'end_recurring_period' : end_recur
    }
    form = EventForm(data=request.POST or None, initial=init_values)
    if form.is_valid():
        event = form.save(commit=False)
        event.creator = request.user
        
        # time zone manipulation
        account = Account.objects.get(user=request.user)
        user_timezone = account.timezone
        server_timezone = os.environ['TZ']
        new_start = adjust_datetime_to_timezone(starttime, user_timezone, server_timezone)
        event.start = new_start.replace(tzinfo=None)
        event.end = event.start + datetime.timedelta(minutes=30)
        
        event.save()
        calendar.events.add(event)
        next = redirect or reverse('s_event', args=[uri_b64encode(str(event.id * 42))])
        if 'next' in request.GET:
            next = _check_next_url(request.GET['next']) or next
        return HttpResponseRedirect(next)
    return render_to_response('schedule/create_event.html', {
        "form": form,
        "calendar": calendar
    }, context_instance=RequestContext(request))

@user_passes_test(test_func=test_user_function)
def delete_event(request, urlhash=None, redirect=None, login_required=True):
    """
    After the event is deleted there are three options for redirect, tried in
    this order:

    # Try to find a 'next' GET variable
    # If the key word argument redirect is set
    # Lastly redirect to the event detail of the recently create event
    """
    event_id = int(uri_b64decode(str(urlhash))) / 42
    next = redirect or reverse('my_conferences')
    if 'next' in request.GET:
        next = _check_next_url(request.GET['next']) or next
    return delete_object(request,
                         model = Event,
                         object_id = event_id,
                         post_delete_redirect = next,
                         template_name = "schedule/delete_event.html",
                         login_required = login_required
                        )
    

def del_event(request, urlhash=None, redirect=True, login_required=True):
    event = get_object_from_hash(Event, urlhash)
    if request.user == event.creator:
        event.is_valid = False
        event.save()
        url = reverse("my_conferences")
        return HttpResponseRedirect(url)
    else:
        raise Http404
del_event = login_required(del_event)

def _check_next_url(next):
    """
    Checks to make sure the next url is not redirecting to another page.
    Basically it is a minimal security check.
    """
    if '://' in next:
        return None
    return next

def calendar_compact_month( request, calendar_id=None,calendar_slug=None, year=None, month=None ):
    return calendar( request, calendar_id,calendar_slug, year, month,
                    template='schedule/calendar_compact_month.html' )

def calendar_month( request, calendar_id=None,calendar_slug=None, year=None, month=None, user=None ):
    user = request.user
    return calendar( request, calendar_id, calendar_slug, year, month, user,
                    template='schedule/calendar_month.html' )

def calendar_tri_month( request, calendar_id=None, calendar_slug=None, year=None, month=None ):
    if calendar_id:
        cal = get_object_or_404(Calendar, id = calendar_id)
    elif calendar_slug:
        cal = get_object_or_404(Calendar, slug = calendar_slug)
    if year and month:
        month = cal.get_month(datetime.date(int(year),int(month),1))
    else:
        month = cal.get_month()
    return render_to_response('schedule/calendar_tri_month.html', {
                "calendar": cal,
                "month": month,
    }, context_instance=RequestContext(request))

def calendar_year( request, calendar_id=None,calendar_slug=None, year=None ):
    if calendar_id:
        cal = get_object_or_404(Calendar, id = calendar_id)
    elif calendar_slug:
        cal = get_object_or_404(Calendar, slug = calendar_slug)
    if year:
        year = int(year)
    else:
        year = datetime.datetime.today().year
    months = ["",]
    months.extend( [datetime.date(year=year,month=mn,day=1) for mn in range(1,13)] )
    return render_to_response('schedule/calendar_year.html', {
                "calendar": cal,
                "months": months,
                "prev_year": year - 1,
                "next_year": year + 1,
    }, context_instance=RequestContext(request))

def calendar_week( request, calendar_id=None,calendar_slug=None, year=None, month=None, day=None ):
    days = []
    return render_to_response('schedule/calendar_week.html', {
                        "calendar": calendar,
                        "days": days,
    }, context_instance=RequestContext(request))

def calendar_day( request, calendar_id=None,calendar_slug=None, year=None, month=None, day=None ):
    # if calendar_id:
    #     cal = get_object_or_404(Calendar, id = calendar_id)
    # if calendar_slug:
    #     cal = get_object_or_404(Calendar, slug = calendar_slug)
    cal = get_object_or_404(Calendar, id = 1)
    if year and month and day:
        dt = datetime.datetime(year=int(year),month=int(month),day=int(day))
        daynumber = int(day)
    else:
        dt = datetime.datetime.today()
        daynumber = dt.day
    user_obj = request.user
    month = cal.get_my_month(dt, user=user_obj)
    day = month.get_day( daynumber )
    return render_to_response('schedule/calendar_day.html', {
                        "calendar": cal,
                        "day": day,
    }, context_instance=RequestContext(request))
    
def calendar_by_periods2(request, periods=None,
    template_name="schedule/all_events.html"):
    calendar = get_object_or_404(Calendar, slug='netconference-main')
    start = datetime.datetime.now()
    end = start + datetime.timedelta(days=30)
    period_objects = dict([(period.__name__.lower(), period(calendar.events.all(), end)) for period in periods])
    return render_to_response(template_name,{
            'date': start,
            'periods': period_objects,
            'calendar': calendar,
            'weekday_names': weekday_names,
        },context_instance=RequestContext(request),)

def compose_event_invite(request, urlhash=None, recipient=None, form_class=EventInviteForm,
        template_name='schedule/event_invite.html', success_url=None, recipient_filter=None, to_emails=None):
    """
    Displays and handles the ``form_class`` form to compose new messages.
    Required Arguments: None
    Optional Arguments:
        ``recipient``: username of a `django.contrib.auth` User, who should
                       receive the message, optionally multiple usernames
                       could be separated by a ','
        ``form_class``: the form-class to use
        ``template_name``: the template to use
        ``success_url``: where to redirect after successful submission
    """
    event_id = int(uri_b64decode(str(urlhash))) / 42
    event = get_object_or_404(Event, id=event_id)
    account = Account.objects.get(user=request.user)
    user_timezone = account.timezone
    server_timezone = 'America/Los_Angeles'
    
    old_start = event.start
    new_start = adjust_datetime_to_timezone(old_start, server_timezone, user_timezone)
    event.start = new_start.replace(tzinfo=None)
    event.end = event.start + datetime.timedelta(minutes=30)
    
    if request.method == "POST" and "groups" in request.POST:
        sender = request.user
        groups = request.POST.getlist('groups')
        recipients = ""
        str_list = []
        for g in groups:
            group = ContactGroup.objects.get(id=g)
            for obj in group.contact_groups.all():
                if obj.user == request.user:
                    str_list.append(obj.email)
                else:
                    pass
        recipients = ', '.join(str_list)
        form = form_class(request.POST, recipient_filter=recipient_filter)
        if form.is_valid():
            form.save(sender=sender, event=event, recipient=recipients)
            groups_string = request.POST['groups']
            request.user.message_set.create(message="Invitation sent to: %s" % recipients)
            # if success_url is None:
            #     success_url = reverse("s_event", args=[event.id]),
            # @@@ need to make this more re-usable by making the reverse URL (above) work - I got stuck on it.
            success_url = "http://netconference.com/schedule/event/%s/" % urlhash
            if request.GET.has_key('next'):
                success_url = request.GET['next']
            return HttpResponseRedirect(success_url)
        else:
            return form.errors
    
    if request.method == "POST" and "recipient" in request.POST:
        sender = request.user
        form = form_class(request.POST, recipient_filter=recipient_filter)
        if form.is_valid():
            # instance = request.POST['instance']
            recipient_string = request.POST['recipient']
            if recipient_string[-1:] == ',':
                recipient_string = recipient_string[0:-1]
            else:
                recipient_string = recipient_string
            form.save(sender=sender, event=event, recipient=recipient_string)
            request.user.message_set.create(message="Invitation sent to %s" % recipient_string)
            # if success_url is None:
            #     success_url = reverse("s_event", args=[event.id]),
            # @@@ need to make this more re-usable by making the reverse URL (above) work - I got stuck on it.
            success_url = "http://netconference.com/schedule/event/%s/" % urlhash
            if request.GET.has_key('next'):
                success_url = request.GET['next']
            return HttpResponseRedirect(success_url)
        else:
            return form.errors
    else:
        form = form_class()
        event_invite_form = EventInviteForm({
            'recipient': ' ',
            'subject': 'NetConference invitation: %s' % event.title,
            'body': "Please join me in my state-of-the-art digital NetConference room.\n\nWHEN: %s.\n\nMY TIME ZONE: %s" % (event.start.strftime('%A, %B %d at %I:%M %p'), user_timezone),
        })
        recipient_string = None
        if ('recipient' in request.GET) and request.GET['recipient'].strip():
            recipient_string = request.GET['recipient']
        if recipient is not None:
            recipients = [u for u in User.objects.filter(username__in=[r.strip() for r in recipient.split(',')])]
            form.fields['recipient'].initial = recipients
    return render_to_response('schedule/event_invite.html', {
        'form': event_invite_form,
        'recipient': recipient_string,
        'event': event,
    }, context_instance=RequestContext(request))
    
def invite_email_ajax(request):
    reporting_recepients_emails = ''
    reporting_recepients = request.session.get("reporting_users", None)
    if reporting_recepients:
        reporting_recepients_emails  = ", ".join([user.email for user in reporting_recepients])
        request.session.pop("reporting_users", None)
        
            
    return direct_to_template(request, template = "schedule/invite_email_ajax.html",
                               extra_context={"reporting_recepients_emails": reporting_recepients_emails})     


def invite_contacts_ajax(request):
    user = request.user
    qs = Contact.objects.filter(user=user)
    if request.is_ajax:
        response = object_list(request, template_name='schedule/invite_contacts_ajax.html', queryset=qs, extra_context={'user': user})
        setattr(response, "djangologging.suppress_output", True)
        return response
    else:
        raise Http404
invite_contacts_ajax = login_required(invite_contacts_ajax)

def invite_groups_ajax(request):
    user = request.user
    qs = ContactGroup.objects.filter(owner=user)
    if request.is_ajax:
        response = object_list(request, template_name='schedule/invite_groups_ajax.html', queryset=qs, extra_context={'user': user})
        setattr(response, "djangologging.suppress_output", True)
        return response
    else:
        raise Http404
invite_groups_ajax = login_required(invite_groups_ajax)


    

# def event_invite(request, event_id=None):
#     user = request.user
#     event = get_object_or_404(Event, id=event_id)
#     if user != event.creator:
#         raise Http404
#     current_domain = Site.objects.get_current().domain
#     account = Account.objects.get(user=user)
#     timezone = account.timezone
#     return render_to_response('schedule/event_invite.html', {
#         "event": event,
#         "user": user,
#         "site_url": 'http://%s' % current_domain,
#         "timezone": timezone,
#     }, context_instance=RequestContext(request))

@require_POST
def rsvp_for_event(request, urlhash=None, template="vmail/view_invite.html"):
    form = RsvpForm(request.POST)
    current_domain = Site.objects.get_current().domain
    site_url = "http://www.%s" % current_domain
    if form.is_valid():
        form.save()
        event = form.cleaned_data["event"]
        message = _("You've been successfully RSVPed to this event.")
        form = RsvpForm()
        return render_to_response(template, {"form": form, "message": message,
                                             "event": event,
                                             "urlhash": urlhash,
                                             "site_url": site_url,}, 
                                  context_instance=RequestContext(request))
    try:
        event = Event.objects.get(id=request.POST.get("event"))
    except (Event.DoesNotExist, ValueError):
        event = None
    return render_to_response(template, {"form": form, "event": event,
                                         "urlhash": urlhash,
                                         "site_url": site_url,}, 
                              context_instance=RequestContext(request))

def conf_stats(request, urlhash=None, template="schedule/conference_statistics.html"):
    event = Event.objects.get(id=int(uri_b64decode(str(urlhash))) / 42)
    invites = VmailAttachment.objects.filter(invite=event)
    rsvps = [ii for ii in Rsvp.objects.filter(event=event)]
    attendees = [ii for ii in Attendees.objects.filter(event=event)]
    attendee_emails = []
    for attendee in attendees:
        if attendee_emails:
            attendee_emails.append(attendee.email)
    absentees = [ii for ii in rsvps if ii.email_field not in attendee_emails]
    now_time = datetime.datetime.now()
    event_ended = now_time > event.end
    return render_to_response(template, {"event": event,
                                         "urlhash": urlhash,
                                         "invites": invites,
                                         "rsvps": rsvps,
                                         "attendees": attendees,
                                         "absentees": absentees,
                                         "event_ended": event_ended,}, 
                              context_instance=RequestContext(request))

@login_required
def watch_recording(request, urlhash = None):
    if request.method =='GET':
        event = get_object_from_hash(Event, urlhash)
    
        is_host = False
        if request.user == event.creator:
            is_host = True    
        is_conference_end = False
        if datetime.datetime.now() > event.end:
            is_conference_end = True
            
        recordings = RecordedConference.objects.filter(conference = event)
        
        return direct_to_template(request, "schedule/watch_recording.html",
                                  {'event':event,
                                   'is_conference_end':is_conference_end,
                                   'is_host':is_host,
                                   'recordings':recordings
                                   })
    else:
        '''delete recording'''
        event = get_object_from_hash(Event, urlhash)
        deletes = [int(x) for x in request.POST.getlist('chk_files')]
        recordings = RecordedConference.objects.filter(conference = event, id__in=deletes)
        
        for record in recordings:
            file = None
            try:
                file = record.userfile
            except:
                pass
            if file:
                file.delete()
            record.delete()
        
        next = reverse('watch_recording', args=[urlhash])       
        return HttpResponseRedirect(next)
    

