from restrictions.base_restriction import Restriction
from django.db.models import Sum
from restrictions.models import MessageSendLogger, VmailSendLogger, NetSignSendLogger, ConferenceMinutesLogger

class ConferenceMinutesRestriction(Restriction):
    
    verbose = 'Conference minutes used'
    var_name = 'conference_min'
    logger = ConferenceMinutesLogger
    max_field = 'conference_minutes_limit'
    
from files.models import UserFile
class FileUploadRestriction(Restriction):

    var_name = 'files_uploaded'
    max_field = 'file_size_mb_limit'
    verbose = 'Total files uploaded, in MB'
    
    def used(self):
        files_uploaded_this_period = self.user.userfile_set.filter(timestamp__gte=self.period_start)
        total_file_size = files_uploaded_this_period.aggregate(tfs = Sum('file_size'))['tfs'] or 0
        total_file_size = int(total_file_size /1024.0/1024) #Convert from bytes to MB
        return total_file_size
    
    def used_total(self):
        return int(UserFile.objects.aggregate(total=Sum('file_size'))['total']/(1024*1024.0))
    
    def used_all_time(self):
        return int((UserFile.objects.filter(user=self.user).aggregate(total=Sum('file_size'))['total'] or 0)/(1024*1024.0))

from netsign.models import NetSignDocument

class MessageSendRestriction(Restriction):

    var_name = 'messages_sent'
    logger = MessageSendLogger
    max_field = 'message_send_limit'
    verbose = 'Messages sent'
    
class VmailSendRestriction(Restriction):
    
    var_name = 'vmails_sent'
    logger = VmailSendLogger
    max_field = 'vmail_send_limit'
    verbose = 'Vmails sent'
    
class NetSignsendRestriction(Restriction):

    var_name = 'netsigns_sent'
    logger = NetSignSendLogger
    max_field = 'netsign_send_limit'
    verbose = 'Netsigns Sent'


def get_all_restrictions():
    global_dict = globals().copy()
    return [el for el in global_dict.values() if getattr(el,'__base__',None)==Restriction]
    
def get_restriction_objects_for_user(user):
    return [el(user) for el in get_all_restrictions()]
