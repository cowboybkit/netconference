"""
This file demonstrates two different styles of tests (one doctest and one
unittest). These will both pass when you run "manage.py test".

Replace these with more appropriate tests for your application.
"""

from django.test import TestCase
from files.models import UserFile
from django.contrib.auth.models import User
from vmail.models import Vmail
from pprint import pprint
from debug import ipython
from netconf_utils.tests import TestUserTestCase
from subscription.models import Subscription, UserSubscription
from restrictions.models import ConferenceMinutesLogger, MessageSendLogger
import datetime, uuid

class RestrictionsTest(TestCase):
    def setUp(self):
        self.test_user = User.objects.create_user('test','lennon@thebeatles.com',password='test')
        self.client.login(username='test',password='test')
        Subscription.objects.create(name='Free',price=0)

        #First create a userfile object
        self.user_file = UserFile(title='Test File',
                                  user=self.test_user,
                                  filename='Test Name',
                                  source_type='pdf',
                                  current_type='pdf',
                                  processed=1,
                                  is_active=True,
                                  file_size=1,
                                  private=True)
        self.user_file.save()

    def test_all_restrictions_values(self):
        from restrictions.views import get_context
        rest_values = get_context(user=self.test_user)
        tuples = [(el.max,el.used()) for el in  rest_values['meter_dict']]
        print tuples
        
    def test_file_view_restriction(self):
        response = self.client.get("/file/%s/"%self.user_file.urlhash)
        pprint(response)
        from restrictions.models import get_file_views_month
        self.assertEqual(get_file_views_month(self.user_file),1)
        
        
class ConferenceMinutestest(TestUserTestCase):
    def test_update_table(self):
        response = self.get("update_minutes")
        self.assertEquals(response.status_code, 200)
        res2 = self.get("update_minutes")
        self.assertEquals(res2.status_code, 200)
        mins = ConferenceMinutesLogger.objects.all().count()
        self.assertEquals(mins,1)
        cmin = ConferenceMinutesLogger.objects.get()
        self.assertEquals(cmin.minutes,3)
        #ipython()

def make_datetime(year, month, day):
    class MockDatetime(datetime.datetime):
        @classmethod
        def today(cls):
            return datetime.datetime(year, month, day)
    return MockDatetime

class MessageSentTestCase(TestCase):
    def setUp(self):
        self.user = User.objects.create_user("kevin", "kevin@kevin.com", password="test")
        self.client.login(username='kevin',password='test')
        sub = Subscription.objects.create(name='Free',price=0)
        UserSubscription.objects.create(user=self.user, subscription=sub, 
                                        expires=datetime.datetime(2011, 12, 31))

    def testNumMessagesSentAtStartOfThisMonth(self):
        today = datetime.datetime.today()
        day = today.day
        month = today.month
        year = today.year
        olddatetime = datetime.datetime
        datetime.datetime = make_datetime(year, month, day)
        for ii in range(25):
            Vmail.objects.create(instance=uuid.uuid4(), subject="Test %d" %ii, body="Test Message",
                                 sender=self.user, recipient='test@example.com')
        self.assertEquals(MessageSendLogger.objects.get_count_for_user_month(self.user), 25)
        datetime.datetime = olddatetime

    def testNumMessagesSentAtStartOfNextMonth(self):
        today = datetime.datetime.today() + datetime.timedelta(days=31)
        day = today.day
        month = today.month
        year = today.year
        olddatetime = datetime.datetime
        datetime.datetime = make_datetime(2011, month, day)
        self.assertEquals(MessageSendLogger.objects.get_count_for_user_month(self.user), 0)
        datetime.datetime = olddatetime
