# Create your views here.
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from restrictions.used_restrictions import get_restriction_objects_for_user
from django.contrib.auth.decorators import login_required
from restrictions.models import ConferenceMinutesLogger
from datetime import date
from django.http import HttpResponse
from debug import idebug, ipython

def get_context(user):
    meter_dict = get_restriction_objects_for_user(user)
    user_period_start = user.get_usersubscription().period_start
    payload = {'meter_dict':meter_dict,
               'period_start':user_period_start}
    
    
    rest_dict = {}
    
    for el in meter_dict:
        rest_dict[el.var_name] = el
        
    payload.update(rest_dict)
    
    return payload

@login_required
def show_context(request):
    payload = get_context(request.user)
    navs = {'subnav_current_add':"meter",
            'nav_current':'dashboard_nav'}
    payload.update(navs)
    return render_to_response('restrictions/meter.html',
                              payload,
                              RequestContext(request))

@login_required
def total_usage(request):
    rests = get_restriction_objects_for_user(request.user)
    return render_to_response('restrictions/total_meter.html',
                              {'meters':rests,
                               'nav_current':'dashboard_nav',
                               'subnav_current_add':"Total usage",},
                              RequestContext(request))
    
    

@login_required
def update_minutes(request):
    today = date.today()
    try:
        cml = ConferenceMinutesLogger.objects.get(datetime__day=today.day,datetime__month=today.month,datetime__year=today.year,user=request.user)
    except ConferenceMinutesLogger.DoesNotExist:
        cml = ConferenceMinutesLogger.objects.create(minutes=0,user=request.user)
    cml.minutes+=1
    cml.save()
    return HttpResponse('True')