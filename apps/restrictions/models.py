from django.contrib.auth.models import User
from django.db import models
from django.db.models import Count, Sum
from django.db.models.signals import post_save
from django.template.loader import render_to_string
try:
    from mailer import send_html_mail as send_mail, mail_admins
except ImportError:
    from django.core.mail import send_mail, mail_admins
from django.contrib.sites.models import Site
from django.conf import settings

from files.models import UserFile
from netsign.models import NetSignInvitation
from vmail.models import Vmail, VmailAttachment

import datetime


class LoggerManager(models.Manager):
    
    def get_total(self):
        return self.count()
    
    def get_count(self, **kwargs):
        return self.filter(**kwargs).count()
    
    def get_count_since(self, date, **kwargs):
        return self.get_count(datetime__gte=date, **kwargs)
    
    def get_count_this_month(self, **kwargs):
        """
        Main function to be used for objects specific Loggers
        """
        object_with_user_fk = kwargs.values()[0]
        period_start = object_with_user_fk.user.get_usersubscription().period_start
        return self.get_count_since(date=period_start, **kwargs)
    
    def get_count_for_user_since(self, user, date, **kwargs):
        return self.get_count_since(date, user=user)
    
    def get_count_for_user_month(self, user, **kwargs):
        """
        Main function to be used for user specific Loggers
        """
        period_start = user.get_usersubscription().period_start
        return self.get_count_for_user_since(user=user, date=period_start)
    
    def get_count_user_all_time(self,user):
        return self.get_count(user=user)
    
    
class DurationManager(LoggerManager):

    def get_total(self):
        from django.db.models import Sum
        return self.aggregate(total_time=Sum('minutes'))['total_time']
    
    def get_count(self, **kwargs):
        from django.db.models import Sum
        return self.filter(**kwargs).aggregate(total_time=Sum('minutes'))['total_time'] or 0
        
    def get_count_since(self, date, **kwargs):
        return self.get_count(datetime__gte=date, **kwargs)
    
    
class Logger(models.Model):
    datetime = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User, blank=True, null=True)

    objects = LoggerManager()

    class Meta:
        abstract = True

    
# Create your models here.

class FileDownloadLogger(Logger):
    """
    File loggers are updated within the decorator
    """
    userfile = models.ForeignKey(UserFile)
   
def get_file_downloads_month(uf):
    return FileDownloadLogger.objects.get_count_this_month(userfile=uf)
    
class FileViewLogger(Logger):
    userfile = models.ForeignKey(UserFile)
  
def get_file_views_month(uf):
    return FileViewLogger.objects.get_count_this_month(userfile=uf)
    
    
class NetSignSendLogger(Logger):
    """
    Compose page loggers are updated via signals
    """
    netsign_invite = models.ForeignKey(NetSignInvitation)
    
class VmailSendLogger(Logger):
    vmail_attachment = models.ForeignKey(VmailAttachment)

class MessageSendLogger(Logger):
    message = models.ForeignKey(Vmail)
    
class ConferenceMinutesLogger(Logger):
    minutes = models.PositiveIntegerField()
    objects = DurationManager()
    
class LastRestrictionEmailSentManager(models.Manager):
    def touch(self, user):
        mail_sent_at = datetime.datetime.now()
        try:
            last_restriction_email = self.get(user = user)
            last_restriction_email.mail_sent_at = mail_sent_at
            last_restriction_email.save()
        except self.model.DoesNotExist:
            
            last_restriction_email = self.\
                create(user = user, mail_sent_at = mail_sent_at)
        return last_restriction_email
    
    def get_for_user(self, user):
        try:
            last_restriction_email = self.get(user = user)
        except self.model.DoesNotExist:
            #Set time 31 minutes in the past.
            mail_sent_at = datetime.datetime.now() - \
                             datetime.timedelta(minutes = 31)
            last_restriction_email = self.\
                create(user = user, mail_sent_at = mail_sent_at)
        return last_restriction_email
         
    
class LastRestrictionEmailSent(models.Model):
    user = models.ForeignKey(User)
    mail_sent_at = models.DateTimeField()  
    
    objects = LastRestrictionEmailSentManager()  
    
    

def nsi_create_handler(instance, **kwargs):
    NetSignSendLogger.objects.get_or_create(netsign_invite=instance, user=instance.from_user)
    
post_save.connect(nsi_create_handler, sender=NetSignInvitation)

def vmail_create_handler(instance, **kwargs):
    if instance.vmail_recording:
        VmailSendLogger.objects.get_or_create(vmail_attachment=instance, user=instance.message.sender)
    
post_save.connect(vmail_create_handler, sender=VmailAttachment)

def roadbloack_mail_factory(logger_class, message_text, roadblock_attr):
    def roadblock_mail_function(instance, **kwargs):
        meslog_count = logger_class.objects.get_count_for_user_month(instance.user)
        user = instance.user
        usub = instance.user.get_usersubscription()
        sub = usub.subscription
        if meslog_count >= getattr(sub, roadblock_attr):
            #Send messages to User that they should be upgraded.
            site = Site.objects.get_current()
            payload = {"user": user, 
                       "message_text": message_text,
                       "site": site,
                       "MEDIA_URL": settings.MEDIA_URL, }
            message = render_to_string("restrictions/confmin.txt", payload)
            subject = "Thanks for using NetConference. You should upgrade."
            from_email = settings.DEFAULT_FROM_EMAIL
            recipient_list = [user.email]
            if logger_class == ConferenceMinutesLogger:
                last_email_sent = LastRestrictionEmailSent.objects.get_for_user(user)
                now = datetime.datetime.now()
                mins_ago_30 =  now - datetime.timedelta(minutes = 30)
                if last_email_sent.mail_sent_at < mins_ago_30:
                    LastRestrictionEmailSent.objects.touch(user)
                    send_mail(subject, "", message, from_email, recipient_list)
                    mail_admins(subject, message)
            else:
                send_mail(subject, "", message, from_email, recipient_list)
    return roadblock_mail_function

send_mails_for_vmail_restrictions = roadbloack_mail_factory(VmailSendLogger, 
                                                            "your sent vmail limit", 
                                                            "vmail_send_limit")      
post_save.connect(send_mails_for_vmail_restrictions, sender = VmailSendLogger)

send_mails_for_messages_restrictions = roadbloack_mail_factory(MessageSendLogger, 
                                                               "your sent message limit",
                                                               "message_send_limit",
                                                               )        
post_save.connect(send_mails_for_messages_restrictions, sender=MessageSendLogger)               
    
send_message_for_confmin_count_restrictions = roadbloack_mail_factory(ConferenceMinutesLogger,
                                                                      "your conferencing time limit",
                                                                      "conference_minutes_limit",
                                                                      )        
post_save.connect(send_message_for_confmin_count_restrictions, sender=ConferenceMinutesLogger)


send_message_for_netsign_send_restrictions = roadbloack_mail_factory(NetSignSendLogger,
                                                                       "your Netsign send limit",
                                                                       "netsign_send_limit")  
post_save.connect(send_message_for_netsign_send_restrictions, sender=NetSignSendLogger)

send_message_for_file_view_restrictions = roadbloack_mail_factory(FileViewLogger,
                                                                       "your file view limit",
                                                                       "file_views_limit")  
post_save.connect(send_message_for_file_view_restrictions, sender=FileViewLogger)
    



def message_create_handler(instance, **kwargs):
    MessageSendLogger.objects.get_or_create(message=instance, user=instance.sender)
    
post_save.connect(message_create_handler, sender=Vmail)
          
from profiles.models import Profile

class UsageMeterManager(models.Manager):
    def get_query_set(self):
        qs = super(UsageMeterManager,self).get_query_set()
        new_qs = qs.annotate(conf_min=Count('user__conferenceminuteslogger'),file_upld=Sum('user__userfile__file_size'))
        return new_qs                           


class UsageMeter(Profile):
    objects = UsageMeterManager()
    
    class Meta:
        proxy = True
#        ordering = ['-conf_min','-file_upld']
        
    def restrictions(self):
        from restrictions.used_restrictions import get_restriction_objects_for_user
        return get_restriction_objects_for_user(self.user)

    def restrictions_dict(self):
        rd = {}
        for el in self.restrictions():
            rd[el.var_name] = el
        return rd
        
    @property
    def rd(self):
        return self.restrictions_dict()
    
    def disp_restrictions(self):
        return [el.__unicode__() for el in self.restrictions]
        
    def conference_min(self):
        return self.rd['conference_min']
        
    def files_uploaded(self):
        return self.rd['files_uploaded']

    def netsigns_created(self):
        return self.rd['netsigns_created']
    
    def messages_sent(self):
        return self.rd['messages_sent']
    
    def vmails_sent(self):
        return self.rd['vmails_sent']
    
    def netsigns_sent(self):
        return self.rd['netsigns_sent']
    
    
    def period_start(self):
        return self.user.get_usersubscription().period_start
    
class UsageMeterAllTime(UsageMeter):
    class Meta:
        proxy = True
        
    def restrictions_dict(self):
        rd = {}
        for el in self.restrictions():
            rd[el.var_name] = el.used_all_time()
        return rd
    
