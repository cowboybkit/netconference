from restrictions.used_restrictions import ConferenceMinutesRestriction

def get_conference_flashvars(user):
    if user.is_anonymous():
        return {'max_minutes':30,
                'used_minutes':0}
    rest = ConferenceMinutesRestriction(user)
    return {'max_minutes':rest.max,
            'used_minutes':rest.used()}

def get_conference_flashvars2(request):
    if request.user.is_anonymous():
        return {'max_minutes':30,
                'used_minutes':0}
    rest = ConferenceMinutesRestriction(request.user)
    return {'max_minutes':rest.max,
            'used_minutes':rest.used()}
