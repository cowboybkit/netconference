class Restriction(object):
    
    
    def __init__(self, user):
        self.warn_limit = 3
        self.user = user
        self.usub = self.user.get_usersubscription() 
        self.sub = self.usub.subscription
        self.period_start = self.usub.period_start
        self.max = getattr(self.sub, self.max_field)
        
    def __unicode__(self):
        return unicode(self.used())
        
    def verbose_display(self):
        return "%s : %s"%(self.verbose,self.used())
        
    def used(self):
        try:
            used_value = self.logger.objects.get_count_for_user_month(self.user)
        except:
            used_value = 0
        return used_value
    
    def used_all_time(self):
        """
        Use with caution
        """
        try:
            used_value = self.logger.objects.get_count_user_all_time(self.user)
        except:
            used_value = 0
        return used_value
    
    def used_total(self):
        """
        Only to monitor the system
        """
        return self.logger.objects.get_total()
    
    def warn_msg(self):
        return "You are approaching a limit of your subscription. Please upgrade your account to increase your limits"
    
    def percent_complete(self):
        try:
            consumed = float(self.used()) / self.max
            return consumed if consumed < 1 else 1
        except:
            return ""
        
    def disp_percent_complete(self):
        pc = self.percent_complete()
        if pc == "":
            return 100
        return "%d" % (pc * 100)
    
    def allow(self):
        return self.used() < self.max
    
    def should_warn(self):
        return self.max - self.used <= self.warn_limit
    
    @property
    def redirect_to(self):
        return '/subscription/'
    
