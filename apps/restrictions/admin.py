from django.contrib import admin

from restrictions.models import FileDownloadLogger, FileViewLogger, NetSignSendLogger, VmailSendLogger, \
            MessageSendLogger, ConferenceMinutesLogger, UsageMeter, UsageMeterAllTime, LastRestrictionEmailSent
from restrictions.used_restrictions import get_all_restrictions

class ConferenceMinLoggerAdmin(admin.ModelAdmin):
    list_display = ['user', 'datetime', 'minutes']
    list_filter = ['datetime']
    search_fields = ['user__username']

admin.site.register(FileDownloadLogger)
admin.site.register(FileViewLogger)
admin.site.register(NetSignSendLogger)
admin.site.register(VmailSendLogger)
admin.site.register(MessageSendLogger)
admin.site.register(LastRestrictionEmailSent)
admin.site.register(ConferenceMinutesLogger, ConferenceMinLoggerAdmin)

meter_fields = [el.var_name for el in get_all_restrictions()]

class UsageMeterAdmin(admin.ModelAdmin):
    list_display = ['user','period_start']
    list_display += meter_fields
    search_fields = ['user__username']


admin.site.register(UsageMeter,UsageMeterAdmin)
admin.site.register(UsageMeterAllTime,UsageMeterAdmin)
