from dateutil.relativedelta import relativedelta
from django.db.models import Sum
from subscription.views import subscription_list
from django.shortcuts import redirect
from netconf_utils.encode_decode import get_object_from_hash
from files.models import UserFile
from files.models import UserFile
from debug import ipython, idebug
from restrictions.models import *
from conferencing.models import Conference
from django.utils.translation import ugettext_lazy as _


class UpgradeNeeded(Exception):
    pass


from restrictions.used_restrictions import FileUploadRestriction, ConferenceMinutesRestriction
from schedule.models import Event

def check_file_upload_limit(view):
    def new_func(request, *args, **kwargs):
        ec = kwargs.get("extra_context", None) or {}
        ec["file_upload_disallow"] = False
        restriction = FileUploadRestriction(request.user)
        if not restriction.allow():
            ec["file_upload_disallow"] = True
        kwargs["extra_context"] = ec
        return view(request, *args, **kwargs)
    return new_func


def check_conference_allow(view):
    def new_func(request, urlhash, *args, **kwargs):
        conference = get_object_from_hash(Event, urlhash)
        if conference.creator != request.user:
            return view(request, urlhash, *args, **kwargs)
        restriction = ConferenceMinutesRestriction(request.user)
        if restriction.allow():
            return view(request, urlhash, *args, **kwargs)
        else:
            message = _("I'm sorry, you have exceeded the maximum conference minutes %s used for your subscription type. Please upgrade to hold more conferences." % restriction.max)
            request.user.message_set.create(message=message)
            return redirect(restriction.redirect_to)
    return new_func
    
    
    
def check_file_view_limit(view):
    def new_view(request, urlhash, *args, **kwargs):
        uf = get_object_from_hash(UserFile, urlhash)
        views_this_month = get_file_views_month(uf=uf)
        us = request.user.get_usersubscription()
        if views_this_month < us.subscription.file_views_limit:
            if us.subscription.file_views_limit - views_this_month <= 5:
                request.user.message_set.create(message=_('This file is approaching the maximum view limit. Please upgrade your account to increase your limits.'))
            uf.fileviewlogger_set.create(user=request.user)
            return view(request, urlhash, *args, **kwargs)
        else:
            message = _("I'm sorry, you have exceeded the maximum limit of file viewings"
            "for your subscription type. Please upgrade your account if you wish to continue to view your files this month.")
            request.user.message_set.create(message=message)
            return redirect(subscription_list)
    return new_view
    
def check_file_download_limit(view):
    def new_view(request, urlhash, *args, **kwargs):
        uf = get_object_from_hash(UserFile, urlhash)
        us = request.user.get_usersubscription()
        downloads_this_month = get_file_downloads_month(uf=uf)
        if downloads_this_month < us.subscription.file_downloads_limit:
            if us.subscription.file_downloads_limit - downloads_this_month <= 5:
                request.user.message_set.create(message=_('This file is approaching the maximum download '
                                         'limit. Please upgrade your account to increase your limits.'))
            uf.filedownloadlogger_set.create(user=request.user)
            return view(request, urlhash, *args, **kwargs)
        else:
            message = _("I'm sorry, you have exceeded the maximum allocated file download "
                        "limit for your subscription type. Please upgrade your account to download it again")
            request.user.message_set.create(message=message)
            return redirect(subscription_list)
    return new_view


def compose_page_decorator(view):
    def new_view(request, *args, **kwargs):
        ec = kwargs['extra_context'] = {}
        
        user_vmail_month = VmailSendLogger.objects.get_count_for_user_month(request.user)
        user_message_month = MessageSendLogger.objects.get_count_for_user_month(request.user)
        
        user_sub = request.user.get_usersubscription()
        subscription = user_sub.subscription
        netsign_limit = subscription.netsign_send_limit
        vmail_limit = subscription.vmail_send_limit
        message_limit = subscription.message_send_limit

        user_netsign_month = request.user.netsign_from.filter(saved_at__gte=user_sub.period_start).values('document').distinct().count()

        if user_netsign_month >= netsign_limit:
            ec['netsign_disallow'] = True
            
        if user_vmail_month >= vmail_limit:
            ec['vmail_disallow'] = True
            
        if user_message_month >= message_limit:
            ec['message_disallow'] = True

        return view(request, *args, **kwargs)
    return new_view
