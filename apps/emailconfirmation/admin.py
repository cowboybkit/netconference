from django.contrib import admin
from emailconfirmation.models import EmailAddress, EmailConfirmation

class EmailAddressAdmin(admin.ModelAdmin):
    list_display = ["user", "email", "primary"]
    search_fields = ["user__username", "email"]

admin.site.register(EmailAddress, EmailAddressAdmin)
admin.site.register(EmailConfirmation)
