import datetime, decimal, urllib

from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.contrib.sites.models import Site
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse
from django.db import transaction
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render_to_response, get_object_or_404
from django.views.generic.list_detail import object_list, object_detail
from django.views.generic.simple import direct_to_template
from django.template import RequestContext

from subscription.forms import ProductAddToCartForm, ChangeSubscriptionForm
from cart import cart
from netconference.apps.checkout.models import OrderItem, Order, TransactionDetails

import logging

_formclass = getattr(settings, 'SUBSCRIPTION_PAYPAL_FORM', 'paypal.standard.forms.PayPalPaymentsForm')
_formclass_dot = _formclass.rindex('.')
_formclass_module = __import__(_formclass[:_formclass_dot], {}, {}, [''])
PayPalForm = getattr(_formclass_module, _formclass[_formclass_dot+1:])

from models import Subscription, UserSubscription

from xml.etree import ElementTree
from django.contrib.auth.models import User
from dateutil.relativedelta import relativedelta

# http://paypaldeveloper.com/pdn/board/message?board.id=basicpayments&message.id=621
if settings.PAYPAL_TEST:
    cancel_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_subscr-find&alias=%s' % urllib.quote(settings.PAYPAL_RECEIVER_EMAIL)
else:
    cancel_url = 'https://www.paypal.com/cgi-bin/webscr?cmd=_subscr-find&alias=%s' % urllib.quote(settings.PAYPAL_RECEIVER_EMAIL)

# https://cms.paypal.com/us/cgi-bin/?cmd=_render-content&content_ID=developer/e_howto_html_Appx_websitestandard_htmlvariables

def _paypal_form_args(upgrade_subscription=False, **kwargs):
    "Return PayPal form arguments derived from kwargs."
    def _url(rel):
        if not rel.startswith('/'): rel = '/'+rel
        return 'http://%s%s' % ( Site.objects.get_current().domain, rel )

    if upgrade_subscription: returl = reverse('subscription_change_done')
    else: returl = reverse('subscription_done')

    rv = settings.SUBSCRIPTION_PAYPAL_SETTINGS.copy()
    rv.update( notify_url = _url(reverse('paypal-ipn')),
               return_url = _url(returl),
               cancel_return = _url(reverse("subscription_cancel")),
               **kwargs)
    return rv

def _paypal_form(subscription, user, upgrade_subscription=False):
    if not user.is_authenticated: return None

    if subscription.recurrence_unit:
        if not subscription.trial_unit:
            trial = {}
        else:
            trial = {
                'a1': 0,
                'p1': subscription.trial_period,
                't1': subscription.trial_unit,
                }
        return PayPalForm(
            initial = _paypal_form_args(
                cmd='_xclick-subscriptions',
                item_name='%s: %s' % ( Site.objects.get_current().name,
                                       subscription.name ),
                item_number = subscription.id,
                custom = user.id,
                a3=subscription.price,
                p3=subscription.recurrence_period,
                t3=subscription.recurrence_unit,
                src=1,                  # make payments recur
                sra=1,            # reattempt payment on payment error
                upgrade_subscription=upgrade_subscription,
                modify=upgrade_subscription and 2 or 0, # subscription modification (upgrade/downgrade)
                **trial),
            button_type='subscribe'
            )
    else:
        return PayPalForm(
            initial = _paypal_form_args(
                item_name='%s: %s' % ( Site.objects.get_current().name,
                                       subscription.name ),
                item_number = subscription.id,
                custom = user.id,
                amount=subscription.price))

def subscription_list(request,message=''):
    new_subs = Subscription.objects.all().exclude(corresponding_term_plan=None).exclude(display_name='Enterprise').order_by("price")
    enterprise_sub = Subscription.objects.get(display_name='Enterprise')
    if request.user.is_authenticated():
        new_subs = new_subs.exclude(name=request.user.usersubscription_set.all()[0].subscription)
    return object_list(request, template_name='newer_pricing.html', 
                       queryset=new_subs,
                       extra_context={'enterprise':enterprise_sub})

@login_required
def subscription_personal(request):
    us = request.user.get_usersubscription()
    """orders = Order.objects.filter(user=request.user)
    return render_to_response('subscription/subscription_personal.html', {
                "usersubscription": us,
                "orders": orders,
    }, context_instance=RequestContext(request))"""
    transactions = TransactionDetails.objects.filter(user=request.user).order_by('-transaction_time')
    return render_to_response('subscription/subscription_personal.html', {
                "usersubscription": us,
                "transactions": transactions,
    }, context_instance=RequestContext(request))
    
    
    
def subscription_detail_(request, object_id):
    s = get_object_or_404(Subscription, id=object_id)
    us = request.user.get_usersubscription()
    s_us = us.subscription
    
    if us:
        if s.price < us.subscription.price:
            sub_status = "downgrade"
        elif s.price > us.subscription.price:
            sub_status = "upgrade"
        else:
            sub_status = None
    else:
        sub_status = "subscribe"

    form = ProductAddToCartForm(request)
    form.fields['product_slug'].widget.attrs['value'] = s.id
    
    if True:
        if True:
            us.subscription = s
            # Save subscription
            us.save()
            request.user.message_set.create(message="You have successfully updated your subscription.")
            url = '.'
            return HttpResponseRedirect(url)
        else:
            request.user.message_set.create(message="Uh oh! There was a problem.")
            url = '.'
            return HttpResponseRedirect(url)            
    
    elif request.method == 'POST' and 'change_subscription' not in request.POST:
        #create the bound form
        postdata = request.POST.copy()
        cart_form = ProductAddToCartForm(request, postdata)
        #check if posted data is valid
        if cart_form.is_valid():
            #add to cart and redirect to cart page
            cart.add_to_cart(request)
            # if test cookie worked, get rid of it
            if request.session.test_cookie_worked():
                request.session.delete_test_cookie()
            url = reverse('show_cart')
            return HttpResponseRedirect(url)
        else:
            pass
    else:
        #create the unbound form. Notice the request as a keyword argument
        cart_form = ProductAddToCartForm(request=request, label_suffix=':')
    # assign the hidden input the product slug
    cart_form.fields['product_slug'].widget.attrs['value'] = object_id
    # set test cookie to make sure cookies are enabled
    request.session.set_test_cookie()

    return direct_to_template(
        request, template='subscription/subscription_detail.html',
        extra_context=dict(object=s, 
                           usersubscription=us,
                           form=form, 
                           cart_form=cart_form, 
                           cancel_url=cancel_url, 
                           sub_status=sub_status))
    

@login_required
def subscription_detail(request, object_id):
    s = get_object_or_404(Subscription, id=object_id)
    us = request.user.get_usersubscription()
    s_us = us.subscription
    
    if us:
        if s.price < us.subscription.price:
            sub_status = "downgrade"
        elif s.price > us.subscription.price:
            sub_status = "upgrade"
        else:
            sub_status = None
    else:
        sub_status = "subscribe"

    form = ProductAddToCartForm(request)
    form.fields['product_slug'].widget.attrs['value'] = s.id
    
    if request.method == 'POST' and 'change_subscription' in request.POST:
        postdata = request.POST.copy()
        change_form = ChangeSubscriptionForm(request, postdata)
        if change_form.is_valid():
            us.subscription = s
            # Save subscription
            us.save()
            request.user.message_set.create(message="You have successfully updated your subscription.")
            url = '.'
            return HttpResponseRedirect(url)
        else:
            request.user.message_set.create(message="Uh oh! There was a problem.")
            url = '.'
            return HttpResponseRedirect(url)            
    
    elif request.method == 'POST' and 'change_subscription' not in request.POST:
        #create the bound form
        postdata = request.POST.copy()
        cart_form = ProductAddToCartForm(request, postdata)
        #check if posted data is valid
        if cart_form.is_valid():
            #add to cart and redirect to cart page
            cart.add_to_cart(request)
            # if test cookie worked, get rid of it
            if request.session.test_cookie_worked():
                request.session.delete_test_cookie()
            url = reverse('show_cart')
            return HttpResponseRedirect(url)
        else:
            pass
    else:
        #create the unbound form. Notice the request as a keyword argument
        cart_form = ProductAddToCartForm(request=request, label_suffix=':')
    # assign the hidden input the product slug
    cart_form.fields['product_slug'].widget.attrs['value'] = object_id
    # set test cookie to make sure cookies are enabled
    request.session.set_test_cookie()

    return direct_to_template(
        request, template='subscription/subscription_detail.html',
        extra_context=dict(object=s, 
                           usersubscription=us,
                           form=form, 
                           cart_form=cart_form, 
                           cancel_url=cancel_url, 
                           sub_status=sub_status))

    


@login_required
def order_details(request, object_id, template_name="subscription/order_details.html"):
   """ displays the details of a past customer order; order details can only be loaded by the same 
   user to whom the order instance belongs.

   """
   order = get_object_or_404(Order, id=object_id, user=request.user)
   page_title = 'Order Details for Order #' + object_id
   order_items = OrderItem.objects.filter(order=order)
   return render_to_response(template_name, locals(), context_instance=RequestContext(request))

from .utils import output_response
def recurly_notification(request):
    #import ipdb
    #ipdb.set_trace()
    post_data = request.raw_post_data
    response_element = ElementTree.fromstring(post_data)
    if response_element.tag == "expired_subscription_notification":
        #print "This was an expired subscription notification"
        account = response_element.getchildren()[0]
        account_code = account.getchildren()[0].text
        user = User.objects.get(pk=account_code)
        user.is_active = False
        user.save()
    elif response_element.tag == "renewed_subscription_notification":
        #print "We need to renew the subscription"
        account = response_element.getchildren()[0]
        account_code = account.getchildren()[0].text
        subscription = response_element.getchildren()[1]
        subscription_uuid = subscription.getchildren()[1].text
        plan = subscription.getchildren()[0]
        plan_code = plan.getchildren()[0].text
        user = User.objects.get(pk=account_code)
        user_subscription = UserSubscription.active_objects.filter(user=user)[0]
        user_subscription.recurly_subscription_uuid = subscription_uuid
        user_subscription.save()
        user_subscription = UserSubscription.active_objects.filter(user=user)[0]
        subs = user_subscription.subscription
        """In case of downgrade of monthly plan, we sent a request to recurly
           to downgrade but did not downgrade on our end. So, we need to check
           that and change the subscription on our end, if its already done 
           on recurly"""
        if Subscription.objects.get(name=plan_code).price < subs.price:
            new_subscription = Subscription.objects.get(name=plan_code)
            rec_unit = new_subscription.recurrence_unit
            rec_period = new_subscription.recurrence_period
            today = datetime.datetime.today()
            if rec_unit=='D':
                next_month = today+relativedelta(days=rec_period)
            elif rec_unit=='M':
                next_month = today+relativedelta(months=rec_period)
            recurly_subscription_uuid = user_subscription.recurly_subscription_uuid
            user_subscription.delete()
            user.usersubscription_set.create(user=user, subscription=new_subscription, expires = next_month, active=True, cancelled=False)
            user_subscription = UserSubscription.active_objects.filter(user=user)[0]
            user_subscription.recurly_subscription_uuid = recurly_subscription_uuid
            user_subscription.save()
        else:
            rec_unit = subs.recurrence_unit
            rec_period = subs.recurrence_period
            today = datetime.datetime.today()
            if rec_unit=='D':
                next_month = today+relativedelta(days=rec_period)
            elif rec_unit=='M':
                next_month = today+relativedelta(months=rec_period)
            elif rec_unit=='Y':
                next_month = today+relativedelta(years=rec_period)
            #next_month = today + relativedelta(months=1)
            user_subscription.expires = next_month
            user_subscription.recurly_subscription_uuid = subscription_uuid
            user_subscription.save()
    elif response_element.tag == "new_subscription_notification":
        #import ipdb
        #ipdb.set_trace()
        account = response_element.getchildren()[0]
        account_code = account.getchildren()[0].text
        subscription = response_element.getchildren()[1]
        subscription_uuid = subscription.getchildren()[1].text
        try:
            user = User.objects.get(pk=account_code)
            user_subscription = UserSubscription.active_objects.filter(user=user)[0]
            user_subscription.recurly_subscription_uuid = subscription_uuid
            user_subscription.save()
        except User.DoesNotExist:
            pass
    elif response_element.tag == "successful_payment_notification":
        childrens = response_element.getchildren()
        account = childrens[0]
        account_code = account.getchildren()[0].text
        try:
            user = User.objects.get(pk=account_code)
            transaction = childrens[1]
            transaction_children = transaction.getchildren()
            id = transaction_children[0].text
            amount_in_cents = transaction_children[6].text
            amount = (float(amount_in_cents))/100
            t = TransactionDetails(user=user, transaction_id=id, amount=amount,transaction_time=datetime.datetime.now())
            t.save()
            us=UserSubscription.objects.filter(user=user)[0]
            us.is_paid = True
            us.save()
        except User.DoesNotExist:
            pass
    return HttpResponse()
