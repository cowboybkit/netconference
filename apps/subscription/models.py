import datetime

from django.conf import settings
from django.db import models
from django.contrib.auth.models import User, Group
from django.utils.translation import ugettext as _, ungettext, ugettext_lazy

from paypal.standard import ipn
from paypal.standard.ipn.models import *

import signals, utils

from dateutil.relativedelta import relativedelta

NEXT_YEAR = datetime.datetime.today() + relativedelta(years=1)

class Transaction(models.Model):
    timestamp = models.DateTimeField(auto_now_add=True, editable=False)
    subscription = models.ForeignKey('subscription.Subscription',
                                     null=True, blank=True, editable=False)
    user = models.ForeignKey(User,
                             null=True, blank=True, editable=False)
    ipn = models.ForeignKey(PayPalIPN,
                            null=True, blank=True, editable=False)
    event = models.CharField(max_length=100, editable=False)
    amount = models.DecimalField(max_digits=64, decimal_places=2,
                                 null=True, blank=True, editable=False)
    comment = models.TextField(blank=True, default='')

    class Meta:
        ordering = ('-timestamp',)


_recurrence_unit_days = {
    'D' : 1.,
    'W' : 7.,
    'M' : 30.4368,                      # http://en.wikipedia.org/wiki/Month#Julian_and_Gregorian_calendars
    'Y' : 365.2425,                     # http://en.wikipedia.org/wiki/Year#Calendar_year
    }

_TIME_UNIT_CHOICES=(
    ('D', ugettext_lazy('Day')),
    ('W', ugettext_lazy('Week')),
    ('M', ugettext_lazy('Month')),
    ('Y', ugettext_lazy('Year')),
    )

class SubscriptionManager(models.Manager):
    
    def available(self):
        return self.exclude(name='Unlimited')
    
    def get_non_free(self):
        return self.available().exclude(name="Free")
        
    
    def get_free(self):
        try:
            return self.get(name='Free')
        except Subscription.DoesNotExist:
            return self.create(name='Free', price=0)

class Subscription(models.Model):
    """name can't use capital letters since there must be something which matches plan_code on recurly.
       So, we need another field which can have capital letters and can be used as display name on
       subscription list page"""
    name = models.CharField(max_length=100, unique=True, null=False)
    display_name = models.CharField(max_length=100, unique=True, null=True)
    description = models.TextField(blank=True)
    price = models.DecimalField(max_digits=64, decimal_places=2)
    corresponding_term_plan = models.ForeignKey('self', blank=True, null=True)
    trial_period = models.PositiveIntegerField(null=True, blank=True)
    trial_unit = models.CharField(max_length=1, null=True,
                                       choices = ((None, ugettext_lazy("No trial")),)
                                       + _TIME_UNIT_CHOICES)
    recurrence_period = models.PositiveIntegerField(null=True, blank=True)
    recurrence_unit = models.CharField(max_length=1, null=True,
                                       choices = ((None, ugettext_lazy("No recurrence")),)
                                       + _TIME_UNIT_CHOICES)
    group = models.OneToOneField(Group,blank=True,null=True)
    
    video_seats = models.IntegerField(blank=True, null=True)
    total_guests = models.IntegerField(blank=True, null=True)
    vmail_seconds = models.IntegerField(blank=True, null=True)

    #Limits for subscription
    conference_minutes_limit = models.IntegerField(help_text='Number of minutes of conference time for this subscription',
                                                   default=30)
    guests_limit = models.IntegerField(help_text='Max # of guests allowed',
                                       default=25)
    file_size_mb_limit = models.IntegerField(help_text='Max total file sizes, in MB',
                                             default=200)
    file_downloads_limit = models.IntegerField(help_text='Maximum # of download of a given file',
                                               default=5)
    file_views_limit = models.IntegerField(help_text='Maximum # of file views of each file, in a month',
                                           default=25)
    message_send_limit = models.IntegerField(help_text='Maximum # of messages sent per month',
                                             default=25)
    vmail_length_limit = models.IntegerField(help_text='Maximum # of minutes in vmail',
                                             default=1)
    vmail_save_days_limit = models.IntegerField(help_text='Maximum # of days for which a vmail is stored',
                                                default=30)    
    vmail_send_limit = models.IntegerField(help_text='Maximum # of vmails sent, per month',
                                            default=5)
    netsign_send_limit = models.IntegerField(help_text='Maximum # of netsign sent invites, per month',
                                             default=5)
    netsign_stored_limit = models.IntegerField(help_text='Maximum # of days for which a netsign doc is stored',
                                               default=30)
    netsign_create_limit = models.IntegerField(help_text='Maximum # of netsign doc createable per month',
                                               default=5)
    netsign_total_limit = models.IntegerField(help_text='Maximum # of netsign stored',
                                              default=5)
    monthly_price = models.DecimalField(max_digits=64, decimal_places=2, blank=True, null=True)
    concurrent_documents_open = models.IntegerField(default=0)
    
    objects = SubscriptionManager()
    
    _PLURAL_UNITS = {
        'D': 'days',
        'W': 'weeks',
        'M': 'months',
        'Y': 'years',
        }

    class Meta:
        ordering = ('price','-recurrence_period')

    def __unicode__(self): return self.name

    def get_recurrence_unit_display(self):
        return [x[1] for x in _TIME_UNIT_CHOICES if x[0] == self.recurrence_unit][0]

    def price_per_day(self):
        """Return estimate subscription price per day, as a float.

        This is used to charge difference when user changes
        subscription.  Price returned is an estimate; month length
        used is 30.4368 days, year length is 365.2425 days (averages
        including leap years).  One-time payments return 0.
        """
        if self.recurrence_unit is None:
            return 0
        return float(self.price) / (
            self.recurrence_period*_recurrence_unit_days[self.recurrence_unit]
            )

    @models.permalink
    def get_absolute_url(self):
        return ( 'subscription_detail', (), dict(object_id=str(self.id)) )

    def get_pricing_display(self):
        if not self.price: return u'Free'
        elif self.recurrence_period:
            return ungettext('%(price).02f / %(unit)s',
                             '%(price).02f / %(period)d %(unit_plural)s',
                             self.recurrence_period) % {
                'price':self.price,
                'unit':self.get_recurrence_unit_display(),
                'unit_plural':_(self._PLURAL_UNITS[self.recurrence_unit],),
                'period':self.recurrence_period,
                }
        else: return _('%(price).02f one-time fee') % { 'price':self.price }

    def get_trial_display(self):
        if self.trial_period:
            return ungettext('One %(unit)s',
                             '%(period)d %(unit_plural)s',
                             self.trial_period) % {
                'unit':self.get_trial_unit_display().lower(),
                'unit_plural':_(self._PLURAL_UNITS[self.trial_unit],),
                'period':self.trial_period,
                }
        else:
            return _("No trial")

# add User.get_subscription() method
def __user_get_subscription(user):
    return user.get_usersubscription().subscription
User.add_to_class('get_subscription', __user_get_subscription)



class ActiveUSManager(models.Manager):
    """Custom Manager for UserSubscription that returns only live US objects."""
    def get_query_set(self, **kwargs):
        return super(ActiveUSManager, self).get_query_set(**kwargs).filter(active=True)
    
    def get_free_users(self, **kwargs):
        return self.get_query_set(**kwargs).filter(subscription__name == "Free")

class UserSubscription(models.Model):
    user = models.ForeignKey(User)
    subscription = models.ForeignKey(Subscription)
    expires = models.DateField(null = True, default=datetime.date.today)
    active = models.BooleanField(default=True)
    cancelled = models.BooleanField(default=False)
    minutes_used = models.IntegerField(default=0)
    creation_date = models.DateField(auto_now_add=True, null=True)
    is_paid = models.BooleanField(default=False)
    
    #recurring_id = models.CharField()
    recurly_subscription_uuid = models.CharField(max_length=50, blank=True, default=None, null=True)
    
    objects = models.Manager()
    active_objects = ActiveUSManager()

    grace_timedelta = datetime.timedelta(
        getattr(settings, 'SUBSCRIPTION_GRACE_PERIOD', 2))

    class Meta:
        ordering = ['-expires']

    def user_is_group_member(self):
        "Returns True if user is member of subscription's group"
        return self.subscription.group in self.user.groups.all()
    user_is_group_member.boolean = True

    def expired(self):
        """Returns true if there is more than SUBSCRIPTION_GRACE_PERIOD
        days after expiration date."""
        return self.expires is not None and (
            datetime.datetime(self.expires.year,self.expires.month,self.expires.day) + self.grace_timedelta < datetime.datetime.today() )
    expired.boolean = True

    def valid(self):
        """Validate group membership.
        Returns True if not expired and user is in group, or expired
        and user is not in group."""
        if self.expired() or not self.active: return not self.user_is_group_member()
        else: return self.user_is_group_member()
    valid.boolean = True

    def unsubscribe(self):
        """Unsubscribe user."""
        self.user.groups.remove(self.subscription.group)
        self.user.save()

    def subscribe(self):
        """Subscribe user."""
        self.user.groups.add(self.subscription.group)
        self.user.save()

    def fix(self):
        """Fix group membership if not valid()."""
        if not self.valid():
            if self.expired() or not self.active:
                self.unsubscribe()
                Transaction(user=self.user, subscription=self.subscription, ipn=None,
                            event='subscription expired'
                            ).save()
                if self.cancelled:
                    self.delete()
                    Transaction(user=self.user, subscription=self.subscription, ipn=None,
                                event='remove subscription (expired)'
                                ).save()
            else: self.subscribe()

    def extend(self, timedelta=None):
        """Extend subscription by `timedelta' or by subscription's
        recurrence period."""
        
        #Mark all other subscriptions of this user inactive
        old_subs = self.user.usersubscription_set.all()
        for el in old_subs:
            el.active=False
            el.save()

        if timedelta is not None:
            self.expires += timedelta
        else:
            if self.subscription.recurrence_unit:
                try:
                    self.expires = utils.extend_date_by(
                        self.expires,
                        self.subscription.recurrence_period,
                        self.subscription.recurrence_unit)
                except:
                    if self.subscription.recurrence_unit == "M" and self.subscription.recurrence_period == 1:
                        self.expires += datetime.timedelta(days=30)
            else:
                self.expires = None

    def try_change(self, subscription):
        """Check whether upgrading/downgrading to `subscription' is possible.

        If subscription change is possible, returns false value; if
        change is impossible, returns a list of reasons to display.

        Checks are performed by sending
        subscription.signals.change_check with sender being
        UserSubscription object, and additional parameter
        `subscription' being new Subscription instance.  Signal
        listeners should return None if change is possible, or a
        reason to display.
        """
        if self.subscription == subscription:
            if self.active and self.cancelled: return None # allow resubscribing
            return [ _(u'This is your current subscription.') ]
        return [
            res[1]
            for res in signals.change_check.send(
                self, subscription=subscription)
            if res[1] ]

    @models.permalink
    def get_absolute_url(self):
        return ( 'subscription_usersubscription_detail',
                 (), dict(object_id=str(self.id)) )

    def __unicode__(self):
        rv = u"%s's %s" % ( self.user, self.subscription )
        if self.expired():
            rv += u' (expired)'
        return rv

    @property
    def period_start(self):
        today = datetime.datetime.today()
        expiry_date = self.expires.day
        try:
            new_date = datetime.datetime(today.year,today.month,expiry_date)
        except ValueError:
            new_date = datetime.datetime(today.year, today.month, 1) + relativedelta(months=1)
        if new_date > today:
            new_date -= relativedelta(months=1)
        return new_date
    
    def save(self,*args,**kwargs):
        if not self.pk:
            from subscription.extras import FREE_SUBSCRIPTION
            
            #Mark all other subscriptions of this user inactive
            old_subs = self.user.usersubscription_set.all()
            for el in old_subs:
                el.active=False
                el.save()
            self.active = True
                
            try:
                self.subscription
            except Subscription.DoesNotExist:
                self.subscription = FREE_SUBSCRIPTION

            if self.subscription == FREE_SUBSCRIPTION:
                self.expires = NEXT_YEAR
        super(UserSubscription,self).save(*args,**kwargs)
        
from django.db.models import Q, Max

# add User.get_user_subscription() method
def __user_get_user_subscription(user):
    try:
        us = user.usersubscription_set.get(active=True)
    except UserSubscription.MultipleObjectsReturned:
        max_price = user.usersubscription_set.filter(active=True).aggregate(Max('subscription__price'))['subscription__price__max']
        us = user.usersubscription_set.filter(active=True).filter(Q(subscription__price=max_price)).latest('expires')
    except UserSubscription.DoesNotExist:
        us = user.usersubscription_set.create()
    return us

User.add_to_class('get_usersubscription', __user_get_user_subscription)

class Referer(models.Model):
    user = models.ForeignKey(User, unique=True)
    source = models.CharField(max_length=25, null=True, blank=True)
    campaign = models.CharField(max_length=50, null=True, blank=True)
    keyword = models.CharField(max_length=50, null=True, blank=True)

    def __unicode__(self):
        return "%s : %s" %(self.user.username, self.source)

def unsubscribe_expired():
    """Unsubscribes all users whose subscription has expired.

    Loops through all UserSubscription objects with `expires' field
    earlier than datetime.date.today() and forces correct group
    membership."""
    for us in UserSubscription.objects.get(expires__lt=datetime.date.today()):
        us.fix()

#### Handle PayPal signals

def _ipn_usersubscription(payment):
    class PseudoUS(object):
        pk = None
        def __nonzero__(self): return False
        def __init__(self, user, subscription):
            self.user = user
            self.subscription = subscription

    try: s = Subscription.objects.get(id=payment.item_number)
    except Subscription.DoesNotExist: s = None

    try: u = User.objects.get(id=payment.custom)
    except User.DoesNotExist: u = None

    if u and s:
        try: us = UserSubscription.objects.get(subscription=s, user=u)
        except UserSubscription.DoesNotExist:
            us = UserSubscription(user=u, subscription=s, active=False)
            Transaction(user=u, subscription=s, ipn=payment,
                        event='new usersubscription', amount=payment.mc_gross
                        ).save()
    else: us = PseudoUS(user=u,subscription=s) 

    return us

def handle_payment_was_successful(sender, **kwargs):
    us = _ipn_usersubscription(sender)
    u, s = us.user, us.subscription
    if us:
        if not s.recurrence_unit:
            if sender.mc_gross == s.price:
                us.subscribe()
                us.expires = None
                us.active = True
                us.save()
                Transaction(user=u, subscription=s, ipn=sender,
                            event='one-time payment', amount=sender.mc_gross
                            ).save()
                signals.signed_up.send(s, ipn=sender, subscription=s, user=u,
                                       usersubscription=us)
            else:
                Transaction(user=u, subscription=s, ipn=sender,
                            event='incorrect payment', amount=sender.mc_gross
                            ).save()
                signals.event.send(s, ipn=sender, subscription=s, user=u,
                                   usersubscription=us, event='incorrect payment')
        else:
            if sender.mc_gross == s.price:
                us.extend()
                us.save()
                Transaction(user=u, subscription=s, ipn=sender,
                            event='subscription payment', amount=sender.mc_gross
                            ).save()
                signals.paid.send(s, ipn=sender, subscription=s, user=u,
                                  usersubscription=us)
            else:
                Transaction(user=u, subscription=s, ipn=sender,
                            event='incorrect payment', amount=sender.mc_gross
                            ).save()
                signals.event.send(s, ipn=sender, subscription=s, user=u,
                                   usersubscription=us, event='incorrect payment')
    else:
        Transaction(user=u, subscription=s, ipn=sender,
                    event='unexpected payment', amount=sender.mc_gross
                    ).save()
        signals.event.send(s, ipn=sender, subscription=s, user=u, event='unexpected_payment')
ipn.signals.payment_was_successful.connect(handle_payment_was_successful)

def handle_payment_was_flagged(sender, **kwargs):
    us = _ipn_usersubscription(sender)
    u, s = us.user, us.subscription
    Transaction(user=u, subscription=s, ipn=sender,
                event='payment flagged', amount=sender.mc_gross
                ).save()
    signals.event.send(s, ipn=sender, subscription=s, user=u, event='flagged')
ipn.signals.payment_was_flagged.connect(handle_payment_was_flagged)

def handle_subscription_signup(sender, **kwargs):
    us = _ipn_usersubscription(sender)
    u, s = us.user, us.subscription
    if us:
        # deactivate or delete all user's other subscriptions
        for old_us in u.usersubscription_set.all():
            if old_us==us: continue     # don't touch current subscription
            if old_us.cancelled:
                old_us.delete()
                Transaction(user=u, subscription=s, ipn=sender,
                            event='remove subscription (deactivated)', amount=sender.mc_gross
                            ).save()
            else:
                old_us.active = False
                old_us.unsubscribe()
                old_us.save()
                Transaction(user=u, subscription=s, ipn=sender,
                            event='deactivated', amount=sender.mc_gross
                            ).save()

        # activate new subscription
        us.subscribe()
        us.active = True
        us.cancelled = False
        us.save()
        Transaction(user=u, subscription=s, ipn=sender,
                    event='activated', amount=sender.mc_gross
                    ).save()

        signals.subscribed.send(s, ipn=sender, subscription=s, user=u,
                                usersubscription=us)
    else:
        Transaction(user=u, subscription=s, ipn=sender,
                    event='unexpected subscription', amount=sender.mc_gross
                    ).save()
        signals.event.send(s, ipn=sender, subscription=s, user=u,
                           event='unexpected_subscription')
ipn.signals.subscription_signup.connect(handle_subscription_signup)

def handle_subscription_cancel(sender, **kwargs):
    us = _ipn_usersubscription(sender)
    u, s = us.user, us.subscription
    if us.pk is not None:
        if not us.active:
            us.unsubscribe()
            us.delete()
            Transaction(user=u, subscription=s, ipn=sender,
                        event='remove subscription (cancelled)', amount=sender.mc_gross
                        ).save()
        else:
            us.cancelled = True
            us.save()
            Transaction(user=u, subscription=s, ipn=sender,
                        event='cancel subscription', amount=sender.mc_gross
                        ).save()
        signals.unsubscribed.send(s, ipn=sender, subscription=s, user=u,
                                  usersubscription=us,
#                                  refund=refund, reason='cancel')
                                  reason='cancel')
    else:
        Transaction(user=u, subscription=s, ipn=sender,
                    event='unexpected cancel', amount=sender.mc_gross
                    ).save()
        signals.event.send(s, ipn=sender, subscription=s, user=u, event='unexpected_cancel')
ipn.signals.subscription_cancel.connect(handle_subscription_cancel)
ipn.signals.subscription_eot.connect(handle_subscription_cancel)

def handle_subscription_modify(sender, **kwargs):
    us = _ipn_usersubscription(sender)
    u, s = us.user, us.subscription
    print 'modify', u, s
    if us:
        # delete all user's other subscriptions
        for old_us in u.usersubscription_set.all():
            if old_us==us: continue     # don't touch current subscription
            old_us.delete()
            Transaction(user=u, subscription=s, ipn=sender,
                        event='remove subscription (deactivated)', amount=sender.mc_gross
                        ).save()

        # activate new subscription
        us.subscribe()
        us.active = True
        us.cancelled = False
        us.save()
        Transaction(user=u, subscription=s, ipn=sender,
                    event='activated', amount=sender.mc_gross
                    ).save()

        signals.subscribed.send(s, ipn=sender, subscription=s, user=u,
                                usersubscription=us)
    else:
        Transaction(user=u, subscription=u, ipn=sender,
                    event='unexpected subscription modify', amount=sender.mc_gross
                    ).save()
        signals.event.send(s, ipn=sender, subscription=s, user=u,
                           event='unexpected_subscription_modify')
ipn.signals.subscription_modify.connect(handle_subscription_modify)
