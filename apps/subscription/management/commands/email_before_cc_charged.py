import datetime
from django.core.management.base import NoArgsCommand
from django.conf import settings
from subscription.models import UserSubscription
from mailer import send_html_mail as send_mail
from django.template.loader import render_to_string
from django.contrib.sites.models import Site
from django.utils.translation import ugettext_lazy as _

class Command(NoArgsCommand):
    help = "Send a mail 7 days before the CC is charged."

    def handle_noargs(self, **options):
        today = datetime.datetime.today()
        seven_days = today + datetime.timedelta(days=7)
        eight_days = today + datetime.timedelta(days=8)
        user_subs = UserSubscription.objects.filter(expires__gte=seven_days, 
                                                    expires__lte=eight_days, 
                                                    cancelled=False)
        for user_sub in user_subs:
            if seven_days - user_sub.user.date_joined > datetime.timedelta(days=31):
                continue
            subject = _("Your FREE NetConference Trial is Coming to Conclusion")
            site = Site.objects.get_current()
            message_html = render_to_string("subscription/email_before_cc_charged.html",
                                            {"MEDIA_URL": settings.MEDIA_URL,
                                             "site" : site})
            from_email = "support@nxtgen.tv"
            recipient_list = [user_sub.user.email]
            send_mail(subject, "", message_html, from_email, recipient_list)
