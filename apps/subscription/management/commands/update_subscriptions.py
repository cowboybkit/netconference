import logging
import datetime
from django.core.management.base import NoArgsCommand
from django.conf import settings

class Command(NoArgsCommand):
    filename = os.path.join(settings.LOG_FOLDER, 'subscriptions.log')
    logging.basicConfig(
        level = logging.INFO,
        format = '%(asctime)s %(levelname)s %(message)s',
        filename = filename,
        filemode = 'a'
    )
    help = "Update all subscriptions, and send an email to the admin with any new transactions listed."
    def handle_noargs(self, **options):
        logging.info("Updating subscriptions for %s" % datetime.date.today())
        logging.basicConfig(level=logging.DEBUG, format="%(message)s")
        logging.info("-" * 72)
        from netconference.apps.subscription.update import check_subscriptions
        check_subscriptions()