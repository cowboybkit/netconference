class RefererTrackerMiddleware(object):
    def process_response(self, request, response):
        if request.GET.get("utm_source"):
            response.set_cookie("Referer", request.GET["utm_source"])
        if request.GET.get("utm_campaign"):
            response.set_cookie("Campaign", request.GET["utm_campaign"])
        if request.GET.get("utm_keyword"):
            response.set_cookie("Keyword", request.GET["utm_keyword"])
        return response
