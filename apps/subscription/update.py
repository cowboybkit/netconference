from django.db import models
from django.utils import simplejson
from django.core.mail import send_mail
import datetime
import settings
import logging

from apps.checkout.models import Order, OrderItem
from apps.subscription.models import Subscription, UserSubscription
from apps.checkout import authnet
from apps.checkout.checkout import create_order
from apps.checkout.forms import CheckoutForm
from apps.billing import passkey
from apps.billing.models import Card
from apps.cart.cart import get_cart_items

filename = os.path.join(settings.LOG_FOLDER, 'subscriptions.log')

logging.basicConfig(
    level = logging.INFO,
    format = '%(asctime)s %(levelname)s %(message)s',
    filename = filename,
    filemode = 'a'
)


def trigger_reminders():
    today = datetime.date.today()
    three_days = (today + datetime.timedelta(days=3))
    for us in UserSubscription.objects.get(expires__lte=three_days):
        if us.expires < (today + datetime.timedelta(days=2)):
            us.send_reminder()
        else:
            pass
        

def unsubscribe_expired():
    """Unsubscribes all users whose subscription has expired.

    Loops through all UserSubscription objects with `expires' field
    earlier than datetime.date.today() and forces correct group
    membership."""
    for us in UserSubscription.objects.get(expires__lt=datetime.date.today()):
        us.fix()


def _re_bill(user_subscription):
    """ Uses stored credit card information and the existing subscription level to assess charge; pings the payment gateway with the billing information and returns a Python dictionary with two entries: 'order_number' and 'message' based on the success of the payment processing. An unsuccessful billing will have an order_number of 0 and an error message, and a successful billing with have an order number and an empty string message.

    """
    
    logging.basicConfig(
        level = logging.INFO,
        format = '%(asctime)s %(levelname)s %(message)s',
        filename = filename,
        filemode = 'a'
    )
    
    # Transaction results
    APPROVED = '1'
    DECLINED = '2'
    ERROR = '3'
    HELD_FOR_REVIEW = '4'
    
    
    subscription = user_subscription.subscription
    logging.info(subscription)
    billee = user_subscription.user
    logging.info(billee)
    try:
        card = Card.objects.get(user=billee)
    except:
        results = {'order_number': 0, 'message': u'No card on file.'}
        return results
    encrypted_json = card.data
    # retrieve the encrypted JSON
    decrypted_json = passkey.decrypt(encrypted_json)
    # convert the decrypted JSON into a dictionary
    
    decrypted_data = simplejson.loads(decrypted_json)
    card_num = decrypted_data['card_number']
    exp_month = decrypted_data['card_expire_month']
    exp_year = decrypted_data['card_expire_year']
    exp_date = exp_month + exp_year
    card_cvv = decrypted_data['card_cvv']
    card_type = decrypted_data['card_type']
    # amount = decrypted_data.cart_subtotal(request)
    amount = user_subscription.subscription.price
    address = billee.get_profile().billing_address_1
    city = billee.get_profile().billing_city
    state = billee.get_profile().billing_state
    zip = billee.get_profile().billing_zip

    results = {}

    response = authnet.do_auth_capture(amount=amount, 
                                       card_num=card_num, 
                                       exp_date=exp_date,
                                       card_cvv=card_cvv,
                                       address=address,
                                       city=city,
                                       state=state,
                                       zip=zip)
    logging.info(response)
    
    if response[0] == APPROVED:
        transaction_id = response[6]
        order = create_order(request, transaction_id)
        results = {'order_number': order.id, 'message': u''}
        if billee.first_name and billee.last_name:
            shipping_name = "%s %s" % (billee.first_name, billee.last_name)
        else:
            shipping_name = "No Name Given"
        logging.info("Shipping Name: %s" % shipping_name)
        form_data = {
            'email': str(billee.email),
            'shipping_name': str(shipping_name),
            'phone': billee.get_profile().phone,
            # 'shipping_address_1': billee.get_profile().shipping_address_1,
            # 'shipping_address_2': billee.get_profile().shipping_address_2,
            # 'shipping_city': billee.get_profile().shipping_city,
            # 'shipping_state': billee.get_profile().shipping_state,
            # 'shipping_country': billee.get_profile().shipping_country,
            # 'shipping_zip': billee.get_profile().shipping_zip,
            'billing_name': billee.get_profile().billing_name,
            'billing_address_1': billee.get_profile().billing_address_1,
            'billing_address_2': billee.get_profile().billing_address_2,
            'billing_city': billee.get_profile().billing_city,
            'billing_state': billee.get_profile().billing_state,
            'billing_country': billee.get_profile().billing_country,
            'billing_zip': billee.get_profile().billing_zip,
            'credit_card_cvv': card_cvv,
            'credit_card_expire_month': exp_month,
            'credit_card_expire_year': exp_year,
            'credit_card_number': card_num,
            'credit_card_type': card_type,
        }

        order = Order()
        form = CheckoutForm(form_data, instance=order)
        order = form.save(commit=False)
        order.transaction_id = str(response[6])
        order.ip_address = '777.7.7.7'
        order.user = billee
        order.status = Order.SUBMITTED
        order.save()
        logging.info("Saving the order...")

        if order.pk:
           """ if the order save succeeded """
           oi = OrderItem()
           oi.order = order
           oi.quantity = 1
           oi.price = amount
           oi.product = subscription
           oi.save()
           logging.info("Order %s for %s (%s) was successful." % (oi.order, user_subscription.subscription.name, oi.price))
        
    if response[0] == DECLINED:
        results = {'order_number': 0, 'message': u'There is a problem with your credit card. Error message: %s' % response[3]}
    if response[0] == ERROR or response[0] == HELD_FOR_REVIEW:
        results = {'order_number': 0, 'message': u'Error processing your order.'}
    return results
    
    logging.info(results)
    return results

def check_subscriptions():
    """check each subscription for expired status. run _re_bill on expired subscriptions and respond with emails to users and admins reporting the transactions."""
    
    logging.basicConfig(
        level = logging.INFO,
        format = '%(asctime)s %(levelname)s %(message)s',
        filename = filename,
        filemode = 'a'
    )
    
    subscriptions = UserSubscription.active_objects.all()
    now = datetime.date.today()
    message = "NetConference Subscriptions for %s:\n\n" % now
    if subscriptions:
        for s in subscriptions:
            if (s.subscription.pk != 1) and (s.subscription.pk != 4):
                if s.expires < now and not s.cancelled:
                    message += "%s's %s has expired as of %s\n" % (s.user.username, s.subscription.name, s.expires)
                    message += "Re-billing...\n"
                    result = _re_bill(s)
                    if result['order_number']:
                        s.cancelled = False
                        s.extend()
                        s.save()
                        message += "Order successful and subscription extended. Order number is %s\n\n" % result['order_number']
                    else:
                        s.cancelled = True
                        s.save()
                        message += "Order unsuccessful. Error message: %s\n\n" % result['message']
                else:
                    pass
            else:
                pass
    else:
        message +="No subscriptions are in the system.\n"
    message += "The check_subscriptions function has successfully completed its program.\n"
    
    send_mail('NetConference Subscriptions for %s' % now, message, 'billing@netconference.com', ['colby@colbypalmer.com',], fail_silently=False)
    
    logging.info(message)
        
    return message