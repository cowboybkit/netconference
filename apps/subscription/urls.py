from django.conf.urls.defaults import *
from django.views.generic.simple import direct_to_template



import views

urlpatterns = patterns('subscription.views',
    url(r'^$', 'subscription_list', {}, name='subscription_list'),
    url("full/$", direct_to_template, {"template": "subscription/subscription_list_full.html"}, name="subscription_pricing_full"),
    (r'^personal/$', 'subscription_personal', {}, 'subscription_personal'),
    (r'^order/(?P<object_id>[-\w]+)/$', 'order_details', 
	 	{'template_name': 'subscription/order_details.html'}, 'order_details'),
    (r'^(?P<object_id>\d+)/$', 'subscription_detail', {}, 'subscription_detail'),
    )

urlpatterns += patterns('',
    (r'^paypal/', include('paypal.standard.ipn.urls')),
    (r'^done/', 'django.views.generic.simple.direct_to_template', dict(template='subscription/subscription_done.html'), 'subscription_done'),
    (r'^change-done/', 'django.views.generic.simple.direct_to_template', dict(template='subscription/subscription_change_done.html', extra_context=dict(cancel_url=views.cancel_url)), 'subscription_change_done'),
    (r'^cancel/', 'django.views.generic.simple.direct_to_template', dict(template='subscription/subscription_cancel.html'), 'subscription_cancel'),
    (r'^thanks/', 'django.views.generic.simple.direct_to_template', dict(template='subscription/subscription_done.html'), 'subscription_thanks'),
    
    )
