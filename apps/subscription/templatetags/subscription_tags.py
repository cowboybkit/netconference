from django import template

register = template.Library()

@register.filter
def file_size_units(size):
    """converts disk space MB units to GB"""
    if isinstance(size, long):
        if size>=1000:
            return str(size/1000)+' GB'
        return  str(size)+' MB'
    return size


@register.filter
def price_float_to_int(price):
    price_str = str(price)
    price = price_str[:-3]
    return price
