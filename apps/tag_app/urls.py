from django.conf.urls.defaults import *

urlpatterns = patterns('',
    # all tags
    # url(r'^(?P<tag>.+)/$', 'tag_app.views.tags', name='tag_results'),
    url(r'^(?P<username>\w+)/(?P<tag>\w+)/$', 'tag_app.views.user_tagged', name='tag_results_public'),
    url(r'^(?P<tag>.+)/$', 'tag_app.views.my_tagged', name='tag_results'),
)
