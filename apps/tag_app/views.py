from django.shortcuts import render_to_response, get_object_or_404
from django.http import Http404
from django.template import RequestContext
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from tagging.views import tagged_object_list

from blog.models import Post
from tagging.models import Tag, TaggedItem
from photos.models import Image
from bookmarks.models import BookmarkInstance
from tribes.models import Tribe
from topics.models import Topic as TribeTopic
from files.models import UserFile

from wiki.models import Article as WikiArticle

import logging


def tags(request, tag, template_name='tags/index.html'):
    tag = get_object_or_404(Tag, name=tag)
    
    alltags = TaggedItem.objects.get_by_model(Post, tag).filter(status=2)
    
    # phototags = TaggedItem.objects.get_by_model(Image, tag)
    # bookmarktags = TaggedItem.objects.get_by_model(BookmarkInstance, tag)
    
    tribe_tags = TaggedItem.objects.get_by_model(Tribe, tag)
    tribe_topic_tags = TaggedItem.objects.get_by_model(TribeTopic, tag)
    
    # @@@ TODO: tribe_wiki_article_tags and project_wiki_article_tags
    wiki_article_tags = TaggedItem.objects.get_by_model(WikiArticle, tag)
    
    #added by Colby
    user = request.user
    
    file_tags = TaggedItem.objects.get_by_model(UserFile, tag)
    
    files_personal = UserFile.active_objects.filter(user=user)
    file_tags_personal = TaggedItem.objects.get_by_model(files_personal, tag)
    
    files_public= UserFile.active_objects.filter(private=False)
    file_tags_public = TaggedItem.objects.get_by_model(files_public, tag)
    
    events_personal = Event.objects.filter(creator=user)
    event_tags_personal = TaggedItem.objects.get_by_model(events_personal, tag)
    
    events_public = Event.objects.filter(private=False)
    event_tags_public = TaggedItem.objects.get_by_model(public_conference_tags, tag)
    
    return render_to_response(template_name, {
        'tag': tag,
        'alltags': alltags,
        # 'phototags': phototags,
        # 'bookmarktags': bookmarktags,
        'tribe_tags': tribe_tags,
        'tribe_topic_tags': tribe_topic_tags,
        'wiki_article_tags': wiki_article_tags,
        'file_tags': file_tags,
        'file_tags_personal': file_tags_personal,
        'thumb_url': settings.THUMBNAIL_URL,
    }, context_instance=RequestContext(request))


def my_tagged(request, tag, template_name='tags/list_tag.html'):
    user = request.user
    logging.debug(tag)
    files_personal = UserFile.active_objects.filter(user=user)
    files_tagged = TaggedItem.objects.get_by_model(files_personal, tag)
    logging.debug(files_tagged)
    return render_to_response(template_name, {
        'tag': tag,
        'files_tagged': files_tagged,
        'thumb_url': settings.THUMBNAIL_URL,
        'username': user.username,
    }, context_instance=RequestContext(request))
my_tagged = login_required(my_tagged)

def user_tagged(request, username, tag, template_name='tags/list_tag.html'):
    user = request.user
    if username == None:
        files_personal = UserFile.active_objects.filter(user=user)
        files_tagged = TaggedItem.objects.get_by_model(files_personal, tag)
    else:
        if user.username == username:
            files_personal = UserFile.active_objects.filter(user=user)
            files_tagged = TaggedItem.objects.get_by_model(files_personal, tag)
        else:
            try:
                user_req = User.objects.get(username=username)
            except:
                raise Http404
            files_user_public = UserFile.public_objects.filter(user=user_req)
            files_tagged = TaggedItem.objects.get_by_model(files_user_public, tag)
    return render_to_response(template_name, {
        'tag': tag,
        'files_tagged': files_tagged,
        'thumb_url': settings.THUMBNAIL_URL,
        'username': username,
    }, context_instance=RequestContext(request))
user_tagged = login_required(user_tagged)