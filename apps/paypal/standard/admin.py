from string import split as L
from django.contrib import admin
from paypal.standard.models import PayPalStandardBase


# class PayPalStandardBase(admin.ModelAdmin):
#     list_display = L("user method flag flag_code created_at")

admin.site.register(PayPalStandardBase)
