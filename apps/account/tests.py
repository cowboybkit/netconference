from django.test import TestCase

from django.contrib.auth.models import User
from pprint import pprint

class AccountsTest(TestCase):

    def setUp(self):
        self.test_user = User.objects.create_user('test','lennon@thebeatles.com',password='test')
        self.client.login(username='test',password='test')
                    
    def test_duplicate_email_not_allowed(self):
        
        #Test creating netsign documents
        import random
        existing_email = User.objects.all()[0].email
        sf_dict = {'username':str(random.random())[2:],
                   'password1':'pass1','password2':'pass1',
                   'email':existing_email,
                   'confirmation_key':random.random()
                   }
        from account.forms import SignupForm
        sign_form = SignupForm(sf_dict)
        self.failIf(sign_form.is_valid())
        pprint(sign_form.errors)
        self.assert_(sign_form.errors.has_key('email'))
        
        from debug import ipython
        ipython()
        
        
        
__test__ = {"doctest": """
Another way to test that 1 + 1 is equal to 2.

>>> 1 + 1 == 2
True
"""}

