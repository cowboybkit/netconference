
import re, datetime, operator, pytz

from django import forms
from django.template.loader import render_to_string
from django.template import loader, Context
from django.conf import settings
from django.utils.translation import ugettext_lazy as _, ugettext
from django.utils.encoding import smart_unicode
from django.utils.hashcompat import sha_constructor
from django.http import HttpResponseRedirect

from checkout.forms import *

from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.contrib.sites.models import Site

from emailconfirmation.models import EmailAddress
from account.models import Account, LandingPageSignup
from rep_accounts.models import RepAccount

from timezones.forms import TimeZoneField

from account.models import PasswordReset
from signup_codes.models import check_signup_code
from mailer import send_html_mail as send_mail
from user_theme.models import UserTheme
from account.utils import COUNTRIES, STATES

alnum_re = re.compile(r'^\w+$')

PRETTY_TIMEZONE_CHOICES = []
for tz in pytz.common_timezones:
    now = datetime.datetime.now(pytz.timezone(tz))
    PRETTY_TIMEZONE_CHOICES.append((tz, _("GMT%(tz_offset)s : %(tz)s" % {"tz_offset": now.strftime("%z"), "tz": tz})))
PRETTY_TIMEZONE_CHOICES = sorted(PRETTY_TIMEZONE_CHOICES, key=lambda s: s[1].split(":")[0])

class CouponForm(forms.Form):
    coupon = forms.CharField(label="", widget=forms.TextInput({"class":"textfield", "length":40, "style":"width:400px;height:30px;text-align: center;"}))

    def clean_coupon(self):
        data = self.cleaned_data
        if "coupon" in data and not check_signup_code(data["coupon"]):
            raise forms.ValidationError("This is not a valid coupon")
        return data

class LoginForm(forms.Form):

    username = forms.CharField(label=_("Username"), max_length=30, widget=forms.TextInput({"class": "login_type_input"}))
    password = forms.CharField(label=_("Password"), widget=forms.PasswordInput({"class": "login_type_input"}, render_value=False))
    remember = forms.BooleanField(label=_("Remember Me"), help_text=_("If checked you will stay logged in for 3 weeks"), required=False)

    user = None

    def clean(self):
        if self._errors:
            return
        user = authenticate(username=self.cleaned_data["username"], password=self.cleaned_data["password"])
        if user:
            if user.is_active:
                self.user = user
            else:
                raise forms.ValidationError(_("This account is currently inactive. Please check your email for an email confirmation message and link once you perform this step your account will be activated."))
        else:
            raise forms.ValidationError(_("The username and/or password you specified are not correct."))
        return self.cleaned_data

    def login(self, request):
        if self.is_valid():
            login(request, self.user)
            message = ugettext(u"Hi %(username)s. Welcome to %(sitename)s\u2122. Your Office In The Cloud\u2122") % {
            "username": self.user.username,
            "sitename": u"NetConference"                                          
            }
            request.user.message_set.create(message=message)
            if self.cleaned_data['remember']:
                request.session.set_expiry(60 * 60 * 24 * 7 * 3)
            else:
                request.session.set_expiry(0)
            return True
        return False

from django.contrib.localflavor.us.forms import USPhoneNumberField

class BaseSignupForm(forms.Form):
    
    username = forms.CharField(label=_("Username"), max_length=30, widget=forms.TextInput())
    password1 = forms.CharField(label=_("Password"), widget=forms.PasswordInput(render_value=False))
    password2 = forms.CharField(label=_("Confirm Password"), widget=forms.PasswordInput(render_value=False))
    email = forms.EmailField(label=_("Email Adddress"), widget=forms.TextInput())

    def clean_username(self):
        username = self.cleaned_data["username"].strip()
        if not username:
            raise forms.ValidationError(_("This field is required."))
        if len(username) < 3:
            raise forms.ValidationError(_("Require a username greater than 2 characters."))
        if not alnum_re.search(username):
            raise forms.ValidationError(_("Usernames can only contain letters, numbers and underscores."))
        if username in getattr(settings, 'DISALLOWED_USERNAMES', []):
            raise forms.ValidationError(_("Username not allowed. Please choose another"))
        try:
            user = User.objects.get(username__iexact=username)
        except User.DoesNotExist:
            return username
        raise forms.ValidationError(_("This username is already taken. Please choose another."))

    def clean_password1(self):
        password = self.cleaned_data["password1"]
        if len(password) < 6:
            raise forms.ValidationError(_("Please enter a password with minimum 6 characters."))
        return password

    def clean_email(self):
        entered_email = self.cleaned_data['email'].strip()
        if not entered_email:
            raise forms.ValidationError(_("This field is required."))
        try:
            User.objects.get(email=entered_email)
        except User.DoesNotExist:
            return entered_email
        raise forms.ValidationError(_("This email id has already been registered. If you forgot your password, you can reset it."))

    def clean(self):
        if "password1" in self.cleaned_data and "password2" in self.cleaned_data:
            if self.cleaned_data["password1"] != self.cleaned_data["password2"]:
                raise forms.ValidationError(_("You must type the same password each time."))
        return self.cleaned_data

    def save(self):
        username = self.cleaned_data["username"]
        email = self.cleaned_data["email"]
        password = self.cleaned_data["password1"]
        new_user = User.objects.create_user(username, email, password)
        EmailAddress.objects.add_email(user=new_user, email=email)
        return username, password # required for authenticate()

class SignupForm(BaseSignupForm):

    phone_number = forms.CharField(widget=forms.TextInput({"class": "textfield_login"}))


    def __init__(self, *args, **kwargs):
        super(SignupForm, self).__init__(*args, **kwargs)
        self.fields['username'].widget.attrs['class'] = 'textfield_login'
        self.fields['password1'].widget.attrs['class'] = 'textfield_login'
        self.fields['password2'].widget.attrs['class'] = 'textfield_login'
        self.fields['email'].widget.attrs['class'] = 'textfield_login'

    def save(self):
        username, password = super(SignupForm, self).save()
        new_user = User.objects.get(username=username)
        user_profile = new_user.profile_set.get()
        user_profile.phone = self.cleaned_data['phone_number']
        user_profile.save()
        return username, password

class LandingSignupForm(BaseSignupForm):

    name = forms.CharField(widget=forms.TextInput())

    def __init__(self, *args, **kwargs):
        super(LandingSignupForm, self).__init__(*args, **kwargs)
        for each_field in self.fields.values():
            each_field.widget.attrs['class'] = 'textinput'

    def clean_name(self):
        name = self.cleaned_data['name'].strip()
        if not name:
            raise forms.ValidationError(_("This field is required."))
        return name

    def save(self):
        username, password = super(LandingSignupForm, self).save()
        new_user = User.objects.get(username=username)
        user_profile = new_user.profile_set.get()
        user_profile.name = self.cleaned_data['name']
        user_profile.save()
        return username, password

class UserDetailsSignupForm(SignupForm):
    first_name = forms.CharField(label=_("First Name"), max_length=30, widget=forms.TextInput({"class": "textfield_login"})) 
    last_name = forms.CharField(label=_("Last Name"), max_length=30, widget=forms.TextInput({"class": "textfield_login"}))
    company = forms.CharField(label=_("Company"), max_length=30, widget=forms.TextInput({"class": "textfield_login"}))
    
    address = forms.CharField(widget=forms.TextInput({"class": "textfield_login"}))
    #postal_code = forms.CharField(widget=forms.TextInput({"class": "textfield_login"}))
    country = forms.CharField(widget=forms.Select(choices=sorted(COUNTRIES, key=operator.itemgetter(1))))
    state = forms.CharField(widget=forms.Select(choices=sorted(STATES, key=operator.itemgetter(1))))
    city = forms.CharField(widget=forms.TextInput({"class": "textfield_login"}))

    def __init__(self, *args, **kwargs):
        super(UserDetailsSignupForm, self).__init__(*args, **kwargs)
        self.fields['country'].widget.attrs['size'] = 1
        self.fields['country'].widget.attrs['style'] = 'width:200px;'
        self.fields['state'].widget.attrs['size'] = 1

    def clean_first_name(self):
        first_name = self.cleaned_data["first_name"].strip()
        if not first_name:
            raise forms.ValidationError(_("This field is required."))
        return first_name

    def clean_last_name(self):
        last_name = self.cleaned_data["last_name"].strip()
        if not last_name:
            raise forms.ValidationError(_("This field is required."))
        return last_name

    def clean_company(self):
        company = self.cleaned_data["company"].strip()
        if not company:
            raise forms.ValidationError(_("This field is required."))
        return company

    def clean_address(self):
        address = self.cleaned_data["address"].strip()
        if not address:
            raise forms.ValidationError(_("This field is required."))
        return address

    """def clean_postal_code(self):        
        postal_code = self.cleaned_data['postal_code']
        validity = re.match("^[a-zA-Z0-9 -]+$", postal_code)
        try:
            validity.group()
            return postal_code
        except:
            raise forms.ValidationError("The postal code must be a number or an alphanumeric.")"""

    def clean_city(self):
        city = self.cleaned_data["city"].strip()
        if not city:
            raise forms.ValidationError(_("This field is required."))
        return city

    def clean_country(self):
        country = self.cleaned_data["country"].strip()
        if not country or country == u"-":
            raise forms.ValidationError(_("This field is required."))
        return country

    def save(self):
        username, password = super(UserDetailsSignupForm, self).save()
        new_user = User.objects.get(username=username)
        first_name = self.cleaned_data["first_name"]
        last_name = self.cleaned_data["last_name"]
        new_user.first_name = first_name
        new_user.last_name = last_name
        new_user.save()
        user_profile = new_user.profile_set.get()
        user_profile.billing_address_1 = self.cleaned_data['address']
        user_profile.billing_city = self.cleaned_data['city']
        user_profile.billing_country = dict(COUNTRIES)[self.cleaned_data['country']]
        if(self.cleaned_data['country'] != "USA"):
            state = None
        else:
            state = dict(STATES)[self.cleaned_data['state']]
        user_profile.billing_state = state
        #user_profile.billing_zip = self.cleaned_data['postal_code']
        user_profile.save()
        return username, password
        

class UserPaymentSignupForm(UserDetailsSignupForm): 
    credit_card_number = forms.RegexField(widget=forms.TextInput({"class": "textfield_login"}),
                                          regex="\d{16}")
    credit_card_type = forms.CharField(widget=forms.Select(choices=CARD_TYPES))
    credit_card_expire_month = forms.IntegerField(widget=forms.Select(choices=cc_expire_months()),
                                                  initial=datetime.datetime.now().strftime('%m'))
    credit_card_expire_year = forms.IntegerField(widget=forms.Select(choices=cc_expire_years()),
                                                 initial=datetime.datetime.now().strftime('%Y'))
    credit_card_cvv = forms.RegexField(widget=forms.TextInput({"class": "textfield_login"}),
                                       regex="\d{3,4}")
    
    def __init__(self, *args, **kwargs):
        super(UserPaymentSignupForm, self).__init__(*args, **kwargs)
        # override default attributes
        for field in self.fields:
            self.fields[field].widget.attrs['size'] = '30'
        
        self.fields['credit_card_type'].widget.attrs['size'] = '1'
        self.fields['credit_card_expire_year'].widget.attrs['size'] = '1'
        self.fields['credit_card_expire_month'].widget.attrs['size'] = '1'
        self.fields['credit_card_cvv'].widget.attrs['size'] = '5'
        self.fields['country'].widget.attrs['size'] = '1'
        self.fields['state'].widget.attrs['size'] = '1'
        self.fields['country'].widget.attrs['style'] = 'width:200px;'
    
    def clean_credit_card_number(self):
        """ validate credit card number if valid per Luhn algorithm """
        cc_number = self.cleaned_data['credit_card_number']
        stripped_cc_number = strip_non_numbers(cc_number)
        if len(stripped_cc_number) != 16:
            raise forms.ValidationError(_("The credit card number you entered is invalid."))
        if not cardLuhnChecksumIsValid(stripped_cc_number):
            raise forms.ValidationError(_('The credit card number you entered is invalid.'))
        return self.cleaned_data['credit_card_number']

    def clean_phone_number(self):
        import re
        phone = self.cleaned_data['phone_number']
        regx = "^(\d+$)"
        regx += "|^(\d+\sx\d+$)"
        regx += "|^(\d+\sext\d+$)"
        
        regx += "|^(\d+-\d+-\d+-\d+$)"
        regx += "|^(\d+-\d+-\d+-\d+\sx\d+$)"
        regx += "|^(\d+-\d+-\d+-\d+\sext\d+$)"
        
        regx += "|^(\d+.\d+.\d+.\d+$)"
        regx += "|^(\d+.\d+.\d+.\d+\sx\d+$)"
        regx += "|^(\d+.\d+.\d+.\d+\sext\d+$)"
        
        regx += "|^(\d+\s\(\d+\)\s\d+-\d+$)"
        regx += "|^(\d+\s\(\d+\)\s\d+-\d+\sx\d+$)"
        regx += "|^(\d+\s\(\d+\)\s\d+-\d+\sext\d+$)"
        
        regx += "|^(\d+/\d+/\d+/\d+$)"
        regx += "|^(\d+/\d+/\d+/\d+\sx\d+$)"
        regx += "|^(\d+/\d+/\d+/\d+\sext\d+$)"
        p = re.compile(regx)
        m = p.match(phone)
        stripped_phone = strip_non_numbers(phone)
        if m == None:
            raise forms.ValidationError('Enter a valid phone number with area code.')
        return self.cleaned_data['phone_number']
    
    def clean_postal_code(self):
        postal_code = self.cleaned_data['postal_code']
        validity = re.match("^[a-zA-Z0-9 -]+$", postal_code)
        try:
            validity.group()
            return postal_code
        except:
            raise forms.ValidationError("The postal code must be a number or an alphanumeric.")

    def clean_credit_card_cvv(self):
        cvv = self.cleaned_data["credit_card_cvv"]
        if len(cvv) != 3 and len(cvv) !=4:
            raise forms.ValidationError(_("CVV can have a maximum of 4 digits."))
        return cvv

    def clean(self):
        """ ignore cc errors is paying through paypal """
        cleaned_data = super(UserPaymentSignupForm, self).clean()
        if cleaned_data.get("credit_card_type") == "PayPal":
            if 'credit_card_number' in self.errors:
                del self.errors['credit_card_number']
            if 'credit_card_cvv' in self.errors:
                del self.errors['credit_card_cvv']
        else:
            expiry_month = cleaned_data.get("credit_card_expire_month")
            expiry_year = cleaned_data.get("credit_card_expire_year")
            expiry_date = datetime.datetime(expiry_year, expiry_month, 1)
            if expiry_date < datetime.datetime.today():
                raise forms.ValidationError(_("Credit Card is beyond expiry date."))
        return cleaned_data
    

class OpenIDSignupForm(forms.Form):
    username = forms.CharField(label="Username", max_length=30, widget=forms.TextInput())
    email = forms.EmailField(label="Email (optional)", required=False, widget=forms.TextInput())
    
    def __init__(self, *args, **kwargs):
        # @@@ this method needs to be compared to django-openid's form.
        
        # Remember provided (validated!) OpenID to attach it to the new user later.
        self.openid = kwargs.pop("openid")
        # TODO: do something with this?
        reserved_usernames = kwargs.pop("reserved_usernames")
        no_duplicate_emails = kwargs.pop("no_duplicate_emails")
        super(OpenIDSignupForm, self).__init__(*args, **kwargs)
    
    def clean_username(self):
        username = self.cleaned_data["username"].strip()
        if not username:
            raise forms.ValidationError(_("This field is required."))
        if len(username) < 3:
            raise forms.ValidationError(_("Require a username greater than 2 characters."))
        if username in getattr(settings, 'DISALLOWED_USERNAMES', []):
            raise forms.ValidationError(_("Username not allowed. Please choose another"))
        if not alnum_re.search(username):
            raise forms.ValidationError(u"Usernames can only contain letters, numbers and underscores.")
        try:
            User.objects.get(username__iexact=username)
        except User.DoesNotExist:
            return username
        raise forms.ValidationError(u"This username is already taken. Please choose another.")

    def save(self):
        username = self.cleaned_data["username"]
        email = self.cleaned_data["email"]
        new_user = User.objects.create_user(username, "", "!")

        if email:
            new_user.message_set.create(message="Confirmation email sent to %s" % email)
            EmailAddress.objects.add_email(new_user, email)

        if self.openid:
            # Associate openid with the new account.
            new_user.openids.create(openid = self.openid)
        return new_user


class UserForm(forms.Form):

    def __init__(self, user=None, *args, **kwargs):
        self.user = user
        super(UserForm, self).__init__(*args, **kwargs)

class AccountForm(UserForm):

    def __init__(self, *args, **kwargs):
        super(AccountForm, self).__init__(*args, **kwargs)
        try:
            self.account = Account.objects.get(user=self.user)
        except Account.DoesNotExist:
            self.account = Account(user=self.user)


class AddEmailForm(UserForm):

    email = forms.EmailField(label=_("Email"), required=True, widget=forms.TextInput(attrs={'size':'30'}))

    def clean_email(self):
        try:
            EmailAddress.objects.get(user=self.user, email=self.cleaned_data["email"])
        except EmailAddress.DoesNotExist:
            return self.cleaned_data["email"]
        raise forms.ValidationError(_("This email address is already associated with this account."))

    def save(self):
        self.user.message_set.create(message=ugettext(u"Confirmation email sent to %(email)s") % {'email': self.cleaned_data["email"]})
        return EmailAddress.objects.add_email(self.user, self.cleaned_data["email"])


class ChangePasswordForm(UserForm):

    oldpassword = forms.CharField(label=_("Current Password"), widget=forms.PasswordInput(render_value=False))
    password1 = forms.CharField(label=_("New Password"), widget=forms.PasswordInput(render_value=False))
    password2 = forms.CharField(label=_("New Password (again)"), widget=forms.PasswordInput(render_value=False))

    def clean_oldpassword(self):
        if not self.user.check_password(self.cleaned_data.get("oldpassword")):
            raise forms.ValidationError(_("Please type your current password."))
        return self.cleaned_data["oldpassword"]

    def clean_password1(self):
        password = self.cleaned_data["password1"]
        if len(password) < 6:
            raise forms.ValidationError(_("Please enter a password with minimum 6 characters."))
        return password

    def clean_password2(self):
        if "password1" in self.cleaned_data and "password2" in self.cleaned_data:
            if self.cleaned_data["password1"] != self.cleaned_data["password2"]:
                raise forms.ValidationError(_("You must type the same password each time."))
        return self.cleaned_data["password2"]

    def save(self):
        self.user.set_password(self.cleaned_data['password1'])
        self.user.save()
        self.user.message_set.create(message=ugettext(u"Password successfully changed."))


class SetPasswordForm(UserForm):
    
    password1 = forms.CharField(label=_("Password"), widget=forms.PasswordInput(render_value=False))
    password2 = forms.CharField(label=_("Password (again)"), widget=forms.PasswordInput(render_value=False))
    
    def clean_password2(self):
        if "password1" in self.cleaned_data and "password2" in self.cleaned_data:
            if self.cleaned_data["password1"] != self.cleaned_data["password2"]:
                raise forms.ValidationError(_("You must type the same password each time."))
        return self.cleaned_data["password2"]
    
    def save(self):
        self.user.set_password(self.cleaned_data["password1"])
        self.user.save()
        self.user.message_set.create(message=ugettext(u"Password successfully set."))

class ResetPasswordForm(forms.Form):

    email = forms.EmailField(label=_("Email"), required=True, widget=forms.TextInput(attrs={'class':'forgotpw'}))

    def clean_email(self):
        if EmailAddress.objects.filter(email__iexact=self.cleaned_data["email"]).count() == 0 or \
           User.objects.filter(email__iexact=self.cleaned_data["email"]).count() == 0:
            raise forms.ValidationError(_("Email address not associated for any user account"))
        return self.cleaned_data["email"]

    def save(self):
        user_email_list = [el for el in User.objects.filter(email__iexact=self.cleaned_data["email"])]
        for email_addr in  EmailAddress.objects.filter(email__iexact=self.cleaned_data["email"]):
            try:
                user_email_list.append(email_addr.user)
            except User.DoesNotExist:
                pass
        recepients = set(user_email_list)
        for user in recepients:
            temp_key = sha_constructor("%s%s%s%s" % (
                settings.SECRET_KEY,
                user.email,
                user.username,
                datetime.datetime.now().strftime('%s'),
            )).hexdigest()
            password_reset = PasswordReset(user=user, temp_key=temp_key)
            password_reset.save()
            
            current_site = Site.objects.get_current()
            domain = unicode(current_site.domain)
            
            #send the password reset email
            subject = _("Reset your password at NetConference")
            context = {"username":user.username, "domain":domain, "temp_key":temp_key}
            message = loader.get_template("account/newer_password_reset_key_message.html").render(Context(context))
            send_mail(subject, "", message, settings.DEFAULT_FROM_EMAIL, [user.email], priority="high")
        return self.cleaned_data["email"]
        
class ResetPasswordKeyForm(forms.Form):
    temp_key = forms.CharField(label="")
    password1 = forms.CharField(label=_("New Password"), widget=forms.PasswordInput(attrs={'class':'changepw', 'style':'margin-left:20px;'}, render_value=False),
        error_messages={'required':'Required field cannot be left blank'})
    password2 = forms.CharField(label=_("New Password (again)"), widget=forms.PasswordInput(attrs={'class':'changepw', 'style':'margin-left:20px;'}, render_value=False),
        error_messages={'required':'Required field cannot be left blank'})

    def clean_temp_key(self):
        temp_key = self.cleaned_data.get("temp_key")
        if not PasswordReset.objects.filter(temp_key=temp_key, reset=False).count():
            raise forms.ValidationError(_("Password reset key is invalid."))
        return temp_key

    def clean_password1(self):
        password = self.cleaned_data["password1"]
        if len(password) < 6:
            raise forms.ValidationError(_("Please enter a password with minimum 6 characters"))
        return password

    def clean_password2(self):
        if "password1" in self.cleaned_data and "password2" in self.cleaned_data:
            if self.cleaned_data["password1"] != self.cleaned_data["password2"]:
                raise forms.ValidationError(_("You must type the same password each time."))
        return self.cleaned_data["password2"]

    def save(self):
        temp_key = self.cleaned_data.get("temp_key")
        # get the password_reset object
        if not PasswordReset.objects.filter(temp_key__exact=temp_key, reset=False).count():
            return None
        password_reset = PasswordReset.objects.filter(temp_key__exact=temp_key, reset=False).order_by('-timestamp')[0]
        
        # now set the new user password
        #user = User.objects.get(passwordreset__exact=password_reset)
        user = password_reset.user
        if PasswordReset.objects.filter(user=user).order_by('-timestamp')[0].timestamp != password_reset.timestamp:
            return None
        user.set_password(self.cleaned_data["password1"])
        user.save()
        user.message_set.create(message=ugettext(u"Password successfully changed."))
        
        # change all the password reset records to this person to be true.
        for password_reset in PasswordReset.objects.filter(user=user):
            password_reset.reset = True
            password_reset.save()
        return password_reset
            

class ChangeTimezoneForm(AccountForm):

    timezone = TimeZoneField(label=_("Timezone"), required=True, choices=PRETTY_TIMEZONE_CHOICES)

    def __init__(self, *args, **kwargs):
        super(ChangeTimezoneForm, self).__init__(*args, **kwargs)
        self.initial.update({"timezone": self.account.timezone})

    def save(self):
        self.account.timezone = self.cleaned_data["timezone"]
        self.account.save()
        self.user.message_set.create(message=ugettext(u"Timezone successfully updated."))

class ChangeLanguageForm(AccountForm):

    language = forms.ChoiceField(label=_("Language"), required=True, choices=settings.LANGUAGES)

    def __init__(self, *args, **kwargs):
        super(ChangeLanguageForm, self).__init__(*args, **kwargs)
        self.initial.update({"language": self.account.language})

    def save(self):
        self.account.language = self.cleaned_data["language"]
        self.account.save()
        self.user.message_set.create(message=ugettext(u"Language successfully updated."))

class ChangeAppsDomainForm(AccountForm):

    apps_domain = forms.CharField(label=_("Apps Domain"), required=False)
    use_apps_calendar = forms.BooleanField(required=False)

    def clean_apps_domain(self):
        apps_domain = self.cleaned_data["apps_domain"].strip()
        if not apps_domain:
            raise forms.ValidationError(_("This field is required."))
        return apps_domain

    def __init__(self, *args, **kwargs):
        super(ChangeAppsDomainForm, self).__init__(*args, **kwargs)
        self.initial.update({"apps_domain": self.account.apps_domain})
        self.initial.update({"use_apps_calendar": self.account.use_apps_calendar})

    def save(self):
        self.account.apps_domain = self.cleaned_data["apps_domain"]
        self.account.use_apps_calendar = self.cleaned_data["use_apps_calendar"]
        self.account.save()
        self.user.message_set.create(message=ugettext(u"Apps Domain successfully updated."))

class ChangeUsernameForm(UserForm):

    username = forms.RegexField(label=_("Username"), max_length=30, regex=r'^\w+$',
        help_text = _("Required. 30 characters or fewer. Alphanumeric characters only (letters, digits and underscores)."),
        error_message = _("This value must contain only letters, numbers and underscores."))

    def clean_username(self):
        username = self.cleaned_data["username"].strip()
        if not username:
            raise forms.ValidationError(_("This field is required."))
        if username in getattr(settings, 'DISALLOWED_USERNAMES', []):
            raise forms.ValidationError(_("Username not allowed. Please choose another"))
        if len(username) < 3:
            raise forms.ValidationError(_("Require a username greater than 2 characters."))
        return username

    def __init__(self, *args, **kwargs):
        super(ChangeUsernameForm, self).__init__(*args, **kwargs)
        self.initial.update({"username": self.user.username})

    def save(self):
        if not User.objects.filter(username=self.cleaned_data["username"]).count():
            self.user.username = self.cleaned_data["username"]
            self.user.message_set.create(message=ugettext(u"Username updated successfully"))
            return self.user.save()
        if not self.user.username == self.cleaned_data["username"]:
            self.user.message_set.create(message=ugettext(u"Username update failed!"))
        else:
            self.user.message_set.create(message=ugettext("No change in the username."))
        return self.user.save()

class ChangeUserThemeForm(forms.ModelForm):
    logo = forms.ImageField(required=False)
    conference_logo = forms.ImageField(required=False)

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop("user", None)
        super(ChangeUserThemeForm, self).__init__(*args, **kwargs)

    class Meta:
        model = UserTheme
        exclude = ['user']

    def clean(self):
        data = self.cleaned_data
        if not data.get("logo", None) and not data.get("conference_logo", None):
            raise forms.ValidationError(_("Please provide either of dashboard logo or Conference logo."))
        return data

    def save(self, *args, **kwargs):
        self.instance.user = self.user
        return super(ChangeUserThemeForm, self).save(*args, **kwargs)

# @@@ these should somehow be moved out of account or at least out of this module

from account.models import OtherServiceInfo, other_service, update_other_services

class TwitterForm(UserForm):
    username = forms.CharField(label=_("Username"), required=True)
    password = forms.CharField(label=_("Password"), required=True,
                               widget=forms.PasswordInput(render_value=False))

    def __init__(self, *args, **kwargs):
        super(TwitterForm, self).__init__(*args, **kwargs)
        self.initial.update({"username": other_service(self.user, "twitter_user")})

    def save(self):
        from microblogging.utils import get_twitter_password
        update_other_services(self.user,
            twitter_user = self.cleaned_data['username'],
            twitter_password = get_twitter_password(settings.SECRET_KEY, self.cleaned_data['password']),
        )
        self.user.message_set.create(message=ugettext(u"Successfully authenticated."))


class TempSignupForm(forms.ModelForm):
    subscription = forms.CharField(widget=forms.HiddenInput)
        
    class Meta:
        model = LandingPageSignup

