from django.core.management.base import NoArgsCommand
from datetime import datetime
from dateutil.relativedelta import relativedelta
from django.contrib.auth.models import User
from django.template import loader, Context
from mailer import send_html_mail

class Command(NoArgsCommand):
    help = "Send report of users who signed_up/logged_in today"

    def handle_noargs(self, **options):
        current_time = datetime.now()
        last_24_hours = current_time + relativedelta(hours=-24)
        users_signed_up = User.objects.filter(date_joined__gt=last_24_hours, date_joined__lt=current_time)
        users_logged_in = User.objects.filter(last_login__gt=last_24_hours, last_login__lt=current_time)
        html_part = loader.get_template("account/delivery/todays_users.html")
        html_part = html_part.render(Context({"signed_up": users_signed_up, "logged_in": users_logged_in})) 
        text_part = loader.get_template("account/delivery/todays_users.txt")
        text_part = text_part.render(Context({"signed_up": users_signed_up, "logged_in": users_logged_in}))  
        send_html_mail("Following users signed up/logged in today", text_part, html_part, 'noreply@nxtgen.tv', ['brian@illusionfactory.com', 'kevinbennis@nxtgen.tv', 'staceymccuien@netconference.com', 'akshar@agiliq.com', 'hainguyen@esofthead.com', 'netconferenceads@gmail.com'])
