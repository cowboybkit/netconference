from friends.models import Contact
from friends_app.models import ContactGroup
from django.core.management.base import NoArgsCommand
from datetime import datetime
from django.contrib.auth.models import User
from profiles.models import Profile

class Command(NoArgsCommand):
    help = "Create daily groups and update to the groups"

    def handle_noargs(self, **options):
        kevin = User.objects.get(email='jason@nxtgen.tv', username='jason')
        today_date = datetime.today()
        today = today_date.strftime("%Y%m%d")
        group, created = ContactGroup.objects.get_or_create(owner=kevin,
                                                            name=today)
        nc_group, created = ContactGroup.objects.get_or_create(owner=kevin,
                                                               name="NC_Users")
        today_signed_users = User.objects.filter(date_joined__year=today_date.year,
                                                 date_joined__month=today_date.month,
                                                 date_joined__day=today_date.day)
        for user in today_signed_users:
            if Contact.objects.filter(user=kevin, email=user.email).count():
                continue
            try:
                profile = Profile.objects.get(user=user)
            except Profile.DoesNotExist:
                profile = None
            phone = None
            if profile:
                phone = profile.phone
            contact = Contact.objects.create(user=kevin,
                                             name=user.get_full_name() or None,
                                             email=user.email,
                                             phone=phone)
            contact.users.add(kevin)
            contact.groups.add(group)
            contact.groups.add(nc_group)
            contact.save()
