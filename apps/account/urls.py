from django.conf.urls.defaults import *
from account.forms import *
from django.conf import settings

ssl = settings.ENABLE_SSL

urlpatterns = patterns('',
    url(r'^settings/$', 'account.views.acct_settings', name="acct_settings"),
    url(r'^email/$', 'account.views.email', name="acct_email"),
    url(r'^signup/$', "account.views.signup_new", {'SSL': ssl}, name="acct_signup"),
    url(r'^login/$', 'account.views.login', name="acct_login"),
    url(r'^login/loginshare/$', 'account.views.loginshare', name="loginshare"),
    url(r'^login/RedirectToForm/$', 'account.views.RedirectToForm', name="RedirectToForm"),
    url(r'^login/intuit/$', 'account.views.login_intuit', name="acct_login_intuit"),
    #url(r'^login/openid/$', 'account.views.login', {'associate_openid': True}, name="acct_login_openid"),
    url(r'^password_change/$', 'account.views.password_change', name="acct_passwd"),
    url(r'^password_set/$', 'account.views.password_set', name="acct_passwd_set"),
    url(r'^password_delete/$', 'account.views.password_delete', name="acct_passwd_delete"),
    url(r'^password_delete/done/$', 'django.views.generic.simple.direct_to_template', {
        "template": "account/password_delete_done.html",
    }, name="acct_passwd_delete_done"),
    url(r'^password_reset/$', 'account.views.password_reset', name="acct_passwd_reset"),
    url(r'^timezone/$', 'account.views.timezone_change', name="acct_timezone_change"),
    url(r'^username/$', 'account.views.username_change', name="acct_username_change"),
    url(r'^usertheme/$', 'account.views.usertheme_change', name="acct_usertheme_change"),
    url(r'^other_services/$', 'account.views.other_services', name="acct_other_services"),
    url(r'^other_services/remove/$', 'account.views.other_services_remove', name="acct_other_services_remove"),
    
    url(r'^language/(?P<lang_code>[\w\-]+)/$', 'account.views.language_change_by_code', name="acct_language_change_by_code"),
    url(r'^language/$', 'account.views.language_change', name="acct_language_change"),
    url(r'^apps_domain/$', 'account.views.apps_domain_change', name="acct_apps_domain_change"),
    url(r'^logout/$', 'account.views.logout', {"template_name": "account/logout.html", "next_page":"/"}, name="acct_logout"),
    
    url(r'^confirm_email/(\w+)/$', 'emailconfirmation.views.confirm_email', name="acct_confirm_email"),

    # Setting the permanent password after getting a key by email
    url(r'^password_reset_key/(\w+)/$', 'account.views.password_reset_from_key', name="acct_passwd_reset_key"),

    # ajax validation
    (r'^validate/$', 'ajax_validation.views.validate', {'form_class': SignupForm}, 'signup_form_validate'),
    
    # Google AuthSub verification url
    url(r'^authsub/$', 'account.views.authsub_post', name="authsub_post"),

    # PayPal form
    url(r'^paypal/(\d+)$', 'account.views.paypal', name="paypal_payment"),
    # PayPal IPN
    url(r'^paypal/ipn/$', 'account.views.paypal_ipn', name="paypal_ipn"),
    # PayPal return url
    url(r'^paypal/done/(\d+)$', 'account.views.paypal_done', name="paypal_done"),

    # Temp signup url
    url(r'^landing/signup/$', 'account.views.landing_signup', name="landing_signup"),
    url(r'^welcome/$', 'account.views.welcome', name="account_welcome"),
    url(r'^landing_signup/$', 'account.views.landing_signup_new', name="landing_signup_new"),
)
