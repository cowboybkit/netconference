from django import template
from django.core.urlresolvers import reverse

register = template.Library()

URL_NAV_MAP = {
    # Tab URL : URL
    reverse("conferencing") : [reverse("my_calendar"), 
                               reverse("affiliate_dashboard"),
                               reverse("affiliate_opportunity"),
                               reverse("conferencing"),
                               ],
    reverse("invitations_contacts"): [reverse("affiliate_invite"),
                                      reverse("contact_groups"),
                                      reverse("invitations"),
                                      reverse("friends_directory"),
                                      ],
    reverse("my_conferences"): [reverse("conferencing_schedule"),
                                reverse("conferencing_instant"),
                                reverse("my_conferences"),
                                ],
    reverse("my_files"): [reverse("upload_file"),
                          reverse("files_browse"),
                          reverse("subscription_list"),
                          ],
    reverse("vmail_compose"): [reverse("vmail_outbox"),
                               reverse("vmail_trash")],
    reverse("vmail_wizardCompose"): [reverse("vmail_getContactList"),
                               reverse("vmail_getRequestUser"),
                               ],
}

@register.simple_tag
def active_nav(request, tab_url, class_name="active_top_nav"):
    if URL_NAV_MAP.get(tab_url, None) and request.path in URL_NAV_MAP[tab_url]:
        return class_name
    if request.path.startswith(tab_url) and not request.path.startswith(reverse("conferencing")):
        return class_name
    if (request.path.startswith("/conferencing/event/") or request.path.startswith("/conferencing/configure/")) and tab_url == "/conferencing/list/":
        return class_name
    if (request.path.startswith("/file/") or request.path.startswith("/googledriver/")) and tab_url == "/files/":
        return class_name
    return ""
