import datetime
import re

from django import template
from django.conf import settings
from account.models import other_service

register = template.Library()

class OtherServiceNode(template.Node):
    def __init__(self, user, key, asvar):
        self.user = user
        self.key = key
        self.asvar = asvar
    
    def render(self, context):
        user = self.user.resolve(context)
        key = self.key
        value = other_service(user, key)
                    
        if self.asvar:
            context[self.asvar] = value
            return ''
        else:
            return value


@register.tag(name='other_service')
def other_service_tag(parser, token):
    bits = token.split_contents()
    if len(bits) == 3: # {% other_service user key %}
        user = parser.compile_filter(bits[1])
        key = bits[2]
        asvar = None
    elif len(bits) == 5: # {% other_service user key as var %}
        if bits[3] != "as":
            raise template.TemplateSyntaxError("3rd argument to %s should be 'as'" % bits[0])
        user = parser.compile_filter(bits[1])
        key = bits[2]
        asvar = bits[4]
    else:
        raise template.TemplateSyntaxError("wrong number of arguments to %s" % bits[0])
    
    return OtherServiceNode(user, key, asvar)

@register.simple_tag
def get_shareasale_image(user):
    try:
        now = datetime.datetime.now()
        joined = user.date_joined
        delta = now - joined
        user_subscription = user.usersubscription_set.all()[0]
        account = user.account.all()[0]
        amount_paid = user_subscription.subscription.price
        merchant_id = getattr(settings, "SHAREASALE_MERCHANT_ID", "")
        shareasale_min_days = getattr(settings, "SHAREASALE_MIN_DAYS", 34)
        if delta.days > shareasale_min_days and not account.shareasale_updated:
            account.shareasale_updated = True
            account.save()
            return "<img src='https://shareasale.com/sale.cfm?amount=" + str(amount_paid) + "&tracking=" + str(user.pk) + "&transtype=sale&merchantID=" + merchant_id + "' width='1' height='1' />"
        return ""
    except:
        return ""
