try:
  from xml.etree import ElementTree # for Python 2.5 users
except ImportError:
  from elementtree import ElementTree
import gdata.calendar.service
import gdata.service
import atom.service
import gdata.calendar
import atom
import getopt
import sys
import string
import time
import settings

def GetAuthSubUrl(post_next=None):
    if settings.DEBUG:
        if post_next is None:
            next = "http://127.0.0.1:8000/account/authsub/"
        else:
            next = "http://127.0.0.1:8000/account/authsub/?post_next=" + post_next
    else:
        if post_next is None:
            next = "http://www.netconference.com/account/authsub/"
        else:
            next = "http://www.netconference.com/account/authsub/?post_next=" + post_next
    scope = 'http://www.google.com/calendar/feeds/'
    secure = False
    session = True
    calendar_service = gdata.calendar.service.CalendarService()
    return calendar_service.GenerateAuthSubURL(next, scope, secure, session);
authSubUrl = GetAuthSubUrl();

