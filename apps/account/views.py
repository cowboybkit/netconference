from django.conf import settings
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect, HttpResponseForbidden, Http404
from django.http.multipartparser import MultiPartParserError
from django.db.models import Q
from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login
from django.contrib.auth.views import logout as django_logout
from django.template import RequestContext
from django.utils.translation import ugettext, ugettext_lazy as _
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.db import models
from django.contrib.auth.models import User
from django.utils.hashcompat import sha_constructor
from random import random

from apps.checkout import authnet
from account.utils import get_default_redirect
from account.models import OtherServiceInfo
from account.forms import SignupForm, AddEmailForm, LoginForm, \
    ChangePasswordForm, SetPasswordForm, ResetPasswordForm, \
    ChangeTimezoneForm, ChangeLanguageForm, TwitterForm, ResetPasswordKeyForm, \
    ChangeAppsDomainForm, ChangeUsernameForm, ChangeUserThemeForm, TempSignupForm, \
    LandingSignupForm
from apps.account.forms import UserPaymentSignupForm, CouponForm, UserDetailsSignupForm
from apps.checkout.models import Order, ActiveTransaction
from user_theme.models import UserTheme
    
try:
    from mailer import send_html_mail as send_mail
except ImportError:
    raise
from emailconfirmation.models import EmailAddress, EmailConfirmation

import gdata.calendar.service
import gdata.service
import atom.service
import gdata.calendar
import os
import logging
from subscription.models import Subscription, Referer, UserSubscription
import datetime
from dateutil.relativedelta import relativedelta

from affiliate.utils import add_free_req_affiliate
from django.contrib.sites.models import Site
from django.template.loader import render_to_string
from django.template import loader, Context
from django.contrib.auth.models import User

from paypal.standard.forms import PayPalPaymentsForm

from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.views.decorators.http import require_POST
from django.views.i18n import set_language
import copy
import imghdr
from avatar.views import change

from apps.checkout import use_recurly

from apps.account.utils import populate_overview_files

filename = os.path.join(settings.LOG_FOLDER, 'google.log')

logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s %(levelname)s %(message)s',
    filename=filename,
    filemode='a',
)

association_model = models.get_model('django_openid', 'Association')
if association_model is not None:
    from django_openid.models import UserOpenidAssociation
    
def _check_next_url(next):
    """
    Checks to make sure the next url is not redirecting to another page.
    Basically it is a minimal security check.
    """
    if '://' in next:
        return None
    return next

def logout(request, template_name="account/logout.html", next_page="/"):
    return django_logout(request, template_name=template_name) 

def login(request, form_class=LoginForm,
        template_name="account/login.html", success_url=None,
        associate_openid=False, openid_success_url=None, url_required=False, next=None):
    next = None
    if 'next' in request.GET or 'next' in request.POST:
        next = request.GET.get('next') or request.POST.get('next') or ""
        # minimal security check to make sure it's not redirecting to another site
        if '://' in next:
            next = None
    if success_url is None:
        success_url = next or get_default_redirect(request)
    if next is None and success_url is None:
        success_url = next
    if request.method == "POST" and not url_required:
        form = form_class(request.POST)
        if form.login(request):
            if associate_openid and association_model is not None:
                for openid in request.session.get('openids', []):
                    assoc, created = UserOpenidAssociation.objects.get_or_create(
                        user=form.user, openid=openid.openid
                    )
                success_url = openid_success_url or success_url
            return HttpResponseRedirect(success_url)
    else:
        form = form_class()
    return render_to_response(template_name, {
        "form": form,
        "url_required": url_required,
        "next": success_url
    }, context_instance=RequestContext(request))
    
def login_intuit(request, form_class=LoginForm,
        template_name="account/login_intuit.html", success_url=None,
        associate_openid=False, openid_success_url=None, url_required=False, next=None):
    if 'next' in request.GET:
        next = request.GET['next'] or None
        # minimal security check to make sure it's not redirecting to another site
        if '://' in next:
            next = None
    if success_url is None:
        success_url = get_default_redirect(request)
    if next is None and success_url is None:
        success_url = next
    if request.method == "POST" and not url_required:
        form = form_class(request.POST)
        if form.login(request):
            if associate_openid and association_model is not None:
                for openid in request.session.get('openids', []):
                    assoc, created = UserOpenidAssociation.objects.get_or_create(
                        user=form.user, openid=openid.openid
                    )
                success_url = openid_success_url or success_url
            return HttpResponseRedirect(success_url)
    else:
        form = form_class()
    return render_to_response(template_name, {
        "form": form,
        "url_required": url_required,
    }, context_instance=RequestContext(request))


def sixty_day_coupon(request):
    form = CouponForm()
    if request.method == "POST":
        form = CouponForm(data=request.POST)
        if form.is_valid():
            request.session["has_60_coupon"] = True
            return HttpResponseRedirect("%s?subscription=5" % reverse("acct_signup"))
    payload = {"form": form}
    return render_to_response("account/60.html", payload, RequestContext(request))

def signup_new(request):
    credit_card_failed = []
    form_class = UserPaymentSignupForm
    show_cc_form = True
    sixty_day_trial = False
    free_days = 30
    if request.session.get("has_60_coupon", False):
        free_days = 60
        show_cc_form = False
        form_class = UserDetailsSignupForm
        sixty_day_trial = True
    signup_data = request.session.get("signup_data", None)
    form = form_class()
    if signup_data:
        form = form_class(initial=signup_data)
    if "subscription" in request.REQUEST:
        subs = Subscription.objects.get(pk=request.REQUEST["subscription"])
    else:
        subs = Subscription.objects.get_non_free().filter(name__istartswith="standard")[0]
    trial_period = subs.trial_period
    trial_unit = subs.trial_unit
    if trial_unit == 'M':
        trial_till = datetime.date.today() + relativedelta(months=1)
    elif trial_unit == 'D':
        trial_till = datetime.date.today() + relativedelta(days=1)
    elif trial_unit == 'N':
        trial_till = False

    #In case this user has 60 days coupon
    if free_days == 60:
        trial_till = datetime.datetime.today() + relativedelta(months=2)

    if request.method == "POST":
        form = form_class(data=request.POST)
        if form.is_valid():
            username, password = form.save()
            user = authenticate(username=username, password=password)
            if form.cleaned_data.get("credit_card_type") == "PayPal":
                user.is_active = False
                user.save()
                return HttpResponseRedirect(reverse("paypal_payment", args=[user.id]))
            data = form.cleaned_data
            if not request.session.get("has_60_coupon", False):
                cvv = data["credit_card_cvv"]
                cc_number = data["credit_card_number"]
                cc_exp_m = data["credit_card_expire_month"]
                cc_exp_y = data["credit_card_expire_year"]
            
                amount = subs.price
                cc_response = use_recurly.create_recurly_account(user, cc_number, cvv, cc_exp_m, cc_exp_y, subs, company=data["company"])
                
                if not cc_response[0]:
                    #Credit card failed
                    for msg in cc_response[-1]:
                        credit_card_failed.append(msg)
                    user.delete()
                    return render_to_response("account/signup.html", {
                            "form": form,
                            "subs": subs,
                            "trial_till": trial_till,
                            "credit_card_failed": credit_card_failed,
                            "show_cc_form": show_cc_form,
                            }, context_instance=RequestContext(request))
                else:
                    pass
            
            auth_login(request, user)
            next_month = trial_till
            user.usersubscription_set.create(user=user,
                                             subscription=subs,
                                             expires=next_month,
                                             active=True,
                                             cancelled=False)
            populate_overview_files(user)
            if not Referer.objects.filter(user=user).count():
                Referer.objects.create(user=user,
                                       source=request.COOKIES.get("Referer", None),
                                       campaign=request.COOKIES.get("Campaign", None),
                                       keyword=request.COOKIES.get("Keyword", None))
            message = _("Hi %(username)s. Welcome to %(sitename)s.") % {
            "username":user.username,
            "sitename": "Netconference"                                                         
            } 
            request.user.message_set.create(
                message=message)
            add_free_req_affiliate(request)
            send_welcome_autoresponder(user)
            request.session["show_welcome_page"] = True
            return HttpResponseRedirect(reverse('cc_signup_complete'))
    return render_to_response("account/signup.html", {
        "form": form,
        "subs": subs,
        "trial_till": trial_till,
        "credit_card_failed": credit_card_failed,
        "show_cc_form": show_cc_form,
    }, context_instance=RequestContext(request))

def paypal(request, uid):
    user = get_object_or_404(User, pk=uid)
    if "subscription" in request.REQUEST:
        subs = Subscription.objects.get(pk=request.REQUEST["subscription"])
    else:
        subs = Subscription.objects.get_non_free().filter(name__istartswith="standard")[0]
    notify_url = request.build_absolute_uri(reverse("paypal_ipn"))
    if settings.PAYPAL_TEST:
        notify_url = settings.PAYPAL_TEST_IPN
    return_url = request.build_absolute_uri(reverse("paypal_done", args=[user.pk]))
    trial_duration = 1
    if request.session.get("has_60_coupon", False):
        trial_duration = 2
    paypal_data = {
        'cmd': '_xclick-subscriptions',
        'a1': 0, # Trial 1 Price
        'p1': trial_duration, # Trial 1 Duration
        't1': 'M', # Trial 1 unit of Duration, default to Month
        'a3': subs.price, # monthly price 
        'p3': 1, # duration of each unit (depends on unit)
        't3': 'M', # duration unit ("M for Month")
        'src': '1', # make payments recur
        'sra': '1', # reattempt payment on payment error
        'item_name': "Netconference %s-day free trial" % (30 * trial_duration),
        'invoice': user.pk,
        'return_url': return_url,
        'notify_url': notify_url,
        'cancel_url': request.build_absolute_uri(request.path),
    }
    form = PayPalPaymentsForm(initial=paypal_data)
    return render_to_response('account/paypal.html', {
            'form': form,
        }, context_instance=RequestContext(request)
    )
    
@require_POST
def paypal_ipn(request):
    if "subscr_id" in request.POST:
        user = get_object_or_404(User, pk=request.POST.get("invoice"))
        user.is_active = True
        user.save()
        assign_free_subscription(user)
        register_email_autoresponder(user, True)
    return HttpResponse("OK")

def paypal_done(request, uid):
    user = get_object_or_404(User, pk=uid)
    if user.is_active:
        # hack to login without password
        user.backend = 'django.contrib.auth.backends.ModelBackend'
        auth_login(request, user)
        add_free_req_affiliate(request)
        return HttpResponseRedirect(reverse("conferencing"))
    return HttpResponseForbidden()

def register_email_autoresponder(instance, created, **kwargs):
    user = instance
    if created:
        if True:
            site = Site.objects.get_current()
            payload = {"user": user,
                       "site": site,
                       "MEDIA_URL": settings.MEDIA_URL,
                       }
            if 'request' in kwargs:
                payload.update({'random_password': kwargs['request'].session['random_password']})
            message = render_to_string("restrictions/thanks_for_registering.txt", payload)
            subject = _("Thanks for registering your account at NetConference")
            from_email = settings.DEFAULT_FROM_EMAIL
            recipient_list = [user.email]
            send_mail(subject, "", message, from_email, recipient_list)  
    

def assign_free_subscription(user):
    free_sub = Subscription.objects.get(name='Free')
    today = datetime.datetime.today()
    next_month = today + relativedelta(months=1)
    user.usersubscription_set.create(user=user,
                                     subscription=free_sub,
                                     expires=next_month,
                                     active=True,
                                     cancelled=False)

@login_required
def acct_settings(request, form=None, template_name="account/acct_settings.html"):
    if isinstance(form, AddEmailForm):
        add_email_form = form
    else:
        add_email_form = AddEmailForm()
    if isinstance(form, ChangeTimezoneForm):
        timezone_change_form = form
    else:
        timezone_change_form = ChangeTimezoneForm(request.user)
    if isinstance(form, ChangeLanguageForm):
        language_change_form = form
    else:
        language_change_form = ChangeLanguageForm(request.user)
    if isinstance(form, ChangeAppsDomainForm):
        apps_domain_change_form = form
    else:
        apps_domain_change_form = ChangeAppsDomainForm(request.user)
    user_subscription = request.user.get_usersubscription()
    user_orders = Order.objects.filter(user=request.user)
    if isinstance(form, ChangeUsernameForm):
        username_change_form = form
    else:
        username_change_form = ChangeUsernameForm(user=request.user)
    try:
        user_theme = UserTheme.objects.get(user=request.user)
    except UserTheme.DoesNotExist:
        user_theme = None
    if isinstance(form, ChangeUserThemeForm):
        usertheme_change_form = form
    else:
        usertheme_change_form = ChangeUserThemeForm(instance=user_theme)
    if isinstance(form, ChangePasswordForm):
        password_change_form = form
    else:
        password_change_form = ChangePasswordForm(user=request.user)
    return render_to_response(template_name, {
            "add_email_form": add_email_form,
            "timezone_change_form": timezone_change_form,
            "language_change_form": language_change_form,
            "apps_domain_change_form": apps_domain_change_form,
            "username_change_form": username_change_form,
            "usertheme_change_form": usertheme_change_form,
            "user_subscription": user_subscription,
            "user_theme": user_theme,
            "user_orders": user_orders,
            "password_change_form": password_change_form},
                              context_instance=RequestContext(request))
    
@login_required
def update_settings(request, form_class):
    """ 
    Generic view to save a form and redirect to settings page
    """
    form = None
    if request.method == "POST":
        form = form_class(request.user, request.POST)
        if form.is_valid():
            form.save()
            next = request.META.get('HTTP_REFERER', "/")
            return HttpResponseRedirect(next)
    return acct_settings(request, form=form)

@login_required
def email(request, form_class=AddEmailForm,
        template_name="account/acct_settings.html"):
    add_email_form = form_class()
    if request.method == "POST":
        if request.POST["action"] == "add":
            add_email_form = form_class(request.user, request.POST)
            if add_email_form.is_valid():
                add_email_form.save()
                add_email_form = form_class() # @@@
        else:
            add_email_form = form_class()
            if request.POST["action"] == "send":
                email = request.POST["email"]
                try:
                    email_address = EmailAddress.objects.get(
                        user=request.user,
                        email=email,
                    )
                    request.user.message_set.create(
                        message=unicode(_("Confirmation email sent to %s" % email)))
                    EmailConfirmation.objects.send_confirmation(email_address)
                except EmailAddress.DoesNotExist:
                    pass
            elif request.POST["action"] == "remove":
                email = request.POST["email"]
                try:
                    email_address = EmailAddress.objects.get(
                        user=request.user,
                        email=email
                    )
                    email_address.delete()
                    request.user.message_set.create(
                        message=unicode(_("Removed email address %s" % email)))
                except EmailAddress.DoesNotExist:
                    pass
            elif request.POST["action"] == "primary":
                email = request.POST["email"]
                email_address = EmailAddress.objects.get(
                    user=request.user,
                    email=email,
                )
                email_address.set_as_primary()
    return acct_settings(request, form=add_email_form)

@login_required
def password_change(request, form_class=ChangePasswordForm,
        template_name="account/acct_settings.html"):
    if not request.user.password:
        return HttpResponseRedirect(reverse("acct_passwd_set"))
    return update_settings(request, form_class=form_class)

def password_set(request, form_class=SetPasswordForm,
        template_name="account/acct_settings.html"):
    if request.user.password:
        return HttpResponseRedirect(reverse("acct_passwd"))
    if request.method == "POST":
        password_set_form = form_class(request.user, request.POST)
        if password_set_form.is_valid():
            password_set_form.save()
            return HttpResponseRedirect(reverse("acct_passwd"))
    else:
        password_set_form = form_class(request.user)
    return render_to_response(template_name, {
        "password_set_form": password_set_form,
    }, context_instance=RequestContext(request))
password_set = login_required(password_set)

def password_delete(request, template_name="account/password_delete.html"):
    # prevent this view when openids is not present or it is empty.
    if not request.user.password or \
        (not hasattr(request, "openids") or \
            not getattr(request, "openids", None)):
        return HttpResponseForbidden()
    if request.method == "POST":
        request.user.password = u""
        request.user.save()
        return HttpResponseRedirect(reverse("acct_passwd_delete_done"))
    return render_to_response(template_name, {
    }, context_instance=RequestContext(request))
password_delete = login_required(password_delete)

def password_reset(request, form_class=ResetPasswordForm,
        template_name="account/newer_password_reset.html",
        template_name_done="account/newer_password_reset_done.html"):
    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse("acct_settings"))
    if request.method == "POST":
        password_reset_form = form_class(request.POST)
        if password_reset_form.is_valid():
            email = password_reset_form.save()
            return render_to_response(template_name_done, {
                "email": email,
            }, context_instance=RequestContext(request))
    else:
        password_reset_form = form_class()
    
    return render_to_response(template_name, {
        "password_reset_form": password_reset_form,
    }, context_instance=RequestContext(request))
    
def password_reset_from_key(request, key, form_class=ResetPasswordKeyForm,
        template_name="account/newer_password_reset_from_key.html"):
    success = False
    if request.method == "POST":
        password_reset_key_form = form_class(request.POST)
        if password_reset_key_form.is_valid():
            if password_reset_key_form.save():
                success = True
    else:
        password_reset_key_form = form_class(initial={"temp_key": key})
    
    return render_to_response(template_name, {
        "form": password_reset_key_form,
        "success": success,
    }, context_instance=RequestContext(request))
    

@login_required
def timezone_change(request, form_class=ChangeTimezoneForm,
        template_name="account/acct_settings.html"):
    return update_settings(request, form_class=form_class)

@login_required
def language_change(request, form_class=ChangeLanguageForm,
        template_name="account/acct_settings.html"):
    return update_settings(request, form_class=form_class)


@login_required
def apps_domain_change(request, form_class=ChangeAppsDomainForm,
        template_name="account/acct_settings.html"):
    return update_settings(request, form_class=form_class)

@login_required
def username_change(request, form_class=ChangeUsernameForm,
        template_name="account/acct_settings.html"):
    return update_settings(request, form_class=form_class)

@login_required
def usertheme_change(request, form_class=ChangeUserThemeForm,
        template_name="account/acct_settings.html"):
    try:
        user_theme = UserTheme.objects.get(user=request.user)
    except UserTheme.DoesNotExist:
        user_theme = None
    if request.method == "POST":
        if user_theme:
            form = form_class(request.POST, request.FILES, instance=user_theme, user=request.user)
        else:
            form = form_class(request.POST, request.FILES, user=request.user)
        if form.is_valid():
            if form.cleaned_data["logo"]:
                request.user.message_set.create(message=unicode(_("Dashboard logo successfully updated.")))
            if form.cleaned_data["conference_logo"]:
                request.user.message_set.create(message=unicode(_("Conference logo successfully updated.")))
            form.save()
            next = request.META.get('HTTP_REFERER', reverse("acct_settings"))
            return HttpResponseRedirect(next)
    else:
        form = form_class(user=request.user)
    return acct_settings(request, form=form)

def other_services(request, template_name="account/other_services.html"):
    from microblogging.utils import twitter_verify_credentials
    twitter_form = TwitterForm(request.user)
    twitter_authorized = False
    if request.method == "POST":
        twitter_form = TwitterForm(request.user, request.POST)

        if request.POST['actionType'] == 'saveTwitter':
            if twitter_form.is_valid():
                from microblogging.utils import twitter_account_raw
                twitter_account = twitter_account_raw(
                    request.POST['username'], request.POST['password'])
                twitter_authorized = twitter_verify_credentials(
                    twitter_account)
                if not twitter_authorized:
                    request.user.message_set.create(
                        message=_("Twitter authentication failed"))
                else:
                    twitter_form.save()
    else:
        from microblogging.utils import twitter_account_for_user
        twitter_account = twitter_account_for_user(request.user)
        twitter_authorized = twitter_verify_credentials(twitter_account)
        twitter_form = TwitterForm(request.user)
    return render_to_response(template_name, {
        "twitter_form": twitter_form,
        "twitter_authorized": twitter_authorized,
    }, context_instance=RequestContext(request))
other_services = login_required(other_services)

def other_services_remove(request):
    # TODO: this is a bit coupled.
    OtherServiceInfo.objects.filter(user=request.user).filter(
        Q(key="twitter_user") | Q(key="twitter_password")
    ).delete()
    request.user.message_set.create(message=ugettext(u"Removed twitter account information successfully."))
    return HttpResponseRedirect(reverse("acct_other_services"))
other_services_remove = login_required(other_services_remove)

def authsub_post(request):
    filename = os.path.join(settings.LOG_FOLDER, 'google.log')
    logging.basicConfig(
        level=logging.INFO,
        format='%(asctime)s %(levelname)s %(message)s',
        filename=filename,
        filemode='a',
    )
    if request.GET.has_key('token'):
        authsub_token = request.GET['token']
        logging.info("Authsub token: %s" % authsub_token)
        calendar_service = gdata.service.GDataService()
        logging.info(calendar_service)
        # upgrade single-use token to session token - the first line below is deprecated
        # calendar_service.auth_token = authsub_token
        calendar_service.SetAuthSubToken(authsub_token)
        try:
            calendar_service.UpgradeToSessionToken()
            request.session['authsub_token'] = calendar_service.current_token
            logging.info(calendar_service.auth_token)
            request.user.message_set.create(message=_("Authenticated with Google."))
            post_url = request.GET['post_next'] + "?approved=1"
            return HttpResponseRedirect(post_url)
        except NonAuthSubToken:
            request.user.message_set.create(message=_("There was a problem with your Google authsub."))
            post_url = request.GET['post_next'] + "?approved=0"
    else:
        raise Http404


@require_POST
def landing_signup(request):
    data = request.POST.copy()
    data.update({"subscription": Subscription.objects.get(name="Standard").id})
    form = TempSignupForm(data)
    if form.is_valid():
        form.save()
    request.session["signup_data"] = data
    return HttpResponseRedirect("%s?id=%s" % (reverse("acct_signup"), data["subscription"]))


@login_required
def welcome(request):
    if request.session.get("show_welcome_page", None):
        del request.session["show_welcome_page"]
        user_sub = request.user.usersubscription_set.exclude(subscription__name='Free').order_by('-expires')
        amount = 0.0
        if user_sub.count():
            amount = float(user_sub[0].subscription.price)
        return render_to_response("account/welcome_page.html",
                                  {"DEBUG": settings.DEBUG,
                                   "amount": amount,
                                   "SHAREASALE_MERCHANT_ID": settings.SHAREASALE_MERCHANT_ID},
                                  context_instance=RequestContext(request))
    return HttpResponseRedirect(reverse("conferencing"))

def language_change_by_code(request, lang_code="en"):
    if request.user.is_authenticated():
        form = ChangeLanguageForm(user=request.user, data={"language": lang_code})
        if form.is_valid():
            form.save()        
    new_request = copy.copy(request)
    new_request.method = "POST"
    new_request.POST = {"language" : lang_code}
    return set_language(new_request)

@login_required
def avatar_change(request):
    if request.method == "POST":
        avatar_file = request.FILES.get('avatar', None)
        if avatar_file:
            try:
                avatar_image_format = imghdr.what(avatar_file)
            except MultiPartParserError:
                avatar_image_format = None
            if not avatar_image_format:
                request.user.message_set.create(message=unicode(_("Please upload a valid image format.")))
                return HttpResponseRedirect(request.META.get("HTTP_REFERER", request.path))
    return change(request)

def loginshare(request):
    if request.method == "POST":
        if "username" in request.POST and "password" in request.POST:
            username = request.POST['username']
            password = request.POST['password']
            response = HttpResponse(mimetype="application/xml")
            if password.startswith("#netconference#"):
                user = authenticate_with_username(username)
            else:
                user = authenticate(username=username, password=password)
            if user:
                response.write("<?xml version='1.0' encoding='UTF-8'?>")
                response.write("<loginshare><result>1</result><user>")
                response.write("<usergroup>Registered</usergroup>")
                response.write("<fullname>" + user.first_name + " " + user.last_name + "</fullname>")
                response.write("<designation>User</designation>")
                response.write("<organization>netconference</organization>")
                response.write("<organizationtype>restricted</organizationtype>")
                response.write("<emails><email>" + user.email + "</email></emails>")
                response.write("<phone>12345678</phone>")
                response.write("</user></loginshare>")
                
            else:
                response.write("<?xml version='1.0' encoding='UTF-8'?>")
                response.write("<loginshare><result>0</result>")
                response.write("<message>Invalid Username or Password</message>")
                response.write("</loginshare>")
            response['Content-length'] = str(len(response.content))
            return response
        else:
            return HttpResponse("hello")
    
def authenticate_with_sha1_pass(username, sha1_pass):
    try:
        user = User.objects.get(username=username, password=sha1_pass)
    except User.DoesNotExist:
        user = None
    return user
def authenticate_with_session(id):
    try:
        user = User.objects.get(pk=id)
    except User.DoesNotExist:
        user = None
    return user
def authenticate_with_username(uname):
    try:
        user = User.objects.get(username=uname)
    except User.DoesNotExist:
        user = None
    return user
from django.shortcuts import redirect
def RedirectToForm(request):
    if '_auth_user_id' in request.session:
        try:
            user = authenticate_with_session(request.session['_auth_user_id'])
        except:
            return "noAuth"
        return HttpResponse(user.username)
    else:
        return "noAuth"
    
def landing_signup_new(request):
    form = LandingSignupForm()
    if request.method == 'POST':
        form = LandingSignupForm(request.POST)
        if form.is_valid():
            username, password = form.save()
            user = authenticate(username=username, password=password)
            auth_login(request, user)
            after_withoutcc_signup(request)
            send_welcome_autoresponder(user)
            return HttpResponseRedirect(reverse('cc_signup_complete'))
    return render_to_response("account/landing_signup.html", {"form":form}, context_instance=RequestContext(request))

def after_withoutcc_signup(request):
    user = request.user
    subscription = Subscription.objects.get(name='pro')
    expires = datetime.datetime.today() + relativedelta(months=1)
    user_subscription = UserSubscription(user=user, subscription=subscription, expires=expires)
    user_subscription.save()
    populate_overview_files(user)

def send_welcome_autoresponder(user, send_confirmation_link=True):
    email_address = user.emailaddress_set.get()
    context = {}
    if send_confirmation_link:
        salt = sha_constructor(str(random())).hexdigest()[:5]
        confirmation_key = sha_constructor(salt + email_address.email).hexdigest()
        EmailConfirmation.objects.create(
            email_address=email_address,
            sent=datetime.datetime.now(),
            confirmation_key=confirmation_key)
        site = Site.objects.get()
        path = reverse('emailconfirmation.views.confirm_email', args=[confirmation_key])
        confirmation_url = 'http://' + site.domain + path
        context.update({'confirmation_url':confirmation_url})
    html_part = loader.get_template("autoresponders/nc_welcome.html").render(Context(context))
    send_mail("Welcome to NetConference", "", html_part, "noreply@nxtgen.tv", [email_address.email])
