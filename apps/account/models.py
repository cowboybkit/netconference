from django.db import models
from django.conf import settings
from django.contrib.auth.models import User, AnonymousUser
from django.db.models.signals import post_save
from django.utils.translation import get_language_from_request, ugettext_lazy as _
from datetime import datetime
from friends.models import Friendship
from netconf_utils.encode_decode import uri_b64decode
from files.models import UserFile
from schedule.models import Event, Rule

from timezones.fields import TimeZoneField

class Account(models.Model):
    
    user = models.ForeignKey(User, unique=True, verbose_name=_('user'), related_name="account")
    
    timezone = TimeZoneField(_('timezone'))
    language = models.CharField(_('language'), max_length=10, choices=settings.LANGUAGES, default=settings.LANGUAGE_CODE)
    apps_domain = models.CharField(_('apps_domain'), max_length=50, null=True, blank=True)
    use_apps_calendar = models.NullBooleanField(default=False, null=True, blank=True)
    shareasale_updated = models.BooleanField(default=False)
    send_autoresponder_mails = models.BooleanField(default=True)
    
    def __unicode__(self):
        return self.user.username


class OtherServiceInfo(models.Model):
    
    # eg blogrss, twitter_user, twitter_password
    
    user = models.ForeignKey(User, verbose_name=_('user'))
    key = models.CharField(_('Other Service Info Key'), max_length=50)
    value = models.TextField(_('Other Service Info Value'))
    
    class Meta:
        unique_together = [('user', 'key')]
    
    def __unicode__(self):
        return u"%s for %s" % (self.key, self.user)

class LandingPageSignup(models.Model):
    first_name = models.CharField(max_length=25)
    last_name = models.CharField(max_length=25)
    email = models.EmailField()
    company = models.CharField(max_length=30)

    def __unicode__(self):
        return "%s %s of %s" %(self.first_name, 
                               self.last_name, 
                               self.company)

def other_service(user, key, default_value=""):
    """
    retrieve the other service info for given key for the given user.
    
    return default_value ("") if no value.
    """
    try:
        value = OtherServiceInfo.objects.get(user=user, key=key).value
    except OtherServiceInfo.DoesNotExist:
        value = default_value
    return value

def update_other_services(user, **kwargs):
    """
    update the other service info for the given user using the given keyword args.
    
    e.g. update_other_services(user, twitter_user=..., twitter_password=...)
    """
    for key, value in kwargs.items():
        info, created = OtherServiceInfo.objects.get_or_create(user=user, key=key)
        info.value = value
        info.save()

def create_account(sender, instance=None, **kwargs):
    if instance is None:
        return
    account, created = Account.objects.get_or_create(user=instance)

post_save.connect(create_account, sender=User)

class AnonymousAccount(object):
    def __init__(self, request=None):
        self.user = AnonymousUser()
        self.timezone = settings.TIME_ZONE
        if request is not None:
            self.language = get_language_from_request(request)
        else:
            self.language = settings.LANGUAGE_CODE

    def __unicode__(self):
        return "AnonymousAccount"


class PasswordReset(models.Model):

    user = models.ForeignKey(User, verbose_name=_('user'))

    temp_key = models.CharField(_('temp_key'), max_length=100)
    timestamp = models.DateTimeField(_('timestamp'), default=datetime.now)
    reset = models.BooleanField(_('reset yet?'), default=False)

    def __unicode__(self):
        return "%s (key=%s, reset=%r)" % (self.user.username, self.temp_key, self.reset)


def befriend_team_nc(sender, **kwargs):
    if not kwargs["created"] or kwargs["instance"].username == "teamnc":
        return None
    try:
        Friendship.objects.create(to_user=User.objects.get(username="teamnc"), 
                                  from_user=kwargs["instance"])
    except User.DoesNotExist:
        return None

def upload_get_started_files(sender, **kwargs):
    def clone_file(user_file_inst, new_user):
        user_file_inst.id = None
        user_file_inst.views_total = 0
        user_file_inst.views_this_month = 0
        user_file_inst.downloads_total = 0
        user_file_inst.downloads_this_month = 0
        user_file_inst.project = None
        user_file_inst.user = new_user
        user_file_inst.copied_from = User.objects.get(username="brian")
        user_file_inst.child_uuid = user_file_inst.uuid
        user_file_inst.uuid = None
        # Don't consider the size of these files against user's account.
        user_file_inst.file_size = 0
        user_file_inst.save(force_insert=True)

    if not kwargs["created"]:
        return None
    # Corporate overview:
    # doc_id = int(uri_b64decode('Nzk4ODQ')) / 42
    # Intro Video
    video_id = int(uri_b64decode('MTgzNzA4')) / 42
    # if UserFile.objects.filter(user__username='brian',
    #                            id=doc_id).count():
    #     uf = UserFile.objects.get(user__username='brian',
    #                               id=doc_id)
    #     clone_file(uf, kwargs["instance"])
    if UserFile.objects.filter(user__username='brian',
                               id=video_id).count():
        uf = UserFile.objects.get(user__username='brian',
                                  id=video_id)
        clone_file(uf, kwargs["instance"])

def create_general_conference(sender, **kwargs):
    if not kwargs["created"]:
        return None

    try:
        daily_rule = Rule.objects.get(name='Daily Event')
    except Rule.DoesNotExist:
        return None

    midnight = datetime(2000, 1, 1, 0, 0, 0)
    eod = datetime(2000, 1, 1, 23, 59, 59)

    dict_args = {
        'start': midnight,
        'end': eod,
        'timezone': settings.TIME_ZONE,
        'title': unicode(_("My Conference")),
        'description': unicode(_("A conference that has no start and no end. Available to all users.")),
        'private': False,
        'is_recorded': False,
        'is_recurring': True,
        'creator': kwargs["instance"],
        'rule': daily_rule,
        'end_recurring_period': None,
        'force_registration': False,
        'reminder_interval': 0,
        'reminder_sent': True,
        'type': 'G',
        }
    Event.objects.create(**dict_args)

post_save.connect(befriend_team_nc, sender=User)
post_save.connect(upload_get_started_files, sender=User)
post_save.connect(create_general_conference, sender=User)

from south.modelsinspector import add_introspection_rules
add_introspection_rules([], ["^timezones\.fields\.TimeZoneField"])
