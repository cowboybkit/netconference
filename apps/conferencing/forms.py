from django import forms
from django.contrib.auth.models import User
from conferencing.models import Conference
from django.forms.util import ErrorList
from tagging.forms import TagField

class ConferenceForm(forms.ModelForm):

    #title = forms.CharField()
    #company = forms.ChoiceField()
    tags_string = TagField(label="Tags", required=False)
    date = forms.DateTimeField(widget=forms.SplitDateTimeWidget)
    #uuid = forms.CharField(widget=forms.HiddenInput)

    class Meta:
        model = Conference
        fields = ('title','date','tags_string','private')
        
    def __init__(self, user = None, *args, **kwargs):  
        self.user = user  
        super(ConferenceForm, self).__init__(*args, **kwargs)  
    
    def save(self):  
        if not self.uuid:  
            self.uuid = str(uuid4())  # random so it can't be easily guessed

class AnonymousAttendeeForm(forms.Form):
    screen_name = forms.CharField(max_length=25, required=True)
    email = forms.EmailField(required=True)
