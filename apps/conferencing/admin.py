from django.contrib import admin
from conferencing.models import Conference, SharedFile, RecordedConference, Attendees

class ConferenceAdmin(admin.ModelAdmin):
    list_display = ('title', 'uuid', 'moderator','private')
    list_filter = ['moderator', 'private']
    search_fields = ['title', 'moderator']
    date_hierarchy = 'date'
    actions = ['delete_selected', 'make_public', 'make_private']
    #exclude = ('tags',)
    
    def make_public(self, request, queryset):
        queryset.update(private=False)
    make_public.short_description = "Make selected meetings public"

    def make_private(self, request, queryset):
        queryset.update(private=True)
    make_private.short_description = "Make selected meetings private"

class SharedFileAdmin(admin.ModelAdmin):
    list_display = ('owner', 'conference', 'file')

admin.site.register(Conference, ConferenceAdmin)
admin.site.register(SharedFile, SharedFileAdmin)
admin.site.register(RecordedConference)
admin.site.register(Attendees)
