from django.db import models
from uuid import uuid4
import datetime
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
import tagging
from tagging.fields import TagField
from files.models import UserFile
from schedule.models import Event, LAYOUT_TYPE
from django.core.urlresolvers import reverse
from schedule.models import Rule
from django.template import loader, Context
#from django.core.management.validation import max_length
#  Subclass of Video to filter out inactive and unprocessed videos.
class PublicConferenceManager(models.Manager):
    def get_query_set(self):
        return super(PublicConferenceManager, self).get_query_set().filter(private='0')

class Conference(models.Model):
    """
    A scheduled Conference, with a uuid random identifier for the URL.
    """
    
    uuid = models.CharField(_('session_id'), max_length=36, blank=True, unique=True)
    moderator = models.ForeignKey(User)
    date = models.DateTimeField(auto_now=False, auto_now_add=False)
    length = models.CharField(max_length=36, blank=True)
    title = models.CharField(max_length=50)
    private = models.BooleanField()
    tags_string = TagField()
    
    objects = models.Manager() # The default manager.
    public_objects = PublicConferenceManager() # The active-and-processed-specific manager.
    
    def __unicode__(self):
        return self.uuid
    
    def get_absolute_url(self):
        return ('conference_session', (), { 'slug': self.uuid })
    get_absolute_url = models.permalink(get_absolute_url)
        
##These were broken
#    def _get_tags(self):
#        return Tag.objects.get_for_object(self)
#
#    def _set_tags(self, tag_list):
#        Tag.objects.update_tags(self, tag_list)
        
#    tags = property(_get_tags, _set_tags)
    
    def save(self):
        if not self.uuid:  
            self.uuid = str(uuid4())  # random so it can't be easily guessed
        super(Conference, self).save()
tagging.register(Conference)

class GroupFiles(models.Model):
    """
    References to uploaded video or media files.  THIS IS THE OLD VERSION!
    """
    owner = models.ForeignKey(User)
    conference_id = models.CharField(_('conference_id'), max_length=36)
    stream_id = models.CharField(_('stream_id'), max_length=36, blank=True)
    file_id = models.CharField(_('file_id'), max_length=36, blank=True)

class SharedFile(models.Model):
    """
    References to uploaded video or media files.  THIS IS THE NEW VERSION!
    """
    owner = models.ForeignKey(User)
    conference = models.ForeignKey(Event)
    file = models.ForeignKey(UserFile, related_name='shared_file', null=True, blank=True)
    
    class Meta:
        verbose_name = "Shared File"
        
        
class RecordedConference(models.Model):
    user = models.ForeignKey(User)
    created = models.DateTimeField(auto_now_add=True)
    duration = models.PositiveIntegerField()
    fms_hostroom = models.CharField(max_length=36)
    conference = models.ForeignKey(Event, blank=True, null=True)
    
    class Meta:
        ordering = ("-created", )
    
    def start(self):
        return self.created
    
    @property
    def userfile(self):
        return UserFile.objects.get(uuid = self.fms_hostroom)
    
    def get_absolute_url(self):
        return self.userfile.get_absolute_url()
    
    
    def title(self):
        try:
            return self.userfile.title
        except:
            return "Recorded Conference"
    
    
class Attendees(models.Model):
    """Attendees model"""
    
    event = models.ForeignKey(Event)
    user = models.ForeignKey(User, blank=True, null=True)
    email = models.EmailField(blank=True, null=True)
    screen_name = models.CharField(max_length=25)
    joined_datetime = models.DateTimeField(default=datetime.datetime.now())
    ''' status: [no, yes, maybe, not_defined] '''
    status = models.CharField(max_length = 25, default='not_defined')
    def __unicode__(self):
        return "%s : %s" %(self.event, 
                           self.screen_name or (self.user and self.user.username))

def __event_get_attendees(event):
    attendees = Attendees.objects.filter(event = event)
    return attendees
Event.add_to_class('get_attendees', __event_get_attendees)
        

class ReminderConference(models.Model):
    event = models.ForeignKey(Event, null=False, blank =False)
    rule = models.ForeignKey(Rule, null=True, blank =True)
    sender_email = models.CharField(max_length=255, null = False)
    subject = models.CharField(max_length=255, null = False, default = '')
    email_message = models.TextField(null = False, default= '')
    reminder_interval = models.IntegerField(null = False, default = 1440)
    ToRecipient = models.TextField(null=True)
    rule_text = models.CharField(max_length=255)
    time_send = models.DateTimeField(null=True, default = datetime.datetime.now())
    weekly = models.CharField(max_length = 255, null=True)
    monthly = models.CharField(max_length = 255, null=True)
    start = models.DateTimeField(null=False)
    period = models.DateTimeField(null=False)
    expired = models.BooleanField(null=False, default = False)
    timezone =  models.CharField(max_length=10, null = False,blank= False ,default='+00:00')
    is_guest  = models.BooleanField(null=False, default = True)
       
    def get_reminder_interval(self):
        if self.reminder_interval == 0 or not self.reminder_interval:
            return 0;
        return self.reminder_interval / 60   
       
    def format_reminder_interval(self):
        dic = {}
        time = self.reminder_interval
        if time >= 24*60*60:
            dic['time'] = time/(24*60*60);
            dic['type'] = 'Day'
        elif time >= 60*60:
            dic['time'] = time/(60*60)
            dic['type'] = 'Hour'
        else:
            dic['time'] = time/60
            dic['type'] = 'Minutes'
        return dic
    
    def re_calculate_fromevent(self):
        event = self.event
        self.rule = event.rule
        self.weekly = event.weekly
        self.monthly = event.monthly
        self.start = event.start
        self.period = event.end_recurring_period
        self.reminder_interval = event.reminder_interval
        self.time_send = event.start - datetime.timedelta(seconds = event.reminder_interval)
        self.subject = event.title
        self.sender_email = event.creator.email
        self.save()
        
        
        
    
class ConfigureConference(models.Model):
    user = models.ForeignKey(User, null=False, blank= False)

    is_telephone = models.BooleanField(_("audio type"), null = False, default=False)
    private = models.BooleanField(_("private"), null = False, default=False)
    ''' Host Option '''
    password_required = models.CharField(max_length = 255, null=True, default = None)
    host_must_present = models.BooleanField(_("host must present for guest"), null = False, default=False)
    guest_have_approval =  models.BooleanField(_("Guest must have host's approval"), null = False, default=False)
    host_control_layout  = models.BooleanField(_("Host control layout for guest."), null = False, default=True)
    
    ''' layout conference'''
    layout_type = models.CharField(_("Layout Conference"), max_length=255, choices=LAYOUT_TYPE, default="presentation")
    
    ''' Guest Permission ''' 
    view_guest_list = models.BooleanField(_("view guests list"), null = False, default=True)
    broadcast_video = models.BooleanField(_("broadcast video"), null = False, default=True)
    broadcast_audio = models.BooleanField(_("broadcast audio"), null = False, default=True)
    chat_with_host = models.BooleanField(_("chat with host"), null = False, default=True)
    chat_with_guest = models.BooleanField(_("chat with guest"), null = False, default=True)

def _get_configure_conference(user):
    try:
        configure = ConfigureConference.objects.get(user = user)
    except:
        configure = ConfigureConference(user = user)
        configure.save()
    return configure
User.add_to_class('get_configure_conference', _get_configure_conference)
    
class ConferenceFolder(models.Model):
    name = models.CharField(max_length=100)
    user = models.ForeignKey(User)
    parent_folder = models.ForeignKey('self', null=True, blank=True)
    is_root = models.BooleanField(null=False, default=False)
    #root folder conference deleted
    is_root_trash = models.BooleanField(null=False, default=False)
    absolute_path = models.TextField(editable=False)
    
    def __unicode__(self):
        return self.name
    
    def calculate_absolute_path(self):
        parent_folder_hierarchy = []
        par_folder = self.parent_folder
        while par_folder is not None:
            parent_folder_hierarchy.append(par_folder.name)
            par_folder = par_folder.parent_folder
        parent_folder_hierarchy.reverse()
        absolute_path = "/".join(parent_folder_hierarchy)
        if absolute_path:
            absolute_path = absolute_path + "/" + self.name
        else:
            absolute_path = self.name
        self.absolute_path = absolute_path
        
    def render_absolute_path(self):
        parent_folder_hierarchy = []
        parent_folder_hierarchy.append(self)
        par_folder = self.parent_folder
        while par_folder is not None:
            parent_folder_hierarchy.append(par_folder)
            par_folder = par_folder.parent_folder
        
        parent_folder_hierarchy.reverse()
        
        context = Context({'folder_list':parent_folder_hierarchy})
        html_part = loader.get_template('conferencing/folder_title_holder.html').render(context)
        return html_part
    
    def save(self, *args, **kwargs):
        self.calculate_absolute_path()
        super(ConferenceFolder, self).save(*args, **kwargs)

    def move_to_trash(self):
        root_trash_folder = ConferenceFolder.objects.filter(is_root_trash = True)[0]
        events = Event.objects.filter(folder = self).update(is_valid = False, folder=root_trash_folder)
        children = self.conferencefolder_set.all()
        for child in children:
            child.move_to_trash()
        self.delete()
        
def __user_get_conference_rootfolder(user):
    roots = ConferenceFolder.objects.filter(user = user, parent_folder = None, is_root =True, is_root_trash = False)
    if not roots:
        folder_root = ConferenceFolder(name='Conferences',user = user, parent_folder = None, is_root = True )
        folder_root.save()
        roots = ConferenceFolder.objects.filter(user=user, is_root = True)
    return roots[0]
User.add_to_class('get_conference_rootfolder', __user_get_conference_rootfolder)

def __user_get_conference_trashfolder(user):
    trashs = ConferenceFolder.objects.filter(user = user, parent_folder = None, is_root_trash =True)
    if not trashs:
        folder_trash = ConferenceFolder(name='Trash',user = user, parent_folder = None, is_root_trash= True )
        folder_trash.save()
        trashs = ConferenceFolder.objects.filter(user=user, is_root = True)
    return trashs[0]
User.add_to_class('get_conference_trashfolder', __user_get_conference_trashfolder)
        
class TimezoneConference(models.Model):
    event = models.ForeignKey(Event, null=False, blank= False)
    timezone = models.CharField(_("timezone"), max_length=255)
    
    