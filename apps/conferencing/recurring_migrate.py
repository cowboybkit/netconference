import os,sys
import datetime
activate_this = '/home/esofthead/pinax_env/bin/activate_this.py'
execfile(activate_this, dict(__file__=activate_this))

sys.path.insert(1,'/home/esofthead')
sys.path.insert(1,'/home/esofthead/netconference')
sys.path.insert(1,'/home/esofthead/netconference/apps')
#activate_this = '/home/cowboybkit/workspace/repository/pinax_env/bin/activate_this.py'
#execfile(activate_this, dict(__file__=activate_this))
#
#sys.path.insert(1,'/home/cowboybkit/workspace/recurly_integrate')
#sys.path.insert(1,'/home/cowboybkit/workspace/recurly_integrate/netconference')
#sys.path.insert(1,'/home/cowboybkit/workspace/recurly_integrate/netconference/apps')


os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'

from django.conf import settings
sys.path.append(settings.PINAX_ROOT)
sys.path.append("%s/%s"%(settings.PINAX_ROOT,'apps'))

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()
sys.stdout = sys.stderr

from schedule.models import Event
from django.contrib.auth.decorators import login_required
from django.views.generic.simple import direct_to_template


def run_migrate():
    week_events = Event.objects.filter(weekly=None, rule__frequency='WEEKLY')
    for event in week_events:
        day = event.start.strftime('%A')
        event.weekly = day
        event.save()
        print event.title +','+event.weekly+ '<br/>'
    month_events = Event.objects.filter(monthly=None,rule__frequency='MONTHLY')
    for event in month_events:
        month_day = event.start.strftime('%d')
        event.monthly = month_day
        event.save()
        print event.title +','+event.monthly+ '<br/>'

run_migrate()