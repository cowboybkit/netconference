from conferencing.models import ReminderConference
import datetime
reminders = ReminderConference.objects.all()
for remind in reminders:
    remind.time_send = remind.event.start - datetime.timedelta(seconds = remind.event.reminder_interval)
    remind.save()