from schedule.models import Rule, Event
from conferencing.models import ReminderConference, TimezoneConference
from django.db.models import Q
import datetime
import calendar
from mailer import send_html_mail
from timezones.utils import adjust_datetime_to_timezone
from django.conf import settings
import pytz
from django.template.loader import render_to_string
from django.utils.translation import ugettext_lazy as _
from django.contrib.sites.models import Site
from django.template import loader, Context
from base64 import urlsafe_b64encode, urlsafe_b64decode

def get_list_reminder(recur_minute = 5):
    now = datetime.datetime.now()
    endmonth_day = calendar.monthrange(now.year,now.month)[1]
    month_day = str(now.day)
    now_is_endmonth = False
    if now.day == endmonth_day:
        now_is_endmonth = True
    
    top_timesend = now + datetime.timedelta(minutes = recur_minute)
    print str(top_timesend)
    if now_is_endmonth:
        reminders = ReminderConference.objects.filter(
            Q(rule__id=1, period__gt = now, time_send__gt = now, time_send__lt = top_timesend) 
            | Q(rule__id=2, period__gt = now, time_send__gt = now, time_send__lt = top_timesend
                ,weekly__contains = now.strftime('%A'))
            | Q(rule__id=3, period__gt = now, time_send__gt = now, time_send__lt = top_timesend
                ,monthly = month_day)
             | Q(rule__id=3, period__gt = now, time_send__gt = now, time_send__lt = top_timesend
                ,monthly = 'endmonth')
        )
    else:
        reminders = ReminderConference.objects.filter(
            Q(rule__id=1, period__gt = now, time_send__gt = now, time_send__lt = top_timesend) 
            | Q(rule__id=2, period__gt = now, time_send__gt = now, time_send__lt = top_timesend
                ,weekly__contains = now.strftime('%A'))
            | Q(rule__id=3, period__gt = now, time_send__gt = now, time_send__lt = top_timesend
                ,monthly = month_day)
        )
    
    return reminders

def get_template_email(attendee, message, is_guest):
    event = attendee.event
    frequen = ''
    if event.rule:
        frequen =  event.rule.frequency
    subject = render_to_string("conferencing/reminder_subject.txt", {"type":frequen, 'event':event })
    
    tz = event.timezone
    now = datetime.datetime.now(pytz.timezone(tz))
    tz = (tz, _("%(tz)s: (UTC%(tz_offset)s)" % {"tz_offset": now.strftime("%z"), "tz": tz}))
    event.timezone = {'value':tz[0], 'title':tz[1].strip()}
    
    
    extra_timezones = TimezoneConference.objects.filter(event = event)
    
    for extra_tz in extra_timezones:
        tz = extra_tz.timezone
        now = datetime.datetime.now(pytz.timezone(tz))
        tz = (tz, _("%(tz)s: (UTC%(tz_offset)s)" % {"tz_offset": now.strftime("%z"), "tz": tz}))
        extra_tz.timezone = {'value':tz[0], 'title':tz[1].strip()} 
    
#    if not message:
#        message = "It just reminder"
    
    rsvp = {}
    rsvp['yes'] = urlsafe_b64encode('event='+event.urlhash+'&email='+attendee.email+'&status=yes')
    rsvp['no'] = urlsafe_b64encode('event='+event.urlhash+'&email='+attendee.email+'&status=no')
    rsvp['maybe'] = urlsafe_b64encode('event='+event.urlhash+'&email='+attendee.email+'&status=maybe')
    context_template = {
               'event':event,
               'message':message,
               'site': unicode(Site.objects.get_current()),
               'extra_timezones':extra_timezones,
               'is_guest':is_guest,
               'attendee':attendee,
               'rsvp':rsvp,
               'test':'event='+event.urlhash+'&email='+attendee.email+'&status=yes'
               }
    
    context = Context(context_template)
    html_part = loader.get_template('vmail/delivery/invitation_email.html').render(context)
    
    return html_part

def send_email_invitation(attendee, subject, message, from_email):
    html_part = get_template_email(attendee, message, True)
    send_html_mail(
          subject = subject,
          message = html_part,
          message_html =html_part,
          from_email = from_email,
          recipient_list = [attendee.email],
          cc_list= None,
          bcc_list = None,
      )

def send_email_reminder(reminders):
    for remind in reminders:
        if remind.is_guest:
            attendees = remind.event.get_attendees()
            for attendee in attendees:
                if attendee.status !='no':
                    html_part = get_template_email(attendee, None, remind.is_guest)
                    send_html_mail(
                           subject = remind.subject,
                           message = html_part,
                           message_html =html_part,
                           from_email = remind.event.creator.email,
                           recipient_list = [attendee.email],
                           cc_list= None,
                           bcc_list = None,
                           )
                
        else:
            html_part = get_template_email(attendee, None, remind.is_guest)
            send_html_mail(
                           subject = remind.subject,
                           message = html_part,
                           message_html =html_part,
                           from_email = remind.event.creator.email,
                           recipient_list = [remind.event.creator.email],
                           cc_list= None,
                           bcc_list = None,
                           )
def run_remind_send(minutes = 5):
    reminders = get_list_reminder(minutes)
    if reminders:
        print 'co'
    else:
        print 'ko co'
    if reminders:
        send_email_reminder(reminders)
    