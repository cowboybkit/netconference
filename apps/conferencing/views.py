from netconf_utils import netconf_common_timezones
from netconf_utils.netconf_common_timezones import *
from django.shortcuts import get_object_or_404, render_to_response
from django.views.generic.simple import direct_to_template
from django.views.generic.list_detail import object_detail, object_list
from django.http import HttpResponseRedirect, Http404, HttpResponse
from django import forms
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from urllib2 import urlopen, HTTPError, URLError
import logging
import datetime
from conferencing.forms import ConferenceForm, AnonymousAttendeeForm
from conferencing.models import Conference, SharedFile, Attendees, RecordedConference, ReminderConference, ConferenceFolder, TimezoneConference
from schedule.models import *
from schedule.periods import *
from files.models import UserFile
from files.search import *
from ccall_pins.models import ConferencePin
from uuid import uuid4
import random
from apps.subscription.models import Subscription, UserSubscription
from user_theme.models import UserTheme
from checkout.models import OrderItem, Order
from cart.models import CartItem
from mailer import send_html_mail as send_mail
from django.template.loader import render_to_string
from django.template import RequestContext
from django.contrib.sites.models import Site
from files.views import sorted_object_list
from django.views.decorators.http import require_POST
from django.utils.translation import ugettext_lazy as _
import recurly
from avatar.models import Avatar
from avatar.templatetags.avatar_tags import avatar_url
import dateutil.parser
from account.models import Account
from netconf_utils.encode_decode import *
from timezones.utils import adjust_datetime_to_timezone
import pytz
from schedule.models import Rule
from ccall_pins.models import ConferenceBridgeNumber
from django.template import loader, Context
from django.template.loader import render_to_string
from django.core.urlresolvers import reverse
from mailer import send_mail, send_html_mail
#from dbus.decorators import method
from friends.models import Contact
from vmail_new.models import is_valid_email
import json
from vmail.models import Vmail, VmailAttachment
from uuid import uuid4
import simplejson
from httplib import HTTP
from conferencing.models import *
from django.contrib.sites.models import RequestSite
from conferencing.run_reminder import send_email_invitation
from base64 import urlsafe_b64encode, urlsafe_b64decode
from django.utils.translation import ugettext_lazy
_ = lambda x: unicode(ugettext_lazy(x))

LAYOUT_TYPE = ["presentation", "meeting", "video","webinar"]
HOST_OPTION = ["password_required", "host_must_present", "guest_have_approval","host_control_guest"]
GUEST_PERMISSION = ["view_guest_list", "broad_video", "broad_audio","chat_with_host","chat_with_guest"]


def conference_home(request):
    return direct_to_template(request, 'conferencing/base.html')
    
def my_conferences(request):
    #user = request.user.id
    user = request.user
    query_string = ''
    page_title = 'My Conferences'
    qs = Event.objects.filter(creator=user)
    instant_query = Event.objects.filter(creator=user, type="G")
    if ('q' in request.GET) and request.GET['q'].strip():
        page_title = 'Search results'
        query_string = request.GET['q']
        entry_query = get_query(query_string, ['title', 'description'])
        user = request.user
        qs = qs.filter(entry_query)
    qs, type, date, from_time, to_time, per_page, conference_folder, template_name = filter_conferences(qs, request)
    qs = qs.order_by('type', '-start')
    is_filtered = False
    if 'filter' in request.GET:
        is_filtered = True
    from restrictions.used_restrictions import ConferenceMinutesRestriction
    cmr = ConferenceMinutesRestriction(request.user)
    allow_join = cmr.allow()
    
    
    conference_folders = ConferenceFolder.objects.filter(user=request.user, is_root = True)
    if not conference_folders:
        folder_root = ConferenceFolder(name='Conferences',user = request.user, parent_folder = None, is_root = True )
        folder_root.save()
        conference_folders = ConferenceFolder.objects.filter(user=request.user, is_root = True)
        
    root_folder = conference_folders[0]
    if not conference_folder:
        conference_folder = root_folder
    
    sort_by = False
    if 'sort_by' in request.GET: sort_by=True
    
    return sorted_object_list(
            request, 
            template_name = template_name,  
            queryset=qs, 
            paginate_by= per_page,
            extra_context={
                'nav_current':'conf_nav',
                'allow_join':allow_join,
                'page_title': page_title,
                'is_filtered': is_filtered,
                'type': type,
                'time': date,
                'from_time':from_time,
                'to_time':to_time,
                'per_page': per_page,
                'query_string': query_string,
                'instant':request.user.get_instant_conference(),
                'sort_by':sort_by,
                'root_folder':root_folder,
                'current_folder':conference_folder,
                }
            )
my_conferences = login_required(my_conferences)

def filter_conferences(qs, request):
    type = "all"
    date = "anytime"
    template_name = 'conferencing/newest_conferencing_list.html'
    try:
        per_page =  int(request.GET["per_page"])
    except:
        per_page = settings.ITEMS_LIST_PAGINATE_BY
        
    from_time = datetime.datetime.now()
    to_time = from_time + datetime.timedelta(days = 7)
    
    conference_folder = None
    if "folder" in request.GET:
        root_folder = ConferenceFolder.objects.filter(is_root = True)[0]
        root_folder_trash = ConferenceFolder.objects.filter(is_root_trash = True)[0]
        
        folder_id = int(request.GET['folder'])
        
        conference_folder = ConferenceFolder.objects.get(pk=folder_id)
#        if conference_folder.parent_folder_id == root_folder_trash.id or conference_folder.is_root_trash == True:
        if conference_folder.is_root_trash == True:
            template_name = 'conferencing/conferencing_list_deleted.html'
            qs = qs.filter(is_valid = False, folder=conference_folder)
#            if not conference_folder.is_root_trash:
#                qs = qs.filter(folder=conference_folder)
#            else:
#                qs = qs.filter(Q(folder=conference_folder) | Q(folder=None))
        else:
            qs = qs.filter(is_valid = True)
            
            if not conference_folder.is_root:
                qs = qs.filter(folder=conference_folder)
            else:
                qs = qs.filter(Q(folder=conference_folder) | Q(folder=None))
    else:
        qs = qs.filter(is_valid = True)
        
        root_folder = ConferenceFolder.objects.filter(is_root=True, user = request.user)
        if not root_folder:
            folder_root = ConferenceFolder(name='Conferences',user = request.user, parent_folder = None, is_root = True )
            folder_root.save()
        if 'q' in request.GET or 'filter' in request.GET:
            pass
        else:
            qs = qs.filter(Q(folder=root_folder) | Q(folder=None))
    
    if 'filter' in request.GET:
        if "type" in request.GET:
            type = request.GET.getlist("type")
            if "completed" in  type and "scheduled" in type:
                qs = qs
            elif "completed" in  type:
                qs = qs.filter(start__lte = datetime.datetime.now())
            elif "scheduled" in  type:
                qs = qs.filter(start__gte = datetime.datetime.now())
            if "private" in  type and "public" in type:
                qs = qs
            elif "private" in  type:
                qs = qs.filter(private = True)
            elif "public" in  type:
                qs = qs.exclude(private = True)   
                 
            if "flagged" in type and "unflagged" in type:
                qs = qs
            elif "flagged" in type:
                qs = qs.filter(is_flag = True)
            elif "unflagged" in type:
                qs = qs.filter(is_flag = False)
                
            if "recurring" in type:
                qs = qs.filter(is_recurring = True)
        if "time" in request.GET:
            date = request.GET["time"]
            
            if date == "next24":
                hrs_ago_24 = datetime.datetime.now()
                hrs_now_24 = datetime.datetime.now() + datetime.timedelta(hours=24)
                qs = qs.filter(start__gt=hrs_ago_24, start__lt = hrs_now_24)
            elif date == "nextweek":
                days_ago_7 = datetime.datetime.now()
                days_now_7 = days_ago_7 + datetime.timedelta(days = 7)
                qs = qs.filter(start__gt = days_ago_7, start__lt = days_now_7)
            elif date == "nextmonth":
                days_ago_30 = datetime.datetime.now()
                days_now_30 = datetime.datetime.now() + datetime.timedelta(days = 30)
                qs = qs.filter(start__gt = days_ago_30, start__lt = days_now_30)
            elif date == "next2month":
                days_ago_30 = datetime.datetime.now()
                days_now_30 = datetime.datetime.now() + datetime.timedelta(days = 60)
                qs = qs.filter(start__gt = days_ago_30, start__lt = days_now_30)
            elif date == "nextyear":
                days_ago_365 = datetime.datetime.now()
                days_now_365 = datetime.datetime.now() + datetime.timedelta(days = 365)
                qs = qs.filter(start__gt = days_ago_365, start__lt = days_now_365)
            elif date == "custom":
                from_time = dateutil.parser.parse(request.GET["from_time"])
                to_time = dateutil.parser.parse(request.GET["to_time"])
                qs = qs.filter(start__gt = from_time, start__lt = to_time)
    return qs, type, date, from_time, to_time, per_page, conference_folder, template_name


def my_conferences_inc(request):
    user = request.user.id
    qs = Event.objects.filter(moderator=user).order_by('start')
    return object_list(request, template_name='conferencing/conference_list_inc.html', queryset=qs)
my_conferences_inc = login_required(my_conferences_inc)

def conferences_public(request):
    now = datetime.datetime.now()
    qs = Event.public_objects.filter(end__gte=now).order_by('start')
    return object_list(request, template_name='conferencing/conference_public.html', queryset=qs, paginate_by=settings.ITEMS_LIST_PAGINATE_BY)

def add_conference(request, conference_form=ConferenceForm, tags=None):
    conference_form = ConferenceForm()
    user = request.user.id
    if request.method == 'POST':
        conference_form_request = ConferenceForm(request.user, request.POST)
        if conference_form_request.is_valid():
            user = request.user
            conference_title = conference_form_request.cleaned_data['title']
            conference_date = conference_form_request.cleaned_data['date']
            conference_private = conference_form_request.cleaned_data['private']
            conference_tags = conference_form_request.cleaned_data['tags_string']
            conference_form_add = Conference(moderator=user, title=conference_title, date=conference_date, private=conference_private, tags_string=conference_tags)
            conference_form_add.save()
            request.user.message_set.create(message="Your conference was created!")
            return HttpResponseRedirect('/conferencing/list/',)
        else:
            #raise forms.ValidationError("Form not valid!")
            # logging.debug(request.POST)
            # logging.error(conference_form_request.errors)
            request.user.message_set.create(message="There was an error. Please make sure you entered a title and used the correct date format.")    
    return direct_to_template(request, 'conferencing/conference_create.html', extra_context={'conference_form': conference_form})
add_conference = login_required(add_conference)


def edit_conference(request, slug, conference_form=ConferenceForm, tags=None):
   conference = Conference.objects.get(uuid=slug)
   conference_id = conference.id
   conference_form = ConferenceForm(instance=conference)
   user = request.user.id
   if request.method == 'POST' and 'title' in request.POST:
       conference_form_request = ConferenceForm(request.user, request.POST)
       conference_tags = conference_form_request['tags_string']
       if conference_form_request.is_valid():
           user = request.user
           conference_title = conference_form_request.cleaned_data['title']
           conference_date = conference_form_request.cleaned_data['date']
           conference_private = conference_form_request.cleaned_data['private']
           conference_tags = conference_form_request.cleaned_data['tags_string']
           conference_form_add = Conference(id=conference_id, moderator=user, title=conference_title, date=conference_date, private=conference_private, tags_string=conference_tags, uuid=slug)
           conference_form_add.save()
           request.user.message_set.create(message="Your conference was edited!")
           return HttpResponseRedirect('/conferencing/list/',)
       else:
           logging.error(conference_form_request.errors)
           request.user.message_set.create(message="There was an error. Please make sure you entered a title and used the correct date format.")
   elif request.method == 'POST' and 'delete' in request.POST:
       conference_delete = Conference.objects.get(id=conference_id)
       if request.user == conference_delete.moderator:
           conference_delete.delete()
           request.user.message_set.create(message="Your conference was deleted!")
           return HttpResponseRedirect('/conferencing/list/',)
       else:
           request.user.message_set.create(message="You cannot delete other people's conferences.")
           return HttpResponseRedirect('/conferencing/list/',)
   
   return direct_to_template(request, 'conferencing/conference_edit.html', extra_context={'conference_form': conference_form})
edit_conference = login_required(edit_conference)

from restrictions.utils import get_conference_flashvars, get_conference_flashvars2
from restrictions.decorators import check_conference_allow




@check_conference_allow
def conference_flex(request, urlhash):
    event_id = int(uri_b64decode(str(urlhash))) / 42
    extra_context = get_conference_flashvars2(request)
    free_sub = Subscription.objects.get_free()
    if request.user.is_authenticated() and request.user.get_subscription() == free_sub:
        extra_context["is_free"] = True
        extra_context["non_free_subs"] = Subscription.objects.get_non_free()
    extra_context["conf_urlhash"] = urlhash
    conference = get_object_or_404(Event, pk=event_id)
    user = request.user
    if request.method == "GET" and not request.user.is_authenticated():
        anonymous_token = random.randrange(1000, 9999)
        extra_context.update({'conference': conference, 'anonymous_token': anonymous_token})
        response = direct_to_template(request, 'conferencing/conference_flex_login.html', extra_context)
        setattr(response, "djangologging.suppress_output", True)
        return response

    if request.method == "POST" and "anonymous" in request.POST:
        form = AnonymousAttendeeForm(request.POST)
        if form.is_valid():
            data = {'screenname': form.cleaned_data['screen_name'], 
                    'conferenceHash': str(urlhash),
                    'email': form.cleaned_data['email'],
                    'type': 0
                    };
            if conference.type == "G" and user != conference.creator:
                screen_name = form.cleaned_data["screen_name"]
                send_mail("%s joined your General conference" % screen_name, 
                          "",
                          render_to_string("conferencing/join_general_conference.txt", 
                                           "",
                                           RequestContext(request,
                                                          {"conference": conference,
                                                           "user": screen_name,
                                                           "site": Site.objects.get_current(),})),
                          settings.DEFAULT_FROM_EMAIL, [conference.creator.email],
                          priority="high")
            response = direct_to_template(request, 'conferencing/ConferencePageGuest.html', data)
            setattr(response, "djangologging.suppress_output", True)
            return response
        else:
            extra_context.update({'form': form})
        extra_context.update({'user': user,
                              'urlhash': str(urlhash)})
    if user.username != "":
        extra_context.update({'conference': conference,
                              'user': user, 'screen_name': user.username,
                              'urlhash': str(urlhash)})
        # Send an email to the owner if it is a general conference
        if conference.type == "G" and user != conference.creator:
            send_mail("%s joined your General conference" % request.user.username, 
                      "",
                      render_to_string("conferencing/join_general_conference.txt", 
                                       RequestContext(request,
                                                      {"conference": conference,
                                                       "user": request.user,
                                                       "site": Site.objects.get_current(),})),
                      settings.DEFAULT_FROM_EMAIL, [conference.creator.email],
                      priority="high")
        data = {'screenname':user.username,
                'conferenceHash': str(urlhash),
                'email':user.email,
                'type':1
                };
        response = direct_to_template(request, 'conferencing/ConferencePage.html', data)
    else:
        anonymous_token = random.randrange(1000, 9999)
        extra_context.update({
                      'user': anonymous_token,
                      'screen_name': anonymous_token,
                      'urlhash': str(urlhash)})
        response = direct_to_template(request, 'conferencing/conference_flex_login.html', extra_context)
        
    setattr(response, "djangologging.suppress_output", True)
    return response

from django.contrib.auth.models import *

def conference_flash(request, urlhash):
    conference = get_object_from_hash(Event, urlhash)
    username = conference.creator.username
    user = request.user
    host = get_object_or_404(User, username=username)
    
    hasAdminRight = 'false'
    if user.is_authenticated() and request.user.is_superuser:
        hasAdminRight = 'true';
    
    if not user.is_authenticated():
        return direct_to_template(request,'conferencing/conferencingx.html',
                 {'screenname':'',
                  'cid':settings.AWS_STORAGE_BUCKET_NAME,
                  'first_name':'','last_name':'','type':1,
                  'email':'','userid':-1,
                  'conferenceHash':urlhash,
                  'hasAdminRight':hasAdminRight})
    else:
        try:
            custom_avatar = avatar_url(user,200).split('?Signature')[0]
        except:
            custom_avatar = ''
        
        if user.id == host.id:
            firstname = ''
            lastname = ''
            try:
                if user.get_profile().name:
                    fullname = user.get_profile().name
                    arr = fullname.split(' ') 
                    if len(arr)>0:
                        lastname = arr[len(arr)-1]
                        firstname = fullname[:(len(fullname)-len(lastname))].strip()
                    else:
                        lastname = fullname
                        firstname = ''
            except:
                pass
            return direct_to_template(request,'conferencing/conferencingx.html',
                                      {'screenname':user.username,'cid':settings.AWS_STORAGE_BUCKET_NAME,'first_name':firstname,'last_name':lastname,'type':4,'email':user.email,'userid':user.id,'conferenceHash':urlhash,'avatar':custom_avatar,'hasAdminRight':hasAdminRight})
        else:
            firstname = ''
            lastname = ''
            try:
                if user.get_profile().name:
                    fullname = user.get_profile().name
                    arr = fullname.split(' ') 
                    if len(arr)>0:
                        lastname = arr[len(arr)-1]
                        firstname = fullname[:(len(fullname)-len(lastname))].strip()
                    else:
                        lastname = fullname
                        firstname = ''
            except:
                pass
            return direct_to_template(request,'conferencing/conferencingx.html',
                                      {'screenname':user.username,'cid':settings.AWS_STORAGE_BUCKET_NAME,'first_name':firstname,'last_name':lastname,'type':3,'email':user.email,'userid':user.id,'conferenceHash':urlhash,'avatar':custom_avatar, 'hasAdminRight':hasAdminRight})


@check_conference_allow
def conference_session2(request, urlhash):
    event_id = int(uri_b64decode(str(urlhash))) / 42
    extra_context = get_conference_flashvars2(request)
    free_sub = Subscription.objects.get_free()
    if request.user.is_authenticated() and request.user.get_subscription() == free_sub:
        extra_context["is_free"] = True
        extra_context["non_free_subs"] = Subscription.objects.get_non_free()
    extra_context["conf_urlhash"] = urlhash
    conference = get_object_or_404(Event, pk=event_id)
    username = conference.creator.username
    user = request.user
    gid = str(uuid4())
    host = get_object_or_404(User, username=username)
    shared_files = SharedFile.objects.filter(conference=conference)
    ccall_pin = ConferencePin.objects.get(id=int(host.id))
    try:
        user_theme = UserTheme.objects.get(user=conference.creator)
    except UserTheme.DoesNotExist:
        user_theme = None
    extra_context.update({'conf_user_theme': user_theme})

    if request.method == "GET" and not request.user.is_authenticated():
        anonymous_token = random.randrange(1000, 9999)
        extra_context.update({'conference': conference, 'anonymous_token': anonymous_token})
        response = direct_to_template(request, 'conferencing/conference_login.html', extra_context)
        setattr(response, "djangologging.suppress_output", True)
        return response

    if request.method == "POST" and "anonymous" in request.POST:
        form = AnonymousAttendeeForm(request.POST)
        if form.is_valid():
            try:
                Attendees.objects.get_or_create(event=conference, 
                                                email=form.cleaned_data["email"],
                                                screen_name=form.cleaned_data["screen_name"])
            except Attendees.MultipleObjectsReturned:
                attendee = Attendees.objects.filter(event=conference,
                                                    email=form.cleaned_data["email"],
                                                    screen_name=form.cleaned_data["screen_name"]).order_by('-joined_datetime')[0]
                attendee.joined_datetime = datetime.datetime.now()
                attendee.save()
            extra_context.update({'screen_name': form.cleaned_data["screen_name"],
                                  'urlhash': str(urlhash),
                                  'user': user,
                                  'host': host,
                                  'anonymous': True,
                                  'files': 'none', 
                                  'shared_files': shared_files, 
                                  'ccall_pin': ccall_pin, 
                                  'gid': gid,
                                  'conference': conference,
                                  })
            # Send an email to the owner if it is a general conference
            if conference.type == "G" and user != conference.creator:
                screen_name = form.cleaned_data["screen_name"]
                send_mail("%s joined your General conference" % screen_name, 
                          "",
                          render_to_string("conferencing/join_general_conference.txt", 
                                           "",
                                           RequestContext(request,
                                                          {"conference": conference,
                                                           "user": screen_name,
                                                           "site": Site.objects.get_current(),})),
                          settings.DEFAULT_FROM_EMAIL, [conference.creator.email],
                          priority="high")
            response = direct_to_template(request, 'conferencing/conference_popout2.html', extra_context)
            setattr(response, "djangologging.suppress_output", True)
            return response
        else:
            extra_context.update({'form': form})
        extra_context.update({'conference': conference, 
                              'host': host, 
                              'user': user, 
                              'anonymous': True, 
                              'files': 'none', 
                              'shared_files': shared_files, 
                              'ccall_pin': ccall_pin, 
                              'gid': gid, 
                              'urlhash': str(urlhash)})
    if user.username != "":
        Attendees.objects.get_or_create(event=conference, 
                                        user=user,
                                        email="",
                                        screen_name="")
        files = UserFile.active_objects.filter(user=user).exclude(source_type='rec_conf').order_by('title')
        extra_context.update({'conference': conference, 'host': host, 
                              'user': user, 'screen_name': user.username, 
                              'anonymous': False, 'files': files, 
                              'shared_files': shared_files, 
                              'ccall_pin': ccall_pin, 
                              'gid': gid, 
                              'urlhash': str(urlhash)})
        if user == conference.creator:
            try:
                host_sub = UserSubscription.objects.get(user=user)
            except UserSubscription.DoesNotExist:
                host_sub = None
            except UserSubscription.MultipleObjectsReturned:
                host_sub = UserSubscription.objects.filter(user=user).order_by('-expires')[0]
            if host_sub:
                extra_context.update({"host_sub": host_sub.subscription})
        # Send an email to the owner if it is a general conference
        if conference.type == "G" and user != conference.creator:
            send_mail("%s joined your General conference" % request.user.username, 
                      "",
                      render_to_string("conferencing/join_general_conference.txt", 
                                       RequestContext(request,
                                                      {"conference": conference,
                                                       "user": request.user,
                                                       "site": Site.objects.get_current(),})),
                      settings.DEFAULT_FROM_EMAIL, [conference.creator.email],
                      priority="high")
        
        response = direct_to_template(request, 'conferencing/conference_popout2.html', extra_context)
    else:
        anonymous_token = random.randrange(1000, 9999)
        extra_context.update({'conference': conference,
                      'host': host,
                      'user': anonymous_token,
                      'screen_name': anonymous_token,
                      'anonymous': True,
                      'files': 'none',
                      'shared_files': shared_files,
                      'ccall_pin': ccall_pin,
                      'gid': gid,
                      'urlhash': str(urlhash)})
        response = direct_to_template(request, 'conferencing/conference_login.html', extra_context)
        
    setattr(response, "djangologging.suppress_output", True)
    return response
# conference_session = login_required(conference_session)

@check_conference_allow
def conference_session(request, urlhash):
    event_id = int(uri_b64decode(str(urlhash))) / 42
    try:
        conference = Event.objects.get(pk=event_id)
    except:
        raise Http404
    extra_context = get_conference_flashvars(request.user)
    username = conference.creator.username
    user = request.user
    gid = str(uuid4())
    host = get_object_or_404(User, username=username)
    shared_files = SharedFile.objects.filter(conference=conference)
    ccall_pin = ConferencePin.objects.get(id=int(host.id))
    if request.method == "POST" and "anonymous" in request.POST:
        screen_name = request.POST['screen_name']
        if screen_name != "":
            extra_context.update({'conference': conference, 'host': host, 'user': user, 'screen_name': screen_name, 'anonymous': True, 'files': 'none', 'shared_files': shared_files, 'ccall_pin': ccall_pin, 'gid': gid, 'urlhash': str(urlhash)})
            response = direct_to_template(request, 'conferencing/conference_popout.html', extra_context)
        
    elif not request.user.is_authenticated():
        anonymous_token = random.randrange(1000, 9999)
        extra_context.update({'conference': conference, 'anonymous_token': anonymous_token})
        response = direct_to_template(request, 'conferencing/conference_login.html', extra_context)
        setattr(response, "djangologging.suppress_output", True)
        return response
        
    elif user.username != "":
        files = UserFile.active_objects.filter(user=user).order_by('title')
        extra_context.update({'conference': conference, 'host': host, 'user': user, 'screen_name': user.username, 'anonymous': False, 'files': files, 'shared_files': shared_files, 'ccall_pin': ccall_pin, 'gid': gid, 'urlhash': str(urlhash)})
        response = direct_to_template(request, 'conferencing/conference_popout.html', extra_context)
    else:
        anonymous_token = random.randrange(1000, 9999)
        extra_context.update({'conference': conference, 'host': host, 'user': anonymous_token, 'screen_name': anonymous_token, 'anonymous': True, 'files': 'none', 'shared_files': shared_files, 'ccall_pin': ccall_pin, 'gid': gid, 'urlhash': str(urlhash)})
        response = direct_to_template(request, 'conferencing/conference_popout.html', extra_context)
        
    setattr(response, "djangologging.suppress_output", True)
    return response

def conference_session_quick(request, username):
    """Same as above, but using the username as urlhash / session title"""
    extra_context = get_conference_flashvars(request.user)
    conference = "quickconference"
    user = request.user
    gid = str(uuid4())
    host = get_object_or_404(User, username=username)
    shared_files = SharedFile.objects.filter(conference=username)
    ccall_pin = ConferencePin.objects.get(id=int(host.id))
    if request.method == "POST" and "anonymous" in request.POST:
        screen_name = request.POST['screen_name']
        if screen_name != "":
            extra_context.update({'conference': conference, 'host': host, 'user': user, 'screen_name': screen_name, 'anonymous': True, 'files': 'none', 'shared_files': shared_files, 'ccall_pin': ccall_pin, 'gid': gid, 'urlhash': username})
            response = direct_to_template(request, 'conferencing/conference_popout.html', extra_context)
        
    elif not request.user.is_authenticated():
        anonymous_token = random.randrange(1000, 9999)
        extra_context.update({'conference': conference, 'anonymous_token': anonymous_token})
        response = direct_to_template(request, 'conferencing/conference_login.html', extra_context)
        setattr(response, "djangologging.suppress_output", True)
        return response
        
    elif user.username != "":
        files = UserFile.active_objects.filter(user=user).order_by('title')
        extra_context.update({'conference': conference, 'host': host, 'user': user, 'screen_name': user.username, 'anonymous': False, 'files': files, 'shared_files': shared_files, 'ccall_pin': ccall_pin, 'gid': gid, 'urlhash': username})
        response = direct_to_template(request, 'conferencing/conference_popout.html', extra_context)
    else:
        anonymous_token = random.randrange(1000, 9999)
        extra_context.update({'conference': conference, 'host': host, 'user': anonymous_token, 'screen_name': anonymous_token, 'anonymous': True, 'files': 'none', 'shared_files': shared_files, 'ccall_pin': ccall_pin, 'gid': gid, 'urlhash': username})
        response = direct_to_template(request, 'conferencing/conference_popout.html', extra_context)
        
    setattr(response, "djangologging.suppress_output", True)
    return response

def myconference_search(request):
    query_string = ''
    found_entries = None
    if ('q' in request.GET) and request.GET['q'].strip():
        query_string = request.GET['q']
        entry_query = get_query(query_string, ['title', 'description'])
        user = request.user
        now = datetime.datetime.now()
        found_entries = Event.objects.filter(creator=user).filter(entry_query).filter(end__gte=now).order_by('start')
    return object_list(request, template_name='conferencing/conference_search_list.html', queryset=found_entries, extra_context={'query_string': query_string, 'page_title': 'My Conferences Search'}, paginate_by=settings.ITEMS_LIST_PAGINATE_BY)
myconference_search = login_required(myconference_search)

def publicconference_search(request):
    query_string = ''
    found_entries = None
    if ('q' in request.GET) and request.GET['q'].strip():
        query_string = request.GET['q']
        entry_query = get_query(query_string, ['title', 'description'])
        now = datetime.datetime.now()
        found_entries = Event.objects.filter(private=False).filter(entry_query).filter(end__gte=now).order_by('start')
    return object_list(request, template_name='conferencing/conference_search_list.html', queryset=found_entries, extra_context={'query_string': query_string, 'page_title': 'Public Conferences Search'}, paginate_by=settings.ITEMS_LIST_PAGINATE_BY)
    
def get_shared_files(request, urlhash):
    user = request.user
    event_id = int(uri_b64decode(str(urlhash))) / 42
    conference = Event.objects.get(pk=event_id)
    shared_files = SharedFile.objects.filter(conference=conference)
    if request.is_ajax:
        response = direct_to_template(request, 'conferencing/get_shared_files.html', extra_context={'shared_files': shared_files, 'conference': conference})
        setattr(response, "djangologging.suppress_output", True)
        return response
    else:
        raise Http404


@login_required
def create_desktop_recording(request):
    if request.is_ajax:
        if  request.method == 'POST':
            urlhash = request.POST['urlhash']
            guid = request.POST['guid']
            title = request.POST['title']
            title = "%s (Desktop Recording)" % title
            UserFile.objects.get_or_create(title=title, 
                                           uuid=guid, 
                                           user=request.user,
                                           processed = 1,
                                           private = False,
                                           is_active = True,
                                           file_size = 1,
                                           source_type="rec_conf", 
                                           current_type="rec_conf",
                                           duration=100
                                           )
            return HttpResponse("OK")

def make_shared_file(request):
    user = request.user
    if request.is_ajax:
        if request.method == 'POST' and 'file_id' in request.POST:
            file_id = request.POST['file_id']
            urlhash = request.POST['conference']
            event_id = int(uri_b64decode(str(urlhash))) / 42
            conference = Event.objects.get(pk=event_id)
            obj = UserFile.active_objects.get(id=file_id)
            if obj.user == user:
                    SharedFile.objects.get_or_create(owner=user, conference=conference, file=obj)
                    answer = "ok"
            else:
                    answer = "bad"
        response = direct_to_template(request, 'conferencing/make_shared_file.html', extra_context={'answer': answer})
        setattr(response, "djangologging.suppress_output", True)
        return response
    else:
        raise Http404
make_shared_file = login_required(make_shared_file)

def remove_shared_file(request):
    user = request.user
    if request.is_ajax:
        if request.method == 'POST' and 'file_id' in request.POST:
            file_id = request.POST['file_id']
            urlhash = request.POST['conference']
            event_id = int(uri_b64decode(str(urlhash))) / 42
            conference = Event.objects.get(pk=event_id)
            obj = UserFile.active_objects.get(id=file_id)
            if obj.user == user:
                SharedFile.objects.get(conference=conference, file=obj).delete()
                answer = "ok"
            elif conference.creator == user:
                SharedFile.objects.get(conference=conference, file=obj).delete()
                answer = "ok"
            else:
                answer = "bad"
        response = direct_to_template(request, 'conferencing/make_shared_file.html', extra_context={'answer': answer})
        setattr(response, "djangologging.suppress_output", True)
        return response
    else:
        raise Http404
make_shared_file = login_required(make_shared_file)

def netfetch(request):
    if request.is_ajax:
        if request.method == 'POST' and 'nf_phone' in request.POST:
            conference_id = request.POST['conference_id']
            nf_phone = request.POST['nf_phone']
            conf = Event.objects.get(pk=conference_id)
            if conf.creator == request.user:
                nf_url = 'http://voip-ca.prvlb.net/netfetch.php?user_id=%s&conference_id=%s&phone=%s' % (conf.creator.id, conference_id, nf_phone)
                try:
                    fetchy = urlopen(nf_url).read()
                except HTTPError, err:
                    answer = "fail"
                except URLError, err:
                    answer = "fail"
                else:
                    answer = fetchy
            else:
                answer = "fail"
            response = direct_to_template(request, 'conferencing/make_shared_file.html', extra_context={'answer': answer})
            setattr(response, "djangologging.suppress_output", True)
            return response
        else:
            raise Http404
    else:
        raise Http404
netfetch = login_required(netfetch)

@login_required
def recorded_conf(request):
    page_title = unicode(_("Recorded Conferences"))
    qs = request.user.recordedconference_set.all()
    if ('q' in request.GET) and request.GET['q'].strip():
        page_title = unicode(_('Search results'))
        entry_query = get_query(request.GET.get('q', "").strip(), ['title'])
        qs = qs.filter(fms_hostroom__in=UserFile.objects.filter(entry_query).values('uuid'))
    if 'filter' in request.GET and request.GET['filter'].strip():
        if 'type' in request.GET and request.GET['type'].strip():
            visibility_type = request.GET['type'].strip()
            visibility_opts = {"private": "R", "public": "U", "protected": "O"}
            visibility = visibility_opts.get(visibility_type, "U")
            qs = qs.filter(fms_hostroom__in=UserFile.objects.filter(visibility=visibility).values('uuid'))
        if "start" in request.GET and request.GET["start"].strip():
            if request.GET["start"].strip() == "today":
                today_files = UserFile.objects.filter(timestamp__gt=datetime.datetime.now() - datetime.timedelta(days=1),
                                                      timestamp__lt=datetime.datetime.now() + datetime.timedelta(days=1))
                qs = qs.filter(fms_hostroom__in=today_files.values('uuid'))
            if request.GET["start"].strip() == "week":
                qs = qs.filter(fms_hostroom__in=UserFile.objects.filter(timestamp__gt=datetime.datetime.now() - datetime.timedelta(days=7)).values('uuid'))
            if request.GET["start"].strip() == "month":
                qs = qs.filter(fms_hostroom__in=UserFile.objects.filter(timestamp__gt=datetime.datetime.now() - datetime.timedelta(days=31)).values('uuid'))
    query_string = request.GET.get("q", "").strip()
    type = request.GET.get("type", "all")
    date = request.GET.get("start", "all")
    if 'filter' in request.GET:
        is_filtered = True
    else:
        is_filtered = False
    return sorted_object_list(
            request,
            template_name="conferencing/conference_rlist.html",
            queryset=qs,
            paginate_by=settings.ITEMS_LIST_PAGINATE_BY,
            extra_context={
                'subnav_current':'recorded_conf',
                'page_title': page_title,
                'query_string': query_string,
                'is_filtered': is_filtered,
                'type': type,
                'start': date,
                }
            )
    
@login_required    
def direct_to_checkout(request, subs_id):
    "Send directly to the checkout page."
    subs = get_object_or_404(Subscription, pk = subs_id)
    user_subscription = request.user.usersubscription_set.all()[0]
    if user_subscription.subscription.recurrence_unit == 'Y':
        return HttpResponseRedirect(reverse('upgrade_term_plan', args=[subs_id]))
    """ip_address = request.META["REMOTE_ADDR"]
    order = Order.objects.create(ip_address = ip_address, user = request.user)
    cart_item = CartItem.objects.create(cart_id = order.pk, product = subs)
    orderitem = OrderItem.objects.create(quantity = 1, product = subs, order = order, price=subs.price)
    request.session["cart_id"] = cart_item.cart_id
    request.session["cart_item"] = cart_item
    request.session["order_item"] = orderitem
    request.session["order_number"] = order.pk
    return HttpResponseRedirect(reverse("checkout"))"""
    action = None
    user = request.user
    user_subscription = UserSubscription.active_objects.filter(user=user)[0]
    try:
        recurly_subscription_uuid = user_subscription.recurly_subscription_uuid
        recurly_subscription = recurly.Subscription.get(user_subscription.recurly_subscription_uuid)
    except:
        request.user.message_set.create(message='It seems your subscription is not in sync with our payment gateway')
        return HttpResponseRedirect(request.META['HTTP_REFERER'])
    try:
        recurly_subscription.plan_code = subs.name
        recurly_subscription.timeframe= 'renewal'
        recurly_subscription.save()
        if user_subscription.subscription.price > subs.price:
            action = "downgrade"
            sub = subs
            price = subs.price
            expires = user_subscription.expires 
        elif user_subscription.subscription.price < subs.price:
            action = "upgrade"
        if action == "upgrade":
            expires = user_subscription.expires
            user.usersubscription_set.create(user=user,
                                                 subscription=subs,
                                                 expires=expires,
                                                 active=True,
                                                 cancelled=False)
            user_subscription.delete()
            user_subscription = UserSubscription.active_objects.filter(user=user)[0]
            user_subscription.recurly_subscription_uuid = recurly_subscription_uuid
            user_subscription.save()
            sub = user_subscription.subscription
            price = user_subscription.subscription.price
            expires = user_subscription.expires 

        #send_mail("Subscription has been changed to %s." % subs.name, "Your subscription has been changed to %s." % subs.name, "server@netconference.com", [request.user.email])
        request.user.message_set.create(message='Your subscription has been upgraded')
        #return HttpResponseRedirect(reverse("conferencing"))
        return render_to_response("checkout/upgrade_message.html", {"sub":sub, "action":action, "expires":expires, "price":price}, context_instance=RequestContext(request))
    except:
        request.user.message_set.create(message='There seems to be some problem because of which we could not upgrade your subscription. We apologize and are looking into the problem')
        return HttpResponseRedirect(request.META['HTTP_REFERER'])

def upgrade_term_plan(request, subs_id):
    subs = get_object_or_404(Subscription, pk = subs_id)
    user_subscription = request.user.usersubscription_set.all()[0]
    if subs.price < user_subscription.subscription.price:
        request.user.message_set.create(message='Downgrading a term plan subscription is not allowed.')
        return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/')) 
    try:
        recurly_subscription_uuid = user_subscription.recurly_subscription_uuid
        recurly_subscription = recurly.Subscription.get(recurly_subscription_uuid) 
    except:
        request.user.message_set.create(message='It seems your subscription is not in sync with the subscription management system.')
        return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/')) 
    try:
        recurly_subscription.plan_code = subs.name
        recurly_subscription.timeframe= 'now'
        recurly_subscription.save()
        expires = user_subscription.expires
        request.user.usersubscription_set.create(user=request.user,
                                             subscription=subs,
                                             expires=expires,
                                             active=True,
                                             cancelled=False)
        user_subscription.delete()
        user_subscription = UserSubscription.active_objects.filter(user=request.user)[0]
        user_subscription.recurly_subscription_uuid = recurly_subscription_uuid
        user_subscription.save()
        request.user.message_set.create(message='Your subscription has been successfully upgraded')
        return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
    except:
        request.user.message_set.create(message='There seems to be some problem with our subscription managemnet system. Give us some time to rectify it.')
        return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))  
      
@require_POST
def conf_length_pingback(request):
    file_uuid = request.POST.get("soid")
    duration = request.POST.get("duration")
    try:
        conf = RecordedConference.objects.get(fms_hostroom=file_uuid)
    except RecordedConference.DoesNotExist:
        return HttpResponse("Error: Recorded Conference does not exist")
    try:
        uf = UserFile.objects.get(uuid=file_uuid)
        uf.duration = int(duration)
        uf.save()
    except UserFile.DoesNotExist:
        return HttpResponse("Error: User File does not exist")
    conf.duration = int(duration)
    conf.save()
    return HttpResponse("Success")

def instant_conference(request, template='conferencing/new_instant_conferencing.html'):
    acc_info = Account.objects.get(user=request.user)
    if acc_info.timezone:
        n_timezone = acc_info.timezone
    else:
        n_timezone = settings.TIME_ZONE
    title = request.GET['name'] 
    description = title 
    now =  datetime.datetime.now()
    event = Event(start =now,
                  end = now+ datetime.timedelta(hours = 3),
                  timezone = n_timezone,
                  title = title,
                  description=description,
                  private  = False,
                  is_recorded = False,
                  is_recurring = False,
                  creator = request.user,
                  created_on = now,
                  rule =None,
                  end_recurring_period = None,
                  type = "S"
                  )
    event.save()
    
    calendar = get_object_or_404(Calendar, id=1)
    calendar.events.add(event)
    
    return direct_to_template(request,template,{'urlhash':event.get_conference_url()})
#    return direct_to_template(request,'conferencing/conferencingx.html',
#                                      {'screenname':request.user.username,'first_name':request.user.first_name,'last_name':request.user.last_name,'type':4,'email':request.user.email,'userid':request.user.id,'conferenceHash':urlhash,'avatar':custom_avatar})
instant_conference = login_required(instant_conference)

def _check_next_url(next):
    """
    Checks to make sure the next url is not redirecting to another page.
    Basically it is a minimal security check.
    """
    if '://' in next:
        return None
    return next

WEEKDAY_PARSER = {'Mon':'Monday',
                  'Tue':'Tuesday',
                  'Wed':'Wednesday',
                  'Thu':'Thursday',
                  'Fri':'Friday',
                  'Sat':'Saturday',
                  'Sun':'Sunday',
                  }
def getlist_weekday(weekdays):
    result = []
    for day in weekdays:
        result.append(WEEKDAY_PARSER[day])
    return ', '.join(result) or ''

def get_more_timezone(request, template = 'conferencing/more_timezone.html'):
    context = Context({'timezone_more':get_common_timezones()})
    html_option = loader.get_template(template).render(context)
    return HttpResponse(html_option)
get_more_timezone = login_required(get_more_timezone)

def schedule_conference(request, template='conferencing/schedule_conferencing_step1.html'):
    acc_info = Account.objects.get(user=request.user)
    rules = Rule.objects.all()
    if request.method == "GET":
        ''' format timezone user'''
        tz = acc_info.timezone
        now = datetime.datetime.now(pytz.timezone(tz))
        tz = (tz, _("%(tz)s: (UTC%(tz_offset)s)" % {"tz_offset": now.strftime("%z"), "tz": tz}))
        acc_info.timezone = {'value':tz[0], 'title':tz[1].strip()}
        open_folder = request.user.get_conference_rootfolder()
        return direct_to_template(request,template,{'timezone_list':get_priority_timezones(),'acc_info':acc_info,'rules':rules,'open_folder':open_folder})
    else:
        rule = int(request.POST['recurring'])
        is_recur = False
        if rule == 0:
            s_rule = None
        else:
            s_rule =  Rule.objects.get(pk=rule)
            is_recur = True
        weekly = None
        monthly = None    
        if s_rule:
            if s_rule.frequency == "WEEKLY":
                weekly = request.POST['weekdays_date']
                monthly = None
            elif s_rule.frequency == "MONTHLY":
                monthly = request.POST["day_month"]
                weekly = None
                
        title = request.POST['title']
        folder = None
        if 'folder_select' in request.POST:
            folder_id = int(request.POST['folder_select'])
            folder = get_object_or_404(ConferenceFolder, pk=folder_id)
        
        private = False
        description = request.POST['description']
        start_date = dateutil.parser.parse(request.POST['start_date'])
        end_date = dateutil.parser.parse(request.POST['end_date'])
        end_recurring = dateutil.parser.parse(request.POST['period_date'])
        
        end_recurring = end_recurring +datetime.timedelta(minutes=59)
        
        time_zone = request.POST['timezone_value']
        
#        reminder_count= 600
        start = adjust_datetime_to_timezone(start_date, time_zone, settings.TIME_ZONE).replace(tzinfo = None)
        end = adjust_datetime_to_timezone(end_date, time_zone, settings.TIME_ZONE).replace(tzinfo = None)
        end_recurring_period= adjust_datetime_to_timezone(end_recurring, settings.TIME_ZONE, settings.TIME_ZONE).replace(tzinfo = None)
        
        userconfigure = request.user.get_configure_conference()
        
        event = Event(start =start,
                      end = end,
                      timezone = time_zone,
                      title = title,
                      description=description,
                      private  = private,
                      is_recorded = False,
                      is_recurring = is_recur,
                      creator = request.user,
                      rule =s_rule,
                      weekly = weekly,
                      monthly = monthly,
                      created_on = datetime.datetime.now(),
#                      reminder_interval = reminder_count,
                      end_recurring_period = end_recurring_period,
                      type = "S",
                      folder = folder,
                      is_telephone          =   userconfigure.is_telephone,
                      password_required     =   userconfigure.password_required,
                      host_must_present     =   userconfigure.host_must_present,
                      guest_have_approval   =   userconfigure.guest_have_approval,
                      host_control_layout   =   userconfigure.host_control_layout,
                      layout_type           =   userconfigure.layout_type,
                      view_guest_list       =   userconfigure.view_guest_list,
                      broadcast_video       =   userconfigure.broadcast_video,
                      broadcast_audio       =   userconfigure.broadcast_audio,
                      chat_with_host        =   userconfigure.chat_with_host,
                      chat_with_guest       =   userconfigure.chat_with_guest,
                      )
        event.save()
        
        calendar = get_object_or_404(Calendar, id=1)
        calendar.events.add(event)
        
        extra_timezones = request.POST.getlist('show_timezone_value')
        for x_timezone in extra_timezones:
            TimezoneConference(event = event, timezone = x_timezone).save()
        
        time_send = None
        
        next = reverse('configure_conference', args=[uri_b64encode(str(event.id * 42))])
        return HttpResponseRedirect(next)

schedule_conference = login_required(schedule_conference)

def schedule_conference_step2(request, urlhash=None, template="conferencing/schedule_conferencing_step2.html"):
    event =  get_object_from_hash(Event, urlhash)
    acc_info = Account.objects.get(user=request.user)
    rules = Rule.objects.all()
    if request.method == 'GET':
        back_url = reverse('s_event', args=[uri_b64encode(str(event.id * 42))])
        return direct_to_template(request,template,{'timezone_list':get_priority_timezones(),'acc_info':acc_info,'rules':rules,'event':event,'back_url':back_url})
    else:
        
        audio = request.POST['audio']
        if audio == 'voip': is_telephone = False 
        else: is_telephone = True
        
        privacy = request.POST['privacy']
        if privacy == 'public': is_privacy = False 
        else: is_privacy = True
        
        ''' host option '''
        hostoption = request.POST.getlist('hostoption')
        password_access = None
        if 'password_required' in hostoption:
            password_access = request.POST['password_detail']
        if 'host_must_present' in hostoption: host_must_present = True
        else: host_must_present = False
        if 'guest_have_approval' in hostoption: guest_have_approval = True
        else: guest_have_approval = False
        if 'host_control_layout' in hostoption: host_control_layout = True
        else: host_control_layout = False
        
        ''' layout option '''
        layout = request.POST['layout']
        
        
        ''' guest permissions '''
        guest_permission = request.POST.getlist('guest_permission')
        
        if 'view_guest_list' in guest_permission: view_guest_list = True
        else: view_guest_list = False
        if 'broad_video' in guest_permission: broad_video = True
        else: broad_video = False
        if 'broad_audio' in guest_permission: broad_audio = True
        else: broad_audio = False
        if 'chat_with_host' in guest_permission: chat_with_host = True
        else: chat_with_host = False
        if 'chat_with_guest' in guest_permission: chat_with_guest = True
        else: chat_with_guest = False
        
        event.private          =   is_privacy
        event.is_telephone          =   is_telephone
        event.password_required     =   password_access
        event.host_must_present     =   host_must_present
        event.guest_have_approval   =   guest_have_approval
        event.host_control_layout   =   host_control_layout
        event.layout_type           =   layout
        event.view_guest_list       =   view_guest_list
        event.broadcast_video       =   broad_video
        event.broadcast_audio       =   broad_audio
        event.chat_with_host        =   chat_with_host
        event.chat_with_guest       =   chat_with_guest
        event.save()
        if "next" in request.POST:
            next =  request.POST['next']
            request.user.message_set.create(message="Saved conference room setup successful.")
        else:
            next = reverse('s_event', args=[uri_b64encode(str(event.id * 42))])
        return HttpResponseRedirect(next)
schedule_conference_step2 = login_required(schedule_conference_step2)


    
def edit_conference(request, template='conferencing/edit_conferencing_step1.html',urlhash=None):
   instance = get_object_from_hash(Event,urlhash)
   acc_info = Account.objects.get(user=request.user)
   rules = Rule.objects.all()
   e_period = ''
   if instance.end_recurring_period:
       e_period = adjust_datetime_to_timezone(instance.end_recurring_period, settings.TIME_ZONE, instance.timezone).strftime('%Y/%m/%d %H:%M:%S')
       
   weekdays = []
   if instance.weekly:
       weekdays_list = instance.weekly.split(", ")
       weekdays = [i for i in weekdays_list if i ]
   if request.method == "GET":
       
        start_str = adjust_datetime_to_timezone(instance.start, settings.TIME_ZONE,instance.timezone).strftime('%Y/%m/%d %H:%M:%S')
        end_str = adjust_datetime_to_timezone(instance.end, settings.TIME_ZONE,instance.timezone).strftime('%Y/%m/%d %H:%M:%S')
        tz = instance.timezone
        now = datetime.datetime.now(pytz.timezone(tz))
        tz = (tz, _("%(tz)s: (UTC%(tz_offset)s)" % {"tz_offset": now.strftime("%z"), "tz": tz}))
        instance.timezone = {'value':tz[0], 'title':tz[1].strip()}
        back_url = reverse('s_event', args=[uri_b64encode(str(instance.id * 42))])
        
        extra_timezones = TimezoneConference.objects.filter(event = instance)
        
        for extra_tz in extra_timezones:
            tz = extra_tz.timezone
            now = datetime.datetime.now(pytz.timezone(tz))
            tz = (tz, _("%(tz)s: (UTC%(tz_offset)s)" % {"tz_offset": now.strftime("%z"), "tz": tz}))
            extra_tz.timezone = {'value':tz[0], 'title':tz[1].strip()}
            
        similar = None
        if 'similar' in request.GET:
            similar = request.GET['similar']
            
        return direct_to_template(request,template,
                {'timezone_list':get_priority_timezones(),
                 'acc_info':acc_info,
                 'rules':rules,
                 'start': start_str,
                 'end': end_str,
                 'recur_period': e_period,
                  'weekly':weekdays,
                  'monthly':instance.monthly,
                  'reminder_count':instance.reminder_interval/60,
                  'event':instance,
                  'back_url':back_url,
                  'extra_timezones':extra_timezones,
                  'similar':similar
                  })
   else:
       rule = int(request.POST['recurring'])
       is_recur = False
       if rule == 0:
           s_rule = None
       else:
           s_rule =  Rule.objects.get(pk=rule)
           is_recur = True
       weekly = None
       monthly = None    
       if s_rule:
           if s_rule.frequency == "WEEKLY":
               weekly = request.POST['weekdays_date']
               monthly = None
           elif s_rule.frequency == "MONTHLY":
               monthly = request.POST["day_month"]
               weekly = None
           
       title = request.POST['title']
       
       description = request.POST['description']
       start_date = dateutil.parser.parse(request.POST['start_date'])
       end_date = dateutil.parser.parse(request.POST['end_date'])
       end_recurring = dateutil.parser.parse(request.POST['period_date'])
       end_recurring = end_recurring +datetime.timedelta(minutes=59)
       
       time_zone = request.POST['timezone_value']
#       reminder_count = 10*60
#       reminder_type = int(request.POST['reminder_type'])
#       if reminder_type ==1:
#           reminder_count = reminder_count*60*60
#       else:
#           reminder_count = reminder_count*60
       start = adjust_datetime_to_timezone(start_date, time_zone, settings.TIME_ZONE).replace(tzinfo = None)
       end = adjust_datetime_to_timezone(end_date, time_zone, settings.TIME_ZONE).replace(tzinfo = None)
       end_recurring_period= adjust_datetime_to_timezone(end_recurring, time_zone, settings.TIME_ZONE).replace(tzinfo = None)
       folder = None
       if 'folder_select' in request.POST:
           folder_id = int(request.POST['folder_select'])
           folder = get_object_or_404(ConferenceFolder, pk=folder_id)
       
       instance.start =start
       instance.end = end
       instance.timezone = time_zone
       instance.title = title
       instance.description=description
#       instance.private  = private
       instance.is_recorded = False
       instance.is_recurring = is_recur
       instance.creator = request.user
       instance.rule =s_rule
       instance.weekly = weekly
       instance.monthly = monthly
#       instance.reminder_interval = reminder_count
       instance.end_recurring_period = end_recurring_period
       instance.folder = folder
       instance.save()
       
       
       extra_timezones = request.POST.getlist('show_timezone_value')
       exists_timezones = TimezoneConference.objects.filter(event = instance).delete()
       for x_timezone in extra_timezones:
           TimezoneConference(event = instance, timezone = x_timezone).save()
       
       time_send = None
       #========================================================================
       # reminder_qs = ReminderConference.objects.filter(event = instance)
       # if reminder_qs:
       #    reminder = reminder_qs[0]
       # else:
       #    reminder = None
       # if s_rule:
       #    
       #    time_send = start - datetime.timedelta(seconds = reminder_count)
       #    time_zone_str = adjust_datetime_to_timezone(datetime.datetime.now(), time_zone, time_zone).strftime('%z')
       #    time_zone_fast = time_zone_str[:3]+':'+time_zone_str[3:]
       #    if reminder:
       #        reminder.event = instance
       #        reminder.rule  = s_rule
       #        reminder.sender_email = request.user.email
       #        reminder.subject = instance.title
       #        reminder.email_message = ''
       #        reminder.reminder_interval = reminder_count
       #        reminder.time_send = time_send
       #        reminder.weekly = weekly
       #        reminder.monthly = monthly
       #        reminder.start = start
       #        reminder.period = end_recurring_period
       #        reminder.timezone = time_zone_fast
       #        reminder.save()
       #    else:
       #        reminder = ReminderConference(event = instance,
       #                                      rule  = s_rule,
       #                                      sender_email = request.user.email,
       #                                      subject = instance.title,
       #                                      email_message = '',
       #                                      reminder_interval = reminder_count,
       #                                      time_send = time_send,
       #                                      weekly = weekly,
       #                                      monthly = monthly,
       #                                      start = start,
       #                                      period = end_recurring_period,
       #                                      timezone = time_zone_fast
       #                                      )
       #        reminder.save()
       # else:
       #    if reminder:
       #        reminder.delete()
       #========================================================================
       
       if 'similar' in request.POST:
           next = reverse('configure_conference', args=[instance.urlhash])
       else:
           next = reverse('s_event', args=[uri_b64encode(str(instance.id * 42))])
       return HttpResponseRedirect(next)

edit_conference = login_required(edit_conference)

#def overview_room_setup(request, urlhash=None, template="schedule/schedule_roomsetup.html"):
#    event = get_object_from_hash(Event, urlhash)
#    acc_info = Account.objects.get(user=request.user)
#    rules = Rule.objects.all()
#    if request.method == 'GET':
#        back_url = reverse('s_event', args=[uri_b64encode(str(event.id * 42))])
#        return direct_to_template(request,template,{'timezone_list':get_priority_timezones(),'acc_info':acc_info,'rules':rules,'event':event,'back_url':back_url})
#overview_room_setup = login_required(overview_room_setup)    



def overview_room_setup(request, urlhash=None, template="schedule/schedule_roomsetup.html"):
    event = get_object_from_hash(Event, urlhash)
    acc_info = Account.objects.get(user=request.user)
    rules = Rule.objects.all()
    user = request.user
    user_host = event.creator
    is_host = False
    if user == user_host:
        is_host = True
    if request.method == 'GET':
        back_url = reverse('s_event', args=[uri_b64encode(str(event.id * 42))])
        return direct_to_template(request,template,{'timezone_list':get_priority_timezones(),'acc_info':acc_info,'rules':rules,'event':event,'back_url':back_url,'is_host':is_host})
overview_room_setup = login_required(overview_room_setup)    


def similar_conference(request, urlhash=None):
    event = get_object_from_hash(Event,urlhash)
    if request.user != event.creator:
        raise Http404
    reminders = ReminderConference.objects.filter(event = event)
    attendees = Attendees.objects.filter(event = event)
    timezones = TimezoneConference.objects.filter(event = event)
    
    ''' clone new event '''
    event.id = None
    event.save()
    clone_event = event
    
    old_start = clone_event.start
    happen_time = clone_event.start - clone_event.end
    now = datetime.datetime.now()
    new_start = datetime.datetime(now.year, now.month, now.day, old_start.hour,old_start.minute, old_start.second, 0)
    new_end = new_start + happen_time
    
    clone_event.start = new_start
    clone_event.end = new_end
    clone_event.save()
    
    
    for remind in reminders:
        remind.id = None
        remind.event = clone_event
        remind.save()
        remind.re_calculate_fromevent()
        
    
    for attend in attendees:
        attend.id = None
        attend.event = clone_event
        attend.status = 'not_defined'
        attend.save()
    
    for timezone in timezones:
        timezone.id= None
        timezone.event = clone_event
        timezone.save()
    
    next = '%s?similar=%s'%(reverse('s_edit_event', args=[clone_event.urlhash]),urlhash)
    return HttpResponseRedirect(next)
    
similar_conference = login_required(similar_conference)

def send_invite_conference(request, template='conferencing/send_invite_conferencing.html',urlhash=None):
    event = get_object_from_hash(Event, urlhash)
    to_emails = None
    if "emails[]" in request.POST:
        emails = request.POST.getlist('emails[]')
        to_emails = ','.join(emails)
    if to_emails:
        return direct_to_template(request,template,{'event':event, 'urlhash':urlhash ,'to_emails':to_emails})
    else: return direct_to_template(request,template,{'event':event, 'urlhash':urlhash})

send_invite_conference = login_required(send_invite_conference)

def send_mail_invite(request):
    if request.method =='POST':
        data  = json.loads(request.POST['param'])
    else:
        raise Http404
    
    event = get_object_from_hash(Event, data['urlhash'])
    response = HttpResponse(mimetype="application/json")
    user = request.user
    
    to_list = [x for x in list(data[u'to_recipient']) if x ]
    cc_list = [x for x in list(data[u'cc_recipient']) if x ]
    bcc_list = [x for x in list(data[u'bcc_recipient']) if x ]
    
    to_group_list = [int(x) for x in list(data[u'to_group']) if x ]
    cc_group_list = [int(x) for x in list(data[u'cc_group']) if x ]
    bcc_group_list = [int(x) for x in list(data[u'bcc_group']) if x ]
    
    for t_grp in to_group_list:
        t_contacts = Contact.objects.filter(user = user, groups__id = t_grp)
        for t_con in t_contacts:
            if t_con.email not in to_list and is_valid_email(t_con.email):
                to_list.append(t_con.email)
    
    for b_grp in bcc_group_list:
        b_contacts = Contact.objects.filter(user = user, groups__id = b_grp)
        for b_con in b_contacts:
            if b_con.email not in bcc_list  and is_valid_email(b_con.email):
                bcc_list.append(b_con.email)
    
    for c_grp in cc_group_list:
        c_contacts = Contact.objects.filter(user = user, groups__id = c_grp)
        for c_con in c_contacts:
            if c_con.email not in cc_list  and is_valid_email(c_con.email):
                cc_list.append(c_con.email)    
                
    
    recipients = []
    
    for i in to_list:
        if i not in recipients: recipients.append(i)
    for i in cc_list:
        if i not in recipients: recipients.append(i)
    for i in bcc_list:
        if i not in recipients: recipients.append(i)
    for rep_email in recipients:
        exists = Attendees.objects.filter(event = event, email = rep_email)
        if not exists:
            attendee = Attendees(
                          event = event,
                          email = rep_email,
                          status = 'not_defined'
                  )
            attendee.save()
        else:
            attendee = exists[0]
            
        send_email_invitation(attendee = attendee,
                           subject = data['subject'],
                           message = data['email_message'],
                           from_email = user.email)
    email_str = ''
    for e in recipients:
        email_str += '&em=' + e
    next = '%s?%s'%(reverse("review_sent_attendee", args=[data['urlhash']]), email_str)
    return HttpResponse(next)

def review_sent_attendee(request, urlhash=None,template='conferencing/review_sent_attendee.html'):
    event = get_object_from_hash(Event, urlhash)
    email_list = request.GET.getlist('em') 
    return direct_to_template(request,template,{'event':event,'email_list':email_list})
    
def change_timezone(request):
    if request.method !='POST':
        raise Http404
    start_time = dateutil.parser.parse(request.POST['start_time'])
    end_time = dateutil.parser.parse(request.POST['end_time'])
    
    from_timezone = request.POST['from_timezone']
    to_timezone = request.POST['to_timezone']
    start_str = adjust_datetime_to_timezone(start_time, from_timezone, to_timezone).replace(tzinfo = None).strftime('%A, %B %d, %Y$%I:%M %p')
    end_str = adjust_datetime_to_timezone(end_time, from_timezone, to_timezone).replace(tzinfo = None).strftime('%A, %B %d, %Y$%I:%M %p')
    #r_string = str(result.year) +','+str(result.month) +','+str(result.day) +','+str(result.hour) +','+str(result.minute)
    
    return HttpResponse(start_str+'___'+end_str)
change_timezone = login_required(change_timezone)

class ReminderEmail():
    def __init__(self, row):
        self.id = row[0]
        self.event = row[1]
        self.sender = row[3]
        self.subject = row[4]
        self.email_message = row[5]
        if row[7]:
            address = str(row[7]).replace(' ','').split(",")
            to_list = [x for x in address if x ]
        else:
            to_list = []
        self.recipient = to_list

def make_reminder_sql(minutes):
    from django.db import connection, transaction
    cursor = connection.cursor()
    weekday = adjust_datetime_to_timezone(datetime.datetime.now(), settings.TIME_ZONE, settings.TIME_ZONE).replace(tzinfo = None).strftime('%A')
    time_zone_str = adjust_datetime_to_timezone(datetime.datetime.now(), settings.TIME_ZONE, settings.TIME_ZONE).strftime('%z')
    time_zone = time_zone_str[:3]+':'+time_zone_str[3:]
    command = render_to_string("conferencing/sql_reminder_schedule.txt", {
            "minutes": str(minutes),
            'weekday':weekday,
            'time_zone':time_zone,
        }).replace("\n", " ")
    cursor.execute(command)
    transaction.commit_unless_managed()
    row = cursor.fetchall()
    return row 

def build_reminder_email(minutes):
    rows = make_reminder_sql(minutes)
    emails = []
    if rows:
        for r in rows:
            email = ReminderEmail(r)
            emails.append(email)
    return emails

def Reminder_Task(minutes):
    emails = build_reminder_email(minutes)
    for email in emails:
        event = Event.objects.get(pk=email.event)
        recipient = event.get_recipient_list()
#        remind = ReminderConference.objects.get(event = event)
#        remind_str = ', '.join(recipient) or ''
#        remind.ToRecipient = remind_str
#        remind.save()
        send_html_mail(subject = email.subject, message = email.email_message, message_html =email.email_message, from_email = email.sender, recipient_list = recipient, cc_list= None, bcc_list = None)

def changeFlags(request):
    event_id = int(request.GET['event_id'])
    flag = True
    if int(request.GET['flag']) == 0:
        flag = False   
    event_object = get_object_or_404(Event, pk=event_id)
    event_object.is_flag = flag
    event_object.save()
    return HttpResponse('success')

changeFlags = login_required(changeFlags)

@login_required
def delete_conferences(request):
    next = request.POST['next']
    chk_schedules = request.POST.getlist('chk_confs')
    selected_schedules = Event.objects.filter(pk__in = chk_schedules, type = 'S')
    root_trash_folder = request.user.get_conference_trashfolder()
    for schedule in selected_schedules:
        schedule.is_valid = False
        schedule.origin_folder = schedule.folder 
        schedule.folder = root_trash_folder
        schedule.save()
    return HttpResponseRedirect(next)

@login_required
def undo_delete_conferences(request):
    chk_schedules = request.POST.getlist('chk_confs')
    root_folder = request.user.get_conference_rootfolder()
    trash_folder = request.user.get_conference_trashfolder()
    action = next = request.POST['action']
    if action == 'undo_delete': 
        selected_schedules = Event.objects.filter(pk__in = chk_schedules)
        for schedule in selected_schedules:
            schedule.is_valid = True
            if schedule.origin_folder:
                schedule.folder = schedule.origin_folder
            else:
                schedule.folder = root_folder
            schedule.save()
    elif action == 'delete':
        selected_schedules = Event.objects.filter(pk__in = chk_schedules).delete()
        request.user.message_set.create(message=_(u"Conference deleted successful."))
    return HttpResponseRedirect('/conferencing/list/?folder=' + str(trash_folder.id))

def create_entire_jstree(folders):
    entire_jstree = []
    for folder in folders:
        root_folder_dict = create_jstree(folder)
        entire_jstree.append(root_folder_dict)
    return entire_jstree


def create_jstree(folder):
    dic = {}
    dic["data"] = folder.name
    dic["children"] = []
    dic["attr"] = {}
    dic["attr"]["id"] = folder.id
    
    if folder.is_root:
        dic["attr"]["rel"] = "root_folder"
    elif folder.is_root_trash:
        dic["attr"]["rel"] = "trash_folder"
    else:
        dic["attr"]["rel"] = "normal_folder"
    dic["metadata"] = {}
    dic["metadata"]["id"] = folder.id
    children = folder.conferencefolder_set.all().order_by('name')
    for child in children:
        child_dic = create_jstree(child)
        dic["children"].append(child_dic)
    
    return dic

#def getFullTree(request):
#    response = HttpResponse(mimetype="application/json")
#    
#    conference_folders = ConferenceFolder.objects.filter(user=request.user, parent_folder = None).order_by('name')
#    if not conference_folders:
#        folder_root = ConferenceFolder(name='Conferences',user = request.user, parent_folder = None, is_root = True )
#        folder_root.save()
#        conference_folders = ConferenceFolder.objects.filter(user=request.user, parent_folder = None).order_by('name')
#    json_data_list = create_entire_jstree(conference_folders) 
#    json_data = simplejson.dumps(json_data_list) 
#    response.write(json_data)
#    return response

def getFullTree(request):
    response = HttpResponse(mimetype="application/json")
    conference_folders = ConferenceFolder.objects.filter(user=request.user, parent_folder = None, is_root = True)
    trash_folders = ConferenceFolder.objects.filter(user=request.user, parent_folder = None, is_root_trash = True)
    if not conference_folders:
        folder_root = ConferenceFolder(name='Conferences',user = request.user, parent_folder = None, is_root = True )
        folder_root.save()
    if not trash_folders:
        folder_root_trash = ConferenceFolder(name='Trash',user = request.user, parent_folder = None, is_root_trash = True )
        folder_root_trash.save()
    root_folders = ConferenceFolder.objects.filter(user=request.user, parent_folder = None)
    json_data_list = create_entire_jstree(root_folders) 
    json_data = simplejson.dumps(json_data_list) 
    response.write(json_data)
    return response

def getMoveFullTree(request):
    response = HttpResponse(mimetype="application/json")
    conference_folders = ConferenceFolder.objects.filter(user=request.user, parent_folder = None, is_root = True, is_root_trash = False)
    if not conference_folders:
        folder_root = ConferenceFolder(name='Conferences',user = request.user, parent_folder = None, is_root = True, is_root_trash = False )
        folder_root.save()
    root_folders = ConferenceFolder.objects.filter(user=request.user, is_root = True, is_root_trash = False)
    json_data_list = create_entire_jstree(root_folders) 
    json_data = simplejson.dumps(json_data_list) 
    response.write(json_data)
    return response

def processActionTree(request):
    response = HttpResponse(mimetype="application/json")
    if 'operation' in request.GET:
        if request.GET['operation'] == 'create_node':
            result = create_folder_action(request)
        if request.GET['operation'] == 'remove_node':
            result = delete_folder_action(request)
        if request.GET['operation'] == 'rename':
            result = rename_folder_action(request)
        if request.GET['operation'] == 'move_node':
            result = move_folder_action(request)
        
    response.write(simplejson.dumps(result))
    return response

def create_folder_action(request):
    if int(request.GET['id']):
        id = int(request.GET['id'])
        parent_folder = ConferenceFolder.objects.get(pk = id)
    else:
        parent_folder = None
    conference_folder = ConferenceFolder(
                             name = request.GET['title'],
                             user = request.user,
                             parent_folder = parent_folder,
                             )
    conference_folder.save()
    result = {}
    result['status'] = 200
    result['id'] = conference_folder.id
    return result

def delete_folder_action(request):
    id = int(request.GET['id'])
    
    conference_folder = ConferenceFolder.objects.get(pk = id)
    #children = conference_folder.conferencefolder_set.all()
    
    parent_folder_id = None
    if conference_folder.parent_folder:
        parent_folder_id = conference_folder.parent_folder.id
    conference_folder.move_to_trash()
    result = {}
    result['status'] = 200
    if parent_folder_id:
        result['parent'] = parent_folder_id
    return result


def rename_child_action(folder):
    folder.calculate_absolute_path()
    folder.save()
    children = folder.conferencefolder_set.all()
    for child in children:
        rename_child_action(child)

def rename_folder_action(request):
    
    if int(request.GET['id']):
        id = int(request.GET['id'])
        conference_folder = ConferenceFolder.objects.get(pk = id)
        conference_folder.name = request.GET['new_name']
        conference_folder.save()
        rename_child_action(conference_folder)
    result = {}
    result['status'] = 200
    result['id'] = conference_folder.id    
    return result

#def move_conference_to_folder(request):
#    if request.method == 'GET':
#        event_ids_st  = request.GET['event_id']
#        current_folder_id = request.GET['current_folder_id']
#        event_ids = [int(x) for x in event_ids_st.split(',') if x]
#        my_conference = Event.objects.filter(creator = request.user, type = 'G')[0]
#        if my_conference.id in event_ids:
#            event_ids.remove(my_conference.id)
#        if event_ids:
#            folder_id  = int(request.GET['folder_id'])
#            folder_object = get_object_or_404(ConferenceFolder, pk=folder_id)
#            origin_folder = None
#            conference_object = Event.objects.filter(id__in=event_ids).update(folder = folder_object)
#        return HttpResponseRedirect('/conferencing/list/?folder=' + current_folder_id)


def move_conference_to_folder(request):
    if request.method == 'GET':
        event_ids_st  = request.GET['event_id']
        event_ids = [int(x) for x in event_ids_st.split(',') if x]
        my_conference = Event.objects.filter(creator = request.user, type = 'G')[0]
        if my_conference.id in event_ids:
            event_ids.remove(my_conference.id)
        if event_ids:
            folder_id  = int(request.GET['folder_id'])
            folder_object = get_object_or_404(ConferenceFolder, pk=folder_id)
            origin_folder = None
            conference_object = Event.objects.filter(id__in=event_ids).update(folder = folder_object)
        if 'current_folder_id' in request.GET:
            current_folder_id = request.GET['current_folder_id']
            return HttpResponseRedirect('/conferencing/list/?folder=' + current_folder_id)
        else: return HttpResponse("success")



def __user_get_instant_conference(user):
    instant_query = Event.objects.filter(creator=user, type="G")
    instant = None
    if not instant_query:
        acc_info = Account.objects.get(user=user)
        if acc_info.timezone:
            n_timezone = acc_info.timezone
        else:
            n_timezone = settings.TIME_ZONE
        title = "My Conference"
        description = 'A conference that has no start and no end. Available to all users.' 
        now =  datetime.datetime.now()
        event = Event(start = now,
                      end = now+ datetime.timedelta(hours = 3),
                      timezone = n_timezone,
                      title = title,
                      description=description,
                      private  = False,
                      is_recorded = False,
                      is_recurring = False,
                      creator = user,
                      created_on = now,
                      rule =None,
                      end_recurring_period = None,
                      type = "G"
                      )
        event.save()
        
        calendar = get_object_or_404(Calendar, id=1)
        calendar.events.add(event)
        
        instant = event
    else:
        instant = instant_query[0]
    return instant
User.add_to_class('get_instant_conference', __user_get_instant_conference)

@login_required
def empty_trash(request):
    root_trash_folder = ConferenceFolder.objects.filter(is_root_trash = True)[0]
    Event.objects.filter(creator=request.user, folder=root_trash_folder).delete()
    return HttpResponseRedirect('/conferencing/list/?folder=' + str(root_trash_folder.id))
    
def email_invitation(request, template='vmail/delivery/invitation_email.html', urlhash=None):
    instance = get_object_from_hash(Event,urlhash)
    acc_info = Account.objects.get(user=request.user)
    rules = Rule.objects.all()
    e_period = ''
    if instance.end_recurring_period:
        e_period = adjust_datetime_to_timezone(instance.end_recurring_period, settings.TIME_ZONE, instance.timezone).strftime('%Y/%m/%d %H:%M:%S')
        
    weekdays = []
    if instance.weekly:
        weekdays_list = instance.weekly.split(", ")
        weekdays = [i for i in weekdays_list if i ]
    if request.method == "GET":
        
         start_str = adjust_datetime_to_timezone(instance.start, settings.TIME_ZONE,instance.timezone).strftime('%Y/%m/%d %H:%M:%S')
         end_str = adjust_datetime_to_timezone(instance.end, settings.TIME_ZONE,instance.timezone).strftime('%Y/%m/%d %H:%M:%S')
         tz = instance.timezone
         now = datetime.datetime.now(pytz.timezone(tz))
         tz = (tz, _("%(tz)s: (UTC%(tz_offset)s)" % {"tz_offset": now.strftime("%z"), "tz": tz}))
         instance.timezone = {'value':tz[0], 'title':tz[1].strip()}
         back_url = reverse('s_event', args=[uri_b64encode(str(instance.id * 42))])
         
         extra_timezones = TimezoneConference.objects.filter(event = instance)
         
         for extra_tz in extra_timezones:
             tz = extra_tz.timezone
             now = datetime.datetime.now(pytz.timezone(tz))
             tz = (tz, _("%(tz)s: (UTC%(tz_offset)s)" % {"tz_offset": now.strftime("%z"), "tz": tz}))
             extra_tz.timezone = {'value':tz[0], 'title':tz[1].strip()}
         
         return direct_to_template(request,template,
                 {'timezone_list':get_priority_timezones(),
                  'acc_info':acc_info,
                  'rules':rules,
                  'start': start_str,
                  'end': end_str,
                  'recur_period': e_period,
                   'weekly':weekdays,
                   'monthly':instance.monthly,
                   'reminder_count':instance.reminder_interval/60,
                   'event':instance,
                   'back_url':back_url,
                   'extra_timezones':extra_timezones
                   })
@login_required
def add_reminder(request):
    
    event_id = request.GET['event_id']
    user_type = request.GET['user_type']
    reminder_interval = int(request.GET['reminder_interval'])*60
    time_type = request.GET['time_type']
    to_recipient = request.GET['to_recipient'];
    event = get_object_or_404(Event,pk =event_id)
    
    
    frequen = ''
    if event.rule:
        frequen = event.rule.frequency
    subject = render_to_string("conferencing/reminder_subject.txt", {"type":frequen, 'event':event })
    context_template = {
                   'event':event,
                   'timezone':event.timezone,
                   'conftime':event.start,
                   'conf_message':True,
                   'message':'It just reminder',
                   'site': unicode(Site.objects.get_current()),
                   'user':event.creator,
                   'voice':event.is_telephone,
                   }
        
    context = Context(context_template)
    email_message = loader.get_template('vmail/delivery/vmail.html').render(context)
    
    is_guest = False
    user_type = request.GET['user_type']
    if user_type == 'guests':
        is_guest = True
    start = event.start
    #reminder_count = reminder_interval
    
    time_send = start - datetime.timedelta(seconds = reminder_interval)
    time_zone = event.timezone
    time_zone_str = adjust_datetime_to_timezone(datetime.datetime.now(), time_zone, time_zone).strftime('%z')
    
    time_zone = time_zone_str[:3]+':'+time_zone_str[3:]
    
    reminder = ReminderConference()
    reminder.event = event
    reminder.subject = subject
    reminder.reminder_interval = reminder_interval
    reminder.ToRecipient = to_recipient
    reminder.sender_email = request.user.email
    reminder.start = start
    reminder.is_guest = is_guest
    reminder.timezone = time_zone
    reminder.rule = event.rule
    reminder.email_message = email_message
    reminder.time_send = time_send
    reminder.weekly = event.weekly
    reminder.monthly = event.monthly
    reminder.period = event.end_recurring_period
    reminder.save()
    return HttpResponse(reminder.id)

@login_required
def remove_reminder(request):
    reminder_id = int(request.GET['reminder_id'])
    reminder = get_object_or_404(ReminderConference, pk = reminder_id)
    if reminder:
        reminder.delete()
    return HttpResponse("success")

@login_required
def edit_reminder(request):
    reminder_id = int(request.GET['reminder_id'])
    reminder = get_object_or_404(ReminderConference, pk = reminder_id)
    if "reminder_interval" in request.GET:
        reminder_interval = int(request.GET['reminder_interval'])*60
        if reminder:
            reminder.reminder_interval = reminder_interval
            reminder.save()
    if "type" in request.GET:
        type = request.GET['type']
        if type == "Guests":
            reminder.is_guest = True
        else: reminder.is_guest = False
        reminder.save()
    return HttpResponse("success")
    
@login_required
def change_guest_permition(request):
    event_id = request.GET['event_id']
    event = get_object_or_404(Event,pk=event_id)
    type = request.GET['type']
    val = int(request.GET['val'])
    if type == "view_guest_list":
        is_guest_view_guest_list = True
        if val == 0:
            is_guest_view_guest_list = False
        event.guest_view_guest_list = is_guest_view_guest_list
        event.save()
    if type == "send_invitation":
        is_guest_send_invitation = True
        if val == 0:
            is_guest_send_invitation = False
        event.guest_send_invatation = is_guest_send_invitation
        event.save()
    return HttpResponse("success")

@login_required
def resend_invitation(request):
    if request.method == 'GET' and 'event_id' in request.GET:
        event_id = request.GET['event_id']
        event = get_object_or_404(Event, pk=event_id)
        
        user = request.user
        
        recipient = request.GET['recipient']
        try:
            attendees = Attendees.objects.get(event=event, email=recipient)
            attendee = attendees
        except Attendees.MultipleObjectsReturned:
            attendee = attendees[0]
        except Attendees.DoesNotExist:
            attendee = Attendees(event=event, user=user,email=recipient,screen_name="")
            attendee.save()
        
        send_email_invitation(attendee = attendee,
                               subject = event.title,
                               message = None,
                               from_email = user.email)
        
        return HttpResponse("success")
    raise Http404

@login_required
def make_conference_public(request):
    event_id = request.GET['event_id']
    event = get_object_or_404(Event,pk=event_id)
    event.private = False
    event.save()
    return HttpResponse("success")

@login_required
def get_invited(request):
    event_id = request.GET['event_id']
    event = get_object_or_404(Event, pk=event_id)
    
    status = request.GET['status']
    if status == 'all':
        attendees = Attendees.objects.filter(event=event)
    else:
        attendees = Attendees.objects.filter(event=event, status=status)
    emails = []
    for a in attendees:
        temp = {}
        temp['email'] = a.email
        contact = None
        try:
            contact = Contact.objects.get(user = request.user, email = a.email)
        except:
            pass
        if contact:
            temp['name'] = contact.get_contact_name()
        else:
             temp['name'] = None
        emails.append(temp)
    
    context_template = {
                   'emails':emails,
                   }
    context = Context(context_template)
    html_part = loader.get_template('conferencing/table_invited.html').render(context)
    return HttpResponse(html_part)



def update_attendee_status(request):
    if request.method == 'GET' and 'code' in request.GET:
        str_request = urlsafe_b64decode(str(request.GET['code']))
        parameters = str_request.split('&')
        
        urlhash = parameters[0].split('event=')[1]
        email = parameters[1].split('email=')[1]
        status = parameters[2].split('status=')[1]

        event = get_object_from_hash(Event, urlhash)
        
        attendee = Attendees.objects.filter(event=event, email = email)
        if attendee:
            attendee[0].status = status
            attendee[0].save()
            return HttpResponseRedirect('%s?status_response=%s'%(reverse("s_event", args=[urlhash]),status))
    raise Http404

@login_required
def update_configure_default(request):
    if request.method == 'POST':
        configure = request.user.get_configure_conference()
        
        audio = request.POST['audio']
        if audio == 'voip': is_telephone = False 
        else: is_telephone = True
        
        privacy = request.POST['privacy']
        if privacy == 'public': is_privacy = False 
        else: is_privacy = True
        
        ''' host option '''
        hostoption = request.POST.getlist('hostoption')
        password_access = None
        if 'password_required' in hostoption:
            password_access = request.POST['password_detail']
        if 'host_must_present' in hostoption: host_must_present = True
        else: host_must_present = False
        if 'guest_have_approval' in hostoption: guest_have_approval = True
        else: guest_have_approval = False
        if 'host_control_layout' in hostoption: host_control_layout = True
        else: host_control_layout = False
        
        ''' layout option '''
        layout = request.POST['layout']
        
        
        ''' guest permissions '''
        guest_permission = request.POST.getlist('guest_permission')
        
        if 'view_guest_list' in guest_permission: view_guest_list = True
        else: view_guest_list = False
        if 'broad_video' in guest_permission: broad_video = True
        else: broad_video = False
        if 'broad_audio' in guest_permission: broad_audio = True
        else: broad_audio = False
        if 'chat_with_host' in guest_permission: chat_with_host = True
        else: chat_with_host = False
        if 'chat_with_guest' in guest_permission: chat_with_guest = True
        else: chat_with_guest = False
        
        configure.private               =   is_privacy
        configure.is_telephone          =   is_telephone
        configure.password_required     =   password_access
        configure.host_must_present     =   host_must_present
        configure.guest_have_approval   =   guest_have_approval
        configure.host_control_layout   =   host_control_layout
        configure.layout_type           =   layout
        configure.view_guest_list       =   view_guest_list
        configure.broadcast_video       =   broad_video
        configure.broadcast_audio       =   broad_audio
        configure.chat_with_host        =   chat_with_host
        configure.chat_with_guest       =   chat_with_guest
        configure.save()
        return HttpResponse('success')
    raise Http404
    
@login_required
def get_configure_default(request):
    if request.method == 'GET':
        userconfigure = request.user.get_configure_conference()
        
        response = HttpResponse(mimetype="application/json")
        
        result = {}
        result['private']               =   userconfigure.private
        result['is_telephone']          =   userconfigure.is_telephone
        result['password_required']     =   userconfigure.password_required
        result['host_must_present']     =   userconfigure.host_must_present
        result['guest_have_approval']   =   userconfigure.guest_have_approval
        result['host_control_layout']   =   userconfigure.host_control_layout
        result['layout_type']           =   userconfigure.layout_type
        result['view_guest_list']       =   userconfigure.view_guest_list
        result['broadcast_video']       =   userconfigure.broadcast_video
        result['broadcast_audio']       =   userconfigure.broadcast_audio
        result['chat_with_host']        =   userconfigure.chat_with_host
        result['chat_with_guest']       =   userconfigure.chat_with_guest
        
        response.write(json.dumps(result))
        return response
    
    
    
    
    
    

