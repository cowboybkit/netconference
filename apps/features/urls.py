from django.conf.urls.defaults import *
from django.views.generic.simple import direct_to_template

urlpatterns = patterns('',

    url(r'^$', direct_to_template, {"template": "features/newer_webconference.html"}, name="features_home"),
    url(r'^company-files/$', direct_to_template, {"template": "features/newer_company.html"}, name="features_company"),
    url(r'^contacts/$', direct_to_template, {"template": "features/newer_contacts.html"}, name="features_contacts"),
    url(r'^desktop/$', direct_to_template, {"template": "features/newer_desktop.html"}, name="features_desktop"),
    url(r'^files/$', direct_to_template, {"template": "features/newer_files.html"}, name="features_files"),
    url(r'^group-chat/$', direct_to_template, {"template": "features/newer_groupchat.html"}, name="features_groupchat"),
    url(r'^hi-def/$', direct_to_template, {"template": "features/newer_hd.html"}, name="features_hd"),
    url(r'^rebrand/$', direct_to_template, {"template": "features/newer_rebrand.html"}, name="features_rebrand"),
    url(r'^event-scheduler/$', direct_to_template, {"template": "features/newer_scheduler.html"}, name="features_scheduler"),
    url(r'^social-tools/$', direct_to_template, {"template": "features/newer_social.html"}, name="features_social"),
    url(r'^svod/$', direct_to_template, {"template": "features/newer_svod.html"}, name="features_svod"),
    url(r'^telephony/$', direct_to_template, {"template": "features/newer_telephony.html"}, name="features_telephony"),
    url(r'^v-mail/$', direct_to_template, {"template": "features/newer_vmail.html"}, name="features_vmail"),
    url(r'^webconference/$', direct_to_template, {"template": "features/newer_webconference.html"}, name="features_webconference"),
    url(r'^multiuser/$', direct_to_template, {"template": "features/newer_multiuser.html"}, name="features_multiuser"),
    url(r'^recorded/$', direct_to_template, {"template": "features/newer_recorded.html"}, name="features_recorded"),
    url(r'^netsign/$', direct_to_template, {"template": "features/netsign.html"}, name="features_netsign"),
    url(r'^no-software/$', direct_to_template, {"template": "features/newer_software.html"}, name="features_no_software"),
    url(r'^full_voip/$', direct_to_template, {"template": "features/newer_voip.html"}, name="features_full_voip"),
    
)
