import settings
import httplib
import urllib, logging, os
from django.conf import settings
from datetime import date

post_url = settings.AUTHNET_POST_URL

filename = os.path.join(settings.LOG_FOLDER, 'authnet_req_responses.log')


logging.basicConfig(
    level = logging.INFO,
    format = '%(asctime)s %(levelname)s %(message)s',
    filename = filename,
    filemode = 'a'
)
def do_auth_capture(amount='0.00', card_num=None, exp_date=None,
                    card_cvv=None, address=None, city=None, 
                    state=None, zip=None):
    """ function that connects to the Authorize.Net with billing information. Returns a Python list 
    containing the response parameters of sent back by the payment gateway.
    The first item in the response list is the reponse code; the 7th item is the uniquely identifying 
    transaction ID
    
    """  
    delimiter = '|'
    raw_params = { 'x_login': settings.AUTHNET_LOGIN,
                   'x_tran_key': settings.AUTHNET_KEY,
                   'x_type': 'AUTH_CAPTURE',
                   'x_amount': amount,
                   'x_version': '3.1',
                   'x_card_num': card_num,
                   'x_exp_date': exp_date,
                   'x_delim_char': delimiter,
                   'x_relay_response': 'FALSE',
                   'x_delim_data': 'TRUE',
                   'x_card_code': card_cvv,
                   'x_address': address,
                   'x_city': city,
                   'x_state': state,
                   'x_zip': zip
                  }
    params = urllib.urlencode(raw_params)
    headers = { 'content-type':'application/x-www-form-urlencoded',
                'content-length':len(params) }
    post_url = settings.AUTHNET_POST_URL
    post_path = settings.AUTHNET_POST_PATH
    cn = httplib.HTTPSConnection(post_url, httplib.HTTPS_PORT)
    cn.request('POST', post_path, params, headers)
    return cn.getresponse().read().split(delimiter)
    
from django.template.loader import render_to_string

def parse_auth_response(xml):

    from xml.etree import ElementTree
    response_element = ElementTree.fromstring(xml)
    response_children = response_element.getchildren()
    auth_refId = response_children[0].text

    if len(response_children) > 1:
        auth_messages = response_children[1]
    else:
        auth_messages = response_children[0]

    result_code = auth_messages.getchildren()[0]

    isError = result_code.text == 'Error'
    isOk = result_code.text == 'Ok'
    
    sub_id_ele = response_element.getiterator(tag='{AnetApi/xml/v1/schema/AnetApiSchema.xsd}subscriptionId')
    
    try:
        sub_id = sub_id_ele[0].text
    except:
        sub_id = 0
    
    msg_elements = auth_messages.getchildren()[1:]
    messages = []
    for msg in msg_elements:
        messages+=["%s : %s"%(el.tag.title(), el.text) for el in msg.getchildren()]

    return isOk, sub_id, auth_refId, messages



def call_recurring_subscription(refid,amount,cc_num,cc_exp,cc_cvv,fname,lname,sixty_day_trial = False):
    if settings.DEBUG:
        post_url = "apitest.authorize.net"
    else:
        post_url = "api.authorize.net"
    post_path = "/xml/v1/request.api"
    cn = httplib.HTTPSConnection(post_url, httplib.HTTPS_PORT)
    headers = { 'content-type':'text/xml'}

    td = date.today()
    from dateutil.relativedelta import relativedelta
    next_month = td + relativedelta(months=1,days=-1)
    if sixty_day_trial:
        next_month = next_month + relativedelta(months=1,days=-1)
    payload = {
        'merchantname':settings.AUTHNET_LOGIN,
        'key':settings.AUTHNET_KEY,
        
        'refid':refid,
        #'sub_name':subscription_name,

        'start_date':next_month.strftime("%Y-%m-%d"),
        'amount':amount,
        'cc_num':cc_num,
        'cc_exp':cc_exp,
        'cc_cvv':cc_cvv,
        
        'first_name':fname,
        'last_name':lname
    }
    
    xml = render_to_string('checkout/subscribe.xml',payload).encode("utf-8")
    #Log the request sent to the authnet
    logging.info(xml)
    
    cn.request('POST',post_path,xml,headers)

    #Call the authnet API
    auth_response_xml = cn.getresponse().read()
    #Log the response returned from authnet
    logging.info(auth_response_xml)
    #print auth_response_xml

    return parse_auth_response(auth_response_xml)
    
    
def cancel_subscription(transid, refid=None):
    if settings.DEBUG:
        post_url = "apitest.authorize.net"
    else:
        post_url = "api.authorize.net"
    post_path = "/xml/v1/request.api"
    cn = httplib.HTTPSConnection(post_url, httplib.HTTPS_PORT)
    headers = { 'content-type':'text/xml'}

    payload = {
        'merchantname':settings.AUTHNET_LOGIN,
        'key':settings.AUTHNET_KEY,
        
        'refid':refid,
        'transid': transid,
    }
    
    xml = render_to_string('checkout/cancel.xml',payload).encode("utf-8")
    #Log the request sent to the authnet
    logging.info(xml)
    
    cn.request('POST',post_path,xml,headers)

    #Call the authnet API
    auth_response_xml = cn.getresponse().read()
    #Log the response returned from authnet
    logging.info(auth_response_xml)
    #print auth_response_xml

    return parse_auth_response(auth_response_xml)
    
    
