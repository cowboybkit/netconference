import recurly

def create_recurly_account(user, cc_number, cc_cvv, cc_exp_month, cc_exp_year,
                           subscription, company=None, affiliate_creation=False):
    acc_code = user.pk
    username = user.username
    email = user.email
    first_name = user.first_name
    last_name = user.last_name
    user_profile = user.profile_set.get()
    address = user_profile.billing_address_1
    city = user_profile.billing_city
    if user_profile.billing_state:
        state = user_profile.billing_state
    else:
        state = None
    country = user_profile.billing_country
    phone = user_profile.phone
    is_ok = None
    messages = []
    try:
        #import ipdb
        #ipdb.set_trace()
        account = recurly.Account(account_code=acc_code, username=username, email=email,
                                  first_name=first_name, last_name=last_name, company_name=company,
                                  billing_info = recurly.BillingInfo(address1=address, city=city, state=state, country=country,  number=cc_number, verification_value=cc_cvv, month=cc_exp_month, year=cc_exp_year, phone=phone))
        account.save()
        #print "account saved"
        sub = recurly.Subscription()
        sub.plan_code = subscription.name
        sub.currency = 'USD'
        sub.unit_amount_in_cents = int(subscription.price*100)
        if affiliate_creation:
            import datetime
            from dateutil.relativedelta import relativedelta
            two_months = datetime.datetime.today() + relativedelta(months=2)
            sub.trial_ends_at = two_months
        account = recurly.Account.get(acc_code)
        account.subscribe(sub)
        is_ok = True
    except:
        messages.append("There seems to be some problem with your credit card")
        is_ok = False
    return is_ok, messages

def update_billing_info(acc_code, first_name, last_name, address, city, state, country, postal_code, cc_number, cc_cvv, cc_exp_month, cc_exp_year):
    messages = []
    try:
        account = recurly.Account.get(acc_code)
    except recurly.NotFoundError:
        messages.append("You account is not in sync with our subscription management system.Contact the admin.")
        return messages
    try:
        billing_info = account.billing_info
        billing_info.first_name = first_name
        billing_info.last_name = last_name
        billing_info.address1 = address
        billing_info.city = city
        billing_info.state = state
        billing_info.country = country
        billing_info.zip = postal_code
        billing_info.number = cc_number
        billing_info.verification_value = cc_cvv
        billing_info.month = cc_exp_month
        billing_info.year = cc_exp_year
        billing_info.save() 
    except recurly.ValidationError:
        messages.append("There seems to be some problem with your credit card")
        return messages
