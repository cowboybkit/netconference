from django.shortcuts import render_to_response
from django.template import RequestContext
from django.core import urlresolvers
from django.http import HttpResponseRedirect
from django.utils import simplejson
from django.contrib.auth.models import User
from django.conf import settings

from cart import cart
from billing.forms import CardForm
from billing import passkey
from apps.checkout.forms import CheckoutForm
from apps.checkout.models import Order, OrderItem, ShareaSaleLead, ActiveTransaction
from apps.checkout import checkout
from account.models import Account
from subscription.models import UserSubscription

from paypal.standard.forms import PayPalPaymentsForm


import logging
from cart.models import CartItem
from django.core.urlresolvers import reverse
from django.views.decorators.http import require_POST
from django.http import HttpResponse

def show_checkout(request, template_name='checkout/checkout.html'):
    """ checkout form page to collect user shipping and billing information """
    
    cart_item = request.session["cart_item"]
    order_item = request.session["order_item"]
    cart_id = cart_item.pk
    notify_url = request.build_absolute_uri(reverse("checkout_ipn"))
    return_url = request.build_absolute_uri(reverse("checkout_receipt_paypal"))
    cancel_return = request.build_absolute_uri(reverse("checkout"))
    paypal_recepient = getattr(settings, "NC_PAYPAL_EMAIL", "brian@netconference.com") 
    paypal_dict = {
    "cmd": "_xclick-subscriptions",
    "business": paypal_recepient,
    "a1": str(0),#Price during trial 0
    "p1":"1",#One trial period
    "t1":"M", #One month trial
    "a3": str(order_item.price),  # monthly price 
    "p3": "1",                           # duration of each unit (depends on unit)
    "t3": "M",                         # duration unit ("M for Month")
    "src": "1",                        # make payments recur
    "sra": "1",                        # reattempt payment on payment error
    "no_note": "1",                    # remove extra notes (optional)
    "item_name": "Subscription for NetConference: %s" % cart_item.product,
    "notify_url": notify_url,
    "return_url": return_url,
    "cancel_return": cancel_return,
    "custom": order_item.pk
    }
    paypal_form = PayPalPaymentsForm(initial=paypal_dict, button_type="subscribe")
    
    
    cart_url = urlresolvers.reverse('show_cart')
    if cart.is_empty(request):
        # cart_url = urlresolvers.reverse('show_cart')
        return HttpResponseRedirect(cart_url)
    if request.method == 'POST':
        postdata = request.POST.copy()
        form = CheckoutForm(postdata)
        cc_data = {
            'cardholder_name': postdata['billing_name'],
            'card_expire_month': postdata['credit_card_expire_month'],
            'card_expire_year': postdata['credit_card_expire_year'],
            'card_number': postdata['credit_card_number'],
            'card_type': postdata['credit_card_type'],
            'card_cvv': postdata['credit_card_cvv'],
        }
        
        cc = CardForm(cc_data)
        if cc.is_valid() and form.is_valid():
            enc_data = cc_data.copy()
            enc_data.pop('card_number')
            enc_data.pop('card_cvv')
            # convert the POST variables into JSON format
            json_data = simplejson.dumps(enc_data)
            # encrypt the JSON
            encrypted_json = passkey.encrypt(json_data)
            
            # retrieve the encrypted JSON
            #decrypted_json = passkey.decrypt(encrypted_json)
            # convert the decrypted JSON into a dictionary
            #decrypted_data = simplejson.loads(decrypted_json)
            
            card = cc.save(commit=False)
            card.user = request.user
            card.num = postdata['credit_card_number'][-4:]
            card.data = encrypted_json
            creditcard_info = cc.save()
            
            from models import AuthnetData
            authnet_data = AuthnetData(user=request.user)
            
            authnet_data.cc_details = creditcard_info
            authnet_data.save()
            response = checkout.process(request, authnet_data)
            order_number = response.get('order_number',0)
            error_message = response.get('message','')
            if order_number:
                receipt_url = update_subscriber(order_number, request, authnet_data)
                if ActiveTransaction.objects.filter(user=request.user).count():
                    ActiveTransaction.objects.filter(user=request.user).update(is_active=False)
                ActiveTransaction.objects.create(transaction=response["recurring_sub_id"],
                                                 is_active=True,
                                                 user=request.user)
                return HttpResponseRedirect(receipt_url)
        else:
            error_message = u'Correct the errors below:'
    else:
        if request.user.is_authenticated():
            user_profile = request.user.get_profile()
            form = CheckoutForm()
            if request.user.email:
                form.fields['email'].initial = str(request.user.email)
            # if request.user.first_name and request.user.last_name:
            #     form.fields['shipping_name'].initial = "%s %s" % (request.user.first_name, request.user.last_name)
            # if request.user.get_profile().phone:
            #     form.fields['phone'].initial = request.user.get_profile().phone
            # if request.user.get_profile().shipping_name:
            #     form.fields['shipping_name'].initial = request.user.get_profile().shipping_name
            # if request.user.get_profile().shipping_address_1:
            #     form.fields['shipping_address_1'].initial = request.user.get_profile().shipping_address_1
            # if request.user.get_profile().shipping_address_2:
            #     form.fields['shipping_address_2'].initial = request.user.get_profile().shipping_address_2
            # if request.user.get_profile().shipping_city:
            #     form.fields['shipping_city'].initial = request.user.get_profile().shipping_city
            # if request.user.get_profile().shipping_state:
            #     form.fields['shipping_state'].initial = request.user.get_profile().shipping_state
            # if request.user.get_profile().shipping_country:
            #     form.fields['shipping_country'].initial = request.user.get_profile().shipping_country
            # if request.user.get_profile().shipping_zip:
            #     form.fields['shipping_zip'].initial = request.user.get_profile().shipping_zip
            
            if request.user.get_profile().billing_name:
                form.fields['billing_name'].initial = request.user.get_profile().billing_name
            if request.user.get_profile().billing_address_1:
                form.fields['billing_address_1'].initial = request.user.get_profile().billing_address_1
            if request.user.get_profile().billing_address_2:
                form.fields['billing_address_2'].initial = request.user.get_profile().billing_address_2
            if request.user.get_profile().billing_city:
                form.fields['billing_city'].initial = request.user.get_profile().billing_city
            if request.user.get_profile().billing_state:
                form.fields['billing_state'].initial = request.user.get_profile().billing_state
            if request.user.get_profile().billing_country:
                form.fields['billing_country'].initial = request.user.get_profile().billing_country
            if request.user.get_profile().billing_zip:
                form.fields['billing_zip'].initial = request.user.get_profile().billing_zip

        else:
            form = CheckoutForm()
    page_title = 'Checkout'
    return render_to_response(template_name, locals(), context_instance=RequestContext(request))

def update_subscriber(order_number, request, authnet_data = None):
                order = Order.objects.get(pk=order_number)
                mo = OrderItem.objects.get(order=order)
                user_sub = UserSubscription.objects.create(user=request.user, subscription=mo.product)
                
                user_sub.cancelled = 0
                user_sub.is_active = 1
                user_sub.subscription = mo.product
                user_sub.extend()
                user_sub.save()
                
                if authnet_data:
                    authnet_data.user_subscription = user_sub
                    authnet_data.save()

                request.session['order_number'] = order_number
                receipt_url = urlresolvers.reverse('checkout_receipt')
                referer = request.session.get('referring_user',None)
                if referer:
                    from affiliate.utils import add_affiliate
                    add_affiliate(request.user,referring_user=referer,subscription=mo.product)
                return receipt_url


def receipt(request, for_paypal = False, template_name='checkout/receipt.html'):
    """ page displayed with order information after an order has been placed successfully """
    order_number = request.session.get('order_number', '')
    #Ok we dont have this data in session.
    #Try request, this can be present, if this is an ipn notification.
    if not order_number:
        order_number = request.REQUEST.get('order_number', '')
    if order_number:
        order = Order.objects.filter(id=order_number)[0]
        order_items = OrderItem.objects.filter(order=order)
        request.session.pop("order_number", None)
        if for_paypal:
            update_subscriber(order_number, request)
        payload = {"order_number": order_number,
                   "order": order,
                   "order_items": order_items,
                   }
        return render_to_response(template_name, payload, context_instance=RequestContext(request))
    return HttpResponseRedirect(urlresolvers.reverse('show_cart'))

def ipn(request):
    return receipt(request, True,)
    
@require_POST
def shareasale(request):
    tracking = request.POST["tracking"]
    userID = request.POST["userID"]
    ShareaSaleLead.objects.create(tracking=tracking,
                                  userID=userID)
    return HttpResponse("Success")
