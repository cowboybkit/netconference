from django.db import models
# from checkout import google_checkout
from cart import cart
from apps.checkout.models import Order, OrderItem
from apps.checkout.forms import CheckoutForm
from apps.checkout import authnet
import settings,os
import logging

from django.core import urlresolvers
import urllib

filename = os.path.join(settings.LOG_FOLDER, 'subscriptions.log')

logging.basicConfig(
    level = logging.INFO,
    format = '%(asctime)s %(levelname)s %(message)s',
    filename = filename,
    filemode = 'a'
)

def get_checkout_url(request):
    """ returns the URL from the checkout module for cart """
    # use this for Google Checkout API:
    # return google_checkout.get_checkout_url(request)
    
    # use this for our own-site checkout
    return urlresolvers.reverse('checkout')
    
def process(request,authnet_data):
    """ takes a POST request containing valid order data; pings the payment gateway with the billing 
    information and returns a Python dictionary with two entries: 'order_number' and 'message' based on
    the success of the payment processing. An unsuccessful billing will have an order_number of 0 and an error message, 
    and a successful billing with have an order number and an empty string message.
    
    """
    logging.basicConfig(
        level = logging.INFO,
        format = '%(asctime)s %(levelname)s %(message)s',
        filename = filename,
        filemode = 'a'
    )
    logging.info("Processing credit card from shopping cart.")
    # Transaction results
    APPROVED = '1'
    DECLINED = '2'
    ERROR = '3'
    HELD_FOR_REVIEW = '4'
    
    postdata = request.POST.copy()
    card_num = postdata.get('credit_card_number','')
    exp_month = postdata.get('credit_card_expire_month','')
    exp_year = postdata.get('credit_card_expire_year','')
    exp_date = exp_month + exp_year
    cvv = postdata.get('credit_card_cvv','')
    amount = cart.cart_subtotal(request)
    address = postdata.get('billing_address_1')
    city = postdata.get('billing_city')
    state = postdata.get('billing_state')
    zip = postdata.get('billing_zip')
    
    results = {}
    
    response = authnet.do_auth_capture(amount=amount, 
                                       card_num=card_num, 
                                       exp_date=exp_date,
                                       card_cvv=cvv,
                                       address=address,
                                       city=city,
                                       state=state,
                                       zip=zip)
    
    authnet_data.gateway_response = response[0]
    authnet_data.gateway_message = response[3] or ''
    authnet_data.authnet_transaction_id = response[6]
    authnet_data.save()
    
    if response[0] == APPROVED:
        transaction_id = response[6]
        order = create_order(request, transaction_id)
        results = {'order_number': order.id, 'message': u''}
        authnet_data.order = order
        authnet_data.save()
    if response[0] in [DECLINED, ERROR, HELD_FOR_REVIEW]:
        results = {'order_number': 0, 'message': u'Error: %s' % response[3]}
    logging.info(results)
    
    if response[0] == APPROVED:
        recur_response = authnet.call_recurring_subscription(refid='order#%s'%order.hash_uuid,
                                                             amount = amount,
                                                             cc_num=card_num,
                                                             cc_exp=exp_date,
                                                             cc_cvv=cvv,
                                                             fname=request.user.first_name or request.user.username,
                                                             lname=request.user.last_name or request.user.username
                                                             )
            
        authnet_data.gateway_recurring_response = recur_response[0]
        authnet_data.authnet_recur_id = recur_response[1]
        authnet_data.authnet_subscription_id = recur_response[2]
        recurring_messages = " ".join(recur_response[3]) or ""
        authnet_data.authnet_recur_messages = recurring_messages
        authnet_data.save()
        
        results['recurring_successful'] = recur_response[0]
        results['recurring_sub_id'] = recur_response[1]
        #response['recurring_messages'] = recur_response[3]
        
    return results

def create_order(request, transaction_id):
    """ if the POST to the payment gateway successfully billed the customer, create a new order
    containing each CartItem instance, save the order with the transaction ID from the gateway,
    and empty the shopping cart
    
    """
    logging.basicConfig(
        level = logging.INFO,
        format = '%(asctime)s %(levelname)s %(message)s',
        filename = filename,
        filemode = 'a'
    )
    order = Order()
    checkout_form = CheckoutForm(request.POST, instance=order)
    order = checkout_form.save(commit=False)
    
    order.transaction_id = transaction_id
    order.ip_address = request.META.get('REMOTE_ADDR')
    order.user = None
    if request.user.is_authenticated():
        order.user = request.user
    order.status = Order.SUBMITTED
    order.save()
    
    if order.pk:
        """ if the order save succeeded """
        logging.info("Order Saved! Transaction ID: %s" % transaction_id)
        cart_items = cart.get_cart_items(request)
        logging.info(cart_items)
        for ci in cart_items:
            """ create order item for each cart item """
            oi = OrderItem()
            oi.order = order
            oi.quantity = ci.quantity
            oi.price = ci.price  # now using @property
            oi.product = ci.product
            oi.save()
        # all set, clear the cart
        cart.empty_cart(request)
        
        # save profile info for future orders
        if request.user.is_authenticated():
            # from accounts import profile
            from account.models import Account
            # @@@ TO DO - make this update the Pinax profile:
            # profile.set(request)
        
    return order
