from django import template

register = template.Library()

@register.inclusion_tag("checkout/form_table_row.html")
def form_table_row(form_field):
    return {'form_field': form_field }


SUBMITTED = 1
PROCESSED = 2
SHIPPED = 3
CANCELLED = 4


@register.filter(name='order_status')
def order_status(value):
    if value == 1:
        return "submitted"
    elif value == 2:
        return "processed"
    elif value == 3:
        return "shipped"
    elif value == 4:
        return "cancelled"
    else:
        return value
    
        