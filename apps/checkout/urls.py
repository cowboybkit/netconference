from django.conf.urls.defaults import *
from django.conf import settings

urlpatterns = patterns('checkout.views',
    url(r'^$', 'show_checkout', {'template_name': 'checkout/checkout.html', 'SSL': settings.ENABLE_SSL }, 'checkout'),
    url(r'^receipt/$', 'receipt', {'template_name': 'checkout/receipt.html', 'SSL': settings.ENABLE_SSL },'checkout_receipt'),
    url(r'^receipt/paypal/$', 'receipt', 
        {'template_name': 'checkout/receipt.html', 'SSL': settings.ENABLE_SSL, "for_paypal": True }, 
        'checkout_receipt_paypal'),
    url(r'^receipt/$', 'ipn', name='checkout_ipn'),
    url(r'^shareasale/$', 'shareasale', name="shareasale_postback"),
)
