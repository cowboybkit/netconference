from django.db import models
from django import forms
from django.contrib.auth.models import User
from subscription.models import Subscription
from subscription.views import *
import decimal
from netconf_utils.hashing import set_unique_random_value
from django.db.models.signals import post_save

class BaseOrderInfo(models.Model):
    """ base class for storing customer order information """
    class Meta:
        abstract = True
        
    #contact info
    email = models.EmailField(max_length=50)
    phone = models.CharField(max_length=20)
    
    #shipping information
    shipping_name = models.CharField(max_length=50, blank=True)
    shipping_address_1 = models.CharField(max_length=50, blank=True)
    shipping_address_2 = models.CharField(max_length=50, blank=True)
    shipping_city = models.CharField(max_length=50, blank=True)
    shipping_state = models.CharField(max_length=2, blank=True)
    shipping_country = models.CharField(max_length=50, blank=True)
    shipping_zip = models.CharField(max_length=10, blank=True)
    
    #billing information
    billing_name = models.CharField(max_length=50)
    billing_address_1 = models.CharField(max_length=50)
    billing_address_2 = models.CharField(max_length=50, blank=True)
    billing_city = models.CharField(max_length=50)
    billing_state = models.CharField(max_length=30)
    billing_country = models.CharField(max_length=50)
    billing_zip = models.CharField(max_length=10)

from subscription.models import UserSubscription
from billing.models import Card

class AuthnetData(models.Model):
    
    RESPONSE_CHOICES = (('1','APPROVED'),
                        ('2','DECLINED'),
                        ('3','ERROR'),
                        ('4','HELD_FOR_REVIEW'))

    user = models.ForeignKey(User)
    
    cc_details = models.ForeignKey(Card)
    gateway_response = models.CharField(max_length='1',choices=RESPONSE_CHOICES)
    gateway_message = models.TextField(blank=True,null=True)
    authnet_transaction_id = models.CharField(max_length=30,blank=True,null=True)
    order = models.ForeignKey('Order',blank=True,null=True)
    
    gateway_recurring_response = models.CharField(max_length=20,blank=True,null=True)
    authnet_recur_id = models.CharField(max_length=30,blank=True,null=True)
    authnet_subscription_id = models.CharField(max_length=30,blank=True,null=True)
    authnet_recur_messages = models.TextField(blank=True,null=True)
    
    user_subscription = models.ForeignKey(UserSubscription,blank=True,null=True)
    
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    
    def __unicode__(self):
        return "By %s on %s which was %s"%(self.user,self.created,self.gateway_response)
    
    
class Order(BaseOrderInfo):
    """ model class for storing a customer order instance """
    # each individual status
    SUBMITTED = 1
    PROCESSED = 2
    SHIPPED = 3
    CANCELLED = 4
    # set of possible order statuses
    ORDER_STATUSES = ((SUBMITTED,'Submitted'),
                      (PROCESSED,'Processed'),
                      (SHIPPED,'Shipped'),
                      (CANCELLED,'Cancelled'),)
    #order info
    date = models.DateTimeField(auto_now_add=True)
    status = models.IntegerField(choices=ORDER_STATUSES, default=SUBMITTED)
    ip_address = models.IPAddressField()
    last_updated = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(User, null=True)
    transaction_id = models.CharField(max_length=20)
    
    hash_uuid = models.CharField(max_length=16)
    
    class Meta:
        ordering = ('-last_updated', 'status')
    
    # MOVED INTO PARENT BaseOrderInfo CLASS IN Chapter 6
    #contact info
    #email = models.EmailField(max_length=50)
    #phone = models.CharField(max_length=20)
    
    #shipping information
    #shipping_name = models.CharField(max_length=50)
    #shipping_address_1 = models.CharField(max_length=50)
    #shipping_address_2 = models.CharField(max_length=50, blank=True)
    #shipping_city = models.CharField(max_length=50)
    #shipping_state = models.CharField(max_length=2)
    #shipping_country = models.CharField(max_length=50)
    #shipping_zip = models.CharField(max_length=10)
    
    #billing information
    #billing_name = models.CharField(max_length=50)
    #billing_address_1 = models.CharField(max_length=50)
    #billing_address_2 = models.CharField(max_length=50, blank=True)
    #billing_city = models.CharField(max_length=50)
    #billing_state = models.CharField(max_length=2)
    #billing_country = models.CharField(max_length=50)
    #billing_zip = models.CharField(max_length=10)
    
    def __unicode__(self):
        
        #Found this error in a production error email report
        if not self.id:
            self.save()
        return u'Order #' + str(self.id) 
    
    def save(self,*args,**kwargs):
        if not self.hash_uuid:
            set_unique_random_value(self,use_sha=False)
        super(Order,self).save(*args,**kwargs)
        
    @property
    def total(self):
        total = decimal.Decimal('0.00')
        order_items = OrderItem.objects.filter(order=self)
        for item in order_items:
            total += item.total
        return total
    
    @property
    def title(self):
        order_item = OrderItem.objects.get(order=self)
        mt = order_item.product.name
        return mt
    
    @models.permalink
    def get_absolute_url(self):
        return ( 'order_details', (), dict(object_id=str(self.id)) )
    
    @property
    def cc_num(self):
        try:
            return self.user.card_set.all()[0].display_number
        except:
            return 'Not configured'
    
class OrderItem(models.Model):
    """ model class for storing each Subscription instance purchased in each order """
    product = models.ForeignKey(Subscription)
    quantity = models.IntegerField(default=1)
    price = models.DecimalField(max_digits=9,decimal_places=2)
    order = models.ForeignKey(Order)
    
    @property
    def total(self):
        return self.quantity * self.price
    
    @property
    def name(self):
        return self.product.name
    
    @property
    def recurrence_unit(self):
        return self.product.recurrence_unit
    
    @property
    def paypal_ipn(self):
        return self.product.transaction_set.get().ipn
    
    # @property
    # def sku(self):
    #     return self.product.sku
    
    def __unicode__(self):
        return self.product.name
    
    def get_absolute_url(self):
        return self.product.get_absolute_url()
    
    
class ShareaSaleLead(models.Model):
    tracking = models.CharField(max_length=10)
    userID = models.CharField(max_length=10)
    # If shareasale has been pinged
    pinged = models.BooleanField(default=False)

    created_on = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return "%s : %s" %(self.tracking, self.userID)

    
class ActiveTransaction(models.Model):
    transaction = models.CharField(max_length=25)
    is_active = models.BooleanField(default=False)
    user = models.ForeignKey(User)

    created_on = models.DateTimeField(auto_now_add=True)
    modified_on = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return "%s : %s on %s is %s" % (self.user, 
                                        self.transaction,
                                        self.created_on,
                                        self.is_active)

def send_receipt(sender, **kwargs):
    # The placeholder where the mail can be sent to the 
    # user after the mail is sent.
    pass

post_save.connect(send_receipt, sender=ActiveTransaction)

class TransactionDetails(models.Model):
    user = models.ForeignKey(User)
    transaction_id = models.CharField(max_length=50)
    transaction_time = models.DateTimeField(auto_now_add=True)
    amount = models.FloatField()
