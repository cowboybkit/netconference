from django.core.management.base import NoArgsCommand
from checkout.models import ShareaSaleLead
from django.contrib.auth.models import User
from subscription.models import UserSubscription
from django.conf import settings
import datetime, urllib2

class Command(NoArgsCommand):
    help = "Ping Shareasale at the end of the user's trial period."

    def handle_noargs(self, **options):
        one_month_hence = datetime.datetime.today() - datetime.timedelta(days=31)
        leads = ShareaSaleLead.objects.filter(created_on__year = one_month_hence.year,
                                              created_on__month = one_month_hence.month,
                                              created_on__day = one_month_hence.day,
                                              pinged = False)
        for lead in leads:
            try:
                user = User.objects.get(id=lead.tracking)
            except User.DoesNotExist:
                continue
            if user.is_active:
                user_subs = UserSubscription.objects.filter(user=user).exclude(subscription__name='Free').order_by('-expires')
                if not user_subs.count():
                    continue
                user_sub = user_subs[0]
                amount = user_sub.subscription.price
                tracking = lead.tracking
                merchant_id = settings.SHAREASALE_MERCHANT_ID
                user_id = lead.userID
                shareasale_url = "https://shareasale.com/q.cfm?amount=%(amount)s&tracking=%(tracking)s&transtype=sale&merchantID=%(merchant_id)s&userID=%(user_id)s" %({"amount": amount, "tracking": tracking, "merchant_id": merchant_id, "user_id": user_id})
                req = urllib2.Request(shareasale_url)
                fp = urllib2.urlopen(req)
                if fp.getcode() == 200:
                    lead.pinged = True
                    lead.save()
