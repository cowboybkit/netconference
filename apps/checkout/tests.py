from django.test import TestCase, Client
from debug import ipython
from apps.checkout.authnet import call_recurring_subscription, parse_auth_response
import random
from pprint import pprint

class TestAuthorizeNetSubscription(TestCase):
    
    def test_authnet_call(self):
        response = call_recurring_subscription(refid='Test',
                                    amount = 25,
                                    cc_num='4111111111111111',
                                    cc_exp='2011-08',
                                    cc_cvv='8888',
                                    fname='John2',
                                    lname=str(random.random())[2:]
                                    )
        
        pprint(response)
        self.assertTrue(response[0])
        
    
    def test_error_on_duplicate(self):
        random_lname = str(random.random())[:2]
        response = call_recurring_subscription(refid='Test',
                                               amount = 251,
                                               cc_num='4111111111111111',
                                               cc_exp='2013-08',
                                               cc_cvv='8878',
                                               fname='John2%s'%random_lname,
                                               lname=random_lname
                                               )
        
        pprint(response)
        self.assertTrue(response[0])
        response = call_recurring_subscription(refid='Test',
                                               amount = 251,
                                               cc_num='4111111111111111',
                                               cc_exp='2013-08',
                                               cc_cvv='8878',
                                               fname='John2%s'%random_lname,
                                               lname=random_lname
                                               )
        
        pprint(response)
        
        self.assertFalse(response[0])
        