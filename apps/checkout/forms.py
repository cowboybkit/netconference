from django import forms
from netconference.apps.checkout.models import Order
from settings import DEBUG
import datetime
import re
from django.utils.translation import ugettext_lazy as _

def cc_expire_years():
    """ list of years starting with current twelve years into the future """
    current_year = datetime.datetime.now().year
    years = range(current_year, current_year+12)
    return [(str(x),str(x)) for x in years]

def cc_expire_months():
    """ list of tuples containing months of the year for use in credit card form.
    [('01','January'), ('02','February'), ... ]
    
    """
    months = []
    for month in range(1,13):
        if len(str(month)) == 1:
            numeric = '0' + str(month)
        else:
            numeric = str(month)
        months.append((numeric, datetime.date(2009, month, 1).strftime('%B')))
    return months

CARD_TYPES = (('Mastercard','Mastercard'),
             ('Visa','Visa'),
             ('Amex','Amex'),
             ('Discover','Discover'),
             ('PayPal','PayPal'),)

def strip_non_numbers(data):
    """ gets rid of all non-number characters """
    non_numbers = re.compile('\D')
    return non_numbers.sub('', data)
    

def cardLuhnChecksumIsValid(card_number):
    """ checks to make sure that the card passes a Luhn mod-10 checksum 
    Taken from: http://code.activestate.com/recipes/172845/
    
    """
    sum = 0
    num_digits = len(card_number)
    oddeven = num_digits & 1
    for count in range(0, num_digits):
        digit = int(card_number[count])
        if not (( count & 1 ) ^ oddeven ):
            digit = digit * 2
        if digit > 9:
            digit = digit - 9
        sum = sum + digit
    return ( (sum % 10) == 0 )

class CheckoutForm(forms.ModelForm):
    """ checkout form class to collect user billing and shipping information for placing an order """
    def __init__(self, *args, **kwargs):
        super(CheckoutForm, self).__init__(*args, **kwargs)
        # override default attributes
        for field in self.fields:
            self.fields[field].widget.attrs['size'] = '30'
            
        # self.fields['shipping_state'].widget.attrs['size'] = '3'
        # self.fields['shipping_state'].widget.attrs['size'] = '3'
        # self.fields['shipping_zip'].widget.attrs['size'] = '6'
        
        #self.fields['billing_state'].widget.attrs['size'] = '3'
        self.fields['billing_zip'].widget.attrs['size'] = '6'
        
        self.fields['credit_card_type'].widget.attrs['size'] = '1'
        self.fields['credit_card_expire_year'].widget.attrs['size'] = '1'
        self.fields['credit_card_expire_month'].widget.attrs['size'] = '1'
        self.fields['credit_card_cvv'].widget.attrs['size'] = '5'
        
    class Meta:
        model = Order
        exclude = ('status','ip_address','user','transaction_id','shipping_name','shipping_address_1','shipping_address_2','shipping_city','shipping_state','shipping_zip','shipping_country','hash_uuid')
        
    credit_card_number = forms.RegexField(regex="\d{16}")
    credit_card_type = forms.CharField(widget=forms.Select(choices=CARD_TYPES))
    credit_card_expire_month = forms.IntegerField(widget=forms.Select(choices=cc_expire_months()),
                                               initial=datetime.datetime.now().strftime('%m'))
    credit_card_expire_year = forms.IntegerField(widget=forms.Select(choices=cc_expire_years()),
                                              initial=datetime.datetime.now().strftime('%Y'))
    credit_card_cvv = forms.RegexField(regex="\d{3}")
    
    def clean_credit_card_number(self):
        """ validate credit card number if valid per Luhn algorithm """
        cc_number = self.cleaned_data['credit_card_number'].strip()
        if not cc_number:
            raise forms.ValidationError(_("This field is required."))
        stripped_cc_number = strip_non_numbers(cc_number)
        if len(stripped_cc_number) != 16:
            raise forms.ValidationError(_("The credit card number you entered is invalid."))
        if not cardLuhnChecksumIsValid(stripped_cc_number):
            raise forms.ValidationError(_('The credit card number you entered is invalid.'))
        return cc_number

    def clean_credit_card_cvv(self):
        cvv = self.cleaned_data["credit_card_cvv"].strip()
        if not cvv:
            raise forms.ValidationError(_("This field is required."))
        if len(cvv) != 3:
            raise forms.ValidationError(_("CVV should have a maximum of 3 digits."))
        return cvv
        
    def clean_phone(self):
        phone = self.cleaned_data['phone']
        stripped_phone = strip_non_numbers(phone)
        if len(stripped_phone) < 10:
            raise forms.ValidationError('Enter a valid phone number with area code.(e.g. 555-555-5555)')
        return self.cleaned_data['phone']

    def clean_billing_name(self):
        if not self.cleaned_data["billing_name"].strip():
            raise forms.ValidationError(_("This field is required."))
        return self.cleaned_data["billing_name"]

    def clean_billing_address_1(self):
        if not self.cleaned_data["billing_address_1"].strip():
            raise forms.ValidationError(_("This field is required."))
        return self.cleaned_data["billing_address_1"]

    def clean_billing_city(self):
        if not self.cleaned_data["billing_city"].strip():
            raise forms.ValidationError(_("This field is required."))
        return self.cleaned_data["billing_city"]

    def clean_billing_state(self):
        if not self.cleaned_data["billing_state"].strip():
            raise forms.ValidationError(_("This field is required."))
        return self.cleaned_data["billing_state"]

    def clean_billing_zip(self):
        if not self.cleaned_data["billing_zip"].strip():
            raise forms.ValidationError(_("This field is required."))
        return self.cleaned_data["billing_zip"]

    def clean_billing_country(self):
        if not self.cleaned_data["billing_country"].strip():
            raise forms.ValidationError(_("This field is required."))
        return self.cleaned_data["billing_country"]

    def  clean(self):
        """ ignore cc errors is paying through paypal """
        cleaned_data = self.cleaned_data
        if cleaned_data.get("credit_card_type") == "PayPal":
            if 'credit_card_number' in self.errors:
                del self.errors['credit_card_number']
            if 'credit_card_cvv' in self.errors:
                del self.errors['credit_card_cvv']
        else:
            expiry_month = cleaned_data.get("credit_card_expire_month")
            expiry_year = cleaned_data.get("credit_card_expire_year")
            expiry_date = datetime.datetime(expiry_year, expiry_month, 1)
            if expiry_date < datetime.datetime.today():
                raise forms.ValidationError(_("Credit Card is beyond expiry date."))
        return cleaned_data
