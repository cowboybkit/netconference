from django import forms
from django.utils.translation import ugettext_lazy as _

from tribes.models import Tribe

# @@@ we should have auto slugs, even if suggested and overrideable

class TribeForm(forms.ModelForm):
    
    slug = forms.SlugField(label=_("Keywords"), max_length=20,
        help_text = _("a short version of the name consisting only of letters, numbers, underscores and hyphens."),
        error_message = _("Keywords must contain only letters, numbers, underscores and hyphens."))
            
    def clean_slug(self):
        if Tribe.objects.filter(slug__iexact=self.cleaned_data["slug"]).count() > 0:
            raise forms.ValidationError(_("A workgroup already exists with that slug."))
        return self.cleaned_data["slug"].lower()
    
    def clean_name(self):
        name = self.cleaned_data["name"].strip()
        if not name:
            raise forms.ValidationError(_("This field is required."))
        if Tribe.objects.filter(name__iexact=name).count() > 0:
            raise forms.ValidationError(_("A workgroup already exists with that name."))
        return name

    def clean_description(self):
        description = self.cleaned_data["description"].strip()
        if not description:
            raise forms.ValidationError(_("This field is required."))
        return description
    
    class Meta:
        model = Tribe
        fields = ('name', 'slug', 'description')


# @@@ is this the right approach, to have two forms where creation and update fields differ?

class TribeUpdateForm(forms.ModelForm):
    
    def clean_name(self):
        name = self.cleaned_data["name"].strip()
        if not name:
            raise forms.ValidationError(_("This field is required."))
        if Tribe.objects.filter(name__iexact=name).count() > 0:
            if name != self.instance.name:
                raise forms.ValidationError(_("A workgroup already exists with that name."))
        return name
    
    def clean_description(self):
        description = self.cleaned_data["description"].strip()
        if not description:
            raise forms.ValidationError(_("This field is required."))
        return description
    
    class Meta:
        model = Tribe
        fields = ('name', 'description')
