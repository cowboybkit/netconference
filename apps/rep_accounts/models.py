from django.db import models

class RepAccount(models.Model):
    """An associate ID, name and email given to us by ByDesign. Used for account verification."""
    
    rep_number = models.CharField(max_length=50)
    name = models.CharField(max_length=100)
    email = models.CharField(max_length=100, null=True, blank=True)
    assigned = models.BooleanField(default=False)
    
    def __unicode__(self):
        return "%s <%s>" % (self.name, self.email)

    # def __init__(self, arg):
    #     super(ConferencePin, self).__init__()
    #     self.arg = arg
