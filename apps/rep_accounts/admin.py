from django.contrib import admin
from rep_accounts.models import RepAccount

class RepAccountAdmin(admin.ModelAdmin):
    list_display = ('id', 'rep_number', 'name', 'email',)

admin.site.register(RepAccount, RepAccountAdmin)