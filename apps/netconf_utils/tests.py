from django.test import TestCase
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from subscription.models import Subscription

class ConvenientTestCase(TestCase):
    def get(self, url_name, *args, **kwargs):
        return self.client.get(reverse(url_name, args=args, kwargs=kwargs))

    def post(self, url_name, *args, **kwargs):
        data = kwargs.pop("data", None)
        return self.client.post(reverse(url_name, args=args, kwargs=kwargs), data)


class TestUserTestCase(ConvenientTestCase):
    def setUp(self):
        self.test_user = User.objects.create_user('test','lennon@thebeatles.com',password='test')
        self.client.login(username='test',password='test')
        Subscription.objects.create(name='Free',price=0)
        