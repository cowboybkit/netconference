from boto.s3.connection import S3Connection, OrdinaryCallingFormat
from django.conf import settings
import hashlib, datetime, random, os
from uuid import uuid4

def generate_random_file_name(prefix=None):
    if not prefix:
        prefix = ""
    return "%s/%s%s" % (prefix,
                       datetime.datetime.now().strftime("%s"),
                       hashlib.md5(str(random.randint(1, 999999))).hexdigest())

def upload_new_file(bucket=None, key=None, file_name=None, fp=None):
    s3_conn = S3Connection(settings.AWS_ACCESS_KEY_ID, 
                           settings.AWS_SECRET_ACCESS_KEY,
                           calling_format = OrdinaryCallingFormat())
    if not bucket:
        bucket = settings.CID
    bkt = s3_conn.get_bucket(bucket)
    key_obj = None
    if key:
        key_obj = bkt.get_key(key)
    if not key_obj:
        if not file_name:
            file_name = generate_random_file_name()
        key_obj = bkt.new_key(file_name)
    if key_obj:
        key_obj.set_contents_from_file(fp)
    return key_obj

def upload_existing_file(bucket=None, key=None, fp=None):
    return upload_new_file(bucket=bucket, key=key, fp=fp)

def generate_url(key=None, expires=None):
    if not key:
        return None
    if not expires:
        expires = (2037 - datetime.datetime.now().year) * 365 * 24 * 60 * 60
    return key.generate_url(expires)

def upload_and_generate_url(bucket=None, key=None, fp=None, expires=None):
    key_obj = upload_existing_file(bucket, key, fp)
    return generate_url(key_obj, expires)

def get_key(bucket=None, key=None):
    conn = S3Connection(settings.AWS_ACCESS_KEY_ID, 
                        settings.AWS_SECRET_ACCESS_KEY,
                        calling_format = OrdinaryCallingFormat())
    if not bucket:
        bucket = settings.CID
    bkt = conn.get_bucket(bucket)
    return bkt.get_key(key)

def delete_key(bucket=None, key=None):
    conn = S3Connection(settings.AWS_ACCESS_KEY_ID, 
                        settings.AWS_SECRET_ACCESS_KEY,
                        calling_format = OrdinaryCallingFormat())
    if not bucket:
        bucket = settings.CID
    bkt = conn.get_bucket(bucket)
    try:
        bkt.delete_key(key)
        return True
    except:
        return False
    
def delete_folder_key(bucket=None, folder=None):
    conn = S3Connection(settings.AWS_ACCESS_KEY_ID, 
                        settings.AWS_SECRET_ACCESS_KEY,
                        calling_format = OrdinaryCallingFormat())
    if not bucket:
        bucket = settings.CID
    bkt = conn.get_bucket(bucket)
    try:
        for file in bkt.list(prefix=folder):
            bkt.delete_key(file)
        return True
    except:
        return False

def download_and_return_tmp_fp(user_file_obj):
    tmp_dir = getattr(settings, "TMP_DIR", "/tmp/")
    file_path = os.path.join(tmp_dir, user_file_obj.uuid)
    fp = open(file_path, "wb")
    fp.write(user_file_obj.get_file().read())
    fp.close()
    fp = open(file_path, "r")
    return fp


def clone_file(bucket=None, from_name=None, path = None, new_name=None):
    
    s3_conn = S3Connection(settings.AWS_ACCESS_KEY_ID, 
                           settings.AWS_SECRET_ACCESS_KEY,
                           calling_format = OrdinaryCallingFormat())
    if not bucket:
        bucket = settings.CID
    bkt = s3_conn.get_bucket(bucket)
    
    if not new_name:
        new_name = str(uuid4())
    new_obj = bkt.copy_key(path+new_name,bucket, path+from_name)
    new_obj.set_acl('public-read')
    return new_obj

def clone_folder(bucket=None, from_foldername=None, path = None, new_foldername=None):
    
    s3_conn = S3Connection(settings.AWS_ACCESS_KEY_ID, 
                           settings.AWS_SECRET_ACCESS_KEY,
                           calling_format = OrdinaryCallingFormat())
    if not bucket:
        bucket = settings.CID
    bkt = s3_conn.get_bucket(bucket)
    
    if not new_foldername:
        new_foldername = str(uuid4())
    
    for file in bkt.list(prefix=path+from_foldername):
        new_key = file.name.replace(from_foldername, new_foldername)
        new_obj = bkt.copy_key(new_key, bucket, file.name )
        new_obj.set_acl('public-read')
    return True
