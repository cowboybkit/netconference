from boto.exception import S3ResponseError
import boto.s3.connection
import pyPdf.pdf
from pyPdf.generic import NameObject

# Monkey-patch to make django-storages work
boto.s3.connection.S3ResponseError = S3ResponseError

class MP_PageObject(pyPdf.pdf.PageObject):
    def _contentStreamRename(stream, rename, pdf):
        if not rename:
            return stream
        stream = pyPdf.pdf.ContentStream(stream, pdf)
        for operands,operator in stream.operations:
            for i in range(len(operands)):
                try:
                    op = operands[i]
                except (KeyError, IndexError):
                    continue
                if isinstance(op, NameObject):
                    operands[i] = rename.get(op, op)
        return stream
    _contentStreamRename = staticmethod(_contentStreamRename)

pyPdf.pdf.PageObject = MP_PageObject
