from base64 import urlsafe_b64encode, urlsafe_b64decode
from django.shortcuts import get_object_or_404
from django.http import Http404

def uri_b64encode(s):
    return urlsafe_b64encode(s).strip('=')

def uri_b64encode_pk(pk):
    return uri_b64encode(str(pk*42))

def uri_b64decode(s):
    return urlsafe_b64decode(s + '=' * (4 - len(s) % 4))

def get_object_from_hash(model,uhash):
    try:
        object_id = int(uri_b64decode(str(uhash))) / 42
    except:
        raise Http404
    model_object = get_object_or_404(model,pk=object_id)
    return model_object