from threadedcomments.views import comment as comment_orig
from django.contrib.contenttypes.models import ContentType
from django.http import Http404

def comment(request, *args, **kwargs):
    try:
        content_type = ContentType.objects.get(id=kwargs.get("content_type", None))
    except ContentType.DoesNotExist:
        raise Http404
    try:
        obj = content_type.model_class().objects.get(id=kwargs.get("object_id", None))
    except content_type.model_class().DoesNotExist:
        raise Http404
    kwargs["extra_context"] = {"object": obj}
    return comment_orig(request, *args, **kwargs)
