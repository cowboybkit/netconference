from django import template
from django.template import *
# from subscription.models import UserSubscription

register = template.Library()

def comments(context, obj):
    # if (context['user'] != "AnonymousUser"):
    #     user_subscription = UserSubscription.active_objects.get(user=context['user'])
    # else:
    #     user_subscription = None
    return {
        'object': obj,
        'request': context['request'],
        'user': context['user'],
        # 'user_subscription': user_subscription,
    }

register.inclusion_tag('threadedcomments/comments.html', takes_context=True)(comments)