from django.contrib import admin
from bans.models import Ban

class BanAdmin(admin.ModelAdmin):
    list_display = ('user', 'banned_username', 'timestamp', 'session_id')
    list_filter = ['user', 'banned_username']
    search_fields = ['user', 'banned_username']
    date_hierarchy = 'timestamp'

admin.site.register(Ban, BanAdmin)