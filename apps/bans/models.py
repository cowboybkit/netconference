from django.db import models
from django.contrib.auth.models import User

class Ban(models.Model):
    """
    A record of when a moderator bans a user.
    """
    user = models.ForeignKey(User)
    banned_username = models.CharField(max_length=70)
    session_id = models.CharField(max_length=40, null=True, blank=True)
    timestamp = models.DateTimeField(auto_now_add=True, null=True, blank=True)