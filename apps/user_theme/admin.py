from django.contrib import admin
from user_theme.models import UserTheme

class UserThemeAdmin(admin.ModelAdmin):
    list_display = ('user',)
    search_fields = ['user',]

admin.site.register(UserTheme, UserThemeAdmin)

# admin.site.register(UserTheme)