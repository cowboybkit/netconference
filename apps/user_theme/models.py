from django.db import models
from django.contrib.auth.models import User
from netconf_utils.amazon_s3 import get_key, generate_url
# from thumbs import ImageWithThumbsField

class UserTheme(models.Model):
    user = models.ForeignKey(User)
    logo = models.ImageField(upload_to='user_theme/logo', help_text='Please make sure this is 32px high and not longer than 290px.', null=True)
    conference_logo = models.ImageField(upload_to='user_theme/logo', help_text='Recommended Size: 182x22', null=True)

    # def save(self):
    #     # stuff goes here
    #     super(UserTheme, self).save()

    def __unicode__(self):
        return self.user.username

    def _fetch_s3_logo(self, file_name):
        key = get_key(key=file_name)
        return generate_url(key)

    def logo_url(self):
        return self._fetch_s3_logo(self.logo)

    def conference_logo_url(self):
        return self._fetch_s3_logo(self.conference_logo)
