from django import forms
from django.contrib.auth.models import User
from django.forms import widgets
from friends.models import *
from friends_app.models import *

from friends.importer import import_vcards
from django.utils.translation import ugettext_lazy as _

# @@@ move to django-friends when ready

class ImportVCardForm(forms.Form):
    
    vcard_file = forms.FileField(label="vCard File")
    
    def save(self, user):
        imported, total = import_vcards(self.cleaned_data["vcard_file"], user)
        return imported, total
        
class ContactInfoForm(forms.ModelForm):

    name = forms.CharField()
    email = forms.EmailField()
    phone = forms.CharField(required=False)
    qs = ContactGroup.objects.all()
    groups = forms.ModelMultipleChoiceField(queryset=qs, required=False)

    class Meta:
        model = Contact
        fields = ('name','email','phone','groups')
        #exclude = ('user', 'added')

    # def save(self):
    #     name = self.cleaned_data["name"]
    #     email = self.cleaned_data["email"]
    #     return name, email

    def __init__(self, *args, **kwargs):
        # self.user = user
        user = kwargs.pop('user')
        super(ContactInfoForm, self).__init__(*args, **kwargs)
        qs = ContactGroup.objects.filter(owner=user)
        self.fields['groups'].queryset = qs
        # self.fields['groups'] = forms.ModelMultipleChoiceField(
        #     queryset=qs,
        #     widget=widgets.SelectMultiple)

    def clean_name(self):
        name = self.cleaned_data["name"].strip()
        if not name:
            raise forms.ValidationError(_("This field is required."))
        return name

class ContactGroupForm(forms.ModelForm):
    
    name = forms.CharField()
    
    class Meta:
        model = ContactGroup
        fields = ('name',)

    def clean_name(self):
        name = self.cleaned_data["name"]
        if not name.strip():
            raise forms.ValidationError(_("Please enter a valid value."))
        return name.strip()
    
    # def __init__(self, user=None, *args, **kwargs):
    #     self.user = user
    #     super(ContactGroupForm, self).__init__(*args, **kwargs)
