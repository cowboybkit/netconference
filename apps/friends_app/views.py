from django.shortcuts import get_object_or_404, render_to_response
from django.template import RequestContext
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.views.generic.simple import direct_to_template
from django.http import HttpResponseRedirect, HttpResponseForbidden, Http404, HttpResponse
from django.views.generic.list_detail import object_list, object_detail
from django.utils.translation import ugettext_lazy
from friends.models import *
from friends.forms import JoinRequestForm
from friends_app.forms import ImportVCardForm, ContactInfoForm, ContactGroupForm
from account.forms import SignupForm
from friends.importer import import_yahoo, import_google
from files.search import *
from files.views import sorted_object_list
from friends_app.models import ContactGroup
from django.views.decorators.http import require_POST
from django.core.urlresolvers import reverse
from django.contrib.sites.models import RequestSite
import oauth2, urllib, json
from netconf_utils.encode_decode import *
from httplib import HTTP, HTTPResponse
import urllib, urllib2, simplejson
import urlparse
from urlparse import parse_qs
from xml.etree.ElementTree import ElementTree, XMLParser, XML, XMLTreeBuilder
from friends.importer import create_contact_emails
import time
import hashlib
import ybrowserauth
from django.utils.encoding import smart_str
# @@@ if made more generic these could be moved to django-friends proper

_ = lambda x: unicode(ugettext_lazy(x))

def friends(request, form_class=JoinRequestForm,
            template_name="friends_app/invitations.html"):
    if request.method == "POST":
        invitation_id = request.POST.get("invitation", None)
        if request.POST["action"] == "accept":
            try:
                invitation = FriendshipInvitation.objects.get(id=invitation_id)
                if invitation.to_user == request.user:
                    invitation.accept()
                    request.user.message_set.create(message=_("Accepted friendship request from %(from_user)s") % {'from_user': invitation.from_user})
            except FriendshipInvitation.DoesNotExist:
                pass
            join_request_form = form_class()
        elif request.POST["action"] == "invite": # invite to join
            join_request_form = form_class(request.POST)
            if join_request_form.is_valid():
                join_request_form.save(request.user)
                join_request_form = form_class() # @@@
        elif request.POST["action"] == "decline":
            try:
                invitation = FriendshipInvitation.objects.get(id=invitation_id)
                if invitation.to_user == request.user:
                    invitation.decline()
                    request.user.message_set.create(message=_("Declined friendship request from %(from_user)s") % {'from_user': invitation.from_user})
            except FriendshipInvitation.DoesNotExist:
                pass
            join_request_form = form_class()
        elif request.POST["action"] == "cancel":
            try:
                invitation = FriendshipInvitation.objects.get(id=invitation_id)
                if invitation.from_user == request.user:
                    invitation.decline()
                    request.user.message_set.create(message=_("Cancelled friendship request to %(to_user)s") % {"to_user": invitation.to_user})
            except FriendshipInvitation.DoesNotExist:
                pass
            join_request_form = form_class()
    else:
        join_request_form = form_class()

    friends = list(friend_set_for(request.user))
    is_filtered = False
    filter_arg = ""
    if request.method == "GET" and request.GET.get("q"):
        is_filtered = True
        filter_arg = request.GET.get("q").strip()
        friends = [ii for ii in friends if ii.get_full_name().lower().startswith(filter_arg) or ii.username.lower().startswith(filter_arg)]
    invites_received = request.user.invitations_to.invitations().order_by("-sent")
    invites_sent = request.user.invitations_from.invitations().order_by("-sent")
    joins_sent = request.user.join_from.all().order_by("-sent")

    return render_to_response(template_name, {
        "join_request_form": join_request_form,
        "invites_received": invites_received,
        "invites_sent": invites_sent,
        "joins_sent": joins_sent,
        "friends": friends,
        "filter_arg": filter_arg,
        "is_filtered": is_filtered,
        "nav_current":"contacts_nav",
        }, context_instance=RequestContext(request))
friends = login_required(friends)

def friends_add(request, contact_id=None, form_class=ContactInfoForm, success_url=None):
    if not contact_id:
        return HttpResponseForbidden()
    user = request.user
    try:
        friend = User.objects.get(id=contact_id)
    except User.DoesNotExist:
        message = _("User does not exist.")
        return HttpResponse(message)
    obj, created = FriendshipInvitation.objects.get_or_create(from_user=user,
                                                              to_user=friend,
                                                              status="2")
    if not created:
        message = _("%(to_user)s has already been sent an invitation" %{"to_user": obj.to_user})
    else:
        message = _("%(to_user)s has successfully been sent an invitation" %{"to_user": obj.to_user})
    return HttpResponse(message)
friends_add = login_required(require_POST(friends_add))

def accept_join(request, confirmation_key, form_class=SignupForm,
                template_name="account/signup.html"):
    join_invitation = get_object_or_404(JoinInvitation, confirmation_key = confirmation_key.lower())
    if request.user.is_authenticated():
        return render_to_response("account/signup.html", {
            "contact_email": settings.CONTACT_EMAIL,
            }, context_instance=RequestContext(request))
    else:
        form = form_class(initial={"email": join_invitation.contact.email, "confirmation_key": join_invitation.confirmation_key })
        return render_to_response(template_name, {
            "form": form,
            "contact_email": settings.CONTACT_EMAIL,
            }, context_instance=RequestContext(request))

@login_required
def contacts(request, form_class=ImportVCardForm,
             template_name="friends_app/contacts.html",show_affiliate=False,extra_context=None):
    oauth_login = ""
    if request.method == "POST":
        add_contact_form = ContactInfoForm(user=request.user)
        action = request.POST.get("action", None)
        if action == "upload_vcard":
            import_vcard_form = form_class(request.POST, request.FILES)
            if import_vcard_form.is_valid():
                imported, total = import_vcard_form.save(request.user)
                request.user.message_set.create(message=_("%(total)s vCards found, %(imported)s contacts imported.") % {'imported': imported, 'total': total})
                import_vcard_form = ImportVCardForm()
                add_contact_form = ContactInfoForm(user=request.user)
            else:
                request.user.message_set.create(message=unicode(_("vCards Error: Could not import any contacts.")))
        else:
            import_vcard_form = form_class()
            if action == "import_yahoo":
                bbauth_token = request.session.get('bbauth_token')
                del request.session['bbauth_token']
                if bbauth_token:
                    imported, total = import_yahoo(bbauth_token, request.user)
                    request.user.message_set.create(message=_("%(total)s people with email found, %(imported)s contacts imported.") % {'imported': imported, 'total': total})
            if action == "import_google":
                oauth_token = request.session.get("oauth_token")
                del request.session["oauth_token"]
                if oauth_token:
                    imported, total = import_google(oauth_token, request.user)
                    request.user.message_set.create(message=_("%(total)s people with email found, %(imported)s contacts imported.") % {'imported': imported, 'total': total})

    else:
        import_vcard_form = form_class()
        add_contact_form = ContactInfoForm(user=request.user)
        params = {"client_id": settings.OAUTH2_CLIENT_ID,
                  "redirect_uri": "http://%s/oauth2callback/" % RequestSite(request).domain,
                  "scope": "https://www.google.com/m8/feeds/",
                  "response_type": "code"}
        oauth_url = "https://accounts.google.com/o/oauth2/auth"
        oauth_login = "%s?%s" %(oauth_url, urllib.urlencode(params))
    add_form = add_contact_form
    groups = ContactGroup.objects.filter(owner=request.user)

    success_url = request.META.get('HTTP_REFERER', None)
    contacts_qs = request.user.contacts.all().extra(select={'lower_name':'LOWER(name)'}).order_by('lower_name')
    payload = {
        "import_vcard_form": import_vcard_form,
        "bbauth_token": request.session.get('bbauth_token'),
        "add_form": add_form,
        # "group_form": group_form,
        "contacts": contacts_qs,
        'nav_current':'contacts_nav',
        'show_affiliate':show_affiliate,
        "groups": groups,
        "oauth_login": oauth_login,
        "oauth_token": request.session.get("oauth_token"),
        }
    if extra_context:
        payload.update(extra_context)
        
    return sorted_object_list(request, template_name, contacts_qs, payload, paginate_by=settings.ITEMS_LIST_PAGINATE_BY)


@login_required
def contacts(request, urlhash = None, template_name="friends_app/new_contacts.html"):
    group_id = None
    
#    is_duplicate = False
#    contacts = request.user.contacts.all()
#    '''
#    Get different contacts
#    '''
#    different_contacts = []
#    for c in contacts:
#        is_assiged = True
#        for dif_c in different_contacts:
#            if is_equal(c, dif_c):
#                is_assiged = False
#                break
#        if is_assiged:
#            different_contacts.append(c)
#    '''
#    Check if duplicate contacts
#    '''        
#    if len(contacts) > len(different_contacts):
#        is_duplicate = True
        
    groups = ContactGroup.objects.filter(owner=request.user)
    success_url = request.META.get('HTTP_REFERER', None)
    if not urlhash:
       contacts_qs = request.user.contacts.all().extra(select={'lower_name':'LOWER(name)'}).order_by('lower_name')
    else:
        group = get_object_from_hash(ContactGroup,urlhash)
        group_id = int(uri_b64decode(str(urlhash))) / 42
        contacts_qs = request.user.contacts.all().filter(groups__id=group.id).extra(select={'lower_name':'LOWER(name)'}).order_by('lower_name')
    contacts_qs, filter_name, alphabet, per_page  = filter_contacts(request, contacts_qs)
    if 'filter' in request.GET:
        is_filtered = True
    else:
        is_filtered = False
    sort_by = False
    if 'sort_by' in request.GET:
        sort_by = True
    
    helper_contact = False
    if 'helper_contact' in request.session and request.session['helper_contact']:
        helper_contact = True    
        
    payload = {
        "bbauth_token": request.session.get('bbauth_token'),
        "contacts": contacts_qs,
        "groups": groups,
        "filter_name": filter_name,
        "alphabet": alphabet,
        "is_filtered": is_filtered,
        "per_page": per_page,
        "group_id":group_id,
        "sort_by": sort_by,
        'helper_contact':helper_contact
       }
      
    return sorted_object_list(request, template_name, contacts_qs, payload, paginate_by=per_page)

@login_required
def contacts_delete(request, contact_id, success_url=None):
    """
    Deletes a contact from your records.

    You can pass ?next=/foo/bar/ via the url to redirect the user to a different
    page (e.g. `/foo/bar/`) than ``success_url`` after deletion of the message.
    """
    user = request.user
    contact = get_object_or_404(Contact, pk=contact_id)
    if success_url is None:
        success_url = reverse('invitations_contacts')
    if request.GET.has_key('next'):
        success_url = request.GET['next']
    if user == contact.user:
        contact.delete()
    else:
        raise Http404
    # success_url = request.META.get('HTTP_REFERER', None)
    user.message_set.create(message=_(u"Contact successfully deleted."))
    return HttpResponseRedirect(success_url)
contacts_delete = login_required(contacts_delete)

def friends_objects(request, template_name, friends_objects_function, extra_context={}):
    """
    Display friends' objects.

    This view takes a template name and a function. The function should
    take an iterator over users and return an iterator over objects
    belonging to those users. This iterator over objects is then passed
    to the template of the given name as ``object_list``.

    The template is also passed variable defined in ``extra_context``
    which should be a dictionary of variable names to functions taking a
    request object and returning the value for that variable.
    """

    friends = friend_set_for(request.user)

    dictionary = {
        "object_list": friends_objects_function(friends),
    }
    for name, func in extra_context.items():
        dictionary[name] = func(request)

    return render_to_response(template_name, dictionary, context_instance=RequestContext(request))
friends_objects = login_required(friends_objects)


#def contacts_search(request):
#    query_string = ''
#    user = request.user
#    found_entries = Contact.objects.none()
#    if ('q' in request.GET) and request.GET['q']:
#        query_string = request.GET['q'].strip()
#        if query_string:
#            entry_query = get_query(query_string, ['name', 'email'])
#            found_entries = Contact.objects.filter(user=user).filter(entry_query)
#    add_contact_form = ContactInfoForm(request.POST or None, user=request.user or None)
#    add_form = add_contact_form
#    import_vcard_form = ImportVCardForm()
#    show_affiliate = False
#    return sorted_object_list(request, 
#                       template_name='friends_app/contacts.html', 
#                       queryset=found_entries, 
#                       extra_context={'query_string': query_string, 
#                                      "add_form": add_form,
#                                      "import_vcard_form": import_vcard_form,
#                                      "bbauth_token": request.session.get('bbauth_token'),
#                                      "authsub_token": request.session.get('authsub_token'),
#                                      "contacts": found_entries,
#                                      "nav_current": "contacts_nav",
#                                      "show_affiliate": show_affiliate,
#                                      "groups": ContactGroup.objects.filter(owner=user)}, 
#                       paginate_by = settings.ITEMS_LIST_PAGINATE_BY)
#contacts_search = login_required(contacts_search)

def contacts_search(request):
    query_string = ''
    user = request.user
    found_entries = Contact.objects.none()
    if ('q' in request.GET) and request.GET['q']:
        query_string = request.GET['q'].strip()
        if query_string:
            entry_query = get_query(query_string, ['name', 'last_name'])
            found_entries = Contact.objects.filter(user=user).filter(entry_query)
    sort_by = False
    if 'sort_by' in request.GET:
        sort_by = True        
    try:
        per_page = int(request.GET['per_page'])
    except:
        per_page = settings.ITEMS_LIST_PAGINATE_BY
        
    return sorted_object_list(request, 
                       template_name='friends_app/new_contacts.html', 
                       queryset=found_entries, 
                       extra_context={'query_string': query_string, 
                                      "bbauth_token": request.session.get('bbauth_token'),
                                      "authsub_token": request.session.get('authsub_token'),
                                      "contacts": found_entries,
                                      "nav_current": "contacts_nav",
                                      'sort_by':sort_by,
                                      'alphabet':'all',
                                      "per_page": per_page,
                                      "groups": ContactGroup.objects.filter(owner=user)}, 
                       paginate_by = per_page)
contacts_search = login_required(contacts_search)

def filter_contacts(request, contacts):
    filter_name = ''
    alphabet = 'all'
    try:
        per_page = int(request.GET['per_page'])
    except:
        per_page = settings.ITEMS_LIST_PAGINATE_BY
    if 'filter' in request.GET:
        if 'filter_name' in request.GET:
            filter_name = request.GET['filter_name']
        if 'alphabet' in request.GET:
            alphabet = request.GET['alphabet']
            if not alphabet == 'all':
                if filter_name == 'name':
                    contacts = contacts.filter(name__istartswith = alphabet)
                if filter_name == 'last_name':
                    contacts = contacts.filter(last_name__istartswith = alphabet)
    return contacts, filter_name, alphabet, per_page


def community_search(request, template_name="friends_app/community_directory.html"):

    users = User.objects.order_by('username')
    is_filtered = False
    filter_arg = ""
    if request.method == "GET":
        if "q" in request.GET:
            if request.GET.get("q").strip():
                is_filtered = True
                filter_arg = request.GET.get("q").strip()
                users = User.objects.filter(username__istartswith=filter_arg)
            else:
                request.user.message_set.create(message=_("Please enter a search query."))

    return render_to_response(template_name, {
            "users": users,
            "is_filtered": is_filtered,
            "query_string": filter_arg,
        }, context_instance=RequestContext(request))
community_search = login_required(community_search)


def contacts_autocomplete(request):
    if request.user.is_authenticated():
        query_string = ''
        found_entries = None
        if ('q' in request.GET) and request.GET['q'].strip():
            query_string = request.GET['q']
            entry_query = get_query(query_string, ['name', 'email'])
            user = request.user
            found_entries = Contact.objects.filter(user=user).filter(entry_query)
        response = object_list(request, template_name='friends_app/contacts_autocomplete.json', queryset=found_entries, extra_context={'query_string': query_string,})
        setattr(response, "djangologging.suppress_output", True)
        return response
    else:
        response = HttpResponseForbidden()
        setattr(response, "djangologging.suppress_output", True)
        return response
contacts_autocomplete = login_required(contacts_autocomplete)

@login_required
def contact_details(request, urlhash=None):
    contact = get_object_from_hash(Contact,urlhash)
    if request.user == contact.user:
        groups = contact.groups.all()
        return direct_to_template(request, 'friends_app/contact_details.html',{'contact':contact,'groups':groups, 'CLOUDFRONT_DOWNLOAD':settings.CLOUDFRONT_DOWNLOAD })
    else:
        raise Http404
@login_required
def contacts_edit(request, contact_id, form_class=ContactInfoForm, success_url=None):
    user = request.user
    contact = Contact.objects.get(pk=contact_id)
    contacts_form = form_class(instance=contact, user=request.user or None)

    if user == contact.user:
        pass
    else:
        return Http404

    if success_url == None:
        success_url = request.META.get('HTTP_REFERER', None)
    if 'page' in request.GET:
        success_url = success_url + '?page=%d' % request.GET['page']

    if request.method == 'POST' and 'email' in request.POST:
        form_request = form_class(request.POST, user=request.user or None)
        if form_request.is_valid():
            contacts_of_user = Contact.objects.filter(user=request.user)
            emails = [c.email for c in contacts_of_user]
            name = form_request.cleaned_data['name']
            email = form_request.cleaned_data['email']
            if email in emails:
                request.user.message_set.create(message='A contact with this email id already exists.')
                response = HttpResponseRedirect(request.META.get("HTTP_REFERER", "/"))
                return response
            phone = form_request.cleaned_data['phone']
            groups = form_request.cleaned_data['groups']
            contact_form_add = Contact(id=contact_id, name = name, email = email, phone = phone, user = user)
            if user == contact_form_add.user:
                contact_form_add.save()
                contact_form_add.groups.all()
                contact_form_add.groups.clear()
                for g in groups:
                    contact_form_add.groups.add(g)
                if request.is_ajax():
                    request.user.message_set.create(message=unicode(_("%(name)s's contact details were edited!" %{"name": name})))
                    response = HttpResponseRedirect(request.META.get("HTTP_REFERER", "/"))
                    return response
                else:
                    request.user.message_set.create(message=unicode(_("%(name)s's contact details were edited!" %{"name": name})))
                    return HttpResponseRedirect(success_url)
            else:
                return Http404
        else:
            if request.is_ajax():
                return HttpResponse(unicode(_("The following errors were encountered: %s" % form_request.errors)))
            request.user.message_set.create(message=unicode(_("There was an error. Please make sure you entered a name and used a valid email address.")))
            return HttpResponseRedirect(success_url)
    elif request.is_ajax():
        return direct_to_template(request, 'friends_app/contacts_edit_ajax.html', extra_context={'contacts_form': contacts_form, 'contact_name': contact.name, 'contact_email': contact.email, 'contact_id': contact.id, 'success_url': success_url})
    else:
        return direct_to_template(request, 'friends_app/contacts_edit.html', extra_context={'contacts_form': contacts_form, 'user': user, 'contact_name': contact.name, 'success_url': success_url})
contacts_edit = login_required(contacts_edit)

@login_required
def contacts_edit(request, urlhash=None, success_url=None):
    user = request.user
    bucket = settings.AWS_STORAGE_BUCKET_NAME
    contact = get_object_from_hash(Contact,urlhash)
    selected_groups = contact.groups.all()
    id_group = []
    for i in selected_groups:
        id_group.append(i.id)
    groups = ContactGroup.objects.filter(owner=user).exclude(id__in = id_group)
    availabel_groups = groups
    all_groups = ContactGroup.objects.filter(owner=user)
    if user == contact.user:
        pass
    else:
        return Http404
    if request.method == 'GET':
        return direct_to_template(request, 'friends_app/edit_contact.html', extra_context={'contact':contact,'availabel_groups':availabel_groups, 'selected_groups':selected_groups, 'all_groups':all_groups,'bucket':bucket})
    else:
        name = request.POST["name"]
        name = " ".join(name.split())
        last_name = request.POST["last_name"]
        last_name = " ".join(last_name.split())
        emails = request.POST.getlist("emails[]")
        old_contact_email = contact.email
        if old_contact_email != emails[0]:
            '''
            check if duplicate mail contact email
            '''
            try:
                Contact.objects.get(user=user, email=emails[0])
                request.user.message_set.create(message=unicode(_("Contact with email address: %(email)s already exists" %{"email": emails[0]})))
                return HttpResponseRedirect(reverse('invitations_contacts'))
            except Contact.MultipleObjectsReturned:
                request.user.message_set.create(message=unicode(_("Contact with email address: %(email)s already exists" %{"email": emails[0]})))
                return HttpResponseRedirect(reverse('invitations_contacts'))
            except Contact.DoesNotExist:
                email_labels = request.POST.getlist("email_labels[]")
                phones = request.POST.getlist("phones[]")
                phone_labels = request.POST.getlist("phone_labels[]")
                img_type = request.POST["img_type"]
                groups = request.POST.getlist("groups[]")
                contact.name = name
                
                contact.last_name = last_name
                contact.email = emails[0]
                contact.save()
                all_groups = ContactGroup.objects.filter(owner=user)
                for g in all_groups:
                    contact.groups.remove(g)
                
                for g in groups:
                    contact_group = get_object_or_404(ContactGroup, pk=g)
                    contact.groups.add(contact_group)
                
                if img_type == 'rad_img_upload':
                    contact_uuid = request.POST["contact_uuid"] 
                    source_type = request.POST["source_type"]
                    if contact_uuid:
                        contact.uuid = contact_uuid
                        contact.source_type = source_type
                        contact.save()
                else:
                    contact.uuid = None
                    contact.source_type = None
                    contact.save()
                
                old_contact_phones = ContactPhone.objects.filter(contact=contact)
                for phone in  old_contact_phones:
                    phone.delete()
                
                for i in range(0, len(phone_labels)):
                    if phones[i].strip():
                        contact_phone = ContactPhone(contact=contact,label=phone_labels[i], phone = phones[i])
                        contact_phone.save()
                   
                old_contact_emails = ContactEmail.objects.filter(contact=contact)
                for email in  old_contact_emails:
                    email.delete()
                
                if len(emails) > 1:
                    for i in range(1, len(email_labels)):
                        contact_email = ContactEmail(contact=contact,label=email_labels[i], email = emails[i])
                        contact_email.save()
                return HttpResponseRedirect(reverse("contact_details",args=[contact.urlhash()]))
        else:
            email_labels = request.POST.getlist("email_labels[]")
            phones = request.POST.getlist("phones[]")
            phone_labels = request.POST.getlist("phone_labels[]")
            img_type = request.POST["img_type"]
            groups = request.POST.getlist("groups[]")
            contact.name = name
            contact.last_name = last_name
            contact.email = emails[0]
            contact.save()
            all_groups = ContactGroup.objects.filter(owner=user)
            for g in all_groups:
                contact.groups.remove(g)
                
            for g in groups:
                contact_group = get_object_or_404(ContactGroup, pk=g)
                contact.groups.add(contact_group)
            if img_type == 'rad_img_upload':
                contact_uuid = request.POST["contact_uuid"] 
                source_type = request.POST["source_type"]
                if contact_uuid:
                    contact.uuid = contact_uuid
                    contact.source_type = source_type
                    contact.save()
            else:
                contact.uuid = None
                contact.source_type = None
                contact.save()
            
            old_contact_phones = ContactPhone.objects.filter(contact=contact)
            for phone in  old_contact_phones:
                phone.delete()
            
            for i in range(0, len(phone_labels)):
                if phones[i].strip():
                    contact_phone = ContactPhone(contact=contact,label=phone_labels[i], phone = phones[i])
                    contact_phone.save()
               
            old_contact_emails = ContactEmail.objects.filter(contact=contact)
            for email in  old_contact_emails:
                email.delete()
            
            if len(emails) > 1:
                for i in range(1, len(email_labels)):
                    contact_email = ContactEmail(contact=contact,label=email_labels[i], email = emails[i])
                    contact_email.save()
            return HttpResponseRedirect(reverse("contact_details",args=[contact.urlhash()]))
         
def contacts_add(request, form_class=ContactInfoForm, success_url=None):
    user = request.user
    contacts_form = form_class(user=user or None)
    if request.method == 'POST' and 'email' in request.POST:
        form_request = form_class(request.POST, user=request.user or None)
        if form_request.is_valid():
            name = form_request.cleaned_data['name']
            email = form_request.cleaned_data['email']
            phone = form_request.cleaned_data['phone']
            groups = form_request.cleaned_data['groups']
            try:
                Contact.objects.get(user=user, email=email)
                request.user.message_set.create(message=unicode(_("Contact with email address: %(email)s already exists" %{"email": email})))
                return HttpResponseRedirect(reverse('invitations_contacts'))
            except Contact.MultipleObjectsReturned:
                request.user.message_set.create(message=unicode(_("Contact with email address: %(email)s already exists" %{"email": email})))
                return HttpResponseRedirect(reverse('invitations_contacts'))
            except Contact.DoesNotExist:
                contact_form_add = Contact(user = user, name = name, email = email, phone = phone)
            contact_form_add.save()
            contact_form_add.groups.all()
            contact_form_add.groups.clear()
            for g in groups:
                contact_form_add.groups.add(g)
            if request.is_ajax():
                response = HttpResponseRedirect(reverse('invitations_contacts'))
                setattr(response, "djangologging.suppress_output", True)
                return response
            else:
                request.user.message_set.create(message=unicode(_("%(name)s's contact details were saved!" %{"name": name})))
                return HttpResponseRedirect(reverse('invitations_contacts'))
        else:
            contacts_form = form_class(request.POST, user=user)
    return direct_to_template(request, 'friends_app/contacts_add.html', extra_context={'contacts_form': contacts_form, 'user': user})
contacts_add = login_required(contacts_add)


@login_required
def create_contact(request):
    user = request.user
    if request.method == 'GET':
        bucket = settings.AWS_STORAGE_BUCKET_NAME
        groups = ContactGroup.objects.filter(owner=user)
        return direct_to_template(request, 'friends_app/create_contact.html',{'groups':groups,'user':user,'bucket':bucket})
    else:
        emails = request.POST.getlist("emails[]")
        '''
        check if duplicate mail contact email
        '''
        try:
            Contact.objects.get(user=user, email=emails[0])
            request.user.message_set.create(message=unicode(_("Contact with email address: %(email)s already exists" %{"email": emails[0]})))
            return HttpResponseRedirect(reverse('invitations_contacts'))
        except Contact.MultipleObjectsReturned:
            request.user.message_set.create(message=unicode(_("Contact with email address: %(email)s already exists" %{"email": emails[0]})))
            return HttpResponseRedirect(reverse('invitations_contacts'))
        except Contact.DoesNotExist:
            name = request.POST["name"]
            name = " ".join(name.split())
            last_name = request.POST["last_name"]
            last_name = " ".join(last_name.split())
            email_labels = request.POST.getlist("email_labels[]")
            phones = request.POST.getlist("phones[]")
            phone_labels = request.POST.getlist("phone_labels[]")
            img_type = request.POST["img_type"]
            if img_type == 'rad_img_upload':
                contact_uuid = request.POST["contact_uuid"]
                source_type = request.POST["source_type"]
                if contact_uuid:
                    contact = Contact(user = user, name = name, last_name=last_name,email=emails[0], uuid=contact_uuid, source_type=source_type)
                    contact.save()
                else:
                    contact = Contact(user = user, name = name, last_name=last_name, email=emails[0])
                    contact.save()
            else:
                contact = Contact(user = user, name = name, last_name=last_name, email=emails[0])
                contact.save()
            group = request.POST["contact_group"]
            if not group == 'no_group':
                contact.groups.add(group)
            for i in range(0, len(phones)):
                if phones[i].strip():
                    contact_phone = ContactPhone(contact=contact,label=phone_labels[i], phone = phones[i])
                    contact_phone.save()
            if len(email_labels) > 1:
                for i in range(1, len(email_labels)):
                    contact_email = ContactEmail(contact=contact,label=email_labels[i], email = emails[i])
                    contact_email.save()
            return HttpResponseRedirect(reverse("contact_details",args=[contact.urlhash()]))




def groups(request, form_class=ContactGroupForm, template_name="friends_app/groups.html"):
    user = request.user
    add_group_form = form_class()
    groups = ContactGroup.objects.filter(owner=user)
    return render_to_response(template_name, {
        "group_form": add_group_form,
        "groups": groups,
        'nav_current':'contacts_nav'
        }, context_instance=RequestContext(request))
    # return object_list(request, template_name='friends_app/groups.html', queryset=groups, extra_context={'group_form': add_group_form, 'user': user})
groups = login_required(groups)

def groups_add(request, form_class=ContactGroupForm, success_url=None):
    user = request.user
    group_form = form_class()
    if request.method == 'POST' and 'name' in request.POST:
        group_form = form_class(request.POST)
        if group_form.is_valid():
            name = group_form.cleaned_data["name"]
            try:
                ContactGroup.objects.get(owner=user, name=name)
                request.user.message_set.create(message=unicode(_("You already have a group with the name %(name)s" %{"name": name})))
            except ContactGroup.DoesNotExist:
                ContactGroup(owner=user, name=name).save()
                request.user.message_set.create(message=unicode(_("The group %(name)s was saved!" %{"name": name})))
        else:
            request.user.message_set.create(message=group_form.errors["name"].as_text())
        return HttpResponseRedirect(reverse('contact_groups'))
    return direct_to_template(request, 'friends_app/groups.html', extra_context={'group_form': group_form, 'user': user})
groups_add = login_required(groups_add)

'''
Rename group contact
'''
def group_rename(request):
    user = request.user
    action = request.POST['action']
    if action == 'rename':
        group_id = request.POST['group_id']
        group_name = request.POST['group_name']
        contact_group = get_object_or_404(ContactGroup, pk=group_id)
        try:
            ContactGroup.objects.get(owner=user, name=group_name)
            request.user.message_set.create(message=unicode(_("You already have a group with the name %(name)s" %{"name": group_name})))
        except ContactGroup.DoesNotExist:
            contact_group.name = group_name
            contact_group.save()
            request.user.message_set.create(message=_(u"Contact Group successfully modified."))
        return HttpResponseRedirect(reverse("invitations_contacts"))
    elif action == 'delete':
        group_id = request.POST['group_id']
        contact_group = get_object_or_404(ContactGroup, pk=group_id)
        contact_group.delete()
        request.user.message_set.create(message=_(u"Contact Group successfully deleted."))
        return HttpResponseRedirect(reverse("invitations_contacts"))
    elif action == 'create':
        group_name = request.POST['group_name']
        try:
            ContactGroup.objects.get(owner=user, name=group_name)
            request.user.message_set.create(message=unicode(_("You already have a group with the name %(name)s" %{"name": group_name})))
        except ContactGroup.DoesNotExist:
            contact_group = ContactGroup(owner=user, name=group_name)
            contact_group.save()
        request.user.message_set.create(message=unicode(_("The group %(name)s was saved!" %{"name": group_name})))
        return HttpResponseRedirect(reverse("invitations_contacts"))
def groups_delete(request, group_id, success_url=None):
    """
    Deletes a contact group and its relations from your records.

    You can pass ?next=/foo/bar/ via the url to redirect the user to a different
    page (e.g. `/foo/bar/`) than ``success_url`` after deletion of the message.
    """
    user = request.user
    group = get_object_or_404(ContactGroup, pk=group_id)
    if success_url is None:
        success_url = reverse('contact_groups')
    # if request.GET.has_key('next'):
    #     success_url = request.GET['next']
    if user == group.owner:
        # group.contacts_set.all()
        # group.contacts_set.clear()
        group.delete()
        return HttpResponseRedirect(success_url)
    else:
        raise Http404
    success_url = request.META.get('HTTP_REFERER', None)
    user.message_set.create(message=_(u"Contact Group successfully deleted."))
    return HttpResponseRedirect(success_url)
groups_delete = login_required(groups_delete)

def friends_search(request, form_class=JoinRequestForm,
            template_name="friends_app/invitations.html"):
    return friends(request)
friends_search = login_required(friends_search)

'''
check if two contacts is duplicate
'''
def is_equal(a, b):
    if(hasattr(a, "name") and hasattr(a, "last_name") and hasattr(b, "name") and hasattr(b, "last_name")):
        if ((not a.name) and (not a.last_name)) or ((not b.name) and (not b.last_name)):
            return False
        return (smart_str(a.name).lower() == smart_str(b.name).lower() and smart_str(a.last_name).lower() == smart_str(b.last_name).lower())
    return False

def get_different_contact_emails(email_contacts):
    different_email_contacts = []
    for c in email_contacts:
            is_assiged = True
            for dif_c in different_email_contacts:
                if c.label.lower() == dif_c.label.lower() and c.email.lower() == dif_c.email.lower():
                    is_assiged = False
                    break
            if is_assiged:
                different_email_contacts.append(c)
    return different_email_contacts

def get_different_contact_phones(phone_contacts):
    different_phone_contacts = []
    for c in phone_contacts:
            is_assiged = True
            for dif_c in different_phone_contacts:
                if c.label.lower() == dif_c.label.lower() and c.phone.lower() == dif_c.phone.lower():
                    is_assiged = False
                    break
            if is_assiged:
                different_phone_contacts.append(c)
    return different_phone_contacts

def get_different_main_emails(contacts):
    different_main_emails = []
    for c in contacts:
            is_assiged = True
            for dif_c in different_main_emails:
                if c.email.lower() == dif_c.email.lower():
                    is_assiged = False
                    break
            if is_assiged:
                different_main_emails.append(c)
    return different_main_emails

def get_different_dic_main_emails(main_emails):
    different_main_emails = []
    for c in main_emails:
            is_assiged = True
            for dif_c in different_main_emails:
                if (c['label']).lower() == (dif_c['label']).lower() and (c['email']).lower() == (dif_c['email']).lower():
                    is_assiged = False
                    break
            if is_assiged:
                different_main_emails.append(c)
    return different_main_emails

def get_different_dic_phones(phones):
    different_phones = []
    for c in phones:
            is_assiged = True
            for dif_c in different_phones:
                if (c['label']).lower() == (dif_c['label']).lower() and (c['phone']).lower() == (dif_c['phone']).lower():
                    is_assiged = False
                    break
            if is_assiged:
                different_phones.append(c)
    return different_phones

@login_required
def find_merge_contacts(request):
    if request.method == "GET":
        is_duplicate = False
        contacts = request.user.contacts.all()
        '''
        Get different contacts
        '''
        different_contacts = []
        for c in contacts:
            is_assiged = True
            for dif_c in different_contacts:
                if is_equal(c, dif_c):
                    is_assiged = False
                    break
            if is_assiged:
                different_contacts.append(c)
        '''
        Check if duplicate contacts
        '''        
        if len(contacts) > len(different_contacts):
            is_duplicate = True
        if is_duplicate:
            '''
            Get duplicate contacts
            '''
            dups = []
            for dif_c in different_contacts:
                count = 0
                for c in contacts:
                    if is_equal(dif_c, c):
                        count = count + 1
                        if count > 1:
                            break
                if count > 1:
                    dups.append(dif_c)
            '''
            Get contacts are merged
            '''        
            merge_contacts = []
            
            for contact_merged in dups:
                count = 0
                selected_contacts = []
                for contact in contacts:
                    if is_equal(contact,contact_merged):
                        count = count + 1
                        selected_contacts.append(contact)
                item = {}
                item['count'] = count
                item['duplicate_contacts'] = selected_contacts
                item['merged_contact'] = {}
                
                main_emails = []
                
                contact_mails = []
                contact_phones = []
                for contact in selected_contacts:  
                    
                    main_emails.append(contact)
                    
                    cons_email = ContactEmail.objects.filter(contact__id = contact.id)
                    for con in cons_email:
                        contact_mails.append(con)
                    
                    cons_phone = ContactPhone.objects.filter(contact__id = contact.id)
                    for con in cons_phone:
                        contact_phones.append(con)
                    
                    m_group = contact.groups.all()
                    for m in m_group:
                        if m not in contact_merged.groups.all():
                            contact_merged.groups.add(m)
                
                contact_mails = get_different_contact_emails(contact_mails)
                contact_phones = get_different_contact_phones(contact_phones)
                main_emails = get_different_main_emails(main_emails)  
                      
                item['merged_contact']['contact_emails'] = contact_mails
                item['merged_contact']['contact_phones'] = contact_phones
                item['merged_contact']['main_emails'] = main_emails 
                item['merged_contact']['contact'] = contact_merged
                
                merge_contacts.append(item)
                    
            return direct_to_template(request, 'friends_app/find_merge_contacts.html',{'total':len(dups), 'merge_contacts':merge_contacts})
        else: 
#            request.user.message_set.create(message=unicode(_("No duplicate contacts found.")))
#            return HttpResponseRedirect(reverse("invitations_contacts"))
            return direct_to_template(request, 'friends_app/no_contact_merge.html',{'next':reverse("invitations_contacts")})
    else:
        '''
        Enter post request to merge contacts
        '''
        selected_contacts_pk = request.POST.getlist("chk_contacts")
        
        dups = Contact.objects.filter(pk__in = selected_contacts_pk)
        
        contacts = request.user.contacts.all()
        
        
        for contact_merged in dups:
            selected_contacts = []
            for contact in contacts:
                if is_equal(contact,contact_merged):
                    selected_contacts.append(contact)
                    
            main_emails = []
            contact_mails = []
            contact_phones = []
            for contact in selected_contacts:
                m_email = {}
                m_email['label'] = 'Main'
                m_email['email'] = contact.email
                
                main_emails.append(m_email)
                
                cons_email = ContactEmail.objects.filter(contact = contact).delete()
                    
                cons_phone = ContactPhone.objects.filter(contact = contact).delete()
                
                m_group = contact.groups.all()
                for m in m_group:
                    if m not in contact_merged.groups.all():
                        contact_merged.groups.add(m)
                        contact_merged.save()
                
                if contact != contact_merged:
                    contact.delete()
            
            email_labels = request.POST.getlist('email_labels'+str(contact_merged.id)+'[]')
            email_after_merged = request.POST.getlist('email_after_merged'+str(contact_merged.id))
            phone_labels = request.POST.getlist('phone_labels'+str(contact_merged.id)+'[]')
            phone_after_merged = request.POST.getlist('phone_after_merged'+str(contact_merged.id))
            merge_contact_name = request.POST['merge_contact_name'+str(contact_merged.id)]
            merge_contact_lastname = request.POST['merge_contact_lastname'+str(contact_merged.id)]
            
            contact_merged.name = merge_contact_name
            contact_merged.last_name = merge_contact_lastname
            contact_merged.save()
            
            assign_email = False
            
            for i in range(0,len(email_labels)):
                if not assign_email and email_labels[i] =='Main':
                    contact_merged.email = email_after_merged[i]
                    contact_merged.save()
                    assign_email = True
                    
                if contact_merged.email != email_after_merged[i]:
                    ContactEmail(contact = contact_merged,
                              label=email_labels[i], 
                              email = email_after_merged[i]
                              ).save()
            assign_phone = False
    
            for i in range(0,len(phone_labels)):
                if not assign_phone and phone_labels[i] =='Main':
                    contact_merged.phone = phone_after_merged[i]
                    contact_merged.save()
                    assign_phone = True
                    
                ContactPhone(contact = contact_merged,
                              label=phone_labels[i], 
                              phone = phone_after_merged[i]
                              ).save()
                
    request.user.message_set.create(message=unicode(_("The contacts were merged successfully.")))
    return HttpResponseRedirect(reverse("invitations_contacts"))



@login_required
@require_POST
def merge_contacts(request):
    if request.method =='GET':
        raise Http404
    
    selected_contacts_pk = request.POST.getlist("chk_contacts")
    merged_contact_pk = request.POST['chk_merged_contact']
    
    selected_contacts = Contact.objects.filter(pk__in = selected_contacts_pk)
    contact_merged = Contact.objects.get(pk=merged_contact_pk)
    
    main_emails = []
    contact_mails = []
    contact_phones = []
    for contact in selected_contacts:
        m_email = {}
        m_email['label'] = 'Main'
        m_email['email'] = contact.email
        
        main_emails.append(m_email)
        
        cons_email = ContactEmail.objects.filter(contact = contact).delete()
            
        cons_phone = ContactPhone.objects.filter(contact = contact).delete()
        
        m_group = contact.groups.all()
        for m in m_group:
            if m not in contact_merged.groups.all():
                contact_merged.groups.add(m)
                contact_merged.save()
        
        if contact != contact_merged:
            contact.delete()
    
    email_labels = request.POST.getlist('email_labels[]')
    email_after_merged = request.POST.getlist('email_after_merged')
    phone_labels = request.POST.getlist('phone_labels[]')
    phone_after_merged = request.POST.getlist('phone_after_merged')
    merge_contact_name = request.POST['merge_contact_name']
    merge_contact_lastname = request.POST['merge_contact_lastname']
    
    
    contact_merged.name = merge_contact_name
    contact_merged.last_name = merge_contact_lastname
    contact_merged.save()
    assign_email = False
    
    for i in range(0,len(email_labels)):
        if not assign_email and email_labels[i] =='Main':
            contact_merged.email = email_after_merged[i]
            contact_merged.save()
            assign_email = True
            
        if contact_merged.email != email_after_merged[i]:
            ContactEmail(contact = contact_merged,
                      label=email_labels[i], 
                      email = email_after_merged[i]
                      ).save()
    
    assign_phone = False
    
    for i in range(0,len(phone_labels)):
        if not assign_phone and phone_labels[i] =='Main':
            contact_merged.phone = phone_after_merged[i]
            contact_merged.save()
            assign_phone = True
            
        ContactPhone(contact = contact_merged,
                      label=phone_labels[i], 
                      phone = phone_after_merged[i]
                      ).save()
                      
    
    
    return HttpResponseRedirect(reverse("contact_details",args=[contact_merged.urlhash()]))
    

    
@login_required
@require_POST
def contacts_add_to_group(request):
    user = request.user
    if request.POST['action'] == 'merge_contacts':
        selected_contacts_pk = request.POST.getlist("chk_contacts")
        selected_contacts = Contact.objects.filter(pk__in = selected_contacts_pk)
        
        contact_merged = None;
        for contact in selected_contacts:
            if contact.name and contact.last_name:
                contact_merged = contact
                break
        if not contact_merged:
            for contact in selected_contacts:
                if contact.name:
                    contact_merged = contact
                    break
        if not contact_merged:
            for contact in selected_contacts:
                if contact.last_name:
                    contact_merged = contact
                    break
        email_main = []
        if not contact_merged:
            contact_merged = selected_contacts[0]
        if contact_merged:
            item = {}
            item['count'] = len(selected_contacts)
            item['duplicate_contacts'] = selected_contacts
            item['merged_contact'] = {}
            main_emails = []
            contact_mails = []
            contact_phones = []
            for contact in selected_contacts:
                main_emails.append(contact)
                cons_email = ContactEmail.objects.filter(contact__id = contact.id)
                for con in cons_email:
                    contact_mails.append(con)
                
                cons_phone = ContactPhone.objects.filter(contact__id = contact.id)
                for con in cons_phone:
                    contact_phones.append(con)
                
                m_group = contact.groups.all()
                for m in m_group:
                    if m not in contact_merged.groups.all():
                        contact_merged.groups.add(m)
                
            contact_mails = get_different_contact_emails(contact_mails)
            contact_phones = get_different_contact_phones(contact_phones)
            main_emails = get_different_main_emails(main_emails)  
                  
            item['merged_contact']['contact_emails'] = contact_mails
            item['merged_contact']['contact_phones'] = contact_phones
            item['merged_contact']['main_emails'] = main_emails 
            item['merged_contact']['contact'] = contact_merged
                
        return direct_to_template(request, 'friends_app/merge_contacts.html',{'merge_contact': item})
                    
    if request.POST['action'] == 'add_contacts_new_group':
        group_name = request.POST['group_name']
        try:
            ContactGroup.objects.get(owner=user, name=group_name)
            request.user.message_set.create(message=unicode(_("You already have a group with the name %(name)s" %{"name": group_name})))
        except ContactGroup.DoesNotExist:
            contact_group = ContactGroup(owner=user, name=group_name)
            contact_group.save()
            contact_ids = request.POST.getlist("chk_contacts")
            for contact_id in contact_ids:
                contact = Contact.objects.get(id=contact_id)
                group_exists = contact.groups.filter(name=group_name, owner=request.user).count()
                if group_exists:
                    continue
                contact.groups.add(contact_group)
            if contact_ids:
                request.user.message_set.create(message=_("The contacts have been successfully modified."))
        return HttpResponseRedirect(reverse("invitations_contacts"))
    if request.POST['action'] == 'delete_contacts':
        next = request.POST['next']
        selected_contacts_pk = request.POST.getlist("chk_contacts")
        selected_contacts = Contact.objects.filter(pk__in = selected_contacts_pk)
        selected_contacts.delete()
        request.user.message_set.create(message=_(u"Contacts successfully deleted."))
        #return HttpResponseRedirect(reverse("invitations_contacts"))
        return HttpResponseRedirect(next)
    try:
        group = ContactGroup.objects.get(id=request.POST.get("group_id"))
    except (ContactGroup.DoesNotExist, ValueError):
        request.user.message_set.create(message=_("Group does not exist."))
        return HttpResponseRedirect(reverse("contact_groups"))
    contacts = request.POST.getlist("chk_contacts")
    if request.POST['action'] == 'add':
        for contact_id in contacts:
            contact = Contact.objects.get(id=contact_id)
            group_exists = contact.groups.filter(name=group.name, owner=request.user).count()
            if group_exists:
                continue
            contact.groups.add(group)
        if contacts:
            request.user.message_set.create(message=_("The contacts have been successfully modified."))
            return HttpResponseRedirect(reverse("invitations_contacts"))
    if request.POST['action'] == 'remove':
        for contact_id in contacts:
            contact = Contact.objects.get(id=contact_id)
            contact.groups.remove(group)
        request.user.message_set.create(message=_("The contacts have been successfully modified."))
        return HttpResponseRedirect(reverse("invitations_contacts"))
    
    
    
@login_required
def contacts_import(request):
    return direct_to_template(request, 'friends_app/import_contacts.html', {})

''' Import Google Contacts '''

@login_required
def google_redirect(request):
    oauth_login = ""
    if request.method == 'GET':
        params = {"client_id": settings.OAUTH2_CLIENT_ID,
                  "redirect_uri": "http://%s/oauth2callback/" % RequestSite(request).domain,
                  "scope": "https://www.google.com/m8/feeds/",
                  "response_type": "code"}
        oauth_url = "https://accounts.google.com/o/oauth2/auth"
        oauth_login = "%s?%s" %(oauth_url, urllib.urlencode(params))
        return HttpResponseRedirect(oauth_login)

@login_required
def oauth_callback(request):
    consumer = oauth2.Consumer(settings.OAUTH2_CLIENT_ID,
                               settings.OAUTH2_CLIENT_SECRET)
    client = oauth2.Client(consumer)
    oauth2_access_url = "https://accounts.google.com/o/oauth2/token"
    params = {"client_id": settings.OAUTH2_CLIENT_ID,
              "client_secret": settings.OAUTH2_CLIENT_SECRET,
              "redirect_uri": "http://%s/oauth2callback/" % RequestSite(request).domain,
              "grant_type": "authorization_code",
              "code": request.GET.get("code", "")}
    response = client.request(oauth2_access_url, method="POST", 
                              body=urllib.urlencode(params))
    if response[0]["status"] != "200":
        request.user.message_set.create(message=_("An error occurred while fetching the contacts from google."))
    else:
        oauth_token = json.loads(response[1])["access_token"]
        request.session["oauth_token"] = json.loads(response[1])["access_token"]
        
        imported, total = import_google(oauth_token, request.user)
        del request.session["oauth_token"]
        request.user.message_set.create(message=_("%(total)s people with email found, %(imported)s contacts imported.") % {'imported': imported, 'total': total})
    return HttpResponseRedirect(reverse("invitations_contacts"))

@login_required
def yahoo_redirect(request):
    oauth_login = ""
    if request.method == 'GET':
        params = {"openid.claimed_id": 'http://specs.openid.net/auth/2.0/identifier_select',
                  "openid.identity": "http://specs.openid.net/auth/2.0/identifier_select",
                  "openid.mode": "checkid_setup",
                  "openid.ns":"http://specs.openid.net/auth/2.0",
                  "openid.realm": "http://%s"% RequestSite(request).domain,
                  "openid.return_to": "http://%s/yahoo2callback/" % RequestSite(request).domain,
                  "openid.ns.oauth":"http://specs.openid.net/extensions/oauth/1.0",
                  "openid.oauth.consumer":settings.YAHOO_CONSUMER_KEY,
                  }
        oauth_url = "https://open.login.yahooapis.com/openid/op/auth"
        oauth_login = "%s?%s" %(oauth_url, urllib.urlencode(params))
        return HttpResponseRedirect(oauth_login)


class YahooOauthData():
    def __init__(self, oauth_token = None,oauth_token_secret = None, oauth_expires_in=None
                 , oauth_session_handle=None,oauth_authorization_expires_in = None, xoauth_yahoo_guid = None):
        self.oauth_token = oauth_token
        self.oauth_token_secret = oauth_token_secret
        self.oauth_expires_in = oauth_expires_in
        self.oauth_session_handle = oauth_session_handle
        self.oauth_authorization_expires_in = oauth_authorization_expires_in
        self.xoauth_yahoo_guid = xoauth_yahoo_guid
    
    @staticmethod
    def parse_oauth_data(s):
        """Deserializes a token from a string like one returned by
        `to_string()`."""
    
        if not len(s):
            raise ValueError("Invalid parameter string.")
    
        params = parse_qs(s, keep_blank_values=False)
        if not len(params):
            raise ValueError("Invalid parameter string.")
    
        try:
            oauth_token = params['oauth_token'][0]
        except Exception:
            raise ValueError("'oauth_token' not found in OAuth request.")
    
        try:
            oauth_token_secret = params['oauth_token_secret'][0]
        except Exception:
            raise ValueError("'oauth_token_secret' not found in " 
                "OAuth request.")
        try:
            xoauth_yahoo_guid = params['xoauth_yahoo_guid'][0]
        except Exception:
            raise ValueError("'xoauth_yahoo_guid' not found in " 
                "OAuth request.")
    
        yahoo_data = YahooOauthData(
                                    oauth_token=oauth_token, 
                                    oauth_token_secret=oauth_token_secret, 
                                    xoauth_yahoo_guid = xoauth_yahoo_guid
                                    )
        return yahoo_data

def yahoo_callback(request):
    consumer = oauth2.Consumer(settings.YAHOO_CONSUMER_KEY,
                               settings.YAHOO_CONSUMER_SECRET)
    client = oauth2.Client(consumer)
    oauth2_access_url = "https://api.login.yahoo.com/oauth/v2/get_token?"
    params = {
              "oauth_consumer_key":settings.YAHOO_CONSUMER_KEY,
              "oauth_nonce":request.GET["openid.response_nonce"],
              "oauth_signature_method":"PLAINTEXT",
              "oauth_token":request.GET["openid.oauth.request_token"],
              "oauth_version":"1.0",
              }
    response = client.request(oauth2_access_url, method="POST", 
                              body=urllib.urlencode(params))
    if response[0]["status"] != "200":
        request.user.message_set.create(message=_("An error occurred while fetching the contacts from yahoo."))
        
    else:
        yahoodata = YahooOauthData.parse_oauth_data(response[1])
        token = oauth2.Token(yahoodata.oauth_token, yahoodata.oauth_token_secret)
        client.token = token
        contact_url = 'http://social.yahooapis.com/v1/user/'+yahoodata.xoauth_yahoo_guid+'/contacts?'
        contact_params = {'format':'xml' }
        headers = {'Content-Type':'Accept'}
        response_data = client.request(contact_url, method="GET", headers = headers,
                                  body=urllib.urlencode(contact_params))
        if response[0]["status"] != "200":
            request.user.message_set.create(message=_("An error occurred while fetching the contacts from yahoo."))
        else:
            imported, total = yahoo_response_parser(request, response_data[1])
            request.user.message_set.create(message=_("%(total)s people with email found, %(imported)s contacts imported.") % {'imported': imported, 'total': total})
    return HttpResponseRedirect(reverse("invitations_contacts"))
    
        
YAHOO_NAMESPACE = '{http://social.yahooapis.com/v1/schema.rng}'
def fixtag(tag):
    return  YAHOO_NAMESPACE+tag       
def yahoo_response_parser(request, text_xml):
    parser = XMLTreeBuilder()
    dataxml = parser.feed(text_xml)
    root = parser.close()
    xmltree = ElementTree(element = root)
    contacts = xmltree.getiterator(fixtag('contact'))
    imported = 0
    total = 0
    for contact in contacts:
        try:
            fields = contact.getiterator(fixtag('fields'))
            emails = []
            fname = ''
            lname = ''
            phone = None
            for field in fields:
                if field.getiterator(fixtag('type'))[0].text == 'yahooid':
                    email = field.getiterator(fixtag('value'))[0].text+'@yahoo.com'
                    emails.append(email)
                elif field.getiterator(fixtag('type'))[0].text == 'email':
                    email = field.getiterator(fixtag('value'))[0].text
                    emails.append(email)
                elif field.getiterator(fixtag('type'))[0].text == 'name':
                    fname = field.getiterator(fixtag('givenName'))[0].text
                    lname= field.getiterator(fixtag('familyName'))[0].text
                elif field.getiterator(fixtag('type'))[0].text == 'phone':
                    phone = field.getiterator(fixtag('value'))[0].text
            if emails:
                create_contact_emails(request.user, fname, lname, emails, phone)
                imported+=1
        except:
            pass
        total+=1
        
    return imported, total

