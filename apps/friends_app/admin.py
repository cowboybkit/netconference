from django.contrib import admin
from friends_app.models import ContactGroup

class ContactGroupAdmin(admin.ModelAdmin):
    list_display = ('owner', 'name', 'order')
   
admin.site.register(ContactGroup, ContactGroupAdmin)