from django.db import models
from django.contrib.auth.models import User
from netconf_utils.encode_decode import *
class ContactGroup(models.Model):
    """User-defined Groups that they can assign a Contact to."""
    
    owner = models.ForeignKey(User, related_name="group_owner")
    name = models.CharField(max_length=100)
    order = models.IntegerField(blank = True, null = True)
    
    def urlhash(self):
        return uri_b64encode(str(self.pk * 42))
    
    def __unicode__(self):
        return self.name
    
class ContactFolder(models.Model):
    name = models.CharField(max_length=100)
    user = models.ForeignKey(User)
    parent_folder = models.ForeignKey('self', null=True, blank=True)
    absolute_path = models.TextField(editable=False)

    def __unicode__(self):
        return self.name

    def calculate_absolute_path(self):
        parent_folder_heirarachy = []
        par_folder = self.parent_folder
        while par_folder is not None:
            parent_folder_heirarachy.append(par_folder.name)
            par_folder = par_folder.parent_folder
        parent_folder_heirarachy.reverse()
        absolute_path = "/".join(parent_folder_heirarachy)
        if absolute_path:
            absolute_path = absolute_path + "/" + self.name
        else:
            absolute_path = self.name
        self.absolute_path = absolute_path

    def save(self, *args, **kwargs):
        self.calculate_absolute_path()
        super(ContactFolder, self).save(*args, **kwargs)
