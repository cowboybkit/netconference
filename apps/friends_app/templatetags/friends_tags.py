from django import template
from friends.models import Friendship, FriendshipInvitation
from django.utils.translation import ugettext_lazy as _

register = template.Library()

class IsFriendNode(template.Node):
    def __init__(self, user, friend, context_var = None):
        self.user = user
        self.friend = friend
        self.context_var = context_var or "is_user_friend"

    def render(self, context):
        user = self.user.resolve(context)
        friend = self.friend.resolve(context)
        are_friends = Friendship.objects.are_friends(user, friend)
        has_invited = bool(FriendshipInvitation.objects.invitations(from_user=user, to_user=friend).count())
        context[self.context_var] = (are_friends and _("Friend")) or (has_invited and _("Invited"))
        return ""

def is_friend(parser, token):
    tokens = token.split_contents()
    if len(tokens) != 5:
        raise template.TemplateSyntaxError("is_friend tag expects 5 arguments.")
    if tokens[3] != u"as":
        raise template.TemplateSyntaxError("The 3rd argument must be 'as'.")
    user = template.FilterExpression(tokens[1], parser)
    friend = template.FilterExpression(tokens[2], parser)
    context_var = tokens[4]
    return IsFriendNode(user, friend, context_var)

register.tag(is_friend)
