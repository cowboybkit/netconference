import gdata.auth
import gdata.gauth
import gdata.apps.service
import gdata.docs.service
from checkout import authnet
from integrate.models import Domain, UserOfDomain
from django.conf import settings

import urllib, urllib2;
from poster.encode import multipart_encode
from poster.streaminghttp import register_openers
import os;
from google_api import DocsAPI

def create_default_docs_api():
    docs_api = DocsAPI();
    docs_api.client_login(settings.DEFAULT_GOOGLE_DOCS_ACCOUNT,
                          settings.DEFAULT_GOOGLE_DOCS_PASSWORD);
    return docs_api;
def update_file(file, temp_file_path, user):
    temp_file = open(temp_file_path)
    cid = getattr(settings, "CID", "netconference")
    params = {  'description': file.description,
                'title': file.title,
                'file_upload_filename': file.filename,
                'uid': user.id,
                'uuid': file.uuid,
                'username': user.username,
                'cid': cid,
                'update': '1',
                'redirect_url': 'http://netconference.com/files/',
                'file_upload': temp_file,
                'upload_method': 'auto',
            }
    #post_url = "http://encode1.media.netconf.nxtgencdn.com/uploader_new/vangtest.php"
    post_url = settings.FILE_UPLOADER_URL; 
    register_openers()
    datagen, headers = multipart_encode(params)
    request = urllib2.Request(post_url, datagen, headers)
    
    urllib2.urlopen(request)
    temp_file.close();
    os.remove(temp_file_path);
    
def do_payments(domain, users, data):
    cvv = data['credit_card_cvv'];
    cc_number = data['credit_card_number'];
    cc_exp_m = data['credit_card_expire_month'];
    cc_exp_y = data['credit_card_expire_year'];
    sixty_day_trial = data['sixty_day_trial'];
    subs_obj = data['subscription'];
    amount = len(users) * subs_obj.price
    result = True;
    fname = data['firstname'];
    lname = data['lastname'];
    print "================== firstname: %s - lastname: %s" %(fname, lname);
    domain_obj = Domain.objects.get(name=domain);
    id = 40000 + domain_obj.pk;
    cc_response = authnet.call_recurring_subscription(id, 
                                                      "%s" % amount, 
                                                      cc_number, 
                                                      "%s-%s"%(cc_exp_m, cc_exp_y), 
                                                      cvv, 
                                                      fname, 
                                                      lname,
                                                      sixty_day_trial)
    if not cc_response[0]:
        result = False;
        for user in users:
            UserOfDomain.objects.get(user=user, domain__name=domain).delete();
            user.profile_set.get().delete();
            user.delete();
    queryset = UserOfDomain.objects.filter(domain=domain_obj)
    if queryset.count() == 0:
        domain_obj.delete();
    return result;

def create_oauth_service(service, domain, requestor_id):
    consumer_key = settings.CONSUMER_KEY
    consumer_secret = settings.CONSUMER_SECRET;
    sig_method = gdata.auth.OAuthSignatureMethod.HMAC_SHA1  
    service.SetOAuthInputParameters(sig_method, consumer_key, consumer_secret=consumer_secret, two_legged_oauth=True, requestor_id=requestor_id)
    service.ssl = True;
    service.debug = True;
    service.domain = domain;
    return service;
def create_provisioning_service(domain, requestor_id):
    service = gdata.apps.service.AppsService()
    return create_oauth_service(service, domain, requestor_id)
def create_oauth_docs_service(domain, requestor_id):
    service = gdata.docs.service.DocsService();
    return create_oauth_service(service, domain, requestor_id)
def create_gmail_service(email, password):
    service = gdata.docs.service.DocsService();
    service.ClientLogin(email, password);
    service.ssl = True;
    service.debug = True;
    return service;

def make_choices(users):
    choices = list();
    for user in users:
        item = list();
        item.append(user.email + "$" + user.firstname + "$" + user.lastname);
        item.append(user.email);
        choices.append(item);
    return choices;

from openid.consumer.consumer import SUCCESS
from openid.extensions import ax, sreg
def extract_user_details(openid_response):
    email = fullname = first_name = last_name = nickname = None
    sreg_response = sreg.SRegResponse.fromSuccessResponse(openid_response)
    if sreg_response:
        email = sreg_response.get('email')
        fullname = sreg_response.get('fullname')
        nickname = sreg_response.get('nickname')

    # If any attributes are provided via Attribute Exchange, use
    # them in preference.
    fetch_response = ax.FetchResponse.fromSuccessResponse(openid_response)
    if fetch_response:
        # The myOpenID provider advertises AX support, but uses
        # attribute names from an obsolete draft of the
        # specification.  We check for them first so the common
        # names take precedence.
        
        email = fetch_response.getSingle(
            'http://schema.openid.net/contact/email', email)
        fullname = fetch_response.getSingle(
            'http://schema.openid.net/namePerson', fullname)
        nickname = fetch_response.getSingle(
            'http://schema.openid.net/namePerson/friendly', nickname)

        email = fetch_response.getSingle(
            'http://axschema.org/contact/email', email)
        fullname = fetch_response.getSingle(
            'http://axschema.org/namePerson', fullname)
        first_name = fetch_response.getSingle(
            'http://axschema.org/namePerson/first', first_name)
        last_name = fetch_response.getSingle(
            'http://axschema.org/namePerson/last', last_name)
        nickname = fetch_response.getSingle(
            'http://axschema.org/namePerson/friendly', nickname)

    if fullname and not (first_name or last_name):
        # Django wants to store first and last names separately,
        # so we do our best to split the full name.
        if ' ' in fullname:
            first_name, last_name = fullname.rsplit(None, 1)
        else:
            first_name = u''
            last_name = fullname

    return dict(email=email, nickname=nickname,
                first_name=first_name, last_name=last_name)

ERROR_STRINGS = { 
                    401:'Unable to access your google apps data. Please grant data access for NetConference in your google apps',
                    404:'File not found'
                 }
def get_error_reason(e):
    status = e.args[0]['status'];
    if ERROR_STRINGS.has_key(status):
        return ERROR_STRINGS[status];
    else:
        return e.args[0]['reason'];
'''
from integrate.models import Domain, UserOfDomain
def get_users_of_domain(domain):
    domain_obj = Domain.objects.get(name = domain);
    users_of_domain = list();
    emails_of_domain = list();
    try:
        queryset = UserOfDomain.objects.filter(domain = domain_obj);
        for entry in queryset:
            users_of_domain.append(entry.user);
            emails_of_domain.append(entry.user.email);               
    except:
        pass
    return users_of_domain, emails_of_domain;
'''

'''
def get_entries(domain, email):
    docs = create_docs_gdata(domain, email);
    feed = docs.GetDocumentListFeed();
    
    entries = list();
    for entry in feed.entry:
        item = dict();
        item['title'] = entry.title.text.encode('UTF8');
        item['id'] = entry.resourceId.text.encode('UTF8')
        item['href'] = entry.GetAlternateLink().href;
        type = entry.GetDocumentType();
        item['type'] = type;
        item['ext'] = GOOGLE_DOCS_CONVERT_TABLE[type];
        entries.append(item);
    return entries;
'''

'''
import os;
import tempfile;
from django.test.client import Client;
import urllib, urllib2;
    
def download_documents(user, domain, entries):
    entry = entries[0];
    file_type = entry['type'];
    file_ext = entry['ext'];
    file_title = entry['title'];
    file_name = file_title + "." + file_ext;
    resource_id = entry['id'];
    temp_file_path = os.path.join(tempfile.mkdtemp(), file_name);
    docs = create_docs_gdata(domain.name, user.email);
    if file_type == "document":
        docs.DownloadDocument(resource_id, temp_file_path);
    elif file_type == "spreadsheet":
        docs.DownloadSpreadsheet(resource_id, temp_file_path);
    elif file_type == "presentation":
        docs.DownloadPresentation(resource_id, temp_file_path);
    elif file_type == "pdf":
        docs.DownloadDocument(resource_id, temp_file_path);
    temp_file = open(temp_file_path)
    params = {  'description': "<your description>",
                'title': file_title,
                'file_upload_filename': file_name,
                'uid': user.id,
                'username': user.username,
                'cid': 'beta_netconference',
                'redirect_url': 'http://beta.netconference.com/files/',
                'file_upload': temp_file,
                'upload_method': 'auto',
            }
    #post_url = "http://encode1.media.netconf.nxtgencdn.com/uploader_new/vangtest.php"
    post_url = "http://encode1.media.netconf.nxtgencdn.com/uploader/media-upload.php"#settings.FILE_UPLOADER_URL;
    from poster.encode import multipart_encode
    from poster.streaminghttp import register_openers
    import urllib2
    # Register the streaming http handlers with urllib2
    register_openers()
    # Start the multipart/form-data encoding of the file "DSC0001.jpg"
    # "image1" is the name of the parameter, which is normally set
    # via the "name" parameter of the HTML <input> tag.
    
    # headers contains the necessary Content-Type and Content-Length
    # datagen is a generator object that yields the encoded parameters
    datagen, headers = multipart_encode(params)
    
    # Create the Request object
    request = urllib2.Request(post_url, datagen, headers)
    # Actually do the request, and get the response
    print urllib2.urlopen(request).read()
    temp_file.close();
    os.remove(temp_file_path);
'''

'''
from django.contrib.auth.models import User
def create_users_from_list(domain, users, data):
    try:
        domain_obj = Domain.objects.get(name=domain)
    except Domain.DoesNotExist:
        domain_obj = Domain.objects.create(name=domain)
    users_created = []
    for item in users:
        email = item['email'];
        firstname = item['firstname'];
        lastname = item['lastname'];
        try:
            user_domain = UserOfDomain.objects.get(user__email=email, domain__name=domain);
            user = user_domain.user;
        except:
            username = email.replace("@", "_");
            username = username.replace(".", "_");
            i = 1
            while True:
                if i > 1:
                    username += str(i)
                try:
                    User.objects.get(username__exact=username)
                except User.DoesNotExist:
                    break
                i += 1
            user = User.objects.create_user(username=username, email=email, password=None)
            user.first_name = firstname;
            user.last_name = lastname;
            user.save()
            user_profile = user.profile_set.get()
            user_profile.phone = data['phone_number'];
            user_profile.billing_address_1 = data['address'];
            user_profile.billing_country = data['country'];
            if(data['country'] != "USA"):
                state = None;
            else:
                state = data['state'];
            user_profile.billing_state = state
            user_profile.billing_zip = data['postal_code'];
            user_profile.save();
            user_domain = UserOfDomain.objects.create(user=user, domain=domain_obj);
            user_domain.save();        
        users_created.append(user)
    return users_created
'''

'''
def get_users_from_google(domain, requestor_id):
    apps = create_apps_gdata(domain, requestor_id);    
    #get list of user of domain
    res = apps.RetrieveAllUsers();
    users = list();
    for entry in res.entry:
        print "============================="
        print entry.name;
        user = dict();
        user['email'] = entry.login.user_name + "@" + domain;
        user['firstname'] = entry.name.family_name;
        user['lastname'] = entry.name.given_name;
        users.append(user)        
    return users
'''

'''
def is_admin_of_domain(email):
    arr = email.split('@')
    domain = arr[1];
    username = arr[0];
    apps = create_apps_gdata(domain, email);
    
    #get user of domain
    user = apps.RetrieveUser(username);
    return user.login.admin == "true";
'''