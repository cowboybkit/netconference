import gdata.auth
import gdata.gauth
import gdata.apps.service
import os;
import tempfile;
import urllib, urllib2;
import StringIO;
from django.conf import settings
from poster.encode import multipart_encode
from poster.streaminghttp import register_openers
from gdata.service import RequestError
from gdata.docs import DocumentListEntry
import datetime, re

CONVERT_TABLE = {
                 'document' : 'doc',
                 'presentation' : 'ppt',
                 'spreadsheet' : 'xls',
                 'pdf' : 'pdf',
                 }
REVERT_TABLE = {   
                 'doc' : 'document',
                 'ppt': 'presentation',
                 'xls': 'spreadsheet',
                 'pdf': 'pdf',
                }
UPLOAD_FILE_TYPES = ['doc', 'pdf', 'ppt', 'xls']
DOWNLOAD_FILE_TYPES = ['document', 'presentation', 'spreadsheet',]
EDITABLE_FILE_TYPES = ['doc', 'xls', 'ppt']
class GoogleAppsUser():
    def __init__(self, email=None, firstname=None, lastname=None):
        self.email = email;
        self.firstname = firstname;
        self.lastname = lastname;

class GoogleDocsEntry(DocumentListEntry):
    def get_id(self):
        try:
            return self.attribute_dict['id']
        except:
            return self.resourceId.text
    def get_title(self):
        try:
            return self.attribute_dict['title']
        except:
            return unicode(self.title.text, "utf-8" )
    def get_href(self):
        try:
            return self.attribute_dict['href']
        except:
            return self.GetAlternateLink().href;
    def get_embeded_link(self):
        try:
            if self.get_type() == "presentation":
                embeded_link = self.get_href();
                embeded_link = embeded_link.replace("edit", "view")
                return embeded_link;
            else:
                return self.get_href();
        except:
            return self.GetAlternateLink().href;
    def get_type(self):
        try:
            return self.attribute_dict['type']
        except:
            return self.GetDocumentType();
    def get_extension(self):
        try:
            return self.attribute_dict['extension']
        except:
            return CONVERT_TABLE.get(self.GetDocumentType(), "None")
    def get_edit_link(self):
        try:
            return self.attribute_dict['href']
        except:
            try:
                return self.GetEditLink().href;
            except:
                return "None"
    def get_uri(self):
        try:
            return self.attribute_dict['uri']
        except:
            try:
                return self.GetSelfLink().href;
            except:
                return "None"
    def get_updated_time(self):
        s = self.updated.text
        d = datetime.datetime(*map(int, re.split('[^\d]', s)[:-1]))
        return d;

    def from_string(self, string):
        temp = string.split(',');
        self.attribute_dict = dict();
        self.attribute_dict['id'] = temp[0];
        self.attribute_dict['title'] = temp[1];
        self.attribute_dict['type'] = temp[2];
        self.attribute_dict['extension'] = temp[3];
        self.attribute_dict['href'] = temp[4];
        self.attribute_dict['edit_link'] = temp[5];
        self.attribute_dict['uri'] = temp[6];
        print "======================== type : %s" %self.get_type();
    def from_entry(self, entry):
        DocumentListEntry.__init__(self, 
                                  resourceId = entry.resourceId, 
                                  feedLink = entry.feedLink, 
                                  lastViewed = entry.lastViewed,
                                  lastModifiedBy = entry.lastModifiedBy, 
                                  writersCanInvite = entry.writersCanInvite, 
                                  author = entry.author,
                                  category = entry.category, 
                                  content = entry.content, 
                                  #atom_id = entry.atom_id, 
                                  link = entry.link,
                                  published = entry.published, 
                                  title = entry.title, 
                                  updated = entry.updated, 
                                  text = entry.text,
                                  extension_elements = entry.extension_elements, 
                                  extension_attributes = entry.extension_attributes)
    def to_string(self):
        result = self.get_id();
        result += "," + self.get_title();
        result += "," + self.get_type();
        result += "," + self.get_extension();
        result += "," + self.get_href();
        result += "," + self.get_edit_link();
        result += "," + self.get_uri();
        return result;

from gdata.docs.service import DocsService
from gdata.docs.service import DOWNLOAD_SPREADSHEET_FORMATS
from gdata.spreadsheet.service import SpreadsheetsService
import atom
class GoogleAPI():
    def two_legged_oauth(self, domain, requestor_id, consumer_key, consumer_secret):
        sig_method = gdata.auth.OAuthSignatureMethod.HMAC_SHA1  
        self.SetOAuthInputParameters(sig_method, consumer_key, consumer_secret=consumer_secret, two_legged_oauth=True, requestor_id=requestor_id)
        self.ssl = True;
        self.debug = True;
        self.domain = domain;
    def client_login(self, username, password):
        self.ClientLogin(username, password);
        self.ssl = True;
        self.debug = True;
        
        spreadsheets_client = gdata.spreadsheet.service.SpreadsheetsService()
        spreadsheets_client.ClientLogin(username, password)
        self.spreadsheets_token = spreadsheets_client.GetClientLoginToken();
        self.docs_token = self.GetClientLoginToken();

import time;
from threading import Thread
class DocsAPI(DocsService, GoogleAPI):    
    
    """ Begin override methods of DocsService """
    
    def _UploadFile(self, media_source, title, category, folder_or_uri=None):
        """Uploads a file to the Document List feed.
    
        Args:
          media_source: A gdata.MediaSource object containing the file to be
              uploaded.
          title: string The title of the document on the server after being
              uploaded.
          category: An atom.Category object specifying the appropriate document
              type.
          folder_or_uri: DocumentListEntry or string (optional) An object with a
              link to a folder or a uri to a folder to upload to.
              Note: A valid uri for a folder is of the form:
                    /feeds/folders/private/full/folder%3Afolder_id
    
        Returns:
          A DocumentListEntry containing information about the document created on
          the Google Documents service.
        """
        print "=================== overrided upload file ==============="
        if folder_or_uri:
          try:
            uri = folder_or_uri.content.src
          except AttributeError:
            uri = folder_or_uri
        else:
          uri = '/feeds/default/private/full'
    
        entry = gdata.docs.DocumentListEntry()
        entry.title = atom.Title(text=title)
        entry.category.append(category)
        entry = self.Post(entry, uri, media_source=media_source,
                          extra_headers={'Slug': media_source.file_name, 'GData-Version': 3},
                          converter=gdata.docs.DocumentListEntryFromString)
        return entry
    def DownloadDocument(self, entry, file_path):
        """Downloads a document from the Document List.
    
        Args:
          entry_or_resource_id: DoclistEntry or string of the document
              resource id to download.
          file_path: string The full path to save the file to.  The export
              format is inferred from the the file extension.
        """
        ext = ''
        match = self._DocsService__FILE_EXT_PATTERN.match(file_path)
        if match:
          ext = match.group(1)
        
        resource_id = entry.get_id();
        resource_id = resource_id.replace(':', '%3A')
        doc_id = resource_id[resource_id.find('%3A') + 3:]
        
        if entry.get_href().find("Doc?docid=") != -1:
            export_uri = '/feeds/download/documents/Export'
            export_uri += '?docID=%s&exportFormat=%s' % (doc_id, ext)
        else:
            export_uri = '/document/d/%s' %doc_id
            export_uri += '/export?format=%s' %ext
        self._DownloadFile(export_uri, file_path)
    def DownloadSpreadsheet(self, entry_or_resource_id, file_path, gid=0):
        """Downloads a spreadsheet from the Document List.
    
        Args:
          entry_or_resource_id: DoclistEntry or string of the spreadsheet
            resource id to download.
          file_path: string The full path to save the file to.  The export
              format is inferred from the the file extension.
          gid: string or int (optional) The grid/sheet number to download.
              Used only for tsv and csv exports.
        """
        ext = ''
        match = self._DocsService__FILE_EXT_PATTERN.match(file_path)
        if match:
            ext = match.group(1)
    
        if isinstance(entry_or_resource_id, gdata.docs.DocumentListEntry):
            resource_id = entry_or_resource_id.resourceId.text
        else:
            resource_id = entry_or_resource_id
    
        resource_id = resource_id.replace(':', '%3A')
        key = resource_id[resource_id.find('%3A') + 3:]
    
        export_uri = ('https://spreadsheets.google.com'
                      '/feeds/download/spreadsheets/Export')
        export_uri += '?key=%s&fmcmd=%s' % (key,
                                            DOWNLOAD_SPREADSHEET_FORMATS[ext])
        if ext == 'csv' or ext == 'tsv':
            export_uri += '&gid=' + str(gid)
        if hasattr(self, 'spreadsheets_token'):
            self.SetClientLoginToken(self.spreadsheets_token);
            self._DownloadFile(export_uri, file_path);
            self.SetClientLoginToken(self.docs_token);
        else:
            self._DownloadFile(export_uri, file_path);
    def GetDocumentListEntry(self, uri):
        """Retrieves a particular DocumentListEntry by its unique URI.
    
        Args:
          uri: string The unique URI of an entry in a Document List feed.
    
        Returns:
          A DocumentListEntry object representing the retrieved entry.
        """
        return self.Get(uri, extra_headers={'GData-Version': 3},converter=gdata.docs.DocumentListEntryFromString)
    def QueryDocumentListFeed(self, uri):
        """Retrieves a DocumentListFeed by retrieving a URI based off the Document
           List feed, including any query parameters. A DocumentQuery object can
           be used to construct these parameters.
    
        Args:
          uri: string The URI of the feed being retrieved possibly with query
              parameters.
    
        Returns:
          A DocumentListFeed object representing the feed returned by the server.
        """
        return self.Get(uri, extra_headers={'GData-Version': 3}, converter=gdata.docs.DocumentListFeedFromString)
    def GetDocumentListFeed(self, uri=None):
        """Retrieves a feed containing all of a user's documents.
    
        Args:
          uri: string A full URI to query the Document List feed.
        """
        if not uri:
          uri = '/feeds/default/private/full'
        return self.QueryDocumentListFeed(uri)
    
    def Get(self, uri, extra_headers=None, redirects_remaining=4, encoding='UTF-8', converter=None):
        """Query the GData API with the given URI
    
        The uri is the portion of the URI after the server value 
        (ex: www.google.com).
    
        To perform a query against Google Base, set the server to 
        'base.google.com' and set the uri to '/base/feeds/...', where ... is 
        your query. For example, to find snippets for all digital cameras uri 
        should be set to: '/base/feeds/snippets?bq=digital+camera'
    
        Args:
          uri: string The query in the form of a URI. Example:
               '/base/feeds/snippets?bq=digital+camera'.
          extra_headers: dictionary (optional) Extra HTTP headers to be included
                         in the GET request. These headers are in addition to 
                         those stored in the client's additional_headers property.
                         The client automatically sets the Content-Type and 
                         Authorization headers.
          redirects_remaining: int (optional) Tracks the number of additional
              redirects this method will allow. If the service object receives
              a redirect and remaining is 0, it will not follow the redirect. 
              This was added to avoid infinite redirect loops.
          encoding: string (optional) The character encoding for the server's
              response. Default is UTF-8
          converter: func (optional) A function which will transform
              the server's results before it is returned. Example: use 
              GDataFeedFromString to parse the server response as if it
              were a GDataFeed.
    
        Returns:
          If there is no ResultsTransformer specified in the call, a GDataFeed 
          or GDataEntry depending on which is sent from the server. If the 
          response is niether a feed or entry and there is no ResultsTransformer,
          return a string. If there is a ResultsTransformer, the returned value 
          will be that of the ResultsTransformer function.
        """
        if extra_headers is None:
            extra_headers = {'GData-Version': 3}
        else:
            extra_headers['GData-Version'] = 3;
        return DocsService.Get(self, uri, extra_headers, redirects_remaining, encoding, converter);
    def Post(self, data, uri, extra_headers=None, url_params=None,
           escape_params=True, redirects_remaining=4, media_source=None,
           converter=None):
        if extra_headers is None:
            extra_headers = {'GData-Version': 3}
        else:
            extra_headers['GData-Version'] = 3;
        return DocsService.Post(self, data, uri, extra_headers, url_params,
           escape_params, redirects_remaining, media_source, converter)    
    
    ''' End override method of DocsService '''

    ''' Begin methods of DocsAPI '''
    def retrieve_all_docs(self):
        feed = self.GetDocumentListFeed();
        entries = list();
        for entry in feed.entry:
            item = GoogleDocsEntry();
            item.from_entry(entry);
            entries.append(item);
            #self.retrieve(item.get_uri())
        return entries;
    def retrieve(self, uri):
        print "===================== retrieve uri: %s" %uri;
        entry = self.Get(uri, extra_headers={'GData-Version': 3},converter=gdata.docs.DocumentListEntryFromString)
        #entry = self.GetDocumentListEntry(uri);
        my_entry = GoogleDocsEntry();
        my_entry.from_entry(entry);
        return my_entry;
    def download_docs(self, user, entry):
        if not entry.get_type() in DOWNLOAD_FILE_TYPES:
            raise RequestError, {'status': 415,
                                         'reason': "Unsupported file type",
                                         'body': "Unsupported file type"}
        file_name = entry.get_title() + "." + entry.get_extension();
        temp_file_path = os.path.join(tempfile.mkdtemp(), file_name);
        
        if entry.get_type() == "document":
            self.DownloadDocument(entry, temp_file_path);
        elif entry.get_type() == "spreadsheet":
            self.DownloadSpreadsheet(entry.get_id(), temp_file_path);
        elif entry.get_type() == "presentation":
            self.DownloadPresentation(entry.get_id(), temp_file_path);
        else:
            self.DownloadDocument(entry, temp_file_path);
        temp_file = open(temp_file_path)
        cid = getattr(settings, "CID", "netconference")
        params = {  'description': "<your description>",
                    'title': entry.get_title(),
                    'file_upload_filename': file_name,
                    'uid': user.id,
                    'username': user.username,
                    'cid': cid,
                    'redirect_url': 'http://netconference.com/files/',
                    'file_upload': temp_file,
                    'upload_method': 'auto',
                }
        #post_url = "http://encode1.media.netconf.nxtgencdn.com/uploader_new/vangtest.php"
        post_url = settings.FILE_UPLOADER_URL; #"http://encode1.media.netconf.nxtgencdn.com/uploader/media-upload.php"#settings.FILE_UPLOADER_URL;
        register_openers()
        datagen, headers = multipart_encode(params)
        request = urllib2.Request(post_url, datagen, headers)
        
        urllib2.urlopen(request)
        temp_file.close();
        os.remove(temp_file_path);
    def download(self, user, entry, file_name):
        if not entry.get_type() in DOWNLOAD_FILE_TYPES:
            raise RequestError, {'status': 415,
                                         'reason': "Unsupported file type",
                                         'body': "Unsupported file type"}
        #file_name = entry.title + "." + entry.extension;
        temp_file_path = os.path.join(tempfile.mkdtemp(), file_name);
        
        if entry.get_type() == "document":
            self.DownloadDocument(entry, temp_file_path);
        elif entry.get_type() == "spreadsheet":
            self.DownloadSpreadsheet(entry.get_id(), temp_file_path);
        elif entry.get_type() == "presentation":
            self.DownloadPresentation(entry.get_id(), temp_file_path);
        else:
            self.DownloadDocument(entry, temp_file_path);
        return temp_file_path;
    
    def delete(self, entry):
        self.Delete(entry.get_edit_link(), extra_headers={'If-Match': '*', 'GData-Version': 3})
        
    def upload(self, file_path, file_name, file_extension, public=False):
        if not file_extension.lower() in UPLOAD_FILE_TYPES:
            raise RequestError, {'status': 415,
                                         'reason': "Unsupported file type",
                                         'body': "Unsupported file type"}
        source_file = urllib.urlopen(file_path)
        data = source_file.read();
        source_file.close();
        length = len(data);
        print "=================================== file_path: %s" %file_path
        ms = gdata.MediaSource(file_handle=StringIO.StringIO(data), 
                               content_type=gdata.docs.service.SUPPORTED_FILETYPES[file_extension.upper()],
                               content_length=length,
                               file_name=file_name)
        if REVERT_TABLE[file_extension] == "document":
            entry = self.UploadDocument(ms, file_name)
        elif REVERT_TABLE[file_extension] == "spreadsheet":
            entry = self.UploadSpreadsheet(ms, file_name);
        elif REVERT_TABLE[file_extension] == "presentation":
            entry = self.UploadPresentation(ms, file_name);
        else:
            entry = self.UploadDocument(ms, file_name);
        my_entry = GoogleDocsEntry();
        my_entry.from_entry(entry);
        
        if public == True:
            query = gdata.service.Query(feed='/feeds/default/private/full')
            uri = '%s/%s/%s' % (query.ToUri(), my_entry.get_id(), "acl")
            scope = gdata.docs.Scope(type='default')
            role = gdata.docs.Role(value="writer")
            acl_entry = gdata.docs.DocumentListAclEntry(scope=scope, role=role)
            self.Post(acl_entry, uri, converter=gdata.docs.DocumentListAclEntryFromString)
        return my_entry;
    def publish(self, entry):
        self.change_acl(entry);
        success = False
        while not success:
            time.sleep(10);
            acl_feed = self.retrieve_acl(entry);
            acl_entry = acl_feed.entry[1];
            scope = acl_entry.scope;
            role = acl_entry.role;
            if scope.type == "default" and role.value == "writer":
                success = True;
    def change_acl(self, entry):
        query = gdata.service.Query(feed='/feeds/default/private/full')
        uri = '%s/%s/%s' % (query.ToUri(), entry.get_id(), "acl")
        scope = gdata.docs.Scope(type='default')
        role = gdata.docs.Role(value="writer")
        acl_entry = gdata.docs.DocumentListAclEntry(scope=scope, role=role)
        result = self.Post(acl_entry, uri, converter=gdata.docs.DocumentListAclEntryFromString)
        return result;
    def retrieve_acl(self, entry):
        query = gdata.service.Query(feed='/feeds/default/private/full')
        uri = '%s/%s/%s' % (query.ToUri(), entry.get_id(), "acl")
        acl_feed = self.GetDocumentListAclFeed(uri)
        return acl_feed;

    ''' End methods of DocsAPI '''

class ProvisioningAPI():
    def __init__(self, service):
        self.service = service;
    def retrieve_all_users(self, domain, requestor_id):
        res = self.service.RetrieveAllUsers();
        users = list();
        for entry in res.entry:
            user = GoogleAppsUser();#dict();
            user.email = entry.login.user_name + "@" + domain;
            user.firstname = unicode(entry.name.family_name, "utf-8" )
            user.lastname = unicode(entry.name.given_name, "utf-8");
            users.append(user)        
        return users
    def is_admin_of_domain(self, email):
        arr = email.split('@')
        domain = arr[1];
        username = arr[0];
        user = self.service.RetrieveUser(username);
        return user.login.admin == "true";
