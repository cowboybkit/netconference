from django.conf.urls.defaults import *
from django.views.generic.simple import direct_to_template

urlpatterns = patterns('integrate.views',
    url(r'^setup/$', 'setup', name='integrate-setup'),
    url(r'^manage/$', 'manage', name='integrate-manage'),
    url(r'^get_price/$', 'get_price'),
    url(r'^google_docs/edit/$', 'google_docs_edit'),
    url(r'^google_docs/enter_gmail/$', 'enter_gmail'),
    url(r'^google_docs/export/$', 'google_docs_export'),
    url(r'^google_docs/import/$', 'google_docs_import'),
    url(r'^google_docs/export_one/$', 'google_docs_export_one'),
    #url(r'^test_ajax/$', 'test_ajax'),
    #url(r'^test_ajax_page/$', direct_to_template, {'template':'integrate/test_ajax_page.html'}),
    url(r'^integrate_openid_complete/$', 'integrate_openid_complete', name='integrate-openid-complete'),
)
