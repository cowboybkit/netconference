from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.models import User
from django_openid_auth.views import login_begin
from django.contrib.auth import REDIRECT_FIELD_NAME

from django.http import Http404
def admin_of_domain_require(view):
    def new_func(request, *args, **kwargs):
        try:
            domain = request.GET['domain']
        except:
            raise Http404
        try:
            callback = request.GET['callback']
        except:
            callback = "/integrate/manage/?domain=%s" %domain;
        if request.session.get('domain') and request.session.get('domain') == domain :
            return view(request, *args, **kwargs)
        request.session['callback'] = callback;
        return login_begin(request=request, login_complete_view='integrate-openid-complete')
    return new_func

def openid_required(view):
    def new_func(request, *args, **kwargs):
        #try:
            domain = request.GET['domain'];
            if request.session.has_key('requestor'):
                requestor = request.session['requestor'];
                if requestor is not None:
                    return view(request, *args, **kwargs);
            callback = request.GET.get('callback', "/");
            request.session['callback'] = callback;
            return login_begin(request=request, login_complete_view='integrate-openid-complete');
        #except:
        #    raise Http404;
    return new_func;
        
def google_apps_account_required(view):
    def new_func(request, *args, **kwargs):
        if request.user.is_from_google_apps() :
            return view(request, *args, **kwargs)
        raise Http404;
    return new_func

def openid_gmail_account_required(view):
    def new_func(request, *args, **kwargs):
        if request.user.is_from_google_apps() :
            return view(request, *args, **kwargs)
        else:
            return login_begin(request=request, login_complete_view='integrate-openid-complete', next="/integrate/google_docs/edit/")
    return new_func