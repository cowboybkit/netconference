from django import forms
from checkout.forms import *
import settings, operator
from integrate.models import GmailAccount
from integrate.utils import create_gmail_service;
from account.utils import COUNTRIES, STATES

class EnterGmailForm(forms.Form):
    gmail = forms.CharField(widget=forms.TextInput({"class": "textfield_login"}))
    password = forms.CharField(widget=forms.PasswordInput({"class": "textfield_login"}))
    def __init__(self, *args, **kwargs):
        super(EnterGmailForm, self).__init__(*args, **kwargs)
        # override default attributes
        for field in self.fields:
            self.fields[field].widget.attrs['size'] = '30'
    def save(self, user):
        GmailAccount.objects.create(user=user, gmail=self.cleaned_data['gmail'], password=self.cleaned_data['password']);
class MyMultiChoiceField(forms.Field):
    def validate(self, value):
        "Check if value consists only of valid emails."
        # Use the parent's handling of required fields, etc.
        super(MyMultiChoiceField, self).validate(value)
        #for email in value:
        #    validate_email(email)

from integrate.forms import MyMultiChoiceField
from subscription.models import Subscription

class UsersPaymentForm(forms.Form): 
    credit_card_number = forms.CharField(widget=forms.TextInput({"class": "textfield_login"}))
    credit_card_type = forms.CharField(widget=forms.Select(choices=CARD_TYPES))
    credit_card_expire_month = forms.CharField(widget=forms.Select(choices=cc_expire_months()))
    credit_card_expire_year = forms.CharField(widget=forms.Select(choices=cc_expire_years()))
    credit_card_cvv = forms.CharField(widget=forms.TextInput({"class": "textfield_login"}))
    #list_emails_checkbox = MyMultiChoiceField(widget=forms.CheckboxSelectMultiple(choices = []))
    subscription = forms.ModelChoiceField(queryset=Subscription.objects.none(), widget=forms.Select)
    phone_number = forms.CharField(widget=forms.TextInput({"class": "textfield_login"}))
    address = forms.CharField(widget=forms.TextInput({"class": "textfield_login"}))
    postal_code = forms.CharField(widget=forms.TextInput({"class": "textfield_login"}))
    country = forms.CharField(widget=forms.Select(choices=sorted(COUNTRIES, key=operator.itemgetter(1))))
    state = forms.CharField(widget=forms.Select(choices=sorted(STATES, key=operator.itemgetter(1))))
    city = forms.CharField(widget=forms.TextInput({"class": "textfield_login"}))
    firstname = forms.CharField(widget=forms.TextInput({"class": "textfield_login"}))
    lastname = forms.CharField(widget=forms.TextInput({"class": "textfield_login"}))
    def __init__(self, *args, **kwargs):
        super(UsersPaymentForm, self).__init__(*args, **kwargs)
        # override default attributes
        for field in self.fields:
            self.fields[field].widget.attrs['size'] = '30'
        
        self.fields['credit_card_type'].widget.attrs['size'] = '1'
        self.fields['credit_card_expire_year'].widget.attrs['size'] = '1'
        self.fields['credit_card_expire_month'].widget.attrs['size'] = '1'
        self.fields['credit_card_cvv'].widget.attrs['size'] = '5'
        self.fields['subscription'].widget.attrs['size'] = '1'
        self.fields['subscription'].queryset = Subscription.objects.filter(price__gt=0)
        self.fields['subscription'].empty_label = None
        self.fields['country'].widget.attrs['size'] = '1'
        self.fields['state'].widget.attrs['size'] = '1'
        self.fields['country'].widget.attrs['style'] = 'width:200px;'
    """def set_emails_choices(self, emails_choices):
        self.fields['list_emails_checkbox'].widget.choices = emails_choices
    def get_selected_emails(self):
        selected_items = self.cleaned_data['list_emails_checkbox']
        
    def clean_list_emails_checkbox(self):
        selected_list = self.cleaned_data['list_emails_checkbox'];
        if len(selected_list) == 0:
            raise forms.ValidationError("You must select at least an email");
        return selected_list;"""
    def clean_postal_code(self):
        postal_code = self.cleaned_data['postal_code'];
        if not(postal_code.isdigit()):
            raise forms.ValidationError("The postal code must be a number.")
        return postal_code;
    def clean_firstname(self):
        firstname = self.cleaned_data['firstname'];
        firstname = firstname.strip();
        if firstname == "":
            raise forms.ValidationError("First name must contain at least a char.")
        return firstname;
    def clean_lastname(self):
        lastname = self.cleaned_data['lastname'];
        lastname = lastname.strip();
        if lastname == "":
            raise forms.ValidationError("Last name must contain at least a char.")
        return lastname;
    def clean_credit_card_number(self):
        """ validate credit card number if valid per Luhn algorithm """
        cc_number = self.cleaned_data['credit_card_number']
        stripped_cc_number = strip_non_numbers(cc_number)
        if not cardLuhnChecksumIsValid(stripped_cc_number):
            raise forms.ValidationError('The credit card you entered is invalid.')
        return self.cleaned_data['credit_card_number']
    def get_price(self):
        subs_obj = self.cleaned_data['subscription'];
        return subs_obj.price;
    """def get_selected_users(self):
        entries = self.cleaned_data['list_emails_checkbox'];
        selected_users = list();
        from integrate.google_api import GoogleAppsUser;
        for entry in entries:
            temp = entry.split('$');
            item = GoogleAppsUser();
            item.email = temp[0];
            item.firstname = temp[1];
            item.lastname = temp[2];
            selected_users.append(item);
        return selected_users;"""
    def clean_phone_number(self):
        import re;
        phone = self.cleaned_data['phone_number']
        regx = "^(\d+$)"
        regx += "|^(\d+\sx\d+$)";
        regx += "|^(\d+\sext\d+$)"
        
        regx += "|^(\d+-\d+-\d+-\d+$)"
        regx += "|^(\d+-\d+-\d+-\d+\sx\d+$)"
        regx += "|^(\d+-\d+-\d+-\d+\sext\d+$)"
        
        regx += "|^(\d+.\d+.\d+.\d+$)"
        regx += "|^(\d+.\d+.\d+.\d+\sx\d+$)"
        regx += "|^(\d+.\d+.\d+.\d+\sext\d+$)"
        
        regx += "|^(\d+\s\(\d+\)\s\d+-\d+$)"
        regx += "|^(\d+\s\(\d+\)\s\d+-\d+\sx\d+$)"
        regx += "|^(\d+\s\(\d+\)\s\d+-\d+\sext\d+$)"
        
        regx += "|^(\d+/\d+/\d+/\d+$)"
        regx += "|^(\d+/\d+/\d+/\d+\sx\d+$)"
        regx += "|^(\d+/\d+/\d+/\d+\sext\d+$)"
        p = re.compile(regx);
        m = p.match(phone)
        stripped_phone = strip_non_numbers(phone)
        if m == None:
            raise forms.ValidationError('Enter a valid phone number with area code.')
        return self.cleaned_data['phone_number']
    
    def  clean(self):
        """ ignore cc errors is paying through paypal """
        cleaned_data = self.cleaned_data
        if cleaned_data.get("credit_card_type") == "PayPal":
            if 'credit_card_number' in self.errors:
                del self.errors['credit_card_number']
            if 'credit_card_cvv' in self.errors:
                del self.errors['credit_card_cvv']
        return cleaned_data
