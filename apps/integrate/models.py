from django.db import models
from django.contrib.auth.models import User
from subscription.models import Referer
from django.utils.translation import ugettext_lazy as _

class Domain(models.Model):
    name = models.CharField(max_length=100)
    def get_user_by_email(self, email):
        try:
            user_domain = UserOfDomain.objects.get(domain = self, user__email = email)
            return user_domain.user
        except:
            return None
    
    def get_users(self):
        users_of_domain = list()
        try:
            queryset = UserOfDomain.objects.filter(domain = self)
            for entry in queryset:
                users_of_domain.append(entry.user)             
        except:
            pass
        return users_of_domain
    def create_users(self, users, data):
        users_created = []
        for item in users:
            email = item.email
            firstname = item.firstname
            lastname = item.lastname
            try:
                user_domain = UserOfDomain.objects.get(user__email=email, domain=self)
                user = user_domain.user
            except:
                username = email.replace("@", "_")
                username = username.replace(".", "_")
                i = 1
                while True:
                    if i > 1:
                        username += str(i)
                    try:
                        User.objects.get(username__exact=username)
                    except User.DoesNotExist:
                        break
                    i += 1
                user = User.objects.create_user(username=username, email=email, password=None)
                user.first_name = firstname
                user.last_name = lastname
                user.save()
                user_profile = user.profile_set.get()
                user_profile.phone = data['phone_number']
                user_profile.billing_address_1 = data['address']
                user_profile.billing_country = data['country']
                if(data['country'] != "USA"):
                    state = None
                else:
                    state = data['state']
                user_profile.billing_state = state
                user_profile.billing_zip = data['postal_code']
                user_profile.save()
                user_domain = UserOfDomain.objects.create(user=user, domain=self)
                user_domain.save()
                if not Referer.objects.filter(user=user).count():
                    Referer.objects.create(user=user, 
                                           source="google_apps")
            users_created.append(user)
        return users_created
class UserOfDomain(models.Model):
    user = models.ForeignKey(User)
    domain = models.ForeignKey(Domain)
    
class GmailAccount(models.Model):
    user = models.ForeignKey(User)
    gmail = models.CharField(max_length=100)
    password = models.CharField(max_length=100)
    
class FileFromGoogle(models.Model):
    filename = models.CharField(_('filename'), max_length=100, null=True, blank=True)
    def __unicode__(self):
        return self.title
    
def is_from_google_apps(self):
    try:
        UserOfDomain.objects.get(user__username=self.username)
        return True
    except:
        return False
    
def get_gmail_account(self):
    try:
        gmail_account = GmailAccount.objects.get(user__username=self.username)
        return gmail_account
    except:
        return None
    
def get_domain(self):
    try:
        user_domain = UserOfDomain.objects.get(user=self)
        return user_domain.domain
    except:
        return None
    
User.add_to_class('is_from_google_apps', is_from_google_apps)
User.add_to_class('get_gmail_account', get_gmail_account)
User.add_to_class('get_domain', get_domain)
