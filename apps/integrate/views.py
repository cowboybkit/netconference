from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from integrate.integrate_restrictions import admin_of_domain_require, openid_required

from checkout import authnet
from subscription.models import Subscription
from django.core.urlresolvers import reverse

from integrate.forms import UsersPaymentForm  
import datetime
from integrate.utils import *
from django.contrib.auth.decorators import login_required
import tempfile

from integrate.integrate_restrictions import *
from integrate.models import Domain, UserOfDomain, FileFromGoogle
from google_api import DocsAPI, ProvisioningAPI
from files.models import UserFile
from netconf_utils.encode_decode import get_object_from_hash
import urllib
import gdata
import os.path
from integrate.forms import EnterGmailForm
from integrate.google_api import GoogleDocsEntry
from django.http import Http404
from gdata.service import RequestError
from netconference.http import HttpError
from gdata.apps.service import AppsForYourDomainException
from django.utils.translation import ugettext_lazy as _
from django.views.generic.list_detail import object_detail, object_list
from django.core.paginator import Paginator
from integrate import google_api
from django.contrib.sessions.models import Session
from integrate import integrate_restrictions as restrictions
import threading
import datetime
from django_openid_auth.views import parse_openid_response, default_render_failure, sanitise_redirect_url
from django.contrib.auth import (REDIRECT_FIELD_NAME, authenticate, login as auth_login)
from openid2.consumer.consumer import (Consumer, SUCCESS, CANCEL, FAILURE)
import simplejson as json
from integrate import utils
import os
import md5
from files.models import UserFile
from netconf_utils.encode_decode import *
consumer_key = settings.CONSUMER_KEY
consumer_secret = settings.CONSUMER_SECRET
server_duration = settings.INTEGRATE_SERVER_DURATION
client_duration = settings.INTEGRATE_CLIENT_DURATION

def init_data_to_render(request):
    domain = request.GET.get('domain')
    requestor = request.session.get('requestor');
    free_days = 30
    if request.session.get("has_60_coupon", False):
        free_days = 60
    trial_till = datetime.date.today() + datetime.timedelta(days = free_days)
    credit_card_failed = False
    form = UsersPaymentForm();
    is_new = True
    show_cc_form = True
    try:
        domain_obj = Domain.objects.get(name=domain)
        user = domain_obj.get_user_by_email(requestor.email);
        user_profile = user.profile_set.get()
        initial_form_data = {'firstname':user.first_name, 
                        'lastname':user.last_name, 
                        'phone_number':user_profile.phone,
                        'address':user_profile.billing_address_1,
                        'country':user_profile.billing_country,
                        'state':user_profile.billing_state,
                        'postal_code':user_profile.billing_zip,
                        }
        form = UsersPaymentForm(initial=initial_form_data)
    except:
        initial_form_data = {'firstname':requestor.firstname, 
                        'lastname':requestor.lastname, 
                        }
        form = UsersPaymentForm(initial=initial_form_data)
        
    data = {
        "trial_till": trial_till,
        "credit_card_failed": credit_card_failed,
        "requestor": requestor,
        "form":form,
        "is_new":is_new,
        "show_cc_form":show_cc_form,
    }
    return data
from integrate.google_api import GoogleAppsUser;
def do_integration(request, valid_form):
    domain = request.GET.get('domain')
    requestor = request.session.get('requestor');
    data = valid_form.cleaned_data
    #selected_users = valid_form.get_selected_users();
    selected_users = list();
    selected_users.append(requestor);
    
    data['sixty_day_trial'] = request.session.get("has_60_coupon", False)
    domain_obj = Domain.objects.get_or_create(name=domain)[0]
    print selected_users;
    created_users = domain_obj.create_users(selected_users, data) #create_users_from_list(domain, selected_users, data)   
   
    #Payments: Subscription via Auth.net ARB.           
    if data.get("credit_card_type") == "PayPal":
        return True, HttpResponseRedirect(reverse("paypal_payment", args=[1300]))
    result = do_payments(domain, created_users, data)
    if result:
        callback = request.REQUEST.get('callback')#request.session.get('callback')
        return True, HttpResponseRedirect(callback)
    else:
        return False, None
            
@openid_required  
def setup(request):
    domain = request.GET.get('domain')
    requestor = request.session.get('requestor');
    try:
        domain_obj = Domain.objects.get(name=domain);
        user = domain_obj.get_user_by_email(requestor.email);
        if user is not None:
            request.session['requestor'] = None;
            return HttpResponseRedirect("/openid/login/?domain=%s" %domain);
    except:
        pass
    data_to_render = init_data_to_render(request)
    form = data_to_render['form']
    if request.method == "POST":
        form = UsersPaymentForm(data = request.POST)
        if form.is_valid():  
            result, redirect = do_integration(request, form)
            if result:
                print "=============== integrate success";
                request.session['requestor'] = None;
                return redirect
            else:
                print "===============integrate failed"
                data_to_render['credit_card_failed'] = True
    data_to_render['form'] = form;
    return render_to_response("integrate/integrate_base_page.html", 
                              data_to_render, 
                              context_instance=RequestContext(request))

@admin_of_domain_require   
def manage(request):
    domain = request.session.get('domain')
    requestor_id = request.session.get('requestor_id')
    try:
        domain_obj = Domain.objects.get(name=domain)
    except Domain.DoesNotExist:
        raise HttpError("Domain does not exist")   
    data_to_render = init_data_to_render(request)
    form = data_to_render['form']
    if request.method == "POST":
        form = UsersPaymentForm(data = request.POST)
        if form.is_valid():  
            result, redirect = do_integration(request, form)
            if not result:
                data_to_render['credit_card_failed'] = True
    data_to_render['form'] = form
    return render_to_response("integrate/integrate_manage_page.html",
                              data_to_render,
                              context_instance=RequestContext(request))

def integrate_openid_complete(request, redirect_field_name=REDIRECT_FIELD_NAME,
                   render_failure=default_render_failure):
    redirect_to = request.REQUEST.get(redirect_field_name, '')
    openid_response = parse_openid_response(request)
    if not openid_response:
        raise HttpError("OpenID is not responded")  
    if openid_response.status == SUCCESS:
        #return HttpResponseRedirect(redirect_to)
        details = extract_user_details(openid_response)
        email = details['email'] or ''
        domain = email.split('@')[1]
        callback = request.session.get('callback')
        requestor = GoogleAppsUser();
        requestor.email = email;
        requestor.firstname = details['first_name'];
        requestor.lastname = details['last_name'];
        request.session['requestor'] = requestor;
        print '===================== integrate openid complete';
        return HttpResponseRedirect("/integrate/setup/?domain=%s&callback=%s" %(domain, callback))
    else:
        raise HttpError("OpenID Failed")

@login_required
def google_docs_export_one(request):
    user = request.user
    messages = dict()
    docs_api = DocsAPI()
    if user.is_from_google_apps():
        domain = user.get_domain()
        docs_api.two_legged_oauth(domain.name, user.email, consumer_key, consumer_secret)
    else:
        gmail_account = user.get_gmail_account()
        if gmail_account == None:
            return HttpResponseRedirect("/integrate/google_docs/enter_gmail/?next=/files/")
        else:
            docs_api.client_login(gmail_account.gmail, gmail_account.password)
    file_path_start = settings.DOWNLOAD_URL
    if request.method == "POST":
        urlhash = request.POST['file_urlhash']
        object_id = int(uri_b64decode(str(urlhash))) / 42
        user_file = get_object_or_404(UserFile.active_objects, pk=object_id)
        request.user.message_set.create(message="The selected files were exported.")
        try:
            docs_api.upload(user_file.get_file_path(), user_file.filename, user_file.source_type)
            messages[user_file.id] = "success"
        except RequestError, e:
                messages[user_file.id] = get_error_reason(e)
        url = reverse("my_files")
        return HttpResponseRedirect(url)
    
@login_required
def google_docs_export(request):
    user = request.user
    messages = dict()
    docs_api = DocsAPI()
    if user.is_from_google_apps():
        domain = user.get_domain()
        docs_api.two_legged_oauth(domain.name, user.email, consumer_key, consumer_secret)
    else:
        gmail_account = user.get_gmail_account()
        if gmail_account == None:
            return HttpResponseRedirect("/integrate/google_docs/enter_gmail/?next=/files/")
        else:
            docs_api.client_login(gmail_account.gmail, gmail_account.password)
    file_path_start = settings.DOWNLOAD_URL
    if request.method == "POST":
        selected_files_pk = request.POST.getlist("file-select[]")
        selected_files = UserFile.objects.filter(pk__in = selected_files_pk)
        request.user.message_set.create(message="The selected files were exported.")
        for user_file in selected_files:
            try:
                docs_api.upload(user_file.get_file_path(), user_file.filename, user_file.source_type)
                messages[user_file.id] = "success"
            except RequestError, e:
                messages[user_file.id] = get_error_reason(e)
    user_id = request.user.id
    files = UserFile.active_objects.filter(user=user_id, source_type__in = google_api.UPLOAD_FILE_TYPES)
    for file in files:
        if messages.has_key(file.id):
            file.message = messages[file.id]
    paginator = Paginator(files, settings.ITEMS_LIST_PAGINATE_BY)
    page = request.REQUEST.get('page', 1)
    page_obj = paginator.page(page)
    params = {
                'object_list':files,                
                'page_obj': page_obj, 
                'paginator':paginator
            }
    return render_to_response( "integrate/integrate_google_docs_export.html", 
                                params, 
                                context_instance=RequestContext(request))

@login_required
def enter_gmail(request):
    next = request.GET.get('next') or "/files/"
    enter_fail = False
    form = EnterGmailForm()
    if request.method == "POST":
        form = EnterGmailForm(data=request.POST)
        if form.is_valid():
            gmail = form.cleaned_data['gmail']
            password = form.cleaned_data['password']
            try:
                service = create_gmail_service(gmail, password)
                form.save(request.user)
                return HttpResponseRedirect(next)
            except:
                enter_fail = True
    return render_to_response( "integrate/integrate_enter_gmail.html", 
                                {'form':form, 'enter_fail':enter_fail}, 
                                context_instance=RequestContext(request))

@login_required
def google_docs_import(request):
    user = request.user
    docs_api = DocsAPI()
    file_from_google = FileFromGoogle();
    
    if user.is_from_google_apps():
        domain = UserOfDomain.objects.get(user=user).domain
        docs_api.two_legged_oauth(domain.name, user.email, consumer_key, consumer_secret)
    else:
        gmail_account = user.get_gmail_account()
        if gmail_account == None:
            return HttpResponseRedirect("/integrate/google_docs/enter_gmail/?next=/integrate/google_docs/import/")
        else:
            docs_api.client_login(gmail_account.gmail, gmail_account.password)
    
    messages = dict()
    ''' import multiple files '''
    if request.method == "POST":
        selected_items = request.POST.getlist('file-select[]')
        for item in selected_items:
            entry = GoogleDocsEntry()
            entry.from_string(item)
            title = entry.get_title();
            try:
                all_filename = FileFromGoogle.objects.get(filename=title)
                file = UserFile.objects.get(filename=title, user = user)
                temp_file_path = docs_api.download(user, entry, file.filename)  
                docs_api.download(user, entry, title)
                utils.update_file(file, temp_file_path, user)
                
            except:
                try:
                    docs_api.download_docs(user, entry)
                    file_from_google.filename = title
                    file_from_google.save();
                    messages[entry.get_id()] = "success"
                except RequestError, e:
                    messages[entry.get_id()] = get_error_reason(e)
            
    try:
        entries = docs_api.retrieve_all_docs()
    except RequestError, e:
        raise HttpError(get_error_reason(e))
    for entry in entries:
        if messages.has_key(entry.get_id()):
            entry.message = messages[entry.get_id()]

    paginator = Paginator(entries, settings.ITEMS_LIST_PAGINATE_BY)
    page = request.REQUEST.get('page', 1)
    page_obj = paginator.page(page)
    params = {
                'entries':entries,                
                'page_obj': page_obj, 
                'paginator':paginator
            }
   
    return render_to_response( "integrate/integrate_google_docs_import.html", 
                                params, 
                                context_instance=RequestContext(request))

@login_required  
def google_docs_edit(request):
    urlhash = request.REQUEST.get('urlhash')
    if request.is_ajax():
        action = request.REQUEST.get('action')
        session_key = md5.new(urlhash).hexdigest()
        status = "success"
        session_dict = {}
        #Session.objects.get(pk=session_key).delete()
        try:
            session_obj = Session.objects.get(pk=session_key)
            session_dict = session_obj.get_decoded()
            entry = session_dict['entry']
            session_dict['last_request'] = datetime.datetime.utcnow()
        except Session.DoesNotExist:
            ''' first time'''
            try:
                docs_api = utils.create_default_docs_api()
                user_file = get_object_from_hash(UserFile, urlhash)
                entry = docs_api.upload(user_file.get_file_path(), user_file.filename, user_file.source_type)
                docs_api.publish(entry)
                session_dict =     {
                                       'urlhash' : urlhash, 
                                       'entry':entry, 
                                       'last_request': datetime.datetime.utcnow(),
                                       'last_sync': datetime.datetime.utcnow(),
                                   }
                expire_date = Session.objects.get(pk=request.session.session_key).expire_date
                session_obj = Session.objects.create(session_key=session_key, expire_date=expire_date)
                call_sync(urlhash, request.user, session_key)
            except RequestError, e:
                status = "failure"
                reason = e.args[0]['reason']
        Session.objects.save(session_key, session_dict, session_obj.expire_date)
        data = {'status':status}
        if status == "failure":
            data['reason'] = reason
        if action == "load":
            data['embeded_link'] = entry.get_embeded_link()
        data = json.dumps(data)
        return HttpResponse(data, mimetype="application/json")
    else:
        params = { 'urlhash': urlhash,'client_duration':client_duration }
        return render_to_response( "integrate/integrate_google_docs_edit.html", 
                                    params, 
                                    context_instance=RequestContext(request))
def call_sync(urlhash, user, session_key):
    args = [urlhash, user, session_key]
    t = threading.Timer(server_duration, synchronous_edit_file, args)
    t.start()
def synchronous_edit_file(*args, **kwargs):
    urlhash = args[0]
    user = args[1]
    session_key = args[2]
    user_file = get_object_from_hash(UserFile, urlhash)
    session_obj = Session.objects.get(pk=session_key)
    session_dict = session_obj.get_decoded()
    entry = session_dict['entry']
    last_sync = datetime.datetime.utcnow()
    docs_api = utils.create_default_docs_api()
    temp_file_path = docs_api.download(user, entry, user_file.filename)
    utils.update_file(user_file, temp_file_path, user)
    
    session_obj = Session.objects.get(pk=session_key)
    session_dict = session_obj.get_decoded()
    session_dict['last_sync'] = last_sync
    if session_dict['last_request'] <= session_dict['last_sync']:
        docs_api.delete(entry)
        session_obj.delete()
    else:
        call_sync(urlhash, user, session_key)
        Session.objects.save(session_key, session_dict, session_obj.expire_date)

def print_debug(urlhash, session_key):
    session_dict = Session.objects.get(pk=session_key).get_decoded()
    print "============================last request: %s" %session_dict[urlhash]['last_request']
    print "============================== last sync: %s" %session_dict[urlhash]['last_sync']
    print "============================== sync time: %s" %(session_dict[urlhash]['last_sync'] + datetime.timedelta(seconds=server_duration))
    print "==================================== now: %s" %datetime.datetime.utcnow()
def get_price(request):
    if request.is_ajax():
        subs_id = request.REQUEST.get('subs_id')
        try:
            subs_obj = Subscription.objects.get(pk=subs_id)
            price = subs_obj.price
            status = "success"
        except:
            status = "failure"
        data = {'status':status}
        if status == "success":
            data['price'] = str(price) + "$"
        data = json.dumps(data)
        return HttpResponse(data, mimetype="application/json")
    else:
        raise Http404
def test_ajax(request):
    request.session['requestor'] = None;
    d = dict();
    return HttpResponse("Delete requestor session success")