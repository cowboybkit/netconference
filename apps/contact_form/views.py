"""
View which can render and send email from a contact form.

"""

from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext

from contact_form.forms import ContactForm
from contact_form.forms import AkismetContactForm
from settings import INVEST_MAIL_RECIPIENT, DEFAULT_FROM_EMAIL
from mailer import send_html_mail as send_mail
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from django.template import loader, Context
from django.utils.translation import ugettext_lazy as _
from django.contrib.sites.models import Site
from account.views import assign_free_subscription
from checkout.models import ActiveTransaction
from checkout import authnet
import recurly
from subscription.models import UserSubscription

def contact_form(request, form_class=ContactForm,
                 template_name='contact_form/contact_form.html',
                 success_url=None, extra_context=None,
                 fail_silently=False):
    """
    Render a contact form, validate its input and send an email
    from it.

    **Optional arguments:**

    ``extra_context``
        A dictionary of variables to add to the template context. Any
        callable object in this dictionary will be called to produce
        the end result which appears in the context.

    ``fail_silently``
        If ``True``, errors when sending the email will be silently
        supressed (i.e., with no logging or reporting of any such
        errors. Default value is ``False``.

    ``form_class``
        The form to use. If not supplied, this will default to
        ``contact_form.forms.ContactForm``. If supplied, the form
        class must implement a method named ``save()`` which sends the
        email from the form; the form class must accept an
        ``HttpRequest`` as the keyword argument ``request`` to its
        constructor, and it must implement a method named ``save()``
        which sends the email and which accepts the keyword argument
        ``fail_silently``.

    ``success_url``
        The URL to redirect to after a successful submission. If not
        supplied, this will default to the URL pointed to by the named
        URL pattern ``contact_form_sent``.

    ``template_name``
        The template to use for rendering the contact form. If not
        supplied, defaults to
        :template:`contact_form/contact_form.html`.

    **Context:**

    ``form``
        The form instance.
    
    **Template:**

    The value of the ``template_name`` keyword argument, or
    :template:`contact_form/contact_form.html`.

    """
    #
    # We set up success_url here, rather than as the default value for
    # the argument. Trying to do it as the argument's default would
    # mean evaluating the call to reverse() at the time this module is
    # first imported, which introduces a circular dependency: to
    # perform the reverse lookup we need access to contact_form/urls.py,
    # but contact_form/urls.py in turn imports from this module.
    #
    
    if success_url is None:
        success_url = reverse('contact_form_sent')
    data = {}
    if request.user.is_authenticated():
        if "name" not in data:
            data.update({"name": request.user.get_full_name() 
                         or request.user.username})
        if "email" not in data:
            data.update({"email": request.user.email})
    if request.method == 'POST':
        sdata = request.POST.copy()
        if data:
            sdata.update(data)
        form = form_class(data=sdata, 
                          files=request.FILES, 
                          request=request,
                          initial=sdata)
        if form.is_valid():
            form.save(fail_silently=fail_silently)
            return HttpResponseRedirect(success_url)
    else:
        form = form_class(request=request, initial=data)

    if extra_context is None:
        extra_context = {}
    context = RequestContext(request)
    for key, value in extra_context.items():
        context[key] = callable(value) and value() or value
    
    return render_to_response(template_name,
                              { 'form': form,
                                'page_title': _("Contact Us")},
                              context_instance=context)



def invest(request):
    contact_form = AkismetContactForm(request=request,data=request.POST or None)
    
    if contact_form.is_valid():
        
        send_dict =  contact_form.get_message_dict()
        send_dict["message_html"] = send_dict["message"]
        send_dict['recipient_list'] = INVEST_MAIL_RECIPIENT,
        send_mail(**send_dict)
        return redirect('contact_form_sent')
        
        
    return render_to_response('contact_form/invest.html',
                              {'form':contact_form},
                              RequestContext(request))


@login_required
def cancel_contact_form(request, form_class=ContactForm,
                 template_name='contact_form/contact_form.html',
                 success_url=None, extra_context=None,
                 fail_silently=False):

    
    if success_url is None:
        success_url = reverse('contact_form_sent')
    if request.method == 'POST':
        data = request.POST.copy()
        data.update({'name': request.user.get_full_name() or request.user.username, 
                     'email': request.user.email})
        form = form_class(data=data, files=request.FILES, request=request, initial=data)
        form.recipient_list = ["jason@illusionfactory.com"]
        if form.is_valid():
            form.save(fail_silently=fail_silently)
            user_sub = UserSubscription.active_objects.filter(user=request.user)[0]
            recurly_sub_id = user_sub.recurly_subscription_uuid
            if recurly_sub_id:
                context = Context({'username':request.user.username, 'expiry_date':user_sub.expires})
                subject = "NetConference Cancellation"
                if user_sub.is_paid:
                    message = loader.get_template("autoresponders/nc_cancellationWO_Refund.html").render(context)
                    text_part = loader.get_template("autoresponders/nc_cancellationWO_Refund.txt").render(context)
                else:
                    message = loader.get_template("autoresponders/nc_cancel30daytrial.html").render(context)
                    text_part = loader.get_template("autoresponders/nc_cancel30daytrial.txt").render({})
                subscription = recurly.Subscription.get(recurly_sub_id)
                subscription.cancel()
                request.user.message_set.create(message=_("Your account has been queued for auto-cancellation."))
            else:
                context = RequestContext(request, {'username': data['name'],
                                                   "site": Site.objects.get_current(),})
                message = loader.get_template("account/cancel.html").render(context)
                subject = "Account Cancellation"
                text_part = ""
                try:
                    active_txn = ActiveTransaction.objects.get(user=request.user, is_active=True)
                except ActiveTransaction.DoesNotExist:
                    active_txn = None
                if active_txn:
                    response = authnet.cancel_subscription(active_txn.transaction)
                    if not response[0]:
                        message += "<div><strong>Message from the system</strong>: This account could not be auto-cancelled.</div>"
                        subject = "[Cancellation Failed] %s" % subject
                        request.user.message_set.create(message=_("Your account could not be auto-cancelled. A representative will get in touch with you shortly."))
                    else:
                        user_sub = request.user.usersubscription_set.filter(active=True, cancelled=False).order_by('-expires')
                        if user_sub.count():
                            user_sub[0].active = False
                            user_sub[0].cancelled = True
                            user_sub[0].save()
                            assign_free_subscription(request.user)
                        request.user.message_set.create(message=_("Your account has been queued for auto-cancellation."))
            send_mail(subject, text_part, message, DEFAULT_FROM_EMAIL, [request.user.email, ])
            return HttpResponseRedirect(success_url)
    else:
        form = form_class(request=request)

    if extra_context is None:
        extra_context = {}
    context = RequestContext(request)
    for key, value in extra_context.items():
        context[key] = callable(value) and value() or value
    
    return render_to_response(template_name,
                              { 'form': form,
                                'page_title': _("Account cancellation")},
                              context_instance=context)

