# Create your views here.
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.models import User
from account.models import Account

def unsubscribe_from_autoresponder(request):
    if request.method == 'POST':
        message = ''
        email_address = request.POST['email_address']
        try:
            user = User.objects.get(email=email_address)
            account = Account.objects.get(user=user)
            account.send_autoresponder_mails = False
            account.save()
        except User.DoesNotExist:
            message = "There is no user with the provided email address in our mailing list."
        return render_to_response("autoresponders/unsubscribe_result.html", {'message':message, 'email_address':email_address}, context_instance=RequestContext(request))
    return render_to_response("autoresponders/unsubscribe.html", {}, context_instance=RequestContext(request))
