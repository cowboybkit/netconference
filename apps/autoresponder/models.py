from django.db import models

# Create your models here.
class Autoresponder(models.Model):
    title = models.CharField(max_length=255, null=True)
    html_part = models.TextField()
    text_part = models.TextField()
    on_day = models.PositiveIntegerField()

    def __unicode__(self):
        return self.title
