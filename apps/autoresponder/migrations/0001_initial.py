# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding model 'Autoresponder'
        db.create_table('autoresponder_autoresponder', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('html_part', self.gf('django.db.models.fields.TextField')()),
            ('text_part', self.gf('django.db.models.fields.TextField')()),
            ('on_day', self.gf('django.db.models.fields.PositiveIntegerField')()),
        ))
        db.send_create_signal('autoresponder', ['Autoresponder'])


    def backwards(self, orm):
        
        # Deleting model 'Autoresponder'
        db.delete_table('autoresponder_autoresponder')


    models = {
        'autoresponder.autoresponder': {
            'Meta': {'object_name': 'Autoresponder'},
            'html_part': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'on_day': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'text_part': ('django.db.models.fields.TextField', [], {})
        }
    }

    complete_apps = ['autoresponder']
