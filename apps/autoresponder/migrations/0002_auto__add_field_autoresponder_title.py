# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding field 'Autoresponder.title'
        db.add_column('autoresponder_autoresponder', 'title', self.gf('django.db.models.fields.CharField')(max_length=255, null=True), keep_default=False)


    def backwards(self, orm):
        
        # Deleting field 'Autoresponder.title'
        db.delete_column('autoresponder_autoresponder', 'title')


    models = {
        'autoresponder.autoresponder': {
            'Meta': {'object_name': 'Autoresponder'},
            'html_part': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'on_day': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'text_part': ('django.db.models.fields.TextField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'})
        }
    }

    complete_apps = ['autoresponder']
