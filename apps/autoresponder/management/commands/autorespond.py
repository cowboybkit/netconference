from django.core.management.base import  NoArgsCommand
import datetime

from autoresponder.models import Autoresponder
from django.contrib.auth.models import User
from subscription.models import Subscription, UserSubscription
from mailer import send_html_mail
from django.template.loader import Template, Context

class Command(NoArgsCommand):
    help = "Send autoresponder mails on certain days after signup"

    def handle_noargs(self, **kwargs):
        """This management command will be used to send autoresponder mails.
        If you want an autoresponder mail to be sent on the same day as signup, 
        set on_day as 0. If you want an autoresponder mail to be sent on first
        day after signup, set on_day as 1""" 

        """This management command will be running at 12:05 am server time everyday. 
        This is done to capture all the users on the previous day. 
        So, on 12:05 am, 20th July autoresponders set for day 0 will be sent to all the users who signed up
        on 19th July. 
        Although its being sent a day after, but 5 minutes does not make such a difference"""

        autoresponders = Autoresponder.objects.all()
        today_date = datetime.date.today()
        yesterday_date = today_date + datetime.timedelta(-1)
        for each_responder in autoresponders:
            user_since = yesterday_date - datetime.timedelta(each_responder.on_day)
            users = User.objects.filter(date_joined__year=user_since.year, date_joined__month=user_since.month, date_joined__day=user_since.day)
            if users:
                for each_user in users:
                    context = Context({"user":each_user})
                    text_part = Template(each_responder.text_part).render(context)
                    html_part = Template(each_responder.html_part).render(context)
                    send_html_mail(each_responder.title, text_part, html_part, "noreply@netconference.com", [each_user.email])
