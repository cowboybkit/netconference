from django.conf.urls.defaults import *

urlpatterns = patterns('autoresponder.views',
    url(r'^unsubscribe/$', 'unsubscribe_from_autoresponder', name='unsubscribe_from_autoresponder'),
)
