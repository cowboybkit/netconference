from django.conf import settings

from friends.models import Contact,ContactEmail

import vobject
import ybrowserauth
import simplejson
from gdata.contacts import ContactsFeedFromString
from django.utils.translation import ugettext_lazy as _
import csv, tempfile, urllib
from django.http import HttpResponseRedirect, HttpResponseForbidden, Http404, HttpResponse

def import_vcards(stream, user):
    """
    Imports the given vcard stream into the contacts of the given user.
    
    Returns a tuple of (number imported, total number of cards).
    """
    
    total = 0
    imported = 0
    try:
        for card in vobject.readComponents(stream):
            total += 1
            try:
                name = card.fn.value
                email = card.email.value
                try:
                    Contact.objects.get(user=user, email=email)
                except Contact.DoesNotExist:
                    Contact(user=user, name=name, email=email).save()
                    imported += 1
                except Contact.MultipleObjectsReturned:
                    Contact.objects.filter(user=user, email=email)[0].delete()
            except AttributeError:
                pass
    except vobject.base.ParseError:
        stream.seek(0)
        tmp_file = tempfile.NamedTemporaryFile()
        file_name = tmp_file.name
        tmp_file.write(stream.read())
        tmp_file.flush()
        tmp_file_2 = open(file_name, 'r')
        csv_lines = tmp_file_2.readlines()
        tmp_file_2.close()
        csv_cols = ','.join(csv_lines[:1]).lower()
        csv_stream = [csv_cols] + csv_lines[1:]
        tmp_file.close()
        try:
            cards = [ii for ii in csv.DictReader(csv_stream)]
        except:
            cards = []
        for card in cards:
            total += 1
            try:
                first_name = card['first name']
                last_name = card['last name']
                email = card['e-mail']
                phone = card['phone number']
            except KeyError:
                continue
            if not email:
                continue
            try:
                contact = Contact.objects.get(user=user, email=email)
            except Contact.DoesNotExist:
                contact = Contact(user=user, email=email)
            contact.name = "%s %s" % (first_name, last_name)
            contact.email = email
            contact.phone = phone
            contact.save()
            imported += 1
    return imported, total

def import_yahoo(bbauth_token, user):
    """
    Uses the given BBAuth token to retrieve a Yahoo Address Book and
    import the entries with an email address into the contacts of the
    given user.
    
    Returns a tuple of (number imported, total number of entries).
    """
    
    ybbauth = ybrowserauth.YBrowserAuth(settings.BBAUTH_APP_ID, settings.BBAUTH_SHARED_SECRET)
    ybbauth.token = bbauth_token
    address_book_json = ybbauth.makeAuthWSgetCall("http://address.yahooapis.com/v1/searchContacts?format=json&email.present=1&fields=name,email")
    address_book = simplejson.loads(address_book_json)
    
    total = 0
    imported = 0
    
    for contact in address_book["contacts"]:
        total += 1
        email = contact['fields'][0]['data']
        try:
            first_name = contact['fields'][1]['first']
        except (KeyError, IndexError):
            first_name = None
        try:
            last_name = contact['fields'][1]['last']
        except (KeyError, IndexError):
            last_name = None
        if first_name and last_name:
            name = first_name + " " + last_name
        elif first_name:
            name = first_name
        elif last_name:
            name = last_name
        else:
            name = None
        try:
            Contact.objects.get(user=user, email=email)
        except Contact.DoesNotExist:
            Contact(user=user, name=name, email=email).save()
            imported += 1
    
    return imported, total

def split_name(fullname):
    firstname = ''
    lastname = ''
    try:
        arr = fullname.split(' ') 
        if len(arr)>0:
            lastname = arr[len(arr)-1]
            firstname = fullname[:(len(fullname)-len(lastname))].strip()
        else:
            lastname = fullname
            firstname = ''
    except:
        pass
    return firstname, lastname

def create_contact_emails(user,firstname, lastname, emails, phone):
    contact = Contact(user = user, name=firstname, last_name = lastname, email=emails[0], phone = phone)
    contact.save()
    if len(emails)>1:
        for i in range(1, len(emails)-1):
            ContactEmail(contact= contact, label='email google', email = emails[i]).save()

def import_google(oauth_token, user):
    """
    Uses the given AuthSub token to retrieve Google Contacts and
    import the entries with an email address into the contacts of the
    given user.
    
    Returns a tuple of (number imported, total number of entries).
    """
    
    contacts_url = "https://www.google.com/m8/feeds/contacts/default/full?oauth_token=%s" % oauth_token
    entries = []
    feed = ContactsFeedFromString(urllib.urlopen(contacts_url).read())
    entries.extend(feed.entry)
    next_link = feed.GetNextLink()
    while next_link:
        next_url = "%s&oauth_token=%s" %(next_link.href, oauth_token)
        print next_url
        feed = ContactsFeedFromString(urllib.urlopen(next_url).read())
        entries.extend(feed.entry)
        next_link = feed.GetNextLink()
    total = 0
    imported = 0
    for entry in entries:
        name = entry.title.text
        firstname = ''
        lastname = ''
        if name:
            name = name.decode("utf-8")
            firstname, lastname = split_name(name)
        emails_array = []
        for x in entry.email:
            if x.address:
                emails_array.append(x.address)
        #=======================================================================
        # for e in entry.email:
        #    email = e.address
        #    if not email:
        #        continue
        #    total += 1
        #    try:
        #        exists_contact = Contact.objects.get(user=user, email=email)
        #    except Contact.DoesNotExist:
        #        Contact(user=user, name=name, email=email).save()
        #        imported += 1
        #    except Contact.MultipleObjectsReturned:
        #        pass
        #=======================================================================
        create_contact_emails(user, firstname, lastname, emails_array, None)
        total+=1
        imported +=1
    return imported, total