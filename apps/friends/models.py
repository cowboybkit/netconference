import datetime
from random import random

from django.db import models
from django.template.loader import render_to_string
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.utils.hashcompat import sha_constructor
from django.db.models import signals
from django.conf import settings

from friends_app.models import ContactGroup
from netconf_utils.encode_decode import *

#from django.core.management.validation import max_length

# favour django-mailer but fall back to django.core.mail
if "mailer" in settings.INSTALLED_APPS:
    from mailer import send_html_mail as send_mail
else:
    from django.core.mail import send_mail

if "notification" in settings.INSTALLED_APPS:
    from notification import models as notification
else:
    notification = None

if "emailconfirmation" in settings.INSTALLED_APPS:
    from emailconfirmation.models import EmailAddress
else:
    EmailAddress = None

class Contact(models.Model):
    """
    A contact is a person known by a user who may or may not themselves
    be a user.
    """

    # the user who created the contact
    user = models.ForeignKey(User, related_name="contacts")

    name = models.CharField(max_length=100, null=True, blank=True)
    last_name = models.CharField(max_length=100, null=True, blank=True)
    email = models.EmailField(null=True, blank=True)
    phone = models.CharField(max_length=100, null=True, blank=True)
    added = models.DateField(default=datetime.date.today)
    
    # the user(s) this contact corresponds to
    users = models.ManyToManyField(User, related_name="contact_owners")
    
    # the contact group(s) this contact corresponds to
    groups = models.ManyToManyField(ContactGroup, related_name="contact_groups", null=True, blank=True)
    uuid = models.CharField(max_length=36, blank=True, default =None, null =True)
    source_type = models.CharField(('source_type'), max_length=16, null=True, blank=True)
    
    def urlhash(self):
        return uri_b64encode(str(self.pk * 42))
    
    def __unicode__(self):
        return "%s (%s's contact)" % (self.email, self.user)

    class Meta:
        ordering = ["name"]
        
    def ellipses_full_name(self):
        first_name = self.name
        last_name = self.last_name
        text = ''
        if first_name and last_name:
            count_first_name = first_name.count(' ')
            count_last_name = last_name.count(' ')
            if count_first_name == 0 and count_last_name == 0:
                text = first_name + ' ' + last_name
            elif count_first_name > 0 and count_last_name == 0:
                text = first_name.split(' ')[0] + '...' + last_name
            elif count_first_name == 0 and count_last_name > 0:
                text = first_name + '...' + last_name.split(' ')[-1]
            elif count_first_name > 0 and count_last_name > 0:
                text = first_name.split(' ')[0] + '...' + last_name.split(' ')[-1]
        elif first_name:
            count_first_name = first_name.count(' ')
            if count_first_name > 0:
                text = first_name.split(' ')[0]
            else: text = first_name
        else: 
            count_last_name = last_name.count(' ')
            if count_last_name > 0:
                text = last_name.split(' ')[-1]
            else: text = last_name
        return text
    
    def get_contact_name(self):
        first_name = ''
        last_name = ''
        if self.name:
            first_name = self.name
        if self.last_name:
            last_name = self.last_name
        
        if last_name:
            full_name = first_name + ' ' + last_name
        else:
            full_name = first_name
        return full_name
            
       
def __get_contact_emails(contact):
    contact_emails = ContactEmail.objects.filter(contact=contact)
    return contact_emails
Contact.add_to_class('get_contact_emails', __get_contact_emails)
    
def __get_contact_phones(contact):
    contact_phones = ContactPhone.objects.filter(contact=contact)
    return contact_phones
Contact.add_to_class('get_contact_phones', __get_contact_phones)

def __get_contact_phone_string(contact):
    contact_phones = ContactPhone.objects.filter(contact=contact)
    phone_names = [x.phone for x in contact_phones if x]
    str_phone = ', '.join(phone_names) or ''
    return str_phone
Contact.add_to_class('get_contact_phone_string', __get_contact_phone_string)

def __get_contact_email_string(contact):
    contact_emails = ContactEmail.objects.filter(contact=contact)
    email_names = [x.email for x in contact_emails if x]
    str_email = ', '.join(email_names) or ''
    return str_email
Contact.add_to_class('get_contact_email_string', __get_contact_email_string)

def __get_contact_groups_string(contact):
    contact_groups = contact.groups.all()
    group_names = [x.name for x in contact_groups if x]
    str_group = ', '.join(group_names) or ''
    return str_group
Contact.add_to_class('get_contact_groups_string', __get_contact_groups_string)

def __get_groups_in_contact(contact):
    contact_groups = contact.groups.all()
    return contact_groups
Contact.add_to_class('get_groups_in_contact', __get_groups_in_contact)

def __count_contacts_in_group(group):
    contact = Contact.objects.filter(groups = group)
    count = len(contact)
    return count
ContactGroup.add_to_class('count_contacts_in_group', __count_contacts_in_group)
        
class ContactEmail(models.Model):
  contact = models.ForeignKey(Contact, related_name="contact email")
  label = models.CharField(max_length=255, null=False, blank=False)
  email = models.EmailField()

class ContactPhone(models.Model):
  contact = models.ForeignKey(Contact, related_name="contact phone")
  label = models.CharField(max_length=255, null=True, blank=True)
  phone = models.CharField(max_length=100, null=True, blank=True) 
  

class FriendshipManager(models.Manager):
    def friends_for_user(self, user):
        friends = []
        for friendship in self.filter(from_user=user).select_related(depth=1):
            friends.append({"friend": friendship.to_user, "friendship": friendship})
        for friendship in self.filter(to_user=user).select_related(depth=1):
            friends.append({"friend": friendship.from_user, "friendship": friendship})
        return friends
    
    def are_friends(self, user1, user2):
        if self.filter(from_user=user1, to_user=user2).count() > 0:
            return True
        if self.filter(from_user=user2, to_user=user1).count() > 0:
            return True
        return False

    def remove(self, user1, user2):
        if self.filter(from_user=user1, to_user=user2):
            friendship = self.filter(from_user=user1, to_user=user2)
        elif self.filter(from_user=user2, to_user=user1):
            friendship = self.filter(from_user=user2, to_user=user1)
        friendship.delete()


class Friendship(models.Model):
    """
    A friendship is a bi-directional association between two users who
    have both agreed to the association.
    """
    
    to_user = models.ForeignKey(User, related_name="friends")
    from_user = models.ForeignKey(User, related_name="_unused_")
    # @@@ relationship types
    added = models.DateField(default=datetime.date.today)
    
    objects = FriendshipManager()
    
    class Meta:
        unique_together = (('to_user', 'from_user'),)


def friend_set_for(user):
    return set([obj["friend"] for obj in Friendship.objects.friends_for_user(user)])


INVITE_STATUS = (
    ("1", "Created"),
    ("2", "Sent"),
    ("3", "Failed"),
    ("4", "Expired"),
    ("5", "Accepted"),
    ("6", "Declined"),
    ("7", "Joined Independently"),
    ("8", "Deleted")
)

class JoinInvitationManager(models.Manager):
    
    def send_invitation(self, from_user, to_email, message):
        contact, created = Contact.objects.get_or_create(email=to_email, user=from_user)
        salt = sha_constructor(str(random())).hexdigest()[:5]
        confirmation_key = sha_constructor(salt + to_email).hexdigest()
        accept_url = u"http://%s%s" % (
            unicode(Site.objects.get_current()),
            reverse("friends_accept_join", args=(confirmation_key,)),
        )
        
        subject = render_to_string("friends/join_invite_subject.txt")
        email_message = render_to_string("friends/join_invite_message.txt", {
            "user": from_user,
            "message": message,
            "accept_url": accept_url,
            "site": Site.objects.get_current(),
            "MEDIA_URL": settings.MEDIA_URL,
        })
        
        send_mail(subject, "", email_message, from_user.email or settings.DEFAULT_FROM_EMAIL, [to_email])        
        return self.create(from_user=from_user, contact=contact, message=message, status="2", confirmation_key=confirmation_key)


class JoinInvitation(models.Model):
    """
    A join invite is an invitation to join the site from a user to a
    contact who is not known to be a user.
    """
    
    from_user = models.ForeignKey(User, related_name="join_from")
    contact = models.ForeignKey(Contact)
    message = models.TextField()
    sent = models.DateField(default=datetime.date.today)
    status = models.CharField(max_length=1, choices=INVITE_STATUS)
    confirmation_key = models.CharField(max_length=40)
    
    objects = JoinInvitationManager()
    
    def accept(self, new_user):
        # mark invitation accepted
        self.status = "5"
        self.save()
        # auto-create friendship
        friendship = Friendship(to_user=new_user, from_user=self.from_user)
        friendship.save()
        # notify
        if notification:
            notification.send([self.from_user], "join_accept", {"invitation": self, "new_user": new_user})
            friends = []
            for user in friend_set_for(new_user) | friend_set_for(self.from_user):
                if user != new_user and user != self.from_user:
                    friends.append(user)
            notification.send(friends, "friends_otherconnect", {"invitation": self, "to_user": new_user})

class FriendshipInvitationManager(models.Manager):
    def invitations(self, *args, **kwargs):
        return self.filter(*args, **kwargs).exclude(status__in=["6", "8"])

class FriendshipInvitation(models.Model):
    """
    A frienship invite is an invitation from one user to another to be
    associated as friends.
    """
    
    from_user = models.ForeignKey(User, related_name="invitations_from")
    to_user = models.ForeignKey(User, related_name="invitations_to")
    message = models.TextField()
    sent = models.DateField(default=datetime.date.today)
    status = models.CharField(max_length=1, choices=INVITE_STATUS)
    
    objects = FriendshipInvitationManager()

    def accept(self):
        if not Friendship.objects.are_friends(self.to_user, self.from_user):
            friendship = Friendship(to_user=self.to_user, from_user=self.from_user)
            friendship.save()
            self.status = "5"
            self.save()
            if notification:
                notification.send([self.from_user], "friends_accept", {"invitation": self})
                notification.send([self.to_user], "friends_accept_sent", {"invitation": self})
                for user in friend_set_for(self.to_user) | friend_set_for(self.from_user):
                    if user != self.to_user and user != self.from_user:
                        notification.send([user], "friends_otherconnect", {"invitation": self, "to_user": self.to_user})

    def decline(self):
        if not Friendship.objects.are_friends(self.to_user, self.from_user):
            self.status = "6"
            self.save()

class FriendshipInvitationHistory(models.Model):
    """
    History for friendship invitations
    """
    
    from_user = models.ForeignKey(User, related_name="invitations_from_history")
    to_user = models.ForeignKey(User, related_name="invitations_to_history")
    message = models.TextField()
    sent = models.DateField(default=datetime.date.today)
    status = models.CharField(max_length=1, choices=INVITE_STATUS)

# @@@ this assumes email-confirmation is being used
def new_user(sender, instance, **kwargs):
    if instance.verified:
        for join_invitation in JoinInvitation.objects.filter(contact__email=instance.email):
            if join_invitation.status not in ["5", "7"]: # if not accepted or already marked as joined independently
                join_invitation.status = "7"
                join_invitation.save()
                # notification will be covered below
        for contact in Contact.objects.filter(email=instance.email):
            contact.users.add(instance.user)
            # @@@ send notification
signals.post_save.connect(new_user, sender=EmailAddress)

if EmailAddress:
    def new_user(sender, instance, **kwargs):
        if instance.verified:
            for join_invitation in JoinInvitation.objects.filter(contact__email=instance.email):
                if join_invitation.status not in ["5", "7"]: # if not accepted or already marked as joined independently
                    join_invitation.status = "7"
                    join_invitation.save()
                    # notification will be covered below
            for contact in Contact.objects.filter(email=instance.email):
                contact.users.add(instance.user)
                # @@@ send notification

    # only if django-email-notification is installed
    signals.post_save.connect(new_user, sender=EmailAddress)

def delete_friendship(sender, instance, **kwargs):
    friendship_invitations = FriendshipInvitation.objects.filter(to_user=instance.to_user, from_user=instance.from_user)
    for friendship_invitation in friendship_invitations:
        if friendship_invitation.status != "8":
            friendship_invitation.status = "8"
            friendship_invitation.save()

signals.pre_delete.connect(delete_friendship, sender=Friendship)

# moves existing friendship invitation from user to user to FriendshipInvitationHistory before saving new invitation
def friendship_invitation(sender, instance, **kwargs):
    friendship_invitations = FriendshipInvitation.objects.filter(to_user=instance.to_user, from_user=instance.from_user)
    for friendship_invitation in friendship_invitations:
        FriendshipInvitationHistory.objects.create(
                from_user=friendship_invitation.from_user,
                to_user=friendship_invitation.to_user,
                message=friendship_invitation.message,
                sent=friendship_invitation.sent,
                status=friendship_invitation.status
                )
        friendship_invitation.delete()

signals.pre_save.connect(friendship_invitation, sender=FriendshipInvitation)
