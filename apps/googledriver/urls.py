from django.conf.urls.defaults import *
from django.views.generic.simple import direct_to_template, redirect_to
from googledriver.views import *
urlpatterns = patterns('',
    url(r'^importfile/$', my_files, name='googledriver_my_files'),
    url(r'^authcallback/$', auth_callback, name='googledriver_authcallback'),
    url(r'^fileupload/$', fileupload, name='googledriver_fileupload'),
    url(r'^googlefilelist/$', googlefilelist, name='googlefilelist'),
    url(r'^import/$', multi_file_upload, name='gdrive_import_files'),
    url(r'^process_import/$',  download2Upload, name='download2Upload'),
    url(r'^getFullTree/$', getFullTree, name='getFullTree'),
    
)