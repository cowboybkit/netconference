import pickle
import base64
from django.contrib.auth.models import User
from django.db import models

from oauth2client.django_orm import FlowField
from oauth2client.django_orm import CredentialsField
from south.modelsinspector import add_introspection_rules
add_introspection_rules([], ['oauth2client.django_orm.FlowField'])
add_introspection_rules([], ['oauth2client.django_orm.CredentialsField'])
FOLDER_MIMETYPE = 'application/vnd.google-apps.folder'
DOCX_MIMETYPE = 'application/vnd.google-apps.document'
XLSX_MIMETYPE = 'application/vnd.google-apps.spreadsheet'
PPTX_MIMETYPE = 'application/vnd.google-apps.presentation'

class FlowModel(models.Model):
  user = models.ForeignKey(User, primary_key=True)
  flow = FlowField()


class CredentialsModel(models.Model):
  user = models.ForeignKey(User, primary_key=True)
  credential = CredentialsField()

class DriveFileObject():
    fileExtension = 'default'
    
    def __init__(self, id, name, url, fileExtension, fileSize, createdDate, modifiedDate, isStarred=False):
        self.id = id
        self.name = name
        self.url = url
        self.fileExtension = fileExtension
        self.fileSize = fileSize
        self.isStarred = isStarred
        self.createdDate = createdDate
        self.modifiedDate = modifiedDate
        self.adjust_file_ext()
        
    def adjust_file_ext(self):
        if self.fileExtension != 'default' and self.fileExtension != 'normal_folder':
            if self.is_file_supported() == False:
                self.fileExtension = 'default'
        return self.fileExtension
    
    def is_folder(self):
        if self.fileExtension == 'normal_folder':
            return True
        return False
    
    def is_file_supported(self):
        FILES_SUPPORTED = ['aac','avi','doc', 'docx', 'f4v','flv', 'gif','html', 'jpg', 'm4a','m4v', 'mov', 'mp3','mp4', 'mpg', 'odp','ods', 'odt', 'pdf', 'png', 'ppt', 'pptx','rtf', 'sci', 'swf', 'sxc', 'sxw', 'tif','txt', 'wav', 'wmv', 'wpd', 'xlsx', 'xls', 'zip']
        if self.fileExtension in FILES_SUPPORTED:
           return True
        return False
    