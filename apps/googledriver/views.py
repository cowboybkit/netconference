import logging
from django.http import HttpResponse, Http404
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from oauth2client.django_orm import Storage
from oauth2client.client import OAuth2WebServerFlow
from apiclient.discovery import build
from django.utils.translation import ugettext_lazy
from datetime import datetime
from django.template.defaultfilters import filesizeformat
from urlparse import parse_qs
from uuid import uuid4
from tagging.models import Tag
import urllib2
import os.path

from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render_to_response, get_list_or_404, redirect
from django.contrib.sites.models import Site
from django.conf import settings
from django.views.generic.simple import direct_to_template
from django.views.generic.list_detail import object_detail, object_list
from django.contrib.sites.models import RequestSite
import httplib2
from googledriver.models import CredentialsModel, FlowModel, DriveFileObject
from files.models import UserFile, FileFolder
from googledriver.auth_service import get_authorization_url, CLIENTSECRETS_LOCATION, REDIRECT_URI, SCOPES
from googledriver.file import DriveFile, DriveManager
from django.utils.safestring import SafeString
import pprint,simplejson
from googledriver import MultipartPostHandler
import tempfile
import sys

_ = lambda x: unicode(ugettext_lazy(x))
FOLDER_MIMETYPE = 'application/vnd.google-apps.folder'
DOCX_MIMETYPE = 'application/vnd.google-apps.document'
XLSX_MIMETYPE = 'application/vnd.google-apps.spreadsheet'
PPTX_MIMETYPE = 'application/vnd.google-apps.presentation'
TMP_DIR = os.path.join(settings.PROJECT_ROOT, "gdrive_tmp")

#def sorted_object_list(request, template_name, queryset, extra_context, **kwargs):
#    sort_by = request.GET.get('sort_by')     
#    has_visible_name = False
#    if sort_by:
#        if sort_by in [el.name for el in queryset.model._meta.fields]:
#            queryset = queryset.order_by(sort_by)
#        else:
#            has_visible_name = True
#            if sort_by in request.session:
#                sort_by = request.session[sort_by]
#                try:
#                    queryset = queryset.order_by(sort_by)
#                except:
#                    raise
#    
#    if True:
#        if 'sort_by' in request.GET:
#            getvars = request.GET.copy()
#            if has_visible_name:
#                extra_context['current_sort_field']= request.session.get(getvars['sort_by']) or getvars['sort_by']
#            else:
#                extra_context['current_sort_field']= getvars['sort_by']
#    return object_list(request, template_name=template_name, queryset=queryset, 
#                       extra_context=extra_context, **kwargs)

@login_required
def my_files(request):
    if request.method == 'GET':
        storage = Storage(CredentialsModel, 'user', request.user, 'credential')
        credential = storage.get()
        if credential is None or credential.invalid == True:
            flow = OAuth2WebServerFlow(
                client_id = CLIENTSECRETS_LOCATION,
                client_secret = settings.GOOGLE_DRIVER_APP_SECRET,
                scope=' '.join(SCOPES),
                user_agent='esoftheadnxtgen',
            )
            flow.params['approval_prompt'] = "force"        
            authorize_url = flow.step1_get_authorize_url(REDIRECT_URI)
            f = FlowModel(user =request.user, flow=flow)
            f.save()
            return HttpResponseRedirect(authorize_url)
        else:
            http = httplib2.Http()
            http = credential.authorize(http)
            service = build("drive", "v2", http=http)
            page_title = _(u'My Google Drive')
            
#      if 'filter' in request.GET or 'q' in request.GET:
#          is_filtered = True
            param = {}
            folder_id = ""
            if "folder" in request.GET:
                folder_id = request.GET['folder']
                if folder_id == "0":
                    param['q'] = "'root' in parents"
                elif folder_id == "1":
                    param['q'] = "not 'me' in owners"
                else:
                    param['q'] = "'%s' in parents" %folder_id
            else:
                folder_id = "0"
                param['q'] = "'root' in parents"
                
            try:
                per_page = int(request.GET["per_page"])
            except:
                per_page = settings.ITEMS_LIST_PAGINATE_BY
                
            
            param['maxResults'] = per_page
            param['fields'] = "items(createdDate,downloadUrl,mimeType,fileExtension,exportLinks,fileSize,id,labels/starred,modifiedDate,title,parents/id)"
            
            fetch_list = DriveManager.retrieve_list_by_query(service, param)            
            folder_list = []
            file_list = []
            if 'items' in fetch_list:
                if folder_id == "1":
                    for item in fetch_list['items']:
                        if len(item['parents']) == 0:
                            fileObj = makeDriveFileObject(item)
                        else:
                            continue
                        if fileObj.is_folder():
                            folder_list.append(fileObj)
                        else:
                            file_list.append(fileObj)
                else:
                    for item in fetch_list['items']:
                        fileObj = makeDriveFileObject(item)
                        if fileObj.is_folder():
                            folder_list.append(fileObj)
                        else:
                            file_list.append(fileObj)
            files = folder_list + file_list
            line_insert = -1
            if len(files) < 10:
                line_insert = 10 -len(files)
#            return HttpResponse(simplejson.dumps(fetch_list))
    return direct_to_template(request, 'googledriver/gdrive_file_list.html',
                {
                 'page_title': page_title, 
                 'user_theme': 'user_theme', 
                 'thumb_url': 'settings.THUMBNAIL_URL',
                 'per_page': per_page,
                 'user':request.user,
                 'files': files,
                 'line_insert': line_insert,
                 'current_folder': folder_id,
                 })

def makeDriveFileObject(fileDict):
    
    createdDate = datetime.strptime(fileDict['createdDate'].replace("Z","UTC"),'%Y-%m-%dT%H:%M:%S.%f%Z').strftime("%d/%m/%Y")
    modifiedDate = datetime.strptime(fileDict['modifiedDate'].replace("Z","UTC"),'%Y-%m-%dT%H:%M:%S.%f%Z').strftime("%d/%m/%Y")
    if 'fileExtension' not in fileDict:
        if fileDict['mimeType'] == FOLDER_MIMETYPE:
            fileDict['fileExtension'] = 'normal_folder'
        elif fileDict['mimeType'] == XLSX_MIMETYPE:
            fileDict['fileExtension'] = 'xlsx'
        elif fileDict['mimeType'] == DOCX_MIMETYPE:
            fileDict['fileExtension'] = 'docx'
        elif fileDict['mimeType'] == PPTX_MIMETYPE:
            fileDict['fileExtension'] = 'pptx'
        else:
            fileDict['fileExtension'] = 'default'
    if fileDict['fileExtension'] == 'normal_folder':
        fileDict['downloadUrl'] = 'default'
    if 'downloadUrl' not in fileDict:
        if fileDict['mimeType'] == XLSX_MIMETYPE:
            fileDict['downloadUrl'] = 'https://docs.google.com/feeds/download/spreadsheets/Export?key=%s&exportFormat=xlsx' %fileDict['id']
        elif fileDict['mimeType'] == DOCX_MIMETYPE:
            fileDict['downloadUrl'] = "https://docs.google.com/feeds/download/documents/export/Export?id=%s&exportFormat=docx" %fileDict['id']
        elif fileDict['mimeType'] == PPTX_MIMETYPE:
            fileDict['downloadUrl'] = 'https://docs.google.com/feeds/download/presentations/Export?id=%s&exportFormat=pptx' %fileDict['id']
        else:
            fileDict['downloadUrl'] = fileDict['exportLinks']['application/pdf']
    if 'fileSize' not in fileDict:
        fileDict['fileSize'] = 0
    return DriveFileObject(fileDict['id'], fileDict['title'], urllib2.quote(fileDict['downloadUrl']), fileDict['fileExtension'], fileDict['fileSize'], createdDate, modifiedDate, fileDict['labels']['starred'])

def multi_file_upload(request):
    if request.method == 'POST':
        file_folders = FileFolder.objects.filter(user=request.user, is_root = True)
        if not file_folders:
            folder_root = FileFolder(name='My Files',user = request.user, parent_folder = None, is_root = True )
            folder_root.save()
            file_folders = FileFolder.objects.filter(user=request.user, is_root = True)
        
        root_folder = file_folders[0]
        chk_files_list = request.POST.getlist('chk_files')
        chk_files = []
        for chk_file in chk_files_list:
            chk_file_dict = parse_qs(chk_file)
            chk_files.append(chk_file_dict)
        return direct_to_template(request, 'googledriver/multi_file_upload_simpler.html',
                     {'user':request.user,'bucket':settings.AWS_STORAGE_BUCKET_NAME,
                      'FILE_UPLOADER_URL':settings.FILE_UPLOADER_URL_V2,
                      'root_folder':root_folder,
                      'chk_files_rs':chk_files,})
    else:
        raise Http404

@login_required 
def download2Upload(request):
    if request.method == 'POST':
        storage = Storage(CredentialsModel, 'user', request.user, 'credential')
        credential = storage.get()
        if credential is None or credential.invalid == True:
            flow = OAuth2WebServerFlow(
                client_id = CLIENTSECRETS_LOCATION,
                client_secret = settings.GOOGLE_DRIVER_APP_SECRET,
                scope=' '.join(SCOPES),
                user_agent='esoftheadnxtgen',
            )
            flow.params['approval_prompt'] = "force"        
            authorize_url = flow.step1_get_authorize_url(REDIRECT_URI)
            f = FlowModel(user =request.user, flow=flow)
            f.save()
            return HttpResponseRedirect(authorize_url)
        else:
            http = httplib2.Http()
            http = credential.authorize(http)
            
            
            
            file_url = request.POST['file_url']
            file_name = request.POST['file_name']
            uid = request.POST['uid']
            cid = request.POST['cid']
            username = request.POST['username']
            
            resp, tmpfile = http.request(file_url, 'GET')
            
            file_ext = resp['content-disposition'].split('.')[-1]
            file_ext = file_ext.replace('"','')
            uuid = str(uuid4())
            opener = urllib2.build_opener(MultipartPostHandler.MultipartPostHandler)
            output = open(TMP_DIR + '/' + file_name + '_' + uuid + '.' + file_ext,'w')
            output.write(tmpfile)
            output.close()
            output = open(TMP_DIR + '/' + file_name + '_' + uuid + '.' + file_ext,'rb')
            params = { "uid" : uid,
                      "uuid" : uuid,
                      "title" : file_name,
                      "cid" : cid,
                      "username" : username,
                      "visibility" : "U",
                      "files[]" : output }
            uploadURL = 'http://192.168.1.103:8080/FileUpload/fileupload'
            result = opener.open(uploadURL, params).read()
            output.close()
            os.remove(TMP_DIR + '/' + file_name + '_' + uuid + '.' + file_ext)
            result_uuid = result.split(',',2)[1];
            if result_uuid == uuid:
                process_import(uuid, request)
            return HttpResponse('success')
    else:
        raise Http404
    
def process_import(uuid, request):
    
    description = request.POST['desc']
    
    privacy = request.POST['privacy']
    password = None
    tags = request.POST['tags']
    folderid = int(request.POST['folderid'])
    folder = None
    if folderid!=0:
        folder = get_object_or_404(FileFolder, pk=int(folderid))
    file_object = get_object_or_404(UserFile.active_objects, uuid=uuid)
    
    timestamp = datetime.datetime.today()
    if privacy == 'public':
         visibility = 'U'
    elif privacy == 'private':
        visibility = 'R'
    else:
        visibility = 'O'
        pwd = request.POST['password']
        algo = "sha1"
        salt = get_hexdigest(algo, str(random.random()), str(random.random()))[:5]
        hashed_password = get_hexdigest(algo, salt, pwd)
        password = "%s$%s$%s" %(algo, salt, hashed_password)
    
    file_object.description = description
    file_object.visibility = visibility
    file_object.password = password
    file_object.tags_string = tags
    file_object.timestamp = timestamp
    file_object.folder = folder
    file_object.save()
    if folder:
        folder.auto_update_info()
    Tag.objects.update_tags(file_object, file_object.tags_string)
    update_total_storage(request)

def update_total_storage(request):
    total_file_size = 0
    used_files = UserFile.active_objects.filter(user = request.user)
    for file in used_files:
        total_file_size += file.file_size
    request.session['total_file_size'] = total_file_size

@login_required
def auth_redirect(request):
    storage = Storage(CredentialsModel, 'user', request.user, 'credential')
    credential = storage.get()
    if credential is None or credential.invalid == True:
        flow = OAuth2WebServerFlow(
            client_id = CLIENTSECRETS_LOCATION,
            client_secret = settings.GOOGLE_DRIVER_APP_SECRET,
            scope=' '.join(SCOPES),
            user_agent='esoftheadnxtgen',
            )
        flow.params['approval_prompt'] = "force"        
        authorize_url = flow.step1_get_authorize_url(REDIRECT_URI)
        f = FlowModel(user =request.user, flow=flow)
        f.save()
        return HttpResponseRedirect(authorize_url)
    else:
        http = httplib2.Http()
        http = credential.authorize(http)
        service = build("drive", "v2", http=http)
#        result, next_token = DriveManager.retrieve_files(service, 5, None)
#        files = DriveFile.from_json(result)
        result = DriveManager.retrieve_root_files(service)
#        result = DriveManager.uploadfile_to_amazon()
        return HttpResponse(simplejson.dumps(result))

def auth_callback(request):
    try:
      f = FlowModel.objects.get(user=request.user)
      credential = f.flow.step2_exchange(request.REQUEST)
      storage = Storage(CredentialsModel, 'user', request.user, 'credential')
      storage.put(credential)
      f.delete()
      return HttpResponseRedirect(reverse("googledriver_my_files"))
    except FlowModel.DoesNotExist:
        return HttpResponse("FlowModel.DoesNotExist")

def getFullTree(request):
    storage = Storage(CredentialsModel, 'user', request.user, 'credential')
    credential = storage.get()
    if credential is None or credential.invalid == True:
        return
    
    http = httplib2.Http()
    http = credential.authorize(http)
    service = build("drive", "v2", http=http)
    
    response = HttpResponse(mimetype="application/json")
    json_data_list = create_entire_jstree(service) 
    json_data = simplejson.dumps(json_data_list) 
    response.write(json_data)
    return response

def create_entire_jstree(service):
    tree = []
    
    mydrive = {}
    mydrive['id'] = "0"
    mydrive['title'] = "My Drive"
    mydrive_tree = create_jstree(mydrive, service)
    tree.append(mydrive_tree)
    
    shared_folder = {}
    shared_folder['id'] = "1"
    shared_folder['title'] = "Shared with me"
    shared_folder_tree = create_shared_jstree(shared_folder, service)
    tree.append(shared_folder_tree)
    
    return tree
    
def create_jstree(folder, service):
    folder_tree = {}
    folder_tree['data'] = folder['title']
    folder_tree['children'] = []
    folder_tree['attr'] = {}
    folder_tree['attr']['id'] = folder['id']
    param = {}
    if folder['id'] == "0":
        folder_tree['attr']['rel'] = "root_folder"
        param['q'] = "mimeType='%s' and 'root' in parents" %FOLDER_MIMETYPE
    else:
        folder_tree['attr']['rel'] = "normal_folder"
        param['q'] = "mimeType='%s' and '%s' in parents" %(FOLDER_MIMETYPE, folder['id'])
    param['fields'] = "items(id,title)"
    children = DriveManager.retrieve_file_by_query(service, param)
    for child in children:
        childFinal = create_jstree(child, service)
        folder_tree['children'].append(childFinal)
    return folder_tree

def create_shared_jstree(folder, service):
    folder_tree = {}
    folder_tree['data'] = folder['title']
    folder_tree['children'] = []
    folder_tree['attr'] = {}
    folder_tree['attr']['id'] = folder['id']
    folder_tree['attr']['rel'] = "root_folder"
    param = {}
    param['q'] = "mimeType='%s' and not 'me' in owners" %FOLDER_MIMETYPE
    param['fields'] = "items(id,parents,title)"
    children = DriveManager.retrieve_file_by_query(service, param)
    for child in children:
        if len(child['parents']) == 0:
            childFinal = create_jstree(child, service)
            folder_tree['children'].append(childFinal)
    return folder_tree

def fileupload(request):
    lists = []
    for item in request.POST:
        a = {}
        a['title'] = item
        a['value'] = request.POST[item]
        lists.append(a)
    return HttpResponse(request.FILES['file[]'].name)    
    return direct_to_template(request, "result.html", {'lists':lists})

def googlefilelist(request):
    return direct_to_template(request, "googledriver/gdrive_file_list.html", {})