from django.contrib import admin
from googledriver.models import CredentialsModel, FlowModel
class CredentialsAdmin(admin.ModelAdmin):
    pass


class FlowAdmin(admin.ModelAdmin):
    pass

admin.site.register(CredentialsModel, CredentialsAdmin)
admin.site.register(FlowModel, FlowAdmin)