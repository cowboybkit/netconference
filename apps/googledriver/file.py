import simplejson
from apiclient import errors
import urllib2
#import MultipartPostHandler
from django.conf import settings
FOLDER_MIMETYPE = 'application/vnd.google-apps.folder'

class ParentFile:
    def __init__(self, object):
        if object:
            self.isRoot = object.isRoot
            self.kind = object.kind
            self.id = object.id
            self.selfLink = object.selfLink
            self.parentLink = object.parentLink

class DriveFolder:
    def __init__(self, object):
        if object:
            self.title = None
            self.mimeType = None
            self.id = None
            self.parent_folder = None
            
class DriveFile:
    def __init__(self):
        self.kind = None
        self.id = None
        self.selfLink = None
        self.title = None
        self.mimeType = None
        self.description = None
        self.labels = None
        self.createdDate = None
        self.modifiedDate = None
        self.downloadUrl = None
        self.userPermission = None
        self.fileSize = None
        self.alternateLink = None
        self.embedLink = None
        self.parents = None
        self.exportLinks = None
        self.thumbnailLink = None
        self.webContentLink = None
    @staticmethod
    def from_json(jsons):
        result = list()
        for object in jsons:
            f = DriveFile()
            f.kind = object['kind']
            f.id = object['id']
            if 'selfLink' in object:
                f.selfLink = object['selfLink']
            f.title = object['title']
            f.mimeType = object['mimeType']
            if 'description' in object:
                f.description = object['description']
            if 'downloadUrl' in object:
                f.downloadUrl = object['downloadUrl']
            if 'fileSize' in object:
                f.fileSize = object['fileSize']
            f.alternateLink = object['alternateLink']
            if 'embedLink' in object:
                f.embedLink = object['embedLink']
            if 'exportLinks' in object:    
                f.exportLinks = object['exportLinks']
            if 'thumbnailLink' in object:
                f.thumbnailLink = object['thumbnailLink']
            if 'webContentLink' in object:
                f.webContentLink = object['webContentLink']
            result.append(f)
        return result
        
class DriveManager:
    
    def __init__(self):
        pass
    
    @staticmethod
    def retrieve_files(service, max_results, page_token):
        """Retrieve a list of File resources.
        
        Args:
          service: Drive API service instance.
          max_results: number file request
          page_token: page identity
        Returns:
          List of File resources.
        """
        result = []
        if page_token:
            files = service.files().list(maxResults= max_results, pageToken = page_token).execute()
        else:
            files = service.files().list(maxResults= max_results).execute()
              
        result.extend(files['items'])
        next_token = files.get('nextPageToken')
        return result, next_token
    
    @staticmethod
    def download_file(service, drive_file):
        """Download a file's content.
        
        Args:
          service: Drive API service instance.
          drive_file: Drive File instance.
        
        Returns:
          File's content if successful, None otherwise.
        """
        download_url = drive_file.get('downloadUrl')
        if download_url:
            resp, content = service._http.request(download_url)
            if resp.status == 200:
              return content
            else:
              return None
        else:
            return None
        
    @staticmethod
    def print_file(service, file_id):
        """Print a file's metadata.
        
        Args:
          service: Drive API service instance.
          file_id: ID of the file to print metadata for.
        """
        try:
            file = service.files().get(id=file_id).execute()
            return file
        except errors.HttpError, error:
            print 'An error occurred: %s' % error
    @staticmethod
    def retrieve_files_in_folder(service, folder_id):
        """Print files belonging to a folder.
        
        Args:
          service: Drive API service instance.
          folder_id: ID of the folder to print files from.
        """
        result = []
        page_token = None
        while True:
            try:
                param = {}
                if page_token:
                    param['pageToken'] = page_token
                children = service.children().list(folderId=folder_id, **param).execute()
                
                for child in children.get('items', []):
                  result.append(child)
                page_token = children.get('nextPageToken')
                if not page_token:
                  break
            except errors.HttpError, error:
                break
        return result
    
    @staticmethod
    def retrieve_root_folders(service):
        """Retrieve a list of File resources.
        
        Args:
          service: Drive API service instance.
          max_results: number file request
          page_token: page identity
        Returns:
          List of File resources.
        """
        result = []
        page_token= None
        while True:
            try:
                param = {}
                if page_token:
                    param['pageToken'] = page_token
                files = service.files().list(**param).execute()
                for item in files['items']:
                    if item['mimeType'] == FOLDER_MIMETYPE:
                        if item['parents']:
                            has_root = False
                            for parent in item['parents']:
                                if parent['isRoot'] == True: has_root = True
                            if has_root: result.append(item)
                page_token = files.get('nextPageToken')
                if not page_token:
                    break
            except errors.HttpError, error:
              break
        return result
    
    @staticmethod
    def retrieve_root_files(service):
        """Retrieve a list of File resources.
        
        Args:
          service: Drive API service instance.
          max_results: number file request
          page_token: page identity
        Returns:
          List of File resources.
        """
        result = []
        page_token= None
        while True:
            try:
                param = {}
                if page_token:
                    param['pageToken'] = page_token
                files = service.files().list(**param).execute()
                for item in files['items']:
                    if item['parents']:
                        has_root = False
                        for parent in item['parents']:
                            if parent['isRoot'] == True: has_root = True
                        if has_root: result.append(item)
                page_token = files.get('nextPageToken')
                if not page_token:
                    break
            except errors.HttpError, error:
              break
        return result
    
    @staticmethod
    def retrieve_list_by_query(service, param):
        """Retrieve a list of File resources base on provided params.
        
        Args:
          service: Drive API service instance.
          param: parameters for file searching
        Returns:
          List of File resources.
        """
        result = []
        try:
            if param is None:
                param = {}
            result = service.files().list(**param).execute()            
        except errors.HttpError, error:
            print 'An error occurred: %s' % error
        return result
    
    @staticmethod
    def retrieve_file_by_query(service, param):
        """Retrieve a list of File resources base on provided params.
        
        Args:
          service: Drive API service instance.
          param: parameters for file searching
        Returns:
          List of File resources.
        """
        result = []
        try:
            if param is None:
                param = {}
            files = service.files().list(**param).execute()
            result = files['items']
        except errors.HttpError, error:
            print 'An error occurred: %s' % error
        return result
    
    @staticmethod
    def uploadfile_to_amazon():
        params = {
                  'uid':'327',
                  'title':'test hung drive',
                  'cid':'netconference',
                  'username':'vncdemo',
                  'visibility':'U',
                  'file[]':open('/home/cowboybkit/10.png', 'rb')
                  }
#        opener = urllib2.build_opener(MultipartPostHandler.MultipartPostHandler)
        urllib2.install_opener(opener)
        
        req = urllib2.Request(settings.FILE_UPLOADER_URL_V2, params)
        req.add_header('User-agent', 'Mozilla/5.0 (X11; Linux i686; rv:13.0) Gecko/20100101 Firefox/13.0.1')
        response = urllib2.urlopen(req).read().strip()
        return response 
    