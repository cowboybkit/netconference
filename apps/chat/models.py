import datetime
import urllib2

from django.conf import settings
from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.utils.hashcompat import sha_constructor

XMPP_REST_URL = getattr(settings, 'XMPP_REST_URL', 'http://localhost:5280/rest/')

def generate_token(secret):
    """Create a unique SHA token/hash using secret"""
    hash = sha_constructor(secret).hexdigest()
    return hash[::2]

class ChatUser(models.Model):
	"""User's profile for chat application"""
	user = models.OneToOneField(User)
	xmpp_token = models.CharField(max_length=255, null=True, blank=True)

	def __unicode__(self):
		return self.user.username
	
	def save(self, *args, **kwargs):
		if not self.xmpp_token:
			self.xmpp_token = generate_token(settings.SECRET_KEY + str(self.user.id) + unicode(datetime.datetime.now()))
		super(ChatUser, self).save()

def create_xmpp_account(sender, instance, created, **kwargs):
    """Create user's xmpp account, fired on ChatUser save"""
    register_cmd = 'register %s %s %s' %(instance.user.username, settings.XMPP_DOMAIN, instance.xmpp_token)
    try:
        resp = urllib2.urlopen(XMPP_REST_URL, register_cmd)
    except urllib2.HTTPError:
    	pass

post_save.connect(create_xmpp_account, sender=ChatUser)

class Room(models.Model):
	name = models.CharField(max_length=50)
	description = models.TextField(blank=True, null=True)
	participants = models.ManyToManyField(ChatUser)
	hash = models.CharField(max_length=50, unique=True, editable=False)
	transcript_date = models.DateField(auto_now_add=True)
	
	def __unicode__(self):
	    return self.name
	
	@models.permalink
	def get_absolute_url(self):
		return ('chat_room', (), {'room': self.name})

	def get_invite_url(self):
		return self.get_absolute_url() + "?token=" + self.hash
	
	def get_transcript_template(self):
	    return "speeqe/transcripts/%s/%s.html" %(self.room.name, self.transcript_date.strftime("%Y-%m-%d"))
	
	def save(self, *args, **kwargs):
		if not self.hash:
			self.hash = generate_token(settings.SECRET_KEY + self.name + unicode(datetime.datetime.now()))
		super(Room, self).save()

class SharedFile(models.Model):
    room = models.ForeignKey(Room)
    uploaded_by = models.ForeignKey(ChatUser, null=True, blank=True)
    uploaded_file = models.FileField(upload_to="uploaded_files/chat/", max_length=100)

    def __unicode__(self):
        return self.get_file_name()

    def get_absolute_url(self):
        return "%s%s" %(settings.MEDIA_URL, self.uploaded_file.name)

    def get_file_name(self):
        return self.uploaded_file.name.split('/').pop()

