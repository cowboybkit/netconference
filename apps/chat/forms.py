from django import forms
from django.contrib.auth.models import User
from django.forms.models import inlineformset_factory

from chat.models import ChatUser, Room, SharedFile

class RoomForm(forms.ModelForm):
    class Meta:
        model = Room

class SharedFileForm(forms.ModelForm):
    class Meta:
        model = SharedFile
        fields = ['uploaded_file']

UserMemberForm = inlineformset_factory(User, ChatUser, can_delete=False)

