from django.contrib import admin
from django.forms import ModelForm, CharField

from chat.models import ChatUser, Room
    
class RoomAdminForm(ModelForm):
    invite_url = CharField(max_length=50, help_text="Leave this empty when creating a room", required=False)
    
    class Meta:
        model = Room
        
    def __init__(self, *args, **kwargs):
        super(RoomAdminForm, self).__init__(*args, **kwargs)
        if kwargs.has_key('instance'):
            instance = kwargs['instance']
            self.initial['invite_url'] = instance.get_invite_url()

class RoomAdmin(admin.ModelAdmin):
    form = RoomAdminForm

admin.site.register(ChatUser)
admin.site.register(Room, RoomAdmin)
