from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseForbidden
from django.shortcuts import get_object_or_404, redirect, render_to_response
from django.template import RequestContext
from django.views.decorators.http import require_POST
from django.utils import simplejson

from chat.forms import RoomForm, SharedFileForm
from chat.models import ChatUser, Room

def handle_token(request):
    return render_to_response('chat/token.html', {}, context_instance=RequestContext(request))

def chat(request, room):
    """Show chat room based on room"""

    if not request.user.is_authenticated() and 'nick' not in request.POST:
        return handle_token(request)

    member = None
    room = get_object_or_404(Room, name=room)
    username = settings.XMPP_DOMAIN

    # security checks
    # allow room participants and 
    # people invited to the room through token
    allow = False
    if request.user.is_authenticated():
        member = get_object_or_404(ChatUser, user=request.user)
        username = member.user.username
        if member in room.participants.all():
            allow = True

    token = request.POST.get('token', None)
    if token == room.hash:
        allow = True

    if not allow:
        return HttpResponseForbidden()

    nick = request.POST.get('nick', None)
    xmpp_token = getattr(member, 'xmpp_token', None)
    invite_url = request.build_absolute_uri(room.get_invite_url())
    form = SharedFileForm()

    return render_to_response(
        'chat/room.html',
        {
            'username': username,
            'xmpp_token': xmpp_token,
            'room': room,
            'invite_url': invite_url,
            'nick': nick,
            'form': form,
        },
        context_instance=RequestContext(request))

@login_required
def index(request):
    context = {}
    form = RoomForm()
    # don't show current user in this field
    # we will add him on form save
    form.fields['participants'].queryset = ChatUser.objects.exclude(user=request.user).filter(user__in=request.user.friends.all())

    if request.method == "POST":
        form = RoomForm(request.POST)
        if form.is_valid():
            new_room = form.save()
            # add the current user
            new_room.participants.add(request.user.chatuser)
            new_room.save()
            return redirect(new_room)

    user_rooms = request.user.chatuser.room_set.all()[:10]
    context['featured_rooms'] = user_rooms
    context['form'] = form
    
    return render_to_response('chat/index.html', context, context_instance=RequestContext(request)) 

@require_POST
def upload_file(request, room):
    # request.ajax gives false here
    # because jquery forms posts through
    # iframe

    room = get_object_or_404(Room, name=room)
    form = SharedFileForm(request.POST, request.FILES)
    if form.is_valid():
        shared_file = form.save(commit=False)
        shared_file.room = room
        if request.user.is_authenticated():
            shared_file.uploaded_by = get_object_or_404(ChatUser, user=request.user)
        shared_file.save()
        relative_url = shared_file.get_absolute_url()
        resp = {}
        resp['url'] = request.build_absolute_uri(relative_url)
        resp['filename'] =  shared_file.get_file_name()
        return HttpResponse(simplejson.dumps(resp))
    return HttpResponse(str(form.errors))

