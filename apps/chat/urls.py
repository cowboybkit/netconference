from django.conf.urls.defaults import *
from django.conf import settings
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^room/(?P<room>\w+)/', 'chat.views.chat', name='chat_room'),
    #url(r'^transcript/(?P<room>\w+)/','chat.views.transcript', name='chat_room_transcript'),
    url(r'^upload/(?P<room>\w+)/$', 'chat.views.upload_file', name='shared_file_upload'),
    (r'^admin/', include(admin.site.urls)),
    (r'^$','chat.views.index'),
)

if settings.SERVE_STATIC_URLS:
    urlpatterns += patterns('',
                            (r'^(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.DOCUMENT_ROOT }),
                            )
