from django.contrib import admin
from ccall_pins.models import ConferencePin, ConferenceBridgeNumber

class ConferencePinAdmin(admin.ModelAdmin):
    list_display = ('id', 'dial_in_number', 'access_code', 'password',)


admin.site.register(ConferencePin, ConferencePinAdmin)

class ConferenceBridgeNumberAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'phonenumber', 'modcode','attendeecode')


admin.site.register(ConferenceBridgeNumber, ConferenceBridgeNumberAdmin)