from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
import urllib2
from django.conf import settings

class ConferencePin(models.Model):
    """A call-in number, PIN, playback number, and password. Each is assigned to a user when they sign up."""
    
    dial_in_number = models.CharField(max_length=14)
    playback_number = models.CharField(max_length=14)
    access_code = models.IntegerField(unique=True)
    password = models.IntegerField()
    
    def __unicode__(self):
        return str(self.access_code)

class ConferenceBridgeManager(models.Manager):
    def get_or_create(self,user):
        confBrigde = self.filter(user = user)
        if confBrigde:
            current_phone = confBrigde[0]
            if current_phone.phonenumber == settings.CONFERENCE_PHONENUMBER:
                return current_phone
            else:
                req = urllib2.urlopen('https://signup.secureconf.com/conf/remote/codemanager.php?cmd=create&usr=netconference&pass=T4pp3ta&phone='+settings.CONFERENCE_PHONENUMBER)
                stream = req.read(1000)
                if stream.startswith('RESPONSE\nOK'):
                    response = stream.split(" ")
                    arr = [x for x in response if x ]
                    current_phone.phonenumber=arr[1]
                    current_phone.modcode=arr[2]
                    current_phone.attendeecode = arr[3]
                    current_phone.save(False, False)
                    return current_phone
                else:
                    return current_phone
        req = urllib2.urlopen('https://signup.secureconf.com/conf/remote/codemanager.php?cmd=create&usr=netconference&pass=T4pp3ta&phone='+settings.CONFERENCE_PHONENUMBER)
        stream = req.read(1000)
        if stream.startswith('RESPONSE\nOK'):
            response = stream.split(" ")
            arr = [x for x in response if x ]
            confBrigde = self.create(user = user, phonenumber=arr[1],modcode=arr[2], attendeecode = arr[3])
            confBrigde.save(False, False)
            return confBrigde
        else:
            return None 

class ConferenceBridgeNumber(models.Model):
    user = models.ForeignKey(User, verbose_name=_("User"), null=True, blank=True)
    phonenumber = models.CharField(_("Phone Number"), max_length=120)
    modcode = models.CharField(_("modcode"), max_length=120)
    attendeecode = models.CharField(_("attendee code"), max_length=120)
    
    objects = ConferenceBridgeManager()
    
def _get_conference_call(user):
    call_pin = {}
    
    if settings.CONFERENCE_CALL_TYPE == 0:
        conf_pin = ConferencePin.objects.get(pk = user.id)
        call_pin['phonenumber'] = conf_pin.dial_in_number
        call_pin['modcode'] = conf_pin.access_code
        call_pin['attendeecode'] = conf_pin.password
    elif settings.CONFERENCE_CALL_TYPE == 1:
        conf_pin = ConferenceBridgeNumber.objects.get_or_create(user)
        call_pin['phonenumber'] = conf_pin.phonenumber
        call_pin['modcode'] = conf_pin.modcode
        call_pin['attendeecode'] = conf_pin.attendeecode
        
    return call_pin
User.add_to_class('get_conference_call', _get_conference_call)

def _get_conference_phonenumber(user):
    return user.get_conference_call()['phonenumber']
User.add_to_class('get_conference_phonenumber', _get_conference_phonenumber)

def _get_conference_modcode(user):
    return user.get_conference_call()['modcode']
User.add_to_class('get_conference_modcode', _get_conference_modcode)

def _get_conference_attendeecode(user):
    return user.get_conference_call()['attendeecode']
User.add_to_class('get_conference_attendeecode', _get_conference_attendeecode)
