from django.db import models
import datetime

from profiles.models import Profile
# Create your models here.

class TypeOfCall(models.Model):
    name = models.CharField(max_length = 100)

    def __unicode__(self):
        return self.name
    
class Notes(models.Model):
    user = models.ForeignKey(Profile)
    creation_date = models.DateTimeField(default=datetime.datetime.now())
    type_of_call = models.ForeignKey(TypeOfCall)
    notes = models.TextField()
    representative_name = models.CharField(max_length=100, null=True)

    class Meta:
        verbose_name_plural = "Notes"

    def __unicode__(self):
        return u"%s from %s." % (self.type_of_call, self.user)
    
