from django.contrib import admin

from customer_service.models import TypeOfCall, Notes

class NotesAdmin(admin.ModelAdmin):
    list_display = ('__unicode__','creation_date',)
    search_fields = ['notes']

admin.site.register(Notes, NotesAdmin)
admin.site.register(TypeOfCall)
