from django.contrib.auth.models import User
from subscription.models import UserSubscription
from subscription.models import Subscription
from schedule.models import Event
from traffic_geyser.models import Client, ClientUser
import datetime
from django.views.generic.simple import direct_to_template
from django.views.generic.list_detail import object_detail, object_list
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.core.urlresolvers import reverse
from django import forms
from django.conf import settings
from django.template import RequestContext
from django.core.cache import cache
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404, render_to_response
from django.shortcuts import get_object_or_404, render_to_response, get_list_or_404, redirect
from datetime import timedelta
from uuid import uuid4
from tagging.models import Tag
import logging
from files.search import *
from files.forms import FileUploadForm, FileForm
from files.models import UserFile
from user_theme.models import UserTheme
import mimetypes
import urllib
import os
from netconf_utils.encode_decode import *
from netconf_utils.encode_decode import get_object_from_hash
from restrictions.decorators import check_file_upload_limit,check_file_download_limit,check_file_view_limit
from conferencing.models import Conference, RecordedConference
from django.contrib.sites.models import Site
from django.shortcuts import get_object_or_404
from django.views.generic.simple import direct_to_template
from django.views.generic.list_detail import object_detail, object_list
from django.http import HttpResponseRedirect, Http404, HttpResponse
from django import forms
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from urllib2 import urlopen, HTTPError, URLError
import logging
import datetime
from conferencing.forms import ConferenceForm
from conferencing.models import Conference, SharedFile
from schedule.models import *
from schedule.periods import *
from files.models import UserFile
from files.search import *
from ccall_pins.models import ConferencePin
from uuid import uuid4
from apps.subscription.models import Subscription
from checkout.models import OrderItem, Order
from cart.models import CartItem
from schedule.forms import EventForm
from django.contrib import auth
from traffic_geyser.utils import *
from django.core.exceptions import ObjectDoesNotExist
from traffic_geyser.tg_restrictions import authenticate_client_require

# Create your views here.
def addNewAccount(request):
    if request.method == "POST":
        api_key = request.POST.get('api_key');
        user_id = request.POST.get('user_id');
        subscription = request.POST.get('subscription');
        #check if parameters are not empty
        if not(api_key) or not(user_id) or not(subscription):
            return failResponse("invalid parameters");
        #check if API key is valid
        try:
            client_obj = Client.objects.get(api_key=api_key);
        except Client.DoesNotExist:
            return failResponse("invalid api_key");
        # check if user_id is existed in database
        # if 'yes': return fail
        # if 'no' : add this new user to database
        try:
            existed_user = User.objects.get(username=user_id);
            if existed_user is not None:
                return failResponse("existed user_id");
        except User.DoesNotExist:
            password = randomString(10);
            email = randomString(20);
            created_user = User.objects.create_user(user_id, email, password);               
            #add subscription for user
            subs = Subscription.objects.get(name = subscription);
            user_sub = UserSubscription.objects.create(user=created_user, subscription=subs);
            #add this user to related client
            ClientUser.objects.create(user=created_user, client=client_obj);
            return successResponse();
    else:
        return failResponse("invalid POST method");

@authenticate_client_require
def changeSubscription(request):
    subscription_name = request.POST.get('subscription');
    #check if parameters are not empty
    if not(subscription_name):
        return failResponse("invalid parameters");
    user_obj = request.session['user_of_client'];
    #check if subscription is valid
    try:
        new_subs = Subscription.objects.get(name = subscription_name);
    except Subscription.DoesNotExist:
        return failResponse("invalid subscription");
    #update subscription of user
    user_subs = User.get_usersubscription(user_obj);
    user_subs.subscription = new_subs;
    user_subs.save();
    return successResponse();

@authenticate_client_require
def cancelAccount(request):
    user_obj = request.session['user_of_client'];
    #update user status to be inactive
    user_obj.is_active = 0;
    user_obj.save();
    return successResponse();

def checkOverPayment(client_obj, user_obj, meeting_obj):
    return False


def joinMeeting(request):
    return failResponse("invalid POST method");

@authenticate_client_require
def launchMeeting(request):
    urlhash = request.POST.get('urlhash');
    #check if parameters are not empty
    if not(urlhash):
        return failResponse("invalid parameters");
    #check if event_id is existed.
    event_id = int(uri_b64decode(str(urlhash))) / 42
    try:
        conference = Event.objects.get(pk=event_id);
    except Event.DoesNotExist:
        return failResponse("invalid event_id");      
    #check if this user is host of this event.
    user_obj = request.session['user_of_client'];
    if user_obj.username != conference.creator.username:
        return failResponse("invalid event_id");
    
    host = user_obj;
    extra_context = get_conference_flashvars2(request)
    extra_context["conf_urlhash"] = urlhash
    conference = get_object_or_404(Event, pk=event_id)
    gid = str(uuid4())
    shared_files = SharedFile.objects.filter(conference=conference)
    ccall_pin = ConferencePin.objects.get(id=int(host.id))
    files = UserFile.active_objects.filter(user=host).order_by('title')
    extra_context.update({'conference': conference, 
                          'host': conference.creator, 
                          'visitor': user_obj.username,
                          'screen_name': host.username, 
                          'anonymous': False, 'files': files, 
                          'shared_files': shared_files, 
                          'ccall_pin': ccall_pin, 
                          'gid': gid,
                          'urlhash': str(urlhash)})
    response = direct_to_template(request, 'traffic_geyser_templates/conferencing/conference_popout2.html', extra_context)

    setattr(response, "djangologging.suppress_output", True)
    return response


@authenticate_client_require
def cleanAllEvents(request):
    user_obj = request.session['user_of_client'];
    event = Event.objects.filter(creator = user_obj);
    event.delete();
    return successResponse();


@authenticate_client_require
def createEvent(request):
    event_title = request.POST.get('event_title')
    event_description = request.POST.get('event_description')
    event_datetime = request.POST.get('event_datetime')
    event_duration = request.POST.get('event_duration')
    #check if parameters are not empty
    if not(event_title) or not(event_description) or not(event_datetime) or not(event_duration):
        return failResponse("invalid parameters");
    calendar = Calendar.objects.get(pk=1);
    starttime = datetime.datetime.strptime(event_datetime, "%Y-%m-%d %H:%M:%S");
    endtime = starttime + datetime.timedelta(minutes=int(event_duration));
    end_recur = endtime + datetime.timedelta(days=8)
    user_obj = request.session['user_of_client'];
    event_obj = Event.objects.create(title=event_title, description=event_description, start=starttime, end=endtime, creator=user_obj, end_recurring_period = end_recur);
    urlhash = uri_b64encode(str(event_obj.id * 42));
    return successResponse("event_id", str(urlhash));


@authenticate_client_require
def upload_file(request, file_form=FileUploadForm):
    project_select = request.GET.get('project_id',False)
    if project_select:
        try:
            project = Project.objects.get(pk=project_select)
            if request.user in project.member_users.all():
                request.session['upload_project'] = project
            else:
                project_select = False
        except Project.DoesNotExist:
            project_select = False
    
    if project_select:
        file_form = FileUploadForm(initial = {'project': project.pk})
    else:
        request.session.pop('upload_project','')
        file_form = FileUploadForm()
    user = request.user.id
    site = Site.objects.get_current()
    redirect_url = "http://%s%s" % (site.domain, reverse("my_files"))  
    cid = getattr(settings, "CID", "netconference")
    return direct_to_template(request, 'traffic_geyser_templates/files/files_upload.html', 
                              extra_context={'file_form': file_form, 
                                             'users': user,
                                             'redirect_url':redirect_url,
                                             "cid": cid})


@login_required
@check_file_upload_limit
def upload_file_bak(request, file_form=FileUploadForm):
    project_select = request.GET.get('project_id',False)
    if project_select:
        try:
            project = Project.objects.get(pk=project_select)
            if request.user in project.member_users.all():
                request.session['upload_project'] = project
            else:
                project_select = False
        except Project.DoesNotExist:
            project_select = False
    
    if project_select:
        file_form = FileUploadForm(initial = {'project': project.pk})
    else:
        request.session.pop('upload_project','')
        file_form = FileUploadForm()
    user = request.user.id
    site = Site.objects.get_current()
    redirect_url = "http://%s%s" % (site.domain, reverse("my_files"))  
    cid = getattr(settings, "CID", "netconference")
    return direct_to_template(request, 'traffic_geyser_templates/files/files_upload.html', 
                              extra_context={'file_form': file_form, 
                                             'user': user,
                                             'redirect_url':redirect_url,
                                             "cid": cid})


def conference_home(request):
    return direct_to_template(request, 'traffic_geyser_templates/conferencing/base.html')


def my_conferences(request):
    user = request.user.id
    qs = Event.objects.filter(creator=user).order_by('-start')
    from restrictions.used_restrictions import ConferenceMinutesRestriction
    cmr = ConferenceMinutesRestriction(request.user)
    allow_join = cmr.allow()
    return object_list(request, template_name='traffic_geyser_templates/conferencing/conference_list.html', queryset=qs, paginate_by = settings.ITEMS_LIST_PAGINATE_BY,
                       extra_context={'nav_current':'conf_nav',
                                      'allow_join':allow_join})
my_conferences = login_required(my_conferences)


def my_conferences_inc(request):
    user = request.user.id
    qs = Event.objects.filter(moderator=user).order_by('start')
    return object_list(request, template_name='traffic_geyser_templates/conferencing/conference_list_inc.html', queryset=qs)
my_conferences_inc = login_required(my_conferences_inc)


def conferences_public(request):
    now = datetime.datetime.now()
    qs = Event.public_objects.filter(end__gte=now).order_by('start')
    return object_list(request, template_name='traffic_geyser_templates/conferencing/conference_public.html', queryset=qs, paginate_by=settings.ITEMS_LIST_PAGINATE_BY)


def add_conference(request, conference_form=ConferenceForm, tags=None):
    conference_form = ConferenceForm()
    user = request.user.id
    if request.method == 'POST':
        conference_form_request = ConferenceForm(request.user, request.POST)
        if conference_form_request.is_valid():
            user = request.user
            conference_title = conference_form_request.cleaned_data['title']
            conference_date = conference_form_request.cleaned_data['date']
            conference_private = conference_form_request.cleaned_data['private']
            conference_tags = conference_form_request.cleaned_data['tags_string']
            conference_form_add = Conference(moderator=user, title=conference_title, date=conference_date, private=conference_private, tags_string=conference_tags)
            conference_form_add.save()
            request.user.message_set.create(message="Your conference was created!")
            return HttpResponseRedirect('/traffic_geyser_templates/conferencing/list/',)
        else:
            #raise forms.ValidationError("Form not valid!")
            # logging.debug(request.POST)
            # logging.error(conference_form_request.errors)
            request.user.message_set.create(message="There was an error. Please make sure you entered a title and used the correct date format.")    
    return direct_to_template(request, 'traffic_geyser_templates/conferencing/conference_create.html', extra_context={'conference_form': conference_form})
add_conference = login_required(add_conference)


def edit_conference(request, slug, conference_form=ConferenceForm, tags=None):
    conference = Conference.objects.get(uuid=slug)
    conference_id = conference.id
    conference_form = ConferenceForm(instance=conference)
    user = request.user.id
    if request.method == 'POST' and 'title' in request.POST:
        conference_form_request = ConferenceForm(request.user, request.POST)
        conference_tags = conference_form_request['tags_string']
        if conference_form_request.is_valid():
            user = request.user
            conference_title = conference_form_request.cleaned_data['title']
            conference_date = conference_form_request.cleaned_data['date']
            conference_private = conference_form_request.cleaned_data['private']
            conference_tags = conference_form_request.cleaned_data['tags_string']
            conference_form_add = Conference(id=conference_id, moderator=user, title=conference_title, date=conference_date, private=conference_private, tags_string=conference_tags, uuid=slug)
            conference_form_add.save()
            request.user.message_set.create(message="Your conference was edited!")
            return HttpResponseRedirect('/traffic_geyser_templates/conferencing/list/',)
        else:
            logging.error(conference_form_request.errors)
            request.user.message_set.create(message="There was an error. Please make sure you entered a title and used the correct date format.")
    elif request.method == 'POST' and 'delete' in request.POST:
        conference_delete = Conference.objects.get(id=conference_id)
        if request.user == conference_delete.moderator:
            conference_delete.delete()
            request.user.message_set.create(message="Your conference was deleted!")
            return HttpResponseRedirect('/traffic_geyser_templates/conferencing/list/',)
        else:
            request.user.message_set.create(message="You cannot delete other people's conferences.")
            return HttpResponseRedirect('/traffic_geyser_templates/conferencing/list/',)
    
    return direct_to_template(request, 'traffic_geyser_templates/conferencing/conference_edit.html', extra_context={'conference_form': conference_form})
edit_conference = login_required(edit_conference)


from restrictions.utils import get_conference_flashvars, get_conference_flashvars2
from restrictions.decorators import check_conference_allow


@check_conference_allow
def conference_session2(request, urlhash):
    event_id = int(uri_b64decode(str(urlhash))) / 42
    extra_context = get_conference_flashvars2(request)
    free_sub = Subscription.objects.get_free()
    if request.user.is_authenticated() and request.user.get_subscription() == free_sub:
        extra_context["is_free"] = True
        extra_context["non_free_subs"] = Subscription.objects.get_non_free()
    extra_context["conf_urlhash"] = urlhash
    conference = get_object_or_404(Event, pk=event_id)
    username = conference.creator.username
    user = request.user
    request.user = user_obj
    gid = str(uuid4())
    host = get_object_or_404(User, username=username)
    shared_files = SharedFile.objects.filter(conference=conference)
    ccall_pin = ConferencePin.objects.get(id=int(host.id))
    if request.method == "POST" and "anonymous" in request.POST:
        screen_name = request.POST['screen_name']
        if screen_name != "":
            extra_context.update({'conference': conference, 'host': host, 'user': user, 'screen_name': screen_name, 'anonymous': True, 'files': 'none', 'shared_files': shared_files, 'ccall_pin': ccall_pin, 'gid': gid, 'urlhash': str(urlhash)})
            response = direct_to_template(request, 'traffic_geyser_templates/conferencing/conference_popout2.html', extra_context)
        
    elif not request.user.is_authenticated():
        anonymous_token = random.randrange(1000, 9999)
        extra_context.update({'conference': conference, 'anonymous_token': anonymous_token})
        response = direct_to_template(request, 'traffic_geyser_templates/conferencing/conference_login.html', extra_context)
        setattr(response, "djangologging.suppress_output", True)
        return response
        
    elif user.username != "":
        files = UserFile.active_objects.filter(user=user).order_by('title')
        extra_context.update({'conference': conference, 'host': host, 
                              'user': user, 'screen_name': user.username, 
                              'anonymous': False, 'files': files, 
                              'shared_files': shared_files, 
                              'ccall_pin': ccall_pin, 
                              'gid': gid, 
                              'urlhash': str(urlhash)})
        response = direct_to_template(request, 'traffic_geyser_templates/conferencing/conference_popout2.html', extra_context)
    else:
        anonymous_token = random.randrange(1000, 9999)
        extra_context.update({'conference': conference,
                      'host': host,
                      'user': anonymous_token,
                      'screen_name': anonymous_token,
                      'anonymous': True,
                      'files': 'none',
                      'shared_files': shared_files,
                      'ccall_pin': ccall_pin,
                      'gid': gid,
                      'urlhash': str(urlhash)})
        response = direct_to_template(request, 'traffic_geyser_templates/conferencing/conference_popout2.html', extra_context)
        
    setattr(response, "djangologging.suppress_output", True)
    return response


@check_conference_allow
def conference_session(request, urlhash):
    event_id = int(uri_b64decode(str(urlhash))) / 42
    try:
        conference = Event.objects.get(pk=event_id)
    except:
        raise Http404
    extra_context = get_conference_flashvars(request.user)
    username = conference.creator.username
    user = request.user
    gid = str(uuid4())
    host = get_object_or_404(User, username=username)
    shared_files = SharedFile.objects.filter(conference=conference)
    ccall_pin = ConferencePin.objects.get(id=int(host.id))
    if request.method == "POST" and "anonymous" in request.POST:
        screen_name = request.POST['screen_name']
        if screen_name != "":
            extra_context.update({'conference': conference, 'host': host, 'user': user, 'screen_name': screen_name, 'anonymous': True, 'files': 'none', 'shared_files': shared_files, 'ccall_pin': ccall_pin, 'gid': gid, 'urlhash': str(urlhash)})
            response = direct_to_template(request, 'traffic_geyser_templates/conferencing/conference_popout.html', extra_context)
        
    elif not request.user.is_authenticated():
        anonymous_token = random.randrange(1000, 9999)
        extra_context.update({'conference': conference, 'anonymous_token': anonymous_token})
        response = direct_to_template(request, 'traffic_geyser_templates/conferencing/conference_login.html', extra_context)
        setattr(response, "djangologging.suppress_output", True)
        return response
        
    elif user.username != "":
        files = UserFile.active_objects.filter(user=user).order_by('title')
        extra_context.update({'conference': conference, 'host': host, 'user': user, 'screen_name': user.username, 'anonymous': False, 'files': files, 'shared_files': shared_files, 'ccall_pin': ccall_pin, 'gid': gid, 'urlhash': str(urlhash)})
        response = direct_to_template(request, 'traffic_geyser_templates/conferencing/conference_popout.html', extra_context)
    else:
        anonymous_token = random.randrange(1000, 9999)
        extra_context.update({'conference': conference, 'host': host, 'user': anonymous_token, 'screen_name': anonymous_token, 'anonymous': True, 'files': 'none', 'shared_files': shared_files, 'ccall_pin': ccall_pin, 'gid': gid, 'urlhash': str(urlhash)})
        response = direct_to_template(request, 'traffic_geyser_templates/conferencing/conference_popout.html', extra_context)
        
    setattr(response, "djangologging.suppress_output", True)
    return response

def conference_session_quick(request, username):
    """Same as above, but using the username as urlhash / session title"""
    extra_context = get_conference_flashvars(request.user)
    conference = "quickconference"
    user = request.user
    gid = str(uuid4())
    host = get_object_or_404(User, username=username)
    shared_files = SharedFile.objects.filter(conference=username)
    ccall_pin = ConferencePin.objects.get(id=int(host.id))
    if request.method == "POST" and "anonymous" in request.POST:
        screen_name = request.POST['screen_name']
        if screen_name != "":
            extra_context.update({'conference': conference, 'host': host, 'user': user, 'screen_name': screen_name, 'anonymous': True, 'files': 'none', 'shared_files': shared_files, 'ccall_pin': ccall_pin, 'gid': gid, 'urlhash': username})
            response = direct_to_template(request, 'traffic_geyser_templates/conferencing/conference_popout.html', extra_context)
        
    elif not request.user.is_authenticated():
        anonymous_token = random.randrange(1000, 9999)
        extra_context.update({'conference': conference, 'anonymous_token': anonymous_token})
        response = direct_to_template(request, 'traffic_geyser_templates/conferencing/conference_login.html', extra_context)
        setattr(response, "djangologging.suppress_output", True)
        return response
        
    elif user.username != "":
        files = UserFile.active_objects.filter(user=user).order_by('title')
        extra_context.update({'conference': conference, 'host': host, 'user': user, 'screen_name': user.username, 'anonymous': False, 'files': files, 'shared_files': shared_files, 'ccall_pin': ccall_pin, 'gid': gid, 'urlhash': username})
        response = direct_to_template(request, 'traffic_geyser_templates/conferencing/conference_popout.html', extra_context)
    else:
        anonymous_token = random.randrange(1000, 9999)
        extra_context.update({'conference': conference, 'host': host, 'user': anonymous_token, 'screen_name': anonymous_token, 'anonymous': True, 'files': 'none', 'shared_files': shared_files, 'ccall_pin': ccall_pin, 'gid': gid, 'urlhash': username})
        response = direct_to_template(request, 'traffic_geyser_templates/conferencing/conference_popout.html', extra_context)
        
    setattr(response, "djangologging.suppress_output", True)
    return response

def myconference_search(request):
    query_string = ''
    found_entries = None
    if ('q' in request.GET) and request.GET['q'].strip():
        query_string = request.GET['q']
        entry_query = get_query(query_string, ['title', 'description'])
        user = request.user
        now = datetime.datetime.now()
        found_entries = Event.objects.filter(creator=user).filter(entry_query).filter(end__gte=now).order_by('start')
    return object_list(request, template_name='traffic_geyser_templates/conferencing/conference_search_list.html', queryset=found_entries, extra_context={'query_string': query_string, 'page_title': 'My Conferences Search'}, paginate_by=settings.ITEMS_LIST_PAGINATE_BY)
myconference_search = login_required(myconference_search)

def publicconference_search(request):
    query_string = ''
    found_entries = None
    if ('q' in request.GET) and request.GET['q'].strip():
        query_string = request.GET['q']
        entry_query = get_query(query_string, ['title', 'description'])
        now = datetime.datetime.now()
        found_entries = Event.objects.filter(private=False).filter(entry_query).filter(end__gte=now).order_by('start')
    return object_list(request, template_name='traffic_geyser_templates/conferencing/conference_search_list.html', queryset=found_entries, extra_context={'query_string': query_string, 'page_title': 'Public Conferences Search'}, paginate_by=settings.ITEMS_LIST_PAGINATE_BY)
    
def get_shared_files(request, urlhash):
    user = request.user
    event_id = int(uri_b64decode(str(urlhash))) / 42
    conference = Event.objects.get(pk=event_id)
    shared_files = SharedFile.objects.filter(conference=conference)
    if request.is_ajax:
        response = direct_to_template(request, 'traffic_geyser_templates/conferencing/get_shared_files.html', extra_context={'shared_files': shared_files, 'conference': conference})
        setattr(response, "djangologging.suppress_output", True)
        return response
    else:
        raise Http404


@login_required
def create_desktop_recording(request):
    if request.is_ajax:
        if  request.method == 'POST':
            urlhash = request.POST['urlhash']
            guid = request.POST['guid']
            title = request.POST['title']
            title = "%s (Desktop Recording)" % title
            UserFile.objects.get_or_create(title=title, 
                                           uuid=guid, 
                                           user=request.user,
                                           processed = 1,
                                           private = False,
                                           is_active = True,
                                           file_size = 1,
                                           source_type="rec_conf", 
                                           current_type="rec_conf",
                                           duration=100
                                           )
            return HttpResponse("OK")

def make_shared_file(request):
    user = request.user
    if request.is_ajax:
        if request.method == 'POST' and 'file_id' in request.POST:
            file_id = request.POST['file_id']
            urlhash = request.POST['conference']
            event_id = int(uri_b64decode(str(urlhash))) / 42
            conference = Event.objects.get(pk=event_id)
            obj = UserFile.active_objects.get(id=file_id)
            if obj.user == user:
                    SharedFile.objects.get_or_create(owner=user, conference=conference, file=obj)
                    answer = "ok"
            else:
                    answer = "bad"
        response = direct_to_template(request, 'traffic_geyser_templates/conferencing/make_shared_file.html', extra_context={'answer': answer})
        setattr(response, "djangologging.suppress_output", True)
        return response
    else:
        raise Http404
make_shared_file = login_required(make_shared_file)

def remove_shared_file(request):
    user = request.user
    if request.is_ajax:
        if request.method == 'POST' and 'file_id' in request.POST:
            file_id = request.POST['file_id']
            urlhash = request.POST['conference']
            event_id = int(uri_b64decode(str(urlhash))) / 42
            conference = Event.objects.get(pk=event_id)
            obj = UserFile.active_objects.get(id=file_id)
            if obj.user == user:
                SharedFile.objects.get(conference=conference, file=obj).delete()
                answer = "ok"
            elif conference.creator == user:
                SharedFile.objects.get(conference=conference, file=obj).delete()
                answer = "ok"
            else:
                answer = "bad"
        response = direct_to_template(request, 'traffic_geyser_templates/conferencing/make_shared_file.html', extra_context={'answer': answer})
        setattr(response, "djangologging.suppress_output", True)
        return response
    else:
        raise Http404
make_shared_file = login_required(make_shared_file)

def netfetch(request):
    if request.is_ajax:
        if request.method == 'POST' and 'nf_phone' in request.POST:
            conference_id = request.POST['conference_id']
            nf_phone = request.POST['nf_phone']
            conf = Event.objects.get(pk=conference_id)
            if conf.creator == request.user:
                nf_url = 'http://voip-ca.prvlb.net/netfetch.php?user_id=%s&conference_id=%s&phone=%s' % (conf.creator.id, conference_id, nf_phone)
                try:
                    fetchy = urlopen(nf_url).read()
                except HTTPError, err:
                    answer = "fail"
                except URLError, err:
                    answer = "fail"
                else:
                    answer = fetchy
            else:
                answer = "fail"
            response = direct_to_template(request, 'traffic_geyser_templates/conferencing/make_shared_file.html', extra_context={'answer': answer})
            setattr(response, "djangologging.suppress_output", True)
            return response
        else:
            raise Http404
    else:
        raise Http404
netfetch = login_required(netfetch)

from conferencing.models import RecordedConference

def recorded_conf(request):
    qs = request.user.recordedconference_set.all()
    return object_list(request,
                       template_name="traffic_geyser_templates/conferencing/conference_rlist.html",
                       queryset=qs,
                       paginate_by=settings.ITEMS_LIST_PAGINATE_BY,
                       extra_context={'subnav_current':'recorded_conf'}
                       )
    
@login_required    
def direct_to_checkout(request, subs_id):
    "Send directly to the checkout page."
    subs = get_object_or_404(Subscription, pk = subs_id)
    ip_address = request.META["REMOTE_ADDR"]
    order = Order.objects.create(ip_address = ip_address, user = request.user)
    cart_item = CartItem.objects.create(cart_id = order.pk, product = subs)
    orderitem = OrderItem.objects.create(quantity = 1, product = subs, order = order, price=subs.price)
    request.session["cart_id"] = cart_item.cart_id
    request.session["cart_item"] = cart_item
    request.session["order_item"] = orderitem
    request.session["order_number"] = order.pk
    
    return HttpResponseRedirect(reverse("checkout"))

def testcallback(request):
	 return direct_to_template(request, 'traffic_geyser_templates/files/testcallback.html', )


def checkUpcomingEvent(request):
    if request.method == "POST":
        api_key = request.POST.get('api_key');
        user_id = request.POST.get('user_id');
        amount_time = request.POST.get('amount_time');
        #check if parameters are not empty
        if not(api_key) or not(user_id) or not(amount_time):
            return failResponse("invalid parameters");
        #check if API key is valid
        try:
            client_obj = Client.objects.get(api_key=api_key);
        except Client.DoesNotExist:
            return failResponse("invalid api_key");
        #check if user_id is valid
        try:
            user_obj = User.objects.get(username=user_id);
            client_user = ClientUser.objects.get(user=user_obj, client=client_obj);
        except ObjectDoesNotExist:
            return failResponse("invalid user_id");
        #get list of events
        current_time = datetime.datetime.now();
        limit_time = current_time;
        limit_time += datetime.timedelta(minutes=int(amount_time));
        event_list = Event.objects.filter(creator=user_obj, start__gt=current_time, start__lt=limit_time);
        return eventListResponse(event_list);
    else:
        return failResponse("invalid POST method");
