from django.conf.urls.defaults import *
from django.views.generic.simple import direct_to_template
from django.views.generic.list_detail import object_list, object_detail

urlpatterns = patterns('',
    url(r'^addNewAccount/$', 'traffic_geyser.views.addNewAccount', name='add account'),
    url(r'^changeSubscription/$', 'traffic_geyser.views.changeSubscription', name='change subscription'),
    url(r'^cancelAccount/$', 'traffic_geyser.views.cancelAccount', name='cancel account'),
    url(r'^checkUpcomingEvent/$', 'traffic_geyser.views.checkUpcomingEvent', name='check Upcoming event'),
    url(r'^launchMeeting/$', 'traffic_geyser.views.launchMeeting', name='launch a meeting'),
    url(r'^joinMeeting/$', 'traffic_geyser.views.joinMeeting', name='join a meeting'),
    url(r'^createEvent/$', 'traffic_geyser.views.createEvent', name='create event'),
    url(r'^cleanAllEvents/$', 'traffic_geyser.views.cleanAllEvents', name='clean all events'),
    
    url(r'^upload_file/$', 'traffic_geyser.views.upload_file', name='upload files'),
    url(r'^testcallback/$', 'traffic_geyser.views.testcallback', name='testcallback'),

    #url(r'^$', 'conferencing.views.my_conferences', name="conferencing"),
    url(r'^$', 'schedule.views.launcher', name="conferencing"),
    url(r'^add/$', 'schedule.views.create_or_edit_event', name='conferencing_add'),
    url(r'^search/$', 'traffic_geyser.views.myconference_search', name='myconference_search'),
    url(r'^list/$', 'traffic_geyser.views.my_conferences', name='my_conferences'),
    url(r'^public/$', 'traffic_geyser.views.conferences_public', name='conferences_public'),
    url(r'^public/search/$', 'traffic_geyser.views.publicconference_search', name='publicconference_search'),
    url(r'^recorded/$', 'traffic_geyser.views.recorded_conf', name='recorded_conf'),
    
    # url(r'^popout/$', 'conferencing.views.conference_session_popout', name='conference_popout'),
    url(r'^share/video/$', 'traffic_geyser.views.make_shared_file', name='make_shared_video'),
    url(r'^share/file/$', 'traffic_geyser.views.make_shared_file', name='make_shared_file'),
    url(r'^record/desktop/$', 'traffic_geyser.views.create_desktop_recording', name='create_desktop_recording'),
    url(r'^unshare/video/$', 'traffic_geyser.views.remove_shared_file', name='remove_shared_video'),
    url(r'^unshare/file/$', 'traffic_geyser.views.remove_shared_file', name='remove_shared_file'),
    # url(r'^popout/(?P<username>[\w\._-]+)/$', 'conferencing.views.conference_session_popout', name='conference_session_popout'),
    # url(r'^(?P<urlhash>[-0-9A-Za-z_-]+)/$', 'conferencing.views.conference_session', name='conference_session'),
    url(r'^netfetch/$', 'traffic_geyser.views.netfetch', name='netfetch_ajax'),
    url(r'^shared/(?P<urlhash>[-0-9A-Za-z_-]+)/$', 'traffic_geyser.views.get_shared_files', name='get_shared_files'),
    url(r'^direct_checkout/(?P<subs_id>[0-9]+)/$', 'traffic_geyser.views.direct_to_checkout', name='conferencing_direct_to_checkout'),

    url(r'^meeting/set/(?P<urlhash>[-0-9A-Za-z_-]+)/$', 'traffic_geyser.views.conference_session2', name = 'conference_session2'),
)
