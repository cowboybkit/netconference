from django.db import models
from django.contrib.auth.models import User
     
class Client(models.Model):
    api_key = models.TextField()
    name = models.TextField()
class ClientUser(models.Model):
    client = models.ForeignKey(Client)
    user = models.ForeignKey(User)
    