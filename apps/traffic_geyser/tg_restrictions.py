from traffic_geyser.models import Client, ClientUser
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponse
from traffic_geyser.utils import *
from django.contrib.auth.models import User

def authenticate_client_require(view):
    def new_func(request, *args, **kwargs):
        if request.method == "POST":
            api_key = request.POST.get('api_key');
            user_id = request.POST.get('user_id');
            #check if parameters are not empty
            if not(api_key) or not(user_id):
                return failResponse("invalid parameters");
            #check if API key is valid
            try:
                client_obj = Client.objects.get(api_key=api_key);
            except Client.DoesNotExist:
                return failResponse("invalid api_key");
            #check if user_id is valid
            try:
                user_obj = User.objects.get(username=user_id);
                client_user = ClientUser.objects.get(user=user_obj, client=client_obj);
                request.session['client'] = client_obj;
                request.session['user_of_client'] = user_obj; 
                return view(request, *args, **kwargs)
            except ObjectDoesNotExist:
                return failResponse("invalid user_id");
        else:
            return failResponse("invalid POST method");
    return new_func