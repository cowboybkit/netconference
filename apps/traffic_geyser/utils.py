import string
from random import choice
from django.http import HttpResponse

def randomString(length=8, chars=string.letters + string.digits):
    return ''.join([choice(chars) for i in range(length)]);
def failResponse(message):
    str = "<respond><result>fail</result><error_code>" + message + "</error_code></respond>";
    return HttpResponse(str, mimetype="text/xml");
def successResponse(tag=None, value=None):  
    if tag is None:
        str = "<respond><result>success</result></respond>";
    else:
        str = "<respond><result>success</result><" + tag + ">" + value + "</" + tag + ">" +"</respond>";
    return HttpResponse(str, mimetype="text/xml");
def eventListResponse(event_list):
    str = "<respond><result>success</result><event_list>";
    for event in event_list:
        str += "<event_id>" + str(event.id) + "</event_id>";
    str += "</event_list></respond>";
    return HttpResponse(str, mimetype="text/xml");