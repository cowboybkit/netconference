from django.db import models
from django.conf import settings
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.utils.translation import ugettext_lazy as _

class Interest(models.Model):
    """Holds choices for intested_in profile field"""
    
    description = models.CharField(max_length=50)

    def __unicode__(self):
        return self.description

class Industry(models.Model):
    """Holds choices for industry profile field"""
    
    description = models.CharField(max_length=50)

    def __unicode__(self):
        return self.description

class Profile(models.Model):
    
    user = models.ForeignKey(User, unique=True, verbose_name=_('user'))
    name = models.CharField(_('name'), max_length=50, null=True, blank=True)
    about = models.TextField(_('about'), null=True, blank=True)
    location = models.CharField(_('location'), max_length=40, null=True, blank=True)
    website = models.URLField(_('website'), null=True, blank=True, verify_exists=False)
    
    phone = models.CharField(max_length=20, null=True, blank=True)
    
    #shipping information
    # shipping_name = models.CharField(max_length=50, null=True, blank=True)
    # shipping_address_1 = models.CharField(max_length=50, null=True, blank=True)
    # shipping_address_2 = models.CharField(max_length=50, null=True, blank=True)
    # shipping_city = models.CharField(max_length=50, null=True, blank=True)
    # shipping_state = models.CharField(max_length=2, null=True, blank=True)
    # shipping_country = models.CharField(max_length=50, null=True, blank=True)
    # shipping_zip = models.CharField(max_length=10, null=True, blank=True)
    
    #billing information
    billing_name = models.CharField(max_length=50, null=True, blank=True)
    billing_address_1 = models.CharField(max_length=50, null=True, blank=True)
    billing_address_2 = models.CharField(max_length=50, null=True, blank=True)
    billing_city = models.CharField(max_length=50, null=True, blank=True)
    billing_state = models.CharField(max_length=50, null=True, blank=True)
    billing_country = models.CharField(max_length=50, null=True, blank=True)
    billing_zip = models.CharField(max_length=10, null=True, blank=True)

    #interests
    interested_in = models.ManyToManyField(Interest, blank=True)

    #industry
    industry = models.ManyToManyField(Industry, blank=True)
   
    def __unicode__(self):
        return self.user.username
    
    def get_absolute_url(self):
        return ('profile_detail', None, {'username': self.user.username})
    get_absolute_url = models.permalink(get_absolute_url)
    
    class Meta:
        verbose_name = _('profile')
        verbose_name_plural = _('profiles')
        
#user_rests = get_all_restrictions()
#for el in user_rests:
    #Profile.add_to_class(el.var_name,el.used)

def create_profile(sender, instance=None, **kwargs):
    if instance is None:
        return
    profile, created = Profile.objects.get_or_create(user=instance)

def add_first_friend(instance,**kwargs):
    corp_user,is_created = User.objects.get_or_create(username=settings.NETCONFERENCE_USERNAME)
    from friends.models import Friendship
    Friendship.objects.create(to_user=instance,from_user=corp_user)
    
    
def add_first_files(instance,**kwargs):
    pass

    
post_save.connect(create_profile, sender=User)
#post_save.connect(add_first_friend, sender=User)
#post_save.connect(create_profile, sender=User)
