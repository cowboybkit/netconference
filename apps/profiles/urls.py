from django.conf.urls.defaults import *

urlpatterns = patterns('',
    url(r'^username_autocomplete/$', 'autocomplete_app.views.username_autocomplete_friends', name='profile_username_autocomplete'),
#    url(r'^$', 'profiles.views.profiles', name='profile_list'),
    url(r'^friends/$', 'profiles.views.friends', name='profile_list'),
    url(r'^profile/(?P<username>[\w\._-]+)/$', 'profiles.views.profile', name='profile_detail'),
    url(r'^profile1/(?P<username>[\w\._-]+)/$', 'profiles.views.profile1', name='profile_detail1'),
    url(r'^profile1/files/(?P<username>[\w\._-]+)/$', 'profiles.views.profile_files', name='profile_files'),
    url(r'^profile1/contacts/(?P<username>[\w\._-]+)/$', 'profiles.views.profile_contacts', name='profile_contacts'),
    url(r'^edit/$', 'profiles.views.profile_edit', name='profile_edit'),
    url(r'^profile1/addContact/(?P<username>[\w\._-]+)$', 'profiles.views.add_to_contacts', name='profile_add_contact'),
)
