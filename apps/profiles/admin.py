from django.contrib import admin
from profiles.models import Profile, Interest, Industry

from customer_service.models import Notes

class NotesInline(admin.StackedInline):
    model = Notes
    extra = 1

class ProfileAdmin(admin.ModelAdmin):
    inlines = [NotesInline]
    search_fields = ['user__username']

admin.site.register(Profile, ProfileAdmin)
#admin.site.register(Profile)
admin.site.register(Interest)
admin.site.register(Industry)
