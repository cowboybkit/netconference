from django import forms
from profiles.models import Profile, Interest, Industry
from django.utils.translation import ugettext_lazy as _

class ProfileForm(forms.ModelForm):
    name = forms.CharField(required=True, max_length=50)
    interested_in = forms.ModelMultipleChoiceField(Interest.objects.all(), widget=forms.CheckboxSelectMultiple(),required=True)
    industry = forms.ModelMultipleChoiceField(Industry.objects.all(), widget=forms.CheckboxSelectMultiple(),required=True)
    phone = forms.RegexField(regex="[\+\d\-]+")

    class Meta:
        model = Profile
        exclude = ('user', 'blogrss', 'timezone', 'language',
            'twitter_user', 'twitter_password')

    def clean_name(self):
        name = self.cleaned_data["name"].strip()
        if not name:
            raise forms.ValidationError(_("This field is required."))
        return name
        
    def clean_phone(self):
        phone = self.cleaned_data["phone"]
        regex_match = self.fields["phone"].regex.match(phone)
        if not regex_match or len(regex_match.group()) != len(self.data["phone"]):
            raise forms.ValidationError(_("Enter a valid value."))
        return phone
