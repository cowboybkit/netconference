from django.core.urlresolvers import reverse
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.http import HttpResponse, HttpResponseForbidden, HttpResponseRedirect
from django.conf import settings

from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User

from django.utils.translation import ugettext_lazy as _
from django.utils.translation import ugettext

from friends.forms import InviteFriendForm
from friends.models import FriendshipInvitation, Friendship

from microblogging.models import Following

from profiles.models import Profile
from profiles.forms import ProfileForm

from avatar.templatetags.avatar_tags import avatar
from schedule.models import Event
from files.views import sorted_object_list
from account.models import Account
from files.models import UserFile
from django.db.models import Sum
from friends.models import Contact
from files.search import get_query


if "notification" in settings.INSTALLED_APPS:
    from notification import models as notification
else:
    notification = None


def profiles(request, show_everyone=True, template_name="profiles/profiles.html", extra_context=None):
    if extra_context is None:
        extra_context = {}
    if show_everyone:
        users = User.objects.all()
    else:
        friends = Friendship.objects.friends_for_user(request.user)
        friend_user_pk = [friend["friend"].pk for friend in friends]
        users = User.objects.filter(pk__in = friend_user_pk)
    search_terms = request.GET.get('search', '')
    order = request.GET.get('order')
    if not order:
        order = 'date'
    if search_terms:
        users = users.filter(username__icontains=search_terms)
    if order == 'date':
        users = users.order_by("-date_joined")
    elif order == 'name':
        users = users.order_by("username")
    return render_to_response(template_name, dict({
        'users': users,
        'order': order,
        'search_terms': search_terms,
    }, **extra_context), context_instance=RequestContext(request))
    
@login_required    
def friends(request, **kwargs):
    return profiles(request, show_everyone = False, **kwargs) 


def profile(request, username, template_name="profiles/profile.html", extra_context=None):
    
    if extra_context is None:
        extra_context = {}
    
    other_user = get_object_or_404(User, username=username)
    
    if request.user.is_authenticated():
        is_friend = Friendship.objects.are_friends(request.user, other_user)
        is_following = Following.objects.is_following(request.user, other_user)
        other_friends = Friendship.objects.friends_for_user(other_user)
        if request.user == other_user:
            is_me = True
        else:
            is_me = False
    else:
        other_friends = []
        is_friend = False
        is_me = False
        is_following = False
    
    if is_friend:
        invite_form = None
        previous_invitations_to = None
        previous_invitations_from = None
        if request.method == "POST":
            if request.POST.get("action") == "remove": # @@@ perhaps the form should just post to friends and be redirected here
                Friendship.objects.remove(request.user, other_user)
                request.user.message_set.create(message=_("You have removed %(from_user)s from friends") % {'from_user': other_user})
                is_friend = False
                invite_form = InviteFriendForm(request.user, {
                    'to_user': username,
                    'message': ugettext("Let's be friends!"),
                })
    
    else:
        if request.user.is_authenticated() and request.method == "POST":
            if request.POST.get("action") == "invite": # @@@ perhaps the form should just post to friends and be redirected here
                invite_form = InviteFriendForm(request.user, request.POST)
                if invite_form.is_valid():
                    invite_form.save()
            else:
                invite_form = InviteFriendForm(request.user, {
                    'to_user': username,
                    'message': ugettext("Let's be friends!"),
                })
                invitation_id = request.POST.get("invitation", None)
                if request.POST.get("action") == "accept": # @@@ perhaps the form should just post to friends and be redirected here
                    try:
                        invitation = FriendshipInvitation.objects.get(id=invitation_id)
                        if invitation.to_user == request.user:
                            invitation.accept()
                            request.user.message_set.create(message=_("You have accepted the friendship request from %(from_user)s") % {'from_user': invitation.from_user})
                            is_friend = True
                            other_friends = Friendship.objects.friends_for_user(other_user)
                    except FriendshipInvitation.DoesNotExist:
                        pass
                elif request.POST.get("action") == "decline": # @@@ perhaps the form should just post to friends and be redirected here
                    try:
                        invitation = FriendshipInvitation.objects.get(id=invitation_id)
                        if invitation.to_user == request.user:
                            invitation.decline()
                            request.user.message_set.create(message=_("You have declined the friendship request from %(from_user)s") % {'from_user': invitation.from_user})
                            other_friends = Friendship.objects.friends_for_user(other_user)
                    except FriendshipInvitation.DoesNotExist:
                        pass
        else:
            invite_form = InviteFriendForm(request.user, {
                'to_user': username,
                'message': ugettext("Let's be friends!"),
            })
    
    previous_invitations_to = FriendshipInvitation.objects.invitations(to_user=other_user, from_user=request.user)
    previous_invitations_from = FriendshipInvitation.objects.invitations(to_user=request.user, from_user=other_user)
    
    return render_to_response(template_name, dict({
        "is_me": is_me,
        "is_friend": is_friend,
        "is_following": is_following,
        "other_user": other_user,
        "other_friends": other_friends,
        "invite_form": invite_form,
        "previous_invitations_to": previous_invitations_to,
        "previous_invitations_from": previous_invitations_from,
    }, **extra_context), context_instance=RequestContext(request))


@login_required
def profile_edit(request, form_class=ProfileForm, **kwargs):
    
    template_name = kwargs.get("template_name", "profiles/profile_edit.html")
    
    if request.is_ajax():
        template_name = kwargs.get(
            "template_name_facebox",
            "profiles/profile_edit_facebox.html"
        )
    
    profile = request.user.get_profile()
    
    if request.method == "POST":
        profile_form = form_class(request.POST, instance=profile)
        if profile_form.is_valid():
            profile = profile_form.save(commit=False)
            profile.user = request.user
            profile.interested_in = profile_form.cleaned_data["interested_in"]
            profile.industry = profile_form.cleaned_data["industry"]
            profile.save()
            return HttpResponseRedirect(reverse("profile_detail", args=[request.user.username]))
    else:
        profile_form = form_class(instance=profile)
    
    return render_to_response(template_name, {
        "profile": profile,
        "profile_form": profile_form,
    }, context_instance=RequestContext(request))

def profile1(request, username):
    if username == request.user.username:
        is_public = False
    else:
        is_public = True
    user_obj = get_object_or_404(User, username=username)
    try:
        account = Account.objects.get(user = user_obj)
    except: 
        account = None
    try:
        timezone = account.timezone
    except:
        timezone = settings.TIME_ZONE 
    #qs = Event.objects.filter(creator=request.user)
    qs = Event.objects.filter(creator=user_obj)
    qs = qs.order_by('type', '-start')
    #return render_to_response('profiles/newer_profile.html', {"conferences_button":True}, context_instance=RequestContext(request))
    try:
        per_page = request.GET['per_page']
        per_page = int(per_page)
    except:
        per_page = settings.PROFILE_PAGE_ITEMS_LIST_PAGINATE_BY 
    return sorted_object_list(
            request,
            template_name = 'profiles/newer_profile.html',
            queryset = qs,
            paginate_by = per_page,
            extra_context ={
                    "conferences_button" : True,
                    "per_page" : per_page,
                    "is_public" : is_public,
                    "user_obj" : user_obj,
                    "timezone" : timezone,
                }
            )

def profile_files(request, username):
    user_obj = get_object_or_404(User, username=username)
    is_public, timezone = get_profile_info(request, username, user_obj) 
    qs = UserFile.active_objects.filter(user = user_obj)
    total_bytes = sum([file.file_size for file in qs]) 
    if total_bytes > 1000000000:
        total_bytes = str(total_bytes/1000000000) + ' GB'
    elif total_bytes > 1000000:
        total_bytes = str(total_bytes/1000000) + ' MB'
    elif total_bytes > 1000:
        total_bytes = str(total_bytes/1000) + ' KB'
    else:
        total_bytes = str(total_bytes) + ' Bytes'
    if is_public:
        qs = qs.filter(visibility = 'U') 
    per_page = request.GET.get('per_page', '')
    if not per_page:
        per_page = settings.PROFILE_PAGE_ITEMS_LIST_PAGINATE_BY
    per_page = int(per_page)
    return sorted_object_list(request,
        template_name="profiles/newer_profile_files.html",
        queryset = qs,
        paginate_by = per_page,
        extra_context = {
            "per_page" : per_page,
            "files_button" : True,
            "user_obj" : user_obj,
            "timezone" : timezone,
            "is_public" : is_public,
            "total_bytes" : total_bytes
        }
    ) 

def profile_contacts(request, username):
    user_obj = get_object_or_404(User, username=username)
    is_public, timezone = get_profile_info(request, username, user_obj)
    contacts = Contact.objects.filter(user=user_obj)
    if request.GET.get('search', ''):
        search_query = request.GET.get('search').strip()
        if search_query:
            entry_query = get_query(search_query, ['name', 'email']) 
            contacts = contacts.filter(entry_query)
    per_page = request.GET.get('per_page', '')
    if not per_page:
        per_page = settings.PROFILE_PAGE_ITEMS_LIST_PAGINATE_BY
    per_page = int(per_page)
    return sorted_object_list(
        request,
        template_name = "profiles/newer_profile_contacts.html",
        queryset = contacts,
        paginate_by = per_page,
        extra_context = {
            "per_page" : per_page,
            "contacts_button" : True,
            "user_obj" : user_obj,
            "timezone" : timezone,
            "is_public" : is_public,
        } 
    )

def get_profile_info(request, username, user_obj):
    if username == request.user.username:
        is_public = False
    else:
        is_public = True
    try:
        account = Account.objects.get(user=user_obj)
    except : 
        account = None
    try:
        timezone = account.timezone
    except:
        timezone = settings.TIME_ZONE 
    return is_public, timezone

def add_to_contacts(request, username):
    user_obj = get_object_or_404(User, username=username)
    try:
        Contact.objects.get(user=request.user, email=user_obj.email)
        request.user.message_set.create(message="Contact with this user's email already exists")
    except Contact.DoesNotExist:
        name = (str(user_obj.first_name) + " " + str(user_obj.last_name)) or user_obj.username
        Contact.objects.create(user=request.user, name=name, email=user_obj.email)
        request.user.message_set.create(message="Contact successfully added")
        return HttpResponseRedirect(reverse("invitations_contacts"))
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
