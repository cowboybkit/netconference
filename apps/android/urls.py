from django.conf.urls.defaults import *

urlpatterns = patterns('',
    url(r'^files$', 'android.views.files', name="android_user_files"),
    url(r'^conferences$', 'android.views.conferences', name="android_user_conferences"),
    url(r'^login/$', 'android.views.login_ajax', name="android_user_login"),
)
