from django.http import  HttpResponse
from django.contrib.auth import authenticate, login
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.utils import simplejson
from django.template.defaultfilters import date
from django.views.decorators.http import require_POST

from schedule.models import Event
from files.models import UserFile

@login_required
def files(request):
    user = request.user
    files = UserFile.active_objects.filter(user=user).order_by('-timestamp')[:6]
    files_json = []
    for file in files:
        json = {}
        json["pk"] = file.id
        json["fields"] = {}
        json["fields"]["title"] = file.title
        json["fields"]["urlhash"] = file.urlhash
        files_json.append(json)
    response = simplejson.dumps(files_json)
    jsonp = request.GET.get('x', '')
    response = "%s(%s)" %(jsonp, response)
    return HttpResponse(response, mimetype="application/x-javascript; charset=utf-8")

@login_required
def conferences(request):
    user = request.user
    events = Event.objects.filter(creator=user).exclude(type="G").order_by('-created_on')[:6]
    events_json = []
    for event in events:
        json = {}
        json["pk"] = event.id
        json["fields"] = {}
        json["fields"]["title"] = event.title
        json["fields"]["date"] = date(event.start, settings.DATETIME_FORMAT)
        events_json.append(json)
    response = simplejson.dumps(events_json)
    jsonp = request.GET.get('x', '')
    response = "%s(%s)" %(jsonp, response)
    return HttpResponse(response, mimetype="application/x-javascript; charset=utf-8")

@require_POST
def login_ajax(request):
    response = simplejson.dumps({"authenticated": "false"})
    user = authenticate(username=request.POST.get("username"), password=request.POST.get("password"))
    if user is not None:
        login(request, user)
        response = simplejson.dumps({"authenticated": "true"})
    return HttpResponse(response, mimetype="application/x-javascript; charset=utf-8")
