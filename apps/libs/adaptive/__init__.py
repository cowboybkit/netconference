from django.conf import settings
import urllib, urllib2
try:
    import json
except ImportError:
    from django.utils import simplejson as json

def get_adaptive_payment_url(returnUrl, cancelUrl, receiverList, **kwargs):
    
    API_END_POINT = 'https://svcs.sandbox.paypal.com/AdaptivePayments/Pay'
    if settings.DEBUG:
        APP_ID = "APP-80W284485P519543T"
    else:
        APP_ID = settings.APP_ID
    headers = {
    "X-PAYPAL-SECURITY-USERID": settings.PAYPAL_API_USER,
    "X-PAYPAL-SECURITY-PASSWORD": settings.PAYPAL_API_KEY,
    "X-PAYPAL-SECURITY-SIGNATURE": settings.PAYPAL_API_SIG,
    "X-PAYPAL-DEVICE-IPADDRESS": settings.PAYPAL_DEVICE_IP_ADDRESS,
    "X-PAYPAL-REQUEST-DATA-FORMAT": "JSON",
    "X-PAYPAL-RESPONSE-DATA-FORMAT": "JSON",
    "X-PAYPAL-APPLICATION-ID": APP_ID
    }
    data = {"returnUrl":returnUrl,
            "requestEnvelope":{"errorLanguage":"en_US"},"currencyCode":"USD",
            "receiverList":{"receiver":receiverList},
            "cancelUrl":cancelUrl,
            "actionType":"PAY"
    }
    data.update(**kwargs)
    
    request = urllib2.Request(API_END_POINT, data = json.dumps(data), headers = headers)
    resp = json.loads(urllib2.urlopen(request).read())
    try:
        key = resp["payKey"]
    except KeyError:
        print resp
        return None
    if settings.DEBUG:
        REDIRECT_ENDPOINT = "https://www.sandbox.paypal.com/webscr"
    else:
        REDIRECT_ENDPOINT = "https://www.paypal.com/webscr"
    return "%s?cmd=_ap-payment&paykey=%s"%(REDIRECT_ENDPOINT,key)
    
    