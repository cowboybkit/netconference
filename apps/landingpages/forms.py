from django import forms
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _, ugettext

from emailconfirmation.models import EmailAddress

class LandingPageSignupForm(forms.Form):
    name = forms.CharField()
    username = forms.CharField(max_length = 30)
    email = forms.EmailField()
    phone = forms.CharField()
