# Create your views here.
from django.shortcuts import render_to_response
from django.template import RequestContext
from landingpages.forms import LandingPageSignupForm
from account.forms import LandingSignupForm
from django.conf import settings
from django.contrib.auth.models import User
from subscription.models import Subscription
from dateutil.relativedelta import relativedelta
import datetime
from django.contrib.auth import login, authenticate
from django.http import HttpResponseRedirect
from account.views import register_email_autoresponder
from emailconfirmation.models import EmailAddress
from django.contrib.auth import authenticate, login
from account.views import after_withoutcc_signup, send_welcome_autoresponder
from django.core.urlresolvers import reverse

FREE_SUBSCRIPTION_NAME = 'pro'
LOGIN_REDIRECT_URL = settings.LOGIN_REDIRECT_URL

def landingpage1(request):
    form = LandingPageSignupForm()
    if request.method == 'POST':
        form = LandingPageSignupForm(data=request.POST)
        if form.is_valid():
            cleaned_data = form.cleaned_data
            user = User.objects.create_user(username=cleaned_data['username'], email=cleaned_data['email']) 
            password = User.objects.make_random_password()
            request.session['random_password'] = password
            user.set_password(password)
            user.save()
            profile = user.profile_set.get()
            profile.name = cleaned_data['name']
            profile.phone = cleaned_data['phone']
            profile.save()
            user = authenticate(username=cleaned_data['username'], password=password)
            login(request, user)
            after_withoutcc_signup(request)
            register_email_autoresponder(user, True, request=request)
            EmailAddress.objects.add_email(user, cleaned_data['email'])
            return HttpResponseRedirect(LOGIN_REDIRECT_URL)
    return render_to_response("landingpages/landingpage.html", {}, context_instance=RequestContext(request))

def landing_signup(request):
    form = LandingSignupForm()
    if request.method == 'POST':
        form = LandingSignupForm(request.POST)
        if form.is_valid():
            username, password = form.save()
            user = authenticate(username=username, password=password)
            login(request, user)
            after_withoutcc_signup(request)
            send_welcome_autoresponder(user)
            return HttpResponseRedirect(reverse('conferencing'))
    return render_to_response("landingpages/landing7.html", {'form':form}, context_instance=RequestContext(request))
