from django.conf.urls.defaults import *
from django.views.generic.simple import direct_to_template
from landingpages.views import landing_signup, landingpage1

urlpatterns = patterns('', 
    url(r'^landing_page1/$', landingpage1, name="landing_page1"),
    url(r'^landing_page6/$', direct_to_template, {"template": "landingpages/landing_page6.html"}, name="landing_page6"),
    url(r'^landing1/$', direct_to_template, {"template": "landingpages/landing1.html"}, name="landing1"),
    url(r'^landing2/$', direct_to_template, {"template": "landingpages/landing2.html"}, name="landing2"),
    url(r'^landing3/$', direct_to_template, {"template": "landingpages/landing3.html"}, name="landing3"),
    url(r'^landing4/$', direct_to_template, {"template": "landingpages/landing4.html"}, name="landing4"),
    url(r'^landing5/$', direct_to_template, {"template": "landingpages/landing5.html"}, name="landing5"),
    url(r'^landing6/$', direct_to_template, {"template": "landingpages/landing6.html"}, name="landing6"),
    url(r'^landing7/$', landing_signup , name="landing7"),
)
