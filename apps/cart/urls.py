from django.conf.urls.defaults import *
import views

urlpatterns = patterns('cart.views',
    (r'^$', 'show_cart', {'template_name':'cart/cart.html'}, 'show_cart'),
)
