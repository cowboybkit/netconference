from django.conf.urls.defaults import *
from django.views.generic.simple import direct_to_template
from django.views.generic.list_detail import object_list, object_detail
from django.conf import settings

ssl = settings.ENABLE_SSL

{'SSL': ssl },

urlpatterns = patterns('',
    url(r'^$', 'netsign.views.my_netsign', {'SSL': ssl }, name='my_netsign'),
    url(r'^about/$', 'netsign.views.about', {'SSL': ssl }, name='netsign_about'),
    url(r'^select/$', 'netsign.views.select', {'SSL': ssl }, name='select_netsign'),
    url(r'^search/$', 'netsign.views.search', {'SSL': ssl }, name='netsign_search'),
    url(r'^get_profile/(?P<username>[\w\._-]+).json$', 'netsign.views._get_profile', {'SSL': ssl }, name='_netsign_profile'),
    url(r'^create/(?P<urlhash>[-0-9A-Za-z_-]+)/$', 'netsign.views.create', {'SSL': ssl }, name = 'netsign_create'),
    url(r'^review/(?P<urlhash>[-0-9A-Za-z_-]+)/$', 'netsign.views.review', {'SSL': ssl }, name = 'netsign_review'),
    url(r'^sign/(?P<urlhash>[-0-9A-Za-z_-]+)/$', 'netsign.views.sign', {'SSL': ssl }, name = 'netsign_sign'),
    url(r'^sign-done/$', direct_to_template, 
        {'SSL': ssl,
         'template':'netsign/netsign_done.html'}, name = 'netsign_sign_done'),
    url(r'^review-sign/(?P<urlhash>[-0-9A-Za-z_-]+)/$', 'netsign.views.review_sign', {'SSL': ssl }, name = 'netsign_review_sign'),
    url(r'^download-pdf/(?P<urlhash>[-0-9A-Za-z_-]+)/$', 'netsign.views.download_signed_pdf', {'SSL': ssl }, name = 'netsign_signed_pdf'),
    url(r'^delete/(?P<urlhash>[-0-9A-Za-z_-]+)/$', 'netsign.views.delete', {'SSL': ssl }, name = 'netsign_delete'),
    url(r'^invitation/sent/$', 'netsign.views.sent_invites', {'SSL': ssl }, name = 'netsign_sent_invites'),
    url(r'^invitation/received/$', 'netsign.views.received_invites', {'SSL': ssl }, name = 'netsign_received_invites'),
    url(r'^_items/(?P<urlhash>[-0-9A-Za-z_-]+)/$', 'netsign.views.netsign_items', {'SSL': ssl }, name = 'netsign_items'),
    url(r'^_items_signed/(?P<urlhash>[-0-9A-Za-z_-]+)/$', 'netsign.views.netsign_items_signed', {'SSL': ssl }, name = 'netsign_items_signed'),
    url(r'^_items_signed_all/(?P<urlhash>[-0-9A-Za-z_-]+)/$', 'netsign.views.netsign_items_signed_all', {'SSL': ssl }, name = 'netsign_items_signed_all'),
    url(r'^preview/(?P<urlhash>[-0-9A-Za-z_-]+)/$', 'netsign.views.preview', {'SSL': ssl }, name = 'netsign_preview'),
    url(r'^status/(?P<urlhash>[-0-9A-Za-z_-]+)/$', 'netsign.views.status', {'SSL': ssl }, name = 'netsign_status'),

    url(r'^user_sign/$', 'netsign.views.user_sign', {'SSL': ssl }, name='netsign_user_sign'),
    url(r'^uuid_url/$', 'netsign.views.uuid_url', {'SSL': ssl }, name='netsign_uuid_url'),
    url(r'^scan_sign_upload/$', 'netsign.views.scan_sign_upload', {'SSL': ssl }, name='netsign_scan_sign_upload'),
    
    # url(r'^search/$', 'files.views.my_files_search', name='file_search'),
    # url(r'^(?P<username>[\w\._-]+)/$', 'files.views.user_files', name="user_files"),
    # url(r'^(?P<username>[\w\._-]+)/search/$', 'files.views.user_files_search', name='user_files_search'),
    # url(r'^edit/(?P<urlhash>[-0-9A-Za-z_-]+)/$', 'files.views.file_edit',  name='file_edit'),
    # url(r'^download/(?P<urlhash>[-0-9A-Za-z_-]+)/$', 'files.views.file_download',  name='file_download'),
    # url(r'^file/delete/(?P<urlhash>[-0-9A-Za-z_-]+)/$', 'file_uploads.views.file_delete',  name='file_delete'),
)
