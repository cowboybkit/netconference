from mailer import send_html_mail as send_mail
from django.conf import settings
from datetime import datetime
from netsign.models import NetSignInvitation
from django.template.loader import render_to_string
from django.core.management.base import NoArgsCommand
from django.contrib.sites.models import Site
from django.utils.translation import ugettext_lazy as _

class Command(NoArgsCommand):
    help = "Send reminders for unsigned netsign invitations"

    def handle_noargs(self, **options):
        now = datetime.now()
        unsigned_netsigns = NetSignInvitation.objects.exclude(deadline__isnull=True).exclude(status="9").filter(deadline__gte=now)
        for netsign in unsigned_netsigns:
            message = render_to_string("netsign/reminder.txt", {"netsign": netsign,
                                                                "site": Site.objects.get_current(),
                                                                "MEDIA_URL": settings.MEDIA_URL,})
            subject = _(u"Reminder to sign document: %(netsign_document)s" %({"netsign_document":netsign.document.title}))
            to_email = netsign.to_email.split(",")
            from_email = netsign.from_user.email or settings.DEFAULT_FROM_EMAIL
            send_mail(subject, "", message, from_email, to_email)
        return unsigned_netsigns.count()
