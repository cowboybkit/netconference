import base64
import datetime, uuid

from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
from netconf_utils import tempfile, amazon_s3

from files.models import UserFile

try:
    import json
except:
    import simplejson as json            

from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import mm
from django.core.urlresolvers import reverse
from pyPdf import PdfFileReader, PdfFileWriter
from pyPdf.utils import PdfReadError
       

pixel = 0.72 # point = 0.75* pixel
MAX_Y_POINT_A4 = 841.88



from base64 import urlsafe_b64encode, urlsafe_b64decode

def uri_b64encode(s):
    return urlsafe_b64encode(s).strip('=')

def uri_b64decode(s):
    return urlsafe_b64decode(s + '=' * (4 - len(s) % 4))


NETSIGN_STATUS = (
    ("1", "Created"),
    ("2", "Sent"),
    ("3", "Failed"),
    ("4", "Expired"),
    ("5", "Accepted"),
    ("6", "Declined"),
    ("7", "Pending"),
    ("8", "Deleted"),
    ("9", "Signed")
)

class DeadlinePassed(Exception):
    pass

class NetSignDocument(models.Model):
    """A NetSign document object, for attaching NetSignItems to."""
    title = models.CharField(blank=True, max_length=255)
    user_file = models.ForeignKey(UserFile, related_name="netsign_document_file")
    creator = models.ForeignKey(User, related_name="netsign_document_creator")
    saved_at = models.DateTimeField(blank=True, auto_now_add=True)
    
    user = models.ForeignKey(User)
    
    class Meta:
        ordering = ('-saved_at',)
        
    def save(self,*args,**kwargs):
        if not self.pk or not self.user:
            self.user = self.user_file.user
        super(NetSignDocument,self).save(*args,**kwargs)

    def ordered_pages_with_content(self):
        """Helper function, to create pdfs"""
        nsis = self.netsign_item.all()
        ordered_content_pages = sorted(set([el.page-1 for el in nsis]))
        return ordered_content_pages
    
    def get_netsign_document_url(self):
        return '/netsign/review/%s/' % uri_b64encode(str(self.pk * 42))

    def get_netsign_document_urlhash(self):
        return uri_b64encode(str(self.pk * 42))

    def get_status_url(self):
        return reverse("netsign_status", kwargs={"urlhash": uri_b64encode(str(self.pk * 42))})
        
    def __unicode__(self):
        return self.title

    @property
    def hash(self):
        return uri_b64encode(str(self.pk * 42))
        
    # commented out, probably not necessary right now
    # def __init__(self, arg):
    #     super(NetSignDocument, self).__init__()
    #     self.arg = arg
        
    def save_items(self,items_dict):
        for page_no,page_val in items_dict.items():
            page_no_int = int(page_no.split("page")[1])
            for item in page_val:
                self.netsign_item.create(page=page_no_int,
                                         title=item['value'],
                                         required=item.get('required',0),
                                         drop_x=item['x'],
                                         drop_y=item['y'],
                                         message=item.get('message',''),
                                         width=item.get('width',''),
                                         height=item.get('height',''),
                                         read_only=item.get('read_only',False),
                                         data_type=item.get('type',''),
                                         party_email=item.get('userid',''))

    def save_signors(self, signors_dict):
        for signor in signors_dict:
            self.netsign_signors.create(name=signor["sname"],
                                        email=signor["semail"],
                                        uuid=uuid.uuid4())
                
    def get_items_json_for_flash(self,as_string=True,signed=False):
        nsitems_list = []
        for nsitem in self.netsign_item.all():
            nsitems_list.append(nsitem.get_dict_for_flash(signed=signed))

        netsign_json = format_dicts_by_page(nsitems_list,as_string=as_string)
            
        return netsign_json
        
    def get_signed_values_for_flash(self,as_string=True):
        document_marks_list = []
        for nsi in self.netsign_invite_file.all():
            marks_list = []
            for mark in nsi.netsignmark_set.all():
                marks_list.append(mark.get_dict_for_flash())
            document_marks_list.extend(marks_list)
        items = self.netsign_item.all()
        for item in items:
            if item.read_only == 1 and item.party_email == self.creator.email:
                document_marks_list.append(item.get_dict_for_flash())

        netsign_marks = format_dicts_by_page(document_marks_list,as_string=as_string)
            
        return netsign_marks

    def merge_pdfs(self, pdf1, pdf2):
        """
            Merges pdf2 on top of pdf1, page by page
            Returns modified pdf1
        """
        for i, page in enumerate(pdf1.pages):
            page.mergePage(pdf2.getPage(i))

        return pdf1

    def generate_summary_pdf(self, pdf_list):
        """
            Merge all pdfs in pdf_list on top of each other
        """
        fp = self.obtain_user_file()
        try:
            original_pdf = PdfFileReader(fp)
        except PdfReadError:
            return None
        num_pages = range(original_pdf.numPages)
        pdf_file_list = []
        for pdf_file in pdf_list:
            try:
                pdf_file_list.append(PdfFileReader(open(pdf_file, 'r')))
            except IOError:
                continue            
        for page_num in num_pages:
            for sign_only_file in pdf_file_list:
                sign_page = sign_only_file.getPage(page_num)
                original_pdf.getPage(page_num).mergePage(sign_page)
        output_pdf = PdfFileWriter()
        for page in original_pdf.pages:
            output_pdf.addPage(page)
        output_pdf_file = open("%s/netsign/signed-final-%s.pdf" %(settings.MEDIA_ROOT, self.id), "wb")
        output_pdf.write(output_pdf_file)
        output_pdf_file.close()
        output_pdf_file = open(output_pdf_file.name, "r")
        amazon_s3.upload_new_file(file_name="netsign/signed-final-%s.pdf" % self.id, fp=output_pdf_file)
        output_pdf_file.close()
        return output_pdf_file.name

    def obtain_user_file(self):
        uf = self.user_file
        return amazon_s3.download_and_return_tmp_fp(uf)
    
    def get_or_obtain_user_file(self):
        return self.obtain_user_file()
                    
from django.utils.datastructures import SortedDict
    
def format_dicts_by_page(nsitems_list,as_string=True):
    netsign_json = {}
    for nsitem_dict in nsitems_list:
        page = "page%s"%nsitem_dict.pop('page')
        try:
            netsign_json[page].append(nsitem_dict)
        except:
            netsign_json[page] = [nsitem_dict,]

    json_string = json.dumps(netsign_json) if as_string else netsign_json
    return json_string
    
    
        
class NetSignInvitationManager(models.Manager):
    def invitations(self, *args, **kwargs):
        return self.filter(*args, **kwargs).exclude(status__in=["6", "8"])

TEXT_SIZE = 10

class NetSignInvitation(models.Model):
    """
    A NetSign invite is an invitation from one user to another to be signed and/or approved.
    """
    document = models.ForeignKey(NetSignDocument, related_name="netsign_invite_file")
    from_user = models.ForeignKey(User, related_name="netsign_from")
    to_user = models.ForeignKey(User, related_name="netsign_to", blank=True, null=True)
    to_email = models.CharField(blank=True, max_length=255)
    message = models.TextField()
    sent = models.DateField(default=datetime.date.today)
    saved_at = models.DateTimeField(blank=True, auto_now_add=True)
    status = models.CharField(max_length=2, choices=NETSIGN_STATUS)
    deadline = models.DateTimeField(null=True)

    objects = NetSignInvitationManager()
    
    class Meta:
        ordering = ('-saved_at',)
        
    def save(self,*args,**kwargs):
        if self.to_email:
            try:
                self.to_user = User.objects.filter(email=self.to_email)[0]
            except:
                pass
        if self.deadline and datetime.datetime.now() > self.deadline:
            raise DeadlinePassed        
        super(NetSignInvitation,self).save(*args,**kwargs)

    def get_source_type(self):
        return self.document.user_file.source_type or "pdf"
    

    
    @property
    def uhash(self):
        return uri_b64encode(str(self.pk * 42))
        

    def __unicode__(self):
        return "%s, from %s to %s" % (self.document, self.from_user, self.to_email)
    
    def get_netsign_invite_url(self):
        return '/netsign/invite/%s/' % uri_b64encode(str(self.pk * 42))

    def accept(self):
        if not Friendship.objects.are_friends(self.to_user, self.from_user):
            friendship = Friendship(to_user=self.to_user, from_user=self.from_user)
            friendship.save()
            self.status = "5"
            self.save()
            if notification:
                notification.send([self.from_user], "friends_accept", {"invitation": self})
                notification.send([self.to_user], "friends_accept_sent", {"invitation": self})
                for user in friend_set_for(self.to_user) | friend_set_for(self.from_user):
                    if user != self.to_user and user != self.from_user:
                        notification.send([user], "friends_otherconnect", {"invitation": self, "to_user": self.to_user})

    def decline(self):
        if not Friendship.objects.are_friends(self.to_user, self.from_user):
            self.status = "6"
            self.save()

    def save_sign(self,netsign_json):
        #Ideally done by obtaining the ns_items and calling sign method on each one of those, to create corresponding mark model; But for now, querying for each.
        #ns_items = list(self.document.netsign_item.all())
        loaded_json = json.loads(netsign_json)
        items_dict = loaded_json.get('netsignitems')
        for page_val in items_dict.values():
            for item in page_val:
                nsitem = NetSignItem.objects.get(pk=item.get('item_pk'))
                self.netsignmark_set.create(signed_value=item['value'],
                                            parent_item=nsitem,
                                            data_type=item.get('type', 'text')
                                            )
    @models.permalink
    def get_sign_url(self):
        return ('netsign_sign',(self.uhash,))
                
    @property
    def verbose_status(self):
        return dict(NETSIGN_STATUS).get(self.status)
    
    def get_signed_values_for_flash(self,as_string=True):
        marks_list = []
        for mark in self.netsignmark_set.all():
            marks_list.append(mark.get_dict_for_flash())

        items = self.document.netsign_item.all()
        for item in items:
            if item.read_only == 1 and item.party_email == self.document.creator.email:
                marks_list.append(item.get_dict_for_flash())

        netsign_marks = format_dicts_by_page(marks_list,as_string=as_string)
            
        return netsign_marks

    @models.permalink
    def get_signed_url(self):
        return ('netsign_review_sign',(self.uhash,))
    
    def get_or_create_sign_pdf(self):
        return self.create_signed_pdf()

    def create_sign_pdf(self,pagesize=A4):
        """Private method that creates a pdf with only user signed elements,
        Returns the filename with full path.
        """
        #Eventually replace this with StringIO for performance.
        sign_file_name = "%s/netsign/sign-only-%s.pdf"%(settings.MEDIA_ROOT,self.id)
        p = canvas.Canvas(sign_file_name,pagesize=pagesize)
        p.setFillColorRGB(0,0,0)
        FONT_SIZE = 7
        p.setFontSize(size=FONT_SIZE)
        pages = self.document.get_signed_values_for_flash(as_string=False)
        fp = self.obtain_user_file()
        original_pdf = PdfFileReader(fp)
        num_pages = range(original_pdf.numPages)
        #Sort pages in the page order before creating the pdf
        for page_no in num_pages:
            if not pages.get('page%s' %(page_no+1)):
                p.showPage()
                continue
            for item in pages['page%s'%str(page_no+1)]:
                x_coord = item['x']*pixel
                y_coord = float(pagesize[1]) - item['y']*pixel
                if item['type'] in ('mousesign', 'scanimage'):
                    decoded_png = tempfile.NamedTemporaryFile(delete=False,suffix='.png')
                    decoded_png.write(base64.decodestring(item['value']))
                    decoded_png.close()
                    IMAGE_SIZE = (264,97)
                    p.drawImage(decoded_png.name,x_coord,y_coord-IMAGE_SIZE[1],mask='auto',width=IMAGE_SIZE[0],height=IMAGE_SIZE[1])
                else:
                    textobject = p.beginText()
                    textobject.setTextOrigin(x_coord,y_coord-10)
                    textobject.textLines(item['value'])
                    p.drawText(textobject)
                    #p.drawString(x_coord,y_coord-FONT_SIZE,item['value'])
            p.showPage()
        p.save()
        return sign_file_name
        
    def obtain_user_file(self):
        uf = self.document.user_file
        return amazon_s3.download_and_return_tmp_fp(uf)
    
    def get_or_obtain_user_file(self):
        return self.obtain_user_file()
        
   
    def create_signed_pdf(self):
        fp = self.get_or_obtain_user_file()
        try:
            original_pdf = PdfFileReader(fp)
        except PdfReadError:
            return ""
        original_pdf_page_size = original_pdf.getPage(0).mediaBox.upperRight
        #only_sign_pdf = PdfFileReader(open(self.get_or_create_sign_pdf()))
        only_sign_pdf = PdfFileReader(open(self.create_sign_pdf(pagesize=original_pdf_page_size)))

        #This is a bug in pyPdf. It detects some files as encrypted, even if they are not. It can be circumvented by asking it to decrypt with an empty string.
        if original_pdf.isEncrypted:
            original_pdf.decrypt('')
        
        for i,sign_page in enumerate(original_pdf.pages):
            sign_page.mergePage(only_sign_pdf.getPage(i))

        signed_pdf = PdfFileWriter()
        for page in original_pdf.pages:
            signed_pdf.addPage(page)

        signed_pdf_filename = "%s/netsign/signed-%s.pdf"%(settings.MEDIA_ROOT,self.id)
        signed_pdf_file = open(signed_pdf_filename,mode='wb')
        
        signed_pdf.write(signed_pdf_file)
        signed_pdf_file.close()
        return signed_pdf_filename
                    
    def get_message(self):
        try:
            return self.attachment_netsign.all()[0].message
        except:
            return None
    
class NetSignItem(models.Model):
    """A vessel for a piece of information, a signature, or text input attached to a NetSign document."""
    user_file = models.ForeignKey(NetSignDocument, related_name="netsign_item")
    title = models.TextField()
    required = models.BooleanField(default=False)
    page = models.IntegerField()
    drop_x = models.IntegerField()
    drop_y = models.IntegerField()
    
    read_only = models.BooleanField(default=False)
    #Using char field for width and height allows using flash format with units
    #TODO Verify the blank and null, if they can be removed.
    width = models.CharField(max_length=10,blank=True,null=True)
    height = models.CharField(max_length=10,blank=True,null=True)
    data_type = models.CharField(max_length=10,default='text')
    party_email = models.EmailField(blank=True)
    
    message = models.TextField(blank=True)
    created_at = models.DateTimeField(blank=True, auto_now_add=True)
    saved_at = models.DateTimeField(blank=True, auto_now=True, auto_now_add=True)

    class Meta:
        ordering = ('-saved_at', 'title')

    def __unicode__(self):
        return self.title
    
    def get_dict_for_flash(self,signed=False,nsi=None):
        required_fields = {'data_type':'type',
                           'required':'required',
                           'drop_x':'x',
                           'drop_y':'y',
                           'width':'width',
                           'read_only':'read_only',
                           'height':'height',
                           'page':'page',
                           'title':'value',
                           'id':'item_pk',
                           'party_email': 'userid'
                           }
        flash_dict = {}
        query_dict = self.__dict__
        if signed:
            query_dict = self.__dict__.copy()
            query_dict.update(self.netsignmark_set.get(parent_invitation=nsi).__dict__)
            required_fields['signed_value'] = 'value'
        for db_name,struct_name in required_fields.items():
            flash_dict[struct_name] = query_dict[db_name]
        
        return flash_dict
    
        
class NetSignMark(models.Model):
    """A 'signing' object attached to the NetSignItem vessel"""
    parent_item = models.ForeignKey(NetSignItem,unique=True)
    parent_invitation = models.ForeignKey(NetSignInvitation)
    
    #if you are saving a mark, status is signed, right?
    #status = models.CharField(max_length=2, choices=NETSIGN_STATUS)
    
    created_at = models.DateTimeField(blank=True, auto_now_add=True)
    saved_at = models.DateTimeField(blank=True, auto_now=True, auto_now_add=True)
    
    signed_value = models.TextField()
    data_type = models.CharField(max_length=25, blank=True)
    
    def __unicode__(self):
        return "%s" % self.signed_value
    
    def get_dict_for_flash(self):
        items_dict = self.parent_item.get_dict_for_flash(signed=True,nsi=self.parent_invitation)
        if self.data_type:
            items_dict['type'] = self.data_type
        return items_dict
    
    def get_decoded_png(self):
        return base64.decodestring(self.signed_value)
        
    
class NetSignUserSignature(models.Model):
    """Signature of the contract creator"""
    
    user = models.ForeignKey(User, unique=True)
    scanned_image = models.TextField()
    text_signature = models.TextField()
    mouse_sign  = models.TextField()

    created_at = models.DateTimeField(auto_now_add=True)
    saved_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return "%s : %s" %(self.user, self.created_at)

    def get_decoded_png(self, signature_type):
        return base64.decodestring(getattr(self, signature_type))


class NetSignSignor(models.Model):
    name = models.CharField(max_length=50)
    email = models.EmailField()
    uuid = models.CharField(max_length=36, default=str(uuid.uuid4()))
    document = models.ForeignKey(NetSignDocument, related_name="netsign_signors")
    
    def __unicode__(self):
        return "%s : %s : %s" %(self.document, self.email, self.uuid)
