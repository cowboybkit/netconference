from django.test import TestCase
from debug import ipython,idebug
from pyPdf import PdfFileReader, PdfFileWriter

from netsign.models import NetSignDocument,NetSignItem,NetSignMark,NetSignInvitation
from files.models import UserFile
from django.contrib.auth.models import User
from pprint import pprint

try:
    import json
except:
    import simplejson as json


class NetsignTest(TestCase):

    def setUp(self):
        self.test_user = User.objects.create_user('test','lennon@thebeatles.com',password='test')
        self.client.login(username='test',password='test')
        
        #First create a userfile object
        self.user_file = UserFile(title='Test File',
                                  user=self.test_user,
                                  filename='Test Name',
                                  source_type='pdf',
                                  current_type='pdf',
                                  processed=1,
                                  is_active=True,
                                  file_size=1)
        self.user_file.save()
                    
    def test_create_and_sign_view(self):
        
        #Test creating netsign documents
        json_string = """{    
                             "username":"adoniscortez",   
                             "netsignstatus":"pending",
                             "pagecount":18,
                             "filename":"Life_and_Times_of_Colin_Powell",
                             "inviteid":"1268103610703",
                             "title":"Life and Times of Colin Powell",
                             "netsignitems":
                             {
                                 "page1":
                                 [
                                     {
                                         "type":"text",
                                         "required":false,
                                         "x":292.1,
                                         "y":97.85,
                                         "value":"Telephone #",
                                         "height":20,
                                         "width":152,
                                         "userid":""
                                     },
                                     {
                                         "type":"text",
                                         "required":false,
                                         "x":69.05,
                                         "y":92.8,
                                         "value":"Date",
                                         "height":20,
                                         "width":152,
                                         "userid":""
                                     }
                                ],
                                 "page2":
                                     [
                                     ]
                                 }
                         }"""
        
        post_dict = {u'create_netsign': u'true', 
                     u'netsign_title': u'ABCD', 
                     u'file_id': u'1', 
                     u'document_json': json_string}
        self.client.post('/netsign/create/%s/'%self.user_file.urlhash,post_dict)
        self.assertEquals(NetSignDocument.objects.count(),1)
        self.assertEquals(NetSignItem.objects.count(),2)
        
        #Create netsign invite
        nsd = NetSignDocument.objects.get()
        nsi = NetSignInvitation.objects.create(document=nsd,
                                               from_user=self.test_user,
                                               to_user=self.test_user,
                                               )
        
        
        #Test signing netsign documents
        signed_json_string = """{    
                             "username":"adoniscortez",   
                             "netsignstatus":"pending",
                             "pagecount":18,
                             "filename":"Life_and_Times_of_Colin_Powell",
                             "inviteid":"1268103610703",
                             "title":"Life and Times of Colin Powell",
                             "netsignitems":
                             {
                                 "page1":
                                 [
                                     {
                                         "type":"text",
                                         "required":false,
                                         "x":292.1,
                                         "y":97.85,
                                         "height":20,
                                         "width":152,
                                         "userid":"",
                                         "value":"131232132"
                                     },
                                     {
                                         "type":"text",
                                         "required":false,
                                         "x":69.05,
                                         "y":92.8,
                                         "height":20,
                                         "width":152,
                                         "userid":"",
                                         "value":"dasasdsd asdasd"
                                     }
                                ],
                                 "page2":
                                     [
                                     ]
                                 }
                         }"""
        
        
        
        
        post_dict['document_json'] = signed_json_string
        #FIXME
        #self.client.post('/netsign/sign/%s/'%nsi.uhash,post_dict)
        #self.assertEquals(NetSignDocument.objects.count(),1)
        #self.assertEquals(NetSignItem.objects.count(),2)
        #self.assertEquals(NetSignMark.objects.count(),2)
        
        #Verify the returned json required for flash
        pprint(nsd.get_items_json_for_flash())
        
        nsitem = nsd.netsign_item.all()[0]
        ##TODO Fix this test
        #pprint(nsitem.get_dict_for_flash(signed=True))
        
        #Verify whether netsign items are returned via new url
        nsd_hash = nsd.get_netsign_document_urlhash()
        items_response = self.client.get('/netsign/_items/%s/'%nsd_hash)
        pprint(items_response.content)
        
        ##TODO Fix this test
        #signed_items_response = self.client.get('/netsign/_items_signed/%s/'%nsi.uhash)
        #pprint(signed_items_response.content)
        
        
    def test_mouse_sign_intact(self):
            
        dd = """{"netsignitems":{"page1":[{"height":45,"type":"mousesign","userid":"","value":"iVBORw0KGgoAAAANSUhEUgAAAYwAAACSCAYAAABWkv0VAAAKLUlEQVR42u3di40iuxIAUGdACB0C\u000aIRACIRACIRACIRACIRACIRDChMCdlbqEx8/tdjMzsPt0joTu3h3oDztyucqfTgkAAODVVp+v8+dr\u000a943P//ns4fN1ecP1/zn3xj8jwO87fb5un6+Pz9d6wec2Y4C4f76uY8C4v/jaD+N1f4yBC4Bf7J1H\u000aY3sYG/7VTDZxyALMoXj/qwLGegxWt+zPe/+cAL/X6H6kr+WcyxgEau89je//857txDE/XnDd2zEw\u000a5cFqNwaPZ483/PD3evLrBfy/WKVHGSm3SV/LO0N6lJqOab5ktbSsNSx8/2o8Ry1g3dLycZi4t59s\u000a4KNMt/uhf6dNUm4D3uiUpgeo//z9eQwo9/HPPY36ekGGsR2Pex8b+kPn546N616aZazH80fG8lNZ\u000axp9j7cfv7zt2471e03ypEOBXbIssYjX+3TELEtHo9TRSQ9aQnzsa6WsWJIZUL43VRPbTatiXZBl5\u000ahnUa7+G7mUAEjNUT2VYcJ8aIbtkxY2LC2q8v8Eq7sQEa0mNc4jo2mNux0bp09vpXY0N2HP/b6glH\u000a+ecw8bNzx3n2HffW07M/Fu/bpufHQIb0mASwSY+B/1NaVuqKQD41RrTvCJgAPyoanqjdrxu9+bkM\u000a45w19NHYlZ8b0qOssm40uvfO88wFsDxbiaCYl5ymMpVbmh7Mb4nAsBvPcynuadVxzec0PTZTBpVL\u000aUp4Cfskh68F/jA1jT83+ktplmv14rFX6On6xzXrWEZx6yj23Rs/6tqCRPGS99Jj6m/f2p7KnY1o+\u000a+D0U3+WxuNa56b5xjecF97fuDJ4ATweMfdbDv6T5MYOhEVjKnvy+aMSOWXDaLLjOy8R5esZHys/k\u000aPfa4l10jc+rNCMprPheZ0DW7j9ZA/LGSVWyy15AFiE36mVlXAE8HkTlTs5LOlYZyX2QVt7RsILk2\u000aUBzjHjFVdclxyrGMKE217vu6sGEus6IodV2zey+D5ioLLBEI8uwvyoXxigB0Sd8bmAd4yjYtGxPY\u000aFg1vObgd77lkDeSQ+uryZaO+G499TI/ZWmlBwLikx2D+urjnuVLckqm55Xvz73TIMrA8OMf6l5hO\u000aHMFwP75iDOhUud8h/ez0X4Auw5MNYy1YxMygGKtYFY3okpk9uyzo3Ivspidg5NdXzlKKxnhuplXv\u000a1Nzyfcfi2PssA7llQSwCQsxI26X6Viv3RkAFeKkl6wRi7UQZLIbsZ1NjFa3FdrUGMcpG5dqMe8dn\u000a8+vLe+TrLFjMBcp9mi/91Kbh3rLAOIzHyUtLeZmsXG9RCwL3iYBq2xHg5ZYsVouxhHJ8IerucxsW\u000a3irZR6oEnmt6zBq6dDSg+bXU1oCcildK89Nn12l+Lcc5fR0LiXGI+O89+/+PLFj07sm1mQiyEfhM\u000aqwVeqnex2pA1cJf0dbV273TXWA3+kf530DnKVsciOG06A8a2kS0NWQ9/yI4/N37TWnlejiXE2E0M\u000aUO/T1xJcjMVEme3UkdltGlnZ0oF5gB/RU5a6ZNlB3ug9s1XFUGm8a1NLa8FhKmDMjTlciwDRM3jc\u000aKl3FKvFTml/XsktfS1K9YzmtgKEsBbzFXFkqGs4hPaZ+5luJPHvOGLw9VxrRw4KAMTcmsU/1Byz1\u000alOPK0tUqfX1gU+zeO9W454sWl84Ya81iWzJhAeDHtMpS0cjF4GysRj6n79XRh9TeUvzSGTDmNi3M\u000af16u7o7tQVpi5fdQZBPXSoA7FPcX91CuVen93ubWydz96gLvMFWWikYvn4oaW4L3bATYyjCujR70\u000afaLHfi96+9dGo1r+vLY31lwpKwas83GHY+W+8xli+ywDK0tQu8a9CRjAPyEvzwzp63TPcovzWNEd\u000agWP9xLliYV6tRLOp9MxrjeQltev4tam8ZZYxtbNtZCQflQzmVgkCtyyruE1kNHnG1hNsz6ldvhIw\u000agLfYpK/bb9wmgkG5aOyQlj0kqPx8NLar4pjniePeiwylNUW3NghdZhnlViS7LDOIBXT5NW8q13XJ\u000agktMDKjdV5nVzA2Az5WuBAzgbSKziPUQZe94P9Ervqb5PalWaXqFcrk/0i0LYEOlkezZuba151I5\u000aJTWePHjNGvNVEdROlUwsDzh5Sar1CNk4xj61FzP2rAMRMIC3qzVkrWmcm9SemrtJ9bUXZTawTl+3\u000aIDlXAtQ9zU8D3s70zsv9n2KNxLHReEc2kJ97k+rrUOZWtcf3sWkE20Oan8ElYABvVdv3qWeRW22V\u000adWwe+JHmF5kdih5+ZDTnouGeGzOZ693nWUycM16tMYVrloXE9eZ7QpXXOHTc77lxTz0PchIwgLep\u000aNbZLdkeNQehNeuwHdUn9C9VuRc8/fwJfBLK5RrL3qXynIquY26E2ZjcdigBTBtfex9rmDX4ZbPMH\u000aUQkYwF+pNuuo9/kLsaAt37F26RbcETDWRaOYrwS/d2RHrbGNfCZTOaW2tZ4jrmWbZSJlOann/OV3\u000au6l8zz1P/WutAgf4VbWB5LlN7rbpsUXGPetdL9n+IlV62dfx86v02JtpPdOr7ilF5ftVrSpZRav0\u000aFqWjXXG9+fl7t0TPz3fKAlkErJ5ylIABvMXUaun4+0PxyqfeHtPjmQ4p6yEvacxqj3rNtwTv2d78\u000a2uiV5yvTt5WsZpM12lPlt32WlZwq39fhiQa8LD3tsu+1J8B7pjfwcq1psbtKwNjNZBBzK7BLp0bj\u000ad+kIGKc0PQU1ZiSdJzKlcjX1eeK6T8V3sW4EvCXyWVeRUfUEgkuyWy3wYkseR/pM73muUYsGfWgE\u000ak10jYEytyeidoVWud5jaV+vWuMZdWrZ4sby/Y5GZzQWfVfKYVuANltbdl5hbn9Hq0U9lAPkzOWLB\u000a3HoiCPTO0KrtL1XOFLv90neYj1lE4JzbfXfzjQAF8FdlF+U5poJGDPquFgSMYWwwN2m6xHRP/eWw\u000aWhZTjkf0LFr8jrjmbRGE9p3fCcA/nV2UDVy5qO/Uee7e2UAxXfaalm+GWD53oiz5tK51ySNun8nO\u000aaspxHYBfNVf2+K2gEY1670OEegJGOV12qVUlC8oDwdT4xW+PJewrmc02KUcBLzT30KHfPO9mYQPb\u000aChit6bLfzYLywfhb4zO/vRYinkgYe0t5LCvwUrVN/f5m94lAkj8B8CfEYPomyyzOE430K4PudgwY\u000aW7+6wCvt/8GGpwwYMV32p4Ne/rzuCB756u78fT1ThgF4U8CI53TE1iG/JQ8c5U6ycQ1KQwB/acCI\u000anWGPLzzvajxvPFSq3OEWgL9M7OH0rqmkUZo6JaurAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\u000aAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\u000aAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\u000aAAAAAAAAAAAAAAAAAAAAAOCN/gOVloMgaHNvhwAAAABJRU5ErkJggg==","item_pk":null,"required":true,"read_only":true,"width":120,"y":173,"x":599}],"page6":[],"page5":[],"page9":[],"page3":[],"page2":[],"page8":[],"page7":[],"page4":[]},"pagecount":9,"title":"big one","username":"lakshman","inviteid":"1271148200808","filename":"peepcode-git-preview","netsignstatus":"pending"}"""
        flash_json = json.loads(dd)
        items_dict = flash_json.get('netsignitems')
        long_sign_value = items_dict['page1'][0]['value']
        
        file_netsign = self.user_file
        netsign_title = 'Testing'
        nsdoc = NetSignDocument.objects.create(user_file=file_netsign,
                                               creator=self.test_user,
                                               title=netsign_title,
                                               )
        nsdoc.save_items(items_dict)

        value_from_db = nsdoc.netsign_item.get().title
        self.assertEquals(value_from_db,long_sign_value)
        pprint('Verified that title is a long text field and mouse sign not truncated.')
        
#class SignedDocumentDownloadTest(TestCase):
    #def setUp(self):
        ##Create an UF that exists in the cdn.
        #self.test_user = User.objects.create_user('lakshman','lakshman+test@uswaretech.com',password='test')
        #self.client.login(username='test',password='test')
        #self.user_file = UserFile(title='dfasdas',
                                  #user=self.test_user,
                                  #filename='Test Name',
                                  #source_type='pdf',
                                  #current_type='pdf',
                                  #processed=1,
                                  #is_active=True,
                                  #file_size=1)
        #self.user_file.save()
            
class CreateSignDoumentTest(TestCase):
    fixtures = ['netsign_data_dump28-4-10.yaml']
    
    def setUp(self):
        self.test_user = User.objects.create_user('test','lennon@thebeatles.com',password='test')
        self.test_user2 = User.objects.create(username='lakshman',email='lennon2@thebeatles.com',id=282,password='test')
        self.client.login(username='test',password='test')
        
        #First create a userfile object
        self.user_file = UserFile(title='dfasdas',
                                  user=self.test_user2,
                                  filename='peepcode-git-preview.pdf',
                                  source_type='pdf',
                                  current_type='pdf',
                                  processed=1,
                                  is_active=True,
                                  file_size=1,
                                  id=1140)
        
        #This is the 1 page a4 pdf userfile, on the cdn
        self.user_file2 = UserFile(title='asdasdasd',
                          user=self.test_user2,
                          filename='p0015.pdf',
                          source_type='pdf',
                          current_type='pdf',
                          processed=1,
                          is_active=True,
                          file_size=1,
                          id=1489)

        self.user_file2.save()

    
    def test_create_only_sign_doc(self):
        uf = UserFile.objects.get(pk=1140L)
        nsd = NetSignDocument.objects.get(pk=171)
        nsi = NetSignInvitation.objects.get(pk=47)
        
        nsi.create_signed_pdf()

        
#Vars for test env

unsi = []

for el in NetSignInvitation.objects.all():
    try:
        unsi.append((el,el.document.user_file))
    except UserFile.DoesNotExist:
        pass


unsi_pdfs = [el for el in unsi if el[0].document.user_file.filename.find('.pdf')!=-1]


        
        
class PDFBasicTest(TestCase):
    def test_merge_pdfs(self,pdf1=None,pdf2=None):
        if pdf1 is None:
            pdf1 = PdfFileReader(open('output5.pdf'))
        if pdf2 is None:
            pdf2 = PdfFileReader(open('strings.pdf'))
        merged_pdf = PdfFileWriter()
        
        for i,page in enumerate(pdf1.pages):
            page.mergePage(pdf2.getPage(i))
        
        for i,page in enumerate(pdf1.pages):
            merged_pdf.addPage(pdf1.getPage(i))
            
        fname = "testing_merged_pdf.pdf"
        pdf_file = open(fname,mode='w')
        merged_pdf.write(pdf_file)
        pdf_file.close()
        
    def test_circumvent_pypdf_bug(self):
        pdf1 = PdfFileReader(open('aaa.pdf'))
        if pdf1.isEncrypted:
            pdf1.decrypt('')
        #ipython()
        self.test_merge_pdfs(pdf1)
        
    def test_proper_merge_z_index(self):
        pdf1 = PdfFileReader(open('sign-only-79'))
        pdf2 = PdfFileReader(open('NDA.pdf'))
        
        
        
class PDFMouseSignTest(TestCase):
    def setUp(self):
        self.test_user = User.objects.create_user('test','lennon@thebeatles.com',password='test')
        self.client.login(username='test',password='test')
        self.test_user2 = User.objects.create(username='lakshman',email='lennon2@thebeatles.com',id=282,password='test')
        self.client.login(username='test',password='test')

        #First create a userfile object
        self.user_file = UserFile(title='zxfdafasdasd',
                                  user=self.test_user2,
                                  filename='EquusiPadNDARevised043010.doc',
                                  source_type='doc',
                                  current_type='pdf',
                                  processed=1,
                                  is_active=True,
                                  file_size=1,
                                  id=1792L)
        
        self.nsd = NetSignDocument(creator=self.test_user2,
                                   title='aqwert',
                                   user_file=self.user_file,
                                   )
        self.nsi = NetSignInvitation(document=self.nsd,
                                     message=' a',
                                     from_user=self.test_user2,
                                     to_user=self.test_user2)
        
        
        
        