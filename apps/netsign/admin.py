from django.contrib import admin
from netsign.models import NetSignItem, NetSignInvitation, NetSignDocument, NetSignMark

class NetSignDocumentAdmin(admin.ModelAdmin):
    list_display = ('title', 'user_file', 'creator', 'saved_at')

class NetSignMarkAdmin(admin.ModelAdmin):
    model = NetSignMark
        
class NetSignMarkInline(admin.TabularInline):
    model = NetSignMark

class NetSignItemInline(admin.TabularInline):
    model = NetSignItem

class NetSignInvitationAdmin(admin.ModelAdmin):
    list_display = ('from_user', 'to_user', 'document', 'status', 'sent')
    inlines = [NetSignMarkInline,]
    
class NetSignItemAdmin(admin.ModelAdmin):
    list_display = ('user_file', 'title')
    inlines = [NetSignMarkInline]

    
    
admin.site.register(NetSignDocument, NetSignDocumentAdmin)
admin.site.register(NetSignItem, NetSignItemAdmin)
admin.site.register(NetSignInvitation, NetSignInvitationAdmin)
admin.site.register(NetSignMark, NetSignMarkAdmin)
