from django.views.generic.simple import direct_to_template
from django.views.generic.list_detail import object_detail, object_list
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.core.urlresolvers import reverse
from django.conf import settings
from django.template import RequestContext
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404, render_to_response, redirect
from files.forms import FileUploadForm, FileForm
from files.models import UserFile
from netsign.models import NetSignInvitation, NetSignDocument, DeadlinePassed, NetSignUserSignature, NetSignSignor
import os, datetime
try:
    import json
except ImportError:
    import simplejson as json            

from django.contrib.auth.decorators import login_required
from mailer import send_html_mail as send_mail
from django.template.loader import render_to_string

from netconf_utils.encode_decode import *
from netconf_utils.amazon_s3 import generate_random_file_name, upload_new_file, generate_url
from django.db.models import Q

from files.search import get_query
from files.views import filter_files, sorted_object_list
from django.utils.translation import ugettext_lazy
from django.contrib.sites.models import Site
from django.views.decorators.http import require_POST

_ = lambda x: unicode(ugettext_lazy(x))

@login_required
def about(request):
    return direct_to_template(request, 'netsign/how_netsign_works.html', extra_context={'page_title': _('My NetSign'), 'thumb_url': settings.THUMBNAIL_URL})

@login_required
def my_netsign(request):
    query_string  = ""
    user_id = request.user.id
    files = NetSignDocument.objects.filter(creator=user_id)
    page_title = _('My NetSign Documents')
    if "delete_all" in request.POST:
        selected_files_pk = request.POST.getlist("file-select[]")
        selected_files = NetSignDocument.objects.filter(pk__in = selected_files_pk)
        selected_files.delete()
        request.user.message_set.create(message=_("The selected files were deleted."))
        return HttpResponseRedirect(".")
    if ('q' in request.GET) and request.GET['q'].strip():
        page_title = _('Search results')
        query_string = request.GET['q']
        entry_query = get_query(query_string, ['title', 'user_file__description', 'user_file__tags_string'])
        user = request.user
        files = files.filter(user=user).filter(entry_query)
    filtered_files, file_type, uploaded = filter_netsign_docs(files, request)
    if 'filter' in request.GET:
        is_filtered = True
    else:
        is_filtered = False
    return sorted_object_list(request, template_name='netsign/my_netsign.html', queryset=filtered_files, extra_context={'page_title': page_title, 'query_string': query_string, 'thumb_url': settings.THUMBNAIL_URL, 'file_type': file_type, 'uploaded': uploaded, 'is_filtered': is_filtered}, paginate_by = settings.ITEMS_LIST_PAGINATE_BY)
    
def filter_netsign_docs(files, request):
    file_type = "all"
    uploaded = "all"
    if 'filter' in request.GET:
        if "type" in request.GET:
            file_type = request.GET["type"]
            if not file_type == "all":
                files = files.filter(user_file__source_type=file_type)
        if "uploaded" in request.GET:
            uploaded = request.GET["uploaded"]
            if uploaded == "today":
                hrs_ago_24 = datetime.datetime.now() - datetime.timedelta(hours = 24)
                files = files.filter(saved_at__gt = hrs_ago_24)
            elif uploaded == "week":
                days_ago_7 = datetime.datetime.now() - datetime.timedelta(hours = 24 * 7)
                files = files.filter(saved_at__gt = days_ago_7)
            elif uploaded == "month":
                days_ago_30 = datetime.datetime.now() - datetime.timedelta(hours = 24 * 30)
                files = files.filter(saved_at__gt = days_ago_30)
    return files, file_type, uploaded

@login_required
def select(request):
    query_string  = ""
    user_id = request.user.id
    page_title = _('Select Document')
    files = UserFile.objects.filter(user=user_id).filter(current_type="pdf")
    if ('q' in request.GET) and request.GET['q'].strip():
        page_title = _('Search results')
        query_string = request.GET['q']
        entry_query = get_query(query_string, ['title', 'description', 'tags_string'])
        user = request.user
        files = files.filter(user=user).filter(entry_query)
    filtered_files, file_type, uploaded = filter_files(files, request)
    if 'filter' in request.GET:
        is_filtered = True
    else:
        is_filtered = False
    return sorted_object_list(request, template_name='netsign/select_netsign.html', queryset=filtered_files, extra_context={'page_title': page_title, 'query_string': query_string, 'thumb_url': settings.THUMBNAIL_URL, 'file_type': file_type, 'uploaded': uploaded, 'is_filtered': is_filtered}, paginate_by = settings.ITEMS_LIST_PAGINATE_BY)

def _get_profile(request, username):
    user_obj = User.objects.get(username=username)
    profile = user_obj.get_profile()
    return direct_to_template(request, 'netsign/_get_profile.html', extra_context={'user_obj': user_obj, 'profile': profile})

@login_required
def create(request, urlhash, *args, **kwargs):
    extra_context = kwargs.get("extra_context", None) or {}
    object_id = int(uri_b64decode(str(urlhash))) / 42

    file_objects = get_object_from_hash(UserFile.active_objects,urlhash)
    
    file_form = FileForm(instance=file_objects)
    user_owner = file_objects.user
    user_requesting = request.user
    try:
        user_theme = UserTheme.objects.get(user=user_owner)
    except:
        user_theme = None
    if request.method == 'POST' and 'create_netsign' in request.POST:
        file_id = int(request.POST['file_id'])
        file_netsign = UserFile.objects.get(id=file_id)
        try:
            flash_json = json.loads(request.POST.get('document_json'))
            items_dict = flash_json.get('netsignitems')
        except:
            request.user.message_set.create(message=_("Your document doesn't contain any fields"))
            return redirect(create,urlhash)
        
        netsign_title = flash_json.get('title')
        nsdoc = NetSignDocument.objects.create(user_file=file_netsign,
                                               creator=user_requesting,
                                               title=netsign_title,
                                               )
        try:
            nsdoc.save_signors(flash_json.get('signors'))
        except:
            request.user.message_set.create(message=_("Could not fetch the signing party details."))
        try:
            nsdoc.save_items(items_dict)
            request.user.message_set.create(message=_("Success! Your NetSign Document was created."))
            return redirect(my_netsign)
        except:
            request.user.message_set.create(message=_("You can't have a netsign document without any fields!"))
        
        
    if request.method == 'POST' and 'delete' in request.POST:
        file_delete = UserFile.objects.get(id=object_id)
        if request.user == file_delete.user:
            file_delete.delete()
            request.user.message_set.create(message=_("Your document was marked as SIGNED."))
        else:
            request.user.message_set.create(message=_("You cannot delete other people's files."))
        return HttpResponseRedirect(reverse('my_files'))
    if file_objects.current_type == "pdf":
        template_name = "netsign/netsign_edit.html"
    else:
        raise Http404
    ctx = {
        'userfile': file_objects,
        'object_id': object_id,
        'user': user_requesting,
        'file_form': file_form,
        'flash_mode': 2,
        'sig_url': reverse('netsign_user_sign'),
        'bucket': settings.CID,
    }
    ctx.update({"ENCODER_URL": settings.NETSIGN_ENCODER_URL})
    ctx.update(extra_context)
    return render_to_response(template_name, ctx, context_instance=RequestContext(request))

@login_required
def review_sign(request,urlhash):
    nsi = get_object_from_hash(NetSignInvitation,urlhash)
    
    try:
        assert nsi.status == "9"
        if request.user not in [nsi.to_user,nsi.from_user] and request.user.email !=nsi.to_email:
            raise
    except:
        request.user.message_set.create(message=_("You cannot view other's signature, unless you are invited"))
        return redirect(my_netsign)
    return render_to_response('netsign/netsign_view.html',
                              {'flash_mode':0,
                               'userfile':nsi.document.user_file,
                               'ndoc':nsi.document,
                               'nsi':nsi,
                               'user':request.user,
                               'netsign_items':nsi.get_signed_values_for_flash(),
                               'items_url':'/netsign/_items_signed/%s/'%nsi.uhash
                               },
                              RequestContext(request)
                              )

@login_required
def preview(request,urlhash):
    ndoc = get_object_from_hash(NetSignDocument, urlhash)

    return render_to_response('netsign/netsign_preview.html',
            {'flash_mode':0,
             'userfile':ndoc.user_file,
             'ndoc':ndoc,
             'user':request.user,
             'netsign_items':ndoc.get_items_json_for_flash(),
             'items_url':'/netsign/_items_signed_all/%s/'%ndoc.hash
             },
            RequestContext(request)
            )

def download_signed_pdf(request,urlhash):
    nsi = get_object_from_hash(NetSignInvitation,urlhash)
    signed_file_name = nsi.create_signed_pdf()
    only_file_name = signed_file_name.split('/')[-1]
    return redirect('/site_media/netsign/%s'%only_file_name)
    

def netsign_items(request, urlhash):
    nsd = get_object_from_hash(NetSignDocument,urlhash)
    netsign_items = nsd.get_items_json_for_flash()
    return HttpResponse(netsign_items)

def netsign_items_signed(request, urlhash):
    nsi = get_object_from_hash(NetSignInvitation,urlhash)
    netsign_items = nsi.get_signed_values_for_flash()
    return HttpResponse(netsign_items)


def netsign_items_signed_all(request, urlhash):
    nsi = get_object_from_hash(NetSignDocument, urlhash)
    netsign_items = nsi.get_signed_values_for_flash()
    return HttpResponse(netsign_items)

@login_required
def review(request, urlhash):
    # file = UserFile.active_objects.filter(pk=object_id)
    
    nsd = get_object_from_hash(NetSignDocument,urlhash)
    object_id = nsd.pk
    file = nsd
    user_file_id = file.user_file.id
    file_objects = get_object_or_404(UserFile.active_objects, pk=user_file_id)    
    
    file_form = FileForm(instance=file_objects)
    user_owner = file_objects.user
    # user_signer = file_objects.netsign_signer
    user_requesting = request.user
    try:
        user_theme = UserTheme.objects.get(user=user_owner)
    except:
        user_theme = None
    if request.method == 'POST' and 'create_netsign' in request.POST:
        
        #Exactly the same stuff as for create
        return create(request,urlhash)
        
    if file_objects.current_type == "pdf":
        template_name = "netsign/netsign_view.html"
    else:
        raise Http404
    
    netsign_items = nsd.get_items_json_for_flash()
    return render_to_response(template_name, {
        'netsign_doc': file,
        'ndoc':file,
        'file': file_objects,
        'userfile':file_objects,
        'object_id': object_id,
        'user': user_requesting,
        'file_form': file_form,    
        'urlhash': urlhash,
        'flash_mode':0,
        'netsign_items':netsign_items,
        'items_url':'/netsign/_items/%s/'%nsd.hash,
        'show_send': request.user == file.creator
    }, context_instance=RequestContext(request))
    
def sign(request, urlhash):
    nsi = get_object_from_hash(NetSignInvitation,urlhash)
    signor_uuid = request.GET.get("uuid", None)
    object_id = nsi.pk
    if nsi.status=="9":
        #Replace this with pinax message
        if request.user.is_authenticated():
            request.user.message_set.create(message=_('The document is already signed. You are viewing the signed document.'))
        return redirect(review_sign,urlhash)
    
    user_file_id = nsi.document.id
    file_objects = get_object_or_404(NetSignDocument, pk=user_file_id)    
    nsd = file_objects    
    
    file_form = FileForm(instance=file_objects)
    user_owner = file_objects.user_file.user
    user_requesting = request.user
    try:
        user_theme = UserTheme.objects.get(user=user_owner)
    except:
        user_theme = None
    if request.method == 'POST' and 'create_netsign' in request.POST:
        netsign_title = request.POST.get('netsign_title','')
        nsi.save_sign(request.POST.get('document_json'))
        nsi.status = 9
        success = False
        try:
            nsi.save()
            success = True
        except DeadlinePassed:
            request.user.message_set.create(message=_("Sorry! The deadline to sign this document has passed."))
        if success:
            email_body = render_to_string('netsign/email_content.txt',{'nsi':nsi, "site": Site.objects.get_current(),},RequestContext(request))
            notify_via_email = nsi.from_user.email
            if  notify_via_email:
                send_mail(_('%(title)s netsign document signed by %(to_email)s'%({"title": nsd.title, "to_email": nsi.to_email})),
                          "",
                          email_body,
                          nsi.to_email,
                          [notify_via_email,]
                          )
            if request.user.is_authenticated():
                request.user.message_set.create(message=_("Success! You have digitally signed the document!"))
                return HttpResponseRedirect(reverse('my_netsign'))
            return redirect('netsign_sign_done')

    if file_objects.user_file.current_type == "pdf":
        template_name = "netsign/netsign_sign.html"
    else:
        raise Http404
    
    netsign_items = nsd.get_items_json_for_flash()
    
    return render_to_response(template_name, {
        'netsign_doc' : file_objects,
        'ndoc':file_objects,
        'userfile':file_objects.user_file,
        'file': file_objects,
        'invitation': nsi,
        'object_id': object_id,
        'user': user_requesting,
        'file_form': file_form,    
        'urlhash': urlhash,
        'flash_mode':1,
        'netsign_items':netsign_items,
        'items_url':'/netsign/_items/%s/'%nsd.hash,
        'signor_uuid': signor_uuid,
        'uuid_url': reverse("netsign_uuid_url"),
        'sig_url': reverse("netsign_user_sign"),
    }, context_instance=RequestContext(request))

@login_required
def new_sign(request,urlhash):
    ns_invitation = get_object_from_hash(NetSignInvitation,urlhash)

@login_required
def received_invites(request):
    query_string = ''
    page_title = _('NetSign Agreements Received')
    netsign_invites_received = NetSignInvitation.objects.filter(Q(to_user=request.user)|Q(to_email=request.user.email))
    if ('q' in request.GET) and request.GET['q'].strip():
        page_title = _('Search results')
        query_string = request.GET['q']
        entry_query = get_query(query_string, ['document__title', 'from_user__username', 'message'])
        netsign_invites_received = netsign_invites_received.filter(entry_query)
    filtered_invites_received, file_type, uploaded = filter_netsign_invites(netsign_invites_received, request)
    is_filtered = False
    if "filter" in request.GET:
        is_filtered = True
    return sorted_object_list(request,
                              queryset=filtered_invites_received,
                              template_name='netsign/netsign_received.html',
                              template_object_name='invite',
                              paginate_by=settings.ITEMS_LIST_PAGINATE_BY,
                              extra_context={
            'page_title': page_title,
            'query_string': query_string,
            'file_type': file_type,
            'uploaded': uploaded,
            'is_filtered': is_filtered,
            })

@login_required
def sent_invites(request,qs=None,extra_context=None,template_name=None):
    query_string = ''
    netsign_invites_sent = request.user.netsign_from.all()
    page_title = 'NetSign Agreements Sent'
    if ('q' in request.GET) and request.GET['q'].strip():
        page_title = 'Search results'
        query_string = request.GET['q']
        entry_query = get_query(query_string, ['document__title', 'to_user__username', 'message'])
        netsign_invites_sent = netsign_invites_sent.filter(entry_query)
    filtered_invites_sent, file_type, uploaded = filter_netsign_invites(netsign_invites_sent, request)
    is_filtered = False
    if "filter" in request.GET:
        is_filtered = True
    return sorted_object_list(request,
                       queryset=qs or filtered_invites_sent,
                       template_name=template_name or 'netsign/netsign_status.html',
                       template_object_name='invite',
                       paginate_by=settings.ITEMS_LIST_PAGINATE_BY,
                       extra_context=extra_context or {
                                                       'page_title': page_title,
                                                       'query_string': query_string,
                                                       'file_type': file_type,
                                                       'uploaded': uploaded,
                                                       'is_filtered': is_filtered,
                                                      })


def filter_netsign_invites(files, request):
    file_type = "all"
    uploaded = "all"
    if 'filter' in request.GET:
        if "type" in request.GET:
            file_type = request.GET["type"]
            if not file_type == "all":
                files = files.filter(document__user_file__source_type=file_type)
        if "uploaded" in request.GET:
            uploaded = request.GET["uploaded"]
            import datetime
            if uploaded == "today":
                hrs_ago_24 = datetime.datetime.now() - datetime.timedelta(hours = 24)
                files = files.filter(document__user_file__timestamp__gt = hrs_ago_24)
            elif uploaded == "week":
                days_ago_7 = datetime.datetime.now() - datetime.timedelta(hours = 24 * 7)
                files = files.filter(document__user_file__timestamp__gt = days_ago_7)
            elif uploaded == "month":
                days_ago_30 = datetime.datetime.now() - datetime.timedelta(hours = 24 * 30)
                files = files.filter(document__user_file__timestamp__gt = days_ago_30)
    return files, file_type, uploaded

@login_required
def search(request):
    netsign_invites = NetSignInvitation.objects.filter(Q(to_user=request.user)|\
                                                       Q(to_email=request.user.email)|\
                                                       Q(from_user=request.user))
    
    query = request.GET.get("q", "")
    criteria = Q(message__icontains =  query)
    netsign_invites  = netsign_invites.filter(criteria)
    return object_list(request,
                       queryset= netsign_invites,
                       template_name='netsign/netsign_search.html',
                       template_object_name='invite',
                       paginate_by=settings.ITEMS_LIST_PAGINATE_BY,
                       extra_context={})
    
    
    
    

@login_required
def delete(request,urlhash):
    """
    Deletes the netsign object.
    Now, it deletes in a get request. Its ok, because the url has hash; and we verify the creator.
    Perhaps better changed to post, with the confirmation via modal
    """
    netsign_object = get_object_from_hash(NetSignDocument,urlhash)
    if request.user != netsign_object.creator:
        raise Http404
    netsign_object.delete()
    request.user.message_set.create(message=_("The netsign document has been successfully deleted."))
    return redirect(reverse("my_netsign"))


@login_required
def status(request, urlhash=None):
    nsd_id = int(uri_b64decode(str(urlhash))) / 42
    try:
        nsd = NetSignDocument.objects.get(id=nsd_id)
    except NetSignDocument.DoesNotExist:
        raise Http404
    invitations = NetSignInvitation.objects.filter(document=nsd)
    if request.method == "POST":
        signed_pdfs = [] # A list of original + signs-only
        for invitation in invitations.filter(status="9"):
            pdf_file = invitation.get_or_create_sign_pdf()
            if pdf_file:
                signed_pdfs.append(pdf_file)
        final_pdf = nsd.generate_summary_pdf(signed_pdfs)
        if final_pdf:
            file_name = os.path.basename(final_pdf)
            return redirect("/site_media/netsign/%s" %file_name)
        raise Http404
    return render_to_response("netsign/status.html", {"document": nsd,
                                                      "invitations": invitations},
                              context_instance=RequestContext(request))


@login_required
def user_sign(request):
    if request.method == "GET":
        try:
            netsign_user_sign = NetSignUserSignature.objects.get(user=request.user)
        except NetSignUserSignature.DoesNotExist:
            netsign_user_sign = NetSignUserSignature()
    if request.method == "POST":
        user_sign_map = json.loads(request.raw_post_data)
        try:
            netsign_user_sign = NetSignUserSignature.objects.get(user=request.user)
            netsign_user_sign.scanned_image = user_sign_map["scanimage"]
            netsign_user_sign.mouse_sign = user_sign_map["mousesign"]
            netsign_user_sign.text_signature = user_sign_map["signature"]
            netsign_user_sign.save()
        except NetSignUserSignature.DoesNotExist:
            netsign_user_sign = NetSignUserSignature.objects.create(user=request.user,
                                                                    scanned_image=user_sign_map["scanimage"],
                                                                    mouse_sign=user_sign_map["mousesign"],
                                                                    text_signature=user_sign_map["signature"])
    return HttpResponse(json.dumps({"name": request.user.get_full_name() or request.user.username,
                                    "email": request.user.email,
                                    "scanimage": netsign_user_sign.scanned_image,
                                    "mousesign": netsign_user_sign.mouse_sign,
                                    "signature": netsign_user_sign.text_signature,}))
    

def uuid_url(request):
    uuid = request.GET.get("uuid")
    try:
        signor = NetSignSignor.objects.get(uuid=uuid)
    except NetSignSignor.DoesNotExist:
        signor = None
    uuid_map = {}
    if signor:
        uuid_map = {"sname": signor.name,
                    "semail": signor.email,
                    "uuid": signor.uuid}
    return HttpResponse(json.dumps(uuid_map))
    

@require_POST
def scan_sign_upload(request):
    if not os.path.exists(os.path.join(settings.MEDIA_ROOT, "scan_uploads")):
        os.mkdir(os.path.join(settings.MEDIA_ROOT, "scan_uploads"))
    file_upload = request.FILES.get('Filedata', None)
    if not file_upload:
        return HttpResponse("%s?error=%s" %(reverse("netsign_scan_sign_upload"), "Invalid post request"))
    file_name = file_upload.name
    dest_file = open(os.path.join(settings.MEDIA_ROOT, "%s/%s" %("scan_uploads", file_name)), "wb")
    for chunk in file_upload.chunks():
        dest_file.write(chunk)
    dest_file.close()
    dest_file = open(os.path.join(settings.MEDIA_ROOT, "%s/%s" %("scan_uploads", file_name)), "r")
    file_name = generate_random_file_name(prefix="netsign/scan_uploads")
    scan_upload_url = upload_new_file(file_name=file_name, fp=dest_file)
    dest_file.close()
    return HttpResponse("%s?path=%s" %(reverse("netsign_scan_sign_upload"), generate_url(key=scan_upload_url)))
