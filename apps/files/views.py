from django.views.generic.simple import direct_to_template
from django.views.generic.list_detail import object_detail, object_list
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.core.urlresolvers import reverse
# from django.core.paginator import Paginator, InvalidPage, EmptyPage
import dateutil.parser
from django import forms
from django.conf import settings
from django.template import RequestContext
from django.core.cache import cache
from django.contrib.auth.models import User, check_password, get_hexdigest
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404, render_to_response
from django.shortcuts import get_object_or_404, render_to_response, get_list_or_404, redirect
from uuid import uuid4
from tagging.models import Tag
import logging
from files.search import *
from files.forms import FileForm
from files.models import UserFile, FileFolder
from subscription.models import UserSubscription
from user_theme.models import UserTheme
import mimetypes
import urllib
import os, datetime
from netconf_utils.encode_decode import *
from netconf_utils.encode_decode import get_object_from_hash, uri_b64decode
from netconf_utils.amazon_s3 import generate_url, get_key, delete_key, delete_folder_key
from django.utils.translation import ugettext_lazy

from restrictions.decorators import check_file_upload_limit,check_file_download_limit,check_file_view_limit
from conferencing.models import Conference, RecordedConference
from django.contrib.sites.models import Site
from vmail.models import Vmail
import random
from files.forms import FileUploadFormStep1, FileUploadFormStep2, FileUploadFormWizard
from schedule.models import Event
from project.models import Project
import simplejson
from mx.DateTime.mxDateTime.mxDateTime_Python import DateTime
from django.template.defaultfilters import filesizeformat
from httplib import HTTPResponse
_ = lambda x: unicode(ugettext_lazy(x))
import urllib2
from django.db.models import Q

def sorted_object_list(request, template_name, queryset, extra_context, **kwargs):
    sort_by = request.GET.get('sort_by')     
    has_visible_name = False
    if sort_by:
        if sort_by in [el.name for el in queryset.model._meta.fields]:
            queryset = queryset.order_by(sort_by)
        else:
            has_visible_name = True
            if sort_by in request.session:
                sort_by = request.session[sort_by]
                try:
                    queryset = queryset.order_by(sort_by)
                except:
                    raise
    
    if True:
        if 'sort_by' in request.GET:
            getvars = request.GET.copy()
            if has_visible_name:
                extra_context['current_sort_field']= request.session.get(getvars['sort_by']) or getvars['sort_by']
            else:
                extra_context['current_sort_field']= getvars['sort_by']
    return object_list(request, template_name=template_name, queryset=queryset, 
                       extra_context=extra_context, **kwargs)
    
def update_total_storage(request):
    total_file_size = 0
    used_files = UserFile.active_objects.filter(user = request.user)
    for file in used_files:
        total_file_size += file.file_size
    request.session['total_file_size'] = total_file_size

@login_required
def my_files(request):
  if request.method == 'GET':
      query_string = ''
      page_title = _(u'My Files')
      files = UserFile.active_objects.filter(user = request.user).order_by('-last_modified')
      tags = Tag.objects.usage_for_queryset(files)
      total_tags = len(tags)
      if ('q' in request.GET) and request.GET['q'].strip():
          page_title = _(u'Search results')
          query_string = request.GET['q']
          entry_query = get_query(query_string, ['title', 'description', 'tags_string'])
          files = files.filter(user=request.user).filter(entry_query)
      files, file_type, time, from_time, to_time, per_page, file_folder, select_tag, option_tag, child_folders  = filter_files_list(files, request)
      
      total_file_size = 0
      limit_file_size = 0
      
      used_files = UserFile.active_objects.filter(user = request.user)
      
#      if 'filter' in request.GET or 'q' in request.GET:
#          is_filtered = True
          
      if 'filter' in request.GET:
          is_filtered = True
      else:
          is_filtered = False
      if 'total_file_size' in request.session:
          total_file_size = float(request.session['total_file_size'])
      else:
          for file in used_files:
             total_file_size = total_file_size + file.file_size
          request.session['total_file_size'] = total_file_size
      
      limit_file_size = request.user.get_subscription().file_size_mb_limit
      
      file_folders = request.user.get_root_folder()
        
      root_folder = file_folders[0]
      if not file_folder:
          file_folder = root_folder
      sort_by = False
      if 'sort_by' in request.GET:
          sort_by = True 
      
      helper_file = False
      if 'helper_file' in request.session:
          helper_file = request.session['helper_file']
      
      return sorted_object_list(request, template_name='files/newer_file_list.html', 
                               queryset=files, 
             extra_context={
                 'page_title': page_title, 
                 'user_search': 'user_obj', 
                 'user_theme': 'user_theme', 
                 'thumb_url': 'settings.THUMBNAIL_URL',
                 'is_filtered': is_filtered,
                 'type': file_type,
                 'time': time,
                 'from_time':from_time,
                 'to_time':to_time,
                 'query_string': query_string,
                 'is_me': 'is_me',
                 'total_file_size': filesizeformat(total_file_size),
                 'limit_file_size': filesizeformat(limit_file_size*1024*1024),
                 'tags':tags,
                 'per_page': per_page,
                 'option_tag':option_tag,
                 'select_tag':select_tag,
                 'total_tags':total_tags,
                 'bucket':unicode(Site.objects.get_current()),
                 'root_folder':root_folder,
                 'current_folder':file_folder,
                 'sort_by':sort_by,
                 'child_folders':child_folders,
                 'user':request.user,
                 'helper_file':helper_file
                 },
             paginate_by = per_page)

def delete_files(request):
    if request.method == 'POST':
        file_id = request.POST['chk_files']
        if file_id: 
            file =  get_object_or_404(UserFile, pk = file_id)  
            if file:
                parent = file.folder
                file.is_deleted = True
                file.is_mark = True
                file.save()
                if parent:
                    parent.auto_update_info()
        request.user.message_set.create(message=_(u"File(s) deleted successful."))
        return HttpResponseRedirect(reverse('my_files'))

def order_folder_follow_file(child_folders, sort_by):
    if sort_by.find('filename') == 0:
        child_folders = child_folders.order_by('name')
    elif sort_by.find('filename') == 1:
        child_folders = child_folders.order_by('-name')
    elif sort_by.find('page_count') == 0:
        child_folders = child_folders.order_by('total_files') 
    elif sort_by.find('page_count') == 1:
        child_folders = child_folders.order_by('-total_files')
    elif sort_by.find('file_size') == 0:
        child_folders = child_folders.order_by('total_size') 
    elif sort_by.find('file_size') == 1:
        child_folders = child_folders.order_by('-total_size')
    elif sort_by.find('last_modified') == 0:
        child_folders = child_folders.order_by('last_modified') 
    elif sort_by.find('last_modified') == 1:
        child_folders = child_folders.order_by('-last_modified')
    elif sort_by.find('timestamp') == 0:
        child_folders = child_folders.order_by('date_created') 
    elif sort_by.find('timestamp') == 1:
        child_folders = child_folders.order_by('-date_created')
        
    return child_folders

def filter_files_list(files, request):
    file_type = str("all")
    time = str("all")
    select_tag = None
    option_tag = None
    child_folders = None
    try:
        per_page = int(request.GET["per_page"])
    except:
        per_page = settings.ITEMS_LIST_PAGINATE_BY
    from_time = datetime.datetime.now()
    to_time = from_time + datetime.timedelta(days=7)
    file_folder = None
    if "folder" in request.GET:
        folder_id = int(request.GET['folder'])
        file_folder = FileFolder.objects.get(pk=folder_id)
        if file_folder.is_root:
            files = files.filter(Q(folder=file_folder) | Q(folder=None)).exclude(is_deleted = True)
        elif file_folder.is_trash:
            files = files.filter(is_deleted = True, is_mark= True).exclude(folder__is_deleted = True)
        else:
            files = files.filter(folder=file_folder, is_deleted = False)
            
        if file_folder.is_trash:
            child_folders = FileFolder.objects.filter(user = request.user, is_deleted = True, is_mark = True)
        else:
            child_folders = FileFolder.objects.filter(parent_folder = file_folder, is_deleted = False)
    else:
        root_folder = request.user.get_root_folder()[0]
        if 'q' in request.GET or 'filter' in request.GET:
            pass
        else:
            files = files.filter(Q(folder=root_folder) | Q(folder=None), is_deleted = False)
            
            child_folders = FileFolder.objects.filter(parent_folder = root_folder, is_deleted = False)
    if 'sort_by' in request.GET:
        child_folders = order_folder_follow_file(child_folders, request.GET['sort_by'])
        
    
    if 'filter' in request.GET:
        if "option_tag" in request.GET and "select_tag" in request.GET:
            select_tag = request.GET['select_tag']
            option_tag = request.GET['option_tag']
            tags =','.join([select_tag,option_tag]) 
            userfiles = UserFile.active_objects.filter(user = request.user)
            files = TaggedItem.objects.get_union_by_model(userfiles, tags)
        elif "option_tag" in request.GET and request.GET['option_tag']:
            option_tag = request.GET['option_tag']
            userfiles = UserFile.active_objects.filter(user = request.user)
            files = TaggedItem.objects.get_union_by_model(userfiles, option_tag+',')
        elif "select_tag" in request.GET and request.GET['select_tag']:
            select_tag = request.GET['select_tag']
            userfiles = UserFile.active_objects.filter(user = request.user)
            files = TaggedItem.objects.get_union_by_model(userfiles, select_tag+',')
                
        if "type" in request.GET:
            file_type = request.GET.getlist("type")
            total_type = []
            
            if "document" in file_type:
                for x in ['doc', 'docx', 'html', 'ods', 'odt', 'pdf', 'rtf', 'sxc', 'sxw', 'txt', 'wpd', 'xls']: total_type.append(x)
            if "image" in file_type:
                for x in ['gif', 'jpg', 'png', 'sci', 'tif']: total_type.append(x)
            if "presentation" in file_type:
                for x in ['odp', 'ppt', 'pptx']: total_type.append(x)
            if "video" in file_type: 
                for x in ['aac', 'avi', 'f4v', 'flv', 'm4a', 'm4v', 'mov', 'mp3', 'mp4', 'mpg', 'swf', 'wav', 'wmv']: total_type.append(x)
                
            if total_type:
                files = files.filter(source_type__in = total_type)
            if "flagged" in file_type and "unflagged" in file_type:                   
                pass
            elif "flagged" in file_type:
                files = files.filter(is_flag = True)
            elif "unflagged" in file_type:
                files = files.filter(is_flag = False)
              #files = files.filter(source_type__iexact = file_type)
        if "time" in request.GET:
            time = request.GET["time"]
            if time == "last_24h":
               hrs_ago_24 = datetime.datetime.now() - datetime.timedelta(hours=24)
               files = files.filter(timestamp__gt=hrs_ago_24)
            elif time == "last_week":
               days_ago_7 = datetime.datetime.now() - datetime.timedelta(hours=24 * 7)
               files = files.filter(timestamp__gt=days_ago_7)
            elif time == "last_month":
               days_ago_30 = datetime.datetime.now() - datetime.timedelta(hours=24 * 30)
               files = files.filter(timestamp__gt=days_ago_30)
            elif time == "last_2month":
               days_ago_60 = datetime.datetime.now() - datetime.timedelta(hours=24 * 60)
               files = files.filter(timestamp__gt=days_ago_60)
            elif time == "last_year":
               days_ago_365 = datetime.datetime.now() - datetime.timedelta(hours=24 * 365)
               files = files.filter(timestamp__gt=days_ago_365)
            elif time == "custom":
               from_time = dateutil.parser.parse(request.GET["from_time"])
               to_time = dateutil.parser.parse(request.GET["to_time"])
               files = files.filter(timestamp__gt=from_time, timestamp__lt=to_time)
        
    return files, file_type, time, from_time, to_time, per_page, file_folder, select_tag, option_tag, child_folders

def filter_files(files, request):
  file_type = "all"
  uploaded = "all"
  if 'filter' in request.GET:
      if "type" in request.GET:
          file_type = request.GET["type"]
          if not file_type == "all":
              files = files.filter(source_type__iexact = file_type)
      if "uploaded" in request.GET:
          uploaded = request.GET["uploaded"]
          if uploaded == "today":
              hrs_ago_24 = datetime.datetime.now() - datetime.timedelta(hours = 24)
              files = files.filter(timestamp__gt = hrs_ago_24)
          elif uploaded == "week":
              days_ago_7 = datetime.datetime.now() - datetime.timedelta(hours = 24 * 7)
              files = files.filter(timestamp__gt = days_ago_7)
          elif uploaded == "month":
              days_ago_30 = datetime.datetime.now() - datetime.timedelta(hours = 24 * 30)
              files = files.filter(timestamp__gt = days_ago_30)
  return files, file_type, uploaded

def my_files_search(request):
    query_string = ''
    matches = None
    files = UserFile.active_objects.all()
    if ('q' in request.GET) and request.GET['q'].strip():
        query_string = request.GET['q']
        entry_query = get_query(query_string, ['title', 'description', 'tags_string'])
        user = request.user
        files = files.filter(user=user).filter(entry_query)
    return object_list(request, template_name='files/files_search.html', queryset=files, extra_context={'query_string': query_string, 'page_title': _(u'My Files Search'), 'thumb_url': settings.THUMBNAIL_URL}, paginate_by = settings.ITEMS_LIST_PAGINATE_BY)
my_files_search = login_required(my_files_search)
    
def user_files(request, username):
    query_string = ''
    user_obj = get_object_or_404(User, username=username)
    files = UserFile.active_objects.filter(user=user_obj).filter(visibility="U")
    page_title = _(u'%s: Public Files' %(user_obj.username))
    if ('q' in request.GET) and request.GET['q'].strip():
        page_title = _(u'Search results')
        query_string = request.GET['q']
        entry_query = get_query(query_string, ['title', 'description', 'tags_string'])
        files = files.filter(user=user_obj).filter(entry_query)
    files, file_type, uploaded = filter_files(files, request)
    if 'filter' in request.GET:
        is_filtered = True
    else:
        is_filtered = False
    try:
        user_theme = UserTheme.objects.get(user=user_obj)
    except:
        user_theme = None
    is_me = False
    if request.user.is_authenticated() and request.user == user_obj:
        is_me = True
    return sorted_object_list(request, template_name='files/files_user.html', queryset=files, 
            extra_context={
                'page_title': page_title, 
                'user_search': user_obj, 
                'user_theme': user_theme, 
                'thumb_url': settings.THUMBNAIL_URL,
                'is_filtered': is_filtered,
                'file_type': file_type,
                'uploaded': uploaded,
                'query_string': query_string,
                'is_me': is_me,
                }, 
            paginate_by = settings.ITEMS_LIST_PAGINATE_BY)
    
def user_files_search(request, username):
    user_obj = get_object_or_404(User, username=username)
    query_string = ''
    matches = None
    if ('q' in request.GET) and request.GET['q'].strip():
        query_string = request.GET['q']
        entry_query = get_query(query_string, ['title', 'description', 'tags_string'])
        files = UserFile.active_objects.filter(user=user_obj).filter(visibility="U").filter(entry_query)
    return object_list(request, template_name='files/files_user.html', queryset=files, extra_context={'query_string': query_string, 'page_title': _(u'User Files Search'), 'user_search': user_obj, 'thumb_url': settings.THUMBNAIL_URL}, paginate_by = settings.ITEMS_LIST_PAGINATE_BY)

@login_required
@check_file_upload_limit
def upload_file(request, *args, **kwargs):
    site = Site.objects.get_current()
    wizard = FileUploadFormWizard([FileUploadFormStep1, FileUploadFormStep2])
    wizard.initial[0] = {"uid" : request.user.id, "visibility": "U",
                         "username": request.user.username,
                         "redirect_url": "http://%s%s" %(site.domain, reverse("my_files"))}
    cid = getattr(settings, "CID", "netconference")
    project = None
    if request.GET.get("project"):
        try:
            project = Project.objects.get(shortname=request.GET["project"])
        except Project.DoesNotExist:
            project = None
        if project:
            wizard.initial[0].update({"project" : project.id})
    context = {"cid": cid,
               "FILE_UPLOADER_URL": settings.FILE_UPLOADER_URL,
               "FILE_UPLOADER_FLASH_URL": settings.FILE_UPLOADER_FLASH_URL,
               }
    if project:
        context.update({"show_project": project})
        wizard.initial[0]["redirect_url"] = "http://%s%s" %(site.domain, 
                                                            reverse("project_files", 
                                                                    kwargs={"project_name": project.shortname}))
    kwargs.update({"extra_context" : context})
    return wizard(request, *args, **kwargs)

def file_upload(request):
    if request.method == 'GET':
        user = request.user
        user_id = request.user.id
        bucket = settings.AWS_STORAGE_BUCKET_NAME
        limit_file_size = 0
        total_file_size = 0
        SUBSCRIPTION_APPLY = settings.SUBSCRIPTION_APPLY
        
        # Get variables in session or calculate
        if 'total_file_size' in request.session:
            total_file_size = float(request.session['total_file_size'])
        else:
            used_files = UserFile.active_objects.filter(user__id = user_id)
            for file in used_files:
                total_file_size = total_file_size + file.file_size
            request.session['total_file_size'] = total_file_size
         
        
        limit_file_size = request.user.get_subscription().file_size_mb_limit
         
        return direct_to_template(request, 'files/files_upload_.html', {'bucket':bucket, 'user':user,'limit_file_size':limit_file_size, 'total_file_size':total_file_size, 'SUBSCRIPTION_APPLY':SUBSCRIPTION_APPLY})
    else:
         uuid = request.POST['uuid']
         description = request.POST['desc']
         privacy = request.POST['privacy']
         pwd = request.POST['pwd']
         tags = request.POST['tags_string']
         file_name = request.POST['file_name']
         file_title = request.POST['file_title']
         file_size = request.POST['file_size']
         file_object = get_object_or_404(UserFile.active_objects, uuid=uuid)
         file_id = file_object.id
         file_object.description = description
         timestamp = datetime.datetime.today()
         if privacy == 'public':
              visibility = 'U'
         elif privacy == 'private':
             visibility = 'R'
         else:
             visibility = 'O'
         file_object.visibility = visibility
         algo = "sha1"
         salt = get_hexdigest(algo, str(random.random()), str(random.random()))[:5]
         hashed_password = get_hexdigest(algo, salt, pwd)
         file_object.password = "%s$%s$%s" %(algo, salt, hashed_password)
         file_object.tags_string = tags
         file_object.timestamp = timestamp
         file_object.filename = file_name
         file_object.title = file_title
         file_object.file_size = file_size
         file_object.save()
         Tag.objects.update_tags(file_object, file_object.tags_string)
         total_file_size = 0
         user_id = request.user.id
         used_files = UserFile.active_objects.filter(user__id = user_id)
         for file in used_files:
            total_file_size = total_file_size + file.file_size
         request.session['total_file_size'] = total_file_size
         next = reverse('file_detail', args=[uri_b64encode(str(int(file_id) * 42))])
         
         return HttpResponseRedirect(next)

#===============================================================================
# def public_file_detail(request, urlhash):
#  object_id = int(uri_b64decode(str(urlhash))) / 42
#  file_objects = get_object_or_404(UserFile.active_objects, pk=object_id)    
# 
#  file_form = FileForm(instance=file_objects)
#  file_form.initial.pop("password", None)
#  user_owner = file_objects.user
#  user_requesting = request.user
#  if request.method == 'POST' and 'delete' in request.POST:
#      file_delete = UserFile.objects.get(id=object_id)
#      if request.user == file_delete.user:
#          file_delete.delete()
#          request.user.message_set.create(message=_(u"Your file was deleted!"))
#          return HttpResponseRedirect(reverse('my_files'))
#      else:
#          request.user.message_set.create(message=_(u"You cannot delete other people's files."))
#          return HttpResponseRedirect(reverse('my_files'))
#  # THIS WAS CAUSING STUFF TO BREAK - PLEASE FIX
#  if user_requesting != user_owner:
#      file_objects.increment_count()
#  if file_objects.current_type in ["pdf", "ppt", "jpg", "pps", "pptx", "swf", "png"]: 
#      template_name = "files/detail_slides.html"
#  elif file_objects.current_type == "mp3":
#      template_name = "files/detail_mp3.html"
#  elif file_objects.source_type == "rec_conf":
#      template_name = "files/detail_rec_conf.html"
#  elif file_objects.current_type == "vmail":
#      template_name = "files/detail_vmail.html"
#  elif file_objects.current_type != "mp3" and file_objects.duration:
#      template_name = "video.html"#"files/detail_video.html"
#  else:
#      template_name = "files/detail_generic.html"
#  return render_to_response(template_name, {
#      'file': file_objects,
#      'object_id': object_id,
#      'user': user_requesting,
#      'file_form': file_form,    
#      'urlhash':urlhash,
#      'subnav_current_add':'File %s'%file_objects,
#      'bucket':unicode(Site.objects.get_current()),
#      'thumb':file_objects.get_thumbnail()
#  }, context_instance=RequestContext(request))
#===============================================================================

#phatnguyen


@login_required
@check_file_view_limit
def private_file_detail(request, urlhash):
    return public_file_detail(request, urlhash)

def protected_file_detail(request, urlhash):
    return file_check_password(request, urlhash)
    
def file_detail(request, urlhash):
  uf = get_object_from_hash(UserFile,urlhash)
  if uf.visibility == "R":
      if request.user != uf.user:
          if request.user.is_authenticated():
              request.user.message_set.create(message=unicode("This file may only be viewed by the owner."))
          return HttpResponseRedirect(request.META.get("HTTP_REFERER", "/"))
      return private_file_detail(request,urlhash)
  elif uf.visibility == "O":
      if request.user == uf.user or request.session.get("%s_access" %urlhash, None):
          return public_file_detail(request, urlhash)
      return protected_file_detail(request, urlhash)
  else:
      return public_file_detail(request,urlhash)
   
#===============================================================================
# def file_detail(request, urlhash):
#    uf = get_object_from_hash(UserFile,urlhash)
#    return direct_to_template(request, 'files/files_detail.html',
#                              {
#                               'file':uf, 
#                               'CLOUDFRONT_STREAM':settings.CLOUDFRONT_STREAM,
#                               'CLOUDFRONT_DOWNLOAD':settings.CLOUDFRONT_DOWNLOAD
#                              })
#===============================================================================

def public_file_detail(request, urlhash):
   file_objects = get_object_from_hash(UserFile, urlhash)    
#   file_form = FileForm(instance=file_objects)
#   file_form.initial.pop("password", None)
   user_owner = file_objects.user
   user_requesting = request.user
   if user_requesting != user_owner:
       file_objects.increment_count()
   if file_objects.source_type in ['doc','docx','ods','odt','pdf', 'xls','xlsx', 'odp','ppt','pptx']:
       template_name = "files/files_view_document.html"
   elif file_objects.source_type in ['gif','jpg','png','sci','tif','jpeg','bmp']:
       template_name = "files/files_view_image.html"
   elif file_objects.current_type == 'mp3':
       template_name = "files/files_view_mp3.html"
   elif file_objects.current_type == 'mp4':
       template_name = "files/files_view_mp4.html"
   else:
       template_name = "files/files_detail_info.html"
       
   if user_requesting == user_owner:
       is_host = True
   else:
       is_host = False
       
   root_folder = None
   if is_host:
       root_folder = request.user.get_root_folder()[0]
   return direct_to_template(request,template_name, {
        'is_host':is_host,
       'CLOUDFRONT_STREAM':settings.CLOUDFRONT_STREAM,
       'CLOUDFRONT_DOWNLOAD':settings.CLOUDFRONT_DOWNLOAD,                                      
       'file': file_objects,
       'user': user_requesting,
#       'file_form': file_form,    
       'urlhash':urlhash,
       'subnav_current_add':'File %s'%file_objects,
       'bucket':unicode(Site.objects.get_current()),
       'root_folder':root_folder
#       'thumb':file_objects.get_thumbnail()
       })    
    

   #============================================================================
   # uf = get_object_from_hash(UserFile,urlhash)
   # if uf.visibility == "R":
   #    if request.user != uf.user:
   #        if request.user.is_authenticated():
   #            request.user.message_set.create(message=unicode("This file may only be viewed by the owner."))
   #        return HttpResponseRedirect(request.META.get("HTTP_REFERER", "/"))
   #    return private_file_detail(request,urlhash)
   # elif uf.visibility == "O":
   #    if request.user == uf.user or request.session.get("%s_access" %urlhash, None):
   #        return public_file_detail(request, urlhash)
   #    return protected_file_detail(request, urlhash)
   # else:
   #    return public_file_detail(request,urlhash)
   #============================================================================
    


def file_edit(request, urlhash, tags=None):
    
    if request.method == 'GET':
        if 'next' in request.GET:
            url_with_get = request.GET['next']
        else:
            url_with_get = reverse("my_files")
        file_object = get_object_from_hash(UserFile, urlhash) 
        return direct_to_template(request, 'files/newer_files_edit.html', {'file':file_object, 'next':url_with_get,'FILE_UPLOADER_URL':settings.FILE_UPLOADER_URL_V2,'bucket':settings.AWS_STORAGE_BUCKET_NAME})
    else:
        origin = get_object_from_hash(UserFile,urlhash)
        
        if 'uuid' in request.POST:
            uuid = request.POST['uuid'] 
            description = request.POST['description']
            title = request.POST['title']
            privacy = request.POST['privacy']
            password = None
            tags = request.POST['tags_string']
            folderid = int(request.POST['folderid'])
            folder = None
            if folderid!=0:
                folder = get_object_or_404(FileFolder, pk=int(folderid))
            file_object = get_object_or_404(UserFile.active_objects, uuid=uuid)

            timestamp = datetime.datetime.today()
            if privacy == 'public':
                 visibility = 'U'
            elif privacy == 'private':
                visibility = 'R'
            else:
                visibility = 'O'
                pwd = request.POST['password']
                if pwd!='********':
                    algo = "sha1"
                    salt = get_hexdigest(algo, str(random.random()), str(random.random()))[:5]
                    hashed_password = get_hexdigest(algo, salt, pwd)
                    password = "%s$%s$%s" %(algo, salt, hashed_password)
                else:
                    password = origin.password
            
            
            origin.title               = title
            origin.uuid                = file_object.uuid
            origin.filename            = file_object.filename
            origin.description         = description
            origin.tags_string         = tags
            origin.source_type         = file_object.source_type
            origin.current_type        = file_object.current_type
            origin.page_count          = file_object.page_count
            origin.duration            = file_object.duration
            origin.visibility          = visibility
            origin.password            = password
            origin.processed           = 1
            origin.file_size           = file_object.file_size
            origin.dimension_width     = file_object.dimension_width
            origin.dimension_height    = file_object.dimension_height
            origin.folder              = folder
            
            file_object.delete()
            origin.save()
            
            if folder:
                folder.auto_update_info()
                
            Tag.objects.update_tags(origin, origin.tags_string)
            
            update_total_storage(request)
            return HttpResponse('success')
        else:
            
            description = request.POST['description']
            title = request.POST['title']
            privacy = request.POST['privacy']
            password = None
            tags = request.POST['tags_string']
            folderid = int(request.POST['folderid'])
            folder = None
            if folderid!=0:
                folder = get_object_or_404(FileFolder, pk=int(folderid))
                
            timestamp = datetime.datetime.today()
            if privacy == 'public':
                 visibility = 'U'
            elif privacy == 'private':
                visibility = 'R'
            else:
                visibility = 'O'
                pwd = request.POST['password']
                if pwd!='********':
                    algo = "sha1"
                    salt = get_hexdigest(algo, str(random.random()), str(random.random()))[:5]
                    hashed_password = get_hexdigest(algo, salt, pwd)
                    password = "%s$%s$%s" %(algo, salt, hashed_password)
                else:
                    password = origin.password
            
            
            origin.title = title
            origin.description = description
            origin.visibility = visibility
            origin.password = password
            origin.tags_string = tags
            origin.timestamp = timestamp
            origin.folder = folder
            origin.save()
            if folder:
                folder.auto_update_info()
            Tag.objects.update_tags(origin, origin.tags_string)
            update_total_storage(request)
            return HttpResponseRedirect(request.POST['next'])

file_edit = login_required(file_edit)


def file_multiedit(request):
    if request.method == 'GET':
        if 'next' in request.GET:
            url_with_get = request.GET['next']
        else:
            url_with_get = reverse("my_files")
        urlhashs = request.GET.getlist('file')
        edit_file_ids = []
        for urlhash in urlhashs:
            try:
                edit_file_ids.append(int(uri_b64decode(str(urlhash))) / 42)
            except:
                pass
        
        edit_files = UserFile.objects.filter(id__in = edit_file_ids)
        tags = Tag.objects.usage_for_queryset(edit_files)
        tags_str = ', '.join([x.name for x in tags if x])
        
        return direct_to_template(request, 
                                  'files/newer_files_multiedit.html',
                                {'files':edit_files, 
                                 'urlhashs':urlhashs,
                                 'tags':tags_str,
                                'next':url_with_get,
                                'FILE_UPLOADER_URL':settings.FILE_UPLOADER_URL_V2,
                                'bucket':settings.AWS_STORAGE_BUCKET_NAME})
    else:
        if 'next' in request.POST:
            url_with_get = request.POST['next']
        else:
            url_with_get = reverse("my_files")
        urlhashs = request.POST.getlist('file')
        edit_files = []
        for urlhash in urlhashs:
            file_object = get_object_from_hash(UserFile, urlhash)
            edit_files.append(file_object)
            
        description = request.POST['description']
        privacy = request.POST['privacy']
        password = None
        tags = request.POST['tags_string']
        folderid = int(request.POST['folderid'])
        folder = None
        if folderid!=0:
            folder = get_object_or_404(FileFolder, pk=int(folderid))
            
        timestamp = datetime.datetime.today()
        if privacy == 'public':
             visibility = 'U'
        elif privacy == 'private':
            visibility = 'R'
        else:
            visibility = 'O'
            pwd = request.POST['password']
            algo = "sha1"
            salt = get_hexdigest(algo, str(random.random()), str(random.random()))[:5]
            hashed_password = get_hexdigest(algo, salt, pwd)
            password = "%s$%s$%s" %(algo, salt, hashed_password)
                
        for file in edit_files:
            file.description = description
            file.visibility = visibility
            file.password = password
            file.tags_string = tags
            file.folder = folder
            file.save()
            Tag.objects.update_tags(file, file.tags_string)
        
            
        return HttpResponseRedirect(url_with_get)
            
file_multiedit = login_required(file_multiedit)


def file_download(request,urlhash):
    uf = get_object_from_hash(UserFile,urlhash)
    if uf.visibility == "R":
        return private_file_download(request,urlhash)
    else:
        return public_file_download(request,urlhash)

@login_required
@check_file_download_limit
def private_file_download(request, urlhash):
    uf = get_object_from_hash(UserFile, urlhash)
    if request.user == uf.user:
        return public_file_download(request,urlhash)
    request.user.message_set.create(message=unicode(_("You cannot download other's files.")))
    return HttpResponseRedirect(reverse("my_files"))

def public_file_download(request, urlhash):
    # this code based on http://www.djangosnippets.org/snippets/1710/
    object_id = int(uri_b64decode(str(urlhash))) / 42
    user_file = get_object_or_404(UserFile, pk=object_id)
    
    uuid_file = user_file.uuid
    if user_file.child_uuid:
        uuid_file = user_file.child_uuid
    
    if user_file.visibility == "O":
        if request.user != user_file.user and not request.session.get("%s_access" %urlhash, None):
            return HttpResponseRedirect(reverse("file_check_password", kwargs={"urlhash": urlhash}))
    
    #During upload, the filename, if it contains, needs to be changed to exclude that -In flash. This is a temporary fix. 
    if user_file.source_type != 'rec_conf':
        key = get_key(settings.AWS_STORAGE_BUCKET_NAME, "files/%s.%s" %(uuid_file, user_file.source_type))
    else:
#        key = get_key(settings.AWS_STORAGE_BUCKET_NAME, "flv/recording/%s.%s" %(user_file.uuid, 'mp4'))
        key = get_key(settings.AWS_STORAGE_BUCKET_NAME, "files/%s.%s" %(uuid_file, 'mp4'))
    if not key:
        raise Http404
    url = generate_url(key, expires=1800)
    if user_file.source_type in ['png', 'jpg', 'jpeg', 'gif', 'tiff', 'bmp']:
        import urllib2
        file = urllib2.urlopen(url).read()
        file_name_tmp = settings.PROJECT_ROOT+'/server_logs/'+uuid_file+'.'+user_file.source_type
        f = open(file_name_tmp, 'w')
        f.write(file)
        f.close()
        response = HttpResponse(file,mimetype='application/force-download')
        response['Content-Disposition'] = 'attachment; filename=%s' % user_file.filename
        response['X-Sendfile'] = f
        import os
        os.remove(file_name_tmp)
        return response
    return HttpResponseRedirect(url)

@login_required
def create_file(request):
    "Created a UserFile object for a recording"
    soid = request.POST.get("soid", None)
    urlhash = request.POST.get("conf_urlhash", None)
    today = datetime.datetime.today()
    default_title = "Recorded conference %s" % today.strftime("%m/%d/%Y")
    recording_name = request.POST.get("recording_name", default_title)
    try:
        conf = get_object_from_hash(Event, urlhash)
    except Http404:
        conf = None
    title = recording_name
    user_file = UserFile.objects.create(title=title, user=request.user, processed=1, uuid=soid, 
                                        file_size=0, is_active=True, source_type="rec_conf", current_type="rec_conf", duration=100)
    RecordedConference.objects.create(user = request.user, fms_hostroom=user_file.uuid, duration=1, conference=conf)
    return HttpResponse("OK")

from tagging.models import Tag, TaggedItem


def get_tag_list_for_qs(qs):
    tags = set()
    for el in qs:
        tags = tags.union(el.tags)
    return tags

@login_required
def browse(request,tag1=None,tag2=None,tag3=None):
    all_user_files = request.user.userfile_set.all()
    tag_list1 = Tag.objects.usage_for_queryset(all_user_files)
    tag_list2 = []
    tag_list3 = []
    
    tag_list = []
    if tag1:
        tag1 = get_object_or_404(Tag,name=tag1.replace("-", " "))
        tag_list.append(tag1)
        if tag2:
            tag2 = get_object_or_404(Tag,name=tag2.replace("-", " "))
            tag_list.append(tag2)
        if tag3:
            tag3 = get_object_or_404(Tag,name=tag3.replace("-", " "))
            tag_list.append(tag3)
        
        uf_qs = UserFile.tagged.with_all((tag1,)).filter(user=request.user)
        
        #This is causing some error in table alias.
        #tag_list2 = Tag.objects.usage_for_queryset(queryset=uf_qs)
        tag_list2 = get_tag_list_for_qs(uf_qs)
        if tag1 in tag_list2:
            tag_list2.remove(tag1)
            
    if tag2:
        #This is awesome, but can't be used as the tags are user sensitive
        #tag_list3 = Tag.objects.related_for_model(tags=[tag1,tag2],model=UserFile)

        uf_qs = UserFile.tagged.with_all((tag1,tag2)).filter(user=request.user)
        
        tag_list3 = get_tag_list_for_qs(uf_qs)
        if tag2 in tag_list3:
            tag_list3.remove(tag2)
        if tag1 in tag_list3:
            tag_list3.remove(tag1)
        
        
    if tag1:
        user_files = TaggedItem.objects.get_intersection_by_model(all_user_files,tag_list)
    else:
        user_files = all_user_files
    ctx = {
        "tag1": tag1,
        "tag2": tag2,
        "tag3": tag3,
        "tag_list1": tag_list1,
        "tag_list2": tag_list2,
        "tag_list3": tag_list3
        }
    subnav_current = 'browse_files'
    return render_to_response("files/browse.html",
                              locals(),
                              RequestContext(request))

def inline_tags_edit(request):
    uf = get_object_from_hash(UserFile,request.POST.get('id'))
    tag_names = request.POST.get('value').strip()
    Tag.objects.update_tags(obj=uf,tag_names=tag_names)
    return render_to_response("files/browse_tags_block.html",{'uf':uf})
    #return HttpResponse(uf.get_tag_string())

from debug import ipython,idebug

def load_tags_inline(request):
    uf = get_object_from_hash(UserFile,request.GET.get('id'))
    return HttpResponse(uf.get_tag_string())

@login_required
def copy_file(request, urlhash=None):
    file_id = int(uri_b64decode(str(urlhash))) / 42
    user_file = None
    try:
        user_file = UserFile.objects.get(id=file_id)
    except UserFile.DoesNotExist:
        request.user.message_set.create(message=unicode(_(u"File does not exist. Copy failed.")))
        return HttpResponseRedirect(reverse("my_files"))
    if not user_file:
        request.user.message_set.create(message=unicode(_(u"An error occurred. Copy failed.")))
        return HttpResponseRedirect(reverse("my_files"))
    if user_file.visibility == "O" and request.user != user_file.user and not request.session.get("%s_access" %urlhash, None):
        return HttpResponseRedirect(reverse("file_check_password"))
    new_user_file = user_file.copy_file(request.user)
    if not new_user_file:
        request.user.message_set.create(message=unicode(_(u"Cannot copy private files. Copy failed.")))
        return HttpResponseRedirect(reverse("my_files"))
    request.user.message_set.create(message=unicode(_(u"Successfully copied file.")))
    return HttpResponseRedirect(reverse("file_detail", args=[new_user_file.urlhash]))

def file_check_password(request, urlhash=None):
    file_id = int(uri_b64decode(str(urlhash))) / 42
    user_file = None
    password_invalid = False
    try:
        user_file = UserFile.objects.get(id=file_id)
    except UserFile.DoesNotExist:
        if request.user.is_authenticated():
            request.user.message_set.create(message=unicode("File does not exist."))
        else:
            raise Http404
        return HttpResponseRedirect(reverse("my_files"))
    if request.method == "POST":
        password = request.POST.get("password")
        if password:
            if user_file.password and check_password(password, user_file.password):
                request.session["%s_access" %urlhash] = True
                return redirect(request.META.get("HTTP_REFERER", reverse("file_detail", args=[urlhash])))
            password_invalid = True
    return render_to_response("files/new_file_check_password.html",  {"password_invalid": password_invalid},
                              context_instance=RequestContext(request))

def control_folder_structure(request):
    folders = FileFolder.objects.filter(user=request.user, parent_folder=None).order_by('name') 
    folders_json_rep = create_entire_jstree(folders)
    folders_json_rep = simplejson.dumps(folders_json_rep)
    return render_to_response("files/control_folder_structure.html", {"json_data":folders_json_rep}, context_instance=RequestContext(request))

def create_jstree(folder):
    dic = {}
    dic["data"] = folder.name
    dic["children"] = []
    dic["attr"] = {}
    dic["attr"]["id"] = folder.id
    if folder.is_root:
        dic["attr"]["rel"] = "root_folder"
    elif folder.is_system and folder.parent_folder is None:
        dic["attr"]["rel"] = "root_folder"
    elif folder.is_system:
        dic["attr"]["rel"] = "system_folder"
    elif folder.is_trash:
        dic["attr"]["rel"] = "trash_folder"
    else:
        dic["attr"]["rel"] = "normal_folder"
    dic["metadata"] = {}
    dic["metadata"]["id"] = folder.id
    dic["metadata"]["total_files"] = folder.total_files
    dic["metadata"]["name"] = folder.name
    children = folder.filefolder_set.filter(is_deleted = False).order_by('name')
    for child in children:
        child_dic = create_jstree(child)
        dic["children"].append(child_dic)
    return dic

def create_entire_jstree(folders):
    entire_jstree = []
    for folder in folders:
        root_folder_dict = create_jstree(folder)
        entire_jstree.append(root_folder_dict)
    return entire_jstree

def add_folder(request):
    if request.method == 'GET':
        raise Http404
    name = request.POST.get('folder_name', '')
    if not name:
        request.user.message_set.create(message="Provide a name for the folder.")
        return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
    parent_folder_id = request.POST.get('parent_folder', '')
    parent_folder = FileFolder.objects.get(id=parent_folder_id) or None    
    FileFolder(name=name, user=request.user, parent_folder=parent_folder).save()
    request.user.message_set.create(message='New folder created.')
    return HttpResponseRedirect(reverse('control_folder_structure'))

def delete_folder(request):
    if request.method == 'GET':
        raise Http404
    delete_folder = request.POST.get('delete_folder', '')
    if not delete_folder:
        request.user.message_set.create(message="Select a folder to delete.")
        return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
    delete_folder = FileFolder.objects.get(user=request.user, id=delete_folder)
    delete_folder.delete()
    request.user.message_set.create(message='Folder deleted')
    return HttpResponseRedirect(reverse('control_folder_structure'))

def getFullTree(request):
    response = HttpResponse(mimetype="application/json")
    file_folders = []
    root_folders = request.user.get_root_folder()
    system_folders = request.user.get_system_folder()
    trash_folders = request.user.get_trash_folder()
    file_folders.extend(root_folders)
    file_folders.extend(system_folders)
    file_folders.extend(trash_folders)
    json_data_list = create_entire_jstree(file_folders) 
    json_data = simplejson.dumps(json_data_list) 
    response.write(json_data)
    return response

def getMoveTree(request):
    response = HttpResponse(mimetype="application/json")
    file_folders = []
    root_folders = request.user.get_root_folder()
    file_folders.extend(root_folders)
    if request.user.is_superuser:
        system_folders = request.user.get_system_folder()
        file_folders.extend(system_folders)

    json_data_list = create_entire_jstree(file_folders) 
    json_data = simplejson.dumps(json_data_list) 
    response.write(json_data)
    return response

def create_copy_child_folder(request, source_folder, folder):
    files = UserFile.objects.filter(folder = source_folder)
    for file in files:
        new_file = file
        new_file.child_uuid = file.uuid
        new_file.uuid = str(uuid4())
        new_file.folder = folder
        new_file.id = None
        new_file.save()
    
    children = source_folder.filefolder_set.all().order_by('name')
    for child in children:
        copy_folder = FileFolder(name = child.name,
                            user = request.user,
                            parent_folder = folder,
                            )
        copy_folder.save()
        create_copy_child_folder(request, child, copy_folder)




def delete_folder_action(request):
    result = {}
    id = int(request.GET['id'])
    file_folder = FileFolder.objects.get(pk = id)
    if file_folder.is_root or (file_folder.is_system and not request.user.is_superuser) or file_folder.is_trash:
        result['status'] = 403
    else:
        file_folder.move_to_trash()
        file_folder.is_mark = True
        file_folder.save()
        
        parent = file_folder.parent_folder
        parent.auto_update_info()
        update_total_storage(request)
        result['status'] = 200
        result['parent'] = parent.id
    return result



def create_folder_action(request):
    result = {}
    if request.method == 'GET' and 'id' in request.GET:
        parent_folder = get_object_or_404(FileFolder,pk = int(request.GET['id']))
    else:
        parent_folder = None
        
    allow_create = True
    if parent_folder:
        if parent_folder.is_system and not request.user.is_superuser:
            allow_create = False
        if parent_folder.is_trash:
            allow_create = False
            
    if allow_create:
        file_folder = FileFolder(
                                 name = request.GET['title'],
                                 user = request.user,
                                 parent_folder = parent_folder,
                                 is_system = parent_folder.is_system
                                 )
        file_folder.save()
        result['status'] = 200
        result['id'] = file_folder.id
    else:
        result['status'] = 403
            
    return result

def rename_child_action(folder):
    folder.calculate_absolute_path()
    folder.save()
    children = folder.filefolder_set.all()
    for child in children:
        rename_child_action(child)

def rename_folder_action(request):
    result = {}
    if request.method == 'GET' and 'id' in request.GET:
        file_folder = get_object_or_404(FileFolder,pk = int(request.GET['id']))
        allow_rename = True
        if file_folder.is_root or file_folder.is_system or file_folder.is_trash:
            allow_rename = False
        if allow_rename:
            file_folder.name = request.GET['new_name']
            file_folder.save()
            rename_child_action(file_folder)
            result['status'] = 200
            result['id'] = file_folder.id
        else:
            result['status'] = 403
        
    return result
def move_folder_action(request):
    result = {}
    result['status'] = 200
    if int(request.GET['copy']) == 1:
        
        id = int(request.GET['id'])
        source_folder = FileFolder.objects.get(pk = id)
        
        desc_id = int(request.GET['ref'])
        if desc_id!=0:
            desc_folder = FileFolder.objects.get(pk = desc_id)
        else:
            desc_folder = None
        file_folder = FileFolder(name = source_folder.name, user = request.user,parent_folder= desc_folder )
        file_folder.save()
        create_copy_child_folder(request, source_folder, file_folder)
        result['id'] = file_folder.id 
    else:
        id = int(request.GET['id'])
        source_folder = FileFolder.objects.get(pk = id)
        
        desc_id = int(request.GET['ref'])
        if desc_id!=0:
            desc_folder = FileFolder.objects.get(pk = desc_id)
        else:
            desc_folder = None
        
        source_folder.parent_folder = desc_folder
        source_folder.save()
        
    return result

def processActionTree(request):
    response = HttpResponse(mimetype="application/json")
    if 'operation' in request.GET:
        if request.GET['operation'] == 'create_node':
            result = create_folder_action(request)
        if request.GET['operation'] == 'remove_node':
            result = delete_folder_action(request)
        if request.GET['operation'] == 'rename':
            result = rename_folder_action(request)
        if request.GET['operation'] == 'move_node':
            result = move_folder_action(request)
        
    response.write(simplejson.dumps(result))
    return response

def multi_file_upload(request):
    if request.method == 'GET':
        file_folders = FileFolder.objects.filter(user=request.user, is_root = True)
        if not file_folders:
            folder_root = FileFolder(name='My Files',user = request.user, parent_folder = None, is_root = True )
            folder_root.save()
            file_folders = FileFolder.objects.filter(user=request.user, is_root = True)
        
        root_folder = file_folders[0]
        return direct_to_template(request, 'files/multi_file_upload_simpler.html',
                     {'user':request.user,'bucket':settings.AWS_STORAGE_BUCKET_NAME,
                      'FILE_UPLOADER_URL':settings.FILE_UPLOADER_URL_V2,
                      'root_folder':root_folder})

multi_file_upload = login_required(multi_file_upload)
        
def process_multiupload(request):
    uuid = request.POST['uuid']
#    uuid = '9aa8c13f3e2b7c899739d9bd9c86a700'
    description = request.POST['description']
    
    privacy = request.POST['privacy']
    password = None
    tags = request.POST['tags_string']
    folderid = int(request.POST['folderid'])
    folder = None
    if folderid!=0:
        folder = get_object_or_404(FileFolder, pk=int(folderid))
    file_object = get_object_or_404(UserFile.active_objects, uuid=uuid)
    
    
    timestamp = datetime.datetime.today()
    if privacy == 'public':
         visibility = 'U'
    elif privacy == 'private':
        visibility = 'R'
    else:
        visibility = 'O'
        pwd = request.POST['password']
        algo = "sha1"
        salt = get_hexdigest(algo, str(random.random()), str(random.random()))[:5]
        hashed_password = get_hexdigest(algo, salt, pwd)
        password = "%s$%s$%s" %(algo, salt, hashed_password)
    
    file_object.description = description
    file_object.visibility = visibility
    file_object.password = password
    file_object.tags_string = tags
    file_object.timestamp = timestamp
    file_object.folder = folder
    file_object.save()
    if folder:
        folder.auto_update_info()
    Tag.objects.update_tags(file_object, file_object.tags_string)
    update_total_storage(request)
    return HttpResponse('success')

process_multiupload = login_required(process_multiupload)

def move_file_to_folder(request):
    if request.method == 'GET':
        move_files = [int(x) for x in request.GET.getlist('file') if x]
        move_folders = [int(x) for x in request.GET.getlist('folder') if x]
        if move_files or move_folders:
            folder_id  = int(request.GET['folder_id'])
            folder_object = get_object_or_404(FileFolder, pk=folder_id)
            origin_folder = None
            if move_files and move_files[0]:
                origin_folder = UserFile.active_objects.get(pk=move_files[0]).folder
            elif move_folders and move_folders[0]:
                origin_folder = FileFolder.objects.get(pk=move_folders[0]).parent_folder
            
            UserFile.active_objects.filter(id__in=move_files).update(folder = folder_object,is_system = folder_object.is_system )
            FileFolder.objects.filter(id__in=move_folders).exclude(id = folder_object.id).update(parent_folder = folder_object,is_system = folder_object.is_system)
            
            if folder_object:
                folder_object.auto_update_info()
            if origin_folder:
                origin_folder.auto_update_info()
            return HttpResponse('success')
        return HttpResponse('error')
        
def changeFlags(request):
    file_ids = [int(x) for x in request.GET.getlist('file_id') if x]
    folder_ids = [int(x) for x in request.GET.getlist('folder_id') if x]
    flag = True
    if int(request.GET['flag']) == 0:
        flag = False
    if file_ids:   
        UserFile.active_objects.filter(id__in = file_ids).update(is_flag = flag)
    if folder_ids:   
        FileFolder.objects.filter(id__in = folder_ids).update(is_flag = flag)
    return HttpResponse('success')
        
changeFlags = login_required(changeFlags)

def move_files_to_trash(request):
    if request.method == 'POST':
        file_ids = [int(x) for x in request.POST.getlist('file') if x]
        folder_ids = [int(x) for x in request.POST.getlist('folder') if x]
        if file_ids:   
            UserFile.active_objects.filter(id__in = file_ids).update(is_deleted = True, is_mark = True)
        if folder_ids:   
            folders  = FileFolder.objects.filter(id__in = folder_ids)
            for file_folder in folders:
                file_folder.move_to_trash()
                file_folder.is_mark = True
                file_folder.save()
            
        current_folder = int(request.POST['current_folder'])
        try:
            FileFolder.objects.get(pk = current_folder).auto_update_info()
        except:
            pass
        return HttpResponse('success')
move_files_to_trash = login_required(move_files_to_trash)

def undo_delete_from_trash(request):
    file_ids = None
    if 'file_id' in request.GET: 
        file_ids = [int(x) for x in request.GET.getlist('file_id') if x]
    folder_ids = None
    if 'folder_id' in request.GET:
        folder_ids = [int(x) for x in request.GET.getlist('folder_id') if x]
    if file_ids:
        undo_files = UserFile.active_objects.filter(id__in = file_ids)
        for file in undo_files:
            if file.folder:
                if file.folder.is_deleted == False:
                    file.is_deleted = False
                else:
                    file.folder = None
                    file.is_deleted = False
            else:
                file.is_deleted = False
            file.save()
    if folder_ids:
        undo_folders = FileFolder.objects.filter(id__in = folder_ids)
        for file_folder in undo_folders:
            file_folder.undo_delete_from_trash()
    return HttpResponse('success')

undo_delete_from_trash = login_required(undo_delete_from_trash)

def delete_file_from_amazon(file):
    try:
        uuid_file = file.uuid
        if file.child_uuid:
            uuid_file = file.child_uuid
        '''delete orgirin file'''
        delete_key(settings.AWS_STORAGE_BUCKET_NAME, "files/%s.%s" %(uuid_file, file.source_type))
        
        if file.source_type in ['doc','docx','ods','odt','pdf', 'xls','xlsx', 'odp','ppt','pptx']:
            delete_folder_key(settings.AWS_STORAGE_BUCKET_NAME, "slides/%s" %(uuid_file))
        elif file.source_type in ['gif','jpg','png','sci','tif','jpeg','bmp']:
            delete_key(settings.AWS_STORAGE_BUCKET_NAME, "thumb/%s%s" %(uuid_file,'jpg'))
        elif file.current_type == 'mp3':
            delete_key(settings.AWS_STORAGE_BUCKET_NAME, "mp3/%s%s" %(uuid_file,'mp3'))
        elif file.current_type == 'mp4':
            if file.source_type != 'rec_conf':
                delete_key(settings.AWS_STORAGE_BUCKET_NAME, "mp4/%s%s" %(uuid_file+'_500','mp4'))
            else:
                delete_key(settings.AWS_STORAGE_BUCKET_NAME, "flv/recording/%s%s" %(uuid_file,'mp4'))
    except:
        pass
    
    
delete_file_from_amazon = login_required(delete_file_from_amazon)

def delete_filefolder_recur(folder):
    files = UserFile.objects.filter(folder = folder, is_deleted = True)
    for file in files:
        delete_file_from_amazon(file)
        file.delete()
    
delete_filefolder_recur = login_required(delete_filefolder_recur)

def empty_trash(request):
    if request.method == 'POST':
        if 'type' in request.POST and request.POST['type'] == 'all':
            delete_files = UserFile.objects.filter(user = request.user, is_deleted = True)
            for d_file in delete_files:
                delete_file_from_amazon(d_file)
                d_file.delete()
            delete_folders = FileFolder.objects.filter(user = request.user, is_deleted = True)
            for d_folder in delete_folders:
                delete_filefolder_recur(d_folder)
                d_folder.delete()
            return HttpResponse('success') 
        else:
            file_ids = None
            if 'file_id' in request.POST: 
                file_ids = [int(x) for x in request.POST.getlist('file_id') if x]
                delete_files = UserFile.objects.filter(user = request.user, id__in = file_ids, is_deleted = True)
                for d_file in delete_files:
                    delete_file_from_amazon(d_file)
                    d_file.delete()
                
            folder_ids = None
            if 'folder_id' in request.POST:
                folder_ids = [int(x) for x in request.POST.getlist('folder_id') if x]
                delete_folders = FileFolder.objects.filter(user =request.user, id__in = folder_ids)
                for d_folder in delete_folders:
                    delete_filefolder_recur(d_folder)
                    d_folder.delete()
            return HttpResponse('success')
        
empty_trash = login_required(empty_trash)

def tags_autocomplete(request):
    if request.method == 'GET':
        response = HttpResponse(mimetype="application/json")
        tag_value = request.GET['term']
        files = UserFile.active_objects.filter(user = request.user)
        tags = Tag.objects.usage_for_queryset(files)
        tag_objects = []
        for tag in tags:
            if tag.name.startswith(tag_value):
                new_obj = {}
                new_obj['id'] = tag.name
                new_obj['label'] = tag.name
                new_obj['value'] = tag.name
                tag_objects.append(new_obj)
        response.write(simplejson.dumps(tag_objects))
        return response
        
tags_autocomplete = login_required(tags_autocomplete)    

def hide_helper_session(request):
    if request.method == 'POST':
        if request.POST['type'] =='file':
            request.session['helper_file'] = True
        if request.POST['type'] =='contact':
            request.session['helper_contact'] = True
        return HttpResponse('success')
    raise Http404
        
hide_helper_session = login_required(hide_helper_session)


def copy_to_myfile(request, urlhash = None):
    file = get_object_from_hash(UserFile, urlhash)
    if request.user == file.user:
        request.user.message_set.create(message=unicode(_("The file owner by yourself.")))
        return HttpResponseRedirect(reverse("file_detail", args=[file.urlhash]))
    
    newfile = file.copy_file_combine_amazon(request.user)
    if not newfile:
        request.user.message_set.create(message=unicode(_("The file cannot copy.")))
        return HttpResponseRedirect(reverse("file_detail", args=[file.urlhash]))
    return HttpResponseRedirect(reverse('file_detail',args=[newfile.urlhash]))
    
copy_to_myfile = login_required(copy_to_myfile)




 