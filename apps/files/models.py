from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from django.template.defaultfilters import filesizeformat
from uuid import uuid4
import tagging
from tagging.fields import TagField
import datetime
from base64 import urlsafe_b64encode, urlsafe_b64decode
from django.conf import settings
if "notification" in settings.INSTALLED_APPS:
    from notification import models as notification
else:
    notification = None

from project.models import Project    
    
from netconf_utils.encode_decode import *
from netconf_utils.amazon_s3 import get_key, generate_url, clone_file,clone_folder
import urllib
from django.contrib.sites.models import Site
from tagging.models import Tag
from django.template import loader, Context
from django.db.models import Q
FILE_STATUS_CHOICES = (
    ("R", _("Private")),
    ("U", _("Public")),
    ("O", _("Protected")),
)
    
#  Subclass of Video to filter out inactive and unprocessed videos.
class ActiveFileManager(models.Manager):
    def get_query_set(self):
        return super(ActiveFileManager, self).get_query_set().filter(processed='1').filter(is_active=True)
        
#  Subclass of Video to filter out private, inactive and unprocessed videos.
class PublicFileManager(models.Manager):
    def get_query_set(self):
        return super(PublicFileManager, self).get_query_set().filter(processed='1').filter(is_active=True).filter(visibility="U")

class FileFolder(models.Model):
    name            = models.CharField(max_length=100)
    user            = models.ForeignKey(User)
    parent_folder   = models.ForeignKey('self', null=True, blank=True)
    is_root         = models.BooleanField(null=False, default=False)
    absolute_path   = models.TextField(editable=False)
    total_files     = models.IntegerField(null = False, default = 0)
    total_size      = models.IntegerField(null = False, default = 0)
    last_modified   = models.DateTimeField(auto_now = True, null = False, default = datetime.datetime.now())
    date_created    = models.DateTimeField(auto_now_add = True, null=False, default = datetime.datetime.now())
    is_private      = models.BooleanField(null = False, default = False)
    total_views     = models.IntegerField(null = False, default = 0)
    is_flag         = models.BooleanField(null = False, default = False)
    is_system       = models.BooleanField(null=False, default=False)
    is_trash        = models.BooleanField(null=False, default=False)
    is_deleted      = models.BooleanField(null=False, default=False)
    ''' display in trash '''
    is_mark         = models.BooleanField(null=False, default=False)
    
    def __unicode__(self):
        return self.name
    
    @property
    def urlhash(self):
        return uri_b64encode(str(self.pk * 42))
    
    def calculate_absolute_path(self):
        parent_folder_heirarachy = []
        par_folder = self.parent_folder
        while par_folder is not None:
            parent_folder_heirarachy.append(par_folder.name)
            par_folder = par_folder.parent_folder
        parent_folder_heirarachy.reverse()
        absolute_path = "/".join(parent_folder_heirarachy)
        if absolute_path:
            absolute_path = absolute_path + "/" + self.name
        else:
            absolute_path = self.name
        self.absolute_path = absolute_path
        
    def render_absolute_path(self):
        parent_folder_heirarachy = []
        parent_folder_heirarachy.append(self)
        par_folder = self.parent_folder
        while par_folder is not None:
            parent_folder_heirarachy.append(par_folder)
            par_folder = par_folder.parent_folder
        
        parent_folder_heirarachy.reverse()
        root = False
        if len(parent_folder_heirarachy) ==1:
            root = True
        context = Context({'folder_list':parent_folder_heirarachy,'is_only':root})
        html_part = loader.get_template('files/folder_title_holder.html').render(context)
        return html_part
    def getinfo_from_child(self):
        if self.is_root:
            files = UserFile.active_objects.filter(user = self.user, is_deleted = self.is_deleted).filter(Q(folder=self) | Q(folder=None))
        else:
            files = UserFile.active_objects.filter(user = self.user, folder = self, is_deleted = self.is_deleted)
        total_files = len(files)
        total_size = 0
        for file in files:
            total_size +=file.file_size
        childrens = self.filefolder_set.filter(is_deleted = self.is_deleted)
        for child in childrens:
            child_files, child_size = child.getinfo_from_child()
            total_files +=child_files
            total_size +=child_size
        return total_files, total_size
    
    def auto_update_info(self):
        total_files, total_size = self.getinfo_from_child()
        self.total_files = total_files
        self.total_size = total_size
        self.save()
        par_folder = self.parent_folder
        while par_folder is not None:
            par_folder.auto_update_info()
            par_folder = par_folder.parent_folder
            
    
    def move_to_trash(self):
        files = UserFile.active_objects.filter(user = self.user, folder = self).update(is_deleted = True)
        childrens = self.filefolder_set.all()
        for child in childrens:
            child.move_to_trash()
            child.is_deleted = True
            child.save()
        self.is_deleted = True
        self.save()
        
    def undo_delete_from_trash(self):
        files = UserFile.active_objects.filter(user = self.user, folder = self).update(is_deleted = False, is_mark = False)
        childrens = self.filefolder_set.all()
        for child in childrens:
            child.undo_delete_from_trash()
            child.is_deleted = False
            child.is_mark = False
            child.save()
        self.is_deleted = False
        self.is_mark = False
        self.save()
    
    def get_all_child(self):
        result = []
        child_folders = self.filefolder_set.all()
        for f in child_folders:
            result.append(f)
            result.extend(f.get_all_child())
        return result
        
    def save(self, *args, **kwargs):
        self.calculate_absolute_path()
        super(FileFolder, self).save(*args, **kwargs)

def __user_get_root_folder(user):
    root_folder = FileFolder.objects.filter(user=user, parent_folder = None, is_root = True)
    if not root_folder:
        root_folder = FileFolder(name='My Files',user = user, parent_folder = None, is_root = True )
        root_folder.save()
        root_folder = FileFolder.objects.filter(user=user, parent_folder = None, is_root = True)
    return root_folder
User.add_to_class('get_root_folder', __user_get_root_folder)

def __user_get_system_folder(user):
    system_folder = FileFolder.objects.filter(parent_folder = None, is_system = True)
    if not system_folder:
        users = User.objects.filter(is_superuser = True)
        if users:
            admin_user = users[0]
        system_folder = FileFolder(name='System Files',user = admin_user, parent_folder = None, is_system = True )
        system_folder.save()
        system_folder = FileFolder.objects.filter(parent_folder = None, is_system = True)
    return system_folder
User.add_to_class('get_system_folder', __user_get_system_folder)

def __user_get_trash_folder(user):
    trash_folder = FileFolder.objects.filter(user=user, parent_folder = None, is_trash = True)
    if not trash_folder:
        trash_folder = FileFolder(name='Trash',user = user, parent_folder = None, is_trash = True )
        trash_folder.save()
        trash_folder = FileFolder.objects.filter(user=user, parent_folder = None, is_trash = True)
    return trash_folder
User.add_to_class('get_trash_folder', __user_get_trash_folder)
    
class UserFile(models.Model):
    """
    A file of any type uploaded to the site.  Currently processed to pdf by OpenOffice,
    then split into jpg slides by our server.
    """
    title               = models.CharField(_('title'), max_length=100)
    user                = models.ForeignKey(User)
    uuid                = models.CharField(_('uuid'), max_length=36, blank=True, unique=True)
    timestamp           = models.DateTimeField(_('timestamp'), auto_now_add=True, blank=True)
    filename            = models.CharField(_('filename'), max_length=100, null=True, blank=True)
    description         = models.TextField(null=True, blank=True)
    tags_string         = TagField()
    source_type         = models.CharField(_('source_type'), max_length=16, null=True, blank=True)
    current_type        = models.CharField(_('current_type'), max_length=16, null=True, blank=True)
    page_count          = models.IntegerField(_('page_count'), null=True, blank=True)
    duration            = models.IntegerField(max_length=50, null=True, blank=True)
    visibility          = models.CharField(_('private'), max_length=1, default="U")
    password            = models.CharField(_('password'), max_length=75, null=True, blank=True)
    processed           = models.IntegerField(_('processed'), max_length=2)
    is_active           = models.BooleanField(_('is_active'))
    
    views_total         = models.PositiveIntegerField(default=0, editable=False)
    views_this_month    = models.PositiveIntegerField(default=0, editable=False)
    
    downloads_total     = models.PositiveIntegerField(default=0, editable=False)
    downloads_this_month = models.PositiveIntegerField(default=0, editable=False)
    
    file_size           = models.PositiveIntegerField(help_text='Size of the file, in bytes')
    project             = models.ForeignKey(Project,blank=True,null=True, related_name="projectfile_set")

    orig_file_id        = models.PositiveIntegerField(default=0, editable=False)
    copied_from         = models.ForeignKey(User, null=True, related_name="file_copy_user")
    child_uuid          = models.CharField(_('child uuid'), max_length=36, null=True)
    
    last_modified = models.DateTimeField(_('last_modified'), blank=True)
    
    dimension_width     = models.IntegerField(max_length=50, null=True, blank=True)
    dimension_height    = models.IntegerField(max_length=50, null=True, blank=True)
    
    
    objects             = models.Manager() # The default manager.
    active_objects      = ActiveFileManager() # The active-and-processed-specific manager.
    public_objects      = PublicFileManager()
    
    folder              =  models.ForeignKey(FileFolder, blank=False, null=True, default = None)
    is_flag             = models.BooleanField(_('is_flag'), null=False, default=False)
    is_deleted          = models.BooleanField(null=False, default=False)
    ''' display in trash '''
    is_mark             = models.BooleanField(null=False, default=False)
    is_system           = models.BooleanField(null=False, default=False)

    @property
    def urlhash(self):
        return uri_b64encode(str(self.pk * 42))

    class Meta:
        ordering = ('-timestamp', 'title')
    
    def increment_count(self):
        self.views_total += 1
        self.save()
    
    def __unicode__(self):
        return self.title

    def copy_file(self, to_user):
        if self.visibility == "R":
            return None
        self.views_total = 0
        self.views_this_month = 0
        self.downloads_total = 0
        self.downloads_this_month = 0
        self.copied_from = self.user
        self.user = to_user
        self.child_uuid = self.uuid
        self.uuid = ""
        self.orig_file_id = self.id
        self.id = None
        return self.save(force_insert=True)
    
    def clone_on_amazon(self):
        random_name = str(uuid4())
        old_name = self.uuid
        if self.child_uuid:
            old_name = self.child_uuid
            
        clone_file(settings.AWS_STORAGE_BUCKET_NAME, '%s.%s'%(old_name, self.source_type) , 'files/', '%s.%s'%(random_name, self.source_type))
        
        if self.current_type =='mp4':
            if self.source_type == 'rec_conf':
                clone_file(settings.AWS_STORAGE_BUCKET_NAME, '%s.%s'%(old_name, 'mp4') , 'flv/recording/', '%s.%s'%(random_name, 'mp4'))
            else:
                clone_file(settings.AWS_STORAGE_BUCKET_NAME, '%s.%s'%(old_name+'_500', 'mp4') , 'mp4/', '%s.%s'%(random_name+'_500', 'mp4'))
            
            clone_file(settings.AWS_STORAGE_BUCKET_NAME, '%s.%s'%(old_name, 'jpg') , 'thumb/', '%s.%s'%(random_name, 'jpg'))
        
        if self.source_type in ['doc','docx','ods','odt','pdf', 'xls','xlsx', 'odp','ppt','pptx']:
            clone_folder(settings.AWS_STORAGE_BUCKET_NAME, old_name, 'slides/%s/'%(old_name), random_name)
        
        elif self.current_type == 'mp3':
            clone_file(settings.AWS_STORAGE_BUCKET_NAME, '%s.%s'%(old_name, 'mp3') , 'mp3/', '%s.%s'%(random_name, 'mp3'))
            
        return random_name
            
    def copy_file_combine_amazon(self, to_user):
        if self.visibility == "R":
            return None
        self.views_total = 0
        self.views_this_month = 0
        self.downloads_total = 0
        self.downloads_this_month = 0
        self.copied_from = self.user
        self.user = to_user
        self.uuid = self.clone_on_amazon()
        self.folder = None
        self.orig_file_id = self.id
        self.id = None
        self.save(force_insert=True)
        return self
    
    def copy_file_free(self, to_user):
        """This allows putting a file for
           a User without increasing his file uploaded limit"""
        self.file_size = 0
        return self.copy_file(to_user)
        
    def get_file(self):
        key = get_key(key="files/%s.%s" % (self.uuid, self.current_type))
        fp = urllib.urlopen(generate_url(key=key))
        return fp
    
    def is_selling(self):
        "Is this file available for sale in the store?"
        from store.models import StoreObject
        try:
            StoreObject.objects.get(file = self)
            return True
        except StoreObject.DoesNotExist:
            return False    
    def sell(self):
        "Start selling this object"
        from store.models import StoreObject
        try:
            obj = StoreObject.objects.get(file = self)
            return obj
        except StoreObject.DoesNotExist:
            return StoreObject.objects.create(file = self, user=self.user)
        
    def un_sell(self):
        "Stop selling this object"
        from store.models import StoreObject
        try:
            obj = StoreObject.objects.get(file = self)
            obj.delete()
        except StoreObject.DoesNotExist:
            return None
    
    def toggle_sale(self):
        if self.is_selling():
            self.un_sell()
        else:
            self.sell()

        
    def get_absolute_url(self):
        return '/file/%s/' % uri_b64encode(str(self.pk * 42))
        
    def get_edit_url(self):
        return '/files/edit/%s/' % uri_b64encode(str(self.pk * 42))
    
    def get_download_url(self):
        return '/files/download/%s/' % uri_b64encode(str(self.pk * 42))
    
    def get_amazonS3_url(self):
        key = get_key(settings.AWS_STORAGE_BUCKET_NAME, "files/%s.%s" %(self.uuid, self.source_type))
        if not key:
            return None
        url = generate_url(key, expires=1800)
        return url
    def get_thumbnail(self):
        key = get_key(settings.AWS_STORAGE_BUCKET_NAME, "thumb/%s.jpg" %(self.uuid))
        if not key:
            return ''
        url = generate_url(key, expires=1800)
        return url.split('?Signature')[0]
    
    def get_thumbnail(self):
        key = get_key(settings.AWS_STORAGE_BUCKET_NAME, "thumb/%s.jpg" %(self.uuid))
        if not key:
            return ''
        url = generate_url(key, expires=1800)
        return url.split('?Signature')[0]
    
    def get_netsign_create_url(self):
        return '/netsign/create/%s/' % uri_b64encode(str(self.pk * 42))
    
    def get_netsign_review_url(self):
        return '/netsign/review/%s/' % uri_b64encode(str(self.pk * 42))
    def get_file_path(self):
        file_path_start = settings.DOWNLOAD_URL
        file_path = "%s%s_%s" % (file_path_start, self.user.username, self.filename)
        return file_path;
    def is_editable(self):
        from integrate import google_api
        if self.source_type in google_api.EDITABLE_FILE_TYPES:
            return True;
        return False;
    @property
    def size(self):
        return filesizeformat(self.file.size)
        
    def save(self, *args, **kwargs):
        if not self.title:  
            self.title = self.filename
        if not self.uuid:
            self.uuid = str(uuid4())  # random so it can't be easily guessed
        if not self.last_modified:
            self.last_modified = datetime.datetime.now()
        super(UserFile, self).save( *args, **kwargs)
        
    def get_tag_string(self):
        tags_string = ""
        for el in self.tags:
            tags_string += "%s "%el.name
        tags_string.strip()
        return tags_string
    
    def is_document_file(self):
        DOCUMENT_LIST = ['doc','docx','html','ods','odt','pdf','rtf','sxc','sxw','txt','wpd','xls']
        if self.source_type in DOCUMENT_LIST:
            return True
        return False
    
    def is_file_supported(self):
        FILES_SUPPORTED = ['aac','avi','doc','f4v','flv', 'gif','html', 'jpg', 'm4a','m4v', 'mov', 'mp3','mp4', 'mpg', 'odp','ods', 'odt', 'pdf', 'png', 'ppt', 'pptx','rtf', 'sci', 'swf', 'sxc', 'sxw', 'tif','txt', 'wav', 'wmv', 'wpd', 'xls', 'zip']
        if self.source_type in FILES_SUPPORTED:
           return True
        return False
    
    def encode_url_video_share(self):
        params = {}
        uuid = self.uuid
        if self.child_uuid:
            uuid = self.child_uuid
        if self.source_type == 'rec_conf':
            params['file'] = 'flv/recording/'+ uuid + '.mp4'
        else:
            params['file'] = 'mp4/'+ uuid + '_500.mp4'
        params['abouttext'] = "Netconference"
        params['aboutlink'] = unicode(Site.objects.get_current())
        params['provider'] = 'rtmp'
        params['streamer'] = 'rtmp://' + settings.CLOUDFRONT_STREAM
        params['controlbar'] = 'bottom'
        params['width'] = self.dimension_width
        params['height'] = self.dimension_height
        return urllib.urlencode(params)

# handle notification of new comments
from threadedcomments.models import ThreadedComment
def new_comment(sender, instance, **kwargs):
    if isinstance(instance.content_object, UserFile):
        file = instance.content_object
        comment = instance.comment
        commenter = instance.user.username
        if notification:
            notification.send([file.user], "files_file_comment", {"file": file, "file_title": file.title, "instance": instance, "comment": comment, "commenter": commenter})
models.signals.post_save.connect(new_comment, sender=ThreadedComment)

try:        
    tagging.register(UserFile)
except tagging.AlreadyRegistered:
    pass

def insert_netconference_overview_files(sender, **kwargs):
    user = kwargs['instance']
    if user.username=='akshar':
        return
    file_owner = User.objects.get(username='akshar')
    #uf = UserFile.objects.get(title='NetConference Overview', user=file_owner)
    netconf_files = []
    uf = UserFile.objects.get(title='NetConference Overview', user=file_owner)
    netconf_files.append(uf)
    uf = UserFile.objects.get(title='Five Steps for a Successful NetConference', user=file_owner)
    netconf_files.append(uf)
    uf = UserFile.objects.get(title='Virtual Teamwork 101', user=file_owner)
    netconf_files.append(uf)
    for uf in netconf_files:
        try:
            UserFile.objects.get(title=uf.title, user=user, file_size=0)
        except UserFile.DoesNotExist:
            uf.copy_file_free(user) 
#models.signals.post_save.connect(insert_netconference_overview_files, sender=User)
