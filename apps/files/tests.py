from files.models import *
from files.views import *
from files.forms import *

from django.test import TestCase
from django.test import Client
from django.contrib.auth.models import User

from debug import ipython

class FileEditTest(TestCase):
    
    def setUp(self):
        self.client = Client()
        
        self.user = User(
            username="testUser"
        )
        self.user.save()
        
        self.file = UserFile(
            title="test file",
            description="this is a test file",
            user = self.user,
            filename="test.doc",
            source_type="test source",
            current_type="test type",
            number_of_slides=1,
            private=True,
            processed=1,
            is_active=True,
            in_queue=False
        )
        self.file.save()
    
    
    def test_modify_file_direct(self):
        request = {
            "title": "edited test file",
            "private": False,
            "description": "this is an edited test file"
        }
        
        form = UserFileForm(self.user, request)
        
        self.assertTrue(form.is_valid())
        
        self.file.title = form.cleaned_data['title']
        self.file.description = form.cleaned_data['description']
        self.file.private = form.cleaned_data['private']
        self.file.save()
        
        self.assertEqual(self.file.title, request['title'])

        # Did it get saved to the database?
        my_file = UserFile.objects.get(pk=self.file.id)        
        self.assertEqual(my_file.title, request['title'])
    
    def test_file_edit_view(self):
        pass
    
class PdfDownloadTest(TestCase):
    fixtures = ['netsign.json',]
    
    def setUp(self):
        pass
        
    def test1(self):
        pass

from netconf_utils.tests import TestUserTestCase
from tagging.models import Tag, TaggedItem
from itertools import combinations
from subscription.models import Subscription

class BrowseTagsTest(TestUserTestCase):
    def setUp(self):
        self.test_user = User.objects.create_user('test','lennon@thebeatles.com',password='test')
        self.client.login(username='test',password='test')
        Subscription.objects.create(name='Free',price=0)
        files = []
        for i in range(10):
            uf = UserFile(
                title="test file",
                description="this is a test file",
                user = test_user,
                filename="test%s.doc"%i,
                source_type="test source",
                current_type="test type",
                private=True,
                processed=1,
                is_active=True,
                file_size=123
                )
            uf.save()
            files.append(uf)
        tags = []
        for i in range(210,220):
            tags.append(Tag.objects.create(name="tag%s"%i))
        tags_list = combinations(tags,5)
        for afile in files:
            file_tags = tags_list.next()
            for atag in file_tags:
                #ipython()
                ti = TaggedItem(object=afile,tag=atag)
                ti.save()
                
        self.files = files
        self.tags_list = tags_list
                
            
            
    
    def test1(self):
        ipython()
        #When no tag passed, the q1 should contain all tags
        
        
        