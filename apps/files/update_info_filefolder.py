import os,sys
import datetime
activate_this = '/home/esofthead/pinax_env/bin/activate_this.py'
execfile(activate_this, dict(__file__=activate_this))
sys.path.insert(1,'/home/esofthead')
sys.path.insert(1,'/home/esofthead/netconference')
sys.path.insert(1,'/home/esofthead/netconference/apps')


#activate_this = '/home/cowboybkit/workspace/repository/pinax_env/bin/activate_this.py'
#execfile(activate_this, dict(__file__=activate_this))
#sys.path.insert(1,'/home/cowboybkit/workspace/recurly_integrate')
#sys.path.insert(1,'/home/cowboybkit/workspace/recurly_integrate/netconference')
#sys.path.insert(1,'/home/cowboybkit/workspace/recurly_integrate/netconference/apps')


os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'

from django.conf import settings
sys.path.append(settings.PINAX_ROOT)
sys.path.append("%s/%s"%(settings.PINAX_ROOT,'apps'))

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()
sys.stdout = sys.stderr

from files.models import FileFolder

def run_update_info():
    folders = FileFolder.objects.all()
    for folder in folders:
        folder.auto_update_info()
run_update_info()