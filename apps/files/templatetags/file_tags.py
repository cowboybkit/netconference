from django import template
#from django.template.defaultfilters import stringfilter
from datetime import timedelta
from django.template.defaultfilters import stringfilter
import unicodedata, re
from django.utils.safestring import mark_safe
from files.models import UserFile

SUPPORTED_FILETYPES = ['rtf', 'wmv', 'ppt', 'wav', 'mp4', 'zip', 
                       'txt', 'sxw', 'sci', 'jpg', 'm4a', 'flv', 
                       'odp', 'avi', 'aac', 'sxc', 'doc', 'mp3', 
                       'mov', 'pdf', 'swf', 'wpd', 'f4v', 'xls', 
                       'odt', 'mpg', 'html', 
                       'docx', 'xlsx', ]

register = template.Library()

@register.filter("vidtime")
def vidtime(value):
    if not value:
        value = 0
    return timedelta(seconds=value)
    
@register.filter("vidprivacy")
def vidprivacy(value):
    if value == "R":
        return "lock"
    elif value == "O":
        return "protected"
    else:
        return "public"

@register.filter("fileicon")
def fileicon(value):
    if not value:
        value = ""
    value = value.lower()
    if value and value not in SUPPORTED_FILETYPES:
        value = ""
    return value

@register.filter("revslug")
def revslug(value):
    "Reversible slug. Just converts spaces to hyphens"
    value = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore')
    return mark_safe(unicode(re.sub('[\s]+', '-', value).strip().lower()))
revslug.is_safe = True
revslug = stringfilter(revslug)

class FileObjectNode(template.Node):
    def __init__(self, inst, attr, context_var = None):
        self.instance = inst
        self.attr = attr
        self.context_var = context_var

    def render(self, context):
        file_instance = self.instance.resolve(context)
        cmp_file_instance = file_instance
        while (cmp_file_instance and cmp_file_instance.child_uuid):
            if self.attr == "uuid" and getattr(file_instance, "child_uuid"):
                file_instance.uuid = cmp_file_instance.child_uuid
            if self.attr == "id" and getattr(file_instance, "orig_file_id"):
                file_instance.id = cmp_file_instance.orig_file_id
            try:
                cmp_file_instance = UserFile.objects.get(id=cmp_file_instance.orig_file_id)
            except UserFile.DoesNotExist:
                cmp_file_instance = None
        if self.context_var:
            context[self.context_var] = getattr(file_instance, self.attr)
            return ""
        return getattr(file_instance, self.attr)

def _calculated_file_attr(parser, token, attr):
    tokens = token.split_contents()
    as_var = None
    if not (len(tokens) == 4 or len(tokens) == 2):
        raise template.TemplateSyntaxError("Invalid number of arguments received.")
    if len(tokens) == 4 and tokens[2] != u"as":
        raise template.TemplateSyntaxError("Expected 3rd token to be 'as'.")
    if len(tokens) == 4:
        as_var = tokens[3]
    file_instance = parser.compile_filter(tokens[1])
    return FileObjectNode(file_instance, attr, as_var)

@register.tag
def calculated_file_uuid(parser, token):
    return _calculated_file_attr(parser, token, "uuid")

@register.tag
def calculated_file_id(parser, token):
    return _calculated_file_attr(parser, token, "id")

@register.simple_tag
def eclipse_description(description):
    result = 'no title'
    if description:
        if len(description)>100:
            result=description[:100]+' ...' 
        else: result = description
    return result

@register.simple_tag
def generate_get_url(title, value):
    result = ''
    separate = '&'+title+'='
    for i in value:
        result += separate+i
    return result