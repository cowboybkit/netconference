from django import template
from django.utils.translation import ugettext as _
from django.conf import settings

register = template.Library()

@register.inclusion_tag('files/sort_link_frag.html', takes_context=True)            
def sort_link(context, link_text, sort_field, visible_name=None):
    """Usage: {% sort_link "link text" "field_name" %}
    Usage: {% sort_link "link text" "field_name" "Visible name" %}
    """
    is_sorted = False
    sort_order = None
    orig_sort_field = sort_field
    if context.get('current_sort_field') == sort_field:
        sort_field = '-%s'%sort_field
        visible_name = '-%s'%(visible_name or orig_sort_field)
        is_sorted = True
        sort_order = 'down'
    elif context.get('current_sort_field') == '-'+sort_field:
        visible_name = '%s'%(visible_name or orig_sort_field)
        is_sorted = True
        sort_order = 'up'

    if visible_name:
        if 'request' in context:
            request = context['request']
            request.session[visible_name] = sort_field
        
    #media_url = context['media_url']
    if 'getsortvars' in context:
        extra_vars = context['getsortvars']
    else:
            request = context['request']
            getvars = request.GET.copy()
            if 'sort_by' in getvars:
                del getvars['sort_by']
            if len(getvars.keys()) > 0:
                context['getsortvars'] = "&%s" % getvars.urlencode()
            else:
                context['getsortvars'] = ''
            extra_vars = context['getsortvars']
                

        
    return {'link_text': _(link_text), 'sort_field':sort_field, 'extra_vars':extra_vars, 'sort_order':sort_order, 'is_sorted':is_sorted, 'visible_name':visible_name, 'MEDIA_URL': settings.MEDIA_URL}
        
        
@register.inclusion_tag('files/new_sort_link_frag.html', takes_context=True)            
def sort_link_new(context, link_text, sort_field, sort_active = False,  visible_name=None, ):
    """Usage: {% sort_link "link text" "field_name" %}
    Usage: {% sort_link "link text" "field_name" "Visible name" %}
    """
    is_sorted = False
    sort_order = None
    orig_sort_field = sort_field
    if context.get('current_sort_field') == sort_field:
        sort_field = '-%s'%sort_field
        visible_name = '-%s'%(visible_name or orig_sort_field)
        is_sorted = True
        sort_order = 'down'
    elif context.get('current_sort_field') == '-'+sort_field:
        visible_name = '%s'%(visible_name or orig_sort_field)
        is_sorted = True
        sort_order = 'up'

    if visible_name:
        if 'request' in context:
            request = context['request']
            request.session[visible_name] = sort_field
        
    #media_url = context['media_url']
    if 'getsortvars' in context:
        extra_vars = context['getsortvars']
    else:
            request = context['request']
            getvars = request.GET.copy()
            if 'sort_by' in getvars:
                del getvars['sort_by']
            if len(getvars.keys()) > 0:
                context['getsortvars'] = "&%s" % getvars.urlencode()
            else:
                context['getsortvars'] = ''
            extra_vars = context['getsortvars']
                

        
    return {'link_text': _(link_text), 'sort_field':sort_field, 'extra_vars':extra_vars, 'sort_order':sort_order, 'is_sorted':is_sorted, 'visible_name':visible_name, 'MEDIA_URL': settings.MEDIA_URL,'sort_active':sort_active}
        
        
