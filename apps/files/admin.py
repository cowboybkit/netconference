from django.contrib import admin
from files.models import UserFile, FileFolder

class UserFileAdmin(admin.ModelAdmin):
    list_display = ('title', 'source_type', 'user', 'timestamp', 'visibility', 'is_active','file_size')
    list_filter = ['user', 'source_type', 'is_active', 'visibility']
    #list_editable = ['user','visibility','is_active']
    search_fields = ['title']
    date_hierarchy = 'timestamp'
    actions = ['delete_selected', 'make_inactive', 'make_active', 'make_public', 'make_visibility']
    
    def make_inactive(self, request, queryset):
        queryset.update(is_active=False)
    make_inactive.short_description = "Make selected files inactive"

    def make_active(self, request, queryset):
        queryset.update(is_active=True)
    make_active.short_description = "Make selected files active"

    def make_public(self, request, queryset):
        queryset.update(visibility=False)
    make_public.short_description = "Make selected files public"

    def make_visibility(self, request, queryset):
        queryset.update(is_active=True)
    make_visibility.short_description = "Make selected files visibility"

admin.site.register(UserFile, UserFileAdmin)
admin.site.register(FileFolder)
