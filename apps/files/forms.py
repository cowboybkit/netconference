from django import forms
from django.contrib.auth.models import User
from files.models import UserFile, FILE_STATUS_CHOICES
from tagging.forms import TagField
from django.forms.util import ErrorList
from debug import ipython
from projects.models import Project
from netconf_utils.widgets import ReadOnlyWidget
from django.utils.translation import ugettext_lazy as _
from django.contrib.formtools.wizard import FormWizard
import re
from django.forms.widgets import RadioSelect
from django.forms.fields import ChoiceField

valid_tag_names = re.compile("[\w\.\_\-\,\s]+")

class FileUploadForm(forms.ModelForm):
    password = forms.CharField(label=_(u"Password"), widget=forms.PasswordInput(render_value=False)) 
    password2 = forms.CharField(label=_(u"Verify Password"), widget=forms.PasswordInput(render_value=False)) 

    class Meta:
        model = UserFile
        fields = ('title','private','description','tags_string', 'password', )
        
    def __init__(self, user = None, *args, **kwargs):
        self.user = user
        super(FileUploadForm, self).__init__(*args, **kwargs)
        project_id = kwargs.get('initial',{}).get('project',None)
        if project_id:
            project = Project.objects.get(pk=project_id)
            self.fields['project'] = forms.CharField(widget=ReadOnlyWidget(original_value=project,display_value=project.name))

    def clean_tags_string(self):
        tags_string = self.cleaned_data["tags_string"].lower()
        match_valid_tag = valid_tag_names.match(tags_string)
        if not match_valid_tag or not len(tags_string) == len(match_valid_tag.group()):
            raise forms.ValidationError(_("Only characters, digits, _ (underscore), "
                                          "- (hyphen) and spaces allowed in a tag"))
        return tags_string

class FileUploadFormStep1(forms.ModelForm):
    visibility = forms.CharField(max_length=1, widget=forms.Select(choices=FILE_STATUS_CHOICES))
    uid = forms.CharField(max_length=10, widget=forms.HiddenInput())
    password = forms.CharField(max_length=75, widget=forms.PasswordInput(), required=False)
    confirm_password = forms.CharField(max_length=75, widget=forms.PasswordInput(), required=False)
    username = forms.CharField(max_length=50, widget=forms.HiddenInput())
    redirect_url = forms.CharField(max_length=100, required=False, widget=forms.HiddenInput())

    def __init__(self, *args, **kwargs):
        if not kwargs.get("initial", None):
            kwargs.update({"initial": {"visibility": "U"} })
        self.user = kwargs.pop("user", None)
        super(FileUploadFormStep1, self).__init__(*args, **kwargs)

    class Meta:
        model = UserFile
        fields = ('title', 'tags_string', 'description', 'project', 'visibility', 'password')

    def clean_title(self):
        title = self.cleaned_data["title"].strip()
        if not title:
            raise forms.ValidationError(_("This field is required."))
        return title

    def clean_tags_string(self):
        tags_string = self.cleaned_data["tags_string"].strip()
        if not tags_string and self.cleaned_data["tags_string"]:
            raise forms.ValidationError(_("Whitespace is not allowed in this field."))
        return tags_string

    def clean_description(self):
        description = self.cleaned_data["description"].strip()
        if not description and self.cleaned_data["description"]:
            raise forms.ValidationError(_("Whitespace is not allowed in this field."))
        return description

    def clean_password(self):
        password = self.cleaned_data["password"].strip()
        if not password and self.cleaned_data["password"]:
            raise forms.ValidationError(_("Whitespace is not allowed in this field."))
        return self.cleaned_data["password"]

    def clean(self):
        confirm_password = self.cleaned_data.get("confirm_password", "")
        password = self.cleaned_data.get("password", "")
        visibility = self.cleaned_data.get("visibility", "")
        if visibility == "O" and not password and not confirm_password:
            raise forms.ValidationError(_("Please enter the password and confirm the password."))
        if visibility == "O" and password and not confirm_password:
            raise forms.ValidationError(_("The confirm password field is required."))
        if visibility == "O" and password and password != confirm_password:
            raise forms.ValidationError(_("The password fields do not match."))
        if visibility != "O" and password:
            self.cleaned_data.pop("password", None)
            self.cleaned_data.pop("confirm_password", None)
        if visibility not in [ii[0] for ii in FILE_STATUS_CHOICES]:
            self.cleaned_data["visibility"] = "U"
        return self.cleaned_data

class FileUploadFormStep2(forms.ModelForm):
    class Meta:
        model = UserFile
        exclude = ('title', 'tags_string', 'description', 'visibility', 'password', 'project', 
                   'uuid', 'filename', 'source_type', 'current_type', 'page_count', 'duration',
                   'processed', 'is_active', 'file_size', 'copied_from', 'child_uuid', 'user')

class FileUploadFormWizard(FormWizard):
    def done(self, request, form_list):
        return render_to_response('files/files_upload.html', {
                'form_data': [form.cleaned_data for form in form_list],
                })

    def get_template(self, step):
        return ["files/files_upload_step_%s.html" % step]

    def prefix_for_step(self, step):
        return ''

class FileForm(forms.ModelForm):

   # title = forms.CharField()
   # company = forms.ChoiceField()
   # date = forms.SplitDateTimeField()
   tags_string = TagField(label=_(u"Tags"), required=False)
   visibility = forms.CharField(label=_(u"Visibility"), widget=forms.Select(choices=FILE_STATUS_CHOICES), required=False)
   password = forms.CharField(label=_(u"Password"), widget=forms.PasswordInput, required=False)
   confirm_password = forms.CharField(label=_(u"Confirm Password"), widget=forms.PasswordInput, required=False)

   class Meta:
       model = UserFile
       fields = ('title','description','visibility','tags_string', 'password')

   def __init__(self, user = None, *args, **kwargs):  
       self.user = user  
       super(FileForm, self).__init__(*args, **kwargs)

   def clean_visibility(self):
       visibility = self.cleaned_data["visibility"]
       if visibility not in [ii[0] for ii in FILE_STATUS_CHOICES]:
           return u"U"
       return visibility

   def clean_tags_string(self):
       tags_string = self.cleaned_data["tags_string"].strip()
       if not tags_string and self.cleaned_data["tags_string"]:
           raise forms.ValidationError(_("This field is required."))
       if tags_string:
           match_valid_tag = valid_tag_names.match(tags_string)
           if not match_valid_tag or not len(tags_string) == len(match_valid_tag.group()):
               raise forms.ValidationError(_("Only characters, digits, _ (underscore), "
                                             "- (hyphen), ',' (comma) and spaces allowed"
                                             " in a tag"))
       return tags_string


   def clean(self):
       data = self.cleaned_data
       if data["visibility"] == "O":
           password = data["password"].strip()
           if not password:
               raise forms.ValidationError(_("The password field is required."))
           if password != data["confirm_password"]:
               raise forms.ValidationError(_("Passwords do not match."))
       return data

#===============================================================================
# PRIVACY_CHOICES = (('private', 'Private'), ('pwd', 'Password'), ('re_pwd', 'Retype'), ('public', 'Public'))
# class FileForm(forms.Form):
#    file = forms.CharField()
#    description = forms.CharField(widget=forms.Textarea)
#    tags = forms.CharField(widget=forms.Textarea)
#    privacy = ChoiceField(widget=RadioSelect, choices=PRIVACY_CHOICES)
# #        super(FileForm, self).__init__(*args, **kwargs)
#===============================================================================
