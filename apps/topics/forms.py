from django import forms
from django.utils.translation import ugettext_lazy as _

from topics.models import Topic

class TopicForm(forms.ModelForm):
    
    class Meta:
        model = Topic
        fields = ('title', 'body', 'tags')

    def clean_title(self):
        title = self.cleaned_data["title"].strip()
        if not title:
            raise forms.ValidationError(_("This field is required."))
        return title

    def clean_body(self):
        body = self.cleaned_data["body"].strip()
        if not body and self.cleaned_data["body"]:
            raise forms.ValidationError(_("This field is required."))
        return body

    def clean_tags(self):
        tags = self.cleaned_data["tags"].strip()
        if not tags and self.cleaned_data["tags"]:
            raise forms.ValidationError(_("This field is required."))
        return tags

