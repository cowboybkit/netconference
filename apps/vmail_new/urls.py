from django.conf.urls.defaults import *
from django.views.generic.simple import direct_to_template, redirect_to
from vmail_new.views import *


urlpatterns = patterns('',
    url(r'^get$', test, name='vmail_new_get'),
    url(r'^getListFile$', getListFile, name='vmail_get_listfile'),
    url(r'^getsettings$', getsettings, name='vmail_getsettings'),
    url(r'^getLinkAmazon$', getLinkAmazon, name='vmail_getLinkAmazon'),
    url(r'^getContactList$', getContactList, name='vmail_getContactList'),
    url(r'^getListThemeTemplate$', getListThemeTemplate, name='vmail_getListThemeTemplate'),
    url(r'^getListEmailTemplate$', getListEmailTemplate, name='vmail_getListEmailTemplate'),
    url(r'^getHtmlEmailTemplate$', getHtmlEmailTemplate, name='vmail_getHtmlEmailTemplate'),
    url(r'^generateVmail$', generateVmail, name='vmail_generateVmail'),
    url(r'^getRequestUser$', getRequestUser, name='vmail_getRequestUser'),
    url(r'^getSubscription$', getSubscription, name='vmail_getSubscription'),
    url(r'^getAllowSpaceInfo$', getAllowSpaceInfo, name='vmail_getAllowSpaceInfo'),
    url(r'^wizardCompose$', compose, name='vmail_wizardCompose'),
    url(r'^getListCatalogue$', getListCatalogue, name='vmail_getListCatalogue'),
    url(r'^uploadtheme$', uploadTheme, name='vmail_uploadTheme'),
    url(r'^emailcontent$', emailcontent, name='vmail_emailcontent'),
    url(r'^email_template$', email_template, name='vmail_email_template'),
    url(r'^categoryManager$', categoryManager, name='vmail_categoryManager'),
    url(r'^sendforward$', sendforward, name='vmail_sendforward'),
    url(r'^contacts_autocomplete/$', contacts_autocomplete, name='vmail_contact_autocomplete'),
    url(r'^forward/(?P<urlhash>[-0-9A-Za-z_-]+)$', forward, name='vmail_forward'),
    url(r'^reply/(?P<urlhash>[-0-9A-Za-z_-]+)$', reply, name='vmail_new_reply'),
    url(r'^(?P<urlhash>[-0-9A-Za-z_-]+)$', viewVmail, name='vmail_viewVmail'),
    
)