# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding model 'EmailTemplateCategory'
        db.create_table('vmail_new_emailtemplatecategory', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(default='', max_length=250)),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal('vmail_new', ['EmailTemplateCategory'])

        # Adding model 'ScheduleDelivery'
        db.create_table('vmail_new_scheduledelivery', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('vmail', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['vmail_new.VM'])),
            ('type', self.gf('django.db.models.fields.CharField')(default='O', max_length=1)),
            ('send_onetime', self.gf('django.db.models.fields.DateTimeField')(null=True)),
            ('start', self.gf('django.db.models.fields.DateTimeField')(null=True)),
            ('end', self.gf('django.db.models.fields.DateTimeField')(null=True)),
            ('week_day', self.gf('django.db.models.fields.CharField')(default='Mon', max_length=3, null=True)),
            ('month_day', self.gf('django.db.models.fields.CharField')(default='01', max_length=2, null=True)),
            ('time_send', self.gf('django.db.models.fields.TimeField')(null=True, blank=True)),
            ('expired', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('vmail_new', ['ScheduleDelivery'])

        # Adding model 'EmailTemplate'
        db.create_table('vmail_new_emailtemplate', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(default='', max_length=250)),
            ('content', self.gf('django.db.models.fields.TextField')()),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['vmail_new.EmailTemplateCategory'])),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal('vmail_new', ['EmailTemplate'])

        # Deleting field 'VM.replied_at'
        db.delete_column('vmail_new_vm', 'replied_at')

        # Deleting field 'VM.recipient_deleted_at'
        db.delete_column('vmail_new_vm', 'recipient_deleted_at')

        # Deleting field 'VM.read_at'
        db.delete_column('vmail_new_vm', 'read_at')

        # Deleting field 'VM.sent_at'
        db.delete_column('vmail_new_vm', 'sent_at')

        # Deleting field 'VM.sender_deleted_at'
        db.delete_column('vmail_new_vm', 'sender_deleted_at')

        # Adding field 'VM.date_create'
        db.add_column('vmail_new_vm', 'date_create', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, default=None, blank=True), keep_default=False)

        # Adding field 'VM.email_template'
        db.add_column('vmail_new_vm', 'email_template', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['vmail_new.EmailTemplate'], null=True, blank=True), keep_default=False)


    def backwards(self, orm):
        
        # Deleting model 'EmailTemplateCategory'
        db.delete_table('vmail_new_emailtemplatecategory')

        # Deleting model 'ScheduleDelivery'
        db.delete_table('vmail_new_scheduledelivery')

        # Deleting model 'EmailTemplate'
        db.delete_table('vmail_new_emailtemplate')

        # Adding field 'VM.replied_at'
        db.add_column('vmail_new_vm', 'replied_at', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True), keep_default=False)

        # Adding field 'VM.recipient_deleted_at'
        db.add_column('vmail_new_vm', 'recipient_deleted_at', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True), keep_default=False)

        # Adding field 'VM.read_at'
        db.add_column('vmail_new_vm', 'read_at', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True), keep_default=False)

        # Adding field 'VM.sent_at'
        db.add_column('vmail_new_vm', 'sent_at', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True), keep_default=False)

        # Adding field 'VM.sender_deleted_at'
        db.add_column('vmail_new_vm', 'sender_deleted_at', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True), keep_default=False)

        # Deleting field 'VM.date_create'
        db.delete_column('vmail_new_vm', 'date_create')

        # Deleting field 'VM.email_template'
        db.delete_column('vmail_new_vm', 'email_template_id')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'files.userfile': {
            'Meta': {'ordering': "('-timestamp', 'title')", 'object_name': 'UserFile'},
            'child_uuid': ('django.db.models.fields.CharField', [], {'max_length': '36', 'null': 'True'}),
            'copied_from': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'file_copy_user'", 'null': 'True', 'to': "orm['auth.User']"}),
            'current_type': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'downloads_this_month': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'downloads_total': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'duration': ('django.db.models.fields.IntegerField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'file_size': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'filename': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'orig_file_id': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'page_count': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'}),
            'processed': ('django.db.models.fields.IntegerField', [], {'max_length': '2'}),
            'project': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'projectfile_set'", 'null': 'True', 'to': "orm['project.Project']"}),
            'source_type': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True', 'blank': 'True'}),
            'tags_string': ('tagging.fields.TagField', [], {}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"}),
            'uuid': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '36', 'blank': 'True'}),
            'views_this_month': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'views_total': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'visibility': ('django.db.models.fields.CharField', [], {'default': "'U'", 'max_length': '1'})
        },
        'project.project': {
            'Meta': {'object_name': 'Project'},
            'created_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': '1', 'blank': 'True'}),
            'end_date': ('django.db.models.fields.DateField', [], {'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'owner': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'project_owner'", 'to': "orm['auth.User']"}),
            'shortname': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '20'}),
            'start_date': ('django.db.models.fields.DateField', [], {})
        },
        'vmail_new.attachment': {
            'Meta': {'object_name': 'Attachment'},
            'file': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['files.UserFile']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'vmail': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['vmail_new.VM']"})
        },
        'vmail_new.bgcatalogue': {
            'Meta': {'object_name': 'bgcatalogue'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '36'})
        },
        'vmail_new.bgtemplate': {
            'Meta': {'object_name': 'bgtemplate'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'catalogue': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['vmail_new.bgcatalogue']"}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'from_image': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['files.UserFile']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '36'})
        },
        'vmail_new.emailtemplate': {
            'Meta': {'object_name': 'EmailTemplate'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['vmail_new.EmailTemplateCategory']"}),
            'content': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '250'})
        },
        'vmail_new.emailtemplatecategory': {
            'Meta': {'object_name': 'EmailTemplateCategory'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '250'})
        },
        'vmail_new.scheduledelivery': {
            'Meta': {'object_name': 'ScheduleDelivery'},
            'end': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'expired': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'month_day': ('django.db.models.fields.CharField', [], {'default': "'01'", 'max_length': '2', 'null': 'True'}),
            'send_onetime': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'start': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'time_send': ('django.db.models.fields.TimeField', [], {'null': 'True', 'blank': 'True'}),
            'type': ('django.db.models.fields.CharField', [], {'default': "'O'", 'max_length': '1'}),
            'vmail': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['vmail_new.VM']"}),
            'week_day': ('django.db.models.fields.CharField', [], {'default': "'Mon'", 'max_length': '3', 'null': 'True'})
        },
        'vmail_new.vm': {
            'Meta': {'ordering': "['-sent_at']", 'object_name': 'VM'},
            'StreamRecordId': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'audioRecordMode': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'avatarImages': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'Vimages'", 'null': 'True', 'to': "orm['files.UserFile']"}),
            'backgroundImage': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['files.UserFile']", 'null': 'True', 'blank': 'True'}),
            'backgroundTemplate': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['vmail_new.bgtemplate']", 'null': 'True', 'blank': 'True'}),
            'bcc_recipient': ('django.db.models.fields.TextField', [], {}),
            'cc_recipient': ('django.db.models.fields.TextField', [], {}),
            'checkDear': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'date_create': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'email_template': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['vmail_new.EmailTemplate']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'instance': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '36'}),
            'isRecord': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'issent': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'mColor': ('django.db.models.fields.CharField', [], {'default': "'e2cbe9'", 'max_length': '6'}),
            'mOpacity': ('django.db.models.fields.IntegerField', [], {'default': '100'}),
            'mSize': ('django.db.models.fields.CharField', [], {'default': "'200,100'", 'max_length': '10'}),
            'message': ('django.db.models.fields.TextField', [], {}),
            'messagePos': ('django.db.models.fields.CharField', [], {'default': "'250,600'", 'max_length': '10'}),
            'parent_msg': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'next_vmails'", 'null': 'True', 'to': "orm['vmail_new.VM']"}),
            'recipient': ('django.db.models.fields.TextField', [], {}),
            'sender': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']", 'null': 'True', 'blank': 'True'}),
            'subject': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'useThemeTemplate': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'videoPos': ('django.db.models.fields.CharField', [], {'default': "'600,650'", 'max_length': '10'})
        }
    }

    complete_apps = ['vmail_new']
