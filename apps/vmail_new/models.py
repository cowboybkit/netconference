from django.db import models
import datetime
from datetime import *
from django.conf import settings
from django.db.models import signals
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from uuid import uuid4
from files.models import UserFile
#from django.core.mail import EmailMultiAlternatives
from django.template import loader, Context
from django.db.models import Count
#from ccall_pins.models import ConferencePin
from django.template.loader import render_to_string
from mailer import send_mail, send_html_mail
from django.contrib.sites.models import Site
#from django.contrib.admindocs.views import verbose
from friends.models import Contact
from netconf_utils.encode_decode import *
from django.utils.encoding import force_unicode
from mailer.models import make_message
from django.forms.fields import email_re

from HTMLParser import HTMLParser

class MLStripper(HTMLParser):
    def __init__(self):
        self.reset()
        self.fed = []
    def handle_data(self, d):
        self.fed.append(d)
    def get_data(self):
        return ''.join(self.fed)

def strip_tags(html):
    s = MLStripper()
    s.feed(html)
    return s.get_data()

def is_valid_email(email):
    return True if email_re.match(email) else False


def send_mutimedia_vmail(sender, to_list , cc_list, bcc_list, subject, email_message):
        return send_html_mail(subject = subject, message = email_message, message_html =email_message, from_email = sender, recipient_list = to_list, cc_list= cc_list, bcc_list = bcc_list)   
    

class bgcatalogue(models.Model):
    name = models.CharField(max_length=255, blank=False)
    active = models.BooleanField(null=False,blank=False,default=True)

class bgtemplateManager(models.Manager):

    def getListVmailTheme(self,cat):
        theme = self.all().filter(active=True,catalogue = cat)
        for t in theme:
            t.description =  u"https://s3.amazonaws.com/%s/files/%s.%s" %(settings.AWS_STORAGE_BUCKET_NAME,t.from_image.uuid,t.from_image.source_type)
#            t.description = t.from_image.get_amazonS3_url()
        return theme
    
class bgtemplate(models.Model):
    """ Theme use background for SuperVmail """
    
    name = models.CharField(max_length=36, blank=False)
    description = models.CharField(max_length=255, blank=True, null=True)
    from_image = models.ForeignKey(UserFile, verbose_name=_("Images"))
    catalogue = models.ForeignKey(bgcatalogue, verbose_name=_("catalogue"))
    active = models.BooleanField(blank=False,null=False,default=True)
    
    objects = bgtemplateManager()


class EmailTemplateCategory(models.Model):
    name = models.CharField(_('name category'), max_length=250, default = '')
    active = models.BooleanField(null=False,blank=False,default=True)

class EmailTemplate(models.Model):
    name = models.CharField(_('name email template'), max_length=250, default = '')
    thumb = models.ForeignKey(UserFile,verbose_name= _('thumb of template'),null=True, blank=True)
    content = models.TextField(_("html of Email"))
    filename = models.CharField(_('filename render'), max_length=255, default = 'template_default.html')
    category = models.ForeignKey(EmailTemplateCategory, verbose_name=_("Category of template"), null=False, blank=False) 
    description = models.CharField(_('thumb link email template'), max_length=250, default = '')
    active = models.BooleanField(null=False,blank=False,default=True)
    default  = models.BooleanField(null=False,blank=False,default=False)


class VMAILCLONE():
    
    def __init__(self, vmail = None):
        if vmail is not None:
            urlBackground = ""
            if not vmail.useThemeTemplate:
                if vmail.backgroundImage:
                   urlBackground = vmail.backgroundImage.get_amazonS3_url()
            else:
                if vmail.backgroundTemplate:
                   urlBackground = vmail.backgroundTemplate.from_image.get_amazonS3_url()
            if not urlBackground:
                urlBackground = 'http://netconference.com/site_media/vmail/themes/netconference/images/background_default.png'
            
            background_uuid = 'background_default'
            background_source = 'png'
            if not vmail.useThemeTemplate:
                if vmail.backgroundImage:
                   background_uuid = vmail.backgroundImage.uuid
                   background_source = vmail.backgroundImage.source_type
            else:
                if vmail.backgroundTemplate:
                   background_uuid = vmail.backgroundTemplate.from_image.uuid
                   background_source = vmail.backgroundTemplate.from_image.source_type
                
            
            ''' message position '''
            mPos = vmail.messagePos.split(',')
            mArr = [int(float(x)) for x in mPos]
            ''' video position '''
            vPos = vmail.videoPos.split(',')
            vArr = [int(float(y)) for y in vPos]
            ''' message size '''
            mSize = vmail.mSize.split(',')
            mSizeArr = [int(z) for z in mSize]
            
            ''' initialize recipient '''
            to_rep = vmail.recipient.strip().split(", ")
            to_list = [x for x in to_rep if x ]
            cc_rep = vmail.cc_recipient.strip().split(", ")
            cc_list = [y for y in cc_rep if y ]
            bcc_rep = vmail.bcc_recipient.strip().split(", ")
            bcc_list = [y for y in bcc_rep if y ]
    
            to_group = vmail.to_group.strip().split(", ")
            to_group_list = [int(x) for x in to_group if x ]
            for t_grp in to_group_list:
                t_contacts = Contact.objects.filter(user = vmail.sender, groups__id = t_grp)
                for t_con in t_contacts:
                    if t_con.email not in to_list and is_valid_email(t_con.email):
                        to_list.append(t_con.email)
            
            bcc_group = vmail.bcc_group.strip().split(", ")
            bcc_group_list = [int(x) for x in bcc_group if x ]
            for b_grp in bcc_group_list:
                b_contacts = Contact.objects.filter(user = vmail.sender, groups__id = b_grp)
                for b_con in b_contacts:
                    if b_con.email not in bcc_list  and is_valid_email(b_con.email):
                        bcc_list.append(b_con.email)
            
            cc_group = vmail.cc_group.strip().split(", ")
            cc_group_list = [int(x) for x in cc_group if x ]
            for c_grp in cc_group_list:
                c_contacts = Contact.objects.filter(user = vmail.sender, groups__id = c_grp)
                for c_con in c_contacts:
                    if c_con.email not in cc_list  and is_valid_email(c_con.email):
                        cc_list.append(c_con.email)
            
            ''' video thumbnail '''
            video_thumbnail  = None
            if vmail.audioRecordMode:
                if vmail.avatarImages:
                    video_thumbnail =  vmail.avatarImages.get_amazonS3_url().split('?Signature')[0]
            else:
                video_thumbnail = 'https://s3.amazonaws.com/'+settings.AWS_STORAGE_BUCKET_NAME+'/thumb/'+vmail.StreamRecordId+'.jpg'
                
            if vmail.capture_thumbnail:
                video_thumbnail = 'https://s3.amazonaws.com/'+settings.AWS_STORAGE_BUCKET_NAME+'/thumb/'+vmail.capture_thumbnail+'.jpg'
            ''' render html email '''
            if vmail.header_image:
                header_img = vmail.header_image.get_amazonS3_url().split('?Signature')[0]
            else:
                header_img = None
            linksite =  u"http://%s/" % (unicode(Site.objects.get_current()))
            urlhash = uri_b64encode(str(vmail.id * 42))
            attachment = Attachment.objects.filter(vmail = vmail)
            name_dear = None
            if  vmail.checkDear:
                if len(to_list) == 1:
                    contact_dear = Contact.objects.filter(user = vmail.sender, email__iexact = to_list[0])
                    if contact_dear:
                        name_dear = contact_dear[0].name
                else:
                    name_dear = 'all'
            user = vmail.sender
            context_template = {
                "message":vmail.email_message,
                "urlhash": urlhash,
                "attachment":attachment,
                "bucket":settings.AWS_STORAGE_BUCKET_NAME,
                'theme': 'netconference',
                'linksite':linksite,
                'header_image':header_img,
                'user':user,
                'name_dear':name_dear
            }
            context = Context(context_template)
            template_name = 'vmail_new/email_template/%s'%vmail.email_template.filename
            html_part = loader.get_template(template_name).render(context)
            
#            f = open('/home/cowboybkit/hung.html','w')
#            f.write(html_part)
#            f.close()
            
            self.id = vmail.id
            self.date_create = vmail.date_create
            self.subject = vmail.subject
            self.message_left = mArr[0]
            self.message_top = mArr[1]
            self.video_left = vArr[0]
            self.video_top = vArr[1]
            self.message_width = mSizeArr[0]
            self.message_height = mSizeArr[1]
            self.message_color = vmail.mColor
            self.message_opacity = vmail.mOpacity
            self.message = vmail.message
            self.message_text = strip_tags(vmail.message)
            self.email_message = vmail.email_message
            self.sender = vmail.sender
            self.to = to_list
            self.bcc = bcc_list
            self.cc = cc_list
            self.isRecord = vmail.isRecord
            self.audioRecordMode = vmail.audioRecordMode
            self.StreamRecordId = vmail.StreamRecordId
            self.video_thumbnail = video_thumbnail
            self.background = urlBackground
            self.background_uuid = background_uuid
            self.background_source = background_source        
            self.html_part = html_part
            self.isNoVideo = vmail.isNoVideo
            self.capture_thumbnail = vmail.capture_thumbnail
            

class VMManager(models.Manager):

    def inbox_for(self, user):
        """
        Returns all messages that were received by the given user and are not
        marked as deleted.
        """
        return self.filter(
            recipient=user,
        )


        

class VM(models.Model):
    """
    A SuperVmail message
    """
    date_create = models.DateTimeField(_("create at"), auto_now_add=True)
    subject = models.CharField(_("Subject"), max_length=120)
    message = models.TextField(_("Message"))
    email_message = models.TextField(_("Email Message"))
    sender = models.ForeignKey(User, verbose_name=_("Sender"), null=True, blank=True)
    recipient = models.TextField(_("To: Email Address"))
    cc_recipient = models.TextField(_("Cc: Email Address"))
    bcc_recipient = models.TextField(_("Bcc: Email Address"))
    
    to_group = models.TextField(_("To: Email group"))
    bcc_group = models.TextField(_("Cc: Email group"))
    cc_group = models.TextField(_("Bcc: Email group"))
    
    parent_msg = models.ForeignKey('self', related_name='next_vmails', null=True, blank=True, verbose_name=_("Parent v-mail"))
    
    issent = models.BooleanField(_("sent vmail"),blank=False,null=False,default=False)
    
    isRecord = models.BooleanField(_("record or choose"),blank=False,null=False,default=True)
    audioRecordMode = models.BooleanField(_("Use audio image"), null=False)
    StreamRecordId = models.CharField(_("StreamRecordId"),max_length=255)
    avatarImages = models.ForeignKey(UserFile,related_name='Vimages', verbose_name=_('Image Avatar'), null=True, blank=True)
    
    useThemeTemplate = models.BooleanField(null=False,blank=False)
    backgroundImage = models.ForeignKey(UserFile,null = True, blank=True)
    backgroundTemplate = models.ForeignKey(bgtemplate,null = True, blank=True)
    
    messagePos = models.CharField(max_length=10, default='250,600')
    videoPos = models.CharField(max_length=10, default='600,650')
    
    mOpacity = models.IntegerField(null=False,blank=False, default=100)
    mColor = models.CharField(max_length=6,default='e2cbe9')
    mSize = models.CharField(max_length=10, default='200,100')
    
    checkDear = models.BooleanField(_("check Dear vmail"),null=False,blank=False,default=True)
    email_template = models.ForeignKey(EmailTemplate, verbose_name=_("Email Template"), null=True, blank=True)
    header_image = models.ForeignKey(UserFile,related_name=("Header Image"), verbose_name=_('Header Image'),null = True, blank=True)
    isNoVideo = models.BooleanField(_("isNoVideo"),blank=False,null=False,default=False)
    capture_thumbnail = models.CharField(_("capture_thumbnail"),max_length=255, null = True, default =True)
    objects = VMManager()
    
  
    def __unicode__(self):
        return self.subject
        return '%s, %s: %s' % (self.sender, self.subject)
   
    def save(self, force_insert=False, force_update=False):
        super(VM, self).save(force_insert, force_update) 
    def own_send(self):
        address = self.recipient.strip().split(", ")
        to_list = [x for x in address if x ]
        cc_rep = self.cc_recipient.strip().split(", ")
        cc_list = [y for y in cc_rep if y ]
        bcc_rep = self.bcc_recipient.strip().split(", ")
        bcc_list = [y for y in bcc_rep if y ]
 
        to_group = self.to_group.strip().split(", ")
        to_group_list = [int(x) for x in to_group if x ]
        for t_grp in to_group_list:
            t_contacts = Contact.objects.filter(user = self.sender, groups__id = t_grp)
            for t_con in t_contacts:
                if t_con.email not in to_list and is_valid_email(t_con.email):
                    to_list.append(t_con.email)
        
        bcc_group = self.bcc_group.strip().split(", ")
        bcc_group_list = [int(x) for x in bcc_group if x ]
        for b_grp in bcc_group_list:
            b_contacts = Contact.objects.filter(user = self.sender, groups__id = b_grp)
            for b_con in b_contacts:
                if b_con.email not in bcc_list  and is_valid_email(b_con.email):
                    bcc_list.append(b_con.email)
        
        cc_group = self.cc_group.strip().split(", ")
        cc_group_list = [int(x) for x in cc_group if x ]
        for c_grp in cc_group_list:
            c_contacts = Contact.objects.filter(user = self.sender, groups__id = c_grp)
            for c_con in c_contacts:
                if c_con.email not in cc_list  and is_valid_email(c_con.email):
                    cc_list.append(c_con.email)
        
        if self.header_image:
            header_img = self.header_image.get_amazonS3_url().split('?Signature')[0]
        else:
            header_img = None
        linksite =  u"http://%s/" % (unicode(Site.objects.get_current()))
        urlhash = uri_b64encode(str(self.id * 42))
        attachment = Attachment.objects.filter(vmail = self)
        name_dear = None
        if  self.checkDear:
            if len(to_list) == 1:
                contact_dear = Contact.objects.filter(user = self.sender, email__iexact = to_list[0])
                if contact_dear:
                    name_dear = contact_dear[0].name
            else:
                name_dear = 'all'
        
        user = self.sender
        context_template = {
            "message":self.email_message,
            "urlhash": urlhash,
            "attachment":attachment,
            "bucket":settings.AWS_STORAGE_BUCKET_NAME,
            'theme': 'netconference',
            'linksite':linksite,
            'header_image':header_img,
            'user':user,
            'name_dear':name_dear
        }
        context = Context(context_template)
        template_name = 'vmail_new/email_template/%s'%self.email_template.filename
        html_part = loader.get_template(template_name).render(context)
        send_html_mail(subject = self.subject, message = self.email_message, message_html = html_part, from_email = self.sender.email, recipient_list = to_list, cc_list= cc_list, bcc_list = bcc_list)

#        clone_instant = VMAILCLONE(self)
#        send_html_mail(subject = clone_instant.subject, message = clone_instant.email_message, message_html = clone_instant.html_part, from_email = clone_instant.sender.email, recipient_list = clone_instant.to, cc_list= clone_instant.cc, bcc_list = clone_instant.bcc)
    
    class Meta:
        verbose_name = _("SuperVmail")
        verbose_name_plural = _("SuperVmails")
        
class Attachment(models.Model):
    vmail = models.ForeignKey(VM,verbose_name=_("Of Vmail"), null=False, blank=False)
    file = models.ForeignKey(UserFile,verbose_name=_("UserFile"), null=False, blank=False )
    
class ScheduleDelivery(models.Model):
    vmail = models.ForeignKey(VM,verbose_name=_("Of Vmail"), null=False, blank=False)    
    type = models.CharField(_('Non Schedule'), max_length=1, default="O")
    send_onetime = models.DateTimeField(_("send only one"), null=True)
    start = models.DateField(_("Start Schedule"), null=True)
    end = models.DateField(_("End Schedule"), null=True)
    week_day = models.CharField(_('day of week'), max_length=3, null=True, default='Mon')
    month_day = models.CharField(_('day of month'), max_length=10,null=True, default='01')
    time_send = models.TimeField(_('time to send only one'), null=True)
    time_zone = models.CharField(_('timezone'),max_length=30, null=False, default='-08:00')
    expired = models.BooleanField(_("expired "), null=False, blank = False, default = False)


class EmailMessage():
    def __init__(self, row):
        self.vmail_id = row[0]
        self.sender_id = row[1]
        self.sender= row[12]
        self.checkDear= row[11]
        if row[2]:
            address = str(row[2]).strip().split(", ")
            to_list = [x for x in address if x ]
        else:
            to_list = []
        self.to = to_list
        if row[3]:
            address = str(row[3]).strip().split(", ")
            cc_list = [x for x in address if x ]
        else:
            cc_list = []
        self.cc = cc_list
        if row[4]:
            address = str(row[4]).strip().split(", ")
            bcc_list = [x for x in address if x ]
        else:
            bcc_list = []
        self.bcc = bcc_list
        
        
        to_group = row[8].strip().split(", ")
        to_group_list = [int(x) for x in to_group if x ]
        for t_grp in to_group_list:
            t_contacts = Contact.objects.filter(user__id = self.sender_id, groups__id = t_grp)
            for t_con in t_contacts:
                if t_con.email not in self.to  and is_valid_email(t_con.email):
                    self.to.append(t_con.email)
        
        bcc_group = row[9].strip().split(", ")
        bcc_group_list = [int(x) for x in bcc_group if x ]
        for b_grp in bcc_group_list:
            b_contacts = Contact.objects.filter(user__id = self.sender, groups__id = b_grp)
            for b_con in b_contacts:
                if b_con.email not in self.bcc  and is_valid_email(b_con.email):
                    self.bcc.append(b_con.email)
        
        cc_group = row[10].strip().split(", ")
        cc_group_list = [int(x) for x in cc_group if x ]
        for c_grp in cc_group_list:
            c_contacts = Contact.objects.filter(user__id = self.sender_id, groups__id = c_grp)
            for c_con in c_contacts:
                if c_con.email not in self.cc  and is_valid_email(c_con.email):
                    self.cc.append(c_con.email)
                    
#        f = open('/home/cowboybkit/hung.html','w')
#        f.write(','.join(self.to) or '')
#        f.write('\n')
#        f.write(','.join(self.bcc) or '')
#        f.write('\n')
#        f.write(','.join(self.cc) or '')
        
        self.subject =  row[5]
        self.raw_message = row[6]
        self.header_image = row[7]
        
        vmail = VM.objects.get(pk = self.vmail_id)
        
        if self.header_image:
            header = UserFile.get(pk=int(self.header_image))
            header_img = vmail.header.get_amazonS3_url().split('?Signature')[0]
        else:
            header_img = None
            
        linksite =  u"http://%s/" % (unicode(Site.objects.get_current()))
        urlhash = uri_b64encode(str(vmail.id * 42))
        attachment = Attachment.objects.filter(vmail = vmail)
        
        name_dear = None
        if  self.checkDear:
            if len(self.to) == 1:
                contact_dear = Contact.objects.filter(user__id = self.sender_id, email__iexact = self.to[0])
                if contact_dear:
                    name_dear = contact_dear[0].name
            else:
                name_dear = 'all'
                
        user = User.objects.get(pk=int(self.sender_id))
        
        context_template = {
            "message": self.raw_message,
            "urlhash": urlhash,
            "attachment":attachment,
            "bucket":settings.AWS_STORAGE_BUCKET_NAME,
            'theme': 'netconference',
            'linksite':linksite,
             'header_image':header_img,
             'user':user,
             'name_dear':name_dear
        }
        context = Context(context_template)
        template_name = 'vmail_new/email_template/%s'%vmail.email_template.filename
        html_part = loader.get_template(template_name).render(context)
        self.email_message = html_part
        
#        f = open('/home/cowboybkit/hung.html','w')
#        f.write(html_part)
#        f.close()
        
COLUMNS = [ 'vmail_id',
            'sender_id',
            'recipient',
            'cc_recipient',
            'bcc_recipient',
            'subject',
            'email_message',
            'header_image_id',
            'to_group',
            'bcc_group',
            'cc_group',
            'checkDear'
           ]        
def make_sql_query(minutes):
    from django.db import connection, transaction
    cursor = connection.cursor()
    commonstr = ','.join(COLUMNS)
#    time_zone_str = adjust_datetime_to_timezone(datetime.datetime.now(), settings.TIME_ZONE, settings.TIME_ZONE).strftime('%z')
#    time_zone = time_zone_str[:3]+':'+time_zone_str[3:]
    command = render_to_string("vmail_new/sql_query_schedule.txt", {
            "common_string": commonstr,
            "minutes": str(minutes),
#            "timezone":time_zone
        })
#    f = open('/home/cowboybkit/hung.html','w')
#    f.write(command)
    cursor.execute(command)
    transaction.commit_unless_managed()
    row = cursor.fetchall()
    return row
def build_email_tosend(minutes):
    rows = make_sql_query(minutes)
    emails = []
    if rows:
        for r in rows:
            email = EmailMessage(r)
            emails.append(email)
    return emails

def cronTaskrun(minutes):
    emails = build_email_tosend(minutes)
    for email in emails:
        send_html_mail(subject = email.subject, message = email.raw_message, message_html =email.email_message, from_email = email.sender, recipient_list = email.to, cc_list= email.cc, bcc_list = email.bcc)
    