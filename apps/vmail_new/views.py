from django.db import models
from django.db.models import Q
from django.core import serializers
from django.http import HttpResponse, Http404
from django.template import RequestContext
from django.contrib.auth.models import User
from files.models import UserFile
import django.contrib.sessions
import json
from account.views import *
from vmail_new.models import *
from django.shortcuts import render_to_response
from django.contrib.auth.decorators import login_required
from django.views.generic.simple import direct_to_template
from netconf_utils.encode_decode import uri_b64encode, uri_b64decode, get_object_from_hash
from django.contrib.sites.models import Site
import datetime
from django.http import HttpResponseRedirect
from django.conf import settings
from uuid import uuid4
from netconf_utils.amazon_s3 import *
from os.path import basename
import dateutil.parser
import time as _time
devMode = True
from django.template.loader import render_to_string
from django.views.generic.list_detail import object_list
from friends.models import Contact
from friends_app.models import ContactGroup
from django.core.urlresolvers import reverse
import urllib2
import urllib


class MLStripper(HTMLParser):
    def __init__(self):
        self.reset()
        self.fed = []
    def handle_data(self, d):
        self.fed.append(d)
    def get_data(self):
        return ''.join(self.fed)

def strip_tags(html):
    s = MLStripper()
    s.feed(html)
    return s.get_data()


def test(request):
    response = HttpResponse(mimetype="application/json")
    id = request.session['_auth_user_id']
    usr = authenticate_with_session(id)
    if usr is not None:
        data = serializers.serialize("json", [usr])
    response.write(data)
    return response
def datetimeServer(str):
    value = dateutil.parser.parse(str)
    off = value.utcoffset() + datetime.timedelta(seconds = _time.timezone)
    return (value - off).replace(tzinfo = None)

def timeServer(str):
    value = dateutil.parser.parse(str)
    off = value.utcoffset() + datetime.timedelta(seconds = _time.timezone)
    return (value - off).replace(tzinfo = None).time()

def compose(request,template_name='vmail_new/compose.html'):
    if request.method=='POST':
        return direct_to_template(request,template_name,{'idReply':request.POST['idReply']})
    return direct_to_template(request,template_name)

compose = login_required(compose)


def reply(request,urlhash):
    try:
        id = int(uri_b64decode(str(urlhash))) / 42
    except:
        raise Http404
    return direct_to_template(request,'vmail_new/reply.html',{'idReply':id})

reply = login_required(reply)

def forward(request,urlhash):
    vmail = get_object_from_hash(VM, urlhash)
    urlBackground = ""
    if not vmail.useThemeTemplate:
        if vmail.backgroundImage:
           urlBackground = vmail.backgroundImage.get_amazonS3_url()
    else:
        if vmail.backgroundTemplate:
           urlBackground = vmail.backgroundTemplate.from_image.get_amazonS3_url()
    
    
    avatar  = None
    if vmail.audioRecordMode:
        if vmail.avatarImages:
            avatar =  vmail.avatarImages.get_amazonS3_url().split('?Signature')[0]
            
    email = vmail.sender.email
    
    mPos = vmail.messagePos.split(',')
    mArr = [int(float(x)) for x in mPos]
    
    vPos = vmail.videoPos.split(',')
    vArr = [int(float(y)) for y in vPos]
    
    mSize = vmail.mSize.split(',')
    mSizeArr = [int(z) for z in mSize]
    
    return direct_to_template(request,'vmail_new/forward.html',
                              {'object':vmail,'urlBackground':urlBackground,
                               'avatar':avatar,'email':email,
                               'mPos_left':mArr[0],'mPos_top':mArr[1],
                               'vPos_left':vArr[0],'vPos_top':vArr[1],
                               'mSize_weight':mSizeArr[0],'mSize_height':mSizeArr[1],
                               'hashcode':urlhash,
                               'CLOUDFRONT_STREAM':settings.CLOUDFRONT_STREAM,
                            'CLOUDFRONT_DOWNLOAD':settings.CLOUDFRONT_DOWNLOAD,
                            'VMAIL_RECORDING_STREAM':settings.VMAIL_RECORDING_STREAM,
                             'VMAIL_FILE_STREAM':settings.VMAIL_FILE_STREAM,
                               })
        
forward = login_required(forward)
    
def sendforward(request):
    if request.method=="POST":
        urlhash = request.POST['hashcode']
        vmail = get_object_from_hash(VM, urlhash)
        
        if devMode:
            attachment = [Attachment.objects.get(pk=2),Attachment.objects.get(pk=3),Attachment.objects.get(pk=4)]
        else:
            attachment = Attachment.objects.filter(vmail = vmail)
            
        vmail.id=None
        vmail.sender = request.user
        vmail.recipient = request.POST['To_Address']
        vmail.cc_recipient = request.POST['Cc_Address']
        vmail.bcc_recipient = request.POST['Bcc_Address']
        vmail.subject = request.POST['subject']
        vmail.issent = True
        vmail.email_message = request.POST['messageContent']
        vmail.save()
        
        for attach in attachment:
            attach.id =None
            attach.vmail = vmail
            attach.save()
        vmail.own_send()
        return HttpResponseRedirect('/vmail/'+request.POST['hashcode'])


def getListFile(request):
    response = HttpResponse(mimetype="application/json")
    response['Access-Control-Allow-Origin'] = '*'
    types = []
    if request.method == 'POST':
        types = json.loads(request.POST['param'])
    if devMode:
        id = 327
    else:
        id = request.session['_auth_user_id']
    if not types:
        query = UserFile.active_objects.filter(user=id).order_by('title')
    else:
        if 'flv' in types:
            query = UserFile.active_objects.filter(user=id,current_type__in= ['mp4','flv']).exclude(source_type='rec_conf').order_by('title')
        elif 'all' in types:
            query = UserFile.active_objects.filter(user=id).exclude(source_type__in=['rec_conf','record_vmail']).order_by('title')
        else:
            query = UserFile.active_objects.filter(user=id,source_type__in= types).order_by('title')
    
    data = serializers.serialize("json",query)
    
    response.write(data)
    return response

if not devMode:
    getListFile = login_required(getListFile)
    
def getLinkAmazon(request):
    response = HttpResponse(mimetype="text/plain")
    response['Access-Control-Allow-Origin'] = '*'
    if request.method == 'POST':
        file_id = request.POST['param']
    else:
        file_id = request.GET['param']
    try:
        fid = int(file_id)
        if not devMode:
            ufile = UserFile.active_objects.get(id=fid)
        else:
            ufile = UserFile.active_objects.get(id=4394)
        url = ufile.get_amazonS3_url()
    except:
        if not devMode:
            ufile = UserFile.active_objects.get(uuid=file_id)
            url = ufile.get_amazonS3_url().split('?Signature')[0]
        else:
            url = 'https://s3.amazonaws.com/beta_netconference/files/316bb420-2ce1-432f-964e-2e52402bede0.jpg'
    response.write(url)
    return response
        

if not devMode:
    getLinkAmazon = login_required(getLinkAmazon)

def getContactList(request):
    response = HttpResponse(mimetype="application/json")
    response['Access-Control-Allow-Origin'] = '*'
    cat = json.loads(request.POST['param'])
    if devMode:
        id = 327
        usr = authenticate_with_session(id)
    else:
        usr = request.user
    contacts = None
    if cat==0:
        contacts = Contact.objects.filter(user = usr, groups = None)
    else:
        contacts = Contact.objects.filter(user = usr, groups__id = int(cat))
    response.write(serializers.serialize("json",contacts))
    return response

if not devMode:
    getContactList = login_required(getContactList)

def generate_thumbnail(vmail):
    params = {}
    clone_instant = VMAILCLONE(vmail)
    
    params['background_uuid'] = clone_instant.background_uuid
    params['background_source'] = clone_instant.background_source
    params['message_left'] = clone_instant.message_left
    params['message_top'] = clone_instant.message_top
    params['message_width'] = clone_instant.message_width
    params['message_height'] = clone_instant.message_height
    params['message_color'] = clone_instant.message_color
    params['message_opacity'] = clone_instant.message_opacity 
    params['message_text'] = clone_instant.message_text
    
    params['bucketname'] = settings.AWS_STORAGE_BUCKET_NAME
    params['outputname'] = uri_b64encode(str(clone_instant.id * 42))
    params['thumb_key'] = 'thumb' 
    params['background_key'] = 'files'
    
    params['isvideo'] = not clone_instant.isNoVideo
    params['video_width'] = 448
    params['video_height'] = 336
    
    params['video_thumb_link'] = ''
    params['video_left'] = None
    params['video_top'] = None
    if not clone_instant.isNoVideo:
        params['video_thumb_link'] = clone_instant.video_thumbnail
        params['video_left'] = clone_instant.video_left
        params['video_top'] = clone_instant.video_top
    
    params['thumb_key1'] = 'vmail/images'
    params['thumb_key2'] = 'vmail/thumb'
    params['scale_width'] = 480
    params['scale_height'] = 320
    
    encode_params = urllib.urlencode(params)
    req = urllib2.urlopen(settings.URL_GENERATE_VMAIL_THUMB+'?'+encode_params).read()
    return req
    
def generate_capture_video(bucket, uuid, catchtime, path):
    params = {}
    params['bucket'] = bucket
    params['uuid'] = uuid
    params['catchtime'] = catchtime
    params['path'] = path
    encode_params = urllib.urlencode(params)
#    response = urllib2.urlopen('http://encode1.media.netconf.nxtgencdn.com/uploader_amazons3/upload-thumbnail-vmail.php?'+encode_params).read()
    response = urllib2.urlopen(settings.URL_GENERATE_VIDEOTHUMB_BYTIME+'?'+encode_params).read()
    return response 
    
def generateVmail(request):
    if request.method =='POST':
        data  = json.loads(request.POST['param'])
    else:
        raise Http404
    
    response = HttpResponse(mimetype="application/json")
    response['Access-Control-Allow-Origin'] = '*'
    if devMode:
        id = 327
    else:
        id = request.session['_auth_user_id']
    usr = authenticate_with_session(id)
    
    if not data[u'avatarImage']:
        avatar = None
    else:
        avatar = UserFile.objects.get(uuid=data[u'avatarImage'])
  
    if not data[u'useThemeTemplate'] :
        if not data[u'themeFileId']:
            img = None
        else:
            if devMode:
                img = UserFile.objects.get(id=4275)
            else:
                img = UserFile.objects.get(id=data[u'themeFileId'])
        template = None
    else:
        img= None
        template = bgtemplate.objects.get(id=data[u'themeFileId'])
   
    
    to_address = [x for x in list(data[u'recipientAddress']) if x ]
    rep = ', '.join(to_address) or ''
    
    bcc_address = [x for x in list(data[u'bcc_recipientAddress']) if x ]
    bcc_rep = ', '.join(bcc_address) or ''
    
    cc_address = [x for x in list(data[u'cc_recipientAddress']) if x ]
    cc_rep = ', '.join(cc_address) or ''
        
    to_grp = [x for x in list(data[u'to_group']) if x ]
    to_group = ', '.join(to_grp) or ''
    
    bcc_grp = [x for x in list(data[u'bcc_group']) if x ]
    bcc_group = ', '.join(bcc_grp) or ''
    
    cc_grp = [x for x in list(data[u'cc_group']) if x ]
    cc_group = ', '.join(cc_grp) or ''
    
    
    messagePos = ','.join(str(int(x)) for x in data[u'messagePosition']) or ''
    videoPos = ','.join(str(int(x)) for x in data[u'videoPosition']) or ''
    messageSize = ','.join(str(int(x)) for x in data[u'messageSize']) or ''
    
    if data[u'parentVmail']>0:
        parentVmail = VM.objects.get(pk=data[u'parentVmail'])
    else:
        parentVmail = None
    if int(data[u'emailtemplateID']) == -1:
        et = EmailTemplate.objects.filter(default = True)[0]
    else:
        et = EmailTemplate.objects.get(pk=int(data[u'emailtemplateID']))
        
    
    header_image = None
    if data[u'header_image'] != '':
        if devMode:
            u_file_header= UserFile.objects.filter(uuid = '3208031a8b58480748888a3e711a8719')
            if u_file_header:
                header_image = u_file_header[0]
        else:
            u_file_header = UserFile.objects.filter(uuid=data[u'header_image'])
            if u_file_header:
                header_image = u_file_header[0]
    
    second_capture = None
    path = None
    uuid_video = None
    bucket_video = settings.AWS_STORAGE_BUCKET_NAME
    if data['second_capture'] !=-1:
        second_capture = data['second_capture']
    if second_capture and not data[u'novideo']:
        if data[u'record'] :
            if not data[u'audioRecordMode']:
                path = 'flv/recording'
                uuid_video  = data[u'StreamRecordId']
        else:
            path = 'mp4'
            uuid_video  = data[u'StreamRecordId']+'_500' 
    
    capture_thumbnail = None
    if second_capture  and uuid_video and path:
        capture_thumbnail = generate_capture_video(bucket_video, uuid_video, second_capture, path)
        
    vmail = VM(subject=data[u"vmailName"],
               message = data[u'messageContent'],
               email_message = data[u'email_message'],
               sender = usr,
               recipient = rep,
               bcc_recipient  =bcc_rep,
               cc_recipient  =cc_rep,
               to_group = to_group,
               bcc_group  =bcc_group,
               cc_group  =cc_group,
               parent_msg = parentVmail,
               issent = data[u'sendNow'],
               isRecord = data[u'record'],
               audioRecordMode = data[u'audioRecordMode'],
               StreamRecordId = data[u'StreamRecordId'],
               avatarImages = avatar,
               useThemeTemplate = data[u'useThemeTemplate'],
               backgroundImage = img,
               backgroundTemplate = template,
               messagePos = messagePos,
               videoPos = videoPos,
               mOpacity = data[u"opacity"],
               mColor = data[u"colorMessage"],
               mSize = messageSize,
               checkDear = data[u"checkDear"],
               email_template = et,
               header_image = header_image,
               isNoVideo = data[u'novideo'],
               capture_thumbnail = capture_thumbnail
               )
    vmail.save()
    
    if data[u'attachUUID']:
       if devMode:
           uuid_list = ['3208031a8b58480748888a3e711a8719','15b0700876b95f776564fd321b2f3098']
       else:
           uuid_list = list(data[u'attachUUID'])
           
       for uuid_att in uuid_list:
           a_file = UserFile.objects.get(uuid=str(uuid_att))
           attachment = Attachment(
                                   vmail =  vmail,
                                   file = a_file
                                   )
           attachment.save()
            
    urlhash = uri_b64encode(str(vmail.id * 42))
    vmail_url = u"http://%s/vmail/%s" % (
            unicode(Site.objects.get_current()),
            urlhash,
        )

    if data[u'sendNow']:
        vmail.own_send()
    else:
        type_schedule = str(data[u'TypeSchedule'])
        
        if type_schedule == "O":
            
            tz_str = dateutil.parser.parse(str(data[u'Send_onetime']))
            Send_onetime = tz_str.replace(tzinfo = None)
            startday = None
            endday = None
            week_day = None
            month_day = None
            timeSend = None
            time_zone = tz_str.strftime('%z')[:3]+':'+tz_str.strftime('%z')[3:]
        else:
            Send_onetime = None
            tz_str = dateutil.parser.parse(str(data[u'timeSend']))
            startday = dateutil.parser.parse(str(data[u'startday'])).date()
            endday = dateutil.parser.parse(str(data[u'endday'])).date()
            week_day = str(data[u'week_day'])
            month_day = str(data[u'month_day'])
            timeSend = dateutil.parser.parse(str(data[u'timeSend'])).time()
            time_zone = tz_str.strftime('%z')[:3]+':'+tz_str.strftime('%z')[3:]
        
        sche_delivery = ScheduleDelivery(vmail  = vmail,
                                         type   = type_schedule,
                                         send_onetime = Send_onetime,
                                         start  = startday,
                                         end    = endday,
                                         week_day = week_day,
                                         month_day = month_day,
                                         time_send = timeSend,
                                         expired = False,
                                         time_zone = time_zone
                                         )
        sche_delivery.save()
        
    generate_thumbnail(vmail)
    response.write(vmail_url)
#    response.write(fuuid)
    return response

if not devMode:
    generateVmail = login_required(generateVmail)
        
def viewVmail(request,urlhash):
    vmail = get_object_from_hash(VM, urlhash)
    
    urlBackground = ""
    if not vmail.useThemeTemplate:
        if vmail.backgroundImage:
           urlBackground = vmail.backgroundImage.get_amazonS3_url()
    else:
        if vmail.backgroundTemplate:
           urlBackground = vmail.backgroundTemplate.from_image.get_amazonS3_url()

    
    avatar  = None
    if vmail.audioRecordMode:
        if vmail.avatarImages:
            avatar =  vmail.avatarImages.get_amazonS3_url().split('?Signature')[0]
            
    email = vmail.sender.email
    
    mPos = vmail.messagePos.split(',')
    mArr = [int(float(x)) for x in mPos]
    
    vPos = vmail.videoPos.split(',')
    vArr = [int(float(y)) for y in vPos]
    
    mSize = vmail.mSize.split(',')
    mSizeArr = [int(z) for z in mSize]
    
    
    vmail_thumbnail = 'https://s3.amazonaws.com/'+settings.AWS_STORAGE_BUCKET_NAME+'/vmail/images/'+uri_b64encode(str(vmail.id * 42))+'.jpg'
    
    return direct_to_template(request,"vmail_new/view.html",
                              {'object':vmail,'urlBackground':urlBackground,
                               'avatar':avatar,'email':email,
                               'mPos_left':mArr[0],'mPos_top':mArr[1],
                               'vPos_left':vArr[0],'vPos_top':vArr[1],
                               'mSize_weight':mSizeArr[0],'mSize_height':mSizeArr[1],
                                'urlhash':urlhash,
                                'CLOUDFRONT_STREAM':settings.CLOUDFRONT_STREAM,
                                'CLOUDFRONT_DOWNLOAD':settings.CLOUDFRONT_DOWNLOAD,
                                'VMAIL_RECORDING_STREAM':settings.VMAIL_RECORDING_STREAM,
                                'VMAIL_FILE_STREAM':settings.VMAIL_FILE_STREAM,
                                'linksite':unicode(Site.objects.get_current()),
                                'vmail_thumbnail':vmail_thumbnail,
                                'description':strip_tags(vmail.message)
                               })
    
        
def getRequestUser(request):
    response = HttpResponse(mimetype="application/json")
    response['Access-Control-Allow-Origin'] = '*'
    if devMode:
        user = User.objects.get(pk=327)
    else:
        user = request.user
        
    response.write(serializers.serialize("json",[user]))
    return response

if not devMode:
    getRequestUser = login_required(getRequestUser)

def getListThemeTemplate(request):
    if request.method == 'POST':
        cat = json.loads(request.POST['param'])
    else:
        raise Http404
    response = HttpResponse(mimetype="application/json")
    response['Access-Control-Allow-Origin'] = '*'
    list = bgtemplate.objects.getListVmailTheme(cat)
    response.write( serializers.serialize("json",list))
    return response

if not devMode:
    getListThemeTemplate = login_required(getListThemeTemplate)
      

def getListCatalogue(request):
    response = HttpResponse(mimetype="application/json")
    response['Access-Control-Allow-Origin'] = '*'
    if devMode:
        id = 327
        usr = authenticate_with_session(id)
    else:
        usr = request.user
        
    if request.method == "POST":
        type= request.POST['param']
        if type == 'background':
            list = bgcatalogue.objects.filter(active=True).order_by('name')
        elif type== 'email':
            list = EmailTemplateCategory.objects.filter(active = True).order_by('name')
        elif type=='contact':
            list = ContactGroup.objects.filter(owner = usr)
        response.write( serializers.serialize("json",list))
        return response
    else:
        raise Http404

if not devMode:
    getListCatalogue = login_required(getListCatalogue)

def uploadTheme(request):
    if not request.user.is_superuser:
        raise Http404
    
    if request.method == 'POST':
        uuid = request.POST['uuid']
        
        file = UserFile.objects.get(uuid=uuid)
        file.title = file.filename
        file.save()
        
        cat = bgcatalogue.objects.get(pk=int(request.POST['cat']))
        theme =bgtemplate(name = file.title,
                          description = file.description,
                          from_image = file,
                          catalogue = cat,
                          active = True
                          ) 
        theme.save()
        return HttpResponse('success')
    else:
        list = bgcatalogue.objects.all()
        return direct_to_template(request,'vmail_new/uploadtheme.html',{'list':list,'user':request.user,'bucket':settings.AWS_STORAGE_BUCKET_NAME})

uploadTheme = login_required(uploadTheme)
    
def categoryManager(request):
    if request.method == "GET":
        if 'action' in request.GET:
            if request.GET['action'] == 'add':
                cat = bgcatalogue(name =request.GET['catname'],active = True )
                cat.save()
                return HttpResponse('<div style="width:100%;clear:both"><label><img width="20px" src="/site_media/img/Close.png"> <input type="hidden" value="'+str(cat.id)+'"> '+str(cat.name)+'</label></div>')
            if request.GET['action'] == 'delete':
                id = int(request.GET['catid'])
                cat = bgcatalogue.objects.get(pk=id)
                themeList = bgtemplate.objects.filter(catalogue = id)
                for theme in themeList:
                    file = theme.from_image
                    delete_key(settings.AWS_STORAGE_BUCKET_NAME, "files/%s.%s" %(file.uuid, file.source_type))
                    file.delete()
                    theme.delete()
                cat.delete()
                return HttpResponse(str(id))
            
categoryManager = login_required(categoryManager)            

def emailcontent(request):
    if request.method == "POST":
        a = ''
        for x in request.FILES['files']:
            a+=str(x)
        return HttpResponse(request.FILES['files'])
    else:
        raise Http404

emailcontent = login_required(emailcontent)    

def email_template(request):
    if not request.user.is_superuser:
        raise Http404
    
    if request.method == 'POST':
        cat =EmailTemplateCategory.objects.get(pk= int(request.POST['categoryMainId']))
        if devMode:
            thum_id = 5770
        else:
            thum_id = int(request.POST['thumb_id'])
        file = UserFile(pk=thum_id)
        
        if request.POST['default_id'] =='yes':
            reset = EmailTemplate.objects.all()
            for x in reset:
                x.default = False
                x.save()
            default_temp = True
        else:
            default_temp = False
        etemp = EmailTemplate(
                              name = request.POST['template_name'],
                              filename = request.POST['template_filename'],
                              thumb = file,
                              content = '',
                              category = cat,
                              active = True,
                              default = default_temp
                              )
        etemp.save()
        return HttpResponse("success")
    else:
        list = EmailTemplateCategory.objects.filter(active = True)
        return direct_to_template(request,'vmail_new/email_template.html',{'list':list,'user':request.user,'bucket':settings.AWS_STORAGE_BUCKET_NAME})

email_template = login_required(email_template)    
        
def getListEmailTemplate(request):
    if request.method == 'POST':
        catId = json.loads(request.POST['param'])
    else:
        raise Http404
    
    response = HttpResponse(mimetype="application/json")
    response['Access-Control-Allow-Origin'] = '*'
    cat = EmailTemplateCategory.objects.get(pk=catId)
    list = EmailTemplate.objects.filter(category = cat)
    for e in list:
        if e.thumb:
            e.description =  u"https://s3.amazonaws.com/%s/files/%s.%s" %(settings.AWS_STORAGE_BUCKET_NAME,e.thumb.uuid,e.thumb.source_type)
    response.write( serializers.serialize("json",list))
    return response

if not devMode:
    getListEmailTemplate = login_required(getListEmailTemplate) 

def getHtmlEmailTemplate(request):
    response = HttpResponse(mimetype="text/html")
    if request.method == 'GET':
        if 'getdefault' in request.GET:
            template = EmailTemplate.objects.filter(default=True)
            if template:
                response.write(str(template[0].id)+";"+template[0].content)
            else:
                response.write("") 
            return response
        tempId = int(request.GET['id'])
        template = EmailTemplate.objects.get(pk=tempId)
        response.write(template.content)
        return response

if not devMode:
    getHtmlEmailTemplate = login_required(getHtmlEmailTemplate)

def getsettings(request):
    response = HttpResponse(mimetype="application/json")
    response['Access-Control-Allow-Origin'] = '*'
    if devMode:
        user = User.objects.get(pk=327)
    else:
        user = request.user
    
    result = {}
    subscription = user.get_subscription()
    result["bucket"] = settings.AWS_STORAGE_BUCKET_NAME
    result["frontstream"] = settings.CLOUDFRONT_STREAM
    result["frontdownload"] = settings.CLOUDFRONT_DOWNLOAD
    result["vmail_length_limit"] = subscription.vmail_length_limit
    result["vmail_save_days_limit"] = subscription.vmail_save_days_limit
    result["vmail_send_limit"] = subscription.vmail_send_limit
    result['domain'] = settings.HTTP_DOMAIN
    result['fileuploadURL'] = settings.FILE_UPLOADER_URL_V2
    response.write(json.dumps(result))
    return response

if not devMode:
    getsettings = login_required(getsettings)

def contacts_autocomplete(request):
    response = HttpResponse(mimetype="application/json")
    response['Access-Control-Allow-Origin'] = '*'
    if devMode:
        userid = 327
    else:
        userid = request.user.id
    if ('term' in request.GET) and request.GET['term'].strip():
        found_entries = Contact.objects.filter(user__id=userid, email__contains=request.GET['term'])
        result = []
        for i in found_entries:
            name = i.name
            if not i.name:
                name = 'empty'
            child = {}
            child['id'] = name
            child['value'] = name +' <'+ str(i.email)+'>'
            child['label'] = name +' <'+ str(i.email)+'>'
            result.append(child)
        response.write(json.dumps(result))
        return response

if not devMode:
    contacts_autocomplete = login_required(contacts_autocomplete)

def getSubscription(request):
    response = HttpResponse(mimetype="text/html")
    response['Access-Control-Allow-Origin'] = '*'
    if devMode:
        user = User.objects.get(pk=327)
    else:
        user = request.user
    subscription = user.get_subscription()
    today = datetime.datetime.now()
    vmails = VM.objects.filter(sender = user, date_create__month = today.month)
    
    if not settings.SUBSCRIPTION_APPLY:
        response.write('success')
        return response
    
    if len(vmails) >= subscription.vmail_send_limit:
        response.write('over')
        return response
    response.write('success')
    return response
if not devMode:    
    getSubscription = login_required(getSubscription)

def getAllowSpaceInfo(request):
    response = HttpResponse(mimetype="text/html")
    response['Access-Control-Allow-Origin'] = '*'
    if devMode:
        user = User.objects.get(pk=327)
    else:
        user = request.user
    file_size = int(request.GET['param'])
    
    subscription = user.get_subscription()
    
    usedStorage = 0
    used_files = UserFile.active_objects.filter(user = user)
    for file in used_files:
        usedStorage  += file.file_size
    
    usedStorage +=file_size
    
    if not settings.SUBSCRIPTION_APPLY:
        response.write('success')
        return response
    
    if  usedStorage > subscription.file_size_mb_limit*(1024*1024):
        response.write(usedStorage)
        return response
    response.write('success')
    return response    

if not devMode:
    getAllowSpaceInfo = login_required(getAllowSpaceInfo)