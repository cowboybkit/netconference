from django.contrib.auth.models import User
from subscription.models import UserSubscription,Subscription
from dateutil.relativedelta import relativedelta
from datetime import datetime

def create_usersubscriptions():
    """
    This creates new user subscriptions for all users that dont have a user subscriptions entry
    For 1 time use
    """
    
    free_subscription = Subscription.objects.get(name='Free')
    one_month = datetime.today() + relativedelta(months=1)
    created = 0
    errors = 0
    for user in User.objects.all():
        try:
            us = UserSubscription.objects.get(user=user,active=True,expires=False)
        except UserSubscription.DoesNotExist:
            us = UserSubscription(user=user,
                                  subscription=free_subscription,
                                  expires=one_month,
                                  active=True,
                                  cancelled=False,
                                  )
            try:
                us.save()
                created+=1
            except:
                errors+=1
    print "Created %s UserSubscriptions and faced errors while creating %s others"%(created,errors)
            
def correct_subscriptions():
    from dateutil.relativedelta import relativedelta
    dt = datetime.today() + relativedelta(month=1)
    added_us = UserSubscription.objects.filter(expires=dt)
    corrected = 0
    deleted = 0
    for w in added_us:
        if w.user.usersubscription_set.count() >= 2:
            w.delete()
            deleted+=1
        else:
            today = datetime.today()
            next_month = today+relativedelta(months=1)
            w.expires = next_month
            w.save()
            corrected+=1



    
    
if __name__ == '__main__':
    create_usersubscriptions()
