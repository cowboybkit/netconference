from netsign.models import NetSignDocument

def add_user_nsd():
    nsds = NetSignDocument.objects.all()
    for el in nsds:
	try:
	    el.user = el.user_file.user
            el.save()
	except:
    	    el.delete()

if __name__=='__main__':
    add_user_nsd()
    
