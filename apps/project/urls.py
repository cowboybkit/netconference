from django.conf.urls.defaults import *
from django.views.generic.simple import direct_to_template
from django.contrib.auth import views as auth_views
from django.contrib import admin

from rss import *

admin.autodiscover()

urlpatterns = patterns('project.foo',
    )

urlpatterns += patterns('project.users',
        url(r'^mail/', 'send_mail', name='send_mail'),
    )


urlpatterns += patterns('project.users',
    url(r'^accounts/login/$', 'login', name='login'),
    url(r'^accounts/logout/$', 'logout', name='logout'),
    url(r'^accounts/profile/$', 'profile', name='profile'),
    url(r'^accounts/register/$', 'register', name='register'),
    url(r'^accounts/settings/$', 'settings', name='settings'),
    url(r'^accounts/password/reset/$', auth_views.password_reset, {'template_name':'registration/password_reset.html', 
                                                                   'email_template_name':'registration/password_reset_mail.html'}, name='password_reset'),
    url(r'^accounts/password/reset/done/$', auth_views.password_reset_done, {'template_name':'registration/passwordreset_done.html',}, name='password_reset_done'),
    #url(r'^accounts/', include('registration.urls')),
    url(r'^(?P<project_name>\w+)/user/(?P<username>\w+)/$', 'user_details', name='user_details'),
    )

feeds = {
    'project': ProjectRss,
}
urlpatterns += patterns('',
    #(r'^feeds/(?P<url>.*)/$', 'django.contrib.syndication.views.feed', {'feed_dict': feeds}),
    url(r'^feeds/(?P<url>.*)/$', 'project.rss.proj_feed', {'feed_dict': feeds}, name='project_feeds')
    )

urlpatterns += patterns('project.main',
    # Example:
    url(r'^$', 'dashboard', name="project_dashboard"),    
    url(r'^create/$', 'create_project', name="project_create_project"),    
    url(r'^help/$', direct_to_template, {'template':'project/dummy.html'}, name='project_help'),
    url(r'^demo/$', direct_to_template, {'template':'project/demo.html'}, name='demo'),
    (r'^admin/(.*)', admin.site.root),
    url(r'^dashboard/$', 'dashboard', name='dashboard'),    
    url(r'^(?P<project_name>\w+)/$', 'project_details', name='project_details'),
    url(r'^(?P<project_name>\w+)/settings/$', 'settings', name='project_settings'),                            
    url(r'^(?P<project_name>\w+)/logs/$', 'full_logs', name='project_logs'),
    url(r'^(?P<project_name>\w+)/noticeboard/$', 'noticeboard', name='project_noticeboard'),
    url(r'^(?P<project_name>\w+)/todo/$', 'todo', name='project_todo'),
)

urlpatterns += patterns('project.tasks',
    url(r'^(?P<project_name>\w+)/tasks/$', 'project_tasks', name='project_tasks'),
    url(r'^(?P<project_name>\w+)/taskhier/$', 'task_hierachy', name='project_task_hierarchy'),
    url(r'^(?P<project_name>\w+)/tasks/quickentry/$', 'tasks_quickentry', name='project_tasks_quickentry'),
    url(r'^(?P<project_name>\w+)/taskitems/quickentry/$', 'taskitems_quickentry', name='project_taskitems_quickentry'),
    url(r'^(?P<project_name>\w+)/taskdetails/(?P<task_num>\d+)/$', 'task_details', name='project_task_details'),
    url(r'^(?P<project_name>\w+)/taskhistory/(?P<task_num>\d+)/$', 'task_history', name='project_task_history'),
    url(r'^(?P<project_name>\w+)/taskdetails/(?P<task_num>\d+)/addnote/$', 'add_task_note', name='project_add_task_note'),
    url(r'^(?P<project_name>\w+)/edittask/(?P<task_num>\d+)/$', 'edit_task', name='project_edit_task'),
    url(r'^(?P<project_name>\w+)/taskrevision/(?P<task_id>\d+)/$', 'task_revision', name='project_task_revision'),
    url(r'^(?P<project_name>\w+)/edititem/(?P<taskitem_num>\d+)/$', 'edit_task_item', name='project_edit_taskitem'),
    url(r'^(?P<project_name>\w+)/taskitemhistory/(?P<taskitem_num>\d+)/$', 'taskitem_history', name='project_taskitem_history'),
    url(r'^(?P<project_name>\w+)/itemrevision/(?P<taskitem_id>\d+)/$', 'taskitem_revision', name='project_taskitem_revision'),
    url(r'^(?P<project_name>\w+)/taskitemhist/(?P<taskitem_num>\d+)/$', 'taskitem_history', name='project_taskitem_history'),
    )

urlpatterns += patterns('project.wiki',
    url(r'^(?P<project_name>\w+)/wiki/$', 'wiki', name='project_wiki'),
    url(r'^(?P<project_name>\w+)/wiki/new/$', 'create_wikipage', name='project_create_wikipage'),
    url(r'^(?P<project_name>\w+)/wiki/(?P<page_name>\w+)/$', 'wikipage', name='project_wikipage'),
    url(r'^(?P<project_name>\w+)/wiki/(?P<page_name>\w+)/revisions/$', 'wikipage_diff', name='project_wikipage_diff'),
    url(r'^(?P<project_name>\w+)/wiki/(?P<page_name>\w+)/edit/$', 'edit_wikipage', name='project_edit_wikipage'),
    url(r'^(?P<project_name>\w+)/wiki/(?P<page_name>\w+)/revisions/(?P<revision_id>\d+)/$', 'wiki_revision', name='project_wiki_revision'),
    )

urlpatterns += patterns('project.metrics',
    url(r'^(?P<project_name>\w+)/health/$', 'project_health', name='project_health'),
    url(r'^(?P<project_name>\w+)/userstats/$', 'user_stats', name='project_user_stats'),
    )

urlpatterns += patterns('project.files',
    url(r'^(?P<project_name>\w+)/files/$', 'files', name='project_files'),
    )

urlpatterns += patterns('project.pcalendar',
    url(r'^(?P<project_name>\w+)/calendar/$', 'index', name='project_calendar'),
    url(r'^(?P<project_name>\w+)/calendar/(?P<year>\d+)/(?P<month>\d+)/$', 'month_cal', name='project_month_cal'),
    )




