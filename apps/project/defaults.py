"""Site wide default"""
notices_per_page = 10
logs_per_page = 30
tasks_on_tasks_page = 10
objects_on_quickentry_page = 8

base_url = 'http://www.dashbard.com'

expires_in = 60*60*24 #One day
bucket = 'i-love-foobar'
