from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.http import Http404

from project.helpers import get_project, render, get_access, give_access
from project.bforms import AddFileForm
from apps.files.models import UserFile
from django.utils.translation import ugettext_lazy as _

@login_required
def files(request, project_name):
    """Files for a project. Shows the files uploaded for a project.
    Actions available:
    Add files:  Owner Participant
    """
    project = get_project(request, project_name)
    access = get_access(project, request.user)
    addfileform = AddFileForm(project = project, user = request.user)    
    if request.method == 'POST':
        if 'Addfile' in request.POST:
            return add_file(request, project_name)
        if 'fileid' in request.POST:
            return delete_file(request, project_name)
    payload = locals()
    return render(request, 'project/files.html', payload)

@give_access(["Owner", "Participant"])
def add_file(request, project_name):
    project = get_project(request, project_name)
    addfileform = AddFileForm(project , request.user, request.POST, request.FILES)
    if addfileform.is_valid():
        addfileform.save()
        request.user.message_set.create(message=unicode(_("File successfully uploaded.")))
    return HttpResponseRedirect(".")

@give_access(["Owner", "Participant"])
def delete_file(request, project_name):
    file_id = request.POST['fileid']
    try:
        u_file = UserFile.objects.get(pk=file_id, user=request.user)
    except UserFile.DoesNotExist:
        request.user.message_set.create(message=unicode(_("File not found.")))
        raise Http404
    u_file.delete()
    request.user.message_set.create(message=unicode(_("File successfully deleted.")))
    return HttpResponseRedirect(".")
