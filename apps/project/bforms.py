from django import forms
import re

from project.models import *
from django.utils.translation import ugettext_lazy as _
from project.dojofields import *
from apps.files.models import UserFile
#import prefs.models as pmodel
import datetime

class CreateProjectForm(MarkedForm):
    """Create a new project.
    Writes to model project
    Short name: Only alphanumeric chars allowed. Length = 20
    Name: Name of project. Length = 200
    Start_date: Start date for project. Defaults to today.
    End_date: End date ofr the project.
    """
    shortname = DojoCharField(max_length = 20, help_text = _('Shortname for your project. Determines URL. Can not contain spaces/special chars.'))
    name = DojoCharField(max_length = 200, help_text=_('Name of the project.'))
    start_date = DojoDateField()
    end_date = DojoDateField(required = False)
    
    def __init__(self, user = None, *args, **kwargs):
        super(CreateProjectForm, self).__init__(*args, **kwargs)
        self.user = user
    
    def save(self):
        project = Project(name = self.cleaned_data['name'], shortname=self.cleaned_data['shortname'])
        project.owner = self.user
        project.start_date = self.cleaned_data['start_date']
        project.save()
        subscribe = SubscribedUser(user = self.user, project = project, group = 'Owner')
        subscribe.save()
        return project
    
    def clean_shortname(self):
        alnum_re = re.compile(r'^\w+$')
        if not alnum_re.search(self.cleaned_data['shortname']):
            raise forms.ValidationError(_("This value must contain only letters, numbers and underscores."))
        self.is_valid_shortname()
        return self.cleaned_data['shortname']
    
    def is_valid_shortname(self):
        try:
            Project.objects.get(shortname = self.cleaned_data['shortname'])
        except Project.DoesNotExist:
            return
        raise forms.ValidationError(_('This project name is already taken. Please try another.'))

    def clean_name(self):
        name = self.cleaned_data["name"].strip()
        if not name:
            raise forms.ValidationError(_("This field is required."))
        return name

    def clean_start_date(self):
        start_date = self.cleaned_data["start_date"]
        if start_date < datetime.datetime.today().date():
            raise forms.ValidationError(_("Start date cannot be in the past."))
        return start_date

    def clean_end_date(self):
        end_date = self.cleaned_data["end_date"]
        start_date = self.cleaned_data.get("start_date", None)
        if start_date and end_date and end_date < start_date:
            raise forms.ValidationError(_("End date cannot be less than the Start date"))
        return end_date
    
class InviteUserForm(MarkedForm):
    """Invite a user to the project.
    Username: username of the user to invite.
    Group: The group in which to put the invited user.
    """
    username = DojoCharField(max_length = 30, help_text = 'User name of the user to invite.')
    group = DojoChoiceField(choices = options, help_text = 'Permissions available to this user.')
    
    def __init__(self, project = None, *args, **kwargs):
        super(InviteUserForm, self).__init__(*args, **kwargs)
        self.project = project
        
    def clean_username(self):
        username = self.cleaned_data["username"].strip()
        if not username:
            raise forms.ValidationError(_("This field is required."))
        try:
            User.objects.get(username = username)
        except User.DoesNotExist:
            raise forms.ValidationError(_('There is no user with that name'))
        self.already_invited()
        self.already_subscribed()
        return username
    
    def clean_group(self):
        if not self.cleaned_data['group'] in ('Owner', 'Participant', 'Viewer'):
            raise forms.ValidationError('No such group')
        return self.cleaned_data['group']
    
    def already_invited(self):
        try:
            user = User.objects.get(username = self.cleaned_data['username'])
            invite = user.inviteduser_set.get(user = user, project = self.project)
        except InvitedUser.DoesNotExist:
            return    
        raise forms.ValidationError('This user is already invited. The invite is pending.')
        
    def already_subscribed(self):
        try:
            user = User.objects.get(username = self.cleaned_data['username'])
            subs = user.subscribeduser_set.get(user = user, project = self.project)
        except SubscribedUser.DoesNotExist:
            return    
        raise forms.ValidationError('This user is already subscribed to the project.')
        
    def save(self):
        user = User.objects.get(username = self.cleaned_data['username'])
        invite = InvitedUser(user = user, project = self.project)
        invite.group = self.cleaned_data['group']
        invite.save()
        return invite
        
class CreateTaskForm(MarkedForm):
    """Create a top level task."""
    name = DojoCharField(label="Name", max_length=200, help_text='Name of the task')
    start_date = DojoDateField(label="Start date", help_text = 'When will this task start?', required=True)
    end_date = DojoDateField(label="End date", required = False, help_text = 'When will this task end?')
    user_responsible = DojoChoiceField(label="User responsible", help_text = 'Who is reponsible for this task?')
    def __init__(self, project , user, *args, **kwargs):
        super(CreateTaskForm, self).__init__(*args, **kwargs)
        self.project = project
        self.user = user
        users = [subs.user for subs in project.subscribeduser_set.all()]
        self.fields['user_responsible'].choices = [('None','None')] + [(user.username, user.username) for user in users]

    def clean_name(self):
        name = self.cleaned_data["name"].strip()
        if not name:
            raise forms.ValidationError(_("This field is required."))
        return name
        
    def clean_start_date(self):
        start_date = self.cleaned_data["start_date"]
        if start_date < datetime.datetime.today().date():
            raise forms.ValidationError(_("Start date cannot be in the past."))
        if start_date and self.project.start_date and start_date < self.project.start_date:
            raise forms.ValidationError(_("Start date cannot be less than project's start date."))
        if start_date and self.project.end_date and start_date > self.project.end_date:
            raise forms.ValidationError(_("Start date cannot be less than project's end date."))
        return start_date

    def clean_end_date(self):
        end_date = self.cleaned_data["end_date"]
        start_date = self.cleaned_data.get("start_date", None)
        if start_date and end_date and end_date < start_date:
            raise forms.ValidationError(_("End date cannot be less than start_date."))
        if end_date and self.project.start_date and end_date < self.project.start_date:
            raise forms.ValidationError(_("End date cannot be less than project's start date."))
        if end_date and self.project.end_date and end_date > self.project.end_date:
            raise forms.ValidationError(_("End date cannot be greater than project's end date."))
        return end_date
        
    def save_without_db(self):
        task = Task(name = self.cleaned_data['name'], expected_start_date = self.cleaned_data['start_date'], )
        if self.cleaned_data['end_date']:
            task.expected_end_date = self.cleaned_data['end_date']
        if not self.cleaned_data['user_responsible'] == 'None':
            user = User.objects.get(username = self.cleaned_data['user_responsible'])
            task.user_responsible = user
        task.project = self.project
        task.created_by = self.user
        task.last_updated_by = self.user
        return task        
        
    def save(self):
        task = self.save_without_db()
        task.save()
        return task


class CreateSubTaskForm(CreateTaskForm):
    """Create a sub task for task."""
    def __init__(self, project, user, parent_task = None, *args, **kwargs):
        super(CreateSubTaskForm, self).__init__(project, user, *args, **kwargs)
        self.parent_task = parent_task

    def clean_start_date(self):
        start_date = super(CreateSubTaskForm, self).clean_start_date()
        if start_date < self.parent_task.expected_start_date:
            raise forms.ValidationError(_("Sub Task start date cannot be less than Parent Task start date."))
        if start_date > self.parent_task.expected_end_date:
            raise forms.ValidationError(_("Sub Task start date cannot be greater than the Parent Task end date."))
        return start_date

    def clean_end_date(self):
        end_date = super(CreateSubTaskForm, self).clean_end_date()
        if end_date and end_date > self.parent_task.expected_end_date:
            raise forms.ValidationError(_("Sub Task end date cannot be greater than the Parent Task end date."))
        return end_date

    def save(self):
        task = self.save_without_db()
        task.parent_task_num = self.parent_task.number
        task.save()
        return task
        
        
class CreateTaskItemForm(MarkedForm):
    """Create a task item."""
    item_name = DojoCharField(label=_("Item name"), max_length = 200, help_text = _('Name of this task item.'))
    user = DojoChoiceField(label=_("User"), help_text = _('Who is going to do this task item?'))
    time = DojoDecimalField(label=_("Time"), help_text = _('How long will this task item take?'))
    units = DojoChoiceField(label=_("Units"), choices = unit_choices)
    
    def __init__(self, project, user, task, *args, **kwargs):
        super(CreateTaskItemForm, self).__init__(*args, **kwargs)
        self.project = project
        self.user = user
        self.task = task
        users = [subs.user for subs in task.project.subscribeduser_set.all()]
        self.fields['user'].choices = [('None','None')] + [(user.username, user.username) for user in users]

    def clean_item_name(self):
        item_name = self.cleaned_data["item_name"].strip()
        if not item_name:
            raise forms.ValidationError(_("This field is required."))
        return item_name
        
    def clean_time(self):
        if self.cleaned_data['time'] <= 0:
            raise forms.ValidationError(_('Time must be greater than 0'))
        return self.cleaned_data['time']
        #return super(CreateTaskItemForm, self).clean()
        
    def save_without_db(self):
        item = TaskItem(name = self.cleaned_data['item_name'], )
        item.project = self.project
        item.created_by = self.user
        item.last_updated_by = self.user
        item.task_num = self.task.number
        if not self.cleaned_data['user'] == 'None':
            user = User.objects.get(username = self.cleaned_data['user'])
            item.user = user
        item.expected_time = self.cleaned_data['time']
        item.unit = self.cleaned_data['units']
        return item
        
    def save(self):
        item = self.save_without_db()
        item.save()
        return item
    
class TaskItemQuickForm(CreateTaskItemForm):
    
    """item_name = DojoCharField(max_length = 200, help_text = 'Name of this task item.')
    user = DojoChoiceField(help_text = 'Who is going to do this task item?')
    time = DojoDecimalField(help_text = 'How long will this task item take?')
    units = DojoChoiceField(choices = unit_choices)"""
    task = forms.ChoiceField(label="Task")
    
    def __init__(self, project, user, *args, **kwargs):
        super(MarkedForm, self).__init__(*args, **kwargs)
        self.project = project
        self.user = user
        users = [subs.user for subs in project.subscribeduser_set.all()]
        self.fields['user'].choices = [('None','None')] + [(user.username, user.username) for user in users]
        tasks = [task for task in project.task_set.all()]
        self.fields['task'].choices = [(task.number, task.name) for task in tasks]
         
    def save(self):
        task = self.cleaned_data['task']
        self.task = Task.objects.get(number = task, project = self.project)
        item = self.save_without_db()
        item.save()
        return item
        
        """task = Task.objects.get(project = self.project, number = self.cleaned_data['task'])
        self.task = task
        taskitem = TaskItem(project = self.project, task = task, name = self.cleaned_data['item_name'],)
        taskitem.created_by  = self.user
        taskitem.last_updated_by = self.user
        if not self.cleaned_data['user'] == 'None':
            user = User.objects.get(username = self.cleaned_data['user'])
            taskitem.user = user
        taskitem.expected_time = self.cleaned_data['time']
        taskitem.unit = self.cleaned_data['units']
        taskitem.save()
        return taskitem   """
    
class AddNoticeForm(MarkedForm):
    """Add a notice to a task."""
    text = DojoCharField(widget = forms.Textarea)
    
    def __init__(self, project = None, user = None, *args, **kwargs):
        super(AddNoticeForm, self).__init__(*args, **kwargs)
        self.project = project
        self.user = user

    def clean_text(self):
        text = self.cleaned_data["text"].strip()
        if not text:
            raise forms.ValidationError(_("This field is required."))
        return text
        
    def save(self):
        notice = Notice(text = self.cleaned_data['text'], user = self.user, project = self.project)
        notice.save()
        return notice
    
class AddTodoListForm(MarkedForm):
    """Add a todo list for the given user."""
    name = DojoCharField(help_text = 'Name of your todo list.')
    
    def __init__(self, project = None, user = None, *args, **kwargs):
        super(AddTodoListForm, self).__init__(*args, **kwargs)
        self.project = project
        self.user = user

    def clean_name(self):
        name = self.cleaned_data["name"].strip()
        if not name:
            raise forms.ValidationError(_("This field is required."))
        return name
        
    def save(self):
        list = TodoList(name = self.cleaned_data['name'], user = self.user, project = self.project)
        list.save()
        return list

class CreateWikiPageForm(MarkedForm):
    """Create a new wiki page."""
    title = DojoCharField(help_text = 'Name of the wiki page.')
    text = DojoTextArea()    
    def __init__(self, project = None, user = None, *args, **kwargs):
        super(CreateWikiPageForm, self).__init__(*args, **kwargs)
        self.project = project
        self.user = user
        
    def save(self):
        page = WikiPage(title = self.cleaned_data['title'],)
        page.project = self.project
        page.save()
        
        page_rev = WikiPageRevision(wiki_text = self.cleaned_data['text'])
        page_rev.wiki_page = page
        page_rev.user = self.user
        page_rev.save()
        
        page.current_revision = page_rev
        page.save()
        return page
        
class EditWikiPageForm(MarkedForm):
    """Edit an existing wiki page."""
    text = DojoTextArea()
    
    def __init__(self, user = None, page = None, *args, **kwargs):
        super(EditWikiPageForm, self).__init__(*args, **kwargs)
        self.page = page
        self.user = user
        self.fields['text'].initial = page.current_revision.wiki_text
    
    def save(self):
        page_rev = WikiPageRevision(wiki_text = self.cleaned_data['text'])
        page_rev.wiki_page = self.page
        page_rev.user = self.user
        page_rev.save()
        
        self.page.current_revision = page_rev
        self.page.save()

class EditTaskForm(CreateTaskForm):
    """Edit a task."""
    actual_start_date = DojoDateField(required = False, help_text=_('When did this task start?'))
    actual_end_date = DojoDateField(required = False, help_text=_('When did this task end?'))
    is_complete = forms.BooleanField(required=False, help_text = _('Is this task complete?'))
    
    def __init__(self, project, user, task, *args, **kwargs):
        super(EditTaskForm, self).__init__(project, user, *args, **kwargs)
        self.task = task
        self.fields['name'].initial = task.name
        self.fields['start_date'].initial = task.expected_start_date
        self.fields['end_date'].initial = task.expected_end_date
        self.fields['actual_start_date'].initial = task.actual_start_date
        self.fields['actual_end_date'].initial = task.actual_end_date
        self.fields['is_complete'].initial = task.is_complete
        if task.user_responsible:
            self.fields['user_responsible'].initial = task.user_responsible

    def clean_actual_end_date(self):
        actual_end_date = self.cleaned_data["actual_end_date"]
        actual_start_date = self.cleaned_data["actual_start_date"]
        if actual_start_date and actual_end_date and actual_end_date < actual_start_date:
            raise forms.ValidationError(_("The Actual end date cannot be less than Actual start date."))
        return actual_end_date
    
    def save(self):
        task = self.task
        task.name = self.cleaned_data['name']
        if not self.cleaned_data['user_responsible'] == 'None':
            user = User.objects.get(username = self.cleaned_data['user_responsible'])
        else:
            user = None
        task.user_responsible = user
        task.expected_start_date = self.cleaned_data['start_date']
        if self.cleaned_data['end_date']:
            task.expected_end_date = self.cleaned_data['end_date']
        if self.cleaned_data['actual_start_date']:
            task.actual_start_date = self.cleaned_data['actual_start_date']
        if self.cleaned_data['actual_end_date']:
            task.actual_end_date = self.cleaned_data['actual_end_date']
        task.is_complete_prop = self.cleaned_data['is_complete']
        task.updated_by = self.user
        task.save()
        return task


"""            
class EditTaskItemForm(forms.ModelForm):
    "Edit a task item."
    user = DojoChoiceField()
    
    def __init__(self, *args, **kwargs):
        super(EditTaskItemForm, self).__init__(*args, **kwargs)
        users = [subs.user for subs in self.instance.task.project.subscribeduser_set.all()]
        self.fields['user'].choices = [('None','None')] + [(user.username, user.username) for user in users]    
    
    class Meta:
        model = TaskItem
        exclude = ('task', 'task_num', 'version_number', 'is_current', 'effective_end_date')
"""

class EditTaskItemForm(forms.Form):
    """Edit as task item."""
    name = DojoCharField()
    user = DojoChoiceField()
    expected_time = DojoDecimalField()
    actual_time = DojoDecimalField(required = False)
    unit = DojoChoiceField(choices = unit_choices)
    is_complete = forms.BooleanField(required=False)
    
    def __init__(self, project, user, taskitem, *args, **kwargs):
        super(EditTaskItemForm, self).__init__(*args, **kwargs)
        self.project = project
        self.taskitem = taskitem
        self.user = user
        users = [subs.user for subs in taskitem.project.subscribeduser_set.all()]
        self.fields['user'].choices = [('None','None')] + [(user.username, user.username) for user in users]
        self.fields['name'].initial = taskitem.name
        self.fields['user'].initial = taskitem.user
        self.fields['expected_time'].initial = taskitem.expected_time
        self.fields['actual_time'].initial = taskitem.actual_time
        self.fields['unit'].initial = taskitem.unit
        self.fields['is_complete'].initial = taskitem.is_complete

    def clean_name(self):
        name = self.cleaned_data["name"].strip()
        if not name:
            raise forms.ValidationError(_("This field is required."))
        return name

    def clean_expected_time(self):
        expected_time = self.cleaned_data["expected_time"]
        if expected_time <= 0:
            raise forms.ValidationError(_("Please provide a value greater than 0."))
        return expected_time
        
    def clean_actual_expected_time(self):
        actual_expected_time = self.cleaned_data["actual_expected_time"]
        if actual_expected_time <= 0:
            raise forms.ValidationError(_("Please provide a value greater than 0."))
        return actual_expected_time
        
    def save(self):
        self.taskitem.name = self.cleaned_data['name']
        if not self.cleaned_data['user'] == 'None':
            user = User.objects.get(username = self.cleaned_data['user'])
        else:
            user = None
        self.taskitem.user = user
        self.taskitem.expected_time = self.cleaned_data['expected_time']
        self.taskitem.actual_time = self.cleaned_data['actual_time']
        self.taskitem.unit = self.cleaned_data['unit']
        self.taskitem.is_complete = self.cleaned_data['is_complete']
        self.taskitem.save()
        return self.taskitem
        
class AddTaskNoteForm(MarkedForm):
    """Add a note to a task."""
    text = DojoCharField(widget = forms.Textarea, help_text = _('Add a note to this task'))
    
    def __init__(self, task, user, *args, **kwargs):
        super(AddTaskNoteForm, self).__init__(*args, **kwargs)
        self.task = task
        self.user = user

    def clean_text(self):
        text = self.cleaned_data["text"].strip()
        if not text:
            raise forms.ValidationError(_("This field is required."))
        return text
        
    def save(self):
        note = self.task.add_note(text = self.cleaned_data['text'], user = self.user)
        return note
    
#from prajact.registration import forms as regforms
#class UserCreationForm(MarkedForm, regforms.RegistrationForm):
    #"""A form that creates a user, with no privileges, from the given username and password."""
    #username = DojoCharField(max_length = 30, required = True, help_text = '')
    #email = forms.EmailField(widget=forms.TextInput(attrs={'class':'input required'}))
    #password1 = DojoPasswordField(max_length = 30, required = True)
    #password2 = DojoPasswordField(max_length = 30, required = True)
    #"""project_name = DojoCharField(max_length = 20, required = False)

    #def clean_username (self):
        #alnum_re = re.compile(r'^\w+$')
        #if not alnum_re.search(self.cleaned_data['username']):
            #raise ValidationError("This value must contain only letters, numbers and underscores.")
        #self.isValidUsername()
        #return self.cleaned_data['username']

    #def clean (self):
        #if self.cleaned_data['password1'] != self.cleaned_data['password2']:
            #raise ValidationError(_("The two password fields didn't match."))
        #return super(MarkedForm, self).clean()
        
    #def isValidUsername(self):
        #try:
            #User.objects.get(username=self.cleaned_data['username'])
        #except User.DoesNotExist:
            #return
        #raise ValidationError(_('A user with that username already exists.'))
    
    #def clean_project_name(self):
        #alnum_re = re.compile(r'^\w+$')
        #if not alnum_re.search(self.cleaned_data['project_name']):
            #raise ValidationError("This value must contain only letters, numbers and underscores.")
        #self.is_valid_shortname()
        #return self.cleaned_data['project_name']
    
    #def is_valid_shortname(self):
        #try:
            #Project.objects.get(shortname = self.cleaned_data['project_name'])
        #except Project.DoesNotExist:
            #return
        #raise ValidationError('This project name is already taken. Please try another.')
    
    #def save(self):
        #user = User.objects.create_user(self.cleaned_data['username'], '', self.cleaned_data['password1'])
        ##profile = UserProfile(user = user)
        ##profile.save()
        #return user"""
    
class AddFileForm(forms.Form):
    """Add a file."""
    filename = forms.FileField()
    
    def __init__(self, project, user, *args, **kwargs):
        super(AddFileForm, self).__init__(*args, **kwargs)
        self.project = project
        self.user = user
    
    def save(self):
        uf = UserFile.objects.create(title=self.cleaned_data["filename"].name,
                                     user=self.user, filename=self.cleaned_data["filename"].name,
                                     project=self.project, processed=False, is_active=True,
                                     visibility="U", file_size=self.cleaned_data["filename"].size)
        return uf
                
class AddTaskOrSubTaskForm(CreateTaskForm):
    parent_task = forms.ChoiceField(label="Parent Task")
    def __init__(self, project , user, *args, **kwargs):
        super(AddTaskOrSubTaskForm, self).__init__(project, user, *args, **kwargs)
        tasks = [task for task in project.task_set.all()]
        self.fields['parent_task'].choices = [('None','None')] + [(task.number, task.name) for task in tasks]#[(user.username, user.username) for user in users]
    
    def save(self):
        task = super(AddTaskOrSubTaskForm, self).save_without_db()
        if self.cleaned_data['parent_task'] == 'None':
            task.parent_task = None
        else:
            task_num = int(self.cleaned_data['parent_task'])
            par_task = Task.objects.get(project = self.project, number = task_num)
            task.parent_task_num = par_task.number
        task.save()    

class FormCollection:
    def __init__(self, FormClass, attrs, num_form):
        self.data = []
        for i in xrange(num_form):
            self.data.append(FormClass(prefix = i, **attrs))
            
    def is_valid(self):
        "Are all the forms valid"
        for form in self.data:
            if form.is_bound and not form.is_valid():
                return False
            return True
            
    def save(self):
        """Save the forms which are valid."""
        for form in self.data:
            if form.is_valid():
                form.save()
                
#class PreferencesForm(forms.ModelForm):
    #class Meta:
        #model = pmodel.UserProfile
        #exclude = ('user',)
    
                
from django.forms import widgets
from django.contrib.auth.models import User

"""
class LoginForm(forms.Form):
    "Login form for users."
    username = forms.RegexField(r'^[a-zA-Z0-9_]{1,30}$',
                                max_length = 30,
                                min_length = 1,
                                widget = widgets.TextInput(attrs={'class':'input'}),
                                error_message = 'Must be 1-30 alphanumeric characters or underscores.')
    password = forms.CharField(min_length = 1, 
                               max_length = 128, 
                               widget = widgets.PasswordInput(attrs={'class':'input'}),
                               label = 'Password')
    remember_user = forms.BooleanField(required = False, 
                                       label = 'Remember Me')

    def clean_username (self):
	try:
            user = User.objects.get(username__iexact = self.cleaned_data['username'])
        except User.DoesNotExist, KeyError:
            raise forms.ValidationError('Invalid username, please try again.')
	if not user.is_active:
	   raise forms.ValidationError('Your account has not yet been validated. Please follow isntructions sent to %s' % user.email)
        return self.cleaned_data['username']
    
    def clean(self):
        user = User.objects.get(username__iexact = self.cleaned_data['username'])
	
        if not user.check_password(self.cleaned_data['password']):
            raise forms.ValidationError('Invalid password, please try again.')
        
        return self.cleaned_data
                
"""
class LoginForm(forms.Form):
    """Login form for users."""
    username = forms.RegexField(r'^[a-zA-Z0-9_]{1,30}$',
                                max_length = 30,
                                min_length = 1,
                                widget = widgets.TextInput(attrs={'class':'input'}),
                                error_message = 'Must be 1-30 alphanumeric characters or underscores.')
    password = forms.CharField(min_length = 1, 
                               max_length = 128, 
                               widget = widgets.PasswordInput(attrs={'class':'input'}),
                               label = 'Password')
    remember_user = forms.BooleanField(required = False, 
                                       label = 'Remember Me')
    def clean_username (self):
	try:
            user = User.objects.get(username__iexact = self.cleaned_data['username'])
        except User.DoesNotExist, KeyError:
            raise forms.ValidationError('Invalid username, please try again.')
	if not user.is_active:
	    raise forms.ValidationError('Your account has not yet been validated. Please follow instructions sent to %s' % user.email)
	return self.cleaned_data['username']
	
    def clean(self):
        try:
		try:
		    user = User.objects.get(username__iexact = self.cleaned_data['username'])
		except User.DoesNotExist, KeyError:
		    raise forms.ValidationError('Invalid username, please try again.')
		
		if not user.check_password(self.cleaned_data['password']):
		    raise forms.ValidationError('Invalid password, please try again.')
	except :
	    raise forms.ValidationError('Either username or password is wrong.')
			
        return self.cleaned_data
