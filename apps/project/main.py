from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required

from project.helpers import *
from project.models import *
from project import bforms
from project.defaults import *
from django.core.paginator import Paginator, InvalidPage
import csv
import StringIO
import sx.pisa3 as pisa

from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _


def index(request):
    """If the user is not logged in, show him the login/register forms, with some blurb about the services. Else redirect to /dashboard/"""
    if request.user.is_authenticated():
        return reverse('project_dashboard')
    if request.method == 'POST':
        return login(request)
    register_form = bforms.UserCreationForm(prefix='register')
    login_form = bforms.LoginForm()
    request.session.set_test_cookie()
    payload = {'register_form':register_form, 'login_form':login_form}
    return render(request, 'project/index.html', payload)

@login_required
def dashboard(request):
    """The point of entry for a logged in user.
    Shows the available active projects for the user, and allows him to create one.
    Shows the pending invites to other projects.
    Shows very critical information about available projects.
    """
    user = request.user
    if request.GET.get('includeinactive', 0):
        subs = user.subscribeduser_set.all()
        includeinactive = True
    else:
        subs = user.subscribeduser_set.filter(project__is_active = True)
        includeinactive = False
    invites = user.inviteduser_set.filter(rejected = False)
    createform = bforms.CreateProjectForm()
    if request.method == 'POST':
        if 'createproject' in request.POST:
            createform = bforms.CreateProjectForm(user, request.POST)
            if createform.is_valid():
                createform.save()
                return HttpResponseRedirect('.')
        elif 'acceptinv' in request.POST:
            project = Project.objects.get(id = request.POST['projid'])
            invite = InvitedUser.objects.get(id = request.POST['invid'])
            subscribe = SubscribedUser(project = project, user = user, group = invite.group)
            subscribe.save()
            invite.delete()
            request.user.message_set.create(message=unicode(_("Project invitation successfully accepted.")))
            return HttpResponseRedirect('.')
        elif 'rejectinv' in request.POST:
            project = Project.objects.get(id = request.POST['projid'])
            invite = InvitedUser.objects.get(id = request.POST['invid'])
            invite.delete()
            request.user.message_set.create(message=unicode(_("Project invitation rejected.")))
            return HttpResponseRedirect('.')
        elif 'activestatus' in request.POST:
            projid = request.POST['projectid']
            project = Project.objects.get(id = projid)
            if request.POST['activestatus'] == 'true':
                project.is_active = False
            elif request.POST['activestatus'] == 'false':
                project.is_active = True
            request.user.message_set.create(message=unicode(_("Project status successfully updated.")))
            project.save()
            return HttpResponseRedirect('.')
        elif 'markdone' in request.POST:
            handle_task_status(request)
    elif request.method == 'GET':
        createform = bforms.CreateProjectForm()


    subs = subs.order_by('-project__start_date')
    payload = {'subs': subs, 'createform':createform, 'invites':invites, 
               'includeinactive': includeinactive}
    if request.GET.get('csv', ''):
        response, writer = reponse_for_cvs()
        writer.writerow(('Project',))
        for sub in subs:
            writer.writerow((sub.project.name, ))
        writer.writerow(())        
        writer.writerow(('Project', 'Task Name', 'Due On'))
        for sub in subs:
            for task in sub.project.overdue_tasks():
                writer.writerow((task.project.name, task.name, task.expected_end_date))
        return response
    return render(request, 'project/projects.html', payload)

@login_required
def create_project(request):
    """
        Renders and saves a project form

    """
    user = request.user
    createform = bforms.CreateProjectForm()
    invites = user.inviteduser_set.filter(rejected = False)
    if request.method == 'POST':
        if request.POST.has_key('createproject'):
            createform = bforms.CreateProjectForm(user, request.POST)
            if createform.is_valid():
                new_project = createform.save()
                request.user.message_set.create(message=unicode(_("Project created successfully.")))
                return HttpResponseRedirect(new_project.get_absolute_url())
        elif request.POST.has_key('acceptinv'):
            project = Project.objects.get(id = request.POST['projid'])
            invite = InvitedUser.objects.get(id = request.POST['invid'])
            subscribe = SubscribedUser(project = project, user = user, group = invite.group)
            subscribe.save()
            invite.delete()
            return HttpResponseRedirect('.')
        elif request.POST.has_key('activestatus'):
            projid = request.POST['projectid']
            project = Project.objects.get(id = projid)
            if request.POST['activestatus'] == 'true':
                project.is_active = False
            elif request.POST['activestatus'] == 'false':
                project.is_active = True
            project.save()
            return HttpResponseRedirect('.')
        elif request.POST.has_key('markdone'):
            handle_task_status(request)
    elif request.method == 'GET':
        createform = bforms.CreateProjectForm()

    payload = {'form':createform, 'invites':invites}
    return render(request, 'project/create.html', payload)

@login_required
def project_details(request, project_name):
    """
    Point of entry for a specific project.
    Shows the important information for a project.
    Shows form to invite an user.
    Form to create a new top task.
    Actions available here:
    Invite: Owner
    New Top Task: Owner Participant
    Mark Done: Owner Participant
    """
    user = request.user
    project = get_project(request, project_name)
    access = get_access(project, request.user)
    inviteform = bforms.InviteUserForm()
    taskform = bforms.CreateTaskForm(project, user)
    new_tasks = project.new_tasks()
    new_tasks = project.new_tasks()
    overdue_tasks = project.overdue_tasks()
    if request.method == 'POST':
        if 'invite' in request.POST:
            inviteform = invite_user(request, project_name)
            if not inviteform.errors:
                inviteform = bforms.InviteUserForm()
        elif 'task' in request.POST:
            taskform = add_task(request, project_name)
            if not taskform.errors:
                taskform = bforms.CreateTaskForm(project, user)
        elif 'markdone' in request.POST or 'markundone' in request.POST:
            return handle_task_status(request, project_name)
        elif 'deletetask' in request.POST:
            return delete_task(request, project_name)
            
    if request.GET.get('csv', ''):
        response, writer = reponse_for_cvs()
        writer.writerow(Project.as_csv_header())
        writer.writerow(project.as_csv())
        writer.writerow(())
        writer.writerow(Task.as_csv_header())
        for task in new_tasks:
            writer.writerow(task.as_csv())
        writer.writerow(())    
        writer.writerow(Task.as_csv_header())
        for task in overdue_tasks:
            writer.writerow(task.as_csv())
        return response
    
    payload = {
            'project':project,
            'inviteform':inviteform,
            'taskform':taskform,
            'new_tasks':new_tasks,
            'overdue_tasks':overdue_tasks,
            'access':access,
            }
    return render(request, 'project/project.html', payload)


@login_required
def full_logs(request, project_name):
    """Shows the logs for a project.
    Actions available here:
    None"""
    project = get_project(request, project_name)
    access = get_access(project, request.user)
    query_set = Log.objects.filter(project = project)
    logs, page_data = get_paged_objects(query_set, request, logs_per_page)
    if request.GET.get('csv', ''):
        response, writer = reponse_for_cvs(project=project)
        writer.writerow((Log.as_csv_header()))
        for log in query_set:
            writer.writerow((log.as_csv()))
        return response
        
    payload = {'project':project, 'logs':logs, 'page_data':page_data}
    return render(request, 'project/fulllogs.html', payload)

@login_required
@give_access(["Owner"])
def settings(request, project_name):
    """Allows settings site sepcific settings."""
    project = get_project(request, project_name)
    access = get_access(project, request.user)
    if request.method == 'POST':
        username = request.POST['user']
        sub = SubscribedUser.objects.get(project__shortname = project_name, user__username = username)
        if request.POST.get('remove', ''):
            sub.delete()
            request.user.message_set.create(message=unicode(_("Successfully unsubscribed user from the project.")))
            if request.user.username == username:
                return HttpResponseRedirect(reverse("dashboard"))
            return HttpResponseRedirect(".")
        if request.POST.get('chgroup', ''):
            sub.group = request.POST['group']
            sub.save()
            request.user.message_set.create(message=unicode(_("Successfully changed group for the user.")))
            return HttpResponseRedirect('.')
    payload = {'project':project}
    return render(request, 'project/settings.html', payload)

@login_required
def noticeboard(request, project_name):
    """A noticeboard for the project.
    Shows the notices posted by the users.
    Shows the add notice form.
    Actions available here:
    Add a notice: Owner Participant Viewer (All)
    """
    project = get_project(request, project_name)
    addnoticeform = bforms.AddNoticeForm()
    if request.method == 'POST':
        addnoticeform = add_notice(request, project_name)
        if not addnoticeform.errors:
            addnoticeform = bforms.AddNoticeForm()
    if request.GET.get('csv', ''):
        response, writer = reponse_for_cvs(project=project)
        writer.writerow(Notice.as_csv_header())
        for notice in query_set:
            writer.writerow(notice.as_csv())
        return response
    query_set = Notice.objects.filter(project = project)
    notices, page_data = get_paged_objects(query_set, request, notices_per_page)
    payload = {'project':project, 
               'notices':notices, 
               'addnoticeform':addnoticeform, 
               'page_data':page_data,}
    return render(request, 'project/noticeboard.html', payload)

@login_required
@give_access(["Owner", "Participant"])
def todo(request, project_name):    
    """Allows to create a new todolist and todoitems.
    Actions available here:
    Add a todolist: Owner Participant
    Add a todoitem: Owner Participant
    """
    project = get_project(request, project_name)
    access = get_access(project, request.user)
    lists = TodoList.objects.filter(user = request.user, project = project, is_complete_attr = False)
    includecomplete = False
    if request.GET.get('includecomplete', 0):
        lists = TodoList.objects.filter(user = request.user, project = project, is_complete_attr = True)
        includecomplete = True
    addlistform = bforms.AddTodoListForm()
    if request.method == 'POST':
        if request.POST.has_key('addlist'):
            addlistform = bforms.AddTodoListForm(project, request.user, request.POST)
            if addlistform.is_valid():
                addlistform.save()
                request.user.message_set.create(message=unicode(_("To do list successfully created.")))
                return HttpResponseRedirect('.')
        elif request.POST.has_key('additem'):
            id = int(request.POST['id'])
            list = TodoList.objects.get(id = id)
            item_form = AddTodoItemForm(request.POST)
            if item_form.is_valid():
                item = TodoItem(list = list, text = item_form.cleaned_data["text"])
                item.save()
                request.user.message_set.create(message=unicode(_("To do item successfully created.")))
            else:
                request.user.message_set.create(message=unicode(item_form.errors["text"][0]))
        elif request.POST.has_key('listmarkdone'):
            id = int(request.POST['id'])
            list = TodoList.objects.get(id = id)
            list.is_complete = True
            list.save()
            request.user.message_set.create(message=unicode(_("To do list successfully updated.")))
            return HttpResponseRedirect('.')
        elif request.POST.has_key('itemmarkdone'):
            id = int(request.POST['id'])
            todoitem = TodoItem.objects.get(id = id)
            todoitem.is_complete = True
            todoitem.save()
            request.user.message_set.create(message=unicode(_("To do item successfully updated.")))
            return HttpResponseRedirect('.')
            
    if request.GET.get('csv', ''):
        response, writer = reponse_for_cvs(project=project)
        writer.writerow(('Todo Lists',))
        writer.writerow(TodoList.as_csv_header())
        lists = TodoList.objects.filter(user = request.user, project = project)
        for list in lists:
            writer.writerow(list.as_csv())
        for list in lists:
            for item in list.todoitem_set.all():
                writer.writerow(item.as_csv())
        return response
    payload = {'project':project, 'lists':lists, 'addlistform':addlistform,
               'access': access, 'includecomplete': includecomplete}
    return render(request, 'project/todo.html', payload)
