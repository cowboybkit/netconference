from django.contrib import admin
from project.models import Project, Task, TaskItem, TodoList, TodoItem, SubscribedUser, InvitedUser

admin.site.register(Project)
admin.site.register(Task)
admin.site.register(TaskItem)
admin.site.register(TodoList)
admin.site.register(TodoItem)
admin.site.register(SubscribedUser)
admin.site.register(InvitedUser)
