from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import Http404
from django.http import HttpResponseRedirect, HttpResponse
import csv

from django.core.paginator import Paginator, InvalidPage
from django.template.loader import get_template
from django.template import Context
import StringIO
import sx.pisa3 as pisa
import defaults

import BeautifulSoup as soup
from django.utils.translation import ugettext_lazy
from project.models import *
from project import bforms
from django.core.urlresolvers import reverse

_ = lambda x: unicode(ugettext_lazy(x))

def get_project(request, project_name):
    """Returns the project with the given name if the logged in user has access to the project. Raises 404 otherwise."""
    try:
        project = Project.objects.get(shortname = project_name)
    except Project.DoesNotExist:
        raise Http404
    try:
        project.subscribeduser_set.get(user = request.user)
    except SubscribedUser.DoesNotExist:
        if request.user.is_authenticated():
            request.user.message_set.create(message=_("You need to be a subscribed user to perform this action."))
    return project

def get_access(project, user):
    """Returns the access of the user passed for the project passed."""
    return SubscribedUser.objects.get(project = project, user = user).group


def give_access(perm_list):
    def decorator(fn):
        def wrapped(request, project_name, *args, **kwargs):
            user = request.user
            project = get_project(request, project_name)
            perm = get_access(project, user)
            if perm not in perm_list:
                user.message_set.create(message=_("You do not have the required permissions to perform the task. "
                                                  "Please contact the project administrator for more details."))
                return HttpResponseRedirect(request.META.get("HTTP_REFERER", project.get_absolute_url()))
            return fn(request, project_name, *args, **kwargs)
        return wrapped
    return decorator

def render(request, template, payload):
    """This populates the site wide template context in the payload passed to the template.
        It the job of this methods to make sure that, if user want to see the PDF they are able to see it.
    """
    if request.GET.get('pdf', ''):
        tarr = template.split('/')
        template = '%s/%s/%s' % (tarr[0], 'pdf', tarr[1])
        template = get_template(template)
        html = template.render(Context(payload))
        import copy
        hsoup = soup.BeautifulSoup(html)
        links = hsoup.findAll('a')
        for link in links:
            if not link['href'].startswith('http'):
                link['href'] = '%s%s' % (defaults.base_url, link['href'])
        html = StringIO.StringIO(str(hsoup))
        result = StringIO.StringIO()
        pdf = pisa.CreatePDF(html, result)
        if pdf.err:
            return HttpResponse(pdf.log)
        return HttpResponse(result.getvalue(), mimetype='application/pdf')
    if not payload.get('subs', ''):
        try:
            subs = request.user.subscribeduser_set.all()
            payload.update({'subs':subs})
        except AttributeError, e:
            pass
    #Populate the PDF path
    if request.META['QUERY_STRING']:
        pdfpath = '%s&pdf=1' % request.get_full_path()
    else:
        pdfpath = '%s?pdf=1'% request.get_full_path()
    payload.update({'pdfpath':pdfpath})
    return render_to_response(template, payload, RequestContext(request))

def get_pagination_data(obj_page, page_num):
    data = {}
    page_num = int(page_num)

    data['has_next_page'] = obj_page.has_next()
    data['next_page'] = page_num + 1
    data['has_prev_page'] = obj_page.has_previous()
    data['prev_page'] = page_num - 1
    data['first_on_page'] = obj_page.start_index()
    data['last_on_page'] = obj_page.end_index()
    return data

def get_paged_objects(query_set, request, obj_per_page):
    try:
        page = request.GET['page']
        page = int(page)
    except KeyError:
        page = 1
    paginator_obj = Paginator(query_set, obj_per_page)
    object_page = paginator_obj.page(page)
    page_data = get_pagination_data(object_page, page)
    page_data['total'] = paginator_obj.count
    return object_page.object_list.all(), page_data

@give_access(["Owner"])
def invite_user(request, project_name):
    project = get_project(request, project_name)
    inviteform = bforms.InviteUserForm(project, request.POST)
    if inviteform.is_valid():
        inviteform.save()
        request.user.message_set.create(message=unicode(_("Invite successfully sent.")))
    return inviteform

@give_access(["Owner", "Participant"])
def add_task(request, project_name):
    project = get_project(request, project_name)
    taskform = bforms.CreateTaskForm(project, request.user, request.POST)
    if taskform.is_valid():
        taskform.save()
        request.user.message_set.create(message=_("Task successfully created."))
    return taskform

@give_access(["Owner", "Participant"])
def add_sub_task(request, project_name, task_num):
    project = get_project(request, project_name)
    task = Task.objects.get(project=project, number=task_num)
    print request.POST
    addsubtaskform = bforms.CreateSubTaskForm(project, request.user, task, request.POST)
    if addsubtaskform.is_valid():
        addsubtaskform.save()
        request.user.message_set.create(message=_("Sub Task successfully created."))
    return addsubtaskform

@give_access(["Owner", "Participant"])
def add_item(request, project_name, task_num):
    project = get_project(request, project_name)
    task = Task.objects.get(project=project, number=task_num)
    additemform = bforms.CreateTaskItemForm(project, request.user, task, request.POST)
    if additemform.is_valid():
        additemform.save()
        request.user.message_set.create(message=_("Item successfully created."))
    return additemform

@give_access(["Owner", "Participant"])
def add_note(request, project_name, task_num):
    project = get_project(request, project_name)
    task = Task.objects.get(project=project, number=task_num)
    noteform = bforms.AddTaskNoteForm(task, request.user, request.POST)
    if noteform.is_valid():
        noteform.save()
        request.user.message_set.create(message=_("Note successfully created."))
    return noteform

@give_access(["Owner", "Participant"])
def handle_task_status(request, project_name):
    """Handle changes to status for a task. (Is_complete status toggle)."""
    id = request.POST['taskid']
    try:
        task = Task.objects.get(id = id)
    except Task.DoesNotExist:
        raise Http404
    if 'markdone' in request.POST:
        task.is_complete_prop = True
    else:
        task.is_complete_prop = False
    task.save()
    request.user.message_set.create(message=_("Task successfully updated."))
    if request.is_ajax():
        return task.id
    return HttpResponseRedirect('.')

@give_access(["Owner", "Participant"])
def handle_taskitem_status(request, project_name):
    """Handle changes to status for a taskitem. (Is_complete status toggle)."""
    id = request.POST['taskitemid']
    taskitem = TaskItem.objects.get(id = id)
    if request.POST.has_key('itemmarkdone'):
        taskitem.is_complete = True
    else:
        taskitem.is_complete = False
    taskitem.save()
    request.user.message_set.create(message=_("Task item successfully updated."))
    return HttpResponseRedirect('.')

@give_access(["Owner", "Participant"])
def delete_task(request, project_name=None):
    """Delete a task."""
    taskid = request.POST['taskid']
    task = Task.objects.get(id = taskid)
    task.delete()
    request.user.message_set.create(message=_("Task successfully deleted."))
    return HttpResponseRedirect('.')

def reponse_for_cvs(filename = 'filename.csv', project=None):
    response = HttpResponse(mimetype='text/csv')
    response['Content-Disposition'] = 'attachment; filename=%s' % filename
    writer = csv.writer(response)
    if project:
        writer.writerow(Project.as_csv_header())
        writer.writerow(project.as_csv())
        writer.writerow(())        
    return response, writer

@give_access(["Owner", "Participant"])
def add_notice(request, project_name):
    project = get_project(request, project_name)
    addnoticeform = bforms.AddNoticeForm(project, request.user, request.POST)
    if addnoticeform.is_valid():
        addnoticeform.save()
        request.user.message_set.create(message=_("Noticeboard updated."))
    return addnoticeform
