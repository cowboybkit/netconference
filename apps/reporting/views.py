from django.views.generic.list_detail import object_list
from django.db.models import Q
from django.contrib.auth.models import User

import datetime, time
from subscription.models import Subscription, UserSubscription, Referer
from django.contrib.admin.views.decorators import staff_member_required
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect

@staff_member_required
def reports(request):
    price_filters = ["free", "paid"]
    time_filters = ["today", "yesterday", "week", "month", "all_time", "date_range"]
    today = datetime.datetime.today()
    time_filters_map = {"today": today, 
                        "yesterday": today - datetime.timedelta(days=1), 
                        "week": today - datetime.timedelta(days=7), 
                        "month": today - datetime.timedelta(days=31), 
                        "all_time": UserSubscription.objects.order_by('user__date_joined')[0].user.date_joined,}
    sub_filters = ["basic", "standard", "premium", "pro"]
    referer_filters = Referer.objects.values("source").distinct()
    report_users = UserSubscription.active_objects.filter()
    if request.method == "GET":
        if request.GET.get("price") and request.GET["price"] in price_filters:
            free_subscription = Subscription.objects.get_free()
            if request.GET["price"] == "free":
                report_users = report_users.filter(subscription=free_subscription)
            if request.GET["price"] == "paid":
                qs = Q()
                for sub in Subscription.objects.exclude(pk=free_subscription.pk):
                    qs |= Q(pk=sub.pk)
                report_users = report_users.filter(qs)
        if request.GET.get("time") and request.GET["time"] in time_filters:
            if request.GET["time"] != "date_range":
                reqd_date = time_filters_map[request.GET["time"]]
                report_users = report_users.filter(user__date_joined__gte=reqd_date)
            if request.GET["time"] == "date_range":
                kwargs = {}
                if request.GET.get("start_date"):
                    start_date = datetime.datetime(*time.strptime(request.GET["start_date"], "%Y-%m-%d")[:3])
                    kwargs.update({"user__date_joined__gte": start_date})
                if request.GET.get("end_date"):
                    end_date = datetime.datetime(*time.strptime(request.GET["end_date"], "%Y-%m-%d")[:3])
                    kwargs.update({"user__date_joined__lte": end_date})
                report_users = report_users.filter(**kwargs)
        if request.GET.get("subscription") and request.GET["subscription"] in sub_filters:
            report_users = report_users.filter(subscription__name=request.GET["subscription"])
        if request.GET.get("referer"):
            report_users = report_users.filter(user__pk__in = Referer.objects.filter(source=request.GET["referer"]).values('user__id'))
        if not request.GET.keys():
            return HttpResponseRedirect("%s?time=today" %reverse("reports"))
    if request.method == "POST":
        query = request.POST.get("q")
        if query:
            report_users = report_users.filter(Q(user__first_name__icontains=query)|
                                               Q(user__last_name__icontains=query)|
                                               Q(user__email__icontains=query))
    return render_to_response("reporting/reports.html", {"report_users": report_users,
                                                         "referer_filters": referer_filters},
                              context_instance=RequestContext(request))

@staff_member_required
def free_users(request, filter_type = None):
    "Shows the various combinations of Free users"
    free_subscription = Subscription.objects.get_free()
    all_free_users = UserSubscription.active_objects.filter(subscription = free_subscription)
    if request.method == "POST":
        users_pk = request.POST.getlist("users[]")
        users = User.objects.filter(pk__in = users_pk)
        request.session["reporting_users"] = users
        return HttpResponseRedirect(reverse("vmail_compose"))
    if filter_type == "all":
        criteria = Q()
    elif filter_type == "week" or filter_type == None:
        days_ago = datetime.datetime.now() - datetime.timedelta(days = 7)
        criteria = Q(user__date_joined__gt = days_ago)
    elif filter_type == "day":
        days_ago = datetime.datetime.now() - datetime.timedelta(days = 1)
        criteria = Q(user__date_joined__gt = days_ago)
    elif filter_type == "month":
        days_ago = datetime.datetime.now() - datetime.timedelta(days = 30)
        criteria = Q(user__date_joined__gt = days_ago)
    filtered_free_user = all_free_users.filter(criteria)
    return object_list(request, queryset = filtered_free_user, template_name="reporting/free_users.html")

@staff_member_required
def paid_users(request, filter_type = None):
    free_subscription = Subscription.objects.get_free()
    paid_subs = Subscription.objects.exclude(pk = free_subscription.pk)
    paid_users_list = UserSubscription.active_objects.filter(subscription__in = paid_subs)
    if request.method == "POST":
        users_pk = request.POST.getlist("users[]")
        users = User.objects.filter(pk__in = users_pk)
        request.session["reporting_users"] = users
        return HttpResponseRedirect(reverse("vmail_compose"))
    if filter_type == "all":
        criteria = Q()
    elif filter_type == "week" or filter_type == None:
        days_ago = datetime.datetime.now() - datetime.timedelta(days = 7)
        criteria = Q(user__date_joined__gt = days_ago)
    elif filter_type == "day":
        days_ago = datetime.datetime.now() - datetime.timedelta(days = 1)
        criteria = Q(user__date_joined__gt = days_ago)
    elif filter_type == "month":
        days_ago = datetime.datetime.now() - datetime.timedelta(days = 30)
        criteria = Q(user__date_joined__gt = days_ago)
    paid_users_list = paid_users_list.filter(criteria)
    return object_list(request, queryset = paid_users_list, template_name="reporting/paid_users.html")
