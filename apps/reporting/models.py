from django.db import models
from django.contrib.auth.models import User

class ReportingUser(models.Model):
    "Keeps a track of reporting specific data on a per user basis."
    user = models.ForeignKey(User)
    contacted = models.BooleanField(default = False)
    last_contacted_on = models.DateTimeField(null = True, blank = True)
