from django.db import models
from django.contrib.auth.models import User
from subscription.models import Subscription
from settings import PRIMARY_AFFILIATE_COMMISION, SECONDARY_AFFILIATE_COMMISSION
# Create your models here.


class Affiliate(models.Model):
    user = models.ForeignKey(User,unique=True)
    referred_by = models.ForeignKey('self',blank=True,null=True)
    tier1_referrings = models.PositiveIntegerField(default=0)
    tier2_referrings = models.PositiveIntegerField(default=0)
    is_active = models.BooleanField(default=True)
    subscription = models.ForeignKey(Subscription,blank=True,null=True)
    
    def __unicode__(self):
        return "%s : by %s"%(self.user,self.referred_by)
    
    def save(self,*args,**kwargs):
        #On a new save, update 2 orders parents count
        super(Affiliate,self).save(*args,**kwargs)
        
    def get_primary_signups(self):
        return self.affiliate_set.all()
    
    def get_money_primary(self):
        return sum([el.subscription.price for el in self.get_primary_signups() if el.subscription])
    
    def get_secondary_signups(self):
        return Affiliate.objects.filter(referred_by__in=self.get_primary_signups())
        
    def get_money_secondary(self):
        return sum([el.subscription.price for el in self.get_primary_signups() if el.subscription])

    def get_total_money(self):
        return self.pri_commission() + self.sec_commission()
    
    def count_primary(self):
        return self.get_primary_signups().count()
    
    def count_secondary(self):
        return self.get_secondary_signups().count()
    
    def pri_commission(self):
        return self.subscription.price*PRIMARY_AFFILIATE_COMMISION if self.subscription else 0
        
    def get_total_count(self):
        return self.count_primary() + self.count_secondary()
    
    def sec_commission(self):
        return self.subscription.price*SECONDARY_AFFILIATE_COMMISSION if self.subscription else 0
        
    
    @models.permalink
    def get_absolute_url(self):
        return []
    
    @models.permalink
    def get_dashboard_url(self):
        return []
    
    
class AffiliateEmailLogManager(models.Manager):
    def get_email_count(self,user,to_email):
        return self.filter(user=user,to_email=to_email).count()



class AffiliateEmailLog(models.Model):
    to_email = models.EmailField()
    from_user = models.ForeignKey(User)
    timestamp = models.DateTimeField(auto_now_add=True)
    
    objects = AffiliateEmailLogManager()
    
    
