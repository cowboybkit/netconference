from affiliate.tests import AffiliateTest
from affiliate.models import Affiliate
from django.contrib.auth.models import User

def populate():
        
    users = []
    
    User.objects.filter(email__icontains='lennon')
    
    for el in range(15):
        users.append(User.objects.create_user('tq%s'%el,
                                              'lennon%s@thebeatles.com'%el,
                                              password='test'))
            
    a0 = Affiliate.objects.create(user=users[0])
    
    for el in [1,2,7]:
        Affiliate.objects.create(user=users[el],
                                 referred_by=a0)
        
    for el in [3,4]:
        Affiliate.objects.create(user=users[el],
                                 referred_by=Affiliate.objects.get(user=users[1]))

    for el in [5,6]:
        Affiliate.objects.create(user=users[el],
                                 referred_by=Affiliate.objects.get(user=users[2]))
    

if __name__=='__main__':
    populate()
    
