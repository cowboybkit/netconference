"""
This file demonstrates two different styles of tests (one doctest and one
unittest). These will both pass when you run "manage.py test".

Replace these with more appropriate tests for your application.
"""

from django.test import TestCase
from affiliate.models import Affiliate
from django.contrib.auth.models import User
from debug import ipython

class AffiliateTest(TestCase):

    def setUp(self):
        """
        Creating the following chain: 
        
        0
        -1
         -3
          -9
           -12
            -13
         -4
          -10
        -2
         -5
         -6
        -7
         -8
          -11
        
        """
        self.users = []
        for el in range(15):
            self.users.append(User.objects.create_user('t%s'%el,
                                                       'lennon%s@thebeatles.com'%el,
                                                       password='test'))
            
        a0 = Affiliate.objects.create(user=self.users[0])
        
        
        for el in [1,2,7]:
            Affiliate.objects.create(user=self.users[el],
                                     referred_by=a0)
            
        for el in [3,4]:
            Affiliate.objects.create(user=self.users[el],
                                     referred_by=Affiliate.objects.get(user=self.users[1]))

        for el in [5,6]:
            Affiliate.objects.create(user=self.users[el],
                                     referred_by=Affiliate.objects.get(user=self.users[2]))


            
            
    def test_2levels(self):
        """
        Tests that 1 + 1 always equals 2.
        """
        ipython()


class FormTest(TestCase):
    def test_subscribefrom(self):
        from affiliate.forms import SubscriptionSelectForm
        ssf = SubscriptionSelectForm()
        from pprint import pprint
        pprint(ssf)
        

