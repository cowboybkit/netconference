from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.shortcuts import render_to_response, redirect, get_object_or_404
from django.template import RequestContext
from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login
from django.http import HttpResponse
from django.template.loader import render_to_string
from django.conf import settings


from affiliate.models import Affiliate
from affiliate.forms import SubscriptionSelectForm
from account.forms import SignupForm
from affiliate.utils import add_affiliate, add_free_req_affiliate
from affiliate.forms import AffiliateSignupForm
from subscription.extras import FREE_SUBSCRIPTION
from friends.models import Contact
from friends_app.views import contacts
from cart.cart import add_given_subscription
#from django.core.mail import send_mail
from mailer import send_html_mail as send_mail
from django.contrib.sites.models import Site
from django.utils.translation import ugettext_lazy as _
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

@login_required
def dashboard(request):
    has_referred = bool(request.user.affiliate_set.count())

    payload = {'has_referred':has_referred,
               'nav_current':'dashboard_nav'}

    if has_referred:
        payload['affiliate'] = request.user.affiliate_set.get()

    return render_to_response('affiliate/dashboard.html',
                              payload,
                              RequestContext(request))


def signup(request, username):
    if request.user.is_authenticated():
        request.user.message_set.create(message=unicode(_(u'You are already logged in to NetConference')))
        return redirect('home')

    referring_user = get_object_or_404(User, username=username)
    request.session['referring_user'] = referring_user
    response = HttpResponseRedirect(reverse("subscription_list"))
    response.set_cookie(key='referring_username', value=referring_user)
    return response


def subscribe(request):
    if request.method == 'POST':
        form = SubscriptionSelectForm(request.POST)
        if form.is_valid():
            from subscription.extras import FREE_SUBSCRIPTION
            if form.cleaned_data['subscription'] == FREE_SUBSCRIPTION:
                form.save(user=request.user)
                add_affiliate(request.user,
                              subscription=form.cleaned_data['subscription'],
                              request=request)
                request.user.message_set.create(message=_(u'Your free account is activated'))
                return redirect('home')
            else:
                #Create the cart and send him to checkout
                add_given_subscription(request, form.cleaned_data['subscription'])
                return redirect('checkout')

    return render_to_response('affiliate/subscription_list.html',
                              {'form':SubscriptionSelectForm()},
                              RequestContext(request))


def show_affiliate_mailsend(request):
    extra_context = {
        'subnav_current':'affiliate_invite',
        'invite_affiliate': True,
    }
    return contacts(request, show_affiliate=True, extra_context=extra_context)
    
    


@login_required
def invite(request, contact_id):
    contact = get_object_or_404(Contact, id=contact_id)
    contact_email = contact.email
    sending_name = contact.name or contact_email.split('@')[0]
    message = request.POST.get('custom_message', None)
    message_body =  render_to_string('affiliate/email_content.txt',
                                     {'sending_name':sending_name,
                                      'site': Site.objects.get_current(),
                                      'message': message},
                                     RequestContext(request))
    mail = send_mail(_(u'You are invited to join Netconference'),
                     "",
                     message_body,
                     from_email=request.user.email or settings.DEFAULT_FROM_EMAIL,
                     recipient_list=[contact_email, ])
    
    return HttpResponse('<h3>Sent<h3>')
