from affiliate.models import Affiliate
from django.shortcuts import get_object_or_404
from django.contrib.auth.models import User

def add_affiliate(user,subscription,referring_user):
    referring_affiliate = referring_user.affiliate_set.get_or_create()[0]
    referring_affiliate.subscription = referring_user.usersubscription_set.all()[0].subscription
    referring_affiliate.save()
    try:
        affiliate_user = Affiliate.objects.get(user=user)
        affiliate_user.subscription = subscription
        affiliate_user.referred_by = referring_affiliate
        affiliate_user.save()
    except:
        Affiliate.objects.create(user=user,
                                 subscription=subscription,
                                 referred_by=referring_affiliate)

def add_affiliate_req(user,request,subscription):
    referring_user = request.session.get('referring_user','')
    if not referring_user:
        referer_str = request.COOKIES.get('referring_username','')
        if referer_str:
            try:
                referring_user = User.objects.get(username=referer_str)
            except User.DoesNotExist:
                return
    if referring_user:
        add_affiliate(user,subscription,referring_user)


def add_free_req_affiliate(request):
    """This method should be called only after a signup.
    """
    
    #This creates free subscription, if none exists.
    affiliate_subscription = request.user.get_subscription()
    return add_affiliate_req(request.user,request,affiliate_subscription)
