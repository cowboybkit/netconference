from account.forms import UserDetailsSignupForm
from django import forms
from django.shortcuts import redirect
from django.contrib.formtools.wizard import FormWizard
from cart.cart import add_given_subscription
from debug import ipython,idebug
from django.contrib.auth import login, authenticate
from affiliate.models import Affiliate
from account.forms import SignupForm
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _

class AffiliateSignupForm(UserDetailsSignupForm):
    
    def save(self):
        data = self.cleaned_data
        new_user = User.objects.create_user(username=data['username'],
                                            password=data['password1'],
                                            email=data['email'])
        user_profile = new_user.profile_set.get()
        user_profile.phone = self.cleaned_data['phone_number']
        user_profile.save()
        return data['username'],data['password1']


class AffiliateSignup(FormWizard):
    
    extra_context = {'post_url':"."}
    
    def done(self,request,form_list):
        signup_form = form_list[0]
        sub_form = form_list[1]
        from subscription.extras import FREE_SUBSCRIPTION
        if sub_form.cleaned_data['subscription'] == FREE_SUBSCRIPTION:
            username,password = signup_form.save()
            auth_user = authenticate(username=username, password=password)
            login(request,auth_user)
            sub_form.save(request.user)
            request.user.message_set.create(message=_('Successfully signedup for free account'))
            ipython()
            #Affiliate.objects.create(user=request.user,
             #                        referred_by=)
            return redirect()
    
    def get_template(self,step):
        if step==0:
            return 'affiliate/signup.html'
        elif step==1:
            return 'affiliate/subscription_list.html'
        
    def process_step(self, request, form, step):
        
        if step==1:
            subscription= form.cleaned_data['subscription']
        
        
from subscription.models import Subscription, UserSubscription
        
class SubscriptionSelectForm(forms.Form):
    subscription = forms.ModelChoiceField(Subscription.objects.available(),
                                          empty_label='Choose One')
    
    def save(self,user):
        us = UserSubscription.objects.get_or_create(subscription=self.cleaned_data['subscription'],
                                                    user=user)
        return us
            
    
def update_affiliate():
    pass
