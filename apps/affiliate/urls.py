from django.conf.urls.defaults import *
from django.views.generic.simple import direct_to_template

urlpatterns = patterns('affiliate.views',
                       url(r'^$', 'dashboard', name="affiliate_dashboard"),
                       url(r'^subscribe/$',
                           'subscribe',
                           name="affiliate_subscribe"),
                       url(r'^opportunity/$',
                           direct_to_template,
                           {'template':'affiliate/opportunity.html',
                            'extra_context':{'nav_current':'oppor_nav'}},
                           name="affiliate_opportunity"),
                       url(r'invite/$',
                           'show_affiliate_mailsend',
                           name='affiliate_invite'),
                       url(r'email_affiliate_ajax/(?P<contact_id>\d+)/',
                           'invite',
                           name="email_affiliate_ajax"),
                       url(r'(?P<username>[\w\._-]+)/$',
                           'signup',
                           name='affiliate_signup'),
                       )
