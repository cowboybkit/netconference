from vo import ContactVO, ContactGroupVO
from friends.models import Contact
from friends_app.models import ContactGroup
from django.contrib.auth.models import User

class ContactService:
    def getContacts(self, request, username):
        user = User.objects.get(username = username)
        contacts = Contact.objects.filter(user = user).order_by('name')
        result = ContactVO.fromContacts(query = contacts)
        return result
    
    def getContactGroups(self, request, username):
        user = User.objects.get(username = username)
        groups = ContactGroup.objects.filter(owner = user).order_by('name')
        result = ContactGroupVO.fromGroups(query = groups)
        return result
