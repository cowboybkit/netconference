from files.models import UserFile, FileFolder
from pyamf.flex import ArrayCollection;
from django.db import models;
from tagging.models import Tag
import sys
from schedule.models import *
from conferencing.models import Conference, SharedFile, Attendees
import vo;
from vo import FileVO, FolderVO;
from django.http import Http404
from django.contrib.auth.models import get_hexdigest
import random
from django.core.paginator import Paginator
import datetime
from django.contrib.auth.models import User
from conferencing.models import RecordedConference
from django.shortcuts import get_object_or_404
from tagging.models import Tag
from netconf_utils.encode_decode import get_object_from_hash
from django.db.models import Q

class FileService:
    def getMyFiles(self, request, username):
        """ authenticate here """
        query = UserFile.active_objects.filter(user__username=username).order_by('title');
        my_files = FileVO.fromUserFiles(query); #vo.fromUserFiles(query);
        return my_files;
    def getPublicFiles(self, request, urlhash):
        event_id = int(uri_b64decode(str(urlhash))) / 42;
        conference = Event.objects.get(pk=event_id);
        query = SharedFile.objects.filter(conference=conference);
        shareFiles = FileVO.fromSharedFiles(query); #vo.fromSharedFiles(query);
        return shareFiles;
    def shareFile(self, request, file_id, urlhash):
        try:
            event_id = int(uri_b64decode(str(urlhash))) / 42
            conference = Event.objects.get(pk=event_id);
            obj = UserFile.active_objects.get(id=file_id)
            SharedFile.objects.get_or_create(owner=obj.user, conference=conference, file=obj)
            answer = True
        except:
            answer = False;
        return answer;
    def removeSharedFile(self, request, file_id, urlhash):
        try:
            event_id = int(uri_b64decode(str(urlhash))) / 42
            conference = Event.objects.get(pk=event_id);
            obj = UserFile.active_objects.get(id=file_id);
            SharedFile.objects.get(conference=conference, file=obj).delete();
            answer = True
        except:
            answer = False;
        return answer;

    def getDocInfo(self, request, doc_id):
        try:
            uf = UserFile.active_objects.get(id=doc_id)
        except UserFile.DoesNotExist:
            raise Http404
        return {"file_id": uf.id,
                "pagecount": uf.page_count,
                "source_type": uf.source_type,
                "title": uf.title,
                "uuid": uf.uuid,
                }
    def removeTagFile(self, request, file_id, tag_list):
        '''remove some tag in the file '''
        try:
            usr_file = UserFile.active_objects.get(id=file_id)
        except UserFile.DoesNotExist:
            return False
        str = usr.tags_string
        if not tag_list:
            return False
        else:
            for t in tag_list:
                str.replace(t, '')
            Tag.objects.update_tags(usr_file, str)
            return True
    """ Update Information UserFile   """
    def updateFileInfo(self, request, file_id, name, title, description, tags):
        ufile = UserFile.active_objects.get(id=file_id)
        if not name:
            name = ufile.filename
        if not description:
            description = ufile.description
        if not title:
            title = ufile.title
        ufile.title = title
        ufile.name = name
        ufile.description = description
        ufile.save()
        
        Tag.objects.update_tags(ufile, tags)
        str_tag = Tag.objects.get_for_object(ufile)
        ufile.tags_string = ','.join([str(x) for x in str_tag])
        
        ufile.save()
        
    def updateVisibilityFile(self,request,file_id,visibility,password):
        if not file_id :
            return "ID File is None"
        try:
            ufile = UserFile.active_objects.get(id=file_id)
        except UserFile.DoesNotExist:
            return "File not exists or not active"
        if visibility in ['R','U','O']:
            if visibility =='O':
                if password:
                    algo = "sha1"
                    salt = get_hexdigest(algo, str(random.random()), str(random.random()))[:5]
                    hashed_password = get_hexdigest(algo, salt, password)
                    ufile.password = "%s$%s$%s" %(algo, salt, hashed_password)
                    ufile.visibility = visibility
                else:
                    return "Password is Empty"
            else:
                ufile.visibility = visibility
            try:
                ufile.save()
            except:
                return "When save file:"+sys.exc_info()[0]
        else:
            return "visibility not in enum"
        
        return True
    
    """ Delete a File """
    def deleteFile(self,request,file_id):
        if not file_id :
            return "ID File is None"
        try:
            ufile = UserFile.active_objects.get(id=file_id)
        except UserFile.DoesNotExist:
            return "File not exists or not active"
        try:
            ufile.delete()
        except:
            return  "Error when delete file"
        return True
    
    def deleteSelectedFiles(self,request,deleteList):
        for item in deleteList:
            if hasattr(item, 'parentID'):
                folder = FileFolder.objects.get(pk = item.id)
                folder.move_to_trash()
                folder.auto_update_info()
            else:
                file = UserFile.objects.get(pk = item.id)
                parent = file.folder
                file.is_deleted = True
                file.is_mark = True
                file.save()
                if parent:
                    parent.auto_update_info()
        
    
    def getPagingFiles(self, request, username, sortType, page, numRow):
        queryset = UserFile.active_objects.filter(user__username=username).order_by(sortType)
        paginator = Paginator(queryset, numRow, allow_empty_first_page=True)
        page_obj = paginator.page(page)
        current_files = None
        if page_obj:
            current_files = FileVO.fromUserFiles(page_obj.object_list)
        return current_files
        
    def playUploadFile(self, request, id):
        """ authenticate here """
        query = UserFile.active_objects.get(pk=id)
        my_files = FileVO(query)
        return my_files
    def getPagingFilesWithCondition(self, request,  username, title, type , fromDate, toDate, page , numRow, folderId):
        if folderId ==0:
            queryset = UserFile.active_objects.filter(user__username=username).filter(Q(folder__is_root=True) | Q(folder=None) ).order_by('-last_modified')
        else:
            queryset = UserFile.active_objects.filter(user__username=username).filter(folder__id = folderId).order_by('-last_modified')
        if title:
            queryset.filter(title__contains = title)
        if type:
            queryset.filter(source_type__in = type)
        if fromDate and toDate:
            queryset = queryset.filter(timestamp__gt = fromDate, timestamp__lt = (toDate + datetime.timedelta(days=1)))
            
        paginator = Paginator(queryset, numRow, allow_empty_first_page=True)
        try:
            page_obj = paginator.page(page)
        except:
            page_obj = None
        current_files = list()
        
        if page == 1:
            if folderId ==0:
                folders = FileFolder.objects.filter(user__username = username).filter(Q(parent_folder__is_root=True) | Q(parent_folder=None) ).exclude(name='My Files').order_by('name')
            else:
                back_folder = FileFolder.objects.get(pk = folderId)
                back_folder_vo = FolderVO(back_folder, False)
                current_files.append(back_folder_vo)
                
                folders = FileFolder.objects.filter(user__username = username, parent_folder__id = folderId).order_by('name')
            
            sub_folders = FolderVO.from_folders(username,folders,False)
            current_files.extend(sub_folders)
            
                
                
        
        if page_obj:
            files = FileVO.fromUserFiles(page_obj.object_list)
            current_files.extend(files)
            
        return current_files
    
    def createRecording(self, request, userid, uuid, duration, title):
        user = User.objects.get(pk=userid)
        file = UserFile(uuid=uuid,
                        user = user,
                        timestamp= datetime.datetime.now(),
                        processed = 1,
                        file_size = 0,
                        is_active = True,
                        title=title,
                        filename = title,
                        source_type = 'rec_conf',
                        current_type = 'mp4',
                        duration = duration,
                        visibility ='U',
                        last_modified = datetime.datetime.now(),
                        )
        file.save()
        conf = RecordedConference(user = user,
                                  created = datetime.datetime.now(),
                                  duration = duration,
                                  fms_hostroom = uuid,
                                  )
        conf.save()
        return file.id
    
    def updateRecording(self, request, uuid, width, height, sizefile):
        file = UserFile.active_objects.get(uuid = uuid)
        file.dimension_width = width
        file.dimension_height = height
        file.file_size = sizefile
        file.save()
        return file.id
    
    def getChildFolder(self, request, username, master):
        user = User.objects.get(username=username)
        
        file_folders = []
        
        query_root = user.get_root_folder()
        root_folders = FolderVO.from_folders(username,query_root, True)
        
        query_system = user.get_system_folder()
        system_folders = FolderVO.from_folders(username, query_system, True)
        
        query_trash = user.get_trash_folder()
        trash_folders = FolderVO.from_folders(username, query_trash, True)
        
        file_folders.extend(root_folders)
        file_folders.extend(system_folders)
        file_folders.extend(trash_folders)
        
        return file_folders
    
    def getFileByID(self, request, username, fileId):
        file = get_object_or_404(UserFile, pk= fileId)
        if file.user.username !=username:
            return None
        file_vo = FileVO(file)
        return file_vo
    
    def getFileByFolderID(self, request, username, folderId):
        
        if folderId == 0:
            query = UserFile.active_objects.filter(user__username = username, is_deleted = False).filter(Q(folder__is_root=True) | Q(folder = None)).order_by('last_modified')
            folders = FileFolder.objects.filter(user__username = username, parent_folder__is_root=True).order_by('name')
        else:
            folder = FileFolder.objects.get(pk=folderId)
            if folder.is_system:
                query = UserFile.active_objects.filter(folder = folder, is_deleted = False).order_by('last_modified')
            elif folder.is_trash:
                query = UserFile.active_objects.filter(user__username = username, is_deleted = True).order_by('last_modified')
            elif folder.is_root:
                query = UserFile.active_objects.filter(user__username = username, is_deleted = False).filter(Q(folder__is_root=True) | Q(folder = None)).order_by('last_modified')
            else:
                query = UserFile.active_objects.filter(user__username = username, folder = folder, is_deleted = False).order_by('last_modified')
                
            if folder.is_system:
                folders = FileFolder.objects.filter(parent_folder__id = folderId, is_deleted = False).order_by('name')
            if folder.is_trash:
                folders = FileFolder.objects.filter(user__username = username,is_deleted = True, is_mark = True).order_by('name')
            else:
                folders = FileFolder.objects.filter(user__username = username, parent_folder__id = folderId, is_deleted = False).order_by('name')
        
        filefolder_sets = []    
        my_files = FileVO.fromUserFiles(query)
        sub_folders = FolderVO.from_folders(username,folders,False)
        filefolder_sets.extend(sub_folders)
        filefolder_sets.extend(my_files)
        
        return filefolder_sets
    
    def searchFilesWithConditions(self, request, username, manualTypingValue, arrayTypeOfFile, fromDate, toDate):
     
        query = UserFile.active_objects.filter(user__username=username, is_deleted = False)
        if manualTypingValue:
            query = query.filter(title__contains = manualTypingValue)
        if arrayTypeOfFile:
            query = query.filter(source_type__in = arrayTypeOfFile)
        if fromDate and toDate:
            query = query.filter(timestamp__gt = fromDate, timestamp__lt = (toDate + datetime.timedelta(days=1)))
        if 'flag' in arrayTypeOfFile and 'unFlag' in arrayTypeOfFile:
            pass
        elif 'flag' in arrayTypeOfFile:
            query = query.filter(is_flag = True)
        elif 'unFlag' in arrayTypeOfFile:
            query = query.filter(is_flag = False)
        
        my_files = FileVO.fromUserFiles(query)
        return my_files
    
    def searchPublicFilesWithConditions(self, request, conferenceHash, manualTypingValue, arrayTypeOfFile, fromDate, toDate):
    
        event_id = int(uri_b64decode(str(conferenceHash))) / 42
        conference = Event.objects.get(pk=event_id)
        query = SharedFile.objects.filter(conference=conference, file__is_deleted = False)
        
        if manualTypingValue:
            query = query.filter(file__title__contains = manualTypingValue)
        if arrayTypeOfFile:
            query = query.filter(file__source_type__in = arrayTypeOfFile)
        if fromDate and toDate:
            query = query.filter(file__timestamp__gt = fromDate, file__timestamp__lt = (toDate + datetime.timedelta(days=1)))
        if 'flag' in arrayTypeOfFile and 'unFlag' in arrayTypeOfFile:
            pass
        elif 'flag' in arrayTypeOfFile:
            query = query.filter(is_flag = True)
        elif 'unFlag' in arrayTypeOfFile:
            query = query.filter(is_flag = False)
            
        shareFiles = FileVO.fromSharedFiles(query)
        return shareFiles
    
from django.http import HttpResponse, HttpResponseRedirect, Http404
def createRecordingFlush(request):
    if request.method == 'POST':
        accesskey = "NxtGenTV2006AccessKEY2012UNIQUE"
        if 'accesskey' and 'userid' and 'uuid' and 'duration' and 'title' and 'width' and 'height' and 'filesize' in request.POST:
            if request.POST['accesskey']==accesskey:
                user = get_object_or_404(User, pk=int(request.POST['userid']))
                file = UserFile(uuid=request.POST['uuid'],
                        user = user,
                        timestamp= datetime.datetime.now(),
                        processed = 1,
                        file_size = request.POST['filesize'],
                        is_active = True,
                        title=request.POST['title'],
                        filename = request.POST['title'],
                        source_type = 'rec_conf',
                        current_type = 'mp4',
                        duration = request.POST['duration'],
                        visibility ='U',
                        last_modified = datetime.datetime.now(),
                        dimension_width = request.POST['width'],
                        dimension_height = request.POST['height'],
                        )
                file.save()
                event = None
                if 'conferencehash' in request.POST:
                    event = get_object_from_hash(Event, request.POST['conferencehash'])
                
                conf = RecordedConference(user = user,
                                          created = datetime.datetime.now(),
                                          duration = request.POST['duration'],
                                          fms_hostroom = request.POST['uuid'],
                                          conference = event
                                          )
                conf.save()
                return HttpResponse(str(file.id))  
    raise Http404
    
def createUploadFile(request):
    accesskey = "NxtGenTV2006AccessKEY2012UNIQUE"
    if request.REQUEST['accesskey'] == accesskey:
        user = get_object_or_404(User, pk=int(request.REQUEST['user_id']))
        folder = None
        if 'folder_id' in request.REQUEST:
            folder_id  = int(request.REQUEST['folder_id'])
            if folder_id!=-1 and folder_id!=0:
                folder = get_object_or_404(FileFolder, pk=folder_id)
        file = UserFile(uuid    =request.REQUEST['uuid'],
                user            = user,
                timestamp       = datetime.datetime.now(),
                processed       = 1,
                file_size       = request.REQUEST['file_size'],
                is_active       = True,
                title           = request.REQUEST['title'],
                filename        = request.REQUEST['filename'],
                source_type     = request.REQUEST['source_type'],
                current_type    = request.REQUEST['current_type'],
                duration        = request.REQUEST['duration'],
                visibility      = request.REQUEST['visibility'],
                last_modified   = datetime.datetime.now(),
                dimension_width = request.REQUEST['dimension_width'],
                dimension_height = request.REQUEST['dimension_height'],
                tags_string     = request.REQUEST['tags_string'],
                page_count      = request.REQUEST['page_count'],
                folder          = folder,
                )
        file.save()
        Tag.objects.update_tags(file, file.tags_string)
        return HttpResponse(str(file.id))
    raise Http404
        