import tagging
from tagging.utils import *
from netconf_utils.encode_decode import uri_b64encode, uri_b64decode, get_object_from_hash
from ccall_pins.models import ConferencePin
from subscription.models import UserSubscription
from ccall_pins.models import ConferenceBridgeNumber
from user_theme.models import UserTheme
from schedule.models import *
import sys
from avatar.templatetags.avatar_tags import avatar_url
from timezones.utils import adjust_datetime_to_timezone
from django.conf import settings
from django.contrib.auth.models import User
from timezones.forms import TimeZoneField
import datetime
from files.models import UserFile
from conferencing.models import SharedFile, Attendees, RecordedConference, ReminderConference,TimezoneConference
from django.shortcuts import get_object_or_404
from django.template.loader import render_to_string
from django.contrib.sites.models import Site

class FileVO:
    def __init__(self, userfile=None):
        if userfile is not None:
            self.id = userfile.id;
            self.title = userfile.title;
            self.filename = userfile.filename;
            self.source_type = userfile.source_type;
            self.current_type = userfile.current_type;
            self.page_count = userfile.page_count;
            self.duration = userfile.duration;
            self.user = userfile.user.username;
            self.uuid = userfile.uuid;
            if userfile.child_uuid:
                self.uuid = userfile.child_uuid;
            self.visibility = userfile.visibility
            tags = []
            if not userfile.tags_string:
                pass
            else:
                arr_tag = get_tag_list(userfile.tags_string)
                for i in arr_tag:
                    tags.append(i.name)
            self.tags = tags;
            self.created_date = userfile.timestamp
            self.description = userfile.description
            self.fileSize = userfile.file_size
            self.lastModified = userfile.last_modified
            self.dimensionWidth = userfile.dimension_width
            self.dimensionHeight = userfile.dimension_height
            self.password = userfile.password
            
            sharefile = SharedFile.objects.filter(file=userfile)
            self.isEditable = False
            if sharefile:
                self.isEditable = True
            
            self.viewsTotal = userfile.views_total
            self.isFlag = userfile.is_flag
            self.isDeleted = userfile.is_deleted
            self.isSystem = userfile.is_system
            
            
            
    @staticmethod
    def fromUserFiles(userfiles):
        files = list();
        for file in userfiles:
            vo = FileVO(file);
            files.append(vo);
        return files;
    
    @staticmethod
    def fromSharedFiles(sharedfiles):
        files = list();
        for sharedfile in sharedfiles:
            vo = FileVO(sharedfile.file);
            files.append(vo);
        return files;
    
class ConferenceVO:
    def __init__(self, event=None):
        if event is not None:
            self.title = event.title
            self.conferenceHash = uri_b64encode(str(event.id * 42))
            self.ccallPin = ConferencePin.objects.get(id=int(event.creator.id))
            self.host = event.creator.username
            self.description = event.description
#            user_subs = UserSubscription.objects.get(user=event.creator)
#            subs = user_subs.subscription;
            user_subs = event.creator.get_usersubscription()
            subs = event.creator.get_subscription()
            self.guestsLimit = subs.guests_limit;
            self.conferenceMinutesLimit = subs.conference_minutes_limit
            self.minutesUsed = user_subs.minutes_used
            self.typeConf = event.type
            self.phonenumber = event.creator.get_conference_phonenumber()
            self.modcode = event.creator.get_conference_modcode()
            self.attendeecode = event.creator.get_conference_attendeecode()
            self.hostUser = event.creator.get_profile().name
            self.hostEmail = event.creator.email
            self.start = adjust_datetime_to_timezone(event.start, settings.TIME_ZONE, event.timezone)
            self.end = adjust_datetime_to_timezone(event.end, settings.TIME_ZONE, event.timezone)
            self.timezone = 'GMT%s:%s'%(self.start.strftime('%z') , event.timezone)
            self.avatar = avatar_url(event.creator,200).split('?Signature')[0]
            self.is_recorded = event.is_recorded
            
            ''' Addition info '''
            self.isTelephone = event.is_telephone
            self.passwordRequired = event.password_required
            self.hostMustPresent = event.host_must_present
            self.guestHaveApproval = event.guest_have_approval
            self.hostControlLayout = event.host_control_layout
            self.layoutType = event.layout_type
            self.viewGuestList = event.view_guest_list
            self.broadcastVideo = event.broadcast_video
            self.broadcastAudio = event.broadcast_audio
            self.chatWithHost = event.chat_with_host
            self.chatWithGuest = event.chat_with_guest
            
            
    @staticmethod
    def fromEvents(query=None):
        if query is not None:
            events = list();
            for item in query:
                vo = ConferenceVO(item);
                events.append(vo);
            return events;
        return None;
    
    @staticmethod
    def editFrom(obj):
        try:
            event_id = int(uri_b64decode(str(obj.conferenceHash))) / 42
            event  = Event.objects.get(pk=event_id)
            event.title = obj.title
            event.description = obj.description
#            user_subs = UserSubscription.objects.get(user=event.creator)
#            subs = user_subs.subscription
            user_subs = event.creator.get_usersubscription()
            subs = event.creator.get_subscription()
            subs.guests_limit = obj.guestsLimit
            subs.conference_minutes_limit = obj.conferenceMinutesLimit
            subs.save()
            user_subs.minutes_used = obj.minutesUsed
            user_subs.save()
            
            
            event.start = obj.start
            event.end = obj.end 
            event.timezone = obj.timezone
            event.is_recorded = obj.is_recorded
            
            event.is_telephone = obj.isTelephone
            event.password_required = obj.passwordRequired 
            event.host_must_present = obj.hostMustPresent
            event.guest_have_approval = obj.guestHaveApproval
            event.host_control_layout = obj.hostControlLayout
            event.layout_type = obj.layoutType
            event.view_guest_list = obj.viewGuestList
            event.broadcast_video = obj.broadcastVideo
            event.broadcast_audio = obj.broadcastAudio
            event.chat_with_host = obj.chatWithHost
            event.chat_with_guest = obj.chatWithGuest
            
            event.save()
            return "Success"
        except:
            return str(sys.exc_info()[0])

class ContactVO:
    def __init__(self, contact=None):
        if contact is not None:
            self.id     = contact.id
            self.name   = contact.name
            self.email  = contact.email
            self.phone  = contact.phone
    
    @staticmethod
    def fromContacts(query=None):
        if query is not None:
            contacts = list()
            for item in query:
                vo = ContactVO(item)
                contacts.append(vo)
            return contacts
        return None
    
class ContactGroupVO:
    def __init__(self, group=None):
        if group is not None:
            self.id = group.id
            self.name = group.name
    
    @staticmethod
    def fromGroups(query=None):
        if query is not None:
            groups = list()
            for item in query:
                vo = ContactGroupVO(item)
                groups.append(vo)
            return groups
        return None

    
class ConferenceRoomVO:
    def __init__(self, event=None):
        if event is not None:
            self.title = event.title
            self.host = event.creator.username
            
            self.hostUser = event.creator.get_full_name()
            self.hostEmail = event.creator.email
            self.description = event.description
            self.start = adjust_datetime_to_timezone(event.start, settings.TIME_ZONE, event.timezone)
            self.end = adjust_datetime_to_timezone(event.end, settings.TIME_ZONE, event.timezone)
            self.timezone = 'GMT%s:%s'%(self.start.strftime('%z') , event.timezone)
            
            query_timezones = TimezoneConference.objects.filter(event = event)
            extra_timezones = []
            for extra in query_timezones:
                extra_timezones.append('GMT%s:%s'%(self.start.strftime('%z') , extra.timezone))
            
            self.extraTimeZone = extra_timezones
            
            self.isPrivate = event.private
            self.rule = 0
            if event.rule:
                self.rule = event.rule.id
            self.recurringPeriod  = event.end_recurring_period
            self.reminderInterval  = event.reminder_interval
            self.conferenceHash = uri_b64encode(str(event.id * 42))
            self.isRecorded = event.is_recorded
            self.createdOn = adjust_datetime_to_timezone(event.created_on, settings.TIME_ZONE, event.timezone)
            self.invitecount = len(event.get_recipient_list())
            self.weekly= None
            self.monthly= None
            if self.rule !=0:
                if event.rule.frequency == "WEEKLY":
                    self.weekly = event.weekly
                if event.rule.frequency == "MONTHLY":
                    self.monthly = event.monthly
                    
            self.phonenumber = event.creator.get_conference_phonenumber()
            self.modcode = event.creator.get_conference_modcode()
            self.attendeecode = event.creator.get_conference_attendeecode()
            self.typeConf = event.type
            
            ''' Addition info '''
            self.isTelephone = event.is_telephone
            self.passwordRequired = event.password_required
            self.hostMustPresent = event.host_must_present
            self.guestHaveApproval = event.guest_have_approval
            self.hostControlLayout = event.host_control_layout
            self.layoutType = event.layout_type
            self.viewGuestList = event.view_guest_list
            self.broadcastVideo = event.broadcast_video
            self.broadcastAudio = event.broadcast_audio
            self.chatWithHost = event.chat_with_host
            self.chatWithGuest = event.chat_with_guest
            self.rememberPassword = event.remember_password
            
            self.isValid = event.is_valid
            self.isFlag = event.is_flag
            
    @staticmethod
    def fromEvents(query=None):
        if query is not None:
            events = list();
            for item in query:
                vo = ConferenceRoomVO(item);
                events.append(vo);
            return events;
        return None;
    
    @staticmethod
    def create(room = None):
        if room is not None:
            user = User.objects.get(username = room.host)
            user_timezone = TimeZoneField().clean(room.timezone)
            
            start = adjust_datetime_to_timezone(room.start, room.timezone, settings.TIME_ZONE).replace(tzinfo = None)
            end = adjust_datetime_to_timezone(room.end, room.timezone, settings.TIME_ZONE).replace(tzinfo = None)
            
            recurring_period = None
            if room.recurringPeriod:
                recurring_period = adjust_datetime_to_timezone(room.recurringPeriod, room.timezone, settings.TIME_ZONE).replace(tzinfo = None)
            is_recur = False
            if room.rule == 0:
                s_rule = None
            else:
                s_rule = Rule.objects.get(pk=room.rule)
                is_recur = True
            weekly = None
            monthly = None
            if s_rule:
                if s_rule.frequency == "WEEKLY":
                    weekly = room.weekly
                    monthly = None
                elif s_rule.frequency == "MONTHLY":
                    monthly = room.monthly
                    weekly = None
            event = Event(start = start,
                          end = end,
                          timezone = user_timezone,
                          title = room.title,
                          description = room.description,
                          private = room.isPrivate,
                          is_recorded = room.isRecorded,
                          creator = user,
                          is_recurring = is_recur,
                          rule = s_rule,
                          end_recurring_period = recurring_period,
                          reminder_interval = room.reminderInterval,
                          weekly = weekly,
                          monthly = monthly,
                          type = "S",
                          is_telephone = room.isTelephone,
                          password_required = room.passwordRequired, 
                          host_must_present = room.hostMustPresent,
                          guest_have_approval = room.guestHaveApproval,
                          host_control_layout = room.hostControlLayout,
                          layout_type = room.layoutType,
                          view_guest_list = room.viewGuestList,
                          broadcast_video = room.broadcastVideo,
                          broadcast_audio = room.broadcastAudio,
                          chat_with_host = room.chatWithHost,
                          chat_with_guest = room.chatWithGuest,
                          is_valid = room.isValid,
                          is_flag = room.isFlag,
                          remember_password = room.rememberPassword
                          )
            event.save()
            
            calendar = get_object_or_404(Calendar, id=1)
            calendar.events.add(event)
            
            frequen = ''
            if s_rule:
                frequen =  s_rule.frequency
            subject = render_to_string("conferencing/reminder_subject.txt", {"type":frequen, 'event':event })
     
            
            context_template = {
                       'event':event,
                       'timezone':event.timezone,
                       'conftime':event.start,
                       'conf_message':True,
                       'message':'It just reminder',
                       'site': unicode(Site.objects.get_current()),
                       'user':event.creator,
                       'voice':event.is_telephone,
                       
                       }
            
            context = Context(context_template)
            html_part = loader.get_template('vmail/delivery/vmail.html').render(context)
            
            time_send = None
            
            if s_rule:
                r_start = event.start
                time_send = (event.start - datetime.timedelta(minutes = room.reminderInterval/60)).time()
                time_zone_str = adjust_datetime_to_timezone(datetime.datetime.now(), event.timezone, event.timezone).strftime('%z')
                time_zone = time_zone_str[:3]+':'+time_zone_str[3:]
                reminder = ReminderConference(event = event,
                                              rule  = s_rule,
                                              sender_email = event.creator.email,
                                              subject = subject,
                                              email_message = html_part,
                                              reminder_interval = room.reminderInterval,
                                              time_send = time_send,
                                              weekly = weekly,
                                              monthly = monthly,
                                              start = event.start,
                                              period = recurring_period,
                                              timezone = time_zone
                                              )
                reminder.save()
            else:
                pass
            
            conference_room = ConferenceRoomVO(event)
            return conference_room
        
        return None 

    @staticmethod
    def edit(room = None):
        if room is not None:
            user = User.objects.get(username = room.host)
            user_timezone = TimeZoneField().clean(room.timezone)
            
            start = adjust_datetime_to_timezone(room.start, room.timezone, settings.TIME_ZONE).replace(tzinfo = None)
            end = adjust_datetime_to_timezone(room.end, room.timezone, settings.TIME_ZONE).replace(tzinfo = None)
            
            recurring_period = None
            if room.recurringPeriod:
                recurring_period = adjust_datetime_to_timezone(room.recurringPeriod, room.timezone, settings.TIME_ZONE).replace(tzinfo = None)
            is_recur = False
            if room.rule ==0:
                s_rule = None
            else:
                s_rule = Rule.objects.get(pk=room.rule)
                is_recur = True
                
            weekly = None
            monthly = None    
            if s_rule:
                if s_rule.frequency == "WEEKLY":
                    weekly = room.weekly
                elif s_rule.frequency == "MONTHLY":
                    monthly = room.monthly
            
            event = get_object_from_hash(Event, room.conferenceHash)
            
            event.start = start
            event.end = end
            event.timezone = user_timezone
            event.title = room.title
            event.description = room.description
            event.private = room.isPrivate
            event.is_recorded = room.isRecorded
            event.creator = user
            event.is_recurring = is_recur
            event.rule = s_rule
            event.end_recurring_period = recurring_period
            event.reminder_interval = room.reminderInterval
            event.weekly = weekly
            event.monthly = monthly
            
            event.is_telephone = room.isTelephone
            event.password_required = room.passwordRequired 
            event.host_must_present = room.hostMustPresent
            event.guest_have_approval = room.guestHaveApproval
            event.host_control_layout = room.hostControlLayout
            event.layout_type = room.layoutType
            event.view_guest_list = room.viewGuestList
            event.broadcast_video = room.broadcastVideo
            event.broadcast_audio = room.broadcastAudio
            event.chat_with_host = room.chatWithHost
            event.chat_with_guest = room.chatWithGuest
            
            event.is_valid = room.isValid
            event.is_flag = room.isFlag
            event.remember_password = room.rememberPassword
            
            event.save()
            
            instance = event
            
            frequen = ''
            if s_rule:
                frequen =  s_rule.frequency
            subject = render_to_string("conferencing/reminder_subject.txt", {"type":frequen, 'event':instance })
            
            context_template = {
                       'event':instance,
                       'timezone':instance.timezone,
                        'conftime':instance.start,
                        'conf_message':True, 
                       'message':"It just reminder",
                       'site': unicode(Site.objects.get_current()),
                       'user':instance.creator,
                       'voice':instance.is_telephone,
                       }
            
            context = Context(context_template)
            html_part = loader.get_template('vmail/delivery/vmail.html').render(context)
            
            time_send = None
            reminder_qs = ReminderConference.objects.filter(event = instance)
            if reminder_qs:
                reminder = reminder_qs[0]
            else:
                reminder = None
            if s_rule:
                time_send = (instance.start - datetime.timedelta(minutes = room.reminderInterval/60)).time()
                time_zone_str = adjust_datetime_to_timezone(datetime.datetime.now(), instance.timezone, instance.timezone).strftime('%z')
                time_zone_fast = time_zone_str[:3]+':'+time_zone_str[3:]
                if reminder:
                    
                    reminder.event = instance
                    reminder.rule  = s_rule
                    reminder.sender_email = instance.creator.email
                    reminder.subject = subject
                    reminder.email_message = html_part
                    reminder.reminder_interval = room.reminderInterval
                    reminder.time_send = time_send
                    reminder.weekly = weekly
                    reminder.monthly = monthly
                    reminder.start = instance.start
                    reminder.period = recurring_period
                    reminder.timezone = time_zone_fast
                    reminder.save()
                else:
                    reminder = ReminderConference(event = instance,
                                                  rule  = s_rule,
                                                  sender_email = instance.creator.email,
                                                  subject = subject,
                                                  email_message = html_part,
                                                  reminder_interval = room.reminderInterval,
                                                  time_send = time_send,
                                                  weekly = weekly,
                                                  monthly = monthly,
                                                  start = instance.start,
                                                  period = recurring_period,
                                                  timezone = time_zone_fast
                                                  )
                    reminder.save()
            else:
                if reminder:
                    reminder.delete()
            
            conference_room = ConferenceRoomVO(event)
            return conference_room
        
        return None
    
    @staticmethod
    def delete(room = None):
        if room is not None:
            event = get_object_from_hash(Event, room.conferenceHash)
            event.is_valid = False
            event.save()
            
            
class ParticipantVO:
    pass;

def fromEvent(event):
    vo = ConferenceVO();
    vo.title = event.title;
    from netconf_utils.encode_decode import uri_b64encode
    vo.conferenceHash = uri_b64encode(str(event.pk * 42));
    return vo;
def fromEvents(query):
    events = list();
    for item in query:
        vo = fromEvent(item)
        events.append(vo);
    return events;

def fromUser(user):
    vo = ParticipantVO();
    vo.screenname = user.username;
    vo.email = user.email;
    vo.type = 1;
    return vo;


def fromFile(userfile):
    vo = FileVO();
    vo.id = userfile.id;
    vo.title = userfile.title;
    vo.filename = userfile.filename;
    vo.source_type = userfile.source_type;
    vo.current_type = userfile.current_type;
    vo.page_count = userfile.page_count;
    vo.duration = userfile.duration;
    vo.user = userfile.user;
    vo.uuid = userfile.uuid;
    if userfile.child_uuid:
        vo.uuid = userfile.child_uuid;
    self.visibility = userfile.visibility
    tags = []
    if not userfile.tags_string:
        pass
    else:
        arr_tag = get_tag_list(userfile.tags_string)
        for i in arr_tag:
            tags.append(i.name)
    vo.tags = tags;
    vo.created_date = userfile.timestamp
    vo.description = userfile.description
    return vo;

def fromUserFiles(userfiles):
    files = list();
    for file in userfiles:
        vo = fromFile(file);
        files.append(vo);
    return files;

def fromSharedFiles(sharedfiles):
    files = list();
    for sharedfile in sharedfiles:
        vo = fromFile(sharedfile.file);
        files.append(vo);
    return files;
    
class SubscriptionVO:
    def __init__(self, user=None):
        if user is not None:
            subscription = user.get_subscription()
            usersubscription = user.get_usersubscription()
            self.cloudStorage = subscription.file_size_mb_limit
            self.conferenceMinutes = subscription.conference_minutes_limit
            self.numberOfVideoChat = subscription.video_seats
            self.numberOfGuest = subscription.total_guests
            self.numberOfOpeningFile = subscription.concurrent_documents_open
            self.minutesUsed = usersubscription.minutes_used
            
            usedStorage = 0
            used_files = UserFile.active_objects.filter(user = user)
            for file in used_files:
                usedStorage  += file.file_size
                
            self.usedStorage = usedStorage/(1024*1024)

    @staticmethod
    def fromUsers(users):
        subscriptions = list();
        for user in users:
            vo = SubscriptionVO(user)
            subscriptions.append(vo)
        return subscriptions
    
    @staticmethod
    def updateMinutesUsed(user, minutesUsed):
        usersubscription = user.get_usersubscription()
        usersubscription.minutes_used += minutesUsed
        usersubscription.save()
        vo = SubscriptionVO(user)
        return vo
    
from django.core import serializers
import simplejson
from files.models import FileFolder, UserFile


def get_child(username, folder):
    if folder.type == 3:
        query = FileFolder.objects.filter(user__username = username, parent_folder__is_root = True, is_deleted = False).order_by('name')
    elif folder.type == 1 or folder.type == 2:
        query = FileFolder.objects.filter(parent_folder__id = folder.id,is_deleted = False).order_by('name')
    elif folder.type == 4:
        query = FileFolder.objects.filter(user__username = username,is_deleted = True, is_mark = True).order_by('name')
    else:
        query = FileFolder.objects.filter(user__username = username, parent_folder__id = folder.id, is_deleted = False).order_by('name')
    childs = generate_folderVO(query)
    for i in childs:
        get_child(username, i)
        folder.children.append(i)
        
class FolderVO:
    '''
    type:     0: normal folder
              1: system_folder_normal
              2: system_folder_root
              3: root_folder
              4: trash_folder
    '''
    def __init__(self, folder = None, is_contact = False):
        if folder is not None:
            self.name = folder.name
            self.parentID = 0
            if folder.parent_folder and folder.parent_folder.is_root == False:
                self.parentID = folder.parent_folder.id
            
            
            self.type = 0
            if not folder.parent_folder:
                if folder.is_root:
                    self.type = 3
                elif folder.is_trash:
                    self.type = 4
                elif folder.is_system:
                    self.type = 2
            else:
                if folder.is_system:
                    self.type = 1
            
            self.id = folder.id
            if not folder.parent_folder and folder.is_root:
                self.id = 0
                
            
            self.totalFiles = folder.total_files
            self.totalSize = folder.total_size
            self.lastModified = folder.last_modified
            self.dateCreated = folder.date_created
            self.isFlag = folder.is_flag
            self.isDeleted = folder.is_deleted
            self.isSystem = folder.is_system
            
            
            self.children = list()
            
    
    @staticmethod
    def from_folders(username, folders, root):
        flash_folders = generate_folderVO(folders, False)
        for f in flash_folders:
            get_child(username, f)
        
        return flash_folders

def generate_folderVO(folders, is_contact = False):
    folder_vo = list()
    for folder in folders:
        vo = FolderVO(folder, is_contact)
        folder_vo.append(vo)
    return folder_vo
