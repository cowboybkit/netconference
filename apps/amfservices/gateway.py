from pyamf.remoting.gateway.django import DjangoGateway
import pyamf;
from file_service import FileService;
from conference_service import ConferenceService;
from user_service import UserService;
from contact_service import ContactService
from files.models import UserFile;
from schedule.models import Event;
import vo;
from ccall_pins.models import ConferencePin

pyamf.register_class(vo.FileVO, 'com.nxtgen.remote.vo.FileVO');
pyamf.register_class(vo.FolderVO, 'com.nxtgen.remote.vo.FolderVO');
pyamf.register_class(vo.ConferenceVO, 'com.nxtgen.remote.vo.ConferenceVO');
pyamf.register_class(vo.ConferenceRoomVO, 'com.nxtgen.remote.vo.ConferenceRoomVO');
pyamf.register_class(ConferencePin, 'com.nxtgen.remote.vo.CallPinVO');
pyamf.register_class(vo.ContactVO, 'com.nxtgen.remote.vo.ContactVO');
pyamf.register_class(vo.ContactGroupVO, 'com.nxtgen.remote.vo.ContactGroupVO');
pyamf.register_class(vo.SubscriptionVO, 'com.nxtgen.remote.vo.SubscriptionVO');

gw = DjangoGateway({
                    "FileService": FileService,
                    "ConferenceService": ConferenceService,
                    "UserService": UserService,
                    "ContactService": ContactService,
                    })