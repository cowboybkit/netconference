from schedule.models import *
from conferencing.models import Conference, SharedFile, Attendees, RecordedConference
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
import vo;
from vo import ConferenceVO, ContactVO, ContactGroupVO, ConferenceRoomVO, SubscriptionVO;
from subscription.models import UserSubscription;
from files.models import UserFile
from datetime import *
from mailer import send_html_mail
from ccall_pins.models import ConferenceBridgeNumber
from django.template import loader, Context
from django.contrib.sites.models import Site
import sys
import os
from account.models import Account
from timezones.utils import adjust_datetime_to_timezone
from user_theme.models import UserTheme
from avatar.templatetags.avatar_tags import avatar_url
from netconf_utils.encode_decode import uri_b64encode, uri_b64decode
from friends.models import Contact
import pytz
from django.utils.translation import ugettext_lazy as _
from timezones.forms import TimeZoneField
from timezones.utils import adjust_datetime_to_timezone
from django.conf import settings
from vmail_new.models import is_valid_email
from friends_app.models import ContactGroup
from vmail.models import Vmail, VmailAttachment
from uuid import uuid4
from netconf_utils.netconf_common_timezones import priority_timezones

class ConferenceService:
    def getConference(self, request, urlhash):
        event_id = int(uri_b64decode(str(urlhash))) / 42;
        event = Event.objects.get(pk=event_id);
        conferenceVO = ConferenceVO(event); # vo.fromEvent(event);
        return conferenceVO;
    def login(self, request, username, password):
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                result = {}
                custom_avatar = avatar_url(user,200).split('?Signature')[0]
                
                if user.first_name:
                    firstname = user.first_name
                else:
                    firstname = ''
                if user.last_name:
                    lastname = user.last_name
                else:
                    lastname = ''
                result.update({
                               'screenname':user.username,
                               'email': user.email,
                               'type':4,
                               'userid': user.id,
                               'avatar': custom_avatar,
                               'firstname':firstname,
                               'lastname':lastname
                               })
                return result
            return None
        return None
    
    def loginx(self, request, username, password, urlhash):
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                result = {}
                try:
                    custom_avatar = avatar_url(user,200).split('?Signature')[0]
                except:
                    custom_avatar = ''
                
                if user.first_name:
                    firstname = user.first_name
                else:
                    firstname = ''
                if user.last_name:
                    lastname = user.last_name
                else:
                    lastname = ''
                event_id = int(uri_b64decode(str(urlhash))) / 42
                event = Event.objects.get(pk=event_id)
                if user.id == event.creator.id:
                    type = 4
                else:
                    type= 3
                result.update({
                               'screenname':user.username,
                               'email': user.email,
                               'type':type,
                               'userid': user.id,
                               'avatar': custom_avatar,
                               'firstname':firstname,
                               'lastname':lastname,
                               'hasAdminRight':user.is_superuser
                               })
                return result
            return None
        return None
    def searchConference(self, request, urlhash):
        try:
            event_id = int(uri_b64decode(str(urlhash))) / 42
            event = Event.objects.get(pk=event_id)
            url = "http://%s/meeting/set/%s" %(unicode(Site.objects.get_current()),urlhash)
            return url
        except:
            return None
    
    def isHost(self, request, username, urlhash):
        event_id = int(uri_b64decode(str(urlhash))) / 42;
        event = Event.objects.get(pk=event_id);
        if username == event.creator.username:
            return True;
        return False;
    def getConferences(self, request, username):
        query = Event.objects.filter(creator__username=username, is_valid = True);
        events = ConferenceRoomVO.fromEvents(query=query);
        return events;
    
    def updateTimer(self, request, username, interval):
        user_subs = UserSubscription.objects.get(user__username=username);
        user_subs.minutes_used = user_subs.minutes_used + interval;
        user_subs.save();
        return user_subs.minutes_used;

    def getVmailSeconds(self, request, username):
        user_sub = UserSubscription.objects.filter(user__username=username, 
                                                   active=True, 
                                                   cancelled=False)
        if not user_sub.count:
            return 0
        user_sub = user_sub[0]
        return user_sub.subscription.vmail_seconds
    def createRecording(self, request, userid, uuid, duration, title):
        user = User.objects.get(pk=userid)
        file = UserFile(uuid=uuid,
                        user = user,
                        timestamp= datetime.now(),
                        processed = 1,
                        file_size = 0,
                        is_active = True,
                        title=title,
                        filename = title,
                        source_type = 'rec_conf',
                        current_type = 'mp4',
                        duration = duration,
                        visibility ='U',
                        )
        file.save()
        conf = RecordedConference(user = user,
                                  created = datetime.now(),
                                  duration = duration,
                                  fms_hostroom = uuid,
                                  )
        conf.save()
        return file.id
    def sendInvite(self, request, contact, title, message, urlhash, voiceIP):
        event_id = int(uri_b64decode(str(urlhash))) / 42
        event = Event.objects.get(pk=event_id)
        
        
        voice = False
        if voiceIP == 1:
            voice = True
          
        context_template = {
                   'event':event,
                   'timezone':event.timezone,
                   'conftime':event.start,
                   'conf_message':True,
                   'message':message,
                   'site': unicode(Site.objects.get_current()),
                   'user':event.creator,
                   'voice':voice,
                   
                   }
        context = Context(context_template)
        html_part = loader.get_template('vmail/delivery/vmail.html').render(context)
        recipient = contact.replace(' ','').split(",")
        recipient_list = [x for x in recipient if x ]
        send_html_mail(title, html_part, html_part, event.creator.email, recipient_list, fail_silently=False)
#        invites = EventInvite(subject = 'netconference.tv NetConference invitation from Flash',
#                              body = message,
#                              sender = event.creator,
#                              recipient = contact,
#                              event = event,
#                              )
#        invites.save()
        recipients_str = ', '.join(recipient_list) or ''
        vmail = Vmail(instance = str(uuid4()),
                      subject = 'netconference.tv NetConference invitation from Flash',
                      body = message,
                      sender = event.creator,
                      recipient = recipients_str,
                      parent_msg = None,
                      sent_at =datetime.now(),
                      )
        vmail.save()
        vmail_attach = VmailAttachment(message = vmail,
                                       vmail_recording = None,
                                       invite = event,
                                       netsign = None
                                       )
        vmail_attach.save()
        
    def editConference(self, request, conf_obj):
        response = ConferenceVO.editFrom(conf_obj)
        return response
    
    
    def newConferenceRoom(self, request, conference):
        response = ConferenceRoomVO.create(conference)
        return response
    
    def editConferenceRoom(self, request, conference):
        response = ConferenceRoomVO.edit(conference)
        return response
    
    def deleteConferenceRoom(self, request, conference):
        ConferenceRoomVO.delete(conference)
        
    def deleteConferenceRooms(self, request, conferences):
        for conference in conferences:
            ConferenceRoomVO.delete(conference)
                
    def editConferenceRooms(self, request, conferences):
        for conference in conferences:
            ConferenceRoomVO.edit(conference)
    
    def getTimezoneList(self, request):
        PRETTY_TIMEZONE_CHOICES = []
        for tz in pytz.common_timezones:
            now = datetime.now(pytz.timezone(tz))
            PRETTY_TIMEZONE_CHOICES.append((tz, _("GMT%(tz_offset)s : %(tz)s" % {"tz_offset": now.strftime("%z"), "tz": tz})))
        PRETTY_TIMEZONE_CHOICES = sorted(PRETTY_TIMEZONE_CHOICES, key=lambda s: s[1].split(":")[0])
        
        response = []
        for p in PRETTY_TIMEZONE_CHOICES:
            response.append(p[1].title())
        return response
    
    def getPriorTimezoneList(self, request):
        PRETTY_TIMEZONE_CHOICES = []
        for tz in priority_timezones:
            now = datetime.now(pytz.timezone(tz))
            PRETTY_TIMEZONE_CHOICES.append((tz, _("GMT%(tz_offset)s : %(tz)s" % {"tz_offset": now.strftime("%z"), "tz": tz})))
        PRETTY_TIMEZONE_CHOICES = sorted(PRETTY_TIMEZONE_CHOICES, key=lambda s: s[1].split(":")[0])
        
        response = []
        for p in PRETTY_TIMEZONE_CHOICES:
            response.append(p[1].title())
        return response    
    
    def getSubscriptionConference(self, request, username):
        user = User.objects.get(username = username)
        sub = SubscriptionVO(user)
        return sub
        
    def updateConferenceMitutes(self, request, username, minutesUsed):
        user = User.objects.get(username = username)
        sub = SubscriptionVO.updateMinutesUsed(user, minutesUsed)
         
    
    def sendInvitation(self, request, emails, contacts, groups, title, message, urlhash, voiceIP):
        event_id = int(uri_b64decode(str(urlhash))) / 42
        event = Event.objects.get(pk=event_id)
        
        recipients = []
        
        for e in emails:
            if e not in recipients and is_valid_email(e):
                recipients.append(e)
        
        for c in contacts:
            contact = Contact.objects.get(pk = c)
            if contact:
                em =  contact.email
                if em not in recipients and is_valid_email(em):
                    recipients.append(em)
                    
        for group_id in groups:
            contactgroups = Contact.objects.filter(groups__id = group_id)
            for g_contact in contactgroups:
                g_contact_email =  g_contact.email
                if g_contact_email not in recipients and is_valid_email(g_contact_email):
                    recipients.append(g_contact_email)
        
        
        voice = False
        if voiceIP == 1:
            voice = True
          
        context_template = {
                   'event':event,
                   'timezone':event.timezone,
                   'conftime':event.start,
                   'conf_message':True,
                   'message':message,
                   'site': unicode(Site.objects.get_current()),
                   'user':event.creator,
                   'voice':voice,
                   
                   }
        context = Context(context_template)
        html_part = loader.get_template('vmail/delivery/vmail.html').render(context)
        
        for recipient in recipients:
            send_html_mail(title, html_part, html_part, event.creator.email, [recipient], fail_silently=False)
        
        
#        invites = EventInvite(subject = 'netconference.tv NetConference invitation from Flash',
#                              body = message,
#                              sender = event.creator,
#                              recipient = recipient_str,
#                              event = event,
#                              )
#        invites.save()
        recipient_str = ', '.join(recipients) or ''
        vmail = Vmail(instance = str(uuid4()),
                      subject = title,
                      body = message,
                      sender = event.creator,
                      recipient = recipient_str,
                      parent_msg = None,
                      sent_at =datetime.now(),
                      )
        vmail.save()
        vmail_attach = VmailAttachment(message = vmail,
                                       vmail_recording = None,
                                       invite = event,
                                       netsign = None
                                       )
        vmail_attach.save()
        
    def getInstantConference(self, request, username):
        query = Event.objects.filter(creator__username=username, type="G")
        event = None
        if query:
            event = ConferenceRoomVO(query[0])
        return event
    
    