from django.conf.urls.defaults import *
from account.forms import *
from django.conf import settings;
from amfservices import wsdl;
from amfservices import views, file_service;

urlpatterns = patterns('',
    #url(r'^file/(?P<emitter_format>.+)/$', wsdl.fileHandler, { 'emitter_format': 'json' }),
    #url(r'^test/$', views.test),
    url(r'^createRecordingFlush/$', file_service.createRecordingFlush),
    url(r'^createUploadFile/$', file_service.createUploadFile),
)
