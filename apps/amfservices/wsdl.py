from piston.handler import BaseHandler, AnonymousBaseHandler;
from piston.resource import Resource;
from piston.authentication import HttpBasicAuthentication;
from file_service import FileService;

class FileServiceHandler(FileService, BaseHandler):
    fields = ('abc');
    def abc(self, request):
        return "success";

fileHandler = Resource(handler=FileServiceHandler);