from datetime import datetime

from django.core.urlresolvers import reverse
from django.contrib.auth.models import  User
from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.conf import settings

from groups.base import Group
from django.template.loader import render_to_string
from django.contrib.sites.models import Site

from mailer import send_html_mail as send_mail

def send_mass_mail(datatuple):
    for data in datatuple:
        send_mail(*data)
                   
#from django.core.mail import send_mass_mail

class Project(Group):
    
    member_users = models.ManyToManyField(User, through="ProjectMember", verbose_name=_('members'))
    
    # private means only members can see the project
    private = models.BooleanField(_('private'), default=True, help_text=_(u'Private projects can only be accessed by members'))

    active = models.BooleanField(_('active'), default=True)
        
    def member_queryset(self):
        return self.member_users.all()
    
    def attached_users(self):
        "Return all assigned users, or an empty queryset."
        members = self.member_users.all()
        members_user_id = [el.id for el in members]
        return User.objects.filter(pk__in=members_user_id)
    
    def user_is_member(self, user):
        if ProjectMember.objects.filter(project=self, user=user).count() > 0: # @@@ is there a better way?
            return True
        else:
            return False
    
    def get_url_kwargs(self):
        return {'group_slug': self.slug}

    def get_recent_activity(self):
        return self.activity_set.all()[:5]
    
    @models.permalink
    def get_absolute_url(self):
        return ['project_detail', [self.slug, ]]

    #def save(self,*args,**kwargs):
        #if not self.pk:
            #if self.objects.filter(slug__iexact=self.slug).count() > 0:
                #from netconf_utils.random_int import rand
                
                ##This way, atleast the slugs are not the same and no errors. 
                ##Ideally AutoSlugField should be used, but Groups is a part of Pinax and a part of the env, not of the repo.
                
                #self.slug = "%s%s" %(self.slug, rand())
        #super(Project,self).save(*args,**kwargs)

                
        #memberadd = not self.pk and self.creator
        #super(Project,self).save(*args,**kwargs)
        #if memberadd:
            #ProjectMember.objects.get_or_create(project=self,user=self.creator)


class ProjectMember(models.Model):
    project = models.ForeignKey(Project, related_name="members", verbose_name=_('project'))
    user = models.ForeignKey(User, related_name="projects", verbose_name=_('user'))
    
    away = models.BooleanField(_('away'), default=False)
    away_message = models.CharField(_('away_message'), max_length=500)
    away_since = models.DateTimeField(_('away since'), default=datetime.now)
    
    class Meta:
        unique_together = ('project', 'user')
    
    
class InvitedEmails(models.Model):
    project = models.ForeignKey(Project)
    invited_by = models.ForeignKey(User)
    invited_at = models.DateTimeField(auto_now=True)
    email = models.EmailField()
    
from django.db.models.signals import post_save

def add_new_user_to_project(sender, instance, **kwargs):
    user_email = instance.email
    projects_to_add_to = InvitedEmails.objects.filter(email=user_email)
    for invited_email in projects_to_add_to:
        ProjectMember.objects.get_or_create(user=instance, project=invited_email.project)
        instance.message_set.create(message=_('You were invited to, and have been successfully added to project %s' % invited_email.project.name))
    

def send_project_addition_notification(sender, instance, created, **kwargs):
    if created:
        member = instance
        message = render_to_string("projects/new_member_notification.txt",
                                   {"member": member,
                                    "site": Site.objects.get_current(),
                                    "MEDIA_URL": settings.MEDIA_URL, })
        subject = _(u"You have been given access to project %s" % member.project)
        sender = member.project.creator.email or settings.DEFAULT_FROM_EMAIL
        mass_mail_datatuple = [
                               (subject, "", message, sender, [member.user.email]),
                               
                               ]
        send_mass_mail(mass_mail_datatuple)    

    
post_save.connect(add_new_user_to_project, sender=User)
post_save.connect(send_project_addition_notification, sender=ProjectMember)







    
