from django.test import TestCase
from projects.models import Project
from netconf_utils.tests import TestUserTestCase
from debug import ipython, idebug
from django.contrib.auth.models import User
from netconf_utils.random_int import rand

class AddEmailTest(TestUserTestCase):

    def setUp(self):
        super(AddEmailTest,self).setUp()
        self.project = Project.objects.create(slug='test',
                                              name='test',
                                              creator=self.test_user,
                                              )

    def test_add_email_to_project(self):
        emails_to_add = ['lakshman@agiliq.com','lakshman1984@gmail.com']
        
        #This adds the new email to the db
        self.post("project_add_user",self.project.slug,data={'emails':emails_to_add,
                                                             'user':self.test_user.id})

        #This should add the new user into the project
        User.objects.create_user(username=rand(),
                                 email='lakshman1984@gmail.com',
                                 password='test')

        self.assertEquals(self.project.member_users.count(),2)
        