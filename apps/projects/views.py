from django.shortcuts import render_to_response, get_object_or_404, redirect
from django.template import RequestContext
from django.http import HttpResponseRedirect, Http404
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.utils.datastructures import SortedDict
from django.utils.translation import ugettext_lazy as _

from django.conf import settings

if "notification" in settings.INSTALLED_APPS:
    from notification import models as notification
else:
    notification = None

from projects.models import Project, ProjectMember
from projects.forms import ProjectForm, ProjectUpdateForm, AddUserForm, AddProjectMemberForm

from activitystream.forms import ActivityForm


TOPIC_COUNT_SQL = """
SELECT COUNT(*)
FROM topics_topic
WHERE
    topics_topic.object_id = projects_project.id AND
    topics_topic.content_type_id = %s
"""
MEMBER_COUNT_SQL = """
SELECT COUNT(*)
FROM projects_projectmember
WHERE projects_projectmember.project_id = projects_project.id
"""

@login_required
def create(request, form_class=ProjectForm, template_name="projects/create.html"):
    project_form = form_class(request.POST or None)
    
    if project_form.is_valid():
        project = project_form.save(commit=False)
        project.creator = request.user
        project.save()
        project_member = ProjectMember(project=project, user=request.user)
        project.members.add(project_member)
        project_member.save()
        if notification:
            # @@@ might be worth having a shortcut for sending to all users
            notification.send(User.objects.all(), "projects_new_project",
                {"project": project}, queue=True)
        return HttpResponseRedirect(project.get_absolute_url())
    
    return render_to_response(template_name, {
        "project_form": project_form,
        'nav_current': 'projects_nav',
        'subnav_current': 'create_project'
    }, context_instance=RequestContext(request))


@login_required
def projects(request, template_name="projects/projects.html"):
    
    projects = Project.objects.filter(members__user=request.user)
    
    search_terms = request.GET.get('search', '')
    if search_terms:
        projects = (projects.filter(name__icontains=search_terms) |
            projects.filter(description__icontains=search_terms))
    
    content_type = ContentType.objects.get_for_model(Project)
    
    projects = projects.extra(select=SortedDict([
        ('member_count', MEMBER_COUNT_SQL),
        ('topic_count', TOPIC_COUNT_SQL),
    ]), select_params=(content_type.id,))
    
    return render_to_response(template_name, {
        'projects': projects,
        'search_terms': search_terms,
        'nav_current': 'projects_nav',
        'subnav_current': 'all_projects',
    }, context_instance=RequestContext(request))


@login_required
def confirm_delete(request, group_slug=None, template_name="projects/confirm_delete.html"):
    project = get_object_or_404(Project, slug=group_slug)
    if not request.user == project.creator:
        return Http404
    return render_to_response(template_name, {
            'project': project,
        },
        context_instance=RequestContext(request))

@login_required
def delete(request, group_slug=None, redirect_url=None):
    project = get_object_or_404(Project, slug=group_slug)
    if not redirect_url:
        redirect_url = reverse('project_list')
    
    # @@@ eventually, we'll remove restriction that project.creator can't leave project but we'll still require project.members.all().count() == 1
    if (request.user.is_authenticated() and request.method == "POST" and
            request.user == project.creator and project.members.all().count() == 1):
        project.delete()
        request.user.message_set.create(message=_("Project %(project_name)s deleted.") % {"project_name": project.name})
        # no notification required as the deleter must be the only member
    
    return HttpResponseRedirect(redirect_url)

@login_required
def set_inactive(request, group_slug=None, redirect_url=None, active=False):
    project = get_object_or_404(Project, slug=group_slug)
    message = "active" if active else "inactive"
    if not redirect_url:
        redirect_url = reverse('project_list')

    if (request.user.is_authenticated() and request.user == project.creator):
        project.active = active
        project.save()
        request.user.message_set.create(message=_("Project %(project_name)s was marked %(status)s.") % {"project_name": project.name, "status": message})

    return HttpResponseRedirect(redirect_url)

@login_required
def set_active(request, group_slug=None, redirect_url=None):
    return set_inactive(request, group_slug, redirect_url, True)

@login_required
def your_projects(request, template_name="projects/your_projects.html"):

    projects = Project.objects.filter(member_users=request.user).order_by("name")

    content_type = ContentType.objects.get_for_model(Project)

    projects = projects.extra(select=SortedDict([
        ('member_count', MEMBER_COUNT_SQL),
        ('topic_count', TOPIC_COUNT_SQL),
    ]), select_params=(content_type.id,))

    return render_to_response(template_name, {
        "projects": projects,
        'nav_current': 'projects_nav',
        'subnav_current':'your_projects'
    }, context_instance=RequestContext(request))


from django.db.models import Q

@login_required
def project(request, group_slug=None, form_class=ProjectUpdateForm, 
            adduser_form_class=AddUserForm,
            template_name="projects/project.html"):
    
    projects = Project.objects.filter(members__user=request.user)
    try:
        project = Project.objects.get(Q(members__user=request.user), slug=group_slug)
    except Project.DoesNotExist:
        project = get_object_or_404(Q(private=False), slug=group_slug)
        
    form = ActivityForm(request.POST)

    if not request.user.is_authenticated():
        is_member = False
    else:
        is_member = project.user_is_member(request.user)
    
    action = request.POST.get("action")
    if request.user == project.creator and action == "update":
        project_form = form_class(request.POST, instance=project)
        if project_form.is_valid():
            project = project_form.save()
    else:
        project_form = form_class(instance=project)
    if request.user == project.creator and action == "add":
        adduser_form = adduser_form_class(request.POST, project=project)
        if adduser_form.is_valid():
            adduser_form.save(request.user)
            adduser_form = adduser_form_class(project=project) # clear form
    else:
        adduser_form = adduser_form_class(project=project)
    
    return render_to_response(template_name, 
                              {
                                  'projects': projects,
                                  'form': form,
                                  "project": project,
                                  "group": project, # @@@ this should be the only context var for the project
                                  "is_member": is_member,
                                  'nav_current': 'projects_nav',
                                  'subnav_current_add': 'Project %s'%project.name,

                                  }, context_instance=RequestContext(request))


@login_required
def add_user(request,project_slug):
    project = get_object_or_404(Project,slug=project_slug)
    project_users = project.member_users.all()
    if request.method=='POST':
        form = AddProjectMemberForm(project=project, user=request.user, data=request.POST)
        if form.is_valid():
            data = form.cleaned_data
            for el in set(project_users) - set(form.cleaned_data['user']):
                el.delete()
            
            for user in form.cleaned_data['user']:
                ProjectMember.objects.get_or_create(user=user,project=project)
                
            users_to_add = User.objects.filter(email__in=data['emails'])
            user_emails_found = set([el.email for el in users_to_add])
            emails_to_invite = set(data['emails']) - user_emails_found
            
            for user in users_to_add:
                ProjectMember.objects.get_or_create(user=user,project=project)
                
            for invite_email in emails_to_invite:
                send_project_invite(to_email=invite_email,
                                    user=request.user,
                                    project=project)
                
                
            request.user.message_set.create(message=_('Project members list updated'))
            if len(emails_to_invite):
                request.user.message_set.create(message=_('%s emails have been invited to join netconference and be a part of this project'%len(emails_to_invite)))
            if len(user_emails_found):
                request.user.message_set.create(message=_('%s emails were found to be of a user within netconference, and they were added to this project'%len(user_emails_found)))
            return redirect(project)
    else:
        form = AddProjectMemberForm(project=project, user=request.user,
                                    initial={'user':[el.pk for el in project_users]})
        
    return render_to_response('projects/add_user.html',
                              {'form':form,
                               'subnav_current_add': 'Edit users to Project %s'%project.name,
                               },
                              RequestContext(request))

from django.template.loader import render_to_string
from mailer import send_html_mail as send_mail
from projects.models import InvitedEmails
from django.contrib.sites.models import Site

def send_project_invite(to_email,user,project):
    context = locals()
    context.update({"site": Site.objects.get_current(),
                    "MEDIA_URL": settings.MEDIA_URL,})
    email_message = render_to_string('projects/project_invite.txt', context)
    send_mail(_('You are invited to be a part of a Netconference Project'),
              "",
              email_message,
              from_email=user.email,
              recipient_list=[to_email,],
              )
    InvitedEmails.objects.get_or_create(project=project,
                                        email=to_email,
                                        invited_by=user)
    return True




