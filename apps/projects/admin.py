from django.contrib import admin
from projects.models import Project, ProjectMember, InvitedEmails

class ProjectAdmin(admin.ModelAdmin):
    list_display = ('name', 'slug', 'creator', 'created')

admin.site.register(Project, ProjectAdmin)
admin.site.register(ProjectMember)
admin.site.register(InvitedEmails)
