from django.conf.urls.defaults import *

from projects.models import Project

from groups.bridge import ContentBridge

from django.views.generic.list_detail import object_detail
from django.views.generic.simple import direct_to_template

project_qs = Project.objects.all()

bridge = ContentBridge(Project, 'projects')

urlpatterns = patterns('projects.views',
    url(r'^$', 'projects', name="project_list"),
    url(r'^create/$', 'create', name="project_create"),
    url(r'^your_projects/$', 'your_projects', name="your_projects"),
    # project-specific
    url(r'^project/(?P<group_slug>[-\w]+)/$', 'project', name="project_detail"),
    url(r'^project/(?P<group_slug>[-\w]+)/confirm/$', 'confirm_delete', name="project_confirm_delete"),
    url(r'^project/(?P<group_slug>[-\w]+)/activate/$', 'set_active', name="project_activate"),
    url(r'^project/(?P<group_slug>[-\w]+)/inactivate/$', 'set_inactive', name="project_inactivate"),
    url(r'^project/(?P<group_slug>[-\w]+)/delete/$', 'delete', name="project_delete"),
    url(r'^project/(?P<project_slug>[-\w]+)/add/$', 'add_user', name="project_add_user"),
    url(r'^project/(?P<slug>[-\w]+)/desc/$', object_detail, {'queryset':project_qs,},
        name="project_description"),
    url(r'^tasks', direct_to_template, {"template": "projects/tasks.html"}, name="project_tasks"),
    url(r'^taskdetails', direct_to_template, {"template": "projects/taskdetails.html"}, name="project_task_details"),
    url(r'^edittask', direct_to_template, {"template": "projects/edittask.html"}, name="project_task_edit"),
)
