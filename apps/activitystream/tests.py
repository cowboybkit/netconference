"""
This file demonstrates two different styles of tests (one doctest and one
unittest). These will both pass when you run "manage.py test".

Replace these with more appropriate tests for your application.
"""

from django.test import TestCase

class SimpleTest(TestCase):
    def test_basic_addition(self):
        """
        Tests that 1 + 1 always equals 2.
        """
        self.failUnlessEqual(1 + 1, 2)

__test__ = {"doctest": """
Another way to test that 1 + 1 is equal to 2.

>>> 1 + 1 == 2
True
"""}

from wakawaka.models import Revision,WikiPage,Category
from activitystream.models import Activity

class TestActivity(TestCase):
    def setUp(self):
        c = self.client
        cat = Category.objects.create(name='tc')
        page = WikiPage.objects.create(slug='tp',
                                       category=cat,
                                       title='tp',
                                       )
        rev = page.revisions.create(creator_ip='1.1.1.1',
                                    )
    
    def test_signals_adding(self):
        """Check whether new revision created activity is created"""
        assert Activity.objects.count()==1
        
        
    

