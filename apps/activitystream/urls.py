from django.conf.urls.defaults import *
from django.views.generic.list_detail import object_detail,object_list

from activitystream.models import Activity

all_act = Activity.objects.all()

urlpatterns = patterns('activitystream.views',
                       url('^$',object_list,
                           {'queryset':all_act},
                           name='site_activity'),
                       url('^(?P<activity_id>\d+)/comment/$',
                           'activity_comment',
                           name='activity_comment'),
                       url('^delete/$',
                           'delete_activity',
                           name='activity_delete'),
                       url('^(?P<project_id>\d+)/create_comment/$',
                           'add_activity',
                           name='activity_create'),
                       url('^(?P<project_id>\d+)/clear/$',
                           'clear_all_entries',
                           name='project_clear_all_entries'),
                       #url('(?P<username>[\w-]+)/$','activitystream.views.userstream',name='user_activity'),
                       #Do object activity here url()
                       )

