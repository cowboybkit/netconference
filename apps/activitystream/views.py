# Create your views here.
from django.views.generic.list_detail import object_list
from django.shortcuts import get_object_or_404, render_to_response, redirect
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.template import RequestContext
from django.template.loader import render_to_string
from django.contrib.auth.decorators import login_required
from django.utils.translation import ugettext, ugettext_lazy as _

from activitystream.models import Activity
from activitystream.forms import ActivityCommentForm, ActivityForm

from projects.models import Project

@login_required
def userstream(request,username):
    act_user = get_object_or_404(User,username=username)
    qs = act_user.activity_set.all()
    return object_list(request,qs)

@login_required
def activity_comment(request,activity_id):
    activity = get_object_or_404(Activity,pk=activity_id)
    form = ActivityCommentForm(request.POST or None)
    if form.is_valid():
        saved_comment = form.save(commit=False)
        saved_comment.user = request.user
        saved_comment.activity = activity
        saved_comment.save()
        return redirect(activity.project)
    return render_to_response('activitystream/activity_comment.html',
                              {'activity':activity,
                               'form':form},
                              RequestContext(request))

@login_required
def add_activity(request,project_id):
    project = get_object_or_404(Project,id=project_id)
    form = ActivityForm(request.POST)
    if form.is_valid():
        activity_obj = form.save(commit=False)
        activity_obj.user = request.user
        activity_obj.project = project
        activity_obj.save()
        return redirect(project)
        
    request.user.message_set.create(message=_('Some error occurred'))
    return redirect(project)

@login_required
def delete_activity(request):
    activity_id = request.POST.get('activity_id')
    referer_url = request.META.get('HTTP_REFERER','')
    activity = get_object_or_404(Activity,id=activity_id)
    project = activity.project
    if request.user in (activity.user,project.creator):
        activity.delete()
        request.user.message_set.create(message=_('Activity deleted'))
    else:
        request.user.message_set.create(message=_('You cannot delete this activity'))
    return redirect(referer_url or project)
    
    
@login_required
def clear_all_entries(request, project_id):
    project = get_object_or_404(Project, pk=project_id)
    if request.method == 'POST' and 'clear' in request.POST:
        if request.user == project.creator:
            for activity in project.activity_set.all():
                activity.delete()
            request.user.message_set.create(message=_('Activities cleared'))
        else:
            request.user.message_set.create(message=_('You cannot clear this project activity'))
        return redirect('project_list')
    else:
        return render_to_response('projects/clear_activities.html', {'project': project}, RequestContext(request))


