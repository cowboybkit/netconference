from django import forms
from activitystream.models import ActivityComment, Activity

class ActivityCommentForm(forms.ModelForm):
    
    class Meta:
        model = ActivityComment
        fields = ('comment_text',)
        
        
class ActivityForm(forms.ModelForm):
    
    class Meta:
        model = Activity
        fields = ('message',)