from django.db import models
from django.contrib.auth.models import User
from django.contrib.contenttypes import generic
from django.contrib.contenttypes.models import ContentType

# Create your models here.
from projects.models import Project
from files.models import UserFile
from django.template.loader import render_to_string
from django.utils.translation import ugettext_lazy as _
from django.contrib.sites.models import Site

#from django.core.mail import send_mail, send_mass_mail
from mailer import send_html_mail as send_mail

def send_mass_mail(datatuple):
    for data in datatuple:
        send_mail(*data)
from django.conf import settings

class Activity(models.Model):
    user = models.ForeignKey(User,blank=True,null=True)
    message = models.TextField(blank=True,null=True)
    datetime = models.DateTimeField(auto_now_add=True)
    
    object_id = models.PositiveIntegerField(blank=True, null=True)
    content_type = models.ForeignKey(ContentType, blank=True, null=True)
    content_object = generic.GenericForeignKey()
    
    project = models.ForeignKey(Project, blank=True, null=True)
    
    class Meta:
        ordering = ('-datetime',)
    
    def __unicode__(self):
        return self.message
    
class ActivityComment(models.Model):
    comment_text = models.TextField()
    activity = models.ForeignKey(Activity)
    user = models.ForeignKey(User)
    created = models.DateTimeField(auto_now_add=True)
    
def create_activity(sender, instance, **kwargs):
    if not instance.project:
        return
    activity_user_names = ['user',]
    for el in activity_user_names:
        if hasattr(instance,el):
            activity_user = getattr(instance,el)
            break
    else:
        activity_user = None
    act = Activity.objects.create(user=activity_user,
                                  content_object=instance,
                                  message=_('%(user)s uploaded a new file %(title)s'%({"user": activity_user, "title": instance.title}),project=instance.project))

def send_email_for_activity(sender, instance, created, **kwargs):
    "When an activity is created"
    "Send a new email"
    if created:
        activity = instance
        if not activity.project:
            return None
        users = activity.project.attached_users()
        message = render_to_string("activitystream/new_activity_notification.txt", {"activity": activity,
                                                                                    "site": Site.objects.get_current(),
                                                                                    "MEDIA_URL": settings.MEDIA_URL,})
        subject = _("New activity on your project %s" % activity.project)
        sender = settings.DEFAULT_FROM_EMAIL
        mass_mail_datatuple = [
                               (subject, "", message, sender, [user.email])
                               for user in users
                               ]
        send_mass_mail(mass_mail_datatuple)
    
    
def send_email_for_activity_comment(sender, instance, created, **kwargs):
    "When an activity comment is created"
    "Send a new email"
    if created:
        comment = instance
        activity = comment.activity
        users = activity.project.attached_users()
        message = render_to_string("activitystream/new_comment_notification.txt", {"activity": activity, 'comment': comment,
                                                                                    "site": Site.objects.get_current(),
                                                                                    "MEDIA_URL": settings.MEDIA_URL,})
        subject = _("New activity on your project %s" % activity.project)
        sender = settings.DEFAULT_FROM_EMAIL
        mass_mail_datatuple = [
                               (subject, message, sender, [user.email])
                               for user in users
                               ]
        send_mass_mail(mass_mail_datatuple)    
    
    
from django.db.models.signals import post_save        

post_save.connect(create_activity, sender=UserFile)
##We do not need to send email explicitly for Files.
##As that creates an activity which will send an email anyway.
post_save.connect(send_email_for_activity, sender=Activity)
post_save.connect(send_email_for_activity_comment, sender=ActivityComment)
