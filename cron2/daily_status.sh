#!/bin/bash

function getStatus {
  today=$(date --date='yesterday' +%Y-%m-%d)
  tomorrow=$(date --date='tomorrow' +%Y-%m-%d)
  echo -e "\\n------------------------------------------"
  echo $1
  echo -e "------------------------------------------\\n"
  git log --date=iso --author=$1 \
          --since=$today \
          --until=$tomorrow \
          --summary --show-notes --date-order \
          --format=format:"- [%H]    %s"
}

getStatus 'shabda'
getStatus 'javed'
getStatus 'thejaswi'
