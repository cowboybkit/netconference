#!/bin/bash
# CRON_FOLDER and PROJECT_ROOT are set as a bash env variable at /etc/profile
DIRECTORY=$(cd `dirname $0` && pwd)
PROJECT_ROOT=`dirname $DIRECTORY`
cd $PROJECT_ROOT/../pinax_env
PINAX_ENV_ROOT=`pwd`

# activate virtual environment
source $PINAX_ENV_ROOT/bin/activate
cd $PROJECT_ROOT

python manage.py retry_deferred >> $PROJECT_ROOT/logs/cron_mail_deferred.log 2>&1
