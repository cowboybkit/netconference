#!/bin/bash

DIRECTORY=$(cd `dirname $0` && pwd)
PROJECT_ROOT=`dirname $DIRECTORY`
cd $PROJECT_ROOT/../pinax_env
PINAX_ENV_ROOT=`pwd`

# activate virtual environment
source $PINAX_ENV_ROOT/bin/activate
cd $PROJECT_ROOT

python manage.py send_netsign_reminders >> $PROJECT_ROOT/logs/cron_netsign_reminder_mail.log 2>&1
