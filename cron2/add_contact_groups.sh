#!/bin/bash

DIRECTORY=$(cd `dirname $0` && pwd)
PROJECT_ROOT=`dirname $DIRECTORY`
cd $PROJECT_ROOT/../pinax_env
PINAX_ENV_ROOT=`pwd`

# activate virtual environment
source $PINAX_ENV_ROOT/bin/activate
cd $PROJECT_ROOT

python manage.py add_contact_groups >> $PROJECT_ROOT/logs/add_contact_groups.log 2>&1
