{% load i18n %}{% blocktrans %}You have been sent a nuriche.tv V-mail by {{ user }}.

{{ user }} said:
{% endblocktrans %}
{{ extra_message }}

Forward Content:

{{ Dear }} ,

{% autoescape on %}
{{ message }}
{% endautoescape %}
{% blocktrans %}
To view your V-Mail, go to

{{ vmail_url }}

V-Mail is provided for nuriche.tv by NetConference.tv - a great way to collaborate with others online, send video e-mail, and watch videos with others in real time.  If you have any questions about NetConference.tv, don't hesitate to contact us at http://netconference.tv/contact.
{% endblocktrans %}