	function S4() {
	   return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
	}
	
	function get_guid() {
	   return (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4());
	}	 
	
    function getSSApplet(){
	 	return document.getElementById("VHScreenShare");
	 }
	 
	function startRecordingDesktop(title){
	 	var applet = getSSApplet();
		var guid = get_guid();
		var urlhash = "{{ urlhash }}";
		try {
			applet.setRecordVideo(guid);
		}
		catch(exception){}
		$.post("{% url create_desktop_recording %}", {
			title: title,
			urlhash: urlhash,
			guid: guid
		});
		
		
		
	 }
	
	function stopRecordingDeskTop(){
		var applet = getSSApplet();
		try {
			applet.stopRecordVideo();
		}
		catch(exception){}
	}


		$(function(){
			$("#startRecordButton").live("click", function(){
				recording_name = prompt("What would you like to name this recording?");
				$("#startRecordButton").hide();
				$("#stopRecordButton").show();
				var player = document.getElementById('controller').getElementsByTagName('object')[0];
				player.startRecord();
				if(is_desktop_streaming){
					startRecordingDesktop(recording_name);	
				}
				$("#recording_notice").show();
			});
			$("#stopRecordButton").live("click", function(){
				$("#startRecordButton").show();
				$("#stopRecordButton").hide();
				var player = document.getElementById('controller').getElementsByTagName('object')[0];
				player.stopRecord();
				stopRecordingDeskTop();
				$("#recording_notice").hide();
			});
			$("#stopRecordButton").hide();
			$("#recording_notice").hide();
		});
		
		should_record = true;
		if (should_record){
					$("#controller").each(function() {
                    flashembed(this, {
                        src: "{{ MEDIA_URL }}swf/controller.swf",
                        version: [10, 0],
                        wmode: 'opaque',
                        onFail: function() {
                            $('#wizard_panel').dialog({
                                width: 425,
                                height: 340,
                                title: 'Webcam and Microphone Setup Wizard',
                                resizable: false
                            });
                            $("#wizard").each(function() {
                                flashembed(this, {
                                    src: "{{ MEDIA_URL }}swf/miccamwizard.swf",
                                    version: [10, 0],
                                    wmode: 'opaque',
                                    expressInstall: "{{ MEDIA_URL }}swf/expressinstall.swf"
                                }, {
                                    gid: '{{ gid }}'
        
                                });
                            });
                        }
                    }, 
					
					{
                        o: '{% ifequal host.username visitor %}j122l{% else %}{{ screen_name }}{% endifequal %}',
                        uid: '{{ screen_name }}',
                        cid: '{{ urlhash }}',
                        gid: '{{ gid }}',
						used_minutes: '{{ used_minutes }}',
						max_minutes: '{{ max_minutes }}'
                    });
                });			
			
		}