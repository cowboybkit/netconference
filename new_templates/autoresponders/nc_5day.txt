Just wanted to let you Know...
Dear Valued Customer,

This is just a friendly reminder that NetConference will not charge the monthly service fee to your credit card until after the 30-Day Trial Period ends and only if you do not inform us of your intent to cancel. You still have 5 more days to take advantage of all of the great features that NetConference has to offer. If you decide to cancel your account prior to the end of the 30-Day Trial Period, you will not incur any charges. If you do not cancel, at the end of your 30-Day Trial Period, you will be automatically charged the monthly service fee. You can cancel your Trial Period Offer at anytime by logging into your NetConference account here:

The NetConference Team
All rights reserved.
You are receiving this email because in the past you registered for NetConference Your Office in the Cloud. Occasionally, you will receive news about NetConference along with tips to increase your productivity. 

Our mailing address is:
NetConference
23679 Calabasas Rd. Suite 785
Calabasas, CA 91302
818-598-8400
