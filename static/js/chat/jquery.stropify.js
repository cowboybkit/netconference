/*
 * Stroqe is a XMPP MUC client written using strophejs
 *
 * jquery.stropify is a jquery plugin using Stroqe
 *
 * This plugin acts as a wrapper to Stroqe and makes it
 * easier to integrate into existing web pages
 *
 * Example use:
 *
 *     login with user registered with xmpp server 
 *
 *     $("#chat_container").stropify({
 *          'username': 'xmpp_user',
 *          'password': 'xmpp_pass',
 *          'room': 'discuss'
 *     });
 *
 *     anonymous login
 *
 *     $("#chat_container").stropify({
 *          'nick': 'my_nick',
 *          'room': 'discuss'
 *     });
 *
 * Options:
 *
 *  chat: main div where received messages will be displayed
 *  txt_msg: text input where user can type the message
 *  btn_connect: a button to toggle connection status
 *  btn_join: a button to toggle join/leave room
 *  nick_class: the css class that will be added to users' nick names
 *  body_class: the css class that will be added to received messages
 *  status_message_class: the css class that will be added to status messages (joined/left/file uploaded)
 *  message_div_class: the css class that will be added to the div containing messages (nick + body)
 *  roster: the list element where room participants will be displayed
 *
 *  jquery.stropify uses custom events to interact with xmpp
 *
 *  Example:
 *      To send message as current user, trigger send_message on 'txt_msg' element of options
 *      where msg is the string of body to be sent to the room
 *
 *      $(txt_msg).trigger("send_message", [ msg ]);
 *
 */


(function($) {

    Stroqe.prototype = {
        
        /* connect to the xmpp server
         *
         * params:
         *   
         *   username: username of xmpp account without domain
         *   password: plaintext password of the account
         *
         * for anonymous logins
         *
         *   nick: the nickname to be displayed after anoymous login
         *
         * returns:
         *   Strophe.Connection object
         */
        connect: function(username, password) {
            this._conn = new Strophe.Connection(settings.BOSH_SERVICE);
            if ( typeof(username) != 'undefined' &&  username.length) {
                this._conn.connect(username + '@' + settings.DOMAIN, password, this._on_connect(this));
                this._nick = username;
            }
            else {
                // anonymous login
                this._conn.connect(settings.DOMAIN, null, this._on_connect(this));
                this._nick = this.options.nick;
            }
            return this._conn;
        },

        /* disconnect from the xmpp server */
        disconnect: function() {
            this._conn.disconnect();
            this._on_disconnect();
        },

        /* callback fired when connection status changes */
        _on_connect: function(context) {
            return function (status, condition) {
                if (settings.DEBUG) {
                    console.log("status is ", status);
                }
                if (status == Strophe.Status.CONNECTED) {
                    context.join(context.options.room);
                }
            };
        },

        /* callback fired before disconnect */
        _on_disconnect: function(status) {
            this.leave();
        },

        /* join a chat room 
         *
         *   rooms are persistent, i.e. a room once created will
         *   have the logs of previous chats
         *
         *   if no xmpp room exists with the given name, a new
         *   room will be created
         *
         * params:
         * 
         *   room: name of the room to join, without domain
         */
        join: function(room) {
            this._room = room + '@' + settings.MUC_DOMAIN;
            if (settings.DEBUG) {
                console.log("joining room ", this._room);
            }
            return this._conn.muc.join(this._room, this._nick, this._on_message(this), this._on_presence(this));
        },

        /* leave a chat room */
        leave: function() {
            return this._conn.muc.leave(this._room, this._nick, this._on_leave);
        },

        /* callback fired when message stanza is received from xmpp server 
         *
         * Example stanza:
         *
         * <message xmlns='jabber:client' from='testroom@conference.localhost/javed' to='37493@localhost/21696' type='groupchat' id='5004'>
         *     <body xmlns='jabber:client'>how you doing?</body>
         *     <active xmlns='http://jabber.org/protocol/chatstates'/>
         * </message>
         *
         */
        _on_message: function(context) {
            return function(stanza_xml) {
                var stanza = $(stanza_xml);
                var type = stanza.attr('type');
                    
                if (stanza.find('subject').length) {
                    context._handle_subject(stanza);
                }
                if (stanza.find('composing').length) {
                    context._handle_composing(stanza);
                }

                if (type == 'groupchat') {
                    if (!context.focus) {
                        context.unread++;
                        document.title = "[" + context.unread + " unread] - " + document.title;
                        $.sound.play(settings.MESSAGE_SOUND_URL);
                    }
                    else if (context.unread>0) {
                        document.title = document.title.split('-').pop().trim();
                    }
                    context.display_message(stanza);
                }
                else if (type == 'chat') {
                    context.display_pm(stanza);
                }
                return true;
            };
        },

        /* display the room message received from the server 
         *
         *  params: 
         *      stanza: the xml stanza received from the server
         *
         *  processes the received stanza to check if it is a status change,
         *  converts urls in text to hyperlinks (using jquery.linkify),
         *  scrolls the chat window to the bottom and appends the message to output
         *
         *  Example html of received message that will be added to 'chat' element of options
         *
         *   <div class="message">
         *      <span class="nick">javed/span>: 
         *      <span class="body">how you doing?</span>
         *      <br>
         *   </div>
         *
         */
        display_message: function(stanza) {
            var from_nick = stanza.attr('from').split('/')[1];
            var body = stanza.find('body').text();
            var nick_elem = "<span class='" + this.options.nick_class  + "'>" + from_nick  + "</span>";
            var body_elem = "<span class='" + this.options.body_class  + "'>" + body + "</span><br />";
            var status_msg = "<span class='" + this.options.status_message_class + "'>" + body_elem + "</span>";

            var message = this.is_status(body) ? status_msg : nick_elem + ": " + body_elem;
            message = this.process_display_message(message, from_nick);
            var message_div = "<div class='" + this.options.message_div_class + "'>" + message  + "</div>";

            $(this.options.chat).append(message_div)
                                .animate(
                                        { 
                                            scrollTop: $(this.options.chat).attr("scrollHeight") - $(this.options.chat).height()
                                        }
                                )
                                .linkify();
            console.log("window in focus?", this.focus);
        },

        /* determine whether the message is a status change */
        is_status: function (text) {
            return (text.indexOf('/me') > -1);
        },

        /* process the message before displaying */
        process_display_message: function(text, sender) {
            return text.replace('/me', sender);
        },

        /* TODO: support private messages */
        display_pm: function(stanza) {

        },

        /* TODO: support subject changes */
        _handle_subject: function(stanza) {

        },

        /* TODO: support typing/paused events */
        _handle_composing: function(stanza) {

        },

        /* callback fired when presence is received from the xmpp server */
        _on_presence: function(context) {
            return function(stanza_xml) {
                var stanza = $(stanza_xml);
                var type = stanza.attr('type') || 'available';
                var from_nick = stanza.attr('from').split('/')[1];

                if (type == 'available') {
                   context._handle_available(from_nick);
                }
                else if (type == 'unavailable') {
                   context._handle_unavailable(from_nick);
                }
                return true;
            };
        },

        /* callback fired when someone becomes joins the room
         *
         * params: 
         *     from_nick: nickname of the user 
         *
         * the users' nick added to the 'roster' ul element of options
         */
        _handle_available: function(from_nick) {
            $(this.options.roster).append("<li id='roster_item_" + from_nick + "'>" + from_nick + "</li>");
        },

        /* callback fired when someone leaves the room
         *
         * params: 
         *     from_nick: nickname of the user
         *
         *  the user' nick will be removed from the 'roster'
         */
        _handle_unavailable: function(from_nick) {
            $("#roster_item_" + from_nick).remove();
        },

        /* callback fired when a user leaves the room */
        _on_leave: function() {

        },

        /* process text message before sending it to xmpp server */
        process_send_message: function(text) {
            return text;
        },

        /* sends message to the xmpp server 
         *
         * params:
         *   to: whom to send the message
         *
         *       sending a message to this._room 
         *       will send a groupchat to the room
         *   msg: body of the message
         *
         */
        send_message: function(to, msg) {
            msg = this.process_send_message(msg);
            this._conn.send(
                    $msg({to: to,
                                from: this._conn.jid,
                                type: 'groupchat',
                                id: this._conn.getUniqueId()})
                    .c("body",
                    {xmlns: Strophe.NS.CLIENT}).t(msg)
                    .up()
                    .c("active", {xmlns: "http://jabber.org/protocol/chatstates"})
            );
        },

        focus: true, /* whether the window has focus */
        unread: 0 /* count of unread messages */
    };

    /* Stroqe class that initializes 
     * event handlers, auto connects to the server
     * and auto joins 'room' of options
     *
     */
    function Stroqe(options) {
        var that = this;
        this.options = options;

        $(options.txt_msg).bind("send_message", function(e, msg) {
            that.send_message(that._room, msg);
            $(this).val("");
        });

        $(options.txt_msg).keypress(function(event) {
            if (event.keyCode == '13') {
                event.preventDefault();
                $(this).trigger("send_message", [ $(this).val() ]);
            }
        });


        function toggle_connect() {
            var connection = that.connect(options.username, options.password);
            $(this).val("Disconnect");
        }

        function toggle_disconnect() {
            that.disconnect();
            $(this).val("Connect");
        }

        $(options.btn_connect).toggle(toggle_disconnect, toggle_connect);
        var connection = that.connect(options.username, options.password);

        $(options.btn_join).toggle( 
            function() {
                that.leave();
                $(this).val("Join");
            },
            function() {
                that.join(options.room);
                $(this).val("Leave");
            }
        );

        $(window).blur(function(context) {
            return function() {
                context.focus = false;
            };
        }(this));

        $(window).focus(function(context) {
            return function() {
                context.focus = true;
            };
        }(this));

    }

    // TODO: return jquery element like a good plugin

    $.fn.stropify = function(opts) {
        var defaults = {
            chat: '#chat',
            txt_msg: '#txt_message',
            btn_connect: '#btn_connect',
            btn_join: '#btn_join',
            nick_class: 'nick',
            body_class: 'body',
            status_message_class: 'status',
            message_div_class: 'message',
            roster: '#roster'
        };
        var options = $.extend(defaults, opts);
        return new Stroqe(options);
    };

})(jQuery);

