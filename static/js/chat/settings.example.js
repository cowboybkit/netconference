settings = {

    /* path to the BOSH url configured in nginx */
    BOSH_SERVICE: '/xmpp-httpbind',     

    /* domain of the BOSH and XMPP url, usually same as HTTP domain */   
    DOMAIN: 'localhost', 

    /* domain where ejabberd is configured to create rooms */
    MUC_DOMAIN: 'conference.localhost',

    DEBUG: true ,

    /* URL to the sound file that will be played on new messages */
    MESSAGE_SOUND_URL: '/site_media/speeqewebclient/KDE-Im-Irc-Event.ogg'

};

