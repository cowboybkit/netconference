var drec;

var screenWidth = screen.availWidth - 10;
var screenHeight = screen.availHeight + 20;

if(screenWidth < 600){
    screenWidth = 752;
}
if(screenHeight < 600){
    screenHeight = 700;
}

var rec = "5,5,"+screenWidth+","+screenHeight;
var capturing = false;

// initial rectangle
var rx1 = 10;
// var rx2 = rx1 + screenWidth;
var rx2 = screenWidth - rx1;
    var ry1 =  15;
// var ry2	= ry1 + screenHeight;
    var ry2 = screenHeight + ry1;
var mpad = 60;

var scrw = 0;
var scrh = 0;
var conn = "";
var si = setInterval  ( "getDesktopRectangle()", 500 );

// gets desktop rectangle
function getDesktopRectangle(){
 	if(drec == undefined){
		// make sure VHScreenShare.jar is loaded before accessing it or else there will be a javascript error...
		try{
			drec = thisMovie("VHScreenShare").get_desktoprect().split(",");
			scrw = parseInt(drec[2]); // get max screen width
			scrh = parseInt(drec[3]); // get max screen height
			clearInterval(si); // clear interval
		}catch(err){
			
		}
	}		
}

//setInterval  ( "trackMouse()", 500 );

// function trackMouse ( )
// {
// 
//      if(!capturing) return;
// 
//      if(drec == undefined){
//          drec = thisMovie("VHScreenShare").get_desktoprect().split(",");
//          scrw = parseInt(drec[2]);
//      scrh = parseInt(drec[3]);
//  }
// 
//      var rec2 = thisMovie("VHScreenShare").get_mousepointer();
// 
//      if(rec == rec2+",752,700") return;
// 
//      var recUpdated = false; 
//      var pta = rec2.split(",");
//      var ptx = parseInt(pta[0]);
//      var pty = parseInt(pta[1]);
// 
// 
//  var pptx1 = ptx-mpad;
//  pptx1 = pptx1<0? 0 : pptx1;
//  var pptx2 = ptx+mpad;
//  pptx2 = pptx2>scrw? scrw :pptx2;
//  var ppty1 = pty-mpad;
//  ppty1 = ppty1<0? 0 : ppty1;
//  var ppty2 = pty+mpad;
//  ppty2 = ppty2>scrh? scrh : ppty2;
// 
// 
//      if(pptx1 < rx1){
//          rx1 = pptx1;
//          rx2 =  rx1 + screenWidth;
//          recUpdated = true;
//      }else if(pptx2>rx2){
//          rx2 = pptx2;
//          rx1 =  rx2 - screenWidth;
//          recUpdated = true;
//      }
// 
//      if(ppty1<ry1){
//          ry1 = ppty1;
//          ry2 =  ry1 + screenHeight;
//          recUpdated = true;
//      }else if(ppty2>ry2){
//          ry2 =ppty2;
//          ry1 =  ry2 - screenHeight;
//          recUpdated = true;
//      }       
// 
//      if(recUpdated){
//          rec2 = rx1+","+ry1+","+screenWidth+","+screenHeight;    
//          thisMovie("VHScreenShare").move_area(rec2);
//          thisMovie("VHScreenShare").update_frame("5,5,9054090,9054090,752x700,"+screenWidth+"x"+screenHeight, rec2);
//          rec = rec2;
//      }
// }

function doAlert(s) {
   alert(s);
}

function startJScrCap(arg) {
	// arg = rtmp+ stream name from flash	
	conn = arg;
	
	var arg1 = arg + ",";	
	arg1 += "100" + ","; // frame per second
	arg1 += "3000" + ","; // keyframe interval
	arg1 += "1000000" + ","; // bits per second
	arg1 += "1000" + ","; // maximum delay
	arg1 += "h264,11,0"; // encoding profile
	
	if(rec == undefined)
		rec = drec;
	//  no resize points
	//thisMovie("VHScreenShare").update_frame("1,5,16728128,16776960,200x200,"+scrw+"x"+scrh, rec); 
	
    thisMovie("VHScreenShare").update_frame("1,5,9054090,9054090,1200x900,1200x900, "+ rec);
    thisMovie("VHScreenShare").start_streaming(arg1, rec);
}

function stopJScrCap()  {
        thisMovie("VHScreenShare").update_frame("0,4,9054090,9054090,1200x900,1200x900, "+ rec);
        thisMovie("VHScreenShare").stop_streaming();
}

function moveJScrCap(arg1) {
	thisMovie("VHScreenShare").move_area(arg1);
    // thisMovie("VHScreenShare").update_frame("5,5,9054090,9054090,752x700,"+screenWidth+"x"+screenHeight, arg1);
    rec = arg1;
}

function thisMovie(movieName) {
    if (navigator.appName.indexOf("Microsoft") != -1) {
        return window[movieName];
    } else {
        return document[movieName];
    }
}

function JScrCap_Event(operation, code, desc) {
    var player = document.getElementById('conference').getElementsByTagName('object')[0];
	try{
	    player.vhLogCallback(operation, code, desc);
	} catch(err){
	    
	}
	if(operation == 'frame_bounds'){
		moveJScrCap(code);
	}
}