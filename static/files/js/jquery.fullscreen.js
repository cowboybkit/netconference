/**
 * @name		jQuery FullScreen Plugin
 * @author		Martin Angelov
 * @version 	1.0
 * @url			http://tutorialzine.com/2012/02/enhance-your-website-fullscreen-api/
 * @license		MIT License
 */

(function($){
	
	// Adding a new test to the jQuery support object
	$.support.fullscreen = supportFullScreen();
	
	// Creating the plugin
	$.fn.fullScreen = function(props){
		
		if(!$.support.fullscreen || this.length != 1){
			
			// The plugin can be called only
			// on one element at a time
			
			return this;
		}
		
		if(fullScreenStatus()){
			// if we are already in fullscreen, exit
			cancelFullScreen();
			return this;
		}
		
		// You can potentially pas two arguments a color
		// for the background and a callback function
		
		var options = $.extend({
			'background' : '#111',
			'callback'	 : function(){}
		}, props);
		
		// This temporary div is the element that is
		// actually going to be enlarged in full screen
		
		var fs = $('<div>',{
			'css' : {
				/*'overflow-y' : 'auto',*/
				'background' : options.background,
				'width'		 : '100%',
				'height'	 : '100%'
			}
		});

		var elem = this;

		// You can use the .fullScreen class to
		// apply styling to your element
		elem.addClass('fullScreen');
		
		// Inserting our element in the temporary
		// div, after which we zoom it in fullscreen
		fs.insertBefore(elem);
		fs.append(elem);
		requestFullScreen(fs.get(0));
		
		fs.click(function(e){
			if(e.target == this){
				// If the black bar was clicked
				cancelFullScreen();
			}
		});
		
		elem.cancel = function(){
			cancelFullScreen();
			return elem;
		};
		
		onFullScreenEvent(function(fullScreen){
			
			if(!fullScreen){
				// EXIT FULLSCREEN
				$('#btn_full_screen').attr('title', 'Full Screen');
				// We have exited full screen.
				// Remove the class and destroy
				// the temporary div
				$('.gzoomSlider').remove();
				$('.ui-icon ui-icon-circle-plus').remove();
				$('.ui-icon ui-icon-circle-minus').remove();
				$('#view_scale').val('Auto Fit');
				
				elem.removeClass('fullScreen').insertBefore(fs);
				fs.remove();
				$('#file_view_content_image_right_col').css({'width':width_before_full_screen, 'height':'auto'});
				$('#file_view_content_doc_right_col').css({'width':width_before_full_screen, 'height':'auto'});
				
				$('#file_view_content_doc_right_col').removeClass('file_view_content_doc_right_col_full_screen');
				$('#file_view_content_image_right_col').removeClass('file_view_content_image_right_col_full_screen');
				
				$('#file_view_content_image_right_col').addClass('file_view_content_image_right_col_normal');
				$('#file_view_action').css({'width':width_before_full_screen});
				
				/*THEM O DAY*/
				/*var html_doc_exit_full = $("#view_file_document_container").clone();
				var html_image_exit_full = $("#view_file_image_container").clone();
				
				$('.gzoomwrap').remove();
				$('#file_view_content_doc_right_col').append(html_doc_exit_full);*/
				/*=====================*/
				
				$("#view_file_document_container").gzoom({
		      	sW: fix_width,
		      	sH: fix_height,
		      	lW: fix_width*3,
		      	lH: fix_height*3,
		      	lighbox : false,
				}).css({'text-align':'center','vertical-align':'middle'});
				
				
				/*THEM O DAY*/
				
				//$('.gzoomwrap').remove();
				//$('#file_view_content_image_right_col').append(html_image_exit_full);
				/*=====================*/
				
				$("#view_file_image_container").gzoom({
		      	sW: fix_width,
		      	sH: fix_height,
		      	lW: fix_width*3,
		      	lH: fix_height*3,
		      	lighbox : false,
				}).css({'text-align':'center','vertical-align':'middle'});
				
			}else{
				// ENTER FULLSCREEN
				
				$('#btn_full_screen').attr('title','Exit Full Screen');
				$('.gzoomSlider').remove();
				$('.ui-icon ui-icon-circle-plus').remove();
				$('.ui-icon ui-icon-circle-minus').remove();
				$('#view_scale').val('Auto Fit');
				
				options.callback(fullScreen);
				$('#file_view_content_doc_right_col').addClass('file_view_content_doc_right_col_full_screen');
				$('#file_view_content_image_right_col').addClass('file_view_content_image_right_col_full_screen');
				
				temp_img_height = client_screen_height;
				temp_img_width = parseInt(temp_img_height*imgWidth/imgHeight);
				if(temp_img_width > client_screen_width) {
					temp_img_width = client_screen_width;
					
					temp_img_height = parseInt(temp_img_width*imgHeight/imgWidth);
				}
				
				$('#file_view_content_image_right_col').css({'width':temp_img_width, 'height':temp_img_height});
				$('#file_view_action').css({'width':temp_img_width});
				
				$('#file_view_content_image_right_col').removeClass('file_view_content_image_right_col_normal');
				
				$('#file_view_action').removeClass('width_945');
				
				/*THEM O DAY*/
				/*var html_image_full = $("#view_file_image_container").clone();
				var html_doc_full = $("#view_file_document_container").clone();
				$('.gzoomwrap').remove();
				$('#file_view_content_image_right_col').append(html_image_full);*/
				/*=====================*/
				$("#view_file_image_container").gzoom({
		      	sW: temp_img_width,
		      	sH: temp_img_height,
		      	lW: temp_img_width*3,
		      	lH: temp_img_height*3,
		      	lighbox : false,
				}).css({'text-align':'center','vertical-align':'middle'});
					
				$('#file_view_content_doc_right_col').css({'width':temp_img_width, 'height':temp_img_height});
				
				$('#file_view_content_doc_right_col').removeClass('file_view_content_doc_right_col_normal');
				
				/*THEM O DAY*/
				
				//$('#file_view_content_doc_right_col').append(html_doc_full);
				/*=====================*/
				
				
				$("#view_file_document_container").gzoom({
		      	sW: temp_img_width,
		      	sH: temp_img_height,
		      	lW: temp_img_width*3,
		      	lH: temp_img_height*3,
		      	lighbox : false,
				}).css({'text-align':'center','vertical-align':'middle'});
				$(window).on('mousemove', function(e) {
			        e.preventDefault();            
			    })
			}
			
			// Calling the user supplied callback
			
		});
		
		return elem;
	};
	
	
	// These helper functions available only to our plugin scope.


	function supportFullScreen(){
		var doc = document.documentElement;
		
		return	('requestFullscreen' in doc) ||
				('mozRequestFullScreen' in doc && document.mozFullScreenEnabled) ||
				('webkitRequestFullScreen' in doc);
	}

	function requestFullScreen(elem){

		if (elem.requestFullscreen) {
		    elem.requestFullscreen();
		}
		else if (elem.mozRequestFullScreen) {
		    elem.mozRequestFullScreen();
		}
		else if (elem.webkitRequestFullScreen) {
		    elem.webkitRequestFullScreen();
		}
	}

	function fullScreenStatus(){
		return	document.fullscreen ||
				document.mozFullScreen ||
				document.webkitIsFullScreen;
	}
	
	function cancelFullScreen(){
		if (document.exitFullscreen) {
		    document.exitFullscreen();
		}
		else if (document.mozCancelFullScreen) {
		    document.mozCancelFullScreen();
		}
		else if (document.webkitCancelFullScreen) {
		    document.webkitCancelFullScreen();
		}
	}

	function onFullScreenEvent(callback){
		$(document).on("fullscreenchange mozfullscreenchange webkitfullscreenchange", function(){
			// The full screen status is automatically
			// passed to our callback as an argument.
			callback(fullScreenStatus());
		});
	}

})(jQuery);
