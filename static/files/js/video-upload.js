
$(document).ready(function(){
	$('#videoUploadID').fileupload({
    dataType: 'html',
    type:'POST',
    maxNumberOfFiles: 1,
    acceptFileTypes: /.(flv|mov|mpg|avi|wmv|mp4|mpeg4|f4v)/i,
    attachmentUpload: false,
    mediapath: 'MEDIACONFIGSITE',
	}).bind('fileuploadsubmit', function (e, data){
		   var uuid = data.context.find('input[name$="uuid"]').val();
		   var filename_text = data.context.find('.name').html();
		   
		   var title = data.context.find('.name').html();
		   
		   var title_custom = $('#title_videouploadFile').val();
		   if (title_custom!=''){
			   title = title_custom;
		   }
		   var selected_node = $('#uploadVideoTree').find('.jstree-clicked').html();
		   folder = null;
		   folder_name = null;
		   if(selected_node!=null) {
				var s_node = $('#uploadVideoTree').find('.jstree-clicked');
				folder = s_node.parent().attr('id');
				folder_name = s_node.parent().data().name;
			}
		   var info = getUserNCInfo();
		   data.formData = {
		   uid: parseInt(info.split(';',3)[0]),
		   uuid: uuid,
		   title: title,
		   cid: info.split(';',3)[2],
		   username: info.split(';',3)[1],
		   visibility: 'U',
		   folder_id:folder,
		   };
		    if (!data.formData.cid||!data.formData.uid||!data.formData.username||!data.formData.visibility) {
		    	return false;
		     }
		    $('#descriptionForm').find('input[type$="text"]').each(function(){
				$(this).prop('disabled', true);
				$('#UploadVideoStartId').prop('disabled', true);
			});
		    
		    $('#uploadVideoTreeblock').hide();
		    $('#uploadVideoSelectHolder').hide();
		    
//		    var text = 'Uploading '+ filename_text + ((folder_name)?(' into '+folder_name):'');
//		    $('#text_status_upload').html(text);
//		    $('#text_status_upload').css('text-decoration','blink');
		    if ( $.browser.msie ) {
		    	$('#IeHackId').attr('value', uuid);
		    }
	});

	if ( $.browser.msie ) {
		$('#videoUploadID').bind('fileuploadalways', function(e, data){
			var uuid = $('#IeHackId').attr('value');
			
			$('#descriptionForm').find('input[type$="text"]').each(function(){
				$(this).prop('disabled', false);
			});
			$('#UploadVideoStartId').prop('disabled', false);
			
//			$('#text_status_upload').hide();
			
		    $('#uploadVideoSelectHolder').show();
		    playVideoByUuid(uuid);
		});
	}else{
		$('#videoUploadID').bind('fileuploaddone', function(e, data){
			var result = data.result.toString();
			var uuid = result.split(',',2)[1];
			$('#descriptionForm').find('input[type$="text"]').each(function(){
				$(this).prop('disabled', false);
			});
			$('#UploadVideoStartId').prop('disabled', false);
			
//			$('#text_status_upload').hide();
			 $('#uploadVideoSelectHolder').show();
			playVideoByUuid(uuid);
		});
	
	}

});
