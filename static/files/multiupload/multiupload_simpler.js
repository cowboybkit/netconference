var store_data_before_submit = null;
var folder_id = null;

var total_count_files = 0;
var uploaded_count_files = 0;
var total_size_upload = 0;

// data common
var general_description = '';
var general_tags = '';
var general_privacy = '';
var general_password = '';


check_privacy_value = function(){
//	var privacy =  $("input[name='privacy']:checked").val();
	window.location = '/files/';
}
check_allow_space = function(){
	$('#table_attach_file tr').each(function(){
		total_size_upload+=	parseInt($(this).find('input[name="size_raw"]:first').val());
	});
	var return_value = true;
	$.ajax({
		  url: "/vmail/getAllowSpaceInfo?param="+total_size_upload,
		  type: 'GET',
		  async:false,
		  dataType: 'html',
		  success: function(html){
			  if(html == 'success'){
				  return_value = true;
			  }else{
				  $('#allowspace_btn').click();
				  return_value =  false;
			  }
		  }
		});
	
	return return_value;
};
check_data_before_submit = function(){
	var result = true;
	var privacy =  $("input[name='privacy']:checked").val();
	if($('#confirm_password').val()!=$('#password').val()&&privacy == 'protected'){
		$('body,html').animate({
			scrollTop: 500
		}, 1000);
		$('#error_pass').show();
		$('#password').focus();
		result = false;
	}else{
		$('#error_pass').hide();
	}
	
	var selected_node = $('#jstree_holder').find('.jstree-clicked').html();
   if(selected_node!=null) {
		var s_node = $('#jstree_holder').find('.jstree-clicked');
		if(s_node.length>1){
			$('#alert_title').html('Warning');
			$('#alert_message').html("Please select only one folder");
			$('#alert_overlay').click();
			folder_id = null;
			result= false;
		}else{
			folder_id = s_node.parent().attr('id');
		}
		
	}
   
   
   return result;
   
};

hide_element_then_upload = function(){
	$('body,html').animate({
		scrollTop: 0
	}, 1000);
	$('#form_upload_hide').hide();
	$('#notification_p').hide();
	$('.left_detail_block').hide();
	$('.line-option-divide').hide();
	$('.privacy_block').hide();
	$('.hostoption_block').hide();
	$('.layoutoption_block').hide();
	$('#extra_button').fadeOut();
	$('#view_upload_list').fadeOut();
	
	$('#multiupload_action_button').fadeOut();
	$('#option_container').removeClass('option_container');
	$('#footer_clear').hide();
	$('#progress-container').css({'margin-top':'-200px','margin-left':'-180px'});
	$('#uploading_p').show();
	$('#space_uploading').show();
	$('#please_wait').show();
	$('#continous_button').show().prop('disabled', false).addClass('continue_disable');
	$('#warning_ppt_type').fadeOut();
	$('#warning_video_type').fadeOut();
}
upload_multi_submit = function(){
	
	if($('#table_attach_file').find('tr').length<1){
		$('body,html').animate({
			scrollTop: 0
		}, 500);
		return false;
	}
	
	if(!check_data_before_submit())
		return false;
	
	if(!check_allow_space())
		return false;
	
	
	total_count_files = $('#table_attach_file').find('tr').length;
	store_data_before_submit = $('#table_attach_file').clone();
	
	general_privacy =  $("input[name='privacy']:checked").val();
	   general_password = $('#password').val();
	   general_tags = $('#current_tags').val();
	   general_description = $('#current_description').val();
	hide_element_then_upload();
	
	$('#attachStartHidden').click();
	return false;
};

display_data_tr = function(row){
	if(row!=null)
	{
		var current_uuid = row.find('td:first').html();
		
		row.addClass('uploadfile_selected');
	}
}


remove_fileupload_selected = function(){
	var row = -1;
	$('#Attachedlist tr').each(function(){
		if($(this).hasClass('uploadfile_selected')){
			var that = $(this);
			row = that.parent().children().index(that);
			
			
			var uuid = $(this).find('td:first').html();
			$('#table_attach_file tr').each(function(){
				if($(this).find('input[name="uuid"]:first').val() == uuid)
				{
					$(this).remove();
				}
			});
			that.remove();
		}
	});
	if(row!=-1){
		var next_row = $('#Attachedlist').find('tr:eq('+row+')');
		if(next_row!=null)
		{
			display_data_tr(next_row);
		}
		else{
			next_row = $('#Attachedlist').find('tr:eq('+row-1+')');
			display_data_tr(next_row);
		}
	}
	
	
};

update_data_before_change = function(){
	$('#Attachedlist').find('tr').each(function(){
		var previous_row_selected = $(this);
		if(previous_row_selected.hasClass('uploadfile_selected')){
			var uuid = previous_row_selected.find('td:first').html();
			$('#table_attach_file tr').each(function(){
				if($(this).find('input[name="uuid"]:first').val() == uuid)
				{
					$(this).find('input[name="tags"]:first').val($('#current_tags').val());
					$(this).find('input[name="description"]:first').val($('#current_description').val());
					$(this).find('input[name="title"]:first').val($('#current_name').val());
				}
			});
		}
	});
};
$(document).ready(function(){
	var triggers = $("#allowspace_btn").overlay({
	      // some mask tweaks suitable for modal dialogs
	      mask: {
	        color: '#000',
	        loadSpeed: 200,
	        opacity: 0.5
	      },
	 
	      closeOnClick: false
	  });
	$("#alert_overlay").overlay({
		 
	      // some mask tweaks suitable for modal dialogs
	      mask: {
	        color: '#000',
	        loadSpeed: 200,
	        opacity: 0.5
	      },
	 
	      closeOnClick: false
	  });
	$("#confirm_overlay").overlay({
		 
	      // some mask tweaks suitable for modal dialogs
	      mask: {
	        color: '#000',
	        loadSpeed: 200,
	        opacity: 0.5
	      },
	 
	      closeOnClick: false
	  });
	
	var buttons_confirm = $("#confirm_container button").click(function(e) {
		var yes = buttons_confirm.index(this);
	 	if(yes == 1) {
	 		$("#jstree_holder").jstree('remove');
	 	}
	});
	$('#Attachedlist tr').live('click',function(){
		var current_row = $(this);
		
		current_row.parent().find('tr').removeClass('uploadfile_selected');
		current_row.addClass('uploadfile_selected');
	});

if ( $.browser.msie) {
		$('#attachUploadID').fileupload({
		    dataType: 'html',
		    type:'POST',
		    attachmentUpload: true,
		    sequentialUploads: true,
			}).bind('fileuploadsubmit', function (e, data){
				   var uuid = data.context.find('input[name$="uuid"]').val();
				   var title = data.context.find('input[name$="title"]').val();
				   data.formData = {
						   uid: $('#user_id').val(),
						   uuid: uuid,
						   title: title,
						   cid: $('#bucket').val(),
						   username: $('#username').val(),
						   visibility: 'U',
				   };
				    if (!data.formData.cid||!data.formData.uid||!data.formData.username||!data.formData.visibility) {
				        return false;
				     }
				  
				   
			});
		
			$('#attachUploadID').bind('fileuploadalways',  function(e, data){
				var uuid = store_data_before_submit.find('tr:eq('+uploaded_count_files+')').find('input[name="uuid"]:first').val();
				if(folder_id==null){
					folder_id = 0
				}
				store_data_before_submit.find('tr').each(function(){
					var that = $(this);
					var response_uuid = that.find('input[name="uuid"]:first').val();
					if(response_uuid == uuid)
					{
						
						var description = general_description;
						var tags = general_tags
						var privacy =  general_privacy
						$.ajax({
							url:'/files/process_multiupload/',
							type:'POST',
							async:false,
							dataType:'html',
							data:'uuid='+response_uuid+'&description='+description+'&tags_string='
								+tags+'&password='+$('#password').val()+'&privacy='+privacy+'&folderid='+folder_id,
							success:function(result){
								
							}
						});
						
						
					}
				});
				$('#Attachedlist tr').each(function(){
					if($(this).find('td:first').html() == uuid){
						$(this).remove();
					}
				});
				
				uploaded_count_files ++;
				$('#progress-bar-show').progressbar(
                        'value',
                        parseInt(uploaded_count_files / total_count_files * 100, 10)
                    );
				if(uploaded_count_files == total_count_files){
					//window.location = '/files/?folder='+folder_id;
					$('#continous_button').click(function(){
						window.location = '/files/?folder='+folder_id;
					});
					setTimeout(function(){
						$('#continous_button').removeClass('continue_disable').removeProp('disabled');
						$('#uploading_p').hide();
						$('#progress-bar-show').show();
						$('.fileupload-progressbar').progressbar('value',100).show();
					},500);
				}
				
			});
				    
				    
	}else{
		$('#attachUploadID').fileupload({
		    dataType: 'html',
		    type:'POST',
		    attachmentUpload: true,
			})
			.bind('fileuploadsubmit', function (e, data){
				   var uuid = data.context.find('input[name$="uuid"]').val();
				   var title = data.context.find('input[name$="title"]').val();
				   data.formData = {
						   uid: $('#user_id').val(),
						   uuid: uuid,
						   title: title,
						   cid: $('#bucket').val(),
						   username: $('#username').val(),
						   visibility: 'U',
				   };
				    if (!data.formData.cid||!data.formData.uid||!data.formData.username||!data.formData.visibility) {
				        return false;
				     }
				    
			});
			$('#attachUploadID').bind('fileuploaddone', function(e, data){
				var result = data.result.toString();
				var uuid = result.split(',',2)[1];
				if(folder_id==null){
					folder_id = 0
				}
				$.ajax({
					url:'/files/process_multiupload/',
					type:'POST',
					async:false,
					dataType:'html',
					data:'uuid='+uuid+'&description='+general_description+'&tags_string='
						+general_tags+'&password='+$('#password').val()+'&privacy='+general_privacy+'&folderid='+folder_id,
					success:function(result){
						
					}
				});
				$('#Attachedlist tr').each(function(){
					if($(this).find('td:first').html() == uuid){
						$(this).remove();
					}
				});
				uploaded_count_files ++;
				if(uploaded_count_files == total_count_files){
					$('#continous_button').click(function(){
						window.location = '/files/?folder='+folder_id;
					});
					
					setTimeout(function(){
						$('#continous_button').removeClass('continue_disable').removeProp('disabled');
						$('#uploading_p').hide();
						$('#progress-bar-show').show();
						$('.fileupload-progressbar').progressbar('value',100).show();
					},500);
					
				}
				
			});
	}

});

ppt_container_click = function(){
	is_ppt_show = false;
	$('#warning_ppt_type').slideUp();
}
video_container_click = function(){
	is_video_show = false;
	$('#warning_video_type').slideUp();
}