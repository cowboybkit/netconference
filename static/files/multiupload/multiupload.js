var store_data_before_submit = null;
var folder_id = null;

var total_count_files = 0;
var uploaded_count_files = 0;
var total_size_upload = 0;
check_privacy_value = function(){
//	var privacy =  $("input[name='privacy']:checked").val();
	window.location = '/files/';
}
check_allow_space = function(){
	
	$('#table_attach_file tr').each(function(){
		total_size_upload+=	parseInt($(this).find('input[name="size_raw"]:first').val());
	});
	var return_value = true;
	$.ajax({
		  url: "/vmail/getAllowSpaceInfo?param="+total_size_upload,
		  type: 'GET',
		  async:false,
		  dataType: 'html',
		  success: function(html){
			  if(html == 'success'){
				  return_value = true;
			  }else{
				  $('#allowspace_btn').click();
				  return_value =  false;
			  }
		  }
		});
	
	return return_value;
};
check_data_before_submit = function(){
	
	var privacy =  $("input[name='privacy']:checked").val();
	if($('#password_confirm').val()!=$('#password').val()&&privacy == 'protected'){
		$('#error_pass').show();
	}else{
		$('#error_pass').hide();
	}
	
	var selected_node = $('#jstree_holder').find('.jstree-clicked').html();
   if(selected_node!=null) {
		var s_node = $('#jstree_holder').find('.jstree-clicked');
		if(s_node.length>1){
			alert('You only select one folder to upload file!!!');
			folder_id = null;
			return false;
		}else{
			folder_id = s_node.parent().attr('id');
			return true;
		}
		
	}
};

hide_element_then_upload = function(){
	$('body,html').animate({
		scrollTop: 0
	}, 1000);
	$('#form_upload_hide').fadeOut();
	$('.multiupload_file_info').fadeOut();
	$('.multiupload_file_folder').fadeOut();
	$('#extra_button').fadeOut();
	$('#view_upload_list').fadeOut();
	$('#notification_p').fadeOut();
	$('#multiupload_action_button').fadeOut();
	$('#uploading_p').show();
	$('#space_uploading').show();
	$('#please_wait').show();
	$('#progress-container').css('margin-top',0);
	$('#continous_button').show().prop('disabled', false).addClass('continue_disable');
	$('#warning_ppt_type').fadeOut();
	$('#warning_video_type').fadeOut();
}
upload_multi_submit = function(){
	if($('#table_attach_file').find('tr').length<1){
		$('body,html').animate({
			scrollTop: 0
		}, 500);
		return false;
	}
	
	
	
	if(!check_data_before_submit())
		return false;
	
	if(!check_allow_space())
		return false;
	
	update_data_before_change();
	
	total_count_files = $('#table_attach_file').find('tr').length;
	store_data_before_submit = $('#table_attach_file').clone();
	
	hide_element_then_upload();
//	$('#progress-bar-show').fadeIn();
	
	
	$('#attachStartHidden').click();
	return false;
};

display_data_tr = function(row){
	if(row!=null)
	{
		var current_uuid = row.find('td:first').html();
		$('#table_attach_file tr').each(function(){
			if($(this).find('input[name="uuid"]:first').val() == current_uuid)
			{
				 $('#current_tags').val($(this).find('input[name="tags"]:first').val());
				$('#current_description').val($(this).find('input[name="description"]:first').val()) ;
				$('#current_name').val($(this).find('input[name="title"]:first').val());
			}
		});
		row.addClass('uploadfile_selected');
	}
}


remove_fileupload_selected = function(){
	var row = -1;
	$('#Attachedlist tr').each(function(){
		if($(this).hasClass('uploadfile_selected')){
			var that = $(this);
			row = that.parent().children().index(that);
			
			
			var uuid = $(this).find('td:first').html();
			$('#table_attach_file tr').each(function(){
				if($(this).find('input[name="uuid"]:first').val() == uuid)
				{
					$(this).remove();
				}
			});
			that.remove();
		}
	});
	if(row!=-1){
		var next_row = $('#Attachedlist').find('tr:eq('+row+')');
		if(next_row!=null)
		{
			display_data_tr(next_row);
		}
		else{
			next_row = $('#Attachedlist').find('tr:eq('+row-1+')');
			display_data_tr(next_row);
		}
	}
	
	
};

update_data_before_change = function(){
	$('#Attachedlist').find('tr').each(function(){
		var previous_row_selected = $(this);
		if(previous_row_selected.hasClass('uploadfile_selected')){
			var uuid = previous_row_selected.find('td:first').html();
			$('#table_attach_file tr').each(function(){
				if($(this).find('input[name="uuid"]:first').val() == uuid)
				{
					$(this).find('input[name="tags"]:first').val($('#current_tags').val());
					$(this).find('input[name="description"]:first').val($('#current_description').val());
					$(this).find('input[name="title"]:first').val($('#current_name').val());
				}
			});
		}
	});
};
$(document).ready(function(){
	var triggers = $("#allowspace_btn").overlay({
	      // some mask tweaks suitable for modal dialogs
	      mask: {
	        color: '#ebecff',
	        loadSpeed: 200,
	        opacity: 0.5
	      },
	 
	      closeOnClick: false
	  });
	
	$('#Attachedlist tr').live('click',function(){
		var current_row = $(this);
		
		// Update data before change
		update_data_before_change();
	/*	$(this).parent().find('tr').each(function(){
			var previous_row_selected = $(this);
			if(previous_row_selected.hasClass('uploadfile_selected')){
				var uuid = previous_row_selected.find('td:first').html();
				$('#table_attach_file tr').each(function(){
					if($(this).find('input[name="uuid"]:first').val() == uuid)
					{
						$(this).find('input[name="tags"]:first').val($('#current_tags').val());
						$(this).find('input[name="description"]:first').val($('#current_description').val());
						$(this).find('input[name="title"]:first').val($('#current_name').val());
					}
				});
			}
		});*/
		
		// Display data current row selected
		var current_uuid = current_row.find('td:first').html();
		$('#table_attach_file tr').each(function(){
			if($(this).find('input[name="uuid"]:first').val() == current_uuid)
			{
				 $('#current_tags').val($(this).find('input[name="tags"]:first').val());
				$('#current_description').val($(this).find('input[name="description"]:first').val()) ;
				$('#current_name').val($(this).find('input[name="title"]:first').val());
			}
		});
		
		current_row.parent().find('tr').removeClass('uploadfile_selected');
		current_row.addClass('uploadfile_selected');
	});

if ( $.browser.msie) {
		$('#attachUploadID').fileupload({
		    dataType: 'html',
		    type:'POST',
//		    acceptFileTypes: /.(png|jpg|jpeg|mp3|acc|wav|flv|wmv|avi|f4v|mov|mpg|mpeg|pdf|doc|docx|xls|xlsx|odt|ods|odp|zip|rar|mp4)/i,
		    attachmentUpload: true,
		    sequentialUploads: true,
		    mediapath: 'MEDIACONFIGSITE',
		    after_add:function(){
		    	$('#Attachedlist tr').removeClass('uploadfile_selected');
		    	display_data_tr($('#Attachedlist').find('tr:first'));
		    },
			}).bind('fileuploadsubmit', function (e, data){
				   var uuid = data.context.find('input[name$="uuid"]').val();
				   var title = data.context.find('input[name$="title"]').val();
//				   var title = data.context.find('.name').html();
				   data.formData = {
						   uid: $('#user_id').val(),
						   uuid: uuid,
						   title: title,
						   cid: $('#bucket').val(),
						   username: $('#username').val(),
						   visibility: 'U',
				   };
				    if (!data.formData.cid||!data.formData.uid||!data.formData.username||!data.formData.visibility) {
				        return false;
				     }
				  
				   
			});
		
			$('#attachUploadID').bind('fileuploadalways',  function(e, data){
				var uuid = store_data_before_submit.find('tr:eq('+uploaded_count_files+')').find('input[name="uuid"]:first').val();
				if(folder_id==null){
					folder_id = 0
				}
				store_data_before_submit.find('tr').each(function(){
					var that = $(this);
					var response_uuid = that.find('input[name="uuid"]:first').val();
					if(response_uuid == uuid)
					{
						
						var title = that.find('input[name="title"]:first').val();
						var description = that.find('input[name="description"]:first').val();
						var tags = that.find('input[name="tags"]:first').val();
						var privacy =  $("input[name='privacy']:checked").val();
//						alert(title);
//						alert('uuid='+response_uuid+'&title='+title+'&description='+description+'&tags_string='
//								+tags+'&password='+$('#password').val()+'&privacy='+privacy+'&folderid='+folder_id);
						$.ajax({
							url:'/files/process_multiupload/',
							type:'POST',
							async:false,
							dataType:'html',
							data:'uuid='+response_uuid+'&title='+title+'&description='+description+'&tags_string='
								+tags+'&password='+$('#password').val()+'&privacy='+privacy+'&folderid='+folder_id,
							success:function(result){
							}
						});
						
						
					}
				});
				$('#Attachedlist tr').each(function(){
					if($(this).find('td:first').html() == uuid){
						$(this).remove();
					}
				});
				
				uploaded_count_files ++;
				$('#progress-bar-show').progressbar(
                        'value',
                        parseInt(uploaded_count_files / total_count_files * 100, 10)
                    );
				if(uploaded_count_files == total_count_files){
					window.location = '/files/?folder='+folder_id;
				}
				
				
				
			});
				    
				    
	}else{
		$('#attachUploadID').fileupload({
		    dataType: 'html',
		    type:'POST',
//		    acceptFileTypes: /.(png|jpg|jpeg|mp3|acc|wav|flv|wmv|avi|f4v|mov|mpg|mpeg|pdf|doc|docx|xls|xlsx|odt|ods|odp|zip|rar|mp4)/i,
		    attachmentUpload: true,
		    mediapath: 'MEDIACONFIGSITE',
		    after_add:function(){
		    	$('#Attachedlist tr').removeClass('uploadfile_selected');
		    	display_data_tr($('#Attachedlist').find('tr:first'));
		    	
		    },
			})
			.bind('fileuploadsubmit', function (e, data){
				   var uuid = data.context.find('input[name$="uuid"]').val();
				   var title = data.context.find('input[name$="title"]').val();
//				   var title = data.context.find('.name').html();
				   data.formData = {
						   uid: $('#user_id').val(),
						   uuid: uuid,
						   title: title,
						   cid: $('#bucket').val(),
						   username: $('#username').val(),
						   visibility: 'U',
				   };
				    if (!data.formData.cid||!data.formData.uid||!data.formData.username||!data.formData.visibility) {
				        return false;
				     }
				    
			});
			$('#attachUploadID').bind('fileuploaddone', function(e, data){
				var result = data.result.toString();
				var uuid = result.split(',',2)[1];
				if(folder_id==null){
					folder_id = 0
				}
				store_data_before_submit.find('tr').each(function(){
					var that = $(this);
					var response_uuid = that.find('input[name="uuid"]:first').val();
					if(response_uuid == uuid)
					{
						var title = that.find('input[name="title"]:first').val();
//						var size_raw = that.find('input[name="size_raw"]:first').val();
						var description = that.find('input[name="description"]:first').val();
						var tags = that.find('input[name="tags"]:first').val();
//						var filename = that.find('td:eq(1)').html();
						var privacy =  $("input[name='privacy']:checked").val();
//						alert('uuid='+response_uuid+'&title='+title+'&description='+description+'&tags_string='
//								+tags+'&password='+$('#password').val()+'&privacy='+privacy+'&folderid='+folder_id);
						$.ajax({
							url:'/files/process_multiupload/',
							type:'POST',
							async:false,
							dataType:'html',
							data:'uuid='+response_uuid+'&title='+title+'&description='+description+'&tags_string='
								+tags+'&password='+$('#password').val()+'&privacy='+privacy+'&folderid='+folder_id,
							success:function(result){
								
							}
						});
					}
				});
				$('#Attachedlist tr').each(function(){
					if($(this).find('td:first').html() == uuid){
						$(this).remove();
					}
				});
				uploaded_count_files ++;
				if(uploaded_count_files == total_count_files){
					window.location = '/files/?folder='+folder_id;
				}
				
			});
	}

});

ppt_container_click = function(){
	is_ppt_show = false;
	$('#warning_ppt_type').slideUp();
}
video_container_click = function(){
	is_video_show = false;
	$('#warning_video_type').slideUp();
}