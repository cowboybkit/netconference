$(document).ready(function(){
		$("#jstree_holder").jstree({ 
			// List of active plugins
			"plugins" : [ 
				/*"themes","json_data","ui","crrm","cookies","dnd","types" */
				"themes","json_data","ui","crrm","types" 
			],
			
			"types" : {
			    "valid_children" : [ "root_folder"],
			    "types" : {
			        "root_folder" : {
			            "valid_children" : [ "root_folder", "normal_folder"],
			            "icon" : { "image" : "{{ MEDIA_URL }}jstree/images/root_folder.png"},
			        },
			        "normal_folder" : {
			            "valid_children" : [ "root_folder", "normal_folder" ],
			            "icon" : { "image" : "{{ MEDIA_URL }}jstree/images/normal_folder.png"},
			        },
			    }
			},
			"themes" : {
				"theme" : "default",
				"dots" : false,
			},
			"json_data" : { 
				"ajax" : {
					"url" : "/files/getMoveTree",
					"async": true,	
					"cache": false,  
					} 
			},
			// Configuring the search plugin
			"metadata" : "a string, array, object, etc",
			"ui" : {
				"initially_select" : [ root_id ]
			},
			"core" : { 
				// just open those two nodes up
				// as this is an AJAX enabled tree, both will be downloaded from the server
				"initially_open" : [ root_id ] 
			}
		})
		.bind("create.jstree", function (e, data) {
			$.get(
				"/files/processActionTree", 
				{ 
					"operation" : "create_node", 
					"id" : data.rslt.parent.attr("id").replace("node_",""), 
					"position" : data.rslt.position,
					"title" : data.rslt.name,
					"type" : data.rslt.obj.attr("rel")
				}, 
				function (r) {
					if(r.status == 200) {
						$(data.rslt.obj).attr("id", r.id);
						$('#jstree_holder').jstree('refresh',-1);
					}
					else {
						$.jstree.rollback(data.rlbk);
					}
				}
			);
		})
		.bind("remove.jstree", function (e, data) {
			data.rslt.obj.each(function () {
				$.ajax({
					async : false,
					type: 'GET',
					url: "/files/processActionTree",
					data : { 
						"operation" : "remove_node", 
						"id" : this.id.replace("node_","")
					}, 
					error: function(){
						 $.jstree.rollback(data.rlbk);
					},
					success : function (r) {
						if(r.status == 200) {
							data.inst.refresh();
						}
						else if(r.status == 403){
							$('#alert_title').html('Warning');
	    					$('#alert_message').html("You can not delete this folder!!!");
	    					$('#alert_overlay').click();
	    					$.jstree.rollback(data.rlbk);
						} 
						else{
							$('#alert_title').html('Connection error ...');
	    					$('#alert_message').html("Problem with your internet connection. Please try again!!!");
	    					$('#alert_overlay').click();
							$.jstree.rollback(data.rlbk);
							//data.inst.refresh();
						}
					}
				});
			});
		})
		.bind("rename.jstree", function (e, data) {
			data.rslt.obj.each(function () {
	            $.ajax({
	                async: false,
	                type: 'GET',
	                url: "/files/processActionTree",
	                data: 
	                {
	                    "operation": "rename",
	                    "id": this.id,
	                    "new_name" : data.rslt.new_name 
	                },
	                error: function(){
	                	  $.jstree.rollback(data.rlbk);
	                },
	                success: function (r) {
	                	if (r.status == 200) {
	                    	$(data.rslt.oc).attr("id", r.id);
	                        if (data.rslt.cy && $(data.rslt.oc).children("UL").length) {
	                            data.inst.refresh(data.inst._get_parent(data.rslt.oc));
	                        }
	                    }else if(r.status == 403){
	                    	$('#alert_title').html('Warning');
	    					$('#alert_message').html("You can not rename this folder!!!");
	    					$('#alert_overlay').click();
	    					$.jstree.rollback(data.rlbk);
	                    }
	                    else {
	                    	
	                    	$.jstree.rollback(data.rlbk);
	                    }
	                }       
	            });
	        });
		})
		.bind("move_node.jstree", function (e, data) {
			data.rslt.o.each(function (i) {
				$.ajax({
					async : false,
					type: 'GET',
					url: "/files/processActionTree",
					data : { 
						"operation" : "move_node", 
						"id" : $(this).attr("id").replace("node_",""), 
						"ref" : data.rslt.cr === -1 ? 1 : data.rslt.np.attr("id").replace("node_",""), 
						"position" : data.rslt.cp + i,
						"title" : data.rslt.name,
						"copy" : data.rslt.cy ? 1 : 0
					},
					error: function(result){
						$.jstree.rollback(data.rlbk);
					},
					success : function (r) {
						if(r.status==200) {
							$(data.rslt.oc).attr("id",r.id);
							/*if(data.rslt.cy && $(data.rslt.oc).children("UL").length) {
								data.inst.refresh(data.inst._get_parent(data.rslt.oc));
							}*/
						}
						else {
							$.jstree.rollback(data.rlbk);
						}
						$("#analyze").click();
					}
				});
			});
		});
		$("#mmenu input").click(function () {
			switch(this.id) {
				case "add_default":
				case "add_folder":
					$("#jstree_holder").jstree("create", null, "last", { "attr" : { "rel" : "normal_folder" } });
					break;
				case "search":
					$("#jstree_holder").jstree("search", document.getElementById("text").value);
					break;
				case "remove":
					var selected_node = $('#jstree_holder').find('.jstree-clicked').html();
				    if(selected_node!=null) {
						var s_node = $('#jstree_holder').find('.jstree-clicked');
						total_files = s_node.parent().data('total_files');
						name = s_node.parent().data('name');
						if(s_node.parent().attr('rel')=='root_folder'){
							$('#alert_title').html('Warning');
							$('#alert_message').html("You can not delete this folder!!!");
							$('#alert_overlay').click();
						}
						else{
							$('#confirm_title').html('Delete '+name+' folder');
							$('#confirm_message').html(name+' contains '+total_files+' files.<br/> Are you sure you want to delete these files and folder?');
							$('#confirm_overlay').click();	
						}
						
					}
					break;
				case "text": break;
				default:
					$("#jstree_holder").jstree(this.id);
					break;
			}
		});

});
