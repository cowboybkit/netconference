var store_data_before_submit = null;
var folder_id = null;

var total_count_files = 0;
var uploaded_count_files = 0;
var total_size_upload = 0;
check_privacy_value = function(){
//	var privacy =  $("input[name='privacy']:checked").val();
	window.location = '/files/';
}
check_allow_space = function(){
	
	$('#table_attach_file tr').each(function(){
		total_size_upload+=	parseInt($(this).find('input[name="size_raw"]:first').val());
	});
	var return_value = true;
	$.ajax({
		  url: "/vmail/getAllowSpaceInfo?param="+total_size_upload,
		  type: 'GET',
		  async:false,
		  dataType: 'html',
		  success: function(html){
			  if(html == 'success'){
				  return_value = true;
			  }else{
				  $('#allowspace_btn').click();
				  return_value =  false;
			  }
		  }
		});
	
	return return_value;
};
check_data_before_submit = function(){
	
	var privacy =  $("input[name='privacy']:checked").val();
	if($('#password_confirm').val()!=$('#password').val()&&privacy == 'protected'){
		$('#error_pass').show();
	}else{
		$('#error_pass').hide();
	}
	
	var selected_node = $('#jstree_holder').find('.jstree-clicked').html();
   if(selected_node!=null) {
		var s_node = $('#jstree_holder').find('.jstree-clicked');
		if(s_node.length>1){
			alert('You only select one folder to upload file!!!');
			folder_id = null;
			return false;
		}else{
			folder_id = s_node.parent().attr('id');
			return true;
		}
		
	}
};

hide_element_then_upload = function(){
	$('body,html').animate({
		scrollTop: 0
	}, 1000);
	$('#form_upload_hide').hide();
	$('#notification_p').hide();
	$('.left_detail_block').hide();
	$('.line-option-divide').hide();
	$('.privacy_block').hide();
	$('.hostoption_block').hide();
	$('.layoutoption_block').hide();
	$('#extra_button').fadeOut();
	$('#view_upload_list').fadeOut();
	
	$('#multiupload_action_button').fadeOut();
	$('#option_container').removeClass('option_container');
	$('#footer_clear').hide();
	$('#progress-container').css({'margin-top':'-180px','margin-left':'-180px'});
	$('#uploading_p').show();
	$('#space_uploading').show();
	$('#please_wait').show();
	$('#continous_button').show().prop('disabled', false).addClass('continue_disable');
	$('#warning_ppt_type').fadeOut();
	$('#warning_video_type').fadeOut();
	$('#progress-container').fadeIn();
}

import_multi_submit = function(){

	if(!check_data_before_submit())
		return false;
	
	total_count_files = $('#Attachedlist').find('tr').length;
	
	general_privacy =  $("input[name='privacy']:checked").val();
	general_password = $('#password').val();
	general_tags = $('#current_tags').val();
	general_description = $('#current_description').val();
	
	var uid = $('#user_id').val();
	var cid = $('#bucket').val();
	var username = $('#username').val();
	
	hide_element_then_upload();
	
	$('#Attachedlist').find('td').each(function (){
		var file_url = encodeURIComponent($(this).find('input[name="file_url"]').val());
		var file_name = $(this).find('span:first').text();
		
		$.ajax({
			type: "POST",
			url: "/googledriver/process_import/",
			dataType: "html",
			data: 'file_url='+file_url+'&file_name='+file_name+'&privacy='+general_privacy+'&password='+general_password+'&tags='+general_tags+'&desc='+general_description+'&uid='+uid+'&cid='+cid+'&username='+username+'&folderid='+folder_id,
			success: function (data){
				console.log(data);
				uploaded_count_files ++;
				var percent = uploaded_count_files / total_count_files * 100;
				$('#progress-bar-show > div.ui-progressbar-value').width(percent + "%");
				if(uploaded_count_files == total_count_files){
					$('#continous_button').click(function(){
						window.location = '/files/?folder='+folder_id;
					});
					setTimeout(function(){
						$('#continous_button').removeClass('continue_disable').removeProp('disabled');
						$('#uploading_p').hide();
					},500);
				}
			},
		});
	});
	
	return false;
	
};

upload_multi_submit = function(){
	if($('#table_attach_file').find('tr').length<1){
		$('body,html').animate({
			scrollTop: 0
		}, 500);
		return false;
	}
	
	
	
	if(!check_data_before_submit())
		return false;
	
	if(!check_allow_space())
		return false;
	
	update_data_before_change();
	
	total_count_files = $('#table_attach_file').find('tr').length;
	store_data_before_submit = $('#table_attach_file').clone();
	
	hide_element_then_upload();
//	$('#progress-bar-show').fadeIn();
	
	
	$('#attachStartHidden').click();
	return false;
};

display_data_tr = function(row){
	if(row!=null)
	{
		var current_uuid = row.find('td:first').html();
		$('#table_attach_file tr').each(function(){
			if($(this).find('input[name="uuid"]:first').val() == current_uuid)
			{
				 $('#current_tags').val($(this).find('input[name="tags"]:first').val());
				$('#current_description').val($(this).find('input[name="description"]:first').val()) ;
				$('#current_name').val($(this).find('input[name="title"]:first').val());
			}
		});
		row.addClass('uploadfile_selected');
	}
}


remove_fileupload_selected = function(){
	var row = -1;
	$('#Attachedlist tr').each(function(){
		if($(this).hasClass('uploadfile_selected')){
			var that = $(this);
			row = that.parent().children().index(that);
			
			
			var uuid = $(this).find('td:first').html();
			$('#table_attach_file tr').each(function(){
				if($(this).find('input[name="uuid"]:first').val() == uuid)
				{
					$(this).remove();
				}
			});
			that.remove();
		}
	});
	if(row!=-1){
		var next_row = $('#Attachedlist').find('tr:eq('+row+')');
		if(next_row!=null)
		{
			display_data_tr(next_row);
		}
		else{
			next_row = $('#Attachedlist').find('tr:eq('+row-1+')');
			display_data_tr(next_row);
		}
	}
	
	
};

update_data_before_change = function(){
	$('#Attachedlist').find('tr').each(function(){
		var previous_row_selected = $(this);
		if(previous_row_selected.hasClass('uploadfile_selected')){
			var uuid = previous_row_selected.find('td:first').html();
			$('#table_attach_file tr').each(function(){
				if($(this).find('input[name="uuid"]:first').val() == uuid)
				{
					$(this).find('input[name="tags"]:first').val($('#current_tags').val());
					$(this).find('input[name="description"]:first').val($('#current_description').val());
					$(this).find('input[name="title"]:first').val($('#current_name').val());
				}
			});
		}
	});
};

ppt_container_click = function(){
	is_ppt_show = false;
	$('#warning_ppt_type').slideUp();
}
video_container_click = function(){
	is_video_show = false;
	$('#warning_video_type').slideUp();
}