initialize_tree = function(id, menu_id, contain_trash){
	var url = "/conferencing/getFullTree";
	if(!contain_trash)
		url = "/conferencing/getMoveFullTree";
	$("#"+id).jstree({ 
		// List of active plugins
		"plugins" : [ 
			/*"themes","json_data","ui","crrm","cookies","dnd","types" */
			"themes","json_data","ui","crrm","types"
		],
		
		"types" : {
		    "valid_children" : [ "root_folder"],
		    "types" : {
		        "root_folder" : {
		            "valid_children" : [ "root_folder", "normal_folder"],
		            "icon" : { "image" : "{{ MEDIA_URL }}jstree/images/root_folder.png"},
		        },
		        "normal_folder" : {
		            "valid_children" : [ "root_folder", "normal_folder" ],
		            "icon" : { "image" : "{{ MEDIA_URL }}jstree/images/normal_folder.png"},
		        },
		    }
		},
		"themes" : {
			"theme" : "default",
			"dots" : false,
		},
		"json_data" : { 
			"ajax" : {
				"url" : url,
				"async": true,	
				"cache": false,  
				} 
		},
		// Configuring the search plugin
		"metadata" : "a string, array, object, etc",
		"ui" : {
			"initially_select" : [ current_folder ]
		},
		"core" : { 
			// just open those two nodes up
			// as this is an AJAX enabled tree, both will be downloaded from the server
			"initially_open" : [ current_folder ] 
		}
	})
	.bind("create.jstree", function (e, data) {
		$.get(
			"/conferencing/processActionTree", 
			{ 
				"operation" : "create_node", 
				"id" : data.rslt.parent.attr("id").replace("node_",""), 
				"position" : data.rslt.position,
				"title" : data.rslt.name,
				"type" : data.rslt.obj.attr("rel")
			}, 
			function (r) {
				if(r.status == 200) {
					$(data.rslt.obj).attr("id", r.id);
					$("#"+id).jstree('refresh',-1);
				}
				else {
					$.jstree.rollback(data.rlbk);
				}
			}
		);
	}).bind("before.jstree", function (e, data) { 
		 /*if(data.func === "remove" && !confirm("Are you sure you want to delete?")) { 
             e.stopImmediatePropagation(); 
             return false; 
		 } */

		
		
	})
	
	.bind("remove.jstree", function (e, data) {
		data.rslt.obj.each(function () {
			$.ajax({
				async : false,
				type: 'GET',
				url: "/conferencing/processActionTree",
				data : { 
					"operation" : "remove_node", 
					"id" : this.id.replace("node_","")
				}, 
				error: function(){
					 $.jstree.rollback(data.rlbk);
				},
				success : function (r) {
					if(r.status == 200) {
						if(contain_trash){
							if(r.parent)
								window.location = '/conferencing/list/?folder='+r.parent;
							else window.location = '/conferencing/list';
						}else{
							data.inst.refresh();	
						}
						
					}else{
						$.jstree.rollback(data.rlbk);
						data.inst.refresh();
					}
				}
			});
		});
	})
	.bind("rename.jstree", function (e, data) {
		
		data.rslt.obj.each(function () {
            $.ajax({
                async: false,
                type: 'GET',
                url: "/conferencing/processActionTree",
                data: 
                {
                    "operation": "rename",
                    "id": this.id,
                    "new_name" : data.rslt.new_name 
                },
                error: function(){
                	  $.jstree.rollback(data.rlbk);
                },
                success: function (r) {
                    if (r.status == 200) {
                    	$(data.rslt.oc).attr("id", r.id);
                        if (data.rslt.cy && $(data.rslt.oc).children("UL").length) {
                            data.inst.refresh(data.inst._get_parent(data.rslt.oc));
                        }
                       
                    }
                    else {
                    	 $.jstree.rollback(data.rlbk);
                    }
                }       
            });
        });
	})
	.bind("move_node.jstree", function (e, data) {
		data.rslt.o.each(function (i) {
			$.ajax({
				async : false,
				type: 'GET',
				url: "/conferencing/processActionTree",
				data : { 
					"operation" : "move_node", 
					"id" : $(this).attr("id").replace("node_",""), 
					"ref" : data.rslt.cr === -1 ? 1 : data.rslt.np.attr("id").replace("node_",""), 
					"position" : data.rslt.cp + i,
					"title" : data.rslt.name,
					"copy" : data.rslt.cy ? 1 : 0
				},
				error: function(result){
					$.jstree.rollback(data.rlbk);
				},
				success : function (r) {
					if(r.status==200) {
						$(data.rslt.oc).attr("id",r.id);
						/*if(data.rslt.cy && $(data.rslt.oc).children("UL").length) {
							data.inst.refresh(data.inst._get_parent(data.rslt.oc));
						}*/
					}
					else {
						$.jstree.rollback(data.rlbk);
					}
					$("#analyze").click();
				}
			});
		});
	});
	$("#"+menu_id).find('input').click(function () {
		var holder = "#jstree_holder";
		if(menu_id == 'mmenu_move')
			holder = "#jstree_holder_move";
		switch(this.id) {
			case "add_default":
			case "add_folder":
				var selected_node = $(holder).find('.jstree-clicked').html();
			    if(selected_node!=null) {
					var s_node = $(holder).find('.jstree-clicked');
					if(s_node.parent().attr('rel') == 'trash_folder') {
					$('#show_warning_container').click();
					}else
						$("#"+id).jstree("create", null, "last", { "attr" : { "rel" : "normal_folder" } });
			    }
				break;
			case "search":
				$("#"+id).jstree("search", document.getElementById("text").value);
				break;
			case "text": break;
			case "remove": 
				var selected_node = $(holder).find('.jstree-clicked').html();
			    if(selected_node!=null) {
					var s_node = $(holder).find('.jstree-clicked');
					
					if(s_node.parent().attr('rel') == 'root_folder') {
						$('#alert_title').html('Warning');
						$('#alert_message').html("You can not delete this folder!!!");
						$('#alert_overlay').click();
					}
					else if(s_node.parent().attr('rel') == 'trash_folder') {
						$('#show_empty_trash').click();
					}
					else{
						$('#confirm_title').html('Delete folder');
						$('#confirm_message').html('Are you sure you want to delete folder?');
						$('#confirm_overlay').click();	
					}
				}
				break;
				
			case "rename":
				var selected_node = $(holder).find('.jstree-clicked').html();
			    if(selected_node!=null) {
					var s_node = $(holder).find('.jstree-clicked');
					if(s_node.parent().attr('rel')=='root_folder'){
						$('#alert_title').html('Warning');
						$('#alert_message').html("You can not rename this folder!!!");
						$('#alert_overlay').click();
					}
					else if(s_node.parent().attr('rel')=='trash_folder') {
						$('#show_cannot_rename').click();
					}
					else {
						$("#"+id).jstree(this.id);
					}
				}
				break;
			
			default:
				$("#"+id).jstree(this.id);
				break;
		}
	});
	
	if(id=='jstree_holder'){
		$("#"+id).delegate(".jstree-open a", "click.jstree", function (event, data) { 
			var node = $(event.target).closest("li");
			window.location = '/conferencing/list/?folder='+node.attr('id');
		});
		$("#"+id).delegate(".jstree-closed a", "click.jstree", function (event, data) { 
			var node = $(event.target).closest("li");
			window.location = '/conferencing/list/?folder='+node.attr('id');
		});
		$("#"+id).delegate(".jstree-leaf a", "click.jstree", function (event, data) { 
			var node = $(event.target).closest("li");
			window.location = '/conferencing/list/?folder='+node.attr('id');
		});
	}else if(id=='jstree_holder_move'){
		$("#"+id).bind("dblclick.jstree", function (event, data) { 
			move_files_to_folder();
			$('#btn_cancel').click();
		})
	}
};



