$(document).ready(function(){

	initialize_tree('jstree_holder','mmenu', '/googledriver/getFullTree/');
	
	
	
});

initialize_tree = function(id, menu_id, url_get_data){
	if(url_get_data==null){
		url_get_data = "/files/getFullTree";
	}
	
	$("#"+id).jstree({ 
		// List of active plugins
		"plugins" : [ 
			/*"themes","json_data","ui","crrm","cookies","dnd","types" */
			"themes","json_data","ui","crrm","types"
		],
		
		"types" : {
		    "valid_children" : [ "root_folder"],
		    "types" : {
		        "root_folder" : {
		            "valid_children" : [ "root_folder", "normal_folder"],
		            "icon" : { "image" : "{{ MEDIA_URL }}jstree/images/root_folder.png"},
		        },
		        "normal_folder" : {
		            "valid_children" : [ "root_folder", "normal_folder" ],
		            "icon" : { "image" : "{{ MEDIA_URL }}jstree/images/normal_folder.png"},
		        },
		    }
		},
		"themes" : {
			"theme" : "default",
			"dots" : false,
		},
		"json_data" : { 
			"ajax" : {
				"url" : url_get_data,
				"async": true,	
				"cache": false,  
				} 
		},
		// Configuring the search plugin
		"metadata" : "a string, array, object, etc",
		"ui" : {
			"initially_select" : [ current_folder ]
		},
		"core" : { 
			// just open those two nodes up
			// as this is an AJAX enabled tree, both will be downloaded from the server
			"initially_open" : [ current_folder ] 
		}
	})
	.bind("create.jstree", function (e, data) {
		$.get(
			"/files/processActionTree", 
			{ 
				"operation" : "create_node", 
				"id" : data.rslt.parent.attr("id").replace("node_",""), 
				"position" : data.rslt.position,
				"title" : data.rslt.name,
				"type" : data.rslt.obj.attr("rel")
			}, 
			function (r) {
				if(r.status == 200) {
					$(data.rslt.obj).attr("id", r.id);
					$("#"+id).jstree('refresh',-1);
				}else if(r.status == 403){
					$('#alert_title').html('Warning');
					$('#alert_message').html("You can not create folder here!!!");
					$('#alert_overlay').click();
					$.jstree.rollback(data.rlbk);
				}
				else {
					$.jstree.rollback(data.rlbk);
				}
			}
		);
	})/*.bind("before.jstree", function (e, data) { 
		 if(data.func === "remove" && !confirm("Are you sure you want to delete?")) { 
             e.stopImmediatePropagation(); 
             return false; 
		 }

	})
	 */
	.bind("remove.jstree", function (e, data) {
		data.rslt.obj.each(function () {
			$.ajax({
				async : false,
				type: 'GET',
				url: "/files/processActionTree",
				data : { 
					"operation" : "remove_node", 
					"id" : this.id.replace("node_","")
				}, 
				error: function(){
					 $.jstree.rollback(data.rlbk);
				},
				success : function (r) {
					/*if(r.status == 200) {
						window.location = '/files/?folder='+r.parent;
					}*/
					if(r.status == 200) {
						if(contain_trash){
							window.location = '/files/?folder='+r.parent;
						}else{
							data.inst.refresh();	
						}
					}
					else if(r.status == 403){
						$('#alert_title').html('Warning');
    					$('#alert_message').html("You can not delete this folder!!!");
    					$('#alert_overlay').click();
    					$.jstree.rollback(data.rlbk);
					} 
					else{
						$('#alert_title').html('Connection error ...');
    					$('#alert_message').html("Problem with your internet connection. Please try again!!!");
    					$('#alert_overlay').click();
						$.jstree.rollback(data.rlbk);
						//data.inst.refresh();
					}
				}
			});
		});
	})
	.bind("rename.jstree", function (e, data) {
		data.rslt.obj.each(function () {
            $.ajax({
                async: false,
                type: 'GET',
                url: "/files/processActionTree",
                data: 
                {
                    "operation": "rename",
                    "id": this.id,
                    "new_name" : data.rslt.new_name 
                },
                error: function(){
                	  $.jstree.rollback(data.rlbk);
                },
                success: function (r) {
                    if (r.status == 200) {
                    	$(data.rslt.oc).attr("id", r.id);
                        if (data.rslt.cy && $(data.rslt.oc).children("UL").length) {
                            data.inst.refresh(data.inst._get_parent(data.rslt.oc));
                        }
                    }else if(r.status == 403){
                    	$('#alert_title').html('Warning');
    					$('#alert_message').html("You can not rename this folder!!!");
    					$('#alert_overlay').click();
    					$.jstree.rollback(data.rlbk);
                    }
                    else {
                    	
                    	$.jstree.rollback(data.rlbk);
                    }
                }       
            });
        });
	})
	.bind("move_node.jstree", function (e, data) {
		data.rslt.o.each(function (i) {
			$.ajax({
				async : false,
				type: 'GET',
				url: "/files/processActionTree",
				data : { 
					"operation" : "move_node", 
					"id" : $(this).attr("id").replace("node_",""), 
					"ref" : data.rslt.cr === -1 ? 1 : data.rslt.np.attr("id").replace("node_",""), 
					"position" : data.rslt.cp + i,
					"title" : data.rslt.name,
					"copy" : data.rslt.cy ? 1 : 0
				},
				error: function(result){
					/*$('#alert_title').html('Connection error ...');
					$('#alert_message').html("Problem with your internet connection. Please try again!!!");
					$('#alert_overlay').click();*/
					$.jstree.rollback(data.rlbk);
				},
				success : function (r) {
					if(r.status==200) {
						$(data.rslt.oc).attr("id",r.id);
						/*if(data.rslt.cy && $(data.rslt.oc).children("UL").length) {
							data.inst.refresh(data.inst._get_parent(data.rslt.oc));
						}*/
					}
					else {
						$.jstree.rollback(data.rlbk);
					}
					$("#analyze").click();
				}
			});
		});
	});
	$("#"+menu_id).find('input').click(function () {
		switch(this.id) {
			case "add_default":
			case "add_folder":
				$("#"+id).jstree("create", null, "last", { "attr" : { "rel" : "normal_folder" } });
				break;
			case "search":
				$("#"+id).jstree("search", document.getElementById("text").value);
				break;
			case "text": break;
			case "remove": 
				var selected_node = $('#'+id).find('.jstree-clicked').html();
			    if(selected_node!=null) {
					var s_node = $('#'+id).find('.jstree-clicked');
					total_files = s_node.parent().data('total_files');
					name = s_node.parent().data('name');
					if(s_node.parent().attr('rel')=='root_folder'){
						console.log('enter 1');
						$('#alert_title').html('Warning');
						$('#alert_message').html("You can not delete this folder!!!");
						$('#alert_overlay').click();
					}else if(s_node.parent().attr('rel')=='trash_folder'){
						console.log('enter 2');
						$('#confirm_empty_btn').click();	
					}
					else{
						$('#confirm_title').html('Delete '+name+' folder');
						$('#confirm_message').html(name+' contains '+total_files+' files.<br/> Are you sure you want to delete these files and folder?');
						$('#confirm_overlay').click();	
					}
					
				}
			   
				
				break;
			default:
				$("#"+id).jstree(this.id);
				break;
		}
	});
	
	if(id=='jstree_holder'){
		$("#"+id).delegate(".jstree-open a", "click.jstree", function (event, data) { 
			var node = $(event.target).closest("li");
			window.location = '/googledriver/myfiles/?folder='+node.attr('id');
		});
		$("#"+id).delegate(".jstree-leaf a", "click.jstree", function (event, data) { 
			var node = $(event.target).closest("li");
			window.location = '/googledriver/myfiles/?folder='+node.attr('id');
		});
		$("#"+id).delegate(".jstree-closed a", "click.jstree", function (event, data) { 
			var node = $(event.target).closest("li");
			window.location = '/googledriver/myfiles/?folder='+node.attr('id');
		});
		
	}else if(id=='jstree_holder_move'){
		$("#"+id).bind("dblclick.jstree", function (event, data) { 
			move_files_to_folder();
			$('#btn_cancel').click();
		})
	}
};
