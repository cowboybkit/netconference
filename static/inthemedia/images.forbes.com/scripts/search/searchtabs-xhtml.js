var is_searchtabs=false;
var isHomePage = false;
var stopSlides = "";

thisURL = this.location.href;

if(typeof(pageType) == "undefined"){
	isHomePage = false; 
}
else if(pageType == 'home'){
	isHomePage = true; 
}
else if((thisURL.indexOf(".com") == -1) && (thisURL.indexOf(".net") == -1) && (thisURL.indexOf(".org") == -1)) {
	if ((thisURL.indexOf("/home") > -1) && (thisURL.indexOf("html")==-1)) { 
		isHomePage = true;
	}
}

function showPanel(tab, name,dosearch)
{   
     if(name=="panel1" && tab =='panel1' && document.SearchMain.MT.value !="" && dosearch){
	  formSubmitted();
          document.SearchMain.submit();
          return true;
     }

     if(name=="panel2" && tab =='panel2' && document.SearchHedform.tkr.value !="" && dosearch){
     	  formSubmitted();
          document.SearchHedform.submit();
          return true;
     }
}

function submitTkr(thisForm) {
	
	formSubmitted();	
	var userInput = "";
	var partnerParam = "";
	var tabSelected= "";

	userInput = escape( document[thisForm].tkr.value );
        tabSelected= "&tab=searchtabquotesdark";

	if( (userInput == null) || (userInput == "") ) {
		return false;
	} 
	else {
		if (typeof document[thisForm].partner != 'undefined' ) {
			partnerParam = "&partner=" + document[thisForm].partner.value;
		}

        	var lookups = "";
                if( (userInput.length == 5) && ( (userInput.charAt(4) == "X") || (userInput.charAt(4) == "x") ) ) {
                      	lookups = "http://www.forbes.com/funds/Tearsheet.jhtml?tkr=" + userInput.toUpperCase() + "&partner=" + partnerParam + tabSelected;
                }
            	else {
                       	if(userInput.indexOf("&")!=-1 || userInput.indexOf("%26")!=-1){
                               	lookups = "http://finapps.forbes.com/finapps/jsp/finance/compinfo/SymbolLookupFormSubmit.do?companyName=" + userInput + "&type=E&countryCode=US" + partnerParam + tabSelected;
                       	}else
                        	lookups = "http://finapps.forbes.com/finapps/jsp/finance/compinfo/CIAtAGlance.jsp?tkr=" + userInput + partnerParam + tabSelected;
                }	
                // Do the lookup
                window.location = lookups;
                // Form should not submit since we are taking care of everything in the javascript
                return false;
	}
}

function checkKeywordLength()
{
	var keywordInput = escape(document.keywordLookup.passName.value);
	if (keywordInput==null || keywordInput.length < 2) {
		var msg = "Please enter at least two characters and try your search again.";
		alert (msg);
		return false;
	}

	return true;
}


function canBePremium(aString){
	if(aString.indexOf('magazine') > -1)
	return true;
	else 
	return false;
}

function premiumInfo(aForm){
	var aSelect = aForm.pub;
	var str = aSelect.options[aSelect.selectedIndex].value;
	var cbp = canBePremium(str);

	if(!cbp){
		//alert("selected pub is "+str+"cannot be premium");
		aForm.premium.checked = true;
	}
}

function clearInputField(aField){

	if(aField.value == "mm/dd/yyyy")
	aField.value = "";
}

function checkTextForWebSearch(){
	if(document.advancedsearch.contentType.options[document.advancedsearch.contentType.selectedIndex].value == "web" &&
			document.advancedsearch.MT.value == ''){

		alert("'With the following text' field cannot be empty for web search!");
		document.advancedsearch.MT.focus();
		document.advancedsearch.MT.select();
		return false;
	}

	return true;
}

function checkDates(aForm){
	
	//var webOK = checkTextForWebSearch();
	//if(!webOK)
	//  return false;

	premiumInfo(aForm);

	if((aForm.author.value != "" || aForm.tickers.value != "" || aForm.contentType.value != "all" ||  aForm.pub.value != "forbes.com,magazine,fyi,best" || aForm.storyType.value != "all") && aForm.MT.value=="") {
		aForm.MT.value = "*";
	}

	if(aForm.pubDateStart.value != "mm/dd/yyyy" && aForm.pubDateStart.value != "")  {
		
		if( (aForm.pubDateEnd.value != "mm/dd/yyyy"  && aForm.pubDateEnd.value != "") && aForm.MT.value == "") {

			aForm.MT.value = "*";
		}
	}    
	
	
	
	if(aForm.MT.value==""){
		aForm.MT.select();
		aForm.MT.focus();
		return false;
	}

	var startDate = aForm.pubDateStart.value;
	var endDate = aForm.pubDateEnd.value;

	if(isValidDate(startDate) && isValidDate(endDate))
	return true;
	else
	return false; 	         
}

function isValidDate(dateStr) {
	// MM/DD/YY   MM/DD/YYYY   MM-DD-YY   MM-DD-YYYY //
	if(dateStr == "" || dateStr == "mm/dd/yyyy")
	return true;

	var datePat = /^(\d{1,2})(\/)(\d{1,2})\2(\d{4})$/; // requires 4 digit year, for MM/DD/YYYY

	var matchArray = dateStr.match(datePat); // is the format ok?

	if (matchArray == null) {
		alert(dateStr + " Date is not in a valid format.")
		return false;
	}

	month = matchArray[1]; // parse date into variables
	day = matchArray[3];  
	year = matchArray[4];
	if (month < 1 || month > 12) { // check month range
		alert("Month must be between 1 and 12.");
		return false;
	}

	if (day < 1 || day > 31) {
		alert("Day must be between 1 and 31.");
		return false;
	}

	if (year < 1900 || year > 2010) {
		alert("Year must be between 1900 and 2010");
		return false;
	}

	if ((month==4 || month==6 || month==9 || month==11) && day==31) {
		alert("Month "+month+" doesn't have 31 days!")
		return false;
	}

	if (month == 2) { // check for february 29th
		var isleap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
		if (day>29 || (day==29 && !isleap)) {
			alert("February " + year + " doesn't have " + day + " days!");
			return false;
		}

	}

	return true;
}

