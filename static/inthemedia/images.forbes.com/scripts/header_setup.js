//Tania - 10/27/2006
//David Dunlop - 10/27/2006
//Robert Lin - 01/19/2007
//Danny H - 03/13/2007
//Alex S - 03/19/2007
//Dave Krunal - 05/11/2007
//Bradford Campeau-Laurion - 08/31/2007
//Shabana Abrez - 12/16/2009
//Alexander Shnayderman - 11/26/2007
//Andrew Dokko - 01/22/2008
//Soby Surendran 02/06/2008
//Amita Singh - 08/22/2008
//Amit Sazawal - 09/23/2008
//Sheeba B - 11/25/08
//Emil Isaakov - 02/11/2009
//Sheeba B - 03/20/09
//Madhavi Sawant - 08/13/2009
//Ryan Huang - 05/03/2010

var scriptChainNode = function(src, condition) {
    var src = src;
    var condition = condition;
    this.getSrc = function() { return src; };
    this.getCondition = function() { return condition; };
}

function loadScriptsForSearch() {

    var chain = new Array();

    chain.push(new scriptChainNode(
        'http://images.forbes.com/scripts/jquery/jquery.js',
        function() { return !window.jQuery; }
    ));
    chain.push(new scriptChainNode(
        'http://images.forbes.com/scripts/jquery/jquery.dimensions.js',
        function() { return !window.jQuery || !window.jQuery.dimensions; }
    ));
    chain.push(new scriptChainNode(
        'http://images.forbes.com/scripts/jquery/jquery.suggest.js',
        function() { return !window.jQuery == 'undefined' || !window.jQuery.suggest; }
    ));
    loadScriptChain(chain);
}

function loadScriptChain(chain, callback) {
    if(chain.length > 0) {
        var chainNode = chain.shift();
        var condition = chainNode.getCondition();
        if(!condition()) // if script is already loaded - recurse
            loadScriptChain(chain, callback);
        else { // otherwise, load the script and recurse
            var src = chainNode.getSrc();
            var head = document.getElementsByTagName('head')[0];
            var script = document.createElement('script');
            script.type = 'text/javascript';
            script.onreadystatechange = function () {
                if(this.readyState=='loaded' || this.readyState=='complete' ) loadScriptChain(chain, callback);
            }
            script.onload= function () { loadScriptChain(chain, callback); }
            script.src = src;
            head.appendChild(script);
            // safari 2.0 does not support .onreadystatechange or .onload, so we need to manually recurse
            if(navigator.userAgent.toLowerCase().match(/safari/) && navigator.userAgent.match(/.*WebKit\/[400-499].*/))
                loadScriptChain(chain, callback);
        }
    }
    else {
        if(callback)
            callback();
    }
}

var typeAheadFlag =false;

function callTypeAheadJs(){
        var script_searchbox = document.createElement('script');
        script_searchbox.type = 'text/javascript';
        script_searchbox.onreadystatechange = function () {
                if(this.readyState=='loaded' || this.readyState=='complete' ) loadScriptsForSearch(); 
            }
         script_searchbox.onload= function () { loadScriptsForSearch(); }
         script_searchbox.src ="http://images.forbes.com/scripts/search/typeahead.js";
        document.body.appendChild(script_searchbox);
}

if(!document.referrer.match(/http:\/\/[^\/]*forbes.com/i)) {
	document.cookie = "wg_originalReferrer=" + document.referrer + ";path=/;domain=.forbes.com";
	document.cookie = "wg_originalDomain=;domain=.forbes.com; path=/;expires=Thu, 01-Jan-70 00:00:01 GMT";
}

document.write('<script src="http://images.forbes.com/scripts/util/CookieCutter.js" type="text/javascript"></script>');
var cssPre = "style_";
//icon
document.write('<link rel="SHORTCUT ICON" href="http://images.forbes.com/icon/favicon.ico">');

var slideshowExprForCenter =/(-slide|_slide).?\d*_?\d*_?\d*\.html/;
var slideshowExprSpecialForCenter = /_slideshow.?\d*_?\d*_?\d*\.html/;
var pageTypeForSlide = false;
if ((this.location.href).match(slideshowExprForCenter) || (this.location.href).match(slideshowExprSpecialForCenter)) {
pageTypeForSlide = true;
}

var storyExprForCenter = /\d{4}\/\d{2}\/\d{2}\//;
var pageTypeForStory = false;
if ((this.location.href).match(storyExprForCenter) ) {
pageTypeForStory = true;
}


var channelArr = new Array('home','lists','business','technology','markets','personalFinance','entrepreneurs','work','lifestyle','opinions','newsletters');
var longChannelArr = new Array('Home','Lists','Business','Tech','Markets','Personal Finance','Entrepreneurs','Leadership','ForbesLife','Opinions','Newsletters');

var searchTab = 0;
var noSearch = 0;

if(typeof hpType != "undefined") {
	var hpCookie = "home_usa";
	if(hpType=="europe") hpCookie = "home_europe";
	if(hpType=="asia") hpCookie = "home_asia";

	var hpCookieExpire = new Date();
	hpCookieExpire.setTime( hpCookieExpire.getTime() + (2*24*60*60*1000) );
	document.cookie='forbes_international='+hpCookie+';expires='+hpCookieExpire.toGMTString()+';path=/;domain=.forbes.com';
}

//Start Highlight home link
function findCookie(NameOfCookie) {  
	if( document.cookie.length > 0 ) { 
		begin = document.cookie.indexOf( NameOfCookie+"=" ); 
		if( begin != -1 ) { 
			begin += NameOfCookie.length + 1; 
			end = document.cookie.indexOf( ";", begin );
			if( end == -1 ) end = document.cookie.length;
			return unescape( document.cookie.substring( begin, end ));
		} 
	}
	return null; 
}
InternationalCookie = findCookie('forbes_international');

//Channel URL
if (!InternationalCookie){var channelURLArr = new Array('/','/lists/','/business/','/technology/','/markets/','/finance/','/entrepreneurs/','/leadership/','/lifestyle/','/opinions/','/newsletters/');
	}
else if (InternationalCookie == "home_usa"){	var channelURLArr = new Array('/home_usa','/lists/','/business/','/technology/','/markets/','/finance/','/entrepreneurs/','/leadership/','/lifestyle/','/opinions/','/newsletters/');
	}
else if (InternationalCookie == "home_europe"){ var channelURLArr = new Array('/home_europe','/lists/','/business/','/technology/','/markets/','/finance/','/entrepreneurs/','/leadership/','/lifestyle/','/opinions/','/newsletters/');
	}
else if (InternationalCookie == "home_asia"){	var channelURLArr = new Array('/home_asia','/lists/','/business/','/technology/','/markets/','/finance/','/entrepreneurs/','/leadership/','/lifestyle/','/opinions/','/newsletters/');
	}

function lightHomepagelink(page){
	var contain = ['<td id="intlhomepagelink" class="intlhomepagelink" nowrap>', "</td>"],
	separator = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;",
	links = {
		home_usa: {
			text: "U.S.",
			url: "home_usa/"
		},
		home_europe: {
			text: "EUROPE",
			url: "home_europe/"
		},
		home_asia: {
			text: "ASIA",
			url: "home_asia/"
		}
	},
	linkshtml = [];
	for (var link in links) {
		if (InternationalCookie == link)
			linkshtml.push('<font color="#000000">' + links[link].text + "</font>")
		else
			linkshtml.push('<a href="http://www.forbes.com/' + links[link].url + '" class="intlhomepagelink">' + links[link].text + "</a>")
	}
	contain.splice(1, 0, linkshtml.join(separator));
	document.write(contain.join(""))
}

//Set Forbes Logo URL
function setLogoURL(page){	
	if (!InternationalCookie) { 
		if (page == "isNotSlide")
			document.write('<a href="http://www.forbes.com"><img id="forbesLogo" src="http://images.forbes.com/media/assets/forbes_home_logo.gif" vspace="0" hspace="0" width="150" height="49" border="0"><\/a>');
		else
			document.write('<a href="http://www.forbes.com"><img id="forbesLogo" src="http://images.forbes.com/media/assets/forbes_home_logo.gif" vspace="3" width="150" height="49" border="0" hspace="30"><\/a>');	
	}
	else if (InternationalCookie == "home_usa"){
		if (page == "isNotSlide")
			document.write('<a href="http://www.forbes.com/home_usa/"><img id="forbesLogo" src="http://images.forbes.com/media/assets/forbes_home_logo.gif" vspace="0" hspace="0" width="150" height="49" border="0"><\/a>');
		else		
			document.write('<a href="http://www.forbes.com/home_usa"><img id="forbesLogo" src="http://images.forbes.com/media/assets/forbes_home_logo.gif" vspace="3" width="150" height="49" border="0" hspace="30"><\/a>');
	}
	else if (InternationalCookie == "home_europe"){
		if (page == "isNotSlide")
			document.write('<a href="http://www.forbes.com/home_europe/"><img id="forbesLogo" src="http://images.forbes.com/media/assets/forbes_home_logo.gif" vspace="0" hspace="0" width="150" height="49" border="0"><\/a>');
		else
			document.write('<a href="http://www.forbes.com/home_europe"><img id="forbesLogo" src="http://images.forbes.com/media/assets/forbes_home_logo.gif" vspace="3" width="150" height="49" border="0" hspace="30"><\/a>');
	}
	else if (InternationalCookie == "home_asia"){
		if (page == "isNotSlide")
			document.write('<a href="http://www.forbes.com/home_asia/"><img id="forbesLogo" src="http://images.forbes.com/media/assets/forbes_home_logo.gif" vspace="0" hspace="0" width="150" height="49" border="0"><\/a>');
		else
			document.write('<a href="http://www.forbes.com/home_asia"><img id="forbesLogo" src="http://images.forbes.com/media/assets/forbes_home_logo.gif" vspace="3" width="150" height="49" border="0" hspace="30"><\/a>');	
	}
	else
		if (page == "isNotSlide")
			document.write('<a href="http://www.forbes.com"><img id="forbesLogo" src="http://images.forbes.com/media/assets/forbes_home_logo.gif" vspace="0" hspace="0" width="150" height="49" border="0"><\/a>');
		else
			document.write('<a href="http://www.forbes.com"><img id="forbesLogo" src="http://images.forbes.com/media/assets/forbes_home_logo.gif" vspace="3" width="150" height="49" border="0" hspace="30"><\/a>');	
}
//End Highlight home link

//pagetype
thisURL = this.location.href;
if ( typeof displayedSection != "undefined" && displayedSection == "searchhome" && ( thisURL.indexOf( "/find" ) != -1 || thisURL.indexOf( "/web" ) != - 1 || thisURL.indexOf( "/blogs" ) != -1 )) {
	 pageType = "search";
}
else if (thisURL.indexOf('beta.forbes.com')>-1) {
	if (thisURL.indexOf('http://')>-1) thisPreURL = 'http://beta.forbes.com';
	else  thisPreURL = 'beta.forbes.com';
	thisURL = thisURL.substr(thisPreURL.length);
}
else if (thisURL.indexOf('www.forbes.com')>-1) {
	if (thisURL.indexOf('http://')>-1) thisPreURL = 'http://www.forbes.com';
	else  thisPreURL = 'www.forbes.com';
	thisURL = thisURL.substr(thisPreURL.length);
}
else if (thisURL.indexOf('clipmarks.forbes.com')>-1) {
	if (thisURL.indexOf('http://')>-1) thisPreURL = 'http://clipmarks.forbes.com';
	else  thisPreURL = 'clipmarks.forbes.com';
	thisURL = thisURL.substr(thisPreURL.length);
	pageType = "clipmarks";
}
else if (thisURL.indexOf('qa.forbes.com/cms/template')>-1) {
	if (thisURL.indexOf('http://')>-1) thisPreURL = 'http://qa.forbes.com/cms/template';
	else  thisPreURL = 'qa.forbes.com/cms/template';
	thisURL = thisURL.substr(thisPreURL.length);
}
else if (thisURL.indexOf('qa.forbes.com')>-1) {
	if (thisURL.indexOf('http://')>-1) thisPreURL = 'http://qa.forbes.com';
	else  thisPreURL = 'qa.forbes.com';
	thisURL = thisURL.substr(thisPreURL.length);
}
else if (thisURL.indexOf('members.forbes.com')>-1) {
         if (thisURL.indexOf('http://')>-1) thisPreURL = 'http://members.forbes.com';
         else  thisPreURL = 'members.forbes.com';
         thisURL = thisURL.substr(thisPreURL.length);
}
if (thisURL.indexOf('index.html')>-1) thisURL = thisURL.substr(0,thisURL.indexOf('index.html'));
if (thisURL.indexOf('index.shtml')>-1) thisURL = thisURL.substr(0,thisURL.indexOf('index.shtml'));
if (thisURL.indexOf('index.jhtml')>-1) thisURL = thisURL.substr(0,thisURL.indexOf('index.jhtml'));
if (thisURL.indexOf('index.jsp')>-1) thisURL = thisURL.substr(0,thisURL.indexOf('index.jsp'));
if (thisURL.indexOf('?')>-1) thisURL = thisURL.substr(0,thisURL.indexOf('?'));
var storyExpr = /\d{4}\/\d{2}\/\d{2}\//;
var magExpr = /(forbes|forbesglobal|global|asap|best|fyi)\/\d{4}\/\d{4}\//;
var listExpr = /lists\/\d{4}\/\d+\//;
var sectionExpr = /\/[a-z]+\/[a-z]+/;
var channelExpr = /\/[a-z]+/;

if (document.referrer.indexOf("/cionetwork") != -1 && (thisURL.match(storyExpr) || thisURL.match(magExpr))) {

        OAS_sitepage = "forbes.com/specialslot/cionetwork";

} else if (document.referrer.indexOf("/businessvisionaries") != -1 && (thisURL.match(storyExpr) || thisURL.match(magExpr))) {

        OAS_sitepage = "forbes.com/specialslot/bizviz";

} else if (document.referrer.indexOf("/promising-companies") != -1 && (thisURL.match(storyExpr) || thisURL.match(magExpr))) {

        OAS_sitepage = "forbes.com/specialslot/promising-companies-09";

} else if (document.referrer.indexOf("/breakthroughs") != -1 && (thisURL.match(storyExpr) || thisURL.match(magExpr))) {

        OAS_sitepage = "forbes.com/technology/breakthroughs/index.jhtml";

} else if (((this.location.href.indexOf("Financial+Adviser+Network") != -1) || document.referrer.indexOf("/advisernetwork") != -1) && (thisURL.match(storyExpr) || thisURL.match(magExpr))) {

    OAS_sitepage = "forbes.com/specialslot/financialaunch08";

} else if (this.location.href.indexOf("forbes-woman-power-women") != -1 && (thisURL.match(storyExpr) || thisURL.match(magExpr))) {
	OAS_sitepage = "forbes.com/forbeswoman/power-women/story";
} else if ((this.location.href.indexOf("forbes-woman-leadership") != -1 || this.location.href.indexOf("forbes-woman-careers") != -1) && (thisURL.match(storyExpr) || thisURL.match(magExpr))) {
	OAS_sitepage = "forbes.com/forbeswoman/leadership/story";
} else if (this.location.href.indexOf("forbes-woman-entrepreneurs") != -1 && (thisURL.match(storyExpr) || thisURL.match(magExpr))) {
	OAS_sitepage = "forbes.com/forbeswoman/entrepreneurs/story";
} else if (this.location.href.indexOf("forbes-woman-style") != -1 && this.location.href.indexOf("2009/07/15/engagement-weddings-diamonds-forbes-woman-style-retail_slide.html") == -1 && this.location.href.indexOf("2009/07/15/fashion-designer-french-forbes-woman-style_slide.html") == -1 && (thisURL.match(storyExpr) || thisURL.match(magExpr))) {
	OAS_sitepage = "forbes.com/forbeswoman/style/story";
} else if ((this.location.href.indexOf("forbes-woman-health") != -1 || this.location.href.indexOf("forbes-woman-well-being") != -1) && (thisURL.match(storyExpr) || thisURL.match(magExpr))) {
	OAS_sitepage = "forbes.com/forbeswoman/well-being/story";
} else if (this.location.href.indexOf("forbes-woman-net-worth") != -1 && (thisURL.match(storyExpr) || thisURL.match(magExpr))) {
	OAS_sitepage = "forbes.com/forbeswoman/net-worth/story";
} else if (this.location.href.indexOf("forbes-woman-time") != -1 && (thisURL.match(storyExpr) || thisURL.match(magExpr))) {
	OAS_sitepage = "forbes.com/forbeswoman/time/story";
} else if (document.referrer.indexOf("/business-aviation") != -1 && (thisURL.match(storyExpr) || thisURL.match(magExpr))) {
        OAS_sitepage = "forbes.com/business/aviation/story";
}

// 2008-06-26 - Added to force top tech story on tech channel page to have SAP ads - NO LONGER NEEDED
// document.write("<scr" + "ipt language='JavaScri" + "pt' src='http://images.forbes.com/scripts/top_tech_story.js'></scr" + "ipt>");

//2010-02-11 - Added to force topstories on the Fact And Comment page to have the proper sitepage for ad targeting
document.write("<scr" + "ipt language='JavaScri" + "pt' src='http://images.forbes.com/scripts/topFCstories.js'></scr" + "ipt>");

// 2009-02-06 - Added to force topstories on the homepage to have the proper sitepage for ad targeting
document.write("<scr" + "ipt language='JavaScri" + "pt' src='http://images.forbes.com/scripts/topstories.js'></scr" + "ipt>");

// 2010-05-03 - Added to force topratedstories on the homepage to have the proper sitepage for ad targeting
document.write("<scr" + "ipt language='JavaScri" + "pt' src='http://images.forbes.com/scripts/topRatedStories.js'></scr" + "ipt>");

if((thisURL.indexOf(".com") == -1) && (thisURL.indexOf(".net") == -1) && (thisURL.indexOf(".org") == -1)) {
	if ((thisURL.indexOf("/home") > -1) && (thisURL.indexOf("html")==-1)) pageType = "home";
	
	if((typeof pageType == "undefined") || (!pageType)) {
		if (thisURL.match(storyExpr)) pageType = "story";
		else if (thisURL.match(magExpr)) pageType = "magstory";
		else if (thisURL.indexOf("_land.html")!=-1) pageType = "lander";
		else if (thisURL.indexOf("/richlist")!=-1) pageType = "lander";
		else if (thisURL.indexOf("/rich400")!=-1) pageType = "lander";
		else if (thisURL.indexOf("/400richest")!=-1) pageType = "lander";
		else if (thisURL.indexOf("/worldsrichest")!=-1) pageType = "lander";
		else if (thisURL.indexOf("thought-leaders")!=-1 && !thisURL.match(magExpr) && !thisURL.match(storyExpr) && !thisURL.match(listExpr)) { pageType = "thought-leaders"; }
		else if (thisURL.match(listExpr)) pageType = "list";
		else if (thisURL.match(sectionExpr) && thisURL.indexOf("html")==-1) pageType = "section";
		else if (thisURL.match(channelExpr) && thisURL.indexOf("html")==-1) {
			if (thisURL.lastIndexOf("/")==thisURL.length-1) thisURL = thisURL.substr(0,thisURL.length-2);
			thisURL = thisURL.substr(0,thisURL.lastIndexOf("/"));
			if (thisURL.length == 0) pageType = "channel";
			else pageType = "generic";
		}
		else  if (thisURL.length<2) pageType = "home";
		else if (typeof wincol_generic_layout != "undefined") {
			if (wincol_generic_layout == "window") pageType = "generic window";
			else if (wincol_generic_layout == "column") pageType = "generic column";
			else pageType = "generic";
		}
		else pageType = "generic";
	}
} else if (typeof wincol_generic_layout != "undefined") {
	if (wincol_generic_layout == "window") pageType = "generic window";
	else if (wincol_generic_layout == "column") pageType = "generic column";
	else pageType = "generic";
} else pageType = "generic";

// css for centering
if(typeof fdc_nocss == 'undefined') {
	if (navigator.appName.indexOf("Netscape") != -1) { 
		
		if ((typeof pageType != 'undefined' && pageType == "home") || typeof fdc_center != "undefined"){
			document.write ('<LINK REL="STYLESHEET" TYPE="text/css" HREF="http://images.forbes.com/css/'+ cssPre + 'ns_home.css">');
		} else {
			document.write ('<LINK REL="STYLESHEET" TYPE="text/css" HREF="http://images.forbes.com/css/'+ cssPre + 'ns.css">');
		}
	} else {
			if((typeof pageType != 'undefined' && pageType == "home") || typeof fdc_center != "undefined") {
			document.write ('<LINK REL="STYLESHEET" TYPE="text/css" HREF="http://images.forbes.com/css/'+ cssPre + 'ie_home.css">');
		} else {
			document.write ('<LINK REL="STYLESHEET" TYPE="text/css" HREF="http://images.forbes.com/css/'+ cssPre + 'ie.css">');
		}
		
	}
}

var centBan = "CenterBanner";

//l0g1kh4k t0 0v3rr1d3 g3n3r1c
if ( typeof displayedSection != "undefined" && displayedSection == "searchhome" && ( thisURL.indexOf( "/find" ) != -1 || thisURL.indexOf( "/web" ) != -1 || thisURL.indexOf( "/blogs" ) != -1 )) {
         pageType = "search";
}

if (pageType == "home") {
	cssPre = "";
	centBan = "CenterBannerHome";
	OAS_listpos = "AdController,BigBanner,Block,x5,LeftBottom,LeftBottom2,RightUndQuotes,x88,x89,AutosModule,x79,x83,SponsorLogo,x110,MyForbesHeader,AttacheButton,x103,x1,x100";
} else if (pageType == "search") {
	OAS_listpos = "AdController,BigBanner,Block,AlertsLogo,AutosModule,x98,x99,x102,LeftBottom,LeftBottom2,LeftBottom3,LeftBottom4,RightUndQuotes,StoryLogo,x88,x89,x5,x85,x83,x113,x112,Loge,x110,MyForbesHeader,AttacheButton,x1,x100";
} else if (pageType == "channel") {
	//this actually covers channel AND section setup (some sections follow channel url structure), also includes rightmiddle for nonresolving friendlies
	var channelListposWincol = "Block";
	if (this.location.href.indexOf('http://orgchart.forbes.com') != -1 || this.location.href.indexOf('http://bfn.forbes.com') != -1 || this.location.href.indexOf('http://www.forbes.com/static_html/econ_calendars/economic_calendar.html') != -1 || this.location.href.indexOf('http://www.forbes.com/businessvisionaries') != -1 || this.location.href.indexOf('http://www.forbes.com/fdc/sitemap.html') != -1 || this.location.href.indexOf('http://www.forbes.com/fdc/help.html') != -1 || this.location.href.indexOf('http://www.forbes.com/fdc/contact.html') != -1 || this.location.href.indexOf('http://www.forbes.com/magazines') != -1 || this.location.href.indexOf('http://www.forbes.com/mobile') != -1 || this.location.href.indexOf('http://www.forbes.com/fdc/rss.html') != -1 || this.location.href.indexOf('http://www.forbes.com/fdc/reprints/Reprints.jhtml') != -1 || this.location.href.indexOf('http://www.forbes.com/fdc/subservices.html') != -1 || (this.location.href.indexOf('http://www.forbes.com/forbes') != -1 && this.location.href.indexOf('forbeswoman') == -1 && this.location.href.indexOf('forbesinvestorteam') == -1) || this.location.href.indexOf('http://www.forbes.com/forbesglobal') != -1 || this.location.href.indexOf('http://www.forbes.com/fdc/privacy.html') != -1 || this.location.href.indexOf('http://www.forbes.com/fdc/terms.html') != -1) { channelListposWincol = "RightMiddle"; }
	OAS_listpos = "AdController,BigBanner," + channelListposWincol + ",AlertsLogo,AutosModule,x98,x99,x102,LeftBottom,LeftBottom2,RightUndQuotes,StoryLogo,SponsorLogo,x88,x5,x85,x83,x113,x68,x112,Loge,x110,MyForbesHeader,AttacheButton,x109,x103,x1,x62,x100";
} else if (pageType == "section") {
    OAS_listpos = "AdController,BigBanner,Block,AlertsLogo,AutosModule,x98,x99,x102,LeftBottom,LeftBottom2,LeftBottom3,LeftBottom4,RightUndQuotes,StoryLogo,SponsorLogo,x88,x5,x85,x83,x113,x112,Loge,x110,MyForbesHeader,AttacheButton,x103,x1,x62,x100";
} else if (pageType == "list") {

        if ((typeof sponsor=="undefined") || (sponsor.length==0)) {
            sponsor = "";
        } else {
            sponsor = "/" + sponsor;
        }
if (typeof listId == "undefined") listId = thisURL.substr(12,3);
if(listId.indexOf('/')>-1) listId = listId.substr(0,2);
	
        OAS_sitepage = "forbes.com/lists/ListID" + listId + "/results" + sponsor;

                if ((thisURL.indexOf("lists/2008/86") != -1 && thisURL.indexOf("The-Philippines-40") == -1) || (thisURL.indexOf("lists/2008/53") != -1 && thisURL.indexOf("The-Celebrity-100") == -1) || (thisURL.indexOf("lists/2008/74") != -1 && thisURL.indexOf("The-400-Richest-Chinese") == -1) || (thisURL.indexOf("lists/2008/56") != -1 && thisURL.indexOf("Asian-Altruists") == -1) || (thisURL.indexOf("lists/2008/78") != -1 && thisURL.indexOf("Australia-and-New-Zealands-40-Richest") == -1) || (thisURL.indexOf("lists/2008/87") != -1 && thisURL.indexOf("Taiwans-Richest") == -1) || (thisURL.indexOf("lists/2008/82") != -1 && thisURL.indexOf("Hong-Kongs-40-Richest") == -1) || (thisURL.indexOf("lists/2007/77") != -1 && thisURL.indexOf("Indias-Richest") == -1) || (thisURL.indexOf("lists/2008/77") != -1 && thisURL.indexOf("Indias-Richest") == -1) || (thisURL.indexOf("lists/2008/73") != -1 && thisURL.indexOf("Japans-Richest") == -1) || (thisURL.indexOf("lists/2008/83") != -1 && thisURL.indexOf("Koreas-Richest") == -1) || (thisURL.indexOf("lists/2008/84") != -1 && thisURL.indexOf("Malaysias-Richest") == -1) || (thisURL.indexOf("lists/2008/6") != -1 && thisURL.indexOf("Best-Countries-for-Business") == -1)) {

                OAS_listpos = "AdController,BigBanner,Block,StoryLogo,AutosModule,x110,MyForbesHeader,AttacheButton,x1,x100";
        } else {
                OAS_listpos = "AdController,BigBanner,RightMiddle,StoryLogo,x110,MyForbesHeader,AttacheButton,x1,x100";
        }


} else if (pageType == "lander") {
        //lander_setup.js

                OAS_listpos = "AdController,BigBanner,RightMiddle,x5,StoryLogo,x110,MyForbesHeader,AttacheButton,x1,x100";
	//end lander_setup.js
} else if ((pageType == "magstory") || (pageType == "story")) {
	// wincol.js
	var fdcQuotesURL = "/cms/components/wincol/quotes_js.jhtml";
	var fdcWincolThreshhold;
	var fdcWincolResult;
	var fdcWincolStyle;
		//  Set this to the % of frequency templates
		//  will be rendered as columns.
		//  eg. 30 = 30% column (and 70% windows).
	var fdcWincolDefault = 5;
	var OAS_listpos = "";
	var fdcDisableCallbacks = 1;

	fdcWincolDecideTargetting();

} else if(pageType == "generic window") {
	OAS_listpos = "AdController,Block,BigBanner,AutosModule,x83,x110,MyForbesHeader,AttacheButton,x1,x100";
} else if(pageType == "generic column") {
	OAS_listpos = "AdController,RightMiddle,BigBanner,x106,MyForbesHeader,AttacheButton,x1,x100";
} else if(pageType == "clipmarks") {
	OAS_listpos = "AdController,BigBanner,MyForbesHeader,AttacheButton,x1,x100";
} else if(pageType == "thought-leaders") {
	OAS_listpos = "AdController,BigBanner,Block,RightMiddle,SponsorLogo,x5";
}

function fdcWincolComputeListpos(template) {
	if(template) {
		if(template == 'wide') {
			OAS_listpos = "AdController,BigBanner,RightMiddle,x5,x110,MyForbesHeader,"+
				"AttacheButton,Loge,x89,StoryLogo,AlertsLogo,LeftBottom,LeftBottom2,LeftBottom3,LeftBottom4,"+
				"x85,x91,x92,x1,x100";
        	} else if(template == 'xWide') {
        	        OAS_listpos = "AdController,BigBanner,x5,x110,MyForbesHeader,StoryLogo,AttacheButton,Loge,x89,x1,x100";
        	} else {
			var largeLeft = "Block";
        	        if(fdcWincolStyle == "column") largeLeft = "RightMiddle";
        	        OAS_listpos = "AdController,BigBanner,"+largeLeft+",x5,x110,MyForbesHeader,StoryLogo,AttacheButton,"+
				"x88,x89,AlertsLogo,LeftBottom,LeftBottom2,LeftBottom3,LeftBottom4,Loge,AutosModule,x83,x85,x91,x92,x1,x100,x70";
        	}
	} else {
		if ((pageType == "magstory") || (pageType == "story")) {
			url = '' + window.location;
			if(fdcWincolStyle=="window") {
				// DO NOT CHANGE WITHOUT APPROVAL
				OAS_listpos = "AdController,Block,BigBanner,x5,LeftBottom,LeftBottom2,x102,StoryLogo,x88,x89,LeftBottom3,LeftBottom4,AlertsLogo,AutosModule,StoryBotLogo,x83,Loge,x85,x110,MyForbesHeader,AttacheButton,x91,x92,x1,x100";
			}
			else {
				// DO NOT CHANGE WITHOUT APPROVAL
				OAS_listpos = "AdController,RightMiddle,BigBanner,x5,LeftBottom,LeftBottom2,x102,x106,StoryLogo,x88,x89,LeftBottom3,LeftBottom4,AlertsLogo,StoryBotLogo,x83,Loge,x85,x110,MyForbesHeader,AttacheButton,x91,x92,x1,x100";
			}
		}
	}
}
function fdcWincolAlert(){
	testStr = "";
	if(typeof pageType != "undefined") testStr = testStr + "pagetype: " + pageType  + "\n";
	if(typeof OAS_sitepage != "undefined") testStr = testStr + "OAS_sitepage: " + OAS_sitepage  + "\n";
	if(typeof OAS_listpos != "undefined") testStr = testStr + "OAS_listpos: " + OAS_listpos  + "\n";
	if(typeof OAS_query != "undefined") testStr = testStr + "OAS_query: " + OAS_query  + "\n";
	if(typeof fdcWincolThreshhold != "undefined") testStr = testStr + "minimum for window: " + fdcWincolThreshhold  + "\n";
	if(typeof fdcWincolResult!= "undefined") testStr = testStr + "result: " + fdcWincolResult  + "\n";
	if(typeof fdcWincolStyle != "undefined") testStr = testStr + "style: " + fdcWincolStyle  + "\n";
	alert(testStr);
}

	function fdcWincolDecideTargetting() {
        var url = window.location + "";
        // use URL for OAS_sitepage value if URL matches following
        if ( url.indexOf("/topstories/") != -1 ) { fdcWincolComputeSitepage(); }
        else { var OAS_sitepage = ""; }
	}
	function fdcWincolComputeSitepage(path){
		url = '' + window.location;
		start = url.indexOf('//') + 2;
		start = url.indexOf('/',start) + 1;
		end = url.indexOf('?'); if(end==-1){end=url.length}
		if(typeof path != "undefined") { OAS_sitepage = 'forbes.com/' + path + url.substring(start, end); }
		else { OAS_sitepage = 'forbes.com/' + url.substring(start, end); }
	}
	function fdcWincolComputeStyle() {
		var url = window.location + "";
		 // 100 = all columns
		// 0 = all windows
		if (url.indexOf("/column/") != -1 )             { fdcWincolThreshhold = 100; }
		else if (url.indexOf("/window/") != -1 )        { fdcWincolThreshhold = 0; }
		else if (typeof storyTemplateType != 'undefined'){ if (storyTemplateType=='iivideo') fdcWincolThreshhold = 100; }
		
		// Wincol overrides for stories by channel/section
                else if (displayedSection == "claytonchristensen" || (document.referrer.indexOf("/claytonchristensen") != -1 && document.referrer.indexOf(".html") == -1))       { fdcWincolThreshhold = 0; }
                else if (displayedSection == "business intelligence" || (document.referrer.indexOf("/business-intelligence") != -1 && document.referrer.indexOf(".html") == -1))       { fdcWincolThreshhold = 0; }
                else if (displayedSection == "fact and comment" || (document.referrer.indexOf("/fact-and-comment") != -1 && document.referrer.indexOf(".html") == -1))       { fdcWincolThreshhold = 0; }
		else if (displayedSection == "logistics" || (document.referrer.indexOf("/logistics") != -1 && document.referrer.indexOf(".html") == -1))       { fdcWincolThreshhold = 10; }
                else if (displayedSection == "thought leaders" || (document.referrer.indexOf("/thought-leaders") != -1 && document.referrer.indexOf(".html") == -1))   { fdcWincolThreshhold = 50; }
                else if (displayedSection == "cmonetwork") { fdcWincolThreshhold = 40; }
                else if (displayedSection == "intelligentinvesting") { fdcWincolThreshhold = 0; }
                
                else if (displayedChannel == "personalFinance" || (document.referrer.indexOf("/finance") != -1 && document.referrer.indexOf(".html") == -1))       { fdcWincolThreshhold = 10; }
                else if (displayedChannel == "markets" || (document.referrer.indexOf("/markets") != -1 && document.referrer.indexOf(".html") == -1))       { fdcWincolThreshhold = 0; }
                else if (displayedSection == "washington" || (document.referrer.indexOf("/beltway") != -1 && document.referrer.indexOf(".html") == -1))       { fdcWincolThreshhold = 50; }
		else if (displayedChannel == "business" || (displayedChannel == "entrepreneurs" && displayedSection != "ampc")) { fdcWincolThreshhold = 30;  }
		else if (displayedChannel == "entrepreneurs" && displayedSection == "ampc") { fdcWincolThreshhold = 90;  }
                else if (displayedChannel == "technology" && displayedSection == "security"){fdcWincolThreshhold = 40; }
                else if (displayedChannel == "technology" ) { fdcWincolThreshhold = 10;  }


		// Wincol overrides for stories by referrer

		// Wincol overrides by author
		else if (fdcWincolCheckByMeta("author", "Ken Fisher")) { fdcWincolThreshhold = 0; }

		// Wincol overrides for stories by URL
		else if (url.indexOf("women-style") != -1 || url.indexOf("woman-style") != -1 || url.indexOf("flew") != -1 || url.indexOf("2009/03/31/travel-perks-executives-women-leadership-trips_slide") != -1)        { fdcWincolThreshhold = 0; }
		else if (url.indexOf("power-09") != -1) { fdcWincolThreshhold = 0; }

		else                                            { fdcWincolThreshhold = fdcWincolDefault; }
	
                // Master override for medical tech special report
                if (url.indexOf("medical-tech-09") != -1 || url.indexOf("2009/09/24/sohaib-abbasi-data-intelligent-technology-informatica.html") != -1 || url.indexOf("global/2009/0921/entrepreneurs-meiloo-china-helping-with-heath-care.html") != -1 || url.indexOf("2009/09/17/robots-health-care-technology-breakthroughs-telehealth.html") != -1 || url.indexOf("2009/07/30/health-wellness-internet-lifestyle-health-online-facebook.html") != -1 || url.indexOf("2009/04/23/health-internet-records-technology-personal-tech-health.html") != -1 || url.indexOf("forbes/2009/1019/forbes-400-rich-list-09-soon-shiong-health-cancer-man.html") != -1) {
                        fdcWincolThreshhold = 0;
                }

                // Master override for Money for Life special report
                if ((url.indexOf("money-life-10") != -1 || (url.indexOf("2010/01/25/retirement-college-planning-mortgage-personal-finance-moneylife_land.html") != -1 )||( url.indexOf("2010/01/25/tips-first-time-homebuyer-personal-finance-mortgage.html") != -1 )|| (url.indexOf("2010/01/25/expecting-parents-money-personal-finance-new-parents.html") != -1) || (url.indexOf("2010/01/25/child-credit-childcare-college-irs-personal-finance-tax-breaks-for-kids.html") != -1) || (url.indexOf("2010/01/25/starting-first-business-personal-finance-first-business.html") != -1 )|| (url.indexOf("2010/01/25/college-savings-529-personal-finance-529.html") != -1)) || url.indexOf("2010/01/25/asset-allocation-retirement-personal-finance-retire.html") != -1||url.indexOf("2009/07/21/freelance-taxes-jobs-personal-finance-freelancers.html") != -1) {
                        fdcWincolThreshhold = 0;
                }
    // Master override for Money life tax
                if (url.indexOf("money-life-tax-10") != -1 || (url.indexOf("2010/02/16/tax-credits-planning-1040-irs-personal-finance-money-life-tax_land.html") != -1 )||( url.indexOf("2010/02/16/credit-cards-roth-rebates-personal-finance-tax-free-income.html") != -1 )|| (url.indexOf("2010/02/16/tax-deduction-mba-education-personal-finance-robert-wood.html") != -1) || (url.indexOf("2010/02/16/tax-credits-windows-solar-power-personal-finance-green-tax-breaks.html") != -1) || (url.indexOf("2010/02/16/tax-cancelled-debt-income-mortgage-personal-finance-robert-wood.html") != -1 )|| (url.indexOf("2010/02/16/irs-1040-tax-preparers-personal-finance-bad-tax-pros.html") != -1) || url.indexOf("2010/01/25/child-credit-childcare-college-irs-personal-finance-tax-breaks-for-kids.html") != -1|| url.indexOf("2010/02/16/custody-exemption-alimony-401k-personal-finance-divorce-tax-tips.html") != -1|| url.indexOf("2010/02/16/taxes-adult-children-gifts-credits-personal-finance-boomerang-kids.html") != -1|| url.indexOf("2010/02/16/vacation-primary-home-capital-gains-personal-finance-second-home-taxes.html") != -1|| url.indexOf("2010/02/16/irs-high-income-millionaire-audits-personal-finance-pwc-tonkovic.html") != -1|| url.indexOf("2010/02/16/taxes-traditional-roth-ira-rmd-personal-finance-roth-conversions-2010.html") != -1|| url.indexOf("2010/02/16/tax-credits-stimulus-seniors-personal-finance-2009-1040-traps") != -1|| url.indexOf("2009/11/03/audit-proof-tax-return-irs-personal-finance-wood.html") != -1|| url.indexOf("2010/01/27/irs-1099-computer-matching-audits-personal-finance-robert-wood.html") != -1) {
                        fdcWincolThreshhold = 0;
                }

                // Master override for best small companies special report
		if (url.indexOf("americas-best-company-10") != -1) { fdcWincolThreshhold = 0; }
                if (url.indexOf("small-companies-09") != -1) {
                        fdcWincolThreshhold = 0;
                }
		if ((url.indexOf("/2009/10/15/ten-best-retirement-havens-personal-finance-retire-abroad.html") != -1) ||(url.indexOf("/2009/10/15/retirement-havens-taxes-medicare-roth-retirement-09_land.html") != -1) || (url.indexOf("/2009/10/15/ten-best-retirement-havens-personal-finance-retire-abroad_slide_2.html") != -1) || (url.indexOf("/2009/10/15/retire-abroad-medicare-insurance-personal-finance-healthcare.html") != -1) || (url.indexOf("/forbes/2009/1102/foreign-retirement-france-italy-best-places-to-retire.html") != -1) || (url.indexOf("/2009/10/15/power-attorney-astor-estate-planning-personal-finance-seven-steps.html") != -1) || (url.indexOf("/2009/10/15/tax-deferred-roth-ira-retirement-personal-finance-bernicke.html1") != -1) || (url.indexOf("/2009/10/01/net-worth-wealth-assets-rank-personal-finance-net-worth-calculator.html") != -1) || (url.indexOf("/2009/10/15/social-security-benefit-personal-finance-2009-benefits.html") != -1) || (url.indexOf("/2009/10/15/social-security-benefit-personal-finance-2009-benefits.html") != -1) || (url.indexOf("/2009/09/14/retirement-moves-happiness-personal-finance-happiness.html") != -1) || (url.indexOf("/2009/10/06/home-equity-seniors-fha-personal-finance-reverse-mortgage.html") != -1))
		{
			fdcWincolThreshhold = 0;
		}

                //Special slot meetings-10
                if(typeof displayedSpecialSlot != "undefined" && (displayedSpecialSlot == "meetings-10" || displayedSpecialSlot == "meetings-09" || displayedSpecialSlot == "money-life-tax-10" || displayedSpecialSlot == "pf-money-milestones-10" ||displayedSpecialSlot =="fw-money-milestones-10" || displayedSpecialSlot =="virtualization-10" || displayedSpecialSlot =="travel-guide-10" || displayedSpecialSlot == "human-capital-10" || displayedSpecialSlot == "fictional-15-10" || displayedSpecialSlot == "global-2000-10" || displayedSpecialSlot == "human-capital-10"  || displayedSpecialSlot == "platinum-brands-bbs-10" || displayedSpecialSlot == "business-aviation-10" ||  displayedSpecialSlot == "retirement-10" || displayedSpecialSlot == "sustainable-tech-10" || displayedSpecialSlot == "cloud-computing-10" || displayedSpecialSlot == "serial-startups-10"))  {
                        fdcWincolThreshhold = 0;
                }
                
                if(typeof displayedSpecialSlot != "undefined" && displayedSpecialSlot == "small-biz-toolkit-10"){
                        fdcWincolThreshhold = 90;
                }
                
                if(typeof displayedSpecialSlot != "undefined" && (displayedSpecialSlot == "business-survival-10" || displayedSpecialSlot == "future-design-10" || displayedSpecialSlot == "Game-changers-10")){
                        fdcWincolThreshhold = 30;
                }
                
                if(typeof displayedSpecialSlot != "undefined" && (displayedSpecialSlot == "best-under-billion-09"|| displayedSpecialSlot == "asia-under-billion-10" || displayedSpecialSlot == "international-investing-10")){
                        fdcWincolThreshhold = 50;
                }
                
                if(typeof displayedSpecialSlot != "undefined" && displayedSpecialSlot == "best-life-10"){
                        fdcWincolThreshhold = 20;
                }
                
		fdcWincolResult = Math.round( Math.random() * 99 );
		fdcWincolStyle = (fdcWincolResult < fdcWincolThreshhold ) ? "column" : "window";
	}
	function fdcWincolCheckByMeta(name, content) {
	  var metas = document.getElementsByTagName('META');
	  var i;
	  for (i = 0; i < metas.length; i++) {
	    if (metas[i].getAttribute('NAME') == name) { break; }
	  }
	  if (typeof(metas[i]) != "undefined" && metas[i].getAttribute('CONTENT') == content) {
		return true;
	  } else {
	 	return false;
	  }
	}
	function fdcWincolStart(){
		if (fdcWincolStyle=="window") {
  	   		_startWindow();
		}
		else {
		    	_startColumn();
		}
	}
	function fdcWincolAd(){
		if (fdcWincolStyle=="window"){_adWindow();}
		else{_adColumn();}
	}
	function fdcWincolEnd(){
		if (fdcWincolStyle=="window")	{_endWindow();}
		else				{_endColumn();}
	}
	function fdcWincolSwap(orig, repl){
		var span = document.getElementById(orig);
		if(span==null){return;}
		var newspan = document.getElementById(repl);
		if(newspan==null){return;}
		while(span.childNodes[0]) { span.removeChild(span.childNodes[0]); }
		span.parentNode.replaceChild(newspan,span);
	}
	function _getBuster(){
		var date = new Date();
		var buster =  "" + date.getYear() + date.getMonth() + date.getDate() + 
				date.getHours() + date.getMinutes() + date.getSeconds();
		return buster;
	}
	function _startWindow(){
		document.write("<CENTER><span class=\"smallgreytxt\">ADVERTISEMENT</span>");
	}
	function _adWindow(){
		document.write('<div class="dynamicadlocation" id="dynamicAdWinDiv"><script language="JavaScript">OAS_AD(\'Block\');<\/script></div>');
	}
	function _endWindow(){
		document.write("</CENTER>");
		//document.write("<FONT COLOR=white>end ad</FONT><BR>");
		var ver = navigator.appName;
		var num = parseInt(navigator.appVersion);
		var myagent = navigator.userAgent.toLowerCase();
		if ((ver == "Microsoft Internet Explorer")&&(num >= 4)&&(myagent.indexOf('mac') < 0)) {
			_doBookmarkWindow();
		}
		_doAlertsWindow();
		document.write("<BR>");
	}
	function escapeSingleQuote(myline) {
      	  if ((myline != null) && (myline.length>0)) {
                var index = myline.indexOf("'");
                while (index != -1) {
                        var firstpart = myline.substring(0,index);
                        var secondpart = myline.substring(index,myline.length);
                        myline=firstpart + "\\" + secondpart;
                        if ( (index+2)<myline.length) {
                                index = myline.indexOf("'",index+2);
                        }
                        else {
                                return myline;
                        }
                }
        	}
        	return myline;
	}
	function _doBookmarkWindow() {
		document.write("<table cellspacing=\"0\" cellpadding=\"2\" border=\"0\" width=\"336\"><tr><td bgcolor =\"dece00\" colspan=\"3\"><img src=\"http://images.forbes.com/media/assets/spacer.gif\" width=\"1\" height=\"2\"></td></tr>");
		document.write("<tr><td valign=\"middle\"><a href=\"#\" onClick=\"doOmnitureTracking(\'winMakeHome\', this, \'forbescom\'); this.style.behavior='url(#default#homepage)';this.setHomePage('http://www.forbes.com');\">Make Forbes.com My Home Page</a></td><td><br>&nbsp;<br></td><td align=\"right\" valign=\"middle\"><a onClick=\"doOmnitureTracking(\'winBookThis\', this, \'forbescom\');window.external.AddFavorite('" + this.location + "','" + escapeSingleQuote(document.title) + "');\" href=\"#\">Bookmark This Page</a></td></tr><tr><td bgcolor=\"dece00\" colspan=\"3\"><img src=\"http://images.forbes.com/media/assets/spacer.gif\" width=\"1\" height=\"2\"></td></tr></table>");
	}
	function _startColumn(){
		document.write("<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"328\"><TR><TD WIDTH=168 VALIGN=TOP>");
		_doAlertsColumn();
		var ver = navigator.appName;
		var num = parseInt(navigator.appVersion);
		var myagent = navigator.userAgent.toLowerCase();
		if ((ver == "Microsoft Internet Explorer")&&(num >= 4)&&(myagent.indexOf('mac') < 0)) {
			_doBookmarkColumn();
		}
		document.write("</td>");
		document.write("<td width=\"10\">&nbsp;&nbsp;</td>");
		document.write("<td width=\"150\" valign=top>");
	}
	function _doBookmarkColumn() {
		document.write("<BR><table cellspacing=\"0\" cellpadding=\"2\" border=\"0\" width=\"168\"><tr><td bgcolor=\"dece00\" colspan=\"3\"><img src=\"http://images.forbes.com/media/assets/spacer.gif\" width=\"1\" height=\"2\"></td></tr>");
		document.write("<tr><td><img src=\"http://images.forbes.com/media/assets/spacer.gif\" width=\"1\" height=\"25\"></td><td valign=\"middle\" align=\"center\"><a href=\"#\" onClick=\"doOmnitureTracking(\'colMakeHome\', this, \'forbescom\');this.style.behavior='url(#default#homepage)';this.setHomePage('http://www.forbes.com');\">Make Forbes.com My Home Page</a></td><td><img src=\"http://images.forbes.com/media/assets/spacer.gif\" width=\"1\" height=\"25\"></td></tr><tr><td><img src=\"http://images.forbes.com/media/assets/spacer.gif\" width=\"1\" height=\"25\"></td><td valign=\"middle\" align=\"center\"><a onClick=\"doOmnitureTracking(\'colBookThis\',this, \'forbescom\');window.external.AddFavorite('" + this.location + "','" + escapeSingleQuote(document.title) + "');\" href=\"#\">Bookmark This Page</a></td><td><img src=\"http://images.forbes.com/media/assets/spacer.gif\" width=\"1\" height=\"25\"></td></tr><tr><td bgcolor=\"dece00\" colspan=\"3\"><img src=\"http://images.forbes.com/media/assets/spacer.gif\" width=\"1\" height=\"2\"></td></tr></table><br clear=\"all\">");
	}
	function _adColumn(){
		document.write('<div class="dynamicadlocation" id="dynamicAdColDiv">');
		OAS_AD('RightMiddle');
 		document.write('</div>');
	}
	function _endColumn(){
		document.write("</CENTER></TD></TR></TABLE>");
	}
        function _checkAlertForm(){
                if (!isMember()) {
                        if (checkAlertForm()) {
	                        setCLevelAlertsCookie();
				return true;
			}
			return false;
                } else {
                        var isOneChecked = verifyOneChecked(document.alertForm);
                        if (!isOneChecked)
                                alert('Please select at least one alert.');
                        return isOneChecked;
                }
        }
        function getTitleDropDown(){
                var titleArray = new Array("President", "Chairman", "Owner/Partner", "CEO", "CFO", "CIO/CTO", "CMO", "COO", "Vice President", "General Manager", "Middle Management", "Technical Staff", "Clerical/Support Staff", "Professional", "Homemaker", "Student", "Retired", "Not Employed", "Other");
                var titleDropDown = '';

                var alertsTitle = "Select Your Title";

                titleDropDown += '<option value=""  selected="selected">Select Your Title</option>';

                for (var i=0; i < titleArray.length; i++) {
                        titleDropDown += '<option value="' + titleArray[i] + '"';
                        titleDropDown += '>' + titleArray[i] + '</option>';
                }

                return titleDropDown;
        }
        function setCLevelAlertsCookie(){
                // Change theDomain to: '192.168.0.164' for testing 
                var theDomain = '.forbes.com';

                var cLevelTitleMap = new Object();
                cLevelTitleMap["President"] = "President";
                cLevelTitleMap["Chairman"] = "Chairman";
                cLevelTitleMap["Owner/Partner"] = "Owner%2FPartner";
                cLevelTitleMap["CEO"] = "CEO";
                cLevelTitleMap["CFO"] = "CFO";
                cLevelTitleMap["CIO/CTO"] = "CIO%2FCTO";
                cLevelTitleMap["CMO"] = "CMO";
                cLevelTitleMap["COO"] = "COO";
		cLevelTitleMap["General Manager"] = escape("General Manager");
                cLevelTitleMap["Vice President"] = escape("Vice President");
                cLevelTitleMap["Technical Staff"] = escape("Technical Staff");

                var aTitle=document.getElementById("alerts_title");
                // Title is not a mandatory field.
                if (aTitle != null){
                        var clevel_title = cLevelTitleMap[aTitle.value];
                        if (clevel_title != null) {
                                var theDate = new Date();
                                var fiveYearsLater = new Date( theDate.getTime() + 153792000000 );
                                var expiryDate = fiveYearsLater.toGMTString();

                                document.cookie='ceotitle=' + escape('Officer Alerts') + ';expires=' + expiryDate + ';path=/;domain=' + theDomain;
                        } else if (aTitle.value != "Select Your Title") {
                                // The user is not clevel.  Remove the cookie if present.
                                var results = document.cookie.match ( 'ceotitle=' );

                                // remove the cookie
                                if ( results ){
                                        var cookie_date = new Date ( );  // current date & time
                                        cookie_date.setTime ( cookie_date.getTime() - 1 );
                                        document.cookie = "ceotitle=;expires=Thu, 01-Jan-1970 00:00:01 GMT;path=/;domain=" + theDomain;
                                }
                        } // else no title select; therefore do nothing with cookies.
                }
        }
        function tableForEach(keyArray, valueHash, numColumns, header) {

                // Used to prepend a prefix to the alert parameters for differentiation purposes.
                var headerAlertMap = new Object();
                headerAlertMap["Companies"] = "tickers";
                headerAlertMap["People"] = "persons";
                headerAlertMap["Topics"] = "keywords";

                if (keyArray.length==0) return;

                document.write('<tr height="22"><td style="padding-top: 5px; border-top: thin dotted" colspan="' +
                                numColumns*2 + '" height="22" ><b>' + header + '</b>  </td></tr>');


                for (var i=0; i<keyArray.length/numColumns; i++) {
                        document.write('<tr valign="top">');
                        for (var j=0; j<numColumns; j++) {
                                if ((i*numColumns+j) < keyArray.length) {
                                        var element = keyArray[i*numColumns+j];
                                        document.write('<td valign="middle" width="20"><input type="checkbox" name="' +
                                                headerAlertMap[header] + '.' + element + '" value="' + valueHash[element] +
                                                '"></td>' + '<td valign="middle" width="134">' + valueHash[element] + '</td>');
                                }
                        }
                        document.write('</tr>');
                }
        }
        function _doAlertsWindow(){
                if((typeof tickerKeyList == "undefined") && (typeof personKeyList == "undefined") && (typeof keywordKeyList == "undefined")){return;}

                document.write('<style type="text/css" media="screen"><!--#alertsbox td {font-size: 11px;}--></style><form action="http://members.forbes.com/alertSignup" method="post" name="alertForm" onSubmit="return _checkAlertForm();"><input type="hidden" name="action" value="alerts_signup"><input type="hidden" name="comingFrom" value="alerts"><input type="hidden" name="service" value="key_membership"><input type="hidden" name="gotoURL" value="' + window.location.href + '"><div id="alertsbox"><table bgcolor="#cccccc" border="0" cellpadding="1" cellspacing="0" width="336"><tbody><tr><td><table bgcolor="#ffffff" border="0" cellpadding="2" cellspacing="0" width="100%"><tbody><tr><td rowspan="100"></td><td colspan="4"></td><td rowspan="100"></td></tr><tr><td  colspan="4"  style="font-size: 12px; font-weight: bold;"><span style=" color: 990000;">News by E-mail </span>Get stories by E-Mail on this topic <span style=" color: 990000;">FREE</span></td></tr><tr><td colspan="3">');
                document.write('<span style="color: #369; font-size: 12px; font-weight: bold; margin-bottom: 6px;"></span></td><td style=" padding-bottom: 7px; padding-top: 7px;" width="134"><a href="http://members.forbes.com/membership/signup.do?comingFrom=alerts"><b></b><scr' + 'ipt language="JavaScript">OAS_AD("AlertsLogo");</scr' + 'ipt></td></tr>');

                if((typeof tickerKeyList != "undefined") && (typeof tickerHash != "undefined")){
                        tableForEach(tickerKeyList, tickerHash, 2, "Companies");
                }
                if((typeof personKeyList != "undefined") && (typeof personHash != "undefined")){
                        tableForEach(personKeyList, personHash, 2, "People");
                }
                if((typeof keywordKeyList != "undefined") && (typeof keywordHash != "undefined")){
                        tableForEach(keywordKeyList, keywordHash, 2, "Topics");
                }

        if (!isMember()) {
                document.write('<tr><td  colspan="4"  style="font-size: 12px; font-weight: bold; padding-bottom: 5px; border-bottom: thin dotted"></td></tr><tr><td colspan="2"><span style="color: #369; font-size: 12px; font-weight: bold; margin-bottom: 6px;">Become a member FREE </span></td><td style=" padding-bottom: 7px; padding-top: 7px;" width="134" colspan="2">Already a Member? <a href="http://members.forbes.com/membership/signup.do?comingFrom=alerts&gotoURL=' + window.location.href + '"><b>Log In</b><scr' + 'ipt language="JavaScript">OAS_AD("AlertsLogo");</scr' + 'ipt></td></tr>');
		document.write('<tr valign="top" height="5"><td style="padding-top: 9px; padding-bottom: 5px; border-top: thin dotted"  colspan="4" align="left" valign="middle" height="5"><table width="291" border="0" cellspacing="0" cellpadding="0"><tr><td width="166"><input style="font-size: 11px;" type="text" name="login" ONFOCUS="clearText(this)" id="login" value="Enter Username" class="alertemail" size="21" ></td><td width="166"><input style="font-size: 11px;" type="text" name="email" ONFOCUS="clearText(this)" id="email" value="Enter E-Mail Address" class="alertemail" size="21" ></td></tr><tr><td style="padding-top: 6px;" width="166"><select style="font-size: 11px;" name="title" size="1" tabindex="1" id="alerts_title">' + getTitleDropDown() + '</select></td><td>Receive Special Offers?&nbsp;<input type="checkbox" name="special_offers" checked></td></tr><tr></td><td style="padding-top: 6px;"><input style="font-size: 11px;" class="alertsignup" value="Sign Me Up!" name="submit" type="submit"></td> </tr></table></td></tr>');
        } else {
                document.write('<tr valign="top" height="5"><td style="padding-top: 9px; padding-bottom: 5px; border-top: thin dotted"  colspan="4" align="left" valign="middle" height="5"><input type="hidden" name="email" id="email" value="fdc@fdc.com"><input type="hidden" name="title" id="alerts_title" value="title"><input style="font-size: 11px;" class="alertsignup" value="Sign Me Up!" name="submit" type="submit"><table width="281" border="0" cellspacing="0" cellpadding="0"><tr><td width="166"></td><td></td></tr><tr><td style="padding-top: 6px;" width="166"><table width="141" border="0" cellspacing="0" cellpadding="0"><tr><td></td><td></td></tr></table></td><td style="padding-top: 6px;"></td> </tr></table></td></tr>');
        }


                document.write('<tr height="22"> <td style="padding-top: 0px;" colspan="4" align="left" height="22"><span style="float: left;"><a href="javascript:popup(\'faq\')">FAQ</a> | <a href="javascript:popup(\'terms\')">Terms, Conditions and Notices</a> | <a href="javascript:popup(\'privacy\')">Privacy Policy</a></span></td> </tr><tr height="22"> <td style="padding-top: 5px; border-top: thin dotted"   colspan="4" align="left" height="22"><b>Also available: </b><a href="http://members.forbes.com/membership/editprofile.do">E-Mail Newsletters</a>');

	if( thisURL.indexOf("CIAtAGlance.jsp") > 0)  {
	document.write('&nbsp;|&nbsp;<a href="http://software.forbes.com">Software Finder</a> <img src="http://ads.forbes.com/RealMedia/ads/adstream_nx.ads/forbes.com/markets/finance/compinfo/CIAtAGlance@x113?x"></td> </tr> <tr> <td class="linkset" align="left"></td> <td class="linkset" align="left" width="141"></td> </tr> </tbody> </table></td></tr></tbody></table></div>');
	} else {
                document.write('</td> </tr> <tr> <td class="linkset" align="left"></td> <td class="linkset" align="left" width="141"></td> </tr> </tbody> </table></td></tr></tbody></table></div>');
}     

}
        function _doAlertsColumn(){
                if((typeof tickerKeyList == "undefined") && (typeof personKeyList == "undefined") && (typeof keywordKeyList == "undefined")){return;}

                document.write('<style type="text/css" media="screen"><!--#alertsbox td {font-size: 11px;}--></style><form action="http://members.forbes.com/alertSignup" method="post" name="alertForm" onSubmit="return _checkAlertForm();"><input type="hidden" name="action" value="alerts_signup"><input type="hidden" name="comingFrom" value="alerts"><input type="hidden" name="service" value="key_membership"><input type="hidden" name="gotoURL" value="' + window.location.href + '"><div id="alertsbox"><table bgcolor="#cccccc" border="0" cellpadding="1" cellspacing="0" width="166"><tbody><tr><td><table bgcolor="#ffffff" border="0" cellpadding="2" cellspacing="0" width="100%"><tbody><tr><td rowspan="100"></td><td></td><td width="141"></td><td rowspan="100"></td></tr><tr><td colspan="2"  style=" font-size: 12px; font-weight: bold; padding-bottom: 5px;"><span style=" color: 990000;">News by E-mail</span><br>Get stories by E-Mail on this topic <span style=" color: 990000;">FREE</span></td></tr><tr><td colspan="2"><scr' + 'ipt language="JavaScript">OAS_AD("AlertsLogo");</scr' + 'ipt><br>');
                document.write('<span style="color: #369; font-size: 12px; font-weight: bold; margin-bottom: 6px;"></span><a href="http://members.forbes.com/membership/signup.do?comingFrom=alerts"><b></b></td></tr>');

                if((typeof tickerKeyList != "undefined") && (typeof tickerHash != "undefined")){
                        tableForEach(tickerKeyList, tickerHash, 1, "Companies");
                }
                if((typeof personKeyList != "undefined") && (typeof personHash != "undefined")){
                        tableForEach(personKeyList, personHash, 1, "People");
                }
                if((typeof keywordKeyList != "undefined") && (typeof keywordHash != "undefined")){
                        tableForEach(keywordKeyList, keywordHash, 1, "Topics");
                }

        if (!isMember()) {
                document.write('<tr><td style="border-top: thin dotted;" colspan="2"></td></tr><tr><td colspan="2"><span style="color: #369; font-size: 12px; font-weight: bold; margin-bottom: 6px;">Become a member FREE </span>Already a Member? <a href="http://members.forbes.com/membership/signup.do?comingFrom=alerts"><b>Log In</b></td></tr>');
		document.write('<tr valign="top" height="24"> <td style="padding-top: 5px; border-top: thin dotted"  colspan="2" valign="middle" height="24"><input style="font-size: 11px;" type="text" name="login" ONFOCUS="clearText(this)" id="login" value="Enter Username" class="alertemail" size="21" ></td></tr> <tr valign="top" height="24"> <td style="padding-top: 5px;"  colspan="2" valign="middle" height="24"><input style="font-size: 11px;" type="text" name="email" ONFOCUS="clearText(this)" id="email" value="Enter E-Mail Address" class="alertemail" size="21" ></td></tr> <tr><td  colspan="2"><select style="font-size: 11px;" name="title" size="1" id="alerts_title"> ' + getTitleDropDown() + '</select></td> </tr><tr> <td colspan="2"><table width="141" border="0" cellspacing="0" cellpadding="0"><tr><td>Receive Special Offers?</td><td><input type="checkbox" name="special_offers" checked></td></tr></table></td></tr> <tr> <td style="padding-bottom: 5px;" colspan="2"><input style="font-size: 11px;" class="alertsignup" value="Sign Me Up!" name="submit" type="submit"><br> </td> </tr></form>');
        } else {
                document.write('<tr valign="top" height="24"> <td style="padding-top: 5px; border-top: thin dotted"  colspan="2" valign="middle" height="24"><input type="hidden" name="email" id="email" value="fdc@fdc.com"><input type="hidden" name="title" id="alerts_title" value="title"><input style="font-size: 11px;" class="alertsignup" value="Sign Me Up!" name="submit" type="submit"></td></tr> <tr><td  colspan="2"></td> </tr><tr> <td colspan="2"><table width="141" border="0" cellspacing="0" cellpadding="0"><tr><td></td><td></td></tr></table></td></tr> <tr> <td style="padding-bottom: 5px;" colspan="2"><br> </td> </tr></form>');
        }
                // FAQ, Privacy policy, newsletters signup link and closing tags
                document.write('<tr height="22"> <td style="padding-bottom: 5px; "  colspan="2" height="22"><span style="float: left;"><a href="javascript:popup(\'faq\')">FAQ</a> |  <a href="javascript:popup(\'privacy\')">Privacy Policy</a></span><br><span style="float: left;"><a href="javascript:popup(\'terms\')">Terms, Conditions and Notices</a> </span></td></tr> <tr height="22"> <td style="padding-top: 5px; border-top: thin dotted" colspan="2" align="left" height="22"><b>Also available:</b><br> </td> </tr> <tr> <td  colspan="2" align="left"><a href="http://members.forbes.com/membership/editprofile.do">E-Mail Newsletters</a> </td> </tr> <tr> <td class="linkset" align="left"></td> <td class="linkset" align="left" width="141"></td> </tr> </tbody> </table></td></tr></tbody></table></div>');
           //document.write('</td> </tr> <tr> <td class="linkset" align="left"></td> <td class="linkset" align="left" width="141"></td> </tr> </tbody> </table></td></tr></tbody></table></div>');

   }

//cookie
var exp = new Date();
exp.setTime(exp.getTime() + (2*24*60*60*1000));
function WriteNew (name, value, exp, path, secure, domain) {
	var argv = WriteNew.arguments;
	var argc = WriteNew.arguments.length;
	var expires = (argc > 2) ? argv[2] : null;
	var path = (argc > 3) ? argv[3] : null;
	var secure = (argc > 4) ? argv[4] : false;
	var domain = (argc > 5) ? argv[5] : null;
	document.cookie = name + "=" + escape (value) +
	((expires == null) ? "" : ("; expires=" + expires.toGMTString())) +
	((path == null) ? "" : ("; path=" + path)) +
	((domain == null) ? "" : ("; domain=" + domain)) +
	((secure == true) ? "; secure" : "");
}

//jumpNav
function jumpNav (form) {
	if (form.jumpSelect.options[form.jumpSelect.selectedIndex].value != "") {
		this.location = form.jumpSelect.options[form.jumpSelect.selectedIndex].value;
	}
}
//popit
function popit(url, popWidth, popHeight) {
	if (popWidth < 760) {
		popWidth = 760;
	}
	slideWin = window.open(url, 'popup', "width=" + popWidth + ",height=" + popHeight + ",toolbar=0,resizable=1,scrollbars=1");
	slideWin.focus();
}
//go
function go(url) {
	if (opener && !opener.closed) {
		opener.location = url;
		opener.focus();
	} else {
		fakeWin = window.open(url, 'fake');
		fakeWin.focus();
	}
}
//closeWindow
function closeWindow() {
window.close()
}

//channel
if ((typeof fdcchannel == "undefined") || (fdcchannel == "")) {
	if ((typeof displayedChannel != "undefined") && (displayedChannel != "")) {
		if ((displayedChannel == 'home_asia') || (displayedChannel == 'home_europe')) displayedChannel = 'home';
		fdcchannel = displayedChannel;
	}
	else {
		var channelIndex = this.location.href.indexOf('fdcchannel=');
		if (channelIndex == -1) fdcchannel = 'home';
		else {
			fdcchannel =  this.location.href.substr(channelIndex+8);
			var channelEnd = fdcchannel.indexOf('&');
			if (channelEnd>-1) fdcchannel = fdcchannel.substr(0,channelEnd);
		}
	}
}
if (fdcchannel == 'leadership') fdcchannel = 'work';
if (fdcchannel == 'forbeslife') fdcchannel = 'lifestyle';
count=0;
for (i=0;i<channelArr.length;i++) {
	if(channelArr[i] == fdcchannel) {
		break;
	}
	count++;
}

if (count==channelArr.length) fdcchannel = "home";

if( typeof displayedChannel == "undefined" || displayedChannel != "experian" ){
	var displayedChannel = fdcchannel;
}

if (typeof fdcsponsor == "undefined") {
	var sponsorIndex = this.location.href.indexOf('fdcsponsor=');
	if (sponsorIndex == -1) fdcsponsor = '';
	else {
		fdcsponsor =  this.location.href.substr(sponsorIndex+8);
		var sponsorEnd = fdcsponsor.indexOf('&');
		if (sponsorEnd>-1) fdcsponsor = fdcsponsor.substr(0,sponsorEnd);
	}
}

var today = new Date();
var fdcwelcb = document.cookie.split(";");
for(var i=0 ; i < fdcwelcb.length ; i++)
	if((fdcweldelim = fdcwelcb[i].indexOf('__welcome')) > -1) {
		var fdcwelCookieTime = fdcwelcb[i].substring(fdcweldelim+9);
		if(fdcwelCookieTime == "") {
			var tomorrow = new Date();
			tomorrow.setTime(tomorrow.getTime() + (24*60*60*1000));
			document.cookie = fdcwelcb[i] +tomorrow.getTime()+'; path=/; domain=.forbes.com; expires=' + tomorrow.toGMTString();
		}
		else if(fdcwelCookieTime>today.getTime()) {
			var fdcwelExpire = new Date;
			fdcwelExpire.setTime(fdcwelCookieTime);
			document.cookie = fdcwelcb[i] +'; path=/; domain=.forbes.com; expires=' + fdcwelExpire.toGMTString();
		}
	}

document.write('<\/head>');
if(this.location.href.indexOf("/comments/most")!=-1)
	document.write('<body id="comments_xxl" bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0" link="#003399" alink="#0080ff" vlink="#6699cc">');
else
	document.write('<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0" link="#003399" alink="#0080ff" vlink="#6699cc">');

//video ad frame
if(displayedSection == 'Video' || (displayedSection == 'intelligentinvesting' && pageType =='story')) {
	//document.write('<iframe name="dummy" width="0" height="0" style="height:0px;" scrolling="no" marginwidth="0" marginheight="0" hspace="0" vspace="0" frameborder="0"></iframe>');
	noSearch = 1;
}

//for thought leaders video page  when you click on advanced in search box
if((typeof storyTemplateType != "undefined") && (displayedSection =="thought leaders")){
if( storyTemplateType == 'iivideo'){
noSearch = 1;
}
}
//partners
var partner = '';
var isForbesPartner = false;
var query = this.location.search.substring(1);
if( query.length > 0 ) {
	var params=query.split("&");
	for (var i = 0 ; i < params.length ; i++) {
		var pos = params[i].indexOf("=");
		var name = params[i].substring(0, pos);
		var value = params[i].substring(pos + 1);
		if( name == "partner" ) {
			partner = value;
			if(typeof OAS_query_addition != 'undefined' && OAS_query_addition.length>0) {
				OAS_query_addition  += "&partner="+value;
			} else {
				OAS_query_addition  = "partner="+value;
			}
			// Homepage ad targeting for embeddable homepage
			if (partner=="embed" && pageType == "home" && hpType == "us") { OAS_sitepage = "homepage.forbes.com/embed/index.jhtml"; }
		}
		// Yahoo! Buzz Testing for query params
		if( name == "ybf1" ) {
			if(typeof OAS_query_addition != 'undefined' && OAS_query_addition.length>0) {
				OAS_query_addition  += "&ybf1="+value;
			} else {
				OAS_query_addition = "ybf1="+value;
			}
		}
	}
}
var url = window.document.URL.toString();
if(url.indexOf("archive") != -1 ) {
	var partner = "keepmedia";
}
var currentHostname=this.location.hostname;
if (currentHostname.length > 0) {
	if (currentHostname == "aol.forbes.com" || currentHostname == "forbes.aol.com") {
		partner = 'aol';
	}
}
var partner_cookie = "partner_session=" + partner;
if (document.cookie.indexOf(partner_cookie) < 0) {
	document.cookie=partner_cookie + ';path=/; domain=.forbes.com';
}
else {
	//the new value of partner is checked and if it is a different, then cookie is set
	var existing_cookie_value = findCookie("partner_session");
	if(partner != existing_cookie_value && partner != ""){
		document.cookie=partner_cookie + ';path=/; domain=.forbes.com';
	}
}

var _rsCG='0';
//adfixxxxx
function docdowrite(s) {
   document.write(s);
}
//end adfix

/* rollover 18APRIL2006 Version 1.0
 * Tania Puell, April 2006
 * Copyright (c) 2006 Forbes.com
 */

var homeArr = new Array();
var newslettersArr = new Array();
var opinionsArr = new Array();
var businessArr = new Array();
var technologyArr = new Array();
var marketsArr = new Array();
var entrepreneursArr = new Array();
var workArr = new Array();
var listsArr = new Array();
var personalFinanceArr = new Array();
var lifestyleArr = new Array();
var hTimer;
var thisSection;
var htimer = 0;
var lastSection;
var thisAdDiv;
var vSection;
homeArr[0] = new Array("Video","http://www.forbes.com/video/");
homeArr[1] = new Array("ForbesWoman","http://www.forbes.com/forbeswoman/");
homeArr[2] = new Array("CEO Network","http://www.forbes.com/ceonetwork/");
homeArr[3] = new Array("Org Chart Wiki","http://orgchart.forbes.com/");
homeArr[4] = new Array("Mobile","http://www.forbes.com/mobile/");
homeArr[5] = new Array("Portfolio Tracker","http://www.forbes.com/portfolio/");
homeArr[6] = new Array("Blogs","http://bfn.forbes.com");
homeArr[7] = new Array("E-mail Newsletters","http://www.forbes.com/membership/signup.jhtml");
homeArr[8] = new Array("Special Reports","http://www.forbes.com/search/storyTypeResults.jhtml?storyType=Special+Report");
businessArr[0] = new Array("Autos","http://www.forbes.com/autos/");
businessArr[1] = new Array("Billionaires","http://www.forbes.com/business/billionaires/");
businessArr[2] = new Array("Energy","http://www.forbes.com/energy/");
businessArr[3] = new Array("Logistics","http://www.forbes.com/logistics/");
businessArr[4] = new Array("Media & Entertainment","http://www.forbes.com/entertainment/");
businessArr[5] = new Array("Pharma & Health","http://www.forbes.com/healthcare/");
businessArr[6] = new Array("SportsMoney","http://www.forbes.com/sportsmoney/");
businessArr[7] = new Array("Wall Street","http://www.forbes.com/wallstreet/");
businessArr[8] = new Array("Washington","http://www.forbes.com/beltway/");
technologyArr[0] = new Array("Breakthroughs","http://www.forbes.com/breakthroughs/");
technologyArr[1] = new Array("Business Intelligence","http://www.forbes.com/business-intelligence/");
technologyArr[2] = new Array("CIO Network","http://www.forbes.com/cionetwork/");
technologyArr[3] = new Array("Intelligent Tech","http://www.forbes.com/intelligent-technology/");
technologyArr[4] = new Array("Personal Tech","http://www.forbes.com/personaltech/");
technologyArr[5] = new Array("Security","http://www.forbes.com/security/");
technologyArr[6] = new Array("Velocity","http://blogs.forbes.com/velocity/");
technologyArr[7] = new Array("Wireless","http://www.forbes.com/wireless/");
marketsArr[0] = new Array("Bonds","http://www.forbes.com/bonds/");
marketsArr[1] = new Array("Commodities","http://www.forbes.com/commodities/");
marketsArr[2] = new Array("Currencies","http://www.forbes.com/currencies/");
marketsArr[3] = new Array("Economic Calendar","http://www.forbes.com/static_html/econ_calendars/economic_calendar.html");
marketsArr[4] = new Array("Economy","http://www.forbes.com/economy/");
marketsArr[5] = new Array("Emerging Markets","http://www.forbes.com/emergingmarkets/");
marketsArr[6] = new Array("Equities","http://www.forbes.com/equities/");
marketsArr[7] = new Array("Intelligent Investing","http://www.forbes.com/intelligentinvesting/");
marketsArr[8] = new Array("Markets Brief","http://www.forbes.com/markets/marketsbrief/");
entrepreneursArr[0] = new Array("Boost Your Business","http://www.forbes.com/entrepreneurs/boostyourbusiness/");
entrepreneursArr[1] = new Array("Finance","http://www.forbes.com/entrepreneurs/finance/");
entrepreneursArr[2] = new Array("HR","http://www.forbes.com/entrepreneurs/humanresources/");
entrepreneursArr[3] = new Array("Law & Tax","http://www.forbes.com/entrepreneurs/lawtaxation/");
entrepreneursArr[4] = new Array("Promising Companies","http://www.forbes.com/entrepreneurs/promising-companies/");
entrepreneursArr[5] = new Array("Sales & Marketing","http://www.forbes.com/entrepreneurs/salesmarketing/");
entrepreneursArr[6] = new Array("Management","http://www.forbes.com/entrepreneurs/management/");
entrepreneursArr[7] = new Array("Tech","http://www.forbes.com/entrepreneurs/technology/");
workArr[0] = new Array("Careers","http://www.forbes.com/leadership/careers/");
workArr[1] = new Array("Corporate Citizenship","http://www.forbes.com/leadership/citizenship/");
workArr[2] = new Array("Governance","http://www.forbes.com/leadership/governance/");
workArr[3] = new Array("Managing","http://www.forbes.com/leadership/managing/");
workArr[4] = new Array("CEO Network","http://www.forbes.com/ceonetwork/");
workArr[5] = new Array("CMO Network","http://www.forbes.com/cmo-network/");
workArr[6] = new Array("ForbesWoman","http://www.forbes.com/forbeswoman/");
workArr[7] = new Array("Thought Leaders","http://www.forbes.com/thought-leaders/");
personalFinanceArr[0] = new Array("Adviser Network","http://www.forbes.com/advisernetwork/");
personalFinanceArr[1] = new Array("ETFs","http://www.forbes.com/finance/etfs/");
personalFinanceArr[2] = new Array("Investing Ideas","http://www.forbes.com/finance/investingideas/");
personalFinanceArr[3] = new Array("Mutual Funds","http://www.forbes.com/finance/funds/");
personalFinanceArr[4] = new Array("Newsletters","http://www.newsletters.forbes.com/DRHM/servlet/ControllerServlet?Action=DisplayHomePage&SiteID=es_764&Locale=en_US&pgm=41464300");
personalFinanceArr[5] = new Array("Philanthropy","http://www.forbes.com/finance/philanthropy/");
personalFinanceArr[6] = new Array("Retirement & College","http://www.forbes.com/finance/retirementcollege/");
personalFinanceArr[7] = new Array("Taxes","http://www.forbes.com/finance/taxesestates/");
lifestyleArr[0] = new Array("Business Travel","http://www.forbes.com/business-travel/");
lifestyleArr[1] = new Array("Collecting","http://www.forbes.com/lifestyle/collecting/");
lifestyleArr[2] = new Array("Health","http://www.forbes.com/lifestyle/health/");
lifestyleArr[3] = new Array("Real Estate","http://www.forbes.com/lifestyle/realestate/");
lifestyleArr[4] = new Array("Sports","http://www.forbes.com/lifestyle/sports/");
lifestyleArr[5] = new Array("Style","http://www.forbes.com/lifestyle/style/");
lifestyleArr[6] = new Array("Travel","http://www.forbes.com/lifestyle/travel/");
lifestyleArr[7] = new Array("Vehicles","http://www.forbes.com/lifestyle/vehicles/");
lifestyleArr[8] = new Array("Wine & Food","http://www.forbes.com/lifestyle/wine/");
listsArr[0] = new Array("Billionaires","http://www.forbes.com/billionaires/");
listsArr[1] = new Array("Richest Americans","http://www.forbes.com/400richest/");
listsArr[2] = new Array("Celebrities","http://www.forbes.com/celebrities/");
listsArr[3] = new Array("Largest Public Companies","http://www.forbes.com/forbes2000/");
listsArr[4] = new Array("Private Companies","http://www.forbes.com/private/");
listsArr[5] = new Array("Education","http://www.forbes.com/education/");
listsArr[6] = new Array("All Lists","http://www.forbes.com/lists/");
opinionsArr[0] = new Array("Books & Culture","http://www.forbes.com/opinions/books/");
opinionsArr[1] = new Array("Business Visionaries","http://www.forbes.com/businessvisionaries/");
opinionsArr[2] = new Array("Columnists","http://www.forbes.com/static_html/opinions/columnists.html");
opinionsArr[3] = new Array("Contributors","http://search.forbes.com/search/storyTypeSearch?storyType=Contributors");
opinionsArr[4] = new Array("High Five","http://search.forbes.com/search/storyTypeSearch?storyType=High%20Five");
opinionsArr[5] = new Array("Thoughts on the Business of Life ","http://thoughts.forbes.com/quotes/");
opinionsArr[6] = new Array("Fact And Comment","http://www.forbes.com/fact-and-comment");
newslettersArr[0] = new Array("Main","http://www.newsletters.forbes.com/DRHM/servlet/ControllerServlet?Action=DisplayMainPage&SiteID=es_764&pgm=56674300");
newslettersArr[1] = new Array("Stocks","http://www.newsletters.forbes.com/DRHM/servlet/ControllerServlet?Action=DisplayCategoryProductListPage&SiteID=es_764&Locale=en_US&parentCategoryID=518900&categoryID=519600&pgm=57154500");
newslettersArr[2] = new Array("Fixed Income","http://www.newsletters.forbes.com/DRHM/servlet/ControllerServlet?Action=DisplayCategoryProductListPage&SiteID=es_764&Locale=en_US&categoryID=519300&pgm=57154600");
newslettersArr[3] = new Array("Commodities","http://www.newsletters.forbes.com/DRHM/servlet/ControllerServlet?Action=DisplayCategoryProductListPage&SiteID=es_764&Locale=en_US&parentCategoryID=518900&categoryID=519700&pgm=57154700");
newslettersArr[4] = new Array("ETFs & Mutual Funds","http://www.newsletters.forbes.com/DRHM/servlet/ControllerServlet?Action=DisplayCategoryProductListPage&SiteID=es_764&Locale=en_US&Env=BASE&categoryID=519000&pgm=57064300");
newslettersArr[5] = new Array("International","http://www.newsletters.forbes.com/DRHM/servlet/ControllerServlet?Action=DisplayCategoryProductListPage&SiteID=es_764&Locale=en_US&categoryID=519100&pgm=57154800");
newslettersArr[6] = new Array("By Guru","http://store.digitalriver.com/store/es_764/Content/pbPage.Forbescom_Meet_Gurus_March2010/pgm.56644300");

var hiColor = '#990000';
var loColor = '#996666';
var lastColor;
var userIni = 0;
var displayedSectionVar;
var searchBoxTopIsSet = false;
var attacheTopIsSet = false;

function setAttacheTop()
{
	var login = document.getElementById('theLogin');
	var bookmark = document.getElementById('bookmarkPage');
	var target = document.getElementById('attacheContain');
	var loginOffTop = getPosTop(login);
	var agt = navigator.userAgent.toLowerCase();
	var padding = 10;
	if(bookmark && agt.indexOf("msie 8.0") == -1) {
		loginOffTop =  getPosTop(bookmark) + 9;
	}
	if((agt.indexOf("firefox") != -1 || agt.indexOf("safari") != -1) && (typeof hpType != "undefined") || this.location.href.indexOf('http://www.forbes.com/entertainment/') == 0) {
		padding += 25;
	}
	var tempAdd = 0;
	if(login.previousSibling)
	{
		tempAdd = login.previousSibling.clientHeight;
	}
	target.style.top = (loginOffTop+login.clientHeight+padding+tempAdd)+"px";
	attacheTopIsSet = true;
}
function getPosTop(theObj)
{
	var theTop,thePar;
	theTop = theObj.offsetTop;
	while(theObj.offsetParent!=null)
	{
		thePar = theObj.offsetParent;
		theTop += thePar.offsetTop;
		theObj = thePar;
	}
	return theTop;
}

function showHMenu(channel) 
{
	if (fdcsponsor == 'noad') 
	{
		if (document.getElementById('searchbox')) 
		{
			document.getElementById('bigBannerDiv').style.height = 0;
		}
	}
	if (document.getElementById('panel1')) {
			if( is_searchtabs==true) {
		document.getElementById('panel1').style.display = "block";
			}
	}
	
	if(document.getElementById('column')) document.getElementById('column').style.display = "block";
	keepMenu();
	hover(channel);
	if ((typeof curChannel == 'undefined') || (channel != curChannel) || (channel == fdcchannel)) {
		curChannel = channel;
		thisChannStr = "";
		var pinknav = false;
		if("home" == channel && window.pinkpzn) {
			pinknav = true;
			thisChannArr = pinkpzn;
			var notPersonilized = [
				{name:'Video',url:'http://www.forbes.com/video/'},
				{name:'E-mail Newsletters',url:'http://www.forbes.com/membership/signup.jhtml'},
				{name:'ForbesWoman',url:'http://www.forbes.com/forbeswoman/'}];
			for(var i=0;i<notPersonilized.length;i++) {
				thisChannStr += '<span id="pinknav'+i+'" class="horizItem" onmouseup="javascript:homenvg(this,\''
					+notPersonilized[i].url+'\',true)" onmouseout="hideMenu()" onmouseover="showVMenu(\'pinknav'+i+'\')">'
					+notPersonilized[i].name+'</span>';
			}
			thisChannStr += '<span class="fastOne">|</span><span class="fastOne">My Sections</span><span class="fastTwo">&gt;</span>';
		} else {
			thisChannArr = eval(channel+'Arr');
		}
		thisWidth = 0;
		thisChannX = document.getElementById(channel+'S').offsetParent.offsetLeft;
		thisChannWidth = document.getElementById(channel+'S').offsetWidth;
		if (navigator.appName == 'Microsoft Internet Explorer') thisChannWidth += 8;
		for(i=0;i<thisChannArr.length;i++) {
			if ((typeof displayedSectionVar != "undefined") && (channel + i == displayedSectionVar)) thisHC = "hirizItem";
			else thisHC = "horizItem";
			thisChannStr += '<span id="'+channel+i+'"onMouseOver="showVMenu(\''+channel+i+'\')" onMouseOut="hideMenu()" class="' + thisHC + '" onMouseUp="javascript:';
			if("home"==pageType && "home"==channel) {
				thisChannStr += 'homenvg(this,\''+thisChannArr[i][1]+'\','+pinknav+')';
			} else {
				thisChannStr += 'nvg(\''+thisChannArr[i][1]+'\')';
			}
			thisChannStr += '">'+thisChannArr[i][0]+'<\/span>';
		}
	
		document.getElementById('hRO').innerHTML = thisChannStr;
	
		for(i=0;i<thisChannArr.length;i++) {
			thisWidth += document.getElementById(channel+i).offsetWidth;
		}
		thisPadding = (790 - thisWidth)/2;
		if (790-thisChannX>=thisWidth) thisPadding = thisChannX;
		else if (thisChannX+thisChannWidth>=thisWidth) thisPadding = thisChannX + thisChannWidth - thisWidth -6;
		if (thisPadding<0) thisPadding = 0;
	
	
		document.getElementById('hRO').style.width = "790px";
		document.getElementById('hRO').style.paddingLeft = thisPadding+"px";
		if (document.getElementById('hRO').offsetWidth > 790) {
			document.getElementById('hRO').style.width = (790 - thisPadding)+"px";
		}

		if(channel != fdcchannel) {
			for(i=0;i<channelArr.length;i++) {
				thisChannel = channelArr[i];
				if(thisChannel != fdcchannel) {
					document.getElementById(thisChannel+'S').style.backgroundColor = 'transparent';
					document.getElementById(thisChannel+'S').style.color = '#336699';
				}
			}
			document.getElementById(channel+'S').style.backgroundColor = '#ffffff';
			document.getElementById(channel+'S').style.color = '#000000';
			document.getElementById(fdcchannel+'S').style.backgroundColor = 'transparent';
			document.getElementById(fdcchannel+'S').style.color = loColor;
			lastColor = loColor;
			
		} else {
			for(i=0;i<channelArr.length;i++) {
				thisChannel = channelArr[i];
				if(thisChannel != fdcchannel) {
					document.getElementById(thisChannel+'S').style.backgroundColor = 'transparent';
				}
			}
			document.getElementById(fdcchannel+'S').style.backgroundColor = '#ffffff';
			if (!userIni) document.getElementById(fdcchannel+'S').style.color = hiColor;
			lastColor = hiColor;
			showSection();
		}
	}
	userIni=0;
}

function nvg(URL) {
	this.location.href=URL;
}
function homenvg(el,url,pinknav) {
	var tracking = "navbarHome_Clk";
	if(pinknav) tracking = "navbarHome_Clk_FAST";
	doOmnitureTracking(tracking,el,'forbescom');
	this.location.href=url;
}
function hover(channel) {
	document.getElementById(channel+'S').style.color = "#000000";
}
function hideHMenu() {
	document.getElementById('hRO').innerHTML = "";
	document.getElementById('hRO').style.width = 790+"px";
	showHMenu(fdcchannel);
	showAd();
}
function hideMenu() {
	if ((typeof thisSection != "undefined") && (document.getElementById(thisSection))) {
		if((typeof displayedSectionVar != "undefined") && (displayedSectionVar == thisSection)) document.getElementById(thisSection).style.color = hiColor;
		else document.getElementById(thisSection).style.color = "#336699";
	}
	for(i=0;i<channelArr.length;i++) {
		thisChannel = channelArr[i];
		if (thisChannel != fdcchannel) document.getElementById(thisChannel+'S').style.color = '#336699';
		else document.getElementById(thisChannel+'S').style.color = lastColor;
	}
	hTimer = setTimeout('hideHMenu()',1000);
}
function showVMenu(section) {
	keepMenu();
	document.getElementById(section).style.color ="#000000";
	thisSection = section;
}
function keepMenu() {
	if (hTimer) clearTimeout(hTimer);
}

function showSection() {
	if ((typeof displayedSection != "undefined") && (displayedSection != "") ) {
	   var tca_varname = displayedChannel + 'Arr';
	   eval( 'thisChannelArr = typeof ' + tca_varname + ' == "undefined" ? [] : ' + tca_varname );
	   count = 0;
	   for(i=0;i<thisChannelArr.length;i++) {
		displayedSectionLnk = thisChannelArr[i][0].toLowerCase();
	   	displayedSectionLnk = displayedSectionLnk.replace(/ \& /g,"");
	   	displayedSectionLnk = displayedSectionLnk.replace(/ /g,"");
	   	if (displayedSectionLnk == displayedSection.toLowerCase()) {
			displayedSectionVar = displayedChannel + i;
			break;
		}
		count++;
	   }
	   if(count==thisChannelArr.length) {
	        displayedSectionLnk = displayedSection;
	        displayedSectionLnk = displayedSectionLnk.substr(displayedSection.indexOf(" ")+1,displayedSectionLnk.length);
	   	for(i=0;i<thisChannelArr.length;i++) {
			checkSection = thisChannelArr[i][0].toLowerCase();
	   		if (checkSection.indexOf(displayedSectionLnk)>-1) {
				displayedSectionVar = displayedChannel + i;
				break;
			}
		}
	   }
//stoopid failsafe

	   switch (displayedSection) {
           	case "wallstreet":
		displayedSectionVar = "business7";
	        break;
           	case "beltway":
		displayedSectionVar = "business8";
	        break;
           	case "enterprisetech":
		displayedSectionVar = "technology2";
	        break;
           	case "infoimaging":
		displayedSectionVar = "technology3";
	        break;
           	case "ebusiness":
		displayedSectionVar = "technology4";
	        break;
           	case "entrefinance":
		displayedSectionVar = "entrepreneurs1";
	        break;
           	case "entrehr":
		displayedSectionVar = "entrepreneurs2";
	        break;
           	case "entrelaw":
		displayedSectionVar = "entrepreneurs3";
	        break;
		case "ampc":
	        displayedSectionVar = "entrepreneurs4";
		break;
           	case "entresales":
		displayedSectionVar = "entrepreneurs5";
	        break;
           	case "entremgmt":
		displayedSectionVar = "entrepreneurs6";
	        break;
           	case "entretech":
		displayedSectionVar = "entrepreneurs7";
	        break;
           	case "estate_planning":
		displayedSectionVar = "personalFinance0";
	        break;
           	case "guruinsights":
		displayedSectionVar = "personalFinance2";
	        break;
		case "starcurrency":
		displayedSectionVar = "business3";
		break;
		case "firewallblog":
		displayedSectionVar = "technology5";
		break;
		case "facetofaceblog":
		displayedSectionVar = "home0";
		break;
		case "workinprogressblog":
		displayedSectionVar = "work6";
		break;
		case "forbeswoman/style":
		displayedSectionVar = "work6";
		case "forbeswoman/powerwomen":
		displayedSectionVar = "work6";
		case "forbeswoman/leadership":
		displayedSectionVar = "work6";
		case "forbeswoman/entrepreneurs":
		displayedSectionVar = "work6";
		case "forbeswoman/well-being":
		displayedSectionVar = "work6";
		case "forbeswoman/time":
		displayedSectionVar = "work6";
		case "forbeswoman/networth":
		displayedSectionVar = "work6";
		break;
	   }
	   if (fdcsponsor == "economic_calendar") { displayedSectionVar="markets3"; }
	   if (document.getElementById(displayedSectionVar)) document.getElementById(displayedSectionVar).style.color=hiColor;

	}
}
function hoverAct(section,on) {
	if (lastSection) {
		document.getElementById(lastSection+'L').style.color = '#369';
	}
	document.getElementById(section+'L').style.color = '#000000';
	lastSection = section;
}
function showAd() {
	if (searchTab == 0) {
		if(document.getElementById(thisAdDiv) != null) document.getElementById(thisAdDiv).style.display = 'block';
  	}
}
function hideAd() {
 	if(document.getElementById(thisAdDiv)) {
		document.getElementById(thisAdDiv).style.display = 'none';
	} 
}

// attache and members
//var noattache=1;
//var maintenance=1;

// american dream
 var amrdrm = window.location + "";
 if ( amrdrm.indexOf("dream0307") != -1 ) { var noattache=1; }

var phatcatId = '35415301490';
function isMember() {
        forbes0 = document.cookie.indexOf('forbesmemb=');
        forbes1 = document.cookie.indexOf('forbesmemb_confirm=');
        if (forbes0 != -1 && forbes1 != -1 && getCookieValue('forbesmemb') != phatcatId)
                return true;
        else 
                return false;
}
function getCookieValue(NameOfCookie) {
    if( document.cookie.length > 0 ) {
        begin = document.cookie.indexOf( NameOfCookie+"=" );
        if( begin != -1 ) {
            begin += NameOfCookie.length + 1;
            end = document.cookie.indexOf( ";", begin );
            if( end == -1 ) end = document.cookie.length;
            return unescape( document.cookie.substring( begin, end ));
        }
    }
    return null;
}
function hasOASAdPos(pos) {
       if (typeof OAS_listpos != undefined) {
               if (OAS_listpos.indexOf(pos) != -1) {
                       return true;
               }
       }
       return false;
}

function get_tnt_cookie(check_cookie){
		//alert("In get_tnt_cookie");

		var tnt_cookie_mix;
		var cookie_name = '';
		var cookie_value = '';
		var a_temp_cookie;
		var check_cookie_found = false;

		var index_tnt_cookie_mix = document.cookie.indexOf("tntcookiemix");
		
		//alert("header - index_tnt_cookie_mix = " + index_tnt_cookie_mix);

		if(index_tnt_cookie_mix > -1){

			var a_all_cookies = document.cookie.split( ';' );
			for( i = 0; i < a_all_cookies.length; i++ ){
				
				// now we'll split apart each name=value pair
				a_temp_cookie = a_all_cookies[i].split( '=' );
				
				// and trim left/right whitespace while we're at it
				cookie_name = a_temp_cookie[0].replace(/^\s+|\s+$/g, '');
				
				if ( cookie_name == 'tntcookiemix' ){
					tnt_cookie_mix  = unescape( a_temp_cookie[1].replace(/^\s+|\s+$/g, '') );
					break;
				}
			}


			var all_tnt_cookies = tnt_cookie_mix.split( ':' );

			for( i = 0; i < all_tnt_cookies.length; i++ ){
				// now we'll split apart each name=value pair
				a_temp_cookie = all_tnt_cookies[i].split( '\/' );

				// and trim left/right whitespace while we're at it
				cookie_name = a_temp_cookie[0].replace(/^\s+|\s+$/g, '');
			
				if(cookie_name == check_cookie){
					check_cookie_found = true;
					cookie_value = unescape( a_temp_cookie[1].replace(/^\s+|\s+$/g, '') );
					//alert("cookie_value = " + cookie_value);
					return cookie_value;
				}
			}

			if(!check_cookie_found){
				// If the cookie doesnot exist in the cookie mix then you can create it as below.
				// tnt_cookie_mix = tnt_cookie_mix + ":" + check_cookie + "\/" + "test1";
				// document.cookie = "tntcookiemix=" + tnt_cookie_mix;
				return null;
			}
		}else{
			var fdc_global_attache_value;
			if(Math.floor(Math.random() * 10) != 0){
				//it is in 90%
				fdc_global_attache_value = "fail";
			}else{
				fdc_global_attache_value = "pass";

			}

			//set expiry to 2 yrs
			var expire = new Date();
			expire.setTime(expire.getTime() + (2 * 365 * 24 * 3600 * 1000));

			// to add more than one cookie
			//document.cookie = "tntcookiemix=fdc_global_attache\/" + fdc_global_attache_value + ":tnt_test1\/test1:tnt_test2\/test2";
			document.cookie = "tntcookiemix=fdc_global_attache\/" + fdc_global_attache_value + "; path=/; domain=.forbes.com; expires="+expire.toUTCString();

			return null;
		}


}

function attache() {
	if (this.location.href.indexOf("video.forbes.com")>-1) noattache = 1;
	var backTo = this.location.href;
	if (backTo.indexOf('forbes.com') == -1 || typeof noattache != "undefined" || noattache) return;
	var isHomePage = (typeof hpType != "undefined");
	var isPhatCat = (getCookieValue('forbesmemb') == phatcatId);
	var isPhatCatHomePage = (isPhatCat && isHomePage);
	var aMember = isMember();
        
	if ((aMember && isHomePage) || isPhatCatHomePage || this.location.href.indexOf("/columnists.html")>-1 || (typeof globalPageName!="undefined" &&typeof globalPageName!="thoughts:quotes"))  { 
		document.write("<div style='display:none'>");
		OAS_AD('MyForbesHeader');
		document.write("</div>");
		MyForbesHeaderCount = " ";
	}
	if (typeof displayedSection == "undefined") displayedSection = '';
	if (!displayedSection) displayedSection = '';
	if (typeof displayedChannel == "undefined") displayedChannel ='';
	if (!displayedChannel) displayedChannel = '';
	var primSecAttache = displayedSection;
	if (primSecAttache =='' && displayedChannel !='') {
		primSecAttache = displayedChannel;
	}
	if(backTo.indexOf('solutions-health') >= 0) {
		primSecAttache = "Healsolutions07";
	}
	if(backTo.indexOf('storyType=Clayton+Christensen') >= 0) {
		primSecAttache = "ClaytonChristensen";
	}
	if (primSecAttache == "business visionaries" || primSecAttache == "business%20visionaries" || backTo.indexOf("businessviz") != -1) {
		primSecAttache = "bizviz08";
	}
	if (primSecAttache == "adviser%20network") {
		primSecAttache = "advisernetwork";
	}
	if (backTo.indexOf("identity08") != -1) {
		primSecAttache = "identity08";
	}
	var agt = navigator.userAgent.toLowerCase();
	var browserVersion = parseInt(navigator.appVersion);
	var is_ie      = (agt.indexOf("msie") != -1);
	var is_ff      = (agt.indexOf("firefox") != -1);
	var is_ff1     = (agt.indexOf("firefox/1.0") != -1);
	var is_mac     = (agt.indexOf("macintosh") != -1);
	var is_ff1_mac = (is_ff1 && is_mac);
	var is_si      = (agt.indexOf("safari") != -1);
	var is_ie3     = (is_ie && (browserVersion < 4));
	var is_ie4     = (is_ie && (browserVersion == 4) && (agt.indexOf("msie 4")!=-1) );
	var is_ie5     = (is_ie && (browserVersion == 4) && (agt.indexOf("msie 5.0")!=-1) );
	var is_ie6     = (is_ie && (browserVersion == 4) && (agt.indexOf("msie 6.0")!=-1) );
	var is_ie7 = (is_ie && (browserVersion == 4) && (agt.indexOf("msie 7.0")!=-1) );
	var is_ie5up   = (is_ie && !is_ie3 && !is_ie4);
	var is_mz    = (is_si || is_ff);
	var is_ok    = (is_ie5up || is_mz);

	if ((backTo.indexOf('/portfolio/')==-1) && (backTo.indexOf('/preview/')==-1) && ((backTo.indexOf('/membership/')==-1) || (backTo.indexOf('editprofile')!=-1)) && (backTo.indexOf('my.forbes.com')==-1) && (is_ok)) {
		document.write('<style> .fifthN { width: 212px; z-index: 20; ');
		var ver = navigator.appName;
		var num = parseInt(navigator.appVersion);
		var myagent = navigator.userAgent.toLowerCase();
		var colapsedHeight = 208;
		if ((ver == "Microsoft Internet Explorer")&&(num >= 4)&&(myagent.indexOf('mac') < 0)) {
			if(((pageType=='story' || pageType=='magstory') && typeof fdc_nocss != 'undefined') || typeof fdc_center != "undefined") {
				colapsedHeight = 195;
				//if(is_ie6) document.write('top:85px; position:absolute; float:right; right:38px;');
				if(is_ie6 || is_ie7) document.write('top:85px; position:absolute; float:right; right:50px;');
				else document.write('top:85px; position:absolute; float:right; right:50px;');
			} else {
				colapsedHeight = 195;
				document.write('top:70px; position:absolute; left:790px;');
			}
		} else { 
			if(pageType=="home" || ((pageType=='story' || pageType=='magstory') && typeof fdc_nocss != 'undefined')|| typeof fdc_center != "undefined") {
				document.write('top:50px; position:absolute; float:right; right:51px;');
			} else {
				document.write('top:35px; position:absolute; left:790px;');
			}
		}
		document.write('display: none;');
		if(!isHomePage) document.write('border: none !important');
		else if(aMember || isPhatCatHomePage)
			document.write('overflow: hidden; height: '+colapsedHeight+'px; border-bottom: solid #ccc 2px;');
		else document.write('border: none !important');
		document.write('} </style>');
		document.write('<div id="attacheContain" class="fifthN">');
		if(aMember || isPhatCat) {
			if(document.cookie.indexOf('fifthinit') == -1)
				document.cookie = 'fifthinit=0;path=/;domain=.forbes.com';
			writeAttache(backTo, primSecAttache, is_ff, isPhatCat);
			document.cookie = 'fifthinit=0;path=/;domain=.forbes.com';
		} else {
			writeStaticAttache(primSecAttache);
			var loadsa = function() { document.getElementById("attacheContain").style.display = 'block'; };
			setTimeout(loadsa, 800);

			//var fdc_global_cookie = get_tnt_cookie("fdc_global_attache");
			//if( fdc_global_cookie == "pass"){
				//document.write('<div class="mboxDefault"></div>');
				//document.write('<script type="text\/javascript">mboxCreate(\'fdc_global_attache\',\'mboxPageValue=5\')<\/script>');
			//}


		}
		document.write('<\/div>');
	}
}

function writeAttache(backTo, primSecAttache, is_ff, isPhatCat) {
	document.write(
		'<div id="attacheOuter">'+
			'<hl><div id="forbesattache">Forbes.com Attaché</div></hl>');
	if(hasOASAdPos('x110')) document.write('<div id="x110ad"><script type="text/javascript">OAS_AD("x110");</script></div>');
	else document.write('<iframe scrolling="no" height="30" frameborder="0" width="210" src="http://ads.forbes.com/RealMedia/ads/adstream_sx.ads/forbes.com/attache@x110,MyForbesHeader!x110" marginheight="0" marginwidth="0"></iframe>');
	document.write(
		'<div id="attacheBody" style="display: block;">'+
			'<ul id="attacheNoDrag">'+
			'<li id="attacheTop"></li>'+'<li id="adtagid" class="adtag" >');
				//'<li><center><small>Advertisement</small></center></li><li id="adtagid" class="adtag" >');
	if(!hasOASAdPos('MyForbesHeader') && typeof hpType == "undefined") {
		document.write('<iframe scrolling="no" height="345" frameborder="0" width="210" src="http://ads.forbes.com/RealMedia/ads/adstream_sx.ads/forbes.com/attache@x110,MyForbesHeader!MyForbesHeader" marginheight="0" marginwidth="0"></iframe>');
	}
	else if(typeof hpType == "undefined")
		document.write('<script type="text/javascript">OAS_AD("MyForbesHeader");</script>');
	document.write(	'</li>'+
				'<li id="attacheButt"></li>'+
			'</ul>'+
			'<ul id="attacheDrag" class="ui-sortable"></ul>'+
		'</div>'+
		'<div id="attacheFooter"');
	if(!isPhatCat) {
	document.write('<div id="attacheColor"><dl id="attacheColo"><dd id="attachered">Red</dd><dd id="attachepaleblue">Pale Blue</dd><dd id="attacheforbesblue">Forbes Blue</dd><dd id="attachegreen">Green</dd><dd id="attachewhite">White</dd><dd id="attachegray">Gray</dd><dd id="attacheblack">Black</dd></dl></div>');
	} else { document.write('<br />'); }
	document.write('</div>'+
		'</div>');
	var pznId = document.cookie.substr(document.cookie.indexOf('forbesmemb=') + 11);
	if(pznId.indexOf(";") != -1) pznId = pznId.substr(0, pznId.indexOf(';'));
	var doLoadAttache = function() {
		return loadAttache('http://my.forbes.com/fifth/fifth.do?type=js&pzn_id='+pznId+'&hpType='+(window.hpType?window.hpType:"")+'&pageType='+pageType+'&backto='+backTo+'&primSec='+primSecAttache+'&channel='+(window.displayedChannel?window.displayedChannel:"")+'&url='+this.location.href);
	};
	is_ff ? window.addEventListener("load", doLoadAttache, true) : doLoadAttache();

        document.write('<link rel="stylesheet" type="text/css" href="http://images.forbes.com/css/fifth/styles.css" />'+
		'<link rel="stylesheet" href="http://images.forbes.com/css/attache/lastSale.css" type="text/css" media="screen" />'+
		'<div style="display:none">'+
			'<!-- Omniture SiteCatalyst -->'+
			'<script type="text/javascript" src="http://images.forbes.com/scripts/omniture/s_code_forbesattache.js"></script>'+
		'</div>');
}

var afterLoadEvent = false;
function loadAttache(url) {
	var setAfterLoadEvent = function() { afterLoadEvent = true; };
	if(window.attachEvent) window.attachEvent("onload", setAfterLoadEvent);
	var s = document.createElement('script');
	s.src = url;
	s.type = "text/javascript";
	var elem = document.getElementById('attacheOuter');
	if(elem) {
		elem.appendChild(s);
	} else { 
		// for Safari 
		var elem = document.getElementsByTagName('head')[0];
		if(elem)
			elem.appendChild(s);
	}
}

function writeStaticAttache(primSecAttache) {
	// click images are numbered 1-6 - get a random image number
	var imgn = Math.floor(Math.random() * 6) + 1;
	var membersUrl = 'http://members.forbes.com/membership/signup_fifth.jhtml?comingFrom=fifth&gotoURL=' + this.location.href;
	document.write(
	'<link rel="stylesheet" href="http://images.forbes.com/css/fifth/styles.css">'+
	'<div id="attacheOuter">'+
		'<h1><div id="forbesattache">Forbes.com Attaché</div></h1>');
	if(hasOASAdPos('x110')) document.write('<div id="x110ad"><script type="text/javascript">OAS_AD("x110");</script></div>');
	else document.write('<iframe scrolling="no" height="30" frameborder="0" width="210" src="http://ads.forbes.com/RealMedia/ads/adstream_sx.ads/forbes.com/attache@x110,MyForbesHeader!x110" marginheight="0" marginwidth="0"></iframe>');
	document.write(
		'<div id="attacheBody"  style="display: block;">'+
			'<a href="'+membersUrl+'"><img border="0" alt="" src="http://images.forbes.com/media/attache/click'+imgn+'b.jpg"/></a>');
	if(typeof hpType != "undefined") {
		document.write('</div><div id="attacheFooter"><br/></div></div>'); return;
	}
	document.write(	'<ul id="attacheNoDrag">'+
				'<li id="ad">'+'<div id="adtagid">');
					//'<small>Advertisement</small><div id="adtagid">');
	if(hasOASAdPos('MyForbesHeader')) document.write('<script type="text/javascript">OAS_AD("MyForbesHeader");</script>');
	else document.write('<iframe scrolling="no" height="345" frameborder="0" width="210" src="http://ads.forbes.com/RealMedia/ads/adstream_sx.ads/forbes.com/attache@x110,MyForbesHeader!MyForbesHeader" marginheight="0" marginwidth="0"></iframe>');
	document.write(	'</div></li>'+
			'</ul>'+
			'<div id="attacheButt"></div>'+
        		'<a href="'+membersUrl+'"><img border="0" alt="New Forbes Attache Enhancements" src="http://images.forbes.com/media/attache/signupclick1.jpg"/></a>'+
		'</div>'+
		'<div id="attacheFooter"><br /></div>'+
        '</div>');
}
//end attache and members

function addPinkTab(ftitd) {
		if (!ftitd) return;
		var doc = document;
		function addStyle(q, o) { for (var p in o) q.style[p] = o[p] }
		var tab = doc.createElement("div");
		var block = doc.createElement("div");
		var pinktab = doc.createElement("img");
		pinktab.src = "http://images.forbes.com/ads/ibm/midsizebusiness_tab.gif";


		addStyle(tab, {
			width: "112px",
			height: "43px",
			position: "absolute",
			backgroundColor: "#ee3f97",
			zIndex:-1
		});
		addStyle(pinktab, {
			width: "112px",
			height: "19px",
			paddingTop: "2px",
			paddingRight: "10px",
			verticalAlign: "bottom",
			display:'block',
			cursor: "pointer"
		});

		function addEvent(el, evt, callback) {
			if (window.addEventListener)
				el.addEventListener(evt, callback, true)
			else if (window.attachEvent)
				el.attachEvent("on" + evt, callback)
		}
		block.appendChild(tab);
		block.appendChild(pinktab);

		ftitd.appendChild(block);
		
		var open = false,
		tabContent,
		iframes = true,
		adDiv = false;
		function hideAd() {
			var video = doc.getElementById('flashVideo');
			if(video) addStyle(video,{left:'-9000px'});
			adDiv = doc.getElementById('dynamicAdColDiv')||doc.getElementById('dynamicAdWinDiv');
			if(adDiv && window.jQuery) {
				var ad = $(adDiv);
				var h = ad.height();
				if($('#winColFillers').length==0)
					ad.after('<div id="winColFillers" style="height:'+h+'px" class="adBgMId"></div>');
				ad.hide();
			}
			if(iframes===true) {
				iframes=[];
				var iframeels = doc.getElementsByTagName('iframe');
				var embeddable = new RegExp("^http://www.forbes.com/video/embed/embed\.html");
				var intelligentinvesting = new RegExp("^http://www.forbes.com/scripts/video/iiplayer/player\.html");
				for(var i=0;i<iframeels.length;i++) {
					if(embeddable.test(iframeels[i].src)) iframes.push(iframeels[i]);
					if(intelligentinvesting.test(iframeels[i].src)) iframes.push(iframeels[i]);
				}
			}
			for(var i=0;i<iframes.length;i++)
				addStyle(iframes[i],{left:'-9000px',position:'relative'});
			if(window.stopSlideshow) stopSlideshow();
			
		}
		function showAd() {
			var video = doc.getElementById('flashVideo');
			if(video) addStyle(video,{left:0});
			if(adDiv && window.jQuery) {
				$(adDiv).show();
				$('#winColFillers').remove();
			}
			for(var i=0;i<iframes.length;i++)
				addStyle(iframes[i],{left:0});
		}
		function closeTab() {
			open = false;
			if (arguments.length > 0) {
				arguments[0].cancelBubble = true;
				if (arguments[0].preventDefault)
					arguments[0].preventDefault()
			}
			ftitd.removeChild(tabContent);
			showAd();
			return false
		}
		addEvent(pinktab, "click", function () {
			if (open) return closeTab()
			open = true;
			hideAd();
			tabContent = doc.createElement("div");
			addStyle(tabContent, {
				position: "absolute",
				left: "0px",
				zIndex: 9999999,
				width: "780px",
//				background: "#ee3f97",
				background: "#fff",
				padding: "10px"
			});
			var close = doc.createElement("a");
			close.href = "#";
			addStyle(close, {
				padding: "0 16px 0 0",
				background: "transparent url(http://images.forbes.com/media/assets/close.gif) no-repeat scroll right center",
				textAlign: "right",
				display: "block",
				cssFloat: "right",
				fontWeight:'bold',
				color:'#000'
			});
			close.innerHTML = "Close";
			addEvent(close, "click", closeTab);
			tabContent.appendChild(close);
			var iframe = doc.createElement("div");
			addStyle(iframe, {
				clear: "both"
			});
			iframe.innerHTML = '<iframe src="http://ads.forbes.com/RealMedia/ads/adstream_sx.ads/forbes.com/ads/ibm-pink/@x101" width="780" height="600" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>';
			tabContent.appendChild(iframe);
			ftitd.appendChild(tabContent);
		});
}

function loadHeaderStyle(){
        document.write('<link rel="stylesheet" type="text/css" href="http://images.forbes.com/css/newrollover-xhtml.css">');

        if (navigator.appName.indexOf("Netscape") != -1)
                document.write('<link rel="stylesheet" type="text/css" href="http://images.forbes.com/css/suggest_ns.css">');
        else
                document.write('<link rel="stylesheet" type="text/css" href="http://images.forbes.com/css/suggest_ie-xhtml.css">');

        document.write('<link rel="stylesheet" type="text/css" href="http://images.forbes.com/css/search/search-dark.css">');
}

function loadMakeHomePage(){
        //To Make Home Page
        var ver = navigator.appName;
        var num = parseInt(navigator.appVersion);
		var addPosition = false;
		var position = '';
		if (isForbesPartner) {
			var agt = navigator.userAgent.toLowerCase();
			if(typeof hpType != "undefined" 
					|| (this.location.href.indexOf('http://www.forbes.com/entertainment/') == 0 && agt.indexOf("msie") != 0)) {
				position = 'absolute';
				addPosition = true;
			} else if (this.location.href.indexOf('http://www.forbes.com/finance/retirementcollege/') == 0
						|| this.location.href.indexOf('http://bfn.forbes.com/') == 0
						|| this.location.href.indexOf('forbes-answer-network-land') != -1
						|| this.location.href.indexOf('/bow/b2c/') != -1
						|| this.location.href.indexOf('http://www.forbes.com/static_html/opinions/columnists') == 0
						|| this.location.href.indexOf('http://www.forbes.com/microsoft/events/businessviz/') == 0
						|| this.location.href.indexOf('http://www.forbes.com/fdc/reprints/Reprints.jhtml') == 0) {
				position = 'static';
				addPosition = true;
			}
		}
        if ( thisURL.match(slideshowExpr)|| typeof fdc_center != 'undefined') {
                document.write('<div class="newlogin" id="theLogin"><div id="utilities" style="position: static;">');
        } else {
                document.write('<div class="newlogin" id="theLogin" ' + (addPosition ? 'style="position: '+position+' !important;"' : '') + '><div id="utilities" ' + (addPosition ? 'style="position: '+position+' !important; top:-13px"' : '') + '>');
        }
        if ((typeof maintenance == "undefined") || !maintenance) {
            if (isMember()) {

                        var mName = findCookie('forbesmembname');
                        if((typeof mName == 'undefined') || !mName) { mName = ''; }
                        //modified for ad served link
                        doMembersHeader('<h3>Welcome <span id="userName">'+mName+'</span>! | <a id="logOutLink" href="http://www.forbes.com/membership/external_logout.jhtml" style="color:#fff;font-size:13px;">Log Out</a></h3>\n', '<a id="portfolioLink" href="http://www.forbes.com/portfolio/">Portfolio</a> | <a href="http://www.forbes.com/membership/editprofile.jhtml" class="whitelink">Profile</a> | <div id="x107div" style="display: inline"><SCR' + 'IPT Language="JavaScript" src="http://ads.forbes.com/RealMedia/ads/adstream_jx.ads/forbes.com/magazinead@x107"></SCR' + 'IPT></div>');

                } else {
                        //modified for ad served link
                        doMembersHeader('<h3><a id="registerLink" href="http://www.forbes.com/membership/signup.jhtml?gotoURL=http://members.forbes.com/membership/logout.do?service=membership&amp;action=logout">Become a member</a> | <a id="registerLink" href="http://www.forbes.com/membership/signup.jhtml?gotoURL=http://members.forbes.com/membership/logout.do?service=membership&action=logout" style="color:#fff;font-size:13px;">Log In</a></h3>\n', '<a id="portfolioLink" href="http://www.forbes.com/portfolio/">Portfolio</a> | <div id="x107div" style="display: inline"><SCR' + 'IPT Language="JavaScript" src="http://ads.forbes.com/RealMedia/ads/adstream_jx.ads/forbes.com/magazinead@x107"></SCR' + 'IPT></div>\n');
                }
        }
}

function loadNavBg(){
        document.write('<div class="navbg">');
        document.write('<table  cellspacing="1" cellpadding="0" border="0" width="790"><tr align="center">');
        for (i=0;i<channelArr.length;i++) {
                thisChannel = channelArr[i];
                thisLongChannel = longChannelArr[i];
                thisChannelURL = channelURLArr[i];
                document.write('<td bgcolor="#e2ebf4"><div class="navitem" id="'+thisChannel+'S" onMouseOut="hideMenu();" onMouseOver="showHMenu(\''+thisChannel+'\');" onMouseUp="javascript:nvg(\'http://www.forbes.com'+thisChannelURL+'\')">&nbsp;'+thisLongChannel+'&nbsp;</div></td>');

        }
        document.write('</tr></table></div>');
        document.write('<div class="horizRO" id="hRO" style="position:relative;"></div>');
        document.write('<div class="vertRO" id="vRO" onMouseOut="hideMenu();" onMouseOver="keepMenu();"></div>');
}

function showSearchBox(){
var main_srch;
var main_tab;

        if( typeof CookieMix != 'undefined' && CookieMix) {
                if(getCookie('search_term')!=null && getCookie('search_term')!= 'undefined' )
                        main_srch=getCookie('search_term');

                if(getCookie('tab_val')!=null && getCookie('tab_val')!= 'undefined' )
                        main_tab=getCookie('tab_val');

                deleteCookie('search_term');
                deleteCookie('tab_val');
        }

        document.write('<tr>');
        document.write('<td>');
        document.write('<form id="searchbox_global" method="get" name="SearchMain" action="http://search.forbes.com/search/find" target="_top" onsubmit="javascript:formSubmitted();"/>');
        document.write('<input name="tab" value="searchtabgeneraldark" type="hidden" />');

        if (pageTypeForSlide)
                document.write('<input id="search_panel1" class="textbox" name="MT" type="text" autocomplete="off" onBlur="termChanged(this.value,\'panel1\');"  onChange="termChanged(this.value,\'panel1\');" onkeyup ="termChanged(this.value,\'panel1\');" onkeypress ="termChanged(this.value,\'panel1\');" onfocus="stopSlideshow();"/>');
        else
                 document.write('<input id="search_panel1" class="textbox" name="MT" type="text" autocomplete="off" onBlur="termChanged(this.value,\'panel1\');"  onChange="termChanged(this.value,\'panel1\');" onkeyup ="termChanged(this.value,\'panel1\');" onkeypress ="termChanged(this.value,\'panel1\');" />');


        if (main_tab == 'panel1' && main_srch!=null && main_srch!= 'undefined')
                document.SearchMain.MT.value = main_srch;

        document.write('<span class="searchbutton"><a href="javascript:document.SearchMain.submit()" onclick="javascript:formSubmitted()">Search</a></span>');
        document.write('<div style="clear:both;"></div>');
        document.write('</form>');
        document.write('</td>');
        document.write('<td width="180">');
        document.write('<form id="searchbox_ticker" name="SearchHedform" action="http://finapps.forbes.com/finapps/jsp/finance/compinfo/CIAtAGlance.jsp" onsubmit="return submitTkr(\'SearchHedform\');"/>');
        document.write('<input name="tab" value="searchtabquotesdark" type="hidden" />');

        if (pageTypeForSlide)
                document.write('<input id="search_panel2" class="textbox" name="tkr" type="text" autocomplete="off" onBlur="termChanged(this.value,\'panel2\');"  onChange="termChanged(this.value,\'panel2\');" onkeyup ="termChanged(this.value,\'panel2\');" onkeypress ="termChanged(this.value,\'panel2\');" onfocus="stopSlideshow();"/>');
        else
                document.write('<input id="search_panel2" class="textbox" name="tkr" type="text" autocomplete="off" onBlur="termChanged(this.value,\'panel2\');"  onChange="termChanged(this.value,\'panel2\');" onkeyup ="termChanged(this.value,\'panel2\');" onkeypress ="termChanged(this.value,\'panel2\');"/>');

        if (main_tab == 'panel2' && main_srch!=null && main_srch!= 'undefined')
                document.SearchHedform.tkr.value = main_srch;

        document.write('<span class="searchbutton"><a href="javascript:document.SearchHedform.submit()" onclick="return submitTkr(\'SearchHedform\');">Stock Quote</a></span>');
        document.write('<div style="clear:both;"></div>');
        document.write('</form>');
        document.write('</td>');
        document.write('</tr>');
}

var welcomeCookie = "undefined";
var frequency;
function firstHTML() {
        document.write('<div style="display:none;">');
        OAS_AD('x5');
        if(welcomeCookie != "undefined" && welcomeCookie != null) {
                if ((document.cookie.indexOf(welcomeCookie)==-1) && (navigator.userAgent.indexOf("Mozilla")>-1) && (navigator.userAgent.indexOf("iPad;")==-1) && (this.location.host.indexOf("forbes.com")!=-1) && (document.referrer.indexOf("digg.com")==-1) && (this.location.search.indexOf("partner=yahoo")==-1) && (this.location.search.indexOf("partner=msn")==-1)&& (this.location.search.indexOf("partner=aol")==-1) && (this.location.search.indexOf("partner=compuserve")==-1) && (this.location.search.indexOf("partner=netscape")==-1) && (this.location.search.indexOf('nowelcome')==-1) && (this.location.search.indexOf("partner=Experian-RMX")==-1) && (this.location.search.indexOf("partner=acurapower")==-1) && (this.location.search.indexOf("partner=rolex400")==-1) && (this.location.search.indexOf("partner=powercouples")==-1) && (this.location.href.indexOf("members.forbes.com/bizviz")==-1)) {
                        var tomorrow = new Date();
                        
                        if(frequency == "undefined" || frequency == null){
                                frequency = 24*60*60*1000;
                        }
                        var nowPlus =  tomorrow.getTime() + frequency;
                        tomorrow.setTime(nowPlus);
                        document.cookie = welcomeCookie +'__welcome'+tomorrow.getTime()+'; path=/; domain=.forbes.com; expires=' + tomorrow.toGMTString();

                        if (document.cookie.indexOf(welcomeCookie) != -1){
                                document.cookie="toURL"+ "=" +escape(document.URL)+";path=/; domain=.forbes.com; expires="+tomorrow.toGMTString();
                                this.location='http://www.forbes.com/fdc/welcome_mjx.shtml';
                        }
                }
        }

        //////// DC INTERSTITIAL HANDLING ////////

        if(getCookie("dcCookie") != null) {

                var dcTimes = parseInt(getCookie("dcCookie"));
                //alert(dcTimes);
                if (dcTimes % 3 == 0 && dcTimes <= 12) {
                        var tomorrow = new Date();
                        if(frequency == "undefined" || frequency == null){
                                frequency = 24*60*60*1000;
                        }
                        var nowPlus =  tomorrow.getTime() + frequency;
                        //var nowPlus =  tomorrow.getTime() + (24*60*60*1000);
                        tomorrow.setTime(nowPlus);
                        document.cookie="toURL"+ "=" +escape(document.URL)+";path=/; domain=.forbes.com; expires="+tomorrow.toGMTString();
                        currentTimes = parseInt(getCookie('dcCookie')) + 1;
                        setCookie('dcCookie', currentTimes, 1);
                        this.location='http://www.forbes.com/fdc/welcome_dc_mjx.shtml';
                }
        }

        document.write('<script type="text/javascript" src="http://ads.forbes.com/RealMedia/ads/adstream_jx.ads/forbes.com/geotarget/setcookie@x106?x"></script>');

        OAS_AD('x100');

        //////// DC INTERSTITIAL HANDLING ////////
        document.write('</div>');

		if (this.location.hostname != 'orgchart.forbes.com') { 
			if ((!thisURL.match(slideshowExpr)) && (!thisURL.match(slideshowExprSpecial)) && thisURL.indexOf("billionaire-homes-expensive-billionaires-2009-lifestyle-real-estate-homes") == -1 && thisURL.indexOf("thought-leaders") == -1 && displayedSection !="thought leaders") {
				attache();
				
			}
		}

        doOmniture();
        loadHeaderStyle();
        document.write('<table  border="0" cellpadding="0" cellspacing="0" width="100%" style="top: -20px;" bgcolor="#336699">');
        document.write('<tobody>');
        document.write('<tr>');
        document.write('<td colspan="2" height="15">');
        document.write('&nbsp;');
        document.write('<\/td>');
        document.write('<\/tr>');
        document.write('<tr>');
        if (this.location.href.indexOf("/video") >= 0 || ( typeof pageType != "undefined" && pageType == "search" )||this.location.host.indexOf("video.forbes.com")) {
                document.write('<td bgcolor="#336699"><div id ="bigBannerDiv">');
        } else {
                document.write('<td bgcolor="#336699"><div id ="bigBannerDiv" style="height: 90px">');
        }
        document.write('<table border="0" cellpadding="0" cellspacing="0" width="780" >');
        document.write('<tr>');
        document.write('<td align="center">');
}

function secondHTML(){
        document.write('</td>');
        document.write('</tr>');
        document.write('</tbody></table></div>');

        document.write('<table border="0" cellpadding="0" cellspacing="0" width="780">');
        document.write('<tbody>');
        document.write('<tr>');
        document.write('<td colspan="5" height="15">&nbsp;</td>');
        document.write('</tr>');
        document.write('<tr>');
        document.write('<!-- Modified table structure for search -->');
        document.write('<td rowspan="3" width="10"> </td>');
        document.write('<td rowspan="2" width="160">');
        document.write('<a href="http://www.forbes.com"><img id="forbesLogo" src="http://images.forbes.com/media/assets/forbes_home_logo.gif" border="0" vspace="0" width="150" height="49" hspace="0"></a>');
        document.write('</td>');
        document.write('<td rowspan="3" width="30"> </td>');
        document.write('<td class="newtagline" style="font:bold 13px Arial,Helvetica,sans-serif; color:#cde;">Home Page for the World\'s Business Leaders</td>');


        document.write('<td style="font:bold 13px Arial,Helvetica,sans-serif; color:#fff;" align="right">');
        document.write('<a id="trialText" href="https://w1.buysub.com/servlet/OrdersGateway?cds_mag_code=FRB&amp;cds_response_key=IMHFT012" class="whitelink">Free Trial Issue</a>&nbsp;');
        document.write('<td rowspan="3" width="30"> </td>');
        document.write('</tr>');

        showSearchBox();

        document.write('<tr valign="top">');
        lightHomepagelink("isNotSlide"); //highlight homaepage link;
        if (window.ibmpink && ibmpink) {
                document.write('<td colspan="2" id="freetrialtxt"></td>');
                addPinkTab(document.getElementById("freetrialtxt"));
        }

        document.write('</tr>');
        document.write('</tbody>');
        document.write('</table>');
        document.write('</td>');

        if (thisURL.match(slideshowExpr) && typeof fdc_slidenew != undefined) {
                document.write('<td width="100%" valign="top">');
        } else if (thisURL.match(slideshowExpr)) {
                document.write('<td width="100%" valign="bottom">');
        } else {
                document.write('<td width="100%" valign="top">');
        }

		isForbesPartner = (window.partner && ((partner.indexOf("aol")>-1) || (partner.indexOf("msn")>-1) || (partner.indexOf("b365")>-1)));
        
		loadMakeHomePage();

        document.write('</tr>');
        document.write('</tbody>');
        document.write('</table>');
        document.write('</tbody>');
        document.write('</table>');
        document.write('</td>');
        document.write('</tr>');
        document.write('</table>');

        loadNavBg();

		if(isForbesPartner) {
			if(document.getElementById('attacheContain') && attacheTopIsSet != true) {
				setAttacheTop();
			}
		}
        document.write('<br>');
        document.write('<div id="removeCentAds">');
}

function thirdHTML() {
        document.write('</div>');

        var centAds = document.getElementById('removeCentAds');
        if (centAds != null && centAds != 'undefined'){
                var centAds_parent = centAds.parentNode;
                if (centAds_parent != null && centAds_parent != 'undefined')
                        centAds_parent.removeChild(centAds);
        }
}

function doMembersHeader(member, links) {
	document.write(''+member+'<div id="utilityLinks">\n'+links+'</div>\n');
	if(navigator.appName == "Microsoft Internet Explorer")
		document.write('<h4 id="homePage"><a href="#" onClick="doOmnitureTracking(\'topMakeHome\',this, \'forbescom\');this.style.behavior=\'url(#default#homepage)\';this.setHomePage(\'http://www.forbes.com\');" >Make Forbes.com My Home Page</a></h4>\n<h4 id="bookmarkPage"><a onClick=\"doOmnitureTracking(\'topBookThis\',this, \'forbescom\');window.external.AddFavorite(\'http://www.forbes.com/\',\'Business News and Financial News at Forbes.com\');\" href=\"#\">Bookmark This Page</a></h4>\n');
       	
       document.write('</div>');
}

var partners = 0;
function doPartners() {
	partners = 1;	
	if (document.cookie.indexOf('partner_session=aolre') != -1) {
		partner = 'aolre';
   		document.write('<\/div><script src=\"http://www.aolcdn.com/realestate/hat.js\" type=\"text/javascript\"></script>');
	}
	else if (document.cookie.indexOf('partner_session=aolautos') != -1) {
		partner = 'aolautos';
   		document.write('<\/div><div style="height:14px;background-color: #99aabb;width:100%">&nbsp;<\/div><script src=\"http://cdn.channel.aol.com/ch_autos/autoshat.js\" type=\"text/javascript\"></script>');
	}
	else if (document.cookie.indexOf('partner_session=aol') != -1 && document.cookie.indexOf('partner_session=aoltravel') == -1 && document.cookie.indexOf('partner_session=aolhome') == -1 && document.cookie.indexOf('partner_session=aolhealth') == -1 && document.cookie.indexOf('partner_session=aolfood') == -1) {
		partner = 'aol';
   		document.write('<\/div><div style="background-color:#226633;width:100%;height:10px;">&nbsp;<\/div><script src=\"http://cdn.channel.aol.com/_media/ch_pf/partner_pf_hat.js\" script type=\"text/javascript\"></script>');
	}
	else if (document.cookie.indexOf('partner_session=msnbc') != -1) {
		partner = 'msnbc';
   		document.write('<\/div><script src=\"http://images.forbes.com/media/partners/msnbc/partner_msnbc_hat.js\" script type=\"text/javascript\"></script>');
	}
	else if (document.cookie.indexOf('partner_session=msnuktravel') != -1) {
		partner = 'msnuktravel';
   		document.write('<\/div><script src=\"http://images.forbes.com/media/partners/msn/partner_msnuktravel_hat.js\" script type=\"text/javascript\"></script>');
	}
        else if (document.cookie.indexOf('partner_session=msnit') != -1) {
                partner = 'msnit';
                document.write('<\/div><script src=\"http://images.forbes.com/media/partners/msn/partner_msnit_hat.js\" script type=\"text/javascript\"></script>');
        }
	else if (document.cookie.indexOf('partner_session=msnuk') != -1) {
		partner = 'msnuk';
   		document.write('<\/div><script src=\"http://images.forbes.com/media/partners/msn/partner_msnuk_hat.js\" script type=\"text/javascript\"></script>');
	}
	else if (document.cookie.indexOf('partner_session=msnent') != -1) {
		partner = 'msnent';
   		document.write('<\/div><script src=\"http://images.forbes.com/media/partners/msn/partner_msnent_hat.js\" script type=\"text/javascript\"></script>');
	}
	else if (document.cookie.indexOf('partner_session=msnre') != -1) {
		partner = 'msnre';
   		document.write('<\/div><script src=\"http://images.forbes.com/media/partners/msn/partner_msnre_hat.js\" script type=\"text/javascript\"></script>');
	}
	else if (document.cookie.indexOf('partner_session=msnarabia') != -1) {
		partner = 'msnarabia';
   		document.write('<\/div><script src=\"http://images.forbes.com/media/partners/msn/partner_msnarabia_hat.js\" script type=\"text/javascript\"></script>');
	}
	else if (document.cookie.indexOf('partner_session=msntravel') != -1) {
		partner = 'msntravel';
   		document.write('<\/div><script src=\"http://images.forbes.com/media/partners/msn/partner_msntravel_hat.js\" script type=\"text/javascript\"></script>');
	}
	else if (document.cookie.indexOf('partner_session=msnhealth') != -1) {
		partner = 'msnhealth';
   		document.write('<\/div><script src=\"http://images.forbes.com/media/partners/msn/partner_msnhealth_hat.js\" script type=\"text/javascript\"></script>');
	}
	else if (document.cookie.indexOf('partner_session=msnedit') != -1) {
		partner = 'msnedit';
   		document.write('<\/div><script src=\"http://images.forbes.com/media/partners/msn/partner_msn_hat.js\" script type=\"text/javascript\"></script>');
	}
	else if (document.cookie.indexOf('partner_session=msncanadafr') != -1) {
		partner = 'msncanadafr';
   		document.write('<\/div><script src=\"http://images.forbes.com/media/partners/msn/partner_msncanadafr_hat.js\" script type=\"text/javascript\"></script>');
	}
	else if (document.cookie.indexOf('partner_session=msncityguides') != -1) {
		partner = 'msncityguides';
   		document.write('<\/div><script src=\"http://images.forbes.com/media/partners/msn/partner_msncityguides_hat.js\" script type=\"text/javascript\"></script>');
	}
	else if (document.cookie.indexOf('partner_session=msn') != -1) {
		var no_msn = document.cookie.search(/msn/);
		var chk1_msn = document.cookie.substr(no_msn,4);
 		if( chk1_msn == "msn" || chk1_msn =="msn;"){
			partner = 'msn';
			document.write('<\/div><script src=\"http://images.forbes.com/media/partners/msn/partner_msn_hat.js\" script type=\"text/javascript\"></script>');
		}else{
			document.write('<\/div><script src=\"http://images.forbes.com/media/partners/msn/partner_msn_hat_old.js\" script type=\"text/javascript\"></script>');
		}
        }
	else if (document.cookie.indexOf('partner_session=b365') != -1) {
		partner = 'b365';
   		document.write('<\/div><script src=\"http://images.forbes.com/media/partners/b365/partner_b365_hat.js\" script type=\"text/javascript\"></script>');
	}
	else if (document.cookie.indexOf('partner_session=ukaol') != -1) {
		partner = 'ukaol';
   		document.write('<\/div><script src=\"http://images.forbes.com/media/partners/aol/partner_ukaol_hat.js\" script type=\"text/javascript\"></script>');
	}
	else if (document.cookie.indexOf('partner_session=yahoode') != -1) {
		partner = 'ukaol';
   		document.write('<\/div><script src=\"http://images.forbes.com/media/partners/yahoo/partner_yahoog_hat.js\" script type=\"text/javascript\"></script>');
	}
	else partners=0;
}
function doOmniture() {
	document.write('<div style="display:none;">');

	if(typeof hpType != "undefined" && hpType == "us") OAS_AD('x110');
	OAS_AD('AdController');
	if(this.location.hostname != 'orgchart.forbes.com') {
		//omniture
		document.write('<span id="omniture" style="height:1px"><script language="JavaScript" src="http://images.forbes.com/scripts/omniture/s_code_forbescom.js"><' + '\/script><\/span>');
		//Omniture Code for Northwestern Mutual for Fact and Comments Pages
		if ( typeof displayedSection != "undefined" && displayedSection == "fact and comment" && typeof displayedChannel != "undefined" && displayedChannel == "opinions") {
		 document.write("<scr" + "ipt language='JavaScri" + "pt' src='http://images.forbes.com/northwesternmutual/s_code.js'></scr" + "ipt>");
		 document.write("<scr" + "ipt language='JavaScri" + "pt' src='http://images.forbes.com/northwesternmutual/sc_custom.js'></scr" + "ipt>");
		}
		//Omniture Code for Northwestern Mutual - Till here
	}
	document.write('<\/div><\/div>');
}


var isSlidePage = false;
function doSlide1() {
	OAS_AD('x50');
	OAS_AD('x91');
        document.write('<script type="text/javascript" src="http://images.forbes.com/scripts/jquery/jquery.js"></script>');
        document.write('<script src="http://images.forbes.com/scripts/acs/thickbox.js" type="text/javascript"></script>');
        document.write('<link rel="stylesheet" href="http://images.forbes.com/css/story/storyStyle_center.css" type="text/css"/>');
        document.write('<link rel="stylesheet" href="http://images.forbes.com/css/story/thickbox.css" type="text/css" media="screen"/>');
	firstHTML();
}
function doSlide2() {
	secondHTML();
	thirdHTML();
	document.write('<div class="sponsorSlide"><script language="JavaScript">OAS_AD("StoryLogo");</script><\/div>');
}

function adjustSlide() {
	if (nonav) {
		if (!partners) {
			if(document.getElementById('createAlerts')) document.getElementById('createAlerts').style.top = '147px';
			if(document.getElementById('dynamicAdWinDiv')) document.getElementById('dynamicAdWinDiv').style.top = '109px';
                        if(document.getElementById('dynamicAdColDiv')) document.getElementById('dynamicAdColDiv').style.top = '109px';
		} else {
			if(document.getElementById('createAlerts')) document.getElementById('createAlerts').style.top = '187px';
			if(document.getElementById('dynamicAdWinDiv')) document.getElementById('dynamicAdWinDiv').style.top = '149px';
			if(document.getElementById('dynamicAdColDiv')) document.getElementById('dynamicAdColDiv').style.top = '149px';
		}
	} else {
		if (!partners) {
			if(document.getElementById('createAlerts')) document.getElementById('createAlerts').style.top = '201px';
			if(document.getElementById('dynamicAdWinDiv')) document.getElementById('dynamicAdWinDiv').style.top = '292px';
			if(document.getElementById('dynamicAdColDiv')) document.getElementById('dynamicAdColDiv').style.top = '292px';
		} else {
			if(document.getElementById('createAlerts')) document.getElementById('createAlerts').style.top = '241px';
			if(document.getElementById('dynamicAdWinDiv')) document.getElementById('dynamicAdWinDiv').style.top = '332px';
			if(document.getElementById('dynamicAdColDiv')) document.getElementById('dynamicAdColDiv').style.top = '332px';
		}
	}
}
doPartners();

function deleteCookie( name, path, domain ) {
	if (document.cookie.indexOf( name ) ) document.cookie = name + "=" +
	( ( path ) ? ";path=" + path : "") +
	( ( domain ) ? ";domain=" + domain : "" ) +
	";expires=Thu, 01-Jan-1970 00:00:01 GMT";
}
//var slideshowExpr =/_slide\d*_?\d*\.html/;
var slideshowExpr =/(-slide|_slide).?\d*_?\d*_?\d*\.html/;
var slideshowExprSpecial = /_slideshow.?\d*_?\d*_?\d*\.html/;
var globeStoryType = "non-slide";

if (thisURL.match(slideshowExpr)) {
		globeStoryType = "slide";
	}else{
		if (pageType !="home" && displayedSection !="Video"){
		deleteCookie('slideGov','\/','.forbes.com');
	}
}

function doOmnitureTracking(trackee, obj,account){
	s_linkTrackVars='prop18';s_linkType='o';s_linkName=trackee;if(typeof(globalPageName)!='undefined')s_prop18=globalPageName;s_lnk=s_co(obj);s_gs(account);
}

var typeahead_main_keys = [];

function formSubmitted() {
	if (thisURL.indexOf("blogs.forbes.com") > -1) {
		if (getCookie('search_term') == null)
			setCookieBlogs('search_term',main_srch);
		if (getCookie('tab_val') == null)
			setCookieBlogs('tab_val',main_tab);
	}
	else {
		setCookie('search_term',main_srch);
		setCookie('tab_val',main_tab);
	}
	return ;
}

if (this.location.host.indexOf("video.forbes.com")>-1){
        noattache = 1;
}
try {
        window.addEventListener( "load", loadScriptsForSearch, true );
} catch( e ) {
        try {
                window.attachEvent( "onload", loadScriptsForSearch );
        } catch( e ) {}
}


function callJQuery(name) {
	// called from searchtabs-xhtml
}

function fdcComputeGlobalPageName(path){
        url = '' + window.location;
        start = url.indexOf('//') + 2;
        start = url.indexOf('/',start) + 1;
        end = url.indexOf('?'); if(end==-1){end=url.length}
        if(typeof path != "undefined") { globalPageName = path + url.substring(start, end); }
        else { globalPageName = url.substring(start, end); }
        globalPageName = globalPageName.replace(/\//g,":");
}
if (this.location.host.indexOf("video.forbes.com")>-1){
        noattache = 1;
}

function matchKeywordsForTypeAhead(searchterm) {
        var total_keywords = typeahead_keys.length;
        var keywords = [];
        var keyword = '';
        var unduped = new Object;
        searchterm = searchterm.replace(/(\.|\+|\*|\[|\]|\(|\)+)/g, '\\'+'$1' );
        var matchregex= new RegExp('^'+searchterm,'i');
        for (var count=0; count<total_keywords; count++) {
                keyword = typeahead_keys[count];
                if (searchterm != '' && keyword.search(matchregex) != -1 ) {
                        keyword = keyword.replace(/(^\s*)|(\s*$)/gi,'');
                        keyword = keyword.replace(/(\s)+/g, ' ');
                        unduped[keyword] = keyword;
                }
        }
        for (var k in unduped) {
                if(keywords.length < 10 )
                        keywords.push(unduped[k]);
        }

        return keywords;
}

function termChanged(element,tabSelect) {
        main_srch=element ;
        main_tab=tabSelect;

        if(typeAheadFlag == false){
                callTypeAheadJs();
                typeAheadFlag = true;
        }

        search_panel = document.getElementById('search_' + tabSelect);
        if(main_tab=='panel2') {
                is_quote=true;
        }else {
                is_quote=false;
        }
        initTypeaheadPanel(search_panel);
        return;
}

function initTypeaheadPanel() {
                if ((window.jQuery != null) && (window.jQuery.dimensions != null) && (window.jQuery.suggest != null)){
                        if (search_panel.getAttribute('check') != 'true') {
                                jQuery(search_panel).suggest(" ",{ onSelect: function() {}});
                                search_panel.setAttribute('check','true');
                        }else {
                                setTimeout( 'initTypeaheadPanel()', 5);
                        }
                }
}

