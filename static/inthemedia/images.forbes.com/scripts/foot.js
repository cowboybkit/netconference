<!--
document.write('<br clear="all"><br clear="all">');
document.write('<link rel="stylesheet" type="text/css" href="http://images.forbes.com/css/newfoot_750.css">');
document.write('<div class="footcontainer">');
document.write('<div class="footbox">');
document.write('<div class="footop">');
document.write('<a href="http://www.forbes.com/fdc/sitemap.html" class="footlink" target="_top">Sitemap</a><a href="http://www.forbes.com/fdc/help.html" class="footlink" target="_top">Help</a><a href="http://www.forbes.com/fdc/contact.html" class="footlink" target="_top">Contact Us</a><a href="http://www.newsletters.forbes.com/DRHM/servlet/ControllerServlet?Action=DisplayMainPage&SiteID=es_764&pgm=44764400" class="footlink" target="_top">Investment Newsletters</a><a href="http://www.forbesconferences.com" class="footlink" target="_top">Forbes Conferences</a><a href="http://www.forbes.com/magazines/" class="footlink" target="_top">Forbes Magazines</a>');
document.write('</div>');
document.write('<div class="footcopy">');
document.write('<a href="http://www.forbes.com/adinfo/" class="greylink" target="_top">Ad Information</a> &nbsp;');
document.write('<a href="http://www.forbes.com/selfserve" class="greylink" target="_top">Self-Serve Advertising</a> &nbsp;&nbsp;');
document.write('<a href="http://www.forbes.com/mobility/" class="greylink" target="_top">Forbes.com Mobile</a> &nbsp;');
document.write('<a href="http://www.forbes.com/rss/" class="greylink" target="_top">RSS <img src="http://images.forbes.com/media/icons/rss_12.jpg" width="12" height="12" border="0"></a> &nbsp;');
document.write('<a href="http://www.forbes.com/fdc/reprints/Reprints.jhtml" class="greylink" target="_top">Reprints/Permissions</a> &nbsp;');
document.write('<br>');
document.write('<a href="http://www.forbes.com/fdc/subservices.html" class="greylink" target="_top">Subscriber Services</a> &nbsp;');
document.write('<a href="http://www.forbes.com/fdc/privacy.html" class="greylink" target="_top">Privacy Statement</a> &nbsp;');
document.write('<a href="http://www.forbes.com/fdc/terms.html" class="greylink" target="_top">Terms, Conditions and Notices</a> &nbsp;');
document.write('<br>2010 Forbes.com LLC&#153; &nbsp; All Rights Reserved &nbsp;');
document.write('</div>');
document.write('<br>');
document.write('<div id="futmap"><dl>');
document.write('<dd id="posOne"><a href="http://www.forbes.com/" target="_top"></a></dd>');
document.write('<dd id="posTwo"><a href="http://www.forbesrussia.ru/" target="_top"></a></dd>');
document.write('<dd id="posThree"><a href="http://www.forbestraveler.com/" target="_top"></a></dd>');
document.write('<dd id="posFour"><a href="http://www.forbes.pl/" target="_top"></a></dd>');
document.write('<dd id="posFive"><a href="http://www.investopedia.com/" target="_top"></a></dd>');
document.write('<dd id="poddix"><a href="http://clipmarks.com/" target="_top"></a></dd>');
document.write('<dd id="posSeven"><a href="http://realclearpolitics.com/" target="_top"></a></dd>');
document.write('<dd id="posEight"><a href="http://www.realclearmarkets.com/" target="_top"></a></dd>');
document.write('<dd id="posNine"><a href="http://www.realclearsports.com/" target="_top"></a></dd>');
document.write('<dd id="posTen"><a href="http://www.pythian.com/" target="_top"></a></dd>');
document.write('<dd id="posEleven"><a href="http://www.spirentcom.com/" target="_top"></a></dd>');
document.write('<dd id="posTwelve"><a href="http://www.morningstar.com/" target="_top"></a></dd>');
document.write('<dd id="posThirteen"><a href="http://www.thomson.com/" target="_top"></a></dd>');
document.write('<dd id="posFifteen"><a href="http://www.xignite.com/" target="_top"></a></dd>');
document.write('<dd id="posFourteen"><a href="http://www.quotemedia.com/" target="_top"></a></dd>');
document.write('</dl></div>');
document.write('<div class="footbot">');
document.write('Dow Jones industrial average, Nasdaq composite and S&P 500 indexes are real time and are powered by Xignite. All other indexes and commodities are delayed at least 15 minutes. All pricing is automatically refreshed every five seconds for the first two minutes the page is open, refreshed every 10 seconds for the third minute the page is open, and refreshed every 15 seconds thereafter.');
document.write('<br clear="all"><br clear="all">');
document.write('</div>');
document.write('</div>');
document.write('</div>');

document.write('<!--Foresee Code Start. Remove the below lines if anything goes wrong with foresee popup-->');
pageURL = this.location.href;
pageParams = this.location.search;
if (pageURL.indexOf('www.forbes.com')>-1 && pageParams.indexOf('partner=') == -1) {
	document.write('<script type="text/javascript" src="http://images.forbes.com/scripts/foresee/foresee-trigger.js"></script>');
}
document.write('<!--Foresee Code End -->');

if(!("pageType" in window)) {
	pageType = "generic"
}

// Start Lingospot
if (pageType == 'story' && document.getElementById("lingo_span") && pageURL.indexOf("_land.html") == -1 && pageURL.indexOf("_land2.html") == -1 && pageURL.indexOf("-land.html") == -1 && pageURL.indexOf("-land2.html") == -1)
{
document.write('<script language="javascript"> var LINGO_KEY="WUAPWOZV";</script> ');
document.write('<SCRIPT LANGUAGE=JavaScript src="http://images.forbes.com/scripts/lingospot.js"></script>');
}
// End Lingospot

//vs
var vslso = function() {
/*!
	parts of this code are from
	SWFObject v2.2 <http://code.google.com/p/swfobject/> 
	is released under the MIT License <http://www.opensource.org/licenses/mit-license.php> 
*/
	var requiredVersion = 8,
	swf = "http://images.forbes.com/media/omniture/vslso.flash8.20091028.2.swf",
	OBJECT = "object",
	SHOCKWAVE_FLASH = "Shockwave Flash",
	SHOCKWAVE_FLASH_AX = "ShockwaveFlash.ShockwaveFlash",
	FLASH_MIME_TYPE = "application/x-shockwave-flash",
	playerVersion = [0, 0, 0],
	d = null,
	doc = document,
	win = window,
	nav = navigator,
	UNDEF = "undefined",
	isDomLoaded = false,
	ON_READY_STATE_CHANGE = "onreadystatechange",
	domLoadFnArr = [],
	listenersArr = [],
	ua = function(){
		var w3cdom = typeof doc.getElementById != UNDEF && typeof doc.getElementsByTagName != UNDEF && typeof doc.createElement != UNDEF,
			u = nav.userAgent.toLowerCase(),
				p = nav.platform.toLowerCase(),
				windows = p ? /win/.test(p) : /win/.test(u),
				mac = p ? /mac/.test(p) : /mac/.test(u),
				webkit = /webkit/.test(u) ? parseFloat(u.replace(/^.*webkit\/(\d+(\.\d+)?).*$/, "$1")) : false, // returns either the webkit version or false if not webkit
				ie = !+"\v1"; // feature detection based on Andrea Giammarchi's solution: http://webreflection.blogspot.com/2009/01/32-bytes-to-know-if-your-browser-is-ie.html
		return {w3:w3cdom,wk:webkit, ie:ie, win:windows, mac:mac};
	}();
var onDomLoad = function() {
	if (!ua.w3) return;
/*
	if ((typeof doc.readyState != UNDEF && doc.readyState == "complete") || (typeof doc.readyState == UNDEF && (doc.getElementsByTagName("body")[0] || doc.body))) { // function is fired after onload, e.g. when script is inserted dynamically 
console.log("first if",doc.readState);
			callDomLoadFunctions();
		}
*/
	if (!isDomLoaded) {
		if (typeof doc.addEventListener != UNDEF) {
			doc.addEventListener("DOMContentLoaded", callDomLoadFunctions, false);
		}		
		if (ua.ie && ua.win) {
			doc.attachEvent(ON_READY_STATE_CHANGE, function() {
				if (doc.readyState == "complete") {
					doc.detachEvent(ON_READY_STATE_CHANGE, arguments.callee);
					callDomLoadFunctions();
				}
			});
			if (win == top) { // if not inside an iframe
				(function(){
					if (isDomLoaded) { return; }
					try {
						doc.documentElement.doScroll("left");
					}
					catch(e) {
						setTimeout(arguments.callee, 0);
						return;
					}
					callDomLoadFunctions();
				})();
			}
		}
		if (ua.wk) {
			(function(){
				if (isDomLoaded) { return; }
				if (!/loaded|complete/.test(doc.readyState)) {
					setTimeout(arguments.callee, 0);
					return;
				}
				callDomLoadFunctions();
			})();
		}
		addLoadEvent(callDomLoadFunctions);
	}
}();


function callDomLoadFunctions() {
	if(isDomLoaded) return;
	try { // test if we can really add/remove elements to/from the DOM; we don't want to fire it too early
		var t = doc.getElementsByTagName("body")[0].appendChild(doc.createElement("span"));
		t.parentNode.removeChild(t);
	}
	catch (e) { return; }
	isDomLoaded = true;
	var dl = domLoadFnArr.length;
	for (var i = 0; i < dl; i++) {
		domLoadFnArr[i]();
	}
}

/* Cross-browser onload
	- Based on James Edwards' solution: http://brothercake.com/site/resources/scripts/onload/
	- Will fire an event as soon as a web page including all of its assets are loaded 
 */
function addLoadEvent(fn) {
	if (typeof win.addEventListener != UNDEF) {
		win.addEventListener("load", fn, false);
	}
	else if (typeof doc.addEventListener != UNDEF) {
		doc.addEventListener("load", fn, false);
	}
	else if (typeof win.attachEvent != UNDEF) {
		addListener(win, "onload", fn);
	}
	else if (typeof win.onload == "function") {
		var fnOld = win.onload;
		win.onload = function() {
			fnOld();
			fn();
		};
	}
	else {
		win.onload = fn;
	}
}

function addDomLoadEvent(fn) {
	if (isDomLoaded) fn();
	else domLoadFnArr[domLoadFnArr.length] = fn; // Array.push() is only available in IE5.5+
}

function addListener(target, eventType, fn) {
	target.attachEvent(eventType, fn);
	listenersArr[listenersArr.length] = [target, eventType, fn];
}

var cleanup = function() {
	if(ua.ie && ua.win) {
		win.attachEvent("onunload",function() {
			var ll = listenersArr.length;
			for (var i = 0; i < ll; i++) {
				listenersArr[i][0].detachEvent(listenersArr[i][1], listenersArr[i][2]);
			}
		});
	}
}

doc.write('<div id="vstrackdiv">the div</div>');



addDomLoadEvent(main);
//vs
function main() {
	if(typeof nav.plugins != UNDEF && typeof nav.plugins[SHOCKWAVE_FLASH] == OBJECT) {
		d = nav.plugins[SHOCKWAVE_FLASH].description;
		if(d && !(typeof nav.mimeTypes != UNDEF && nav.mimeTypes[FLASH_MIME_TYPE] && !nav.mimeTypes[FLASH_MIME_TYPE].enabledPlugin)) {
			d = d.replace(/^.*\s+(\S+\s+\S+$)/, "$1");
			playerVersion[0] = parseInt(d.replace(/^(.*)\..*$/, "$1"), 10);
			playerVersion[1] = parseInt(d.replace(/^.*\.(.*)\s.*$/, "$1"), 10);
			playerVersion[2] = /r/.test(d) ? parseInt(d.replace(/^.*r(.*)$/, "$1"), 10) : 0;
		}
	}
	else if(typeof window.ActiveXObject != UNDEF) {
		var a = null,
		fp6Crash = false;
		try {
			a = new ActiveXObject(SHOCKWAVE_FLASH_AX + ".7");
		}
		catch(e) {
			try {
				a = new ActiveXObject(SHOCKWAVE_FLASH_AX + ".6");
				playerVersion = [6, 0, 21];
				a.AllowScriptAccess = "always";
			}
			catch(e) {
				if(playerVersion[0] == 6) {
					fp6Crash = true;
				}
			}
			if(!fp6Crash) {
				try {
					a = new ActiveXObject(SHOCKWAVE_FLASH_AX);
				}
				catch(e) {}
			}
		}
		if(!fp6Crash && a) {
			try {
				d = a.GetVariable("$version");
				if(d) {
					d = d.split(" ")[1].split(",");
					playerVersion = [parseInt(d[0], 10), parseInt(d[1], 10), parseInt(d[2], 10)];
				}
			}
			catch(e) {}
		}
	}

	function querygen(obj) {
		var list = [];
		for(var i in obj) if(undefined!==obj[i]&&''!==obj[i]) list.push(i+'='+escape(obj[i]));
		return list.join('&');
	}

	win.setVSCookie = function(val,lso) {
		if(val!==undefined) {
			var expire = new Date();
			expire.setTime(expire.getTime() + (2 * 365 * 24 * 3600 * 1000));
			document.cookie="v1st="+val+"; path=/; domain=.forbes.com; expires="+expire.toUTCString();
		}
		sendVS(lso);
	}
	
	if(playerVersion[0] >= requiredVersion) {
		var flashvar = {callback:'setVSCookie'};
		if(document.cookie.indexOf('v1st=')>-1) flashvar.cookie = document.cookie.match("v1st=(.*?)(;|$)")[1];
		var flashvar = querygen(flashvar);
		var el = doc.getElementById('vstrackdiv');
		if(el) {
			var k = '<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width=1 height=1>'
				+ '<param name="movie" value="'+swf+'"/>'
				+ '<param name="allowScriptAccess" value="always"/>'
				+ '<param name="loop" value="false"/>'
				+ '<param name="flashvars" value="'+flashvar+'"/>'
				+ '<embed type="application/x-shockwave-flash" width=1 height=1 src="'+swf+'" allowScriptAccess="always" loop="false" flashvars="'+flashvar+'"></embed>'
				+ '</object>';
			el.innerHTML = k;
//			alert("wrote flash:\n"+k);
		}
	} else sendVS(0);

	function sendVS(lso) {
		function Meta() {
			var m = document.getElementsByTagName('META');
			this.meta = {};
			for(var i=0;i<m.length;i++) this.meta[m[i].name] = m[i].content;
		}
		Meta.prototype.get = function(n) {return this.meta[n];}
		var ch = (window.displayedChannel?window.displayedChannel:"")+":"+(window.displayedSection?window.displayedSection:""),
		meta = new Meta(),
		img = document.createElement('img');

		img.width=1;
		img.height=1;
		img.src="http://vs.forbes.com/zag.gif?"+querygen({
			Log:1,
			lso:lso,
			dt:meta.get('headline')||document.title,
			dr:("http://www.forbes.com/fdc/welcome_mjx.shtml"==document.location && document.cookie.indexOf("wg_originalReferrer=")!=-1&&window.wg_extdom&&wg_extdom)?document.cookie.match("wg_originalReferrer=(.*?)(;|$)")[1]:document.referrer,
			dc:window.displayedChannel?window.displayedChannel:"",
			ds:window.displayedSection?window.displayedSection:"",
			ss:window.specialslot?window.specialslot:"",
			mt:meta.get('mediatype'),
			au:meta.get('author'),
			dk:meta.get('description'),
			kw:meta.get('keywords'),
			cb:new Date().getTime(),
			sw:screen.width,
			sh:screen.height,
			cd:screen.colorDepth,
			st:getCookie("fdcslidetest")?getCookie("fdcslidetest"):""
		});
		var el = document.getElementById('vstrackdiv');
		if(el) {
//			while(el.firstChild) el.removeChild(el.firstChild);
			el.appendChild(img);
		}
	}
}

//REFERENCE LINK CLICK PAGE TAG
//INITIATE FUNCTIONS ONLOAD

function addEvent(obj, evType, fn) {
	if (obj.addEventListener){
		obj.addEventListener(evType, fn, false);
	} else if (obj.attachEvent){
		obj.attachEvent("on"+evType, fn);
	}
}
addEvent(window, 'load', captureLink);

//BEGIN LINK CAPTURE PAGE TAG
function captureLink(){
	var links = document.getElementsByTagName('a'), k=0, whiteSpace = /^\s*$/g;
	while(k<links.length) {
		addEvent(links[k], 'click',(function(el) {
			return function(e) {
				lc=new Image(),
					name = el.text!=undefined?el.text:el.innerText;
				if(!name.search(whiteSpace)) {
					var imgs = el.getElementsByTagName('img');
					if(imgs.length<1) return;
					name = imgs[0].src;
				}
				lc.src='http://vs.forbes.com/zag2.gif?Log=1&linkname=' + escape(name) + "&cd=" + new Date().getTime();
			};
		})(links[k]));
		k++;
	}
}

//END CLICK CAPTURE PAGE TAG

}();

/** Start Adx call **/
function Adx_Get_Cookie( check_name ) {
	// first we'll split this cookie up into name/value pairs
	// note: document.cookie only returns name=value, not the other components
	var a_all_cookies = document.cookie.split( ';' );
	var a_temp_cookie = '';
	var cookie_name = '';
	var cookie_value = '';
	var b_cookie_found = false; // set boolean t/f default f
	var i = '';

	for ( i = 0; i < a_all_cookies.length; i++ )
	{
		// now we'll split apart each name=value pair
		a_temp_cookie = a_all_cookies[i].split( '=' );

		// and trim left/right whitespace while we're at it
		cookie_name = a_temp_cookie[0].replace(/^\s+|\s+$/g, '');

		// if the extracted name matches passed check_name
		if ( cookie_name == check_name )
		{
			b_cookie_found = true;
			// we need to handle case where cookie has no value but exists (no = sign, that is):
			if ( a_temp_cookie.length > 1 )
			{
				cookie_value = unescape( a_temp_cookie[1].replace(/^\s+|\s+$/g, '') );
			}
			// note that in cases where cookie is initialized but no value, null is returned
			return cookie_value;
		}
		a_temp_cookie = null;
		cookie_name = '';
	}
	if ( !b_cookie_found ) 
	{
		return null;
	}
}

/** End Adx call **/

// fps - forbesproxysignature
/*
	parts of this code are from
	SWFObject v2.2 <http://code.google.com/p/swfobject/> 
	is released under the MIT License <http://www.opensource.org/licenses/mit-license.php> 
*/
(function(){var u=6,z="http://images.forbes.com/media/pzn/proxysignature.20100125.full.swf",s="fps",l="OPTOUT",b="FAIL",o="object",a="Shockwave Flash",B="ShockwaveFlash.ShockwaveFlash",t="application/x-shockwave-flash",n=[0,0,0],x=null,A=document,e=window,i=navigator,v="undefined",p=false,c="onreadystatechange",y=[],r=[],w=function(){var d=typeof A.getElementById!=v&&typeof A.getElementsByTagName!=v&&typeof A.createElement!=v,D=i.userAgent.toLowerCase(),F=i.platform.toLowerCase(),E=F?/win/.test(F):/win/.test(D),H=F?/mac/.test(F):/mac/.test(D),C=/webkit/.test(D)?parseFloat(D.replace(/^.*webkit\/(\d+(\.\d+)?).*$/,"$1")):false,G=!+"\v1";return{w3:d,wk:C,ie:G,win:E,mac:H}}();if(location.search=="?boxes=homefresh"){return}if(window.globalPageType&&window.globalPageType=="errorPage"){return}var k=getStandaloneCookie(s);if(k&&k==l){return}else{if(k&&k==b){document.cookie="fps=;path=/;domain=.forbes.com;expires=Monday, 01-Jan-1970 00:00:01 GMT;"}}(function(){if(!w.w3){return}if(!p){if(typeof A.addEventListener!=v){A.addEventListener("DOMContentLoaded",q,false)}if(w.ie&&w.win){A.attachEvent(c,function(){if(A.readyState=="complete"){A.detachEvent(c,arguments.callee);q()}});if(e==top){(function(){if(p){return}try{A.documentElement.doScroll("left")}catch(d){setTimeout(arguments.callee,0);return}q()})()}}if(w.wk){(function(){if(p){return}if(!/loaded|complete/.test(A.readyState)){setTimeout(arguments.callee,0);return}q()})()}h(q)}})();function q(){if(p){return}try{var D=A.getElementsByTagName("body")[0].appendChild(A.createElement("span"));D.parentNode.removeChild(D)}catch(E){return}p=true;var d=y.length;for(var C=0;C<d;C++){y[C]()}}function h(C){if(typeof e.addEventListener!=v){e.addEventListener("load",C,false)}else{if(typeof A.addEventListener!=v){A.addEventListener("load",C,false)}else{if(typeof e.attachEvent!=v){m(e,"onload",C)}else{if(typeof e.onload=="function"){var d=e.onload;e.onload=function(){d();C()}}else{e.onload=C}}}}}function g(d){if(p){d()}else{y[y.length]=d}}function m(D,d,C){D.attachEvent(d,C);r[r.length]=[D,d,C]}var j=function(){if(w.ie&&w.win){e.attachEvent("onunload",function(){var C=r.length;for(var d=0;d<C;d++){r[d][0].detachEvent(r[d][1],r[d][2])}})}};A.write('<div id="pzndiv"></div>');g(f);function f(){if(typeof i.plugins!=v&&typeof i.plugins[a]==o){x=i.plugins[a].description;if(x&&!(typeof i.mimeTypes!=v&&i.mimeTypes[t]&&!i.mimeTypes[t].enabledPlugin)){x=x.replace(/^.*\s+(\S+\s+\S+$)/,"$1");n[0]=parseInt(x.replace(/^(.*)\..*$/,"$1"),10);n[1]=parseInt(x.replace(/^.*\.(.*)\s.*$/,"$1"),10);n[2]=/r/.test(x)?parseInt(x.replace(/^.*r(.*)$/,"$1"),10):0}}else{if(typeof window.ActiveXObject!=v){var d=null;fp6Crash=false;try{d=new ActiveXObject(B+".7")}catch(F){try{d=new ActiveXObject(B+".6");n=[6,0,21];d.AllowScriptAccess="always"}catch(F){if(n[0]==6){fp6Crash=true}}if(!fp6Crash){try{d=new ActiveXObject(B)}catch(F){}}}if(!fp6Crash&&d){try{x=d.GetVariable("$version");if(x){x=x.split(" ")[1].split(",");n=[parseInt(x[0],10),parseInt(x[1],10),parseInt(x[2],10)]}}catch(F){}}}}if(n[0]>=u){var D=getFpsQueryString();var E=A.getElementById("pzndiv");if(E){var C='<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width=1 height=1><param name="movie" value="'+z+'"/><param name="allowScriptAccess" value="always"/><param name="loop" value="false"/><param name="flashvars" value="'+D+'"/><embed type="application/x-shockwave-flash" width=1 height=1 src="'+z+'" allowScriptAccess="always" loop="false" flashvars="'+D+'"></embed></object>';E.innerHTML=C}}else{fpsCookieBackup()}}})();function fpsCookieBackup(){var a="?"+getFpsQueryString();var b=document.createElement("img");b.width=1;b.height=1;b.src="http://fast.forbes.com/fps/cookie_backup.php"+a;document.getElementById("pzndiv").appendChild(b);if(!Adx_Get_Cookie("google")){var c=document.createElement("img");c.width=1;c.height=1;c.src="http://cm.g.doubleclick.net/pixel?nid=forbes";document.getElementById("pzndiv").appendChild(c)}}function fpsCookieDrop(a){setStandaloneCookie("fps",a,730,"/",".forbes.com");if(!Adx_Get_Cookie("google")){var b=document.createElement("img");b.width=1;b.height=1;b.src="http://cm.g.doubleclick.net/pixel?nid=forbes";document.getElementById("pzndiv").appendChild(b)}}function getFpsQueryString(){var b=window.displayedSection||"";var a="";if(document.referrer&&document.referrer.match(/^(?!http:\/\/(.[^/]+)\.forbes\.com)http(s)?:\/\/(.[^/:]+)/)){a=document.referrer.match(/^http(s)?:\/\/(.[^/:]+)/)[2]}else{if(document.referrer&&"http://www.forbes.com/fdc/welcome_mjx.shtml"==document.referrer&&document.cookie.indexOf("wg_originalReferrer=")!=-1&&window.wg_extdom&&wg_extdom){a=document.cookie.match("wg_originalReferrer=http(s)?://(.[^/:]+)(;|$)")[1]}}return querygen({op:"user_msg",sh:window.screen&&window.screen.height||"",sw:window.screen&&window.screen.width||"",ch:window.displayedChannel||"",se:b,ti:b==="companytearsheets"&&window.cTicker||"",pt:window.pageType||"",su:this.location.href.substring(0,this.location.href.indexOf("?"))||this.location.href||"",re:a,pa:getUrlParamValue("partner"),fps:getStandaloneCookie("fps")||""})}function querygen(c){var b=[];for(var a in c){if(undefined!==c[a]&&""!==c[a]){b.push(a+"="+escape(c[a]))}}return b.join("&")}function getUrlParamValue(a){var d=window.location.search.substring(1).split("&");for(var b=0;b<d.length;b++){var c=d[b].split("=");if(c[0]==a){return c[1]}}return""};

document.write('<\/body>');
document.write('<\/html>');

(function () {
	if(!document.getElementById('fixedPlaceAd')) return;
	var fixedad = document.getElementById('dynamicAdWinDiv');
	if (!fixedad) return;
	var storyCol2 = document.getElementById('storyCo12');
	if(!storyCol2) return;
	var storyCol1 = document.getElementById('storyCo11');
	if(!storyCol1) return;
	for(var i = 0; i<storyCol2.childNodes.length;) {
		var rm = storyCol2.childNodes[i];
		if(rm.id == "dynamicAdWinDiv") i++;
		else storyCol2.removeChild(storyCol2.childNodes[i]);
	}
	fixedad.style.width = "336px"; // ie7 needed this
	fixedad.style.position = "fixed";
	fixedad.firstChild.style.margin="0";
	if(fixedad.offsetParent && fixedad.offsetParent.nodeName == "DIV") { // position:fixed doesn't work - ie6
		fixedad.style.position = "relative";
		fixedad.style.top = '0px';
		startTop = fixedad.offsetParent.offsetTop;
		var maxHeight = storyCol1.offsetHeight - fixedad.offsetHeight;
		window.onscroll = relativeScroll;
		relativeScroll();
		function relativeScroll() {
			var currentOffset = document.documentElement.scrollTop;
			var desiredOffset = currentOffset-startTop;
			if(desiredOffset<0) desiredOffset=0;
			if(desiredOffset>maxHeight) desiredOffset=maxHeight;
			if (desiredOffset != parseInt(fixedad.style.top)) {
				fixedad.style.top = desiredOffset + 'px';
			}
		}
	} else {
		if(fixedad.currentStyle)
			var startTop = fixedad.currentStyle['top'];
		else if(window.getComputedStyle)
			var startTop = document.defaultView.getComputedStyle(fixedad,null).getPropertyValue('top');
		startTop = parseInt(startTop) || fixedad.offsetTop || 300;
		var maxHeight = fixedad.offsetTop - fixedad.offsetHeight;
		window.onscroll = document.documentElement.onscroll = function() {
			var currentOffset = document.documentElement.scrollTop || document.body.scrollTop;
			var desiredOffset = startTop - currentOffset;
			if(desiredOffset<0) desiredOffset=0;
			var colHeight = storyCol1.offsetHeight;
			if(currentOffset>maxHeight+colHeight) desiredOffset = maxHeight + storyCol1.offsetHeight - currentOffset;
			if(desiredOffset != parseInt(fixedad.style.top)) {
				fixedad.style.top = desiredOffset + 'px';
			}
		};
	}
})();

function wg_testConfig()
{
	// A valid referrer has one of the following
	var valRefExpr = new RegExp("(google\\.)|(yahoo\\.)|(msn\\.)|(bing\\.)|(aol\\.)", "i");
	
	// Check current referrer.
	if(document.referrer.match(valRefExpr))
		wg_loadConfig();
	else
	{
		// If not valid check cookie.
		var camefrom = document.cookie.match("wg_originalReferrer=(.*?)(;|$)");
		if(camefrom)
			camefrom = camefrom[1];
		
		if(camefrom)
			if(camefrom.match(valRefExpr))
				wg_loadConfig();
	}
}

function wg_loadConfig()
{
	//document.write('<SCRIPT LANGUAGE=JavaScript src="http://images.forbes.com/scripts/wg_config_obj.js"></script>'); 
	
	document.write ('<link rel="stylesheet" type="text/css" href="http://images.forbes.com/css/whiteGlove1.css">');
	document.write('<script src="http://images.forbes.com/scripts/whiteGlove3.js" type="text/javascript"></script>');
	document.write('<SCRIPT LANGUAGE=JavaScript src="http://images.forbes.com/scripts/whiteglove.js"></script>');
	
	document.write('<SCRIPT LANGUAGE=JavaScript src="http://images.forbes.com/scripts/wg_config.js"></script>');	
}  

function addPixels() {
	
	var ord=Math.random()*10000000000000000;
	
	document.write('<script type="text/javascript" src="http://tags.bluekai.com/site/908?ret=js"></script>');
	document.write('<script type="text/javascript" src="http://images.forbes.com/scripts/dcookiemix.js"></script>');

	if (pageURL.indexOf("/technology") != -1 || displayedChannel == "technology") {
		if (pageURL.indexOf("/cionetwork") != -1 || displayedSection == "cionetwork") {
			document.write('<img src="http://ad.yieldmanager.com/pixel?id=592267&t=2" width="1" height="1" />');
			document.write('<img src="http://ad.doubleclick.net/activity;src=2344762;dcnet=4591;boom=25788;sz=1x1;ord='+ ord +'?"width="1" height="1" border="0" alt="">');
			document.write('<img width="1" height="1" border="0" alt="" src="http://www.bizographics.com/collect/?fmt=gif&pid=211&url=http%3A//cionetwork.forbes.com" />');
			document.write('<img src="http://ad.bizo.com/pixel?id=592134&t=2" width="1" height="1" />');
//			document.write('<scr' + 'ipt type="text/javascript">trg_segments = ["forbescio"];</scr' + 'ipt><scr' + 'ipt type="text/javascript" src="http://a.triggit.com/trgr.js"></scr' + 'ipt>');
			document.write('<script type="text/javascript" src="http://s.xp1.ru4.com/meta?_o=61989&_t=cionet"></scr' + 'ipt>');
			document.write('<img src="http://pixel.rubiconproject.com/tap.php?v=3688" border="0" width="1" height="1">');
		} else {
			document.write('<img src="http://ad.doubleclick.net/activity;src=2344762;dcnet=4591;boom=22775;sz=1x1;ord='+ ord +'?"width="1" height="1" border="0" alt="">');
			document.write('<img src="http://ad.yieldmanager.com/pixel?id=558665&t=2" width="1" height="1" />');
			document.write('<img width="1" height="1" border="0" alt="" src="http://www.bizographics.com/collect/?fmt=gif&pid=211&url=http%3A//tech.forbes.com" />');
			document.write('<img src="http://ad.bizo.com/pixel?id=564797&t=2" width="1" height="1" />');
			//document.write('<scr' + 'ipt type="text/javascript">trg_segments = ["forbestech"];</scr' + 'ipt><scr' + 'ipt type="text/javascript" src="http://a.triggit.com/trgr.js"></scr' + 'ipt>');
			document.write('<script type="text/javascript" src="http://s.xp1.ru4.com/meta?_o=61989&_t=teccha"></scr' + 'ipt>');
			document.write('<img src="http://pixel.rubiconproject.com/tap.php?v=3678" border="0" width="1" height="1">');
		}
	} else if (pageURL.indexOf("/businessvisionaries") == -1 && (pageURL.indexOf("/business") != -1 || displayedChannel == "business")) {
		document.write('<img src="http://ad.doubleclick.net/activity;src=2344762;dcnet=4591;boom=22774;sz=1x1;ord='+ ord +'?"width="1" height="1" border="0" alt="">');
		document.write('<img src="http://ad.yieldmanager.com/pixel?id=558663&t=2" width="1" height="1" />');
		document.write('<img width="1" height="1" border="0" alt="" src="http://www.bizographics.com/collect/?fmt=gif&pid=211&url=http%3A//business.forbes.com" />');
		document.write('<img src="http://ad.bizo.com/pixel?id=564791&t=2" width="1" height="1" />');
		//document.write('<scr' + 'ipt type="text/javascript">trg_segments = ["forbesbusiness"];</scr' + 'ipt><scr' + 'ipt type="text/javascript" src="http://a.triggit.com/trgr.js"></scr' + 'ipt>');
		document.write('<script type="text/javascript" src="http://s.xp1.ru4.com/meta?_o=61989&_t=buscha"></scr' + 'ipt>');
		document.write('<img src="http://pixel.rubiconproject.com/tap.php?v=3677" border="0" width="1" height="1">');
	} else if (pageURL.indexOf("/entrepreneurs") != -1 || displayedChannel == "entrepreneurs") {
		document.write('<img src="http://ad.doubleclick.net/activity;src=2344762;dcnet=4591;boom=22777;sz=1x1;ord='+ ord +'?"width="1" height="1" border="0" alt="">');
		document.write('<img src="http://ad.yieldmanager.com/pixel?id=558668&t=2" width="1" height="1" />');	
		document.write('<img width="1" height="1" border="0" alt="" src="http://www.bizographics.com/collect/?fmt=gif&pid=211&url=http%3A//entrep.forbes.com" />');
		document.write('<img src="http://ad.bizo.com/pixel?id=564788&t=2" width="1" height="1" />');
		//document.write('<scr' + 'ipt type="text/javascript">trg_segments = ["forbesentrep"];</scr' + 'ipt><scr' + 'ipt type="text/javascript" src="http://a.triggit.com/trgr.js"></scr' + 'ipt>');
		document.write('<script type="text/javascript" src="http://s.xp1.ru4.com/meta?_o=61989&_t=entcha"></scr' + 'ipt>');
		document.write('<img src="http://pixel.rubiconproject.com/tap.php?v=3680" border="0" width="1" height="1">');
	} else if (pageURL.indexOf("/lifestyle") != -1 || pageURL.indexOf("/forbeslife") != -1 || displayedChannel == "lifestyle") {
		document.write('<img src="http://ad.doubleclick.net/activity;src=2344762;dcnet=4591;boom=22780;sz=1x1;ord='+ ord +'?"width="1" height="1" border="0" alt="">');
		document.write('<img src="http://ad.yieldmanager.com/pixel?id=558670&t=2" width="1" height="1" />');
		document.write('<img width="1" height="1" border="0" alt="" src="http://www.bizographics.com/collect/?fmt=gif&pid=211&url=http%3A//life.forbes.com" />');
		document.write('<img src="http://ad.bizo.com/pixel?id=564794&t=2" width="1" height="1" />');
		//document.write('<scr' + 'ipt type="text/javascript">trg_segments = ["forbeslife"];</scr' + 'ipt><scr' + 'ipt type="text/javascript" src="http://a.triggit.com/trgr.js"></scr' + 'ipt>');
		document.write('<script type="text/javascript" src="http://s.xp1.ru4.com/meta?_o=61989&_t=forlif"></scr' + 'ipt>');
		document.write('<img src="http://pixel.rubiconproject.com/tap.php?v=3683" border="0" width="1" height="1">');
	} else if (pageURL.indexOf("/leadership") != -1 || displayedChannel == "leadership" || displayedChannel == "work") {
		if (pageURL.indexOf("/forbeswoman") != -1 || displayedSection == "forbeswoman") {
			document.write('<img src="http://ad.yieldmanager.com/pixel?id=592270&t=2" width="1" height="1" />');
			document.write('<img src="http://ad.doubleclick.net/activity;src=2344762;dcnet=4591;boom=25789;sz=1x1;ord='+ ord +'?"width="1" height="1" border="0" alt="">');
			document.write('<img width="1" height="1" border="0" alt="" src="http://www.bizographics.com/collect/?fmt=gif&pid=211&url=http%3A//forbeswoman.forbes.com" />');
			document.write('<img src="http://ad.bizo.com/pixel?id=592136&t=2" width="1" height="1" />');
//			document.write('<scr' + 'ipt type="text/javascript">trg_segments = ["forbeswoman"];</scr' + 'ipt><scr' + 'ipt type="text/javascript" src="http://a.triggit.com/trgr.js"></scr' + 'ipt>');
			document.write('<script type="text/javascript" src="http://s.xp1.ru4.com/meta?_o=61989&_t=forwom"></scr' + 'ipt>');
			document.write('<img src="http://pixel.rubiconproject.com/tap.php?v=3691" border="0" width="1" height="1">');
		} else if (pageURL.indexOf("/cmo-network") != -1 || displayedSection == "cmonetwork") {
			document.write('<img src="http://ad.yieldmanager.com/pixel?id=592269&t=2" width="1" height="1" />');
			document.write('<img src="http://ad.doubleclick.net/activity;src=2344762;dcnet=4591;boom=25788;sz=1x1;ord='+ ord +'?"width="1" height="1" border="0" alt="">');
			document.write('<img width="1" height="1" border="0" alt="" src="http://www.bizographics.com/collect/?fmt=gif&pid=211&url=http%3A//cmonetwork.forbes.com" />');
			document.write('<img src="http://ad.bizo.com/pixel?id=592137&t=2" width="1" height="1" />');
//			document.write('<scr' + 'ipt type="text/javascript">trg_segments = ["forbescmo"];</scr' + 'ipt><scr' + 'ipt type="text/javascript" src="http://a.triggit.com/trgr.js"></scr' + 'ipt>');
			document.write('<script type="text/javascript" src="http://s.xp1.ru4.com/meta?_o=61989&_t=cmonet"></scr' + 'ipt>');
			document.write('<img src="http://pixel.rubiconproject.com/tap.php?v=3689" border="0" width="1" height="1">');
		} else if (pageURL.indexOf("/ceonetwork") != -1 || displayedSection == "ceonetwork") {
			document.write('<img src="http://ad.yieldmanager.com/pixel?id=592266&t=2" width="1" height="1" />');
			document.write('<img src="http://ad.doubleclick.net/activity;src=2344762;dcnet=4591;boom=25783;sz=1x1;ord='+ ord +'?"width="1" height="1" border="0" alt="">');
			document.write('<img width="1" height="1" border="0" alt="" src="http://www.bizographics.com/collect/?fmt=gif&pid=211&url=http%3A//ceonetwork.forbes.com" />	');
			document.write('<img src="http://ad.bizo.com/pixel?id=592132&t=2" width="1" height="1" />');
//			document.write('<scr' + 'ipt type="text/javascript">trg_segments = ["forbesceo"];</scr' + 'ipt><scr' + 'ipt type="text/javascript" src="http://a.triggit.com/trgr.js"></scr' + 'ipt>');
			document.write('<script type="text/javascript" src="http://s.xp1.ru4.com/meta?_o=61989&_t=ceonet"></scr' + 'ipt>');
			document.write('<img src="http://pixel.rubiconproject.com/tap.php?v=3687" border="0" width="1" height="1">');
		} else {  
			document.write('<img src="http://ad.doubleclick.net/activity;src=2344762;dcnet=4591;boom=22778;sz=1x1;ord='+ ord +'?"width="1" height="1" border="0" alt="">');
			document.write('<img src="http://ad.yieldmanager.com/pixel?id=558671&t=2" width="1" height="1" />');
			document.write('<img width="1" height="1" border="0" alt="" src="http://www.bizographics.com/collect/?fmt=gif&pid=211&url=http%3A//leader.forbes.com" />');
			document.write('<img src="http://ad.bizo.com/pixel?id=564793&t=2" width="1" height="1" />');
			//document.write('<scr' + 'ipt type="text/javascript">trg_segments = ["forbeslead"];</scr' + 'ipt><scr' + 'ipt type="text/javascript" src="http://a.triggit.com/trgr.js"></scr' + 'ipt>');
			document.write('<script type="text/javascript" src="http://s.xp1.ru4.com/meta?_o=61989&_t=leadsh"></scr' + 'ipt>');
			document.write('<img src="http://pixel.rubiconproject.com/tap.php?v=3681" border="0" width="1" height="1">');
		}
	} else if (pageURL.indexOf("/lists") != -1 || displayedChannel == "lists") {
		document.write('<img src="http://ad.doubleclick.net/activity;src=2344762;dcnet=4591;boom=22781;sz=1x1;ord='+ ord +'?"width="1" height="1" border="0" alt="">');
		document.write('<img src="http://ad.yieldmanager.com/pixel?id=558667&t=2" width="1" height="1" />');
		document.write('<img width="1" height="1" border="0" alt="" src="http://www.bizographics.com/collect/?fmt=gif&pid=211&url=http%3A//lists.forbes.com" />');
		document.write('<img src="http://ad.bizo.com/pixel?id=564798&t=2" width="1" height="1" />');
		//document.write('<scr' + 'ipt type="text/javascript">trg_segments = ["forbeslists"];</scr' + 'ipt><scr' + 'ipt type="text/javascript" src="http://a.triggit.com/trgr.js"></scr' + 'ipt>');
		document.write('<script type="text/javascript" src="http://s.xp1.ru4.com/meta?_o=61989&_t=list"></scr' + 'ipt>');
		document.write('<img src="http://pixel.rubiconproject.com/tap.php?v=3684" border="0" width="1" height="1">');
	} else if (pageURL.indexOf("/markets") != -1 || displayedChannel == "markets") {
		document.write('<img src="http://ad.doubleclick.net/activity;src=2344762;dcnet=4591;boom=22776;sz=1x1;ord='+ ord +'?"width="1" height="1" border="0" alt="">');
		document.write('<img src="http://ad.yieldmanager.com/pixel?id=558666&t=2" width="1" height="1" />');
		document.write('<img width="1" height="1" border="0" alt="" src="http://www.bizographics.com/collect/?fmt=gif&pid=211&url=http%3A//markets.forbes.com" />');
		document.write('<img src="http://ad.bizo.com/pixel?id=564796&t=2" width="1" height="1" />');
//		document.write('<scr' + 'ipt type="text/javascript">trg_segments = ["forbesmarkets"];</scr' + 'ipt><scr' + 'ipt type="text/javascript" src="http://a.triggit.com/trgr.js"></scr' + 'ipt>');
		document.write('<script type="text/javascript" src="http://s.xp1.ru4.com/meta?_o=61989&_t=marcha"></scr' + 'ipt>');
		document.write('<img src="http://pixel.rubiconproject.com/tap.php?v=3679" border="0" width="1" height="1">');
	} else if (pageURL.indexOf("/opinions") != -1 || displayedChannel == "opinions") {
		if (pageURL.indexOf("/businessvisionaries") != -1 || displayedSection == "business visionaries") {
			document.write('<img src="http://ad.yieldmanager.com/pixel?id=592265&t=2" width="1" height="1" />');
			document.write('<img src="http://ad.doubleclick.net/activity;src=2344762;dcnet=4591;boom=25782;sz=1x1;ord='+ ord +'?"width="1" height="1" border="0" alt="">');
			document.write('<img width="1" height="1" border="0" alt="" src="http://www.bizographics.com/collect/?fmt=gif&pid=211&url=http%3A//businessvisionaries.forbes.com" />');
			document.write('<img src="http://ad.bizo.com/pixel?id=592135&t=2" width="1" height="1" />');
//			document.write('<scr' + 'ipt type="text/javascript">trg_segments = ["forbesvisionaries"];</scr' + 'ipt><scr' + 'ipt type="text/javascript" src="http://a.triggit.com/trgr.js"></scr' + 'ipt>');
			document.write('<script type="text/javascript" src="http://s.xp1.ru4.com/meta?_o=61989&_t=busvis"></scr' + 'ipt>');
			document.write('<img src="http://pixel.rubiconproject.com/tap.php?v=3690" border="0" width="1" height="1">');
		} else {
			document.write('<img src="http://ad.doubleclick.net/activity;src=2344762;dcnet=4591;boom=22782;sz=1x1;ord='+ ord +'?"width="1" height="1" border="0" alt="">');
			document.write('<img src="http://ad.yieldmanager.com/pixel?id=558669&t=2" width="1" height="1" />');
			document.write('<img width="1" height="1" border="0" alt="" src="http://www.bizographics.com/collect/?fmt=gif&pid=211&url=http%3A//opinion.forbes.com" />');
			document.write('<img src="http://ad.bizo.com/pixel?id=564789&t=2" width="1" height="1" />');
//			document.write('<scr' + 'ipt type="text/javascript">trg_segments = ["forbesop"];</scr' + 'ipt><scr' + 'ipt type="text/javascript" src="http://a.triggit.com/trgr.js"></scr' + 'ipt>');
			document.write('<script type="text/javascript" src="http://s.xp1.ru4.com/meta?_o=61989&_t=opinio"></scr' + 'ipt>');
			document.write('<img src="http://pixel.rubiconproject.com/tap.php?v=3685" border="0" width="1" height="1">');
		}
	} else if (pageURL.indexOf("/finance") != -1 || displayedChannel == "personalFinance") {
		if (pageURL.indexOf("/advisernetwork") != -1 || displayedSection == "adviser network") {
			document.write('<img src="http://ad.yieldmanager.com/pixel?id=587387&t=2" width="1" height="1" />');
			document.write('<img src="http://ad.doubleclick.net/activity;src=2344762;dcnet=4591;boom=25338;sz=1x1;ord='+ ord +'?"width="1" height="1" border="0" alt="">');
			document.write('<img width="1" height="1" border="0" alt="" src="http://www.bizographics.com/collect/?fmt=gif&pid=211&url=http%3A//advisernetwork.forbes.com" />');
			document.write('<img src="http://ad.bizo.com/pixel?id=592131&t=2" width="1" height="1" />');
//			document.write('<scr' + 'ipt type="text/javascript">trg_segments = ["forbesadviser"];</scr' + 'ipt><scr' + 'ipt type="text/javascript" src="http://a.triggit.com/trgr.js"></scr' + 'ipt>');
			document.write('<script type="text/javascript" src="http://s.xp1.ru4.com/meta?_o=61989&_t=finnet"></scr' + 'ipt>');
			document.write('<img src="http://pixel.rubiconproject.com/tap.php?v=3686" border="0" width="1" height="1">');
		} else {
			document.write('<img src="http://ad.doubleclick.net/activity;src=2344762;dcnet=4591;boom=22779;sz=1x1;ord='+ ord +'?"width="1" height="1" border="0" alt="">');
			document.write('<img src="http://ad.yieldmanager.com/pixel?id=558672&t=2" width="1" height="1" />');	
			document.write('<img width="1" height="1" border="0" alt="" src="http://www.bizographics.com/collect/?fmt=gif&pid=211&url=http%3A//persfin.forbes.com" />');
			document.write('<img src="http://ad.bizo.com/pixel?id=564790&t=2" width="1" height="1" />');
//			document.write('<scr' + 'ipt type="text/javascript">trg_segments = ["forbesfinance"];</scr' + 'ipt><scr' + 'ipt type="text/javascript" src="http://a.triggit.com/trgr.js"></scr' + 'ipt>');
			document.write('<script type="text/javascript" src="http://s.xp1.ru4.com/meta?_o=61989&_t=perfin"></scr' + 'ipt>');
			document.write('<img src="http://pixel.rubiconproject.com/tap.php?v=3682" border="0" width="1" height="1">');
		}
	} else if (pageType == "home") {
		document.write('<img src="http://ad.doubleclick.net/activity;src=2344762;dcnet=4591;boom=22783;sz=1x1;ord='+ ord +'?"width="1" height="1" border="0" alt="">');
		document.write('<img src="http://ad.yieldmanager.com/pixel?id=558664&t=2" width="1" height="1" />');
		document.write('<img width="1" height="1" border="0" alt="" src="http://www.bizographics.com/collect/?fmt=gif&pid=211&url=http%3A//home.forbes.com" />');
		document.write('<img src="http://ad.bizo.com/pixel?id=564787&t=2" width="1" height="1" />');
//		document.write('<scr' + 'ipt type="text/javascript">trg_segments = ["forbeshome"];</scr' + 'ipt><scr' + 'ipt type="text/javascript" src="http://a.triggit.com/trgr.js"></scr' + 'ipt>');
		document.write('<script type="text/javascript" src="http://s.xp1.ru4.com/meta?_o=61989&_t=hompag"></scr' + 'ipt>');
		document.write('<img src="http://pixel.rubiconproject.com/tap.php?v=3676" border="0" width="1" height="1">');
	} else {
		document.write('<img src="http://ad.doubleclick.net/activity;src=2344762;dcnet=4591;boom=22773;sz=1x1;ord='+ ord +'?"width="1" height="1" border="0" alt="">');
		document.write('<img src="http://ad.yieldmanager.com/pixel?id=558662&t=2" width="1" height="1" />');	
		document.write('<img width="1" height="1" border="0" alt="" src="http://www.bizographics.com/collect/?fmt=gif&pid=211&url=http%3A//forbes.com" />');
		document.write('<img src="http://ad.bizo.com/pixel?id=564792&t=2" width="1" height="1" />');
//		document.write('<scr' + 'ipt type="text/javascript">trg_segments = ["forbes"];</scr' + 'ipt><scr' + 'ipt type="text/javascript" src="http://a.triggit.com/trgr.js"></scr' + 'ipt>');
		document.write('<scr' + 'ipt type="text/javascript" src="http://s.xp1.ru4.com/meta?_o=61989&_t=ros"></scr' + 'ipt>');
		document.write('<img src="http://pixel.rubiconproject.com/tap.php?v=3675" border="0" width="1" height="1">');
	}

// Media Bid�s Manager 

		//ROS
	
		//"Network Pixel" c/o "Forbes",  segment: 'ROS Pixel' - DO NOT MODIFY THIS PIXEL IN ANY WAY 
		document.write('<script src="http://segment-pixel.invitemedia.com/pixel?pixelID=12101&partnerID=132&key=segment&returnType=js"></script>');
		document.write('<noscript>');
		document.write('<img src="http://segment-pixel.invitemedia.com/pixel?pixelID=12101&partnerID=132&key=segment" width="1" height="1" />');
		document.write('</noscript>');
		document.write('<!-- End of pixel tag -->');
	if (displayedChannel == "home") {
		//"Network Pixel" c/o "Forbes",  segment: 'Homepage Pixel' - DO NOT MODIFY THIS PIXEL IN ANY WAY 
		document.write('<script src="http://segment-pixel.invitemedia.com/pixel?pixelID=12102&partnerID=132&key=segment&returnType=js"></script>');
		document.write('<noscript>');
		document.write('<img src="http://segment-pixel.invitemedia.com/pixel?pixelID=12102&partnerID=132&key=segment" width="1" height="1" />');
		document.write('</noscript>');
		document.write('<!-- End of pixel tag -->');
	} else if (pageURL.indexOf("/businessvisionaries") != -1 || displayedSection == "business visionaries") {
		//"Network Pixel" c/o "Forbes",  segment: 'Bus. Vis. Pixel' - DO NOT MODIFY THIS PIXEL IN ANY WAY 
		document.write('<script src="http://segment-pixel.invitemedia.com/pixel?pixelID=12116&partnerID=132&key=segment&returnType=js"></script>');
		document.write('<noscript>');
		document.write('<img src="http://segment-pixel.invitemedia.com/pixel?pixelID=12116&partnerID=132&key=segment" width="1" height="1" />');
		document.write('</noscript>');
		document.write('<!-- End of pixel tag -->');
	}else if (pageURL.indexOf("/business") != -1 || displayedChannel == "business") {
		//"Network Pixel" c/o "Forbes",  segment: 'Business Channel Pixel' - DO NOT MODIFY THIS PIXEL IN ANY WAY 
		document.write('<script src="http://segment-pixel.invitemedia.com/pixel?pixelID=12103&partnerID=132&key=segment&returnType=js"></script>');
		document.write('<noscript>');
		document.write('<img src="http://segment-pixel.invitemedia.com/pixel?pixelID=12103&partnerID=132&key=segment" width="1" height="1" />');
		document.write('</noscript>');
		document.write('<!-- End of pixel tag -->');
	} else if (pageURL.indexOf("/advisernetwork") != -1 || displayedSection == "adviser network") {
		//"Network Pixel" c/o "Forbes",  segment: 'Financial Adv network Pixel' - DO NOT MODIFY THIS PIXEL IN ANY WAY 
		document.write('<script src="http://segment-pixel.invitemedia.com/pixel?pixelID=12112&partnerID=132&key=segment&returnType=js"></script>');
		document.write('<noscript>');
		document.write('<img src="http://segment-pixel.invitemedia.com/pixel?pixelID=12112&partnerID=132&key=segment" width="1" height="1" />');
		document.write('</noscript>');
		document.write('<!-- End of pixel tag -->');
	}else if (pageURL.indexOf("/cionetwork") != -1 || displayedSection == "cionetwork") {
		//"Network Pixel" c/o "Forbes",  segment: 'CIO Network Pixel' - DO NOT MODIFY THIS PIXEL IN ANY WAY 
		document.write('<script src="http://segment-pixel.invitemedia.com/pixel?pixelID=12114&partnerID=132&key=segment&returnType=js"></script>');
		document.write('<noscript>');
		document.write('<img src="http://segment-pixel.invitemedia.com/pixel?pixelID=12114&partnerID=132&key=segment" width="1" height="1" />');
		document.write('</noscript>');
		document.write('<!-- End of pixel tag -->');
	}else if (pageURL.indexOf("/technology") != -1 || displayedChannel == "technology") {
		//"Network Pixel" c/o "Forbes",  segment: 'Tech Channel Pixel' - DO NOT MODIFY THIS PIXEL IN ANY WAY 
		document.write('<script src="http://segment-pixel.invitemedia.com/pixel?pixelID=12104&partnerID=132&key=segment&returnType=js"></script>');
		document.write('<noscript>');
		document.write('<img src="http://segment-pixel.invitemedia.com/pixel?pixelID=12104&partnerID=132&key=segment" width="1" height="1" />');
		document.write('</noscript>');
		document.write('<!-- End of pixel tag -->');
	} else if (pageURL.indexOf("/markets") != -1 || displayedChannel == "markets") {
		//"Network Pixel" c/o "Forbes",  segment: 'Markets Channel Pixel' - DO NOT MODIFY THIS PIXEL IN ANY WAY 
		document.write('<script src="http://segment-pixel.invitemedia.com/pixel?pixelID=12105&partnerID=132&key=segment&returnType=js"></script>');
		document.write('<noscript>');
		document.write('<img src="http://segment-pixel.invitemedia.com/pixel?pixelID=12105&partnerID=132&key=segment" width="1" height="1" />');
		document.write('</noscript>');
		document.write('<!-- End of pixel tag -->');
	} else if (pageURL.indexOf("/entrepreneurs") != -1 || displayedChannel == "entrepreneurs") {
		//"Network Pixel" c/o "Forbes",  segment: 'Entrepreneur channel Pixel' - DO NOT MODIFY THIS PIXEL IN ANY WAY 
		document.write('<script src="http://segment-pixel.invitemedia.com/pixel?pixelID=12106&partnerID=132&key=segment&returnType=js"></script>');
		document.write('<noscript>');
		document.write('<img src="http://segment-pixel.invitemedia.com/pixel?pixelID=12106&partnerID=132&key=segment" width="1" height="1" />');
		document.write('</noscript>');
		document.write('<!-- End of pixel tag -->');
	} else if (pageURL.indexOf("/leadership") != -1 || displayedChannel == "leadership") {
		//"Network Pixel" c/o "Forbes",  segment: 'Leadership Channel Pixel' - DO NOT MODIFY THIS PIXEL IN ANY WAY 
		document.write('<script src="http://segment-pixel.invitemedia.com/pixel?pixelID=12107&partnerID=132&key=segment&returnType=js"></script>');
		document.write('<noscript>');
		document.write('<img src="http://segment-pixel.invitemedia.com/pixel?pixelID=12107&partnerID=132&key=segment" width="1" height="1" />');
		document.write('</noscript>');
		document.write('<!-- End of pixel tag -->');
	} else if (pageURL.indexOf("/finance") != -1 || displayedChannel == "personalFinance") {
		//"Network Pixel" c/o "Forbes",  segment: 'Personal Finance Channel Pixel' - DO NOT MODIFY THIS PIXEL IN ANY WAY 
		document.write('<script src="http://segment-pixel.invitemedia.com/pixel?pixelID=12108&partnerID=132&key=segment&returnType=js"></script>');
		document.write('<noscript>');
		document.write('<img src="http://segment-pixel.invitemedia.com/pixel?pixelID=12108&partnerID=132&key=segment" width="1" height="1" />');
		document.write('</noscript>');
		document.write('<!-- End of pixel tag -->');
	} else if (pageURL.indexOf("/lifestyle") != -1 || displayedChannel == "forbeslife") {
		//"Network Pixel" c/o "Forbes",  segment: 'ForbesLife Channel Pixel' - DO NOT MODIFY THIS PIXEL IN ANY WAY 
		document.write('<script src="http://segment-pixel.invitemedia.com/pixel?pixelID=12109&partnerID=132&key=segment&returnType=js"></script>');
		document.write('<noscript>');
		document.write('<img src="http://segment-pixel.invitemedia.com/pixel?pixelID=12109&partnerID=132&key=segment" width="1" height="1" />');
		document.write('</noscript>');
		document.write('<!-- End of pixel tag -->');
	} else if (pageURL.indexOf("/lists") != -1 || displayedChannel == "lists") {
		//"Network Pixel" c/o "Forbes",  segment: 'Lists Channel Pixel' - DO NOT MODIFY THIS PIXEL IN ANY WAY 
		document.write('<script src="http://segment-pixel.invitemedia.com/pixel?pixelID=12110&partnerID=132&key=segment&returnType=js"></script>');
		document.write('<noscript>');
		document.write('<img src="http://segment-pixel.invitemedia.com/pixel?pixelID=12110&partnerID=132&key=segment" width="1" height="1" />');
		document.write('</noscript>');
		document.write('<!-- End of pixel tag -->');
	} else if (pageURL.indexOf("/opinions") != -1 || displayedChannel == "opinions") {
		//"Network Pixel" c/o "Forbes",  segment: 'Opinions Channel Pixel' - DO NOT MODIFY THIS PIXEL IN ANY WAY 
		document.write('<script src="http://segment-pixel.invitemedia.com/pixel?pixelID=12111&partnerID=132&key=segment&returnType=js"></script>');
		document.write('<noscript>');
		document.write('<img src="http://segment-pixel.invitemedia.com/pixel?pixelID=12111&partnerID=132&key=segment" width="1" height="1" />');
		document.write('</noscript>');
		document.write('<!-- End of pixel tag -->');
	}  else if (pageURL.indexOf("/ceonetwork") != -1 || displayedSection == "ceonetwork") {
		//"Network Pixel" c/o "Forbes",  segment: 'CEO Network Pixel' - DO NOT MODIFY THIS PIXEL IN ANY WAY 
		document.write('<script src="http://segment-pixel.invitemedia.com/pixel?pixelID=12113&partnerID=132&key=segment&returnType=js"></script>');
		document.write('<noscript>');
		document.write('<img src="http://segment-pixel.invitemedia.com/pixel?pixelID=12113&partnerID=132&key=segment" width="1" height="1" />');
		document.write('</noscript>');
		document.write('<!-- End of pixel tag -->');
	}  else if (pageURL.indexOf("/cmo-network") != -1 || displayedSection == "cmonetwork") {
		//"Network Pixel" c/o "Forbes",  segment: 'CMO Network Pixel' - DO NOT MODIFY THIS PIXEL IN ANY WAY 
		document.write('<script src="http://segment-pixel.invitemedia.com/pixel?pixelID=12115&partnerID=132&key=segment&returnType=js"></script>');
		document.write('<noscript>');
		document.write('<img src="http://segment-pixel.invitemedia.com/pixel?pixelID=12115&partnerID=132&key=segment" width="1" height="1" />');
		document.write('</noscript>');
		document.write('<!-- End of pixel tag -->');
	}  else if (pageURL.indexOf("/forbeswoman") != -1 || displayedSection == "forbeswoman") {
		//"Network Pixel" c/o "Forbes",  segment: 'Forbes Woman Pixel' - DO NOT MODIFY THIS PIXEL IN ANY WAY 
		document.write('<script src="http://segment-pixel.invitemedia.com/pixel?pixelID=12117&partnerID=132&key=segment&returnType=js"></script>');
		document.write('<noscript>');
		document.write('<img src="http://segment-pixel.invitemedia.com/pixel?pixelID=12117&partnerID=132&key=segment" width="1" height="1" />');
		document.write('</noscript>');
		document.write('<!-- End of pixel tag -->');
	}
	
//End - Media Bid�s Manager 
	document.write('<script type="text/javascript" src="http://ads.forbes.com/RealMedia/ads/adstream_jx.ads/forbes.com/sictarget/setcookie@x98?'+ ord + '"></script>');

}

function checkToolbar() {

	if(getCookie("GTB") == null && navigator.userAgent.indexOf('GTB') != -1) { setCookie("GTB", "true", 1); } 

}

function setSilverlightCookie(){
	document.write('<script type="text/javascript" src="http://images.forbes.com/scripts/util/SilverlightVersion.js"></script>');
	document.write('<script type="text/javascript" src="http://images.forbes.com/scripts/util/SilverlightCookie.js"></script> ');
}

setSilverlightCookie();


wg_testConfig();

addPixels();

checkToolbar();

	function initialCCLogoPlacement(){
		addIdToVideoPlayerIframe('videoplayer_iframe');
		//change href and classname to display email popup box
		var storyUrl = location.href+"";
		if (storyUrl.indexOf('?')>-1) storyUrl = storyUrl.replace(location.search,"");
		var storyTitle = ReplaceAll($("h1").text(),"'", "%27");

		$("a[href^='mailto']").each(function(){
			var onclickAction = this.getAttribute('onclick')+"";
			if (onclickAction.indexOf('s_linkName=\'E-Mail\'')>-1){
				this.className="thickbox";
				onclickAction = onclickAction.replace('s_linkName=\'E-Mail\'','s_linkName=\'artctrlemail\'')+" document.domain='forbes.com';";
				this.setAttribute('onclick',onclickAction);
				this.href="http://orgchart.forbes.com/acs/acsemail.php?url="+storyUrl+"&amp;headline="+storyTitle+"&amp;keepThis=true&amp;TB_iframe=true&amp;height=580&amp;width=500";
			}
			
		});
		//display the logo
		if (typeof adStringx92 != 'undefined') $("#controlsbox").append("<br/><br/>"+adStringx92);
		if (typeof adStringx91 != 'undefined' && $(".controls").length>2) $(".controls:last").append(adStringx91);
	}
	function ReplaceAll(Source,stringToFind,stringToReplace){
      var temp = Source;
        var index = temp.indexOf(stringToFind);
            while(index != -1){
                temp = temp.replace(stringToFind,stringToReplace);
                index = temp.indexOf(stringToFind);
            }
            return temp;
    }
	if (typeof EmailSponsor != 'undefined') initialCCLogoPlacement();

//-->
