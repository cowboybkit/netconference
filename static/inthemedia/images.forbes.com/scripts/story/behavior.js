ForbesStory = {
	fbs_click : function() {
		window.open('http://www.facebook.com/sharer.php?u='+encodeURIComponent(location.href)+
			'&t='+encodeURIComponent(document.title),'sharer','toolbar=0,status=0,width=626,height=436');
		return false;
	},
	RightColumn : {
		columnStyle : fdcWincolStyle, // defined in header_setup
		isWide : 0,
		getPosition1 : function(type) {
			if(this.columnStyle == "column" && !type) {
				var alertsBox = this.getAlertsBox(this.columnStyle);
				document.write(	"<ul id='colAd'>"+
							"<li>" + alertsBox + "</li>"+
							"<li>"); this.writeAdTop(); 
			} else {
				if(type && type == 'wide') this.isWide = 1;
				this.writeAdTop();
			}
		},
		getPosition2 : function() {
			if(this.columnStyle == 'column') return;
			var alertsBox = this.getAlertsBox(this.columnStyle);
			document.write(alertsBox); 
		},
		writeAdTop : function() {
			var top = 'adBgTop', mid = 'adBgMId', div = 'dynamicAdWinDiv';
			if(this.isWide || this.columnStyle == 'column') { 
				top = 'adColBgTop'; mid = 'adColBgMId'; div = 'dynamicAdColDiv';
			}
			document.write(	"<div class='"+top+"'></div>"+
					"<div id='"+div+"' class='"+mid+"'>"+
						"<div class='adContainer'>");
		},
		writeAdCall : function() {
			var bot = (this.columnStyle == 'column' || this.isWide) ? 'RightMiddle' : 'Block';
			document.write("<script type='text/javascript'>OAS_AD('"+bot+"');</script>");
		},
		writeAdBottom : function() {
			var output = '';
			var bot = (this.columnStyle == 'column' || this.isWide) ? 'adColBgBottom' : 'adBgBottom';
			output = "</div></div><div class='"+bot+"'></div>";
			if(this.columnStyle == 'column' && !this.isWide) 
				output += "</li></ul>";
			else if(this.isWide) {
				output += "<hr class='clear'/>"+
                    "<div id='colAd'>" + this.getAlertsBox('column') + "</div>";
			}
			document.write(output);
		},
		getAlertsBox : function(style) {
			if((typeof alertsList == "undefined") || !(alertsList.alerts.length > 0)) {
				return "";
			}
			var alertsBox = 
				'<div id="eNewsltrs">'+
				'<h4>Get Stories By Email</h4>';
				if (style=="window") { alertsBox += '<span class="logo"><script language="JavaScript">OAS_AD("AlertsLogo");</script></span>'; }
				alertsBox += '<form action="http://members.forbes.com/alertSignup" method="post" '+ 
				'name="alertForm" id="alertForm">'+
				'<input name="action" value="alerts_signup" type="hidden"/>'+
				'<input name="comingFrom" value="alerts" type="hidden"/>'+
				'<input name="service" value="key_membership" type="hidden"/>'+
				'<input name="gotoURL" value="'+document.location.href+'" type="hidden"/>'+
				'<div>';
				if (style=="column") { alertsBox += '<span class="logo"><script language="JavaScript">OAS_AD("AlertsLogo");</script></span>'; }
				alertsBox += this.setTickersKeywordsPeople(style)+'<div class="clear"></div>';
			if(!isMember()){
				if(style == "column") {
					alertsBox +=
						'<b>Not a member yet?<br/><a href="http://members.forbes.com/membership/signup.jhtml?'+
							'gotoURL='+document.location.href+'">Join Now!</a></b>'+
						'<span>Already a member? <a href="http://members.forbes.com/membership/signup.jhtml?'+
							'gotoURL='+document.location.href+'">Log In</a></span>'+
						'<input type="text" value="Enter Username" class="alertsUsername" id="textInput" name="login"/>'+
						'<input type="text" value="Enter Email" class="alertsEmail" id="textInput" name="email"/>';
				} else {
					alertsBox +=
					'<span>Already a member? <a href="http://members.forbes.com/membership/signup.jhtml?'+
                            'gotoURL='+document.location.href+'">Log In</a></span>'+
						'<p>Not a member yet?<a href="http://members.forbes.com/membership/signup.jhtml?'+
                            'gotoURL='+document.location.href+'">Join Now!</a></p>'+
						'<dl>'+
						'<dt><input type="text" value="Enter Username" class="alertsUsername" id="textInput" name="login"/></dt>'+
						'<dd><input type="text" value="Enter Email" class="alertsEmail" id="textInput" name="email"/></dd>'+
						'</dl>';
				}
				if(style != "column") alertsBox += '<dl><dt>';
				alertsBox += '<select class="alertsTitle" id="textInput" name="title">'+
					'<option value="">Select Your Title</option>'+
					'<option value="President">President</option>'+
					'<option value="Chairman">Chairman</option>'+
					'<option value="Owner/Partner">Owner/Partner</option>'+
					'<option value="CEO">CEO</option>'+
					'<option value="CFO">CFO</option>'+
					'<option value="CIO/CTO">CIO/CTO</option>'+
					'<option value="CMO">CMO</option>'+
					'<option value="COO">COO</option>'+
					'<option value="Vice President">Vice President</option>'+
					'<option value="General Manager">General Manager</option>'+
					'<option value="Middle Management">Middle Management</option>'+
					'<option value="Technical Staff">Technical Staff</option>'+
					'<option value="Clerical/Support Staff">Clerical/Support Staff</option>'+
					'<option value="Professional">Professional</option>'+
					'<option value="Homemaker">Homemaker</option>'+
					'<option value="Student">Student</option>'+
					'<option value="Retired">Retired</option>'+
					'<option value="Not Employed">Not Employed</option>'+
					'<option value="Other">Other</option>'+
					'</select>';
				if(style == "column") {
					alertsBox += '<input value="Special Offers" type="checkbox" '+
							'id="Special Offers" checked /><label for='+
							'"Special Offers">Receive Special Offers?</label>';
				} else {
					alertsBox += '<dd><input value="Special Offers" type="checkbox" '+ 
							'id="Special Offers" checked /><label for='+
							'"Special Offers">Receive Special Offers?</label></dd>'+
							'</dt></dl>';
				}
			}
			if(style == "column") {
				alertsBox += '<input type="image" src="http://images.forbes.com/media/story/enews_signUp.gif" '+
						'alt="Sign Me Up!" class="submitB"/>'+
					'<cite><a href="http://www.forbes.com/fdc/includes/alertsfaq.shtml">FAQ</a> |'+
					'<a href="http://www.forbes.com/fdc/includes/terms.html">Terms &amp; Conditions</a>'+
					' | <a href="http://www.forbes.com/fdc/privacy.html">Privacy Policy</a></cite>';
			} else {
				alertsBox += '<cite><a href="http://www.forbes.com/fdc/includes/alertsfaq.shtml">FAQ</a> |'+ 
					'<a href="http://www.forbes.com/fdc/includes/terms.html">Terms &amp; Conditions</a>'+
					' | <a href="http://www.forbes.com/fdc/privacy.html">Privacy Policy</a></cite>'+
					'<input type="image" src="http://images.forbes.com/media/story/enews_signUp.gif" alt="Sign '+ 
						'Me Up!" class="submitB"/>';
			}
			alertsBox += '</div></form></div>';
			return alertsBox;
		},
		setTickersKeywordsPeople : function(style) {
			var checkBoxes = '';
			checkBoxes +='<p>Select Topics:</p>'+
			'<ul>';
			if((typeof alertsList != "undefined") && alertsList.alerts.length>0){
				
                                checkBoxes += this.tableAlertForEach(2,style);
			}
			checkBoxes += '</ul>';                       
			return checkBoxes;
		},	
                tableAlertForEach : function(numColumns, style){
                    var checkBoxes = '';
                    var addOpenLi = false;
                 			
                    var len = alertsList.alerts.length;
                    
                    for (var i=0; i<len; i++) {
			if(style == 'window') {
                            if(i>=len/numColumns&&!addOpenLi||i==0){
				checkBoxes += '<li>';
				if(i!=0){
                                  addOpenLi = true;
				}
                            }
			} else if(style == 'column' && i == 0) {
				checkBoxes += '<li>';
                          }			
                        
                        checkBoxes += '<p><input name="'+alertsList.alerts[i].type+'.'+alertsList.alerts[i].key+'" value="'+alertsList.alerts[i].value+'" type="checkbox" id="'+
					alertsList.alerts[i].key+'" /><label for="'+alertsList.alerts[i].key +'" >'+alertsList.alerts[i].value+'</label></p>';
                    }
                  return checkBoxes;                    
                }
	} // END RightColumn
}; // END ForbesStory

jQuery(function() {
	var NEWS_SERVLET = 'http://search.forbes.com/news/relatedBoxes.do';
	//var POP_SCRIPT = 'http://images.forbes.com/omniture/popModuleStories.js';
       var POP_SCRIPT = 'http://images.forbes.com/omniture/mostPopularBox.js';
	var MARKETS_BRIEF = 'http://images.forbes.com/conf/markets/marketsbrief.js';
	var MOST_COMMENTED_SCRIPT = 'http://www.forbes.com/omniture/mostcommented.js';

	if(!$.event.special.click)
		$.event.special.click = {
			setup: function() {
				if(!$.browser.safari) return false;
				this.onclick = function() { return $.event.handle.apply(this, arguments); };
				return true;
			},
			teardown:function() {
				if(!$.browser.safari) return false;
				this.onclick = "";
				return true;
			}
		}

	$.extend($.tabs.prototype, {
		forbesFormatTabs : function() {
			this.$tabs.parents('ul').each(function() {
				var tabs = $('li>a',this);
				tabs.not(':last').addClass('forbes-inner-border');
				var liCount = tabs.size();
				// account for the border and figure out each tabs width
				var aSize = Math.ceil(($(this).width()-liCount+1)/liCount);
				tabs.width(aSize);
				// last tab may be shorter because we will pickup rounding/render issues with it
				if($(this).outerHeight()>20) {
					var tabsWidth = 0;
					tabs.each(function() { tabsWidth += $(this).outerWidth(); });
					var lastLink = tabs.filter(':last');
					lastLink.width(lastLink.width()-(tabsWidth-$(this).width()));
				}
			});
		},
		forbesRemoveTabs : function(tabList) {
			var tabList = tabList.split(',');
			if(tabList < 1) return;
			var that = this;
			var offset = 0;
			this.$tabs.each(function(i, a) {
				if(jQuery.inArray($(a).text(), tabList) > -1)
					that.remove(i + offset--);
			});
			this.forbesFormatTabs();
		}
	});	

        var xignitePrerecs = false;
	function loadXignitePrerecs() {
		if(xignitePrerecs) return;
		if(!$.fn.everyTime) loadScript("http://images.forbes.com/scripts/jquery/jquery.timer-1.0.0.min.js");
		if(!$.effects) {
			loadScript("http://images.forbes.com/scripts/jquery/ui/effects.core.min.js");
			loadScript("http://images.forbes.com/scripts/jquery/ui/effects.highlight.min.js");
		} else if(!$.effects.highlight) loadScript("http://images.forbes.com/scripts/jquery/ui/effects.highlight.min.js");
		loadScript("http://widgets.xignite.com/forbeslastsale.jscript");
		if(!window.Xignite) loadScript("http://widgets.xignite.com/JavaScripts/Widgets/forbeslastsale.js");
		xignitePrerecs = true;
	}


	// Configure/Request story modules
	$(".storyBoxes").each(function() {
		var id = $(this).attr('id');
		switch(id) {
			case 'quotes':
				var tickers = $(this).attr('data-tickers');
				if(tickers===undefined||tickers=="") break;
				loadXignitePrerecs();
				loadScript("http://images.forbes.com/scripts/story/xignite/RelatedBox.Configurable.js");
				break;
			case 'companyBox':
				var tickers = $(this).attr('data-tickers');
				if(tickers===undefined||tickers=="") break;
				loadXignitePrerecs();
				loadScript("http://images.forbes.com/scripts/story/xignite/CompanyBoxWikiInvest.Configurable.js");
				break;
			case 'related':
				var popularString = "<div id='relatedBox' class='makeTab quote'>"+
				"<div id='quotesTab'></div><div id='storiesTab'></div><div id='slidesTab'></div><div id='videosTab'></div>"+
				"<ul class='mkBrd'>";
				var tickers = $(this).attr('data-tickers');
				if($(this).attr('quotes')&&tickers!==undefined&&tickers!="") {
					popularString += "<li><a href='#quotesTab'>Quotes</a></li>";
				}
				popularString = popularString + "<li><a href='#storiesTab'>Stories</a></li>"+
						"<li><a href='#slidesTab'>In Pictures</a></li>"+
						"<li><a href='#videosTab'>Videos</a></li>"+
					"</ul>"+
				"</div>"
				$('#related').replaceWith(popularString);
				$('.mkBrd').tabs();
				$('.mkBrd').tabs('forbesFormatTabs');
				var dkeys = $(this).attr('data-keywords') || "";
				var keys = dkeys.split(',');
				if(keys.length < 4)
					dkeys += ',' + document.title.replace(/(\s)*-\sForbes.com$/, '');
				loadScript(NEWS_SERVLET+"?tickers="+$(this).attr('data-tickers')+"&keywords="+dkeys+"&slideshows="+$(this).attr('slideshows')+"&url="+$(this).attr('url'));
				if($(this).attr('quotes')&&tickers!==undefined&&tickers!="") {
					$('#relatedBox').data('tickers',tickers);
					loadScript("http://images.forbes.com/scripts/story/xignite/RelatedTab.Configurable.js");
				} else { $(".mkBrd").tabs('forbesRemoveTabs', 'Quotes'); }
				break;
			case 'pop':
				loadScript(POP_SCRIPT);
				break;
			case 'mktBrief':
				loadScript(MARKETS_BRIEF);
				break;
			case 'mostcommented':				
				loadScript(MOST_COMMENTED_SCRIPT);	
			case 'customRelated':
				$('.mkBrd').tabs();
				$('.mkBrd').tabs('forbesFormatTabs');				
				if($(this).attr('markets')) {
					var tickers = $(this).attr('data-tickers');
					if($(this).attr('quotes')&&tickers!==undefined&&tickers!="") {					
						$('#relatedBox').data('tickers',tickers);
						loadScript("http://images.forbes.com/scripts/story/xignite/RelatedTab.Configurable.js");
					} else {$(".mkBrd").tabs('forbesRemoveTabs', 'Quotes'); }
				}	
				break;	
		}
	});

	// Write partner backlinks to the story page
	$(".partners").each(function() {
		var id = $(this).attr('id');
		var placement = id.substr(2,id.length);
		if(!partner || !placement) return; // partner defined in header_setup
		var height = '30';
		var width = '130';
		var url = '';
		var link = '';
		var klass = 'top';
		if(partner == 'yahoo' || partner == 'yahoomag' || partner == 'yahootix') {
			if(placement == 'RightTop') {
				url='http://images.forbes.com/media/partners/yahoo/BackToYahooFinanceButton2.gif';
				link='http://finance.yahoo.com';
			} else if(placement == 'MidBottom') {
				url='http://images.forbes.com/media/partners/yahoo/BackToYahooFinanceButton2.gif';
				link='http://finance.yahoo.com';
				klass = 'bottom';
			}
		} else if(partner == 'yahoomail') {
			if(placement == 'RightTop') {
				url='http://images.forbes.com/media/partners/yahoo/yahoo_mail_130x30.gif';
				link='http://mail.yahoo.com';
			}
		} else if(partner == 'my_yahoo') {
			if(placement == 'MidBottom') {
				height='31';
				width='88';
				url='http://images.forbes.com/media/partners/my_yahoo/BackToMyYahooFinanceButton.gif';
				link='http://my.yahoo.com';
				klass = 'bottom';
			}
		}
		if(height != '' && width != '' && url != '' && link != '') {
			$('#p_'+placement).replaceWith('<div id="partnerLogo" class="'+klass+'"><a href="' + link + '"><img src="' + url 
				+ '" height="' + height + '" width="' + width + '" alt="' + partner + '" border="0"></a></div>');
		}
		
	});

	function loadScript(url) {
		var s = document.createElement('script');
		s.src = url;
		document.body.appendChild(s);
	}

	// Construct tabs for Special Reports module
	$('#listSpBox>ul').tabs().tabs('forbesFormatTabs');

	// Bind events for the Alerts Box
	(function() {
		$('#eNewsltrs cite a').click(function() {
			var remote = window.open('', 'TheRemote', "toolbar=0,location=0,directories=0,status=0,"
							+"menubar=0,scrollbars=1,resizable=1,height=400,width=600");
			remote.location.href = $(this).attr('href');
			remote.focus();
			return false;
		});
		$('#alertForm :input[type=text]').focus(function() { if($(this).val()==this.defaultValue) $(this).val(''); });
		$('#alertForm').submit(function() {
			if(isMember()) {
				var isOneChecked = verifyOneChecked(this);
				if (!isOneChecked) alert('Please select at least one alert.');
				return isOneChecked;
			} else {
				if (checkAlertForm()) {
					setCLevelAlertsCookie($('#alerts_title').val());
					return true;
				}
			}
			return false;
		});

		function checkAlertForm() {
			var msg = "";
			if (!loginCheck($('input.alertsUsername').val())) {
				msg = "Usernames must be between 5 and 25 characters, with no spaces or dashes. "
					+"Please use letters, numbers, and underscores only.";
			} else if (!emailCheck($('input.alertsEmail').val())) {
				msg = "Please provide a valid email address";
			} else if (!verifyOneChecked(document.alertForm)) {
				msg = "Please select at least one alert";
			} else if($("select.alertsTitle option:selected").val()=="") {
				msg = "Please select a title from the drop down list.";
			}
			if (msg == "") return true;
			else alert(msg);
			return false;
		}

		function verifyOneChecked(theForm) {
			var retValue = false;
			if ((theForm != null) && (theForm.elements != null)) {
				for (i = 0; i < theForm.elements.length; i++) {					
					if ((theForm.elements[i].type == "checkbox") && (theForm.elements[i].checked) 
							&& (theForm.elements[i].id != "Special Offers")) {
						return true;
					}
				}
			}
			return false;
		}

		function loginCheck(login) {
			if (!login) return false;
			login=$.trim(login);
			// The login should contain only letters, digits or underscore chars;
			// the length of the login must be between 5 and 25 chars.
			if (login.match(/^[\da-z_]{5,25}$/i)==null) return false;
			return true;
		}

		function emailCheck(email) {
			var emailPat = /^(.+)@(.+)$/
			var specialChars = "\\(\\)<>@,;:\\\\\\\"\\.\\[\\]"
			var validChars = "\[^\\s" + specialChars + "\]"
			var quotedUser = "(\"[^\"]*\")"
			var ipDomainPat = /^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/
			var atom = validChars + '+'
			var word = "(" + atom + "|" + quotedUser + ")"
			var userPat = new RegExp("^" + word + "(\\." + word + ")*$")
			var domainPat = new RegExp("^" + atom + "(\\." + atom + ")*$")
			var matchArray = email.match(emailPat)
			if (matchArray == null) return false
			var user = matchArray[1];
			var domain = matchArray[2];

			if (user.match(userPat) == null) return false;

			var IPArray = domain.match(ipDomainPat);
			if (IPArray != null) {
				// this is an IP address
				for (var i = 1; i <= 4; i++) {
					if (IPArray[i] > 255) return false;
				}
				return true;
			}

			// Domain is symbolic name
			var domainArray = domain.match(domainPat);
			if (domainArray == null) return false;

			var atomPat = new RegExp(atom, "g");
			var domArr = domain.match(atomPat);
			var len = domArr.length;
			if (domArr[domArr.length - 1].length < 2 || domArr[domArr.length - 1].length > 3) return false;

			if (len < 2) return false;

			return true;
		}
		$("div#sponsoredLinks").html("");

/* - breaks in ie
		$('#x1div').contents().filter(function() {
			return this.nodeType == Node.TEXT_NODE;
		}).remove()[0];
*/

		function setCLevelAlertsCookie(title) {
			var validTitles = ['President','Chairman','Owner/Partner','CEO','CFO','CIO/CTO',
						'CMO','COO','General Manager','Vice President','Technical Staff'];
			var valid = false;
			for(var i in validTitles) if(title==validTitles[i]) { valid = true; break; }
			if(!valid) {
				delStandaloneCookie("ceotitle");
				return;
			}
			setStandaloneCookie("ceotitle",escape(title),1825);
		}
	})();
             
	// Article Controls slide event binding
	var shareOpen = false;
	$("#sharePanel").click(function(){
		$("#shareOpen").slideToggle("slow",function() {
			if(shareOpen) $("#sharePanel img").attr({src:"http://images.forbes.com/media/story/share_icon.gif"});
			else $("#sharePanel img").attr({src:"http://images.forbes.com/media/story/shareclose_icon.gif"});
			shareOpen=!shareOpen;
		});
		return false;
	});
	$('ul.controls .sharePanel').each(function() {
		var t = this;
		var open = false;
		var docClose = function() {
			$(t).click();
		};
		$(t).click(function() {
			var $img = $('img',this);
			if(open) $(document).unbind('click',docClose);
			else $(document).bind('click',docClose);
			$(this).next().slideToggle("slow",function() {
				if(open) $img.attr({src:"http://images.forbes.com/media/story/share_icon.gif"});
				else $img.attr({src:"http://images.forbes.com/media/story/shareclose_icon.gif"});
				open=!open;
			});
			return false;
		});
	});

	// Twitter cptl.st
	$('#shareOpen,ul.controls .shareOpen').find('a[href^=http://twitter.com]').click(function() {
		var url = $(this).attr('href').split('%20|%20Forbes%20|%20');
		if(url.length!=2) return;
		
		$.getJSON(
			'http://orgchart.forbes.com/bitly/bitly_api.php?callback=?',
			{
				url:url[1],
				format:'jsonp'
			}
			,function(data) {
			if(data.shortURL != undefined) {
				url[1] = data.shortURL;
				document.location = url.join(" via @forbes ");
		 	}
		});
		return false;
	});

	function wgsn_loadConfig() {
		loadScript('http://images.forbes.com/scripts/story/whiteglove_sn.js');
	}
	function wgsn_testConfig() {
        	var camefrom = document.cookie.match("wg_originalDomain=(.*?)(;|$)");
        	var valRefExpr = new RegExp('(digg\\.)|(facebook\\.)|(twitter\\.)|(reddit\\.)', 'i');
		if(camefrom){
			camefrom = camefrom[1];
		  	if (camefrom==''){
                		var referrer = document.referrer;
				camefrom = referrer.match("http\:\/\/(www\\.)?(.*?)(\:.+)?\/");
                		if(camefrom) camefrom = camefrom[2];}}
		else {
			var referrer = document.referrer;
                        camefrom = referrer.match("http\:\/\/(www\\.)?(.*?)(\:.+)?\/");
                        if(camefrom) camefrom = camefrom[2];}
        	if(camefrom)
              		if(camefrom.match(valRefExpr))
                       		wgsn_loadConfig();
	}
	wgsn_testConfig();
}); // END jQuery


var commentsBoxFlag = false;
var commentsFlag = false;

var rtsUtil = {
	starTimeout:175, // how long in milliseconds after the mouse leaves the rate area to reset to the current rating
	commentLength:2000,
	fullStar:"http://images.forbes.com/media/assets/rate/done.gif",
	emptyStar:"http://images.forbes.com/media/assets/rate/star.gif",
	halfStar:"http://images.forbes.com/media/assets/rate/half.gif",
	regBubble:'http://images.forbes.com/scripts/bubbles.js',
        publishedComments:'http://www.forbes.com/comments/',
        rateCall:'http://rate.forbes.com/rating/RatingServlet',
        commentCall:'http://rate.forbes.com/comments/CommentServlet',
	setUserRate:function(id,value) {
		this.userRating = value;
		this.setStars(this.userStars,value);
	},
	setAvgRate:function(id,value) { this.setStars(this.avgStars,value);  
	},
        setComments:function(id,data) { 
		if (commentsBoxFlag || commentsFlag){
			if (commentsBoxFlag) {this.setCommentsBoxHTML(data);};
			if (commentsFlag) {this.setCommentsHTML2(data);}
		}
		else  this.setCommentsHTML(data);
	},
	addCaptcha:function(captcha) { $('#rtsCaptcha').html(captcha.replace('/comments/CommentServlet',this.commentCall)); },
	clearCaptcha:function() { $('#rtsCaptcha').html(''); },
	disableButton:function() { $('#rtsSubmitButton').css({display:'none'}); },
	handleException:function(e) { /*console.error(e);*/ },
	addRts:function(id,params) { //keep addRts for backward compatible
		id = '#'+id;
		this.params=params;
		var t=this,html = "<h3>Rate This Story</h3><div><span>Your Rating </span>";
		function addStars(cls) { for(var i=0;i<5;i++) html+='<img src="'+t.emptyStar+'" class="'+cls+'"></img>'; }
		addStars("star");
		html+="<span>Overall Rating </span>";
		addStars("star no");
		html+="</div>";
		$(id).html(html);
		var i=0,clearHandle;
		t.userRating=0;
		$(id).append('<span id="fdcrtsfajax"></span>');
		t.rateElement = $("#fdcrtsfajax");
		this.userStars = $(id+' img').slice(0,5).each(function() {
			(function(star,i) {
				$(star).mouseover(function() {
					if(clearHandle) clearTimeout(clearHandle);
					t.setStars(t.userStars,i+1);
				}).mouseout(function() { clearHandle = setTimeout(function() { t.setStars(t.userStars,t.userRating); },t.starTimeout);
				}).click(function() {
					t.userRating = i+1;
					t.setStars(t.userStars,i+1);
					fajax.call("fdcrtsid",t.rateElement.get(0),t.rateCall, {
						rating:i+1,
						op:'rate',
						source_type:t.params.source_type,
						source_id:t.params.source_id,
						z:Math.random()
					});
				});
			})(this,i++);
		});
		this.avgStars = $(id+' img').slice(5);
		// get the current ratings
		fajax.call("fdcrtsid", t.rateElement.get(0), t.rateCall, {
			request:'all',
			op:'init',
			source_type:t.params.source_type,
			source_id:t.params.source_id,
			z:Math.random()
		});
		t.comments = $('#readerComments');
		$(id).append('<span id="fdccommentsfajax"></span>');
		t.commentElement = $("#fdccommentsfajax");		
		t.setCommentsHTML();		
		
		fajax.call("commentfdcrtsid",t.commentElement.get(0), t.publishedComments+t.params.source_type+"/"+t.params.source_id.replace(/\.html$/,".js"),{});
		
		t.bubble=false;
		$("#comLink").click(function() {
			$('#commentLink').trigger("click")
			var cForm = $('#commentLink').offset();
			if(!cForm) return false;
			$("html,body").animate({scrollTop:cForm.top-40},"fast");
			return false;
		});
		$('#controlsbox p.comments,ul.controls>.comments').click(function() {
			var comments = $('#readerComments').offset();
			if(!comments) return;
			$("html,body").animate({scrollTop:comments.top-10},"fast");
			return false;
		});
	},
	addRts2:function(id,params) { // without rating        	
		commentsFlag = true;
		id = '#'+id;
		this.params=params;
		var t=this;
		$(id).append('<span id="fdcrtsfajax"></span>');
                t.rateElement = $("#fdcrtsfajax");
		t.comments = $('#readerComments');						
		$(id).append('<span id="fdccommentsfajax"></span>');
		t.commentElement = $("#fdccommentsfajax");				
		fajax.call("commentfdcrtsid",t.commentElement.get(0), t.publishedComments+t.params.source_type+"/"+t.params.source_id.replace(/\.html$/,".js"),{});
		
		t.bubble=false;
		$("#comLink").click(function() {
			$('#commentLink').trigger("click")
			var cForm = $('#commentLink').offset();
			if(!cForm) return false;
			$("html,body").animate({scrollTop:cForm.top-40},"fast");
			return false;
		});
		$('#controlsbox p.comments,ul.controls>.comments').click(function() {
			var comments = $('#readerComments').offset();
			if(!comments) return;
			$("html,body").animate({scrollTop:comments.top-10},"fast");
			return false;
		});
	},
	addRtsBox:function(id,params) { //comments box
		commentsBoxFlag = true;
		id = '#'+id;
		this.params=params;
		var t=this,html = "<h3>Rate This Story</h3><ul><li><h6 class='rating'>Your Rating </h6>";
		function addStars(cls) { for(var i=0;i<5;i++) html+='<img src="'+t.emptyStar+'" class="'+cls+'"></img>'; }
		addStars("star");
		html+="</li>";
		html+="<li><h6 class='rating'>Overall Rating </h6>";
		addStars("star no");
		html+="</li></ul>";
		$(id).html(html);
		var i=0,clearHandle;
		t.userRating=0;
		$(id).append('<span id="fdcrtsfajax"></span>');
                t.rateElement = $("#fdcrtsfajax");
		this.userStars = $(id+' img').slice(0,5).each(function() {
			(function(star,i) {
				$(star).mouseover(function() {
					if(clearHandle) clearTimeout(clearHandle);
					t.setStars(t.userStars,i+1);
				}).mouseout(function() { clearHandle = setTimeout(function() { t.setStars( t.userStars,t.userRating);},t.starTimeout);
				}).click(function() {
					t.userRating = i+1;
					t.setStars(t.userStars,i+1);
					fajax.call("fdcrtsid",t.rateElement.get(0),t.rateCall, {
						rating:i+1,
						op:'rate',
						source_type:t.params.source_type,
						source_id:t.params.source_id,
						z:Math.random()
					});
				});
			})(this,i++);
		});
		this.avgStars = $(id+' img').slice(5);
		// get the current ratings
		fajax.call("fdcrtsid", t.rateElement.get(0), t.rateCall, {
			request:'all',
			op:'init',
			source_type:t.params.source_type,
			source_id:t.params.source_id,
			z:Math.random()
		});
		t.comments = $('#readerCommentsP2');
		$(id).append('<span id="fdccommentsfajax2"></span>');
		t.commentElement = $("#fdccommentsfajax2");
		t.bubble=false;
		$("#comLink2").click(function() {
			$('#commentLink2').trigger("click")
			var cForm = $('#commentLink').offset();
			if(!cForm) return false;
			$("html,body").animate({scrollTop:cForm.top-40},"fast");
			return false;
		});
		$('#controlsbox p.comments,ul.controls>.comments').click(function() {
			var comments = $('#readerCommentsP2').offset();
			if(!comments) return;
			$("html,body").animate({scrollTop:comments.top-10},"fast");
			return false;
		});
	},
	setCommentsHTML:function(data) {

		var t=this;
		var html = "<h3>Reader Comments</h3>";
		if(data) {
			var comments = data.recentComments;
			for(var i in comments) {
				var link = t.commentCall+"?op=cpage&sourcename="+t.params.source_type+"&StoryURI="+t.params.source_id+"&com="+comments[i].id;
				
				if(comments[i].editor) html += '<div class="editor">';
				else html += '<div class="comment">';
				html += '<p><a href="'+link+'">'+comments[i].text+'</a>'+((comments[i].more)?' <a href="'+link+'" class="more">[Read More]</a>':'')+'</p>';
				if(comments[i].tags.length) {
					html += 'Tags: ';
						for(var k in comments[i].tags) {
							comments[i].tags[k] = '<a href="'+t.commentCall+'?op=TPage&pageNumber=1&tagName='+escape(t.remove8203s(comments[i].tags[k]))+'">'+t.remove8203s(comments[i].tags[k])+'</a>';
						}
						html+=comments[i].tags.join(", ");
				}
				html += '<cite>Posted by '+comments[i].author+' | '+comments[i].time+' <a href="http://www.forbes.com/media/rts/report_abuse.html?CommentID='+comments[i].id+'" class="abuse"><img src="http://images.forbes.com/media/assets/rate/reportabuse.jpg" /></a></cite>';
				html += '</div>';
			}
			if(data.totalComments>3) html += '<dl><dt><a href="'+t.commentCall+'?op=cpage&sourcename='+t.params.source_type+'&StoryURI='+t.params.source_id+'">Read All Comments ('+data.totalComments+')</a></dt></dl>';
			if(data.totalComments>0) $('#controlsbox p.comments>a,ul.controls>.comments>a').html('<img src="http://images.forbes.com/media/story/comments_icon.gif" />comments ('+data.totalComments+')');
		}
		html+='<div id="commentLink" class="commentLink"><h4><a name="comment"><span>Post a Comment</span></a></h4></div>';
		t.comments.html(html);
		$('a.abuse',t.comments).click(function() { return t.popWindow(this,380,350,false); });
		$('#commentLink',t.comments).click(function() { 
			if(isMember()) {
				$(this).unbind('click'); // stop capturing the click
				$(this).replaceWith('<div id="commentForm"><form method="post" name="commentForm" method="POST" action="'+t.commentCall+'?StoryURI='+t.params.source_id+'&op=save&sourcename='+t.params.source_type+'"><img alt="Post a Comment" src="http://images.forbes.com/media/assets/rate/speechbubble_25x25.gif"/><h4>Post a Comment</h4><textarea onkeydown="rtsUtil.trimLength(\'#commentForm>form>textarea[name=comment]\','+t.commentLength+');" onkeyup="rtsUtil.trimLength(\'#commentForm>form>textarea[name=comment]\','+t.commentLength+');" cols="44" rows="6" name="comment"></textarea><h4>Tags</h4><span>Separated by commas</span><textarea cols="44" rows="1" name="tags"></textarea><div id="rtsCaptcha">Loading Captcha...</div><input id="submitbutton" type="submit" value="Submit" name="mysubmit"/></form></div>');
				$('a',this).click(function() { return t.popWindow(this,600,400,true); });
				fajax.call("commentid",t.commentElement.get(0),t.commentCall,{op:'showcaptcha',StoryURI:t.params.source_id,z:Math.random(),sourcename:t.params.source_type});
				$("#commentForm>form").submit(function() {
					var captchaid = $('#captchaid');
					if(captchaid.get(0)!=null) {
						if(!t.goodComment()) return;
						else fajax.call("captchaid",t.commentElement.get(0),t.commentCall,{op:"captchacheck",captcha:captchaid.val(),StoryURI:t.params.source_id,sourcename:t.params.source_type,z:Math.random()});
					} else t.save();
					return false;
				});
			} else {
				document.domain = "forbes.com";
				tb_show("", "http://members.forbes.com/membership/regbubble/story_regbubble_signup?keepThis=true&TB_iframe=true&height=350&width=550", "");
				/*
					function bubble() {
						fdcregbubble.bubble('show',$('.commentLink span').get(0),'http://www.forbes.com/'+t.params.source_id,false);
						var wind = $(window).scrollTop();
						var popbox = $('#popupbox').offset().top
						if(wind+$(window).height()<popbox+190 || wind>popbox) $("html,body").animate({scrollTop:popbox-40},"fast");
					}
					if(t.bubble) bubble();
					else {
						fajax.call("fdcrtsid",t.rateElement.get(0),t.regBubble,{},function() {
							t.bubble=true;
							bubble();
						});
					}
				*/
			}
		});

		// To open comment box by default
		if(isMember()) {
			$(this).unbind('click'); // stop capturing the click
			$('#commentLink').replaceWith('<div id="commentForm"><form method="post" name="commentForm" method="POST" action="'+t.commentCall+'?StoryURI='+t.params.source_id+'&op=save&sourcename='+t.params.source_type+'"><img alt="Post a Comment" src="http://images.forbes.com/media/assets/rate/speechbubble_25x25.gif"/><h4>Post a Comment</h4><textarea onkeydown="rtsUtil.trimLength(\'#commentForm>form>textarea[name=comment]\','+t.commentLength+');" onkeyup="rtsUtil.trimLength(\'#commentForm>form>textarea[name=comment]\','+t.commentLength+');" cols="44" rows="6" name="comment"></textarea><h4>Tags</h4><span>Separated by commas</span><textarea cols="44" rows="1" name="tags"></textarea><div id="rtsCaptcha">Loading Captcha...</div><input id="submitbutton" type="submit" value="Submit" name="mysubmit"/></form></div>');
			$('a',this).click(function() { return t.popWindow(this,600,400,true); });
			fajax.call("commentid",t.commentElement.get(0),t.commentCall,{op:'showcaptcha',StoryURI:t.params.source_id,z:Math.random(),sourcename:t.params.source_type});
			$("#commentForm>form").submit(function() {
				var captchaid = $('#captchaid');
				if(captchaid.get(0)!=null) {
					if(!t.goodComment()) return;
					else fajax.call("captchaid",t.commentElement.get(0),t.commentCall,{op:"captchacheck",captcha:captchaid.val(),StoryURI:t.params.source_id,sourcename:t.params.source_type,z:Math.random()});
				} else t.save();
				return false;
			});
		}

	},
	setCommentsHTML2:function(data) {
		var t=this;
		var html = "<h3>Reader Comments</h3>";
		if(data) {
			var comments = data.recentComments;
			for(var i in comments) {
				var link = t.commentCall+"?op=cpage&sourcename="+t.params.source_type+"&StoryURI="+t.params.source_id+"&com="+comments[i].id;
				
				if(comments[i].editor) html += '<div class="editor">';
				else html += '<div class="comment">';
				html += '<p><a href="'+link+'">'+comments[i].text+'</a>'+((comments[i].more)?' <a href="'+link+'" class="more">[Read More]</a>':'')+'</p>';
				if(comments[i].tags.length) {
					html += 'Tags: ';
						for(var k in comments[i].tags) {
							comments[i].tags[k] = '<a href="'+t.commentCall+'?op=TPage&pageNumber=1&tagName='+escape(t.remove8203s(comments[i].tags[k]))+'">'+t.remove8203s(comments[i].tags[k])+'</a>';
						}
						html+=comments[i].tags.join(", ");
				}
				html += '<cite>Posted by '+comments[i].author+' | '+comments[i].time+' <a href="http://www.forbes.com/media/rts/report_abuse.html?CommentID='+comments[i].id+'" class="abuse"><img src="http://images.forbes.com/media/assets/rate/reportabuse.jpg" /></a></cite>';
				html += '</div>';
			}
			if(data.totalComments>3) html += '<dl><dt><a href="'+t.commentCall+'?op=cpage&sourcename='+t.params.source_type+'&StoryURI='+t.params.source_id+'">Read All Comments ('+data.totalComments+')</a></dt></dl>';
			if(data.totalComments>0) $('#controlsbox p.comments>a,ul.controls>.comments>a').html('<img src="http://images.forbes.com/media/story/comments_icon.gif" />comments ('+data.totalComments+')');
		}
		html+='<div id="commentflag"><div id="commentLink" class="commentLink"><h4><a name="comment"><span>Post a Comment</span></a></h4></div></div>';
		t.comments.html(html);
		$('a.abuse',t.comments).click(function() { return t.popWindow(this,380,350,false); });
		$('#commentLink',t.comments).click(function() { 
			t.getCommentForm(t);			
			if ($('#cF2').get(0) != null){
				$('a.abuse','#readerCommentsP2').click(function() { return t.popWindow(this,380,350,false); });		
				t.setOnClick(false,t);
			}		
		});		
		// To open comment box by default.
		if(isMember()) {
			//$('#commentLink').trigger('click');
			$('#commentflag').replaceWith('<div id="cF"><div id="commentForm"><form method="post" name="commentForm" method="POST" action="'+t.commentCall+'?StoryURI='+t.params.source_id+'&op=save&sourcename='+t.params.source_type+'"><img alt="Post a Comment" src="http://images.forbes.com/media/assets/rate/speechbubble_25x25.gif"/><h4>Post a Comment</h4><textarea onkeydown="rtsUtil.trimLength(\'#commentForm>form>textarea[name=comment]\','+t.commentLength+');" onkeyup="rtsUtil.trimLength(\'#commentForm>form>textarea[name=comment]\','+t.commentLength+');" cols="44" rows="6" name="comment"></textarea><h4>Tags</h4><span>Separated by commas</span><textarea cols="44" rows="1" name="tags"></textarea><div id="rtsCaptcha">Loading Captcha...</div><input id="submitbutton" type="submit" value="Submit" name="mysubmit"/></form></div></div>');
				$('a',this).click(function() { return t.popWindow(this,600,400,true); });				
				fajax.call("commentid",t.commentElement.get(0),t.commentCall,{op:'showcaptcha',StoryURI:t.params.source_id,z:Math.random(),sourcename:t.params.source_type});
				$("#commentForm>form").submit(function() {
					var captchaid = $('#captchaid');
					if(captchaid.get(0)!=null) {
						if(!t.goodComment()) return;							
						else fajax.call("captchaid",t.commentElement.get(0),t.commentCall,{op:"captchacheck",captcha:captchaid.val(),StoryURI:t.params.source_id,sourcename:t.params.source_type,z:Math.random()});						
					} else t.save();
					return false;
				});
		}

	},
	setCommentsBoxHTML:function(data) { // comments box
		var t=this;
		var html = "<h3>Reader Comments</h3>";
		if(data) {
			var comments = data.recentComments;
			for(var i in comments) {
				if (i < 1){
					var link = t.commentCall+"?op=cpage&type=new&sourcename="+t.params.source_type+"&StoryURI="+t.params.source_id+"&com="+comments[i].id;
				
					if(comments[i].editor) html += '<div class="editor">';
					else html += '<div class="comment">';
					var text = comments[i].text;
					if (text.length >= 199){ html += '<p><a href="'+link+'">'+comments[i].text + '....</a><br/>';}
					else { html += '<p><a href="'+link+'">'+comments[i].text+'</a><br>';}
					html += '<cite><a href="http://www.forbes.com/media/rts/report_abuse.html?CommentID='+comments[i].id+'" class="abuse"><img src="http://images.forbes.com/media/assets/rate/reportabuse.jpg" /></a></cite>';
				}
			}
			if(data.totalComments>1) html += '<dl><dt><a href="'+t.commentCall+'?op=cpage&type=new&sourcename='+t.params.source_type+'&StoryURI='+t.params.source_id+'">Read All Comments ('+data.totalComments+')</a></dt></dl>';
			if(data.totalComments>0) $('#controlsbox p.comments>a,ul.controls>.comments>a').html('<img src="http://images.forbes.com/media/story/comments_icon.gif" />comments ('+data.totalComments+')');
		}
		html+='<div id="commentflag2"><div id="commentLink2" class="commentLink2"><cite><span style="display: inline;"></span><a name="comment">Post a Comment</a></cite></div></div>';
		$('#readerCommentsP2').html(html);
		$('a.abuse','#readerCommentsP2').click(function() { return t.popWindow(this,380,350,false); });
        	$('#commentLink2','#readerCommentsP2').click(function() {
			t.getCommentFormBox(t);
			if ($('#cF').get(0) != null){
				$('a.abuse','#readerComments').click(function() { return t.popWindow(this,380,350,false); });
				t.setOnClick(true,t);
			}		
		});
	},
	getCommentForm:function(t) {
			if(isMember()) {
				$('#commentflag').replaceWith('<div id="cF"><div id="commentForm"><form method="post" name="commentForm" method="POST" action="'+t.commentCall+'?StoryURI='+t.params.source_id+'&op=save&sourcename='+t.params.source_type+'"><img alt="Post a Comment" src="http://images.forbes.com/media/assets/rate/speechbubble_25x25.gif"/><h4>Post a Comment</h4><textarea onkeydown="rtsUtil.trimLength(\'#commentForm>form>textarea[name=comment]\','+t.commentLength+');" onkeyup="rtsUtil.trimLength(\'#commentForm>form>textarea[name=comment]\','+t.commentLength+');" cols="44" rows="6" name="comment"></textarea><h4>Tags</h4><span>Separated by commas</span><textarea cols="44" rows="1" name="tags"></textarea><div id="rtsCaptcha">Loading Captcha...</div><input id="submitbutton" type="submit" value="Submit" name="mysubmit"/></form></div></div>');
				$('a',this).click(function() { return t.popWindow(this,600,400,true); });
				fajax.call("commentid",t.commentElement.get(0),t.commentCall,{op:'showcaptcha',StoryURI:t.params.source_id,z:Math.random(),sourcename:t.params.source_type});
				$("#commentForm>form").submit(function() {
					var captchaid = $('#captchaid');
					if(captchaid.get(0)!=null) {
						if(!t.goodComment()) return;
						else fajax.call("captchaid",t.commentElement.get(0),t.commentCall,{op:"captchacheck",captcha:captchaid.val(),StoryURI:t.params.source_id,sourcename:t.params.source_type,z:Math.random()});
					} else t.save();
					return false;
				});
			} else {
				document.domain = "forbes.com";
				tb_show("", "http://members.forbes.com/membership/regbubble/story_regbubble_signup?keepThis=true&TB_iframe=true&height=350&width=550", "");
				/*
					function bubble() {
						fdcregbubble.bubble('show',$('.commentLink span').get(0),'http://www.forbes.com/'+t.params.source_id,false);
						var wind = $(window).scrollTop();
						var popbox = $('#popupbox').offset().top
						if(wind+$(window).height()<popbox+190 || wind>popbox) $("html,body").animate({scrollTop:popbox-40},"fast");
					}
					if(t.bubble) bubble();
					else {
						fajax.call("fdcrtsid",t.rateElement.get(0),t.regBubble,{},function() {
							t.bubble=true;
							bubble();
						});
					}
				*/
			}
	},
	getCommentFormBox:function(t) {
			if(isMember()) {
				$('#commentflag2').replaceWith('<div id="cF2"><div id="commentForm"><form method="post" name="commentForm" method="POST" action="'+t.commentCall+'?StoryURI='+t.params.source_id+'&op=save&sourcename='+t.params.source_type+'"><img alt="Post a Comment" src="http://images.forbes.com/media/assets/rate/speechbubble_25x25.gif"/><h4>Post a Comment</h4><textarea onkeydown="rtsUtil.trimLength(\'#commentForm>form>textarea[name=comment]\','+t.commentLength+');" onkeyup="rtsUtil.trimLength(\'#commentForm>form>textarea[name=comment]\','+t.commentLength+');" cols="20" rows="6" name="comment"></textarea><h4>Tags</h4><span>Separated by commas</span><textarea cols="20" rows="1" name="tags"></textarea><div id="rtsCaptcha">Loading Captcha...</div><input id="submitbutton" type="submit" value="Submit" name="mysubmit"/></form></div></div>');
				$('a',this).click(function() { return t.popWindow(this,600,400,true); });
				fajax.call("commentid",t.commentElement.get(0),t.commentCall,{op:'showcaptcha',StoryURI:t.params.source_id,z:Math.random(),sourcename:t.params.source_type,type:'new'});
				$("#commentForm>form").submit(function() {
					var captchaid = $('#captchaid');
					if(captchaid.get(0)!=null) {
						if(!t.goodComment()) return;
						else fajax.call("captchaid",t.commentElement.get(0),t.commentCall,{op:"captchacheck",captcha:captchaid.val(),StoryURI:t.params.source_id,sourcename:t.params.source_type,z:Math.random(),type:'new'});
					} else t.save();
					return false;
				});
			} else {
				document.domain = "forbes.com";
				tb_show("", "http://members.forbes.com/membership/regbubble/story_regbubble_signup?keepThis=true&TB_iframe=true&height=350&width=550", "");
				/*
				function bubble() {
										fdcregbubble.bubble('show',$('#commentLink2>cite>span').get(0),'http://www.forbes.com/'+t.params.source_id,false);
										var wind = $(window).scrollTop();
										var popbox = $('#popupbox').offset().top
										if(wind+$(window).height()<popbox+190 || wind>popbox) $("html,body").animate({scrollTop:popbox-40},"fast");
								}
								if(t.bubble) bubble();
								else {
										fajax.call("fdcrtsid",t.rateElement.get(0),t.regBubble,{},function() {
												t.bubble=true;
												bubble();
										});
				}
				*/
			}
	},
        setOnClick:function(first,t) {
                        if(first){
                                $('#cF').replaceWith('<div id="commentflag"><div id="commentLink" class="commentLink"><h4><a name="comment"><span>Post a Comment</span></a></h4></div></div>');
                                t.getCommentFormBox(t);
                                $('#commentLink','#readerComments').click(function() {
                                                t.setOnClick(false,t);
                                });
                        }else{
                                $('#cF2').replaceWith('<div id="commentflag2"><div id="commentLink2" class="commentLink2"><cite><span style="display: inline;"></span><a name="comment">Post a Comment</a></cite></div></div>');
                                t.getCommentForm(t);
                                $('#commentLink2','#readerCommentsP2').click(function() {
                                                t.setOnClick(true,t);
                                });
                        }
        },
	goodComment:function() {
		if($('#commentForm>form>textarea[name=comment]').val().length<1) {
			alert("Please enter comment.");
			return false;
		}
		return true;
	},
	save:function() {
		if(!this.goodComment()) return;
		else $('#commentForm>form').unbind('submit').submit();
	},
	setStars:function(set,value) {

		for(var i=0;i<set.length;i++) {
			if(value>=i+1) set.eq(i).attr({src:this.fullStar});
			else if(value>=i+0.5) set.eq(i).attr({src:this.halfStar});
			else set.eq(i).attr({src:this.emptyStar});
		}
	},
	popWindow:function(a,width,height,scroll) {
		window.open($(a).attr('href'),"","toolbar=0,location=0,directories=0,status=0,menubar=0,resizable=no,scrollbars="+(scroll?'1':'0')+",width="+width+",height="+height);
		return false;
	},
	trimLength:function(el, len) {
		var e = $(el);
		if(e.val().length > len) {
			e.val(e.val().substr(0,len));
		}
	},	
	remove8203s:function(t) { return ($.browser.ie)?t.replace(/&#8203;/g,'<wbr>'):t; }
};
/* this handles the queueing and calling of fake ajax calls */
var fajax = {
	call:function(key, element, path, data, callback) {
		if(!this.queue[key]) this.queue[key] = [];
		this.queue[key].unshift({path:path,data:data,element:element,callback:callback});
		if(this.queue[key].length == 1)
			this._sendRequest(key, element, path, data);
	},
	fullfilled:function(key) {
		if(!this.queue[key]) return;
		var f = this.queue[key].pop().callback;
		if(typeof f == 'function') f();
		if(this.queue[key].length>=1) {
			var params = this.queue[key][this.queue[key].length-1];
			this._sendRequest(key, params.element, params.path, params.data);
		}
	},
	_sendRequest:function(key, e, path, data) {
		data.key = key;
		var params = '';
		for(var name in data) { params += (params==''?'?':'&')+name+"="+escape(data[name]); }
		var scriptElement = document.createElement('script');
		scriptElement.setAttribute('type','text/javascript');
		scriptElement.setAttribute('src',path+params);
		if(e.hasChildNodes()) e.replaceChild(scriptElement,e.firstChild);
		else e.appendChild(scriptElement);
	},
	queue:[]
};
function hideAd() {
        var ad = $('#'+thisAdDiv);
        var h = ad.height();
        if($('#winColFiller').length==0) ad.after('<div id="winColFiller" style="height:'+h+'px" class="adBgMId"></div>');
        ad.hide();
}
function showAd() {
        if(searchTab!=0) return;
        $('#'+thisAdDiv).show();
        $('#winColFiller').remove();
}

	OAS_AD("x92");
	OAS_AD("x91");
	document.write('<script src="http://images.forbes.com/scripts/acs/thickbox.js" type="text/javascript"></script>');
	document.write('<link rel="stylesheet" href="http://images.forbes.com/css/story/thickbox.css" type="text/css" media="screen"/>');
	document.write('<link rel="stylesheet" href="http://images.forbes.com/css/signup_module.css" type="text/css" media="screen" />');
