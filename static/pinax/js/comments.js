function toggle_comment_form(id){
    var cform = $('#comment_form_' + id);
    if(cform.hasClass('hidden')) {
        cform.prev().children("a").html("Cancel");
        cform.slideDown();
    }
    else {
        cform.prev().children("a").html("Reply");
        cform.slideUp();
    }
    cform.toggleClass('hidden');
}
