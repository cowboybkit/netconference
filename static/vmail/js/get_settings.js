getsettings = function(){
	var setting_url ="/vmail/getsettings";
	if(IS_DEVELOPMENT)
		setting_url = HTTP_DOMAIN_DEV + "/vmail/getsettings";
	if(IS_MOBILE&&!IS_DEVELOPMENT)
		setting_url = HTTP_DOMAIN + "/vmail/getsettings";
	
	var json = null;
	
	$.ajax({
		url: setting_url,
		type: "GET",
		async:false,
		dataType:"html",
		success: function(result){
			json = result;
		}
	});
	
	return json;
}

browser_flash_installed = function(){
	return FlashDetect.installed;
}