/*
 * jQuery File Upload Plugin JS Example 5.1.5
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://creativecommons.org/licenses/MIT/
 */

/*jslint nomen: true, unparam: true, regexp: true */
/*global $, window, document */

$(function () {
    'use strict';

    // Initialize the jQuery File Upload widget:

    $('#fileupload').fileupload({
		dataType: 'html',
		type:'POST',
		sequentialUploads: false,
		/*add: function (e, data) {
			var filepath = data.files[0].name ;
			var filename = basename(filepath);
	        $('#Attachedlist').append('<tr><td style="display:none;"></td><td width="50px"><img width="20px" src="MEDIACONFIGSITE/images/icon/'+getFileExtension(filename)+'.png"></td><td width="300px">'+((filename.length<30)?filename:(filename.substring(0,30)+'...'))+'</td><td><button class="wizard tiny_button">Remove</button> </td></tr>');
//	            .error(function (jqXHR, textStatus, errorThrown) {alert(errorThrown);})
//	            .complete(function (result, textStatus, jqXHR) {});
		}*/
		 
	})
	.bind('fileuploadsubmit', function (e, data){
		   var form = $('#formup');
		   var uuid = data.context.find('input[name$="uuid"]').val();
		   data.formData = {
		   uid: form.find('input[name$="uid"]').val(),
		   uuid: uuid,
		   cid: form.find('input[name$="cid"]').val(),
		   username: form.find('input[name$="username"]').val(),
		   visibility: form.find('input[name$="visibility"]').val(),
		   };
		    if (!data.formData.cid||!data.formData.uid||!data.formData.username||!data.formData.visibility) {
		        return false;
		      }
	}).bind('fileuploaddone', function(e, data){
		alert(data.result);
	});
	

    // Enable iframe cross-domain access via redirect page:
    var redirectPage = window.location.href.replace(
        /\/[^\/]*$/,
        '/result.html?%s'
    );
    $('#fileupload').bind('fileuploadsend', function (e, data) {
        if (data.dataType.substr(0, 6) === 'iframe') {
            var target = $('<a/>').prop('href', data.url)[0];
            if (window.location.host !== target.host) {
                data.formData.push({
                    name: 'redirect',
                    value: redirectPage
                });
            }
        }
    });
    // Open download dialogs via iframes,
    // to prevent aborting current uploads:
    $('#fileupload .files a:not([target^=_blank])').live('click', function (e) {
        e.preventDefault();
        $('<iframe style="display:none;"></iframe>')
            .prop('src', this.href)
            .appendTo('body');
    });


});