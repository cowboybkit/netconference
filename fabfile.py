#!/usr/bin/env python

from fabric.operations import run, sudo
from fabric.context_managers import cd, settings
from fabric.api import env
from datetime import datetime
import os

BETA_SERVER_PATH="/home/webdev/netconf"
BETA_DEPLOY_PATH="/home/webdev/netconference"


LIVE_SERVER_PATH="/home/esofthead/netconf"
LIVE_DEPLOY_PATH="/home/esofthead/netconference"


USER = "webdev"
env.user=USER
env.key_filename = [os.path.join(os.path.expanduser("~%s" %env.local_user), ".ssh/id_rsa.pub")]


def deploy():
    print "Warning you are UPDING the LIVE site. Please type yes to continue."
    typed = raw_input("Type yes")
    if not typed.lower() == "yes":
        return
    with settings(host_string="www.netconference.com", user=USER):
        with cd(LIVE_SERVER_PATH):
            sudo("chown -R webdev.webdev %s" % (LIVE_SERVER_PATH))
            sudo("chown -R webdev.webdev %s" % (LIVE_DEPLOY_PATH))
            run("git pull")
            today = datetime.today().strftime("%d-%m-%Y")
            today_backup = "%s-backup.%s" % (LIVE_SERVER_PATH, today)
            print today_backup
            with settings(warn_only=True):
                sudo("rm -rf %s" %today_backup)
            #Take a backup.
            sudo("cp -r %s %s"%(LIVE_DEPLOY_PATH, today_backup))


            #Ok copy the latest things.
            run("cp -R %s/* %s/" % (LIVE_SERVER_PATH, LIVE_DEPLOY_PATH) )
            #Copy back the files from backup taken today.
            run("cp -r %s/static/swf/* %s/static/swf/" % (today_backup, LIVE_DEPLOY_PATH))
            run("cp -r %s/static/jar/* %s/static/jar/" % (today_backup, LIVE_DEPLOY_PATH))
            user_uploaded_dirs = ["avatars", "netsign", "store_thumbnails", "user_theme"]
            for dir in user_uploaded_dirs:
                run("cp -r %s/static/%s/* %s/static/%s/" % (today_backup, dir, LIVE_DEPLOY_PATH, dir))


            sudo("chown -R www-data.www-data %s" % LIVE_DEPLOY_PATH)
            sudo("chmod -R 775 %s" % LIVE_DEPLOY_PATH)
            sudo("/etc/init.d/apache2 restart")

def beta_deploy():
    with settings(host_string="beta.netconference.com", user=USER):
        with cd(BETA_SERVER_PATH):
            sudo("chown -R webdev.webdev %s" % (BETA_SERVER_PATH))
            sudo("chown -R webdev.webdev %s" % (BETA_DEPLOY_PATH))
            run("git pull")
            today = datetime.today().strftime("%d-%m-%Y")
            today_backup = "%s-backup.%s" % (BETA_SERVER_PATH, today)
            print today_backup
            with settings(warn_only=True):
                sudo("rm -rf %s" %today_backup)
            # Take a backup of the locale
            run("rm -rf /home/webdev/locale_backup")
            run("cp -r %s/locale/ /home/webdev/locale_backup" %BETA_DEPLOY_PATH)
            #Take a backup.
            run("cp -r %s %s"%(BETA_DEPLOY_PATH, today_backup))


            #Ok copy the latest things.
            run("cp -R %s/* %s/" % (BETA_SERVER_PATH, BETA_DEPLOY_PATH) )
            #Copy back the files from backup taken today.
            run("cp -r %s/static/swf/* %s/static/swf/" % (today_backup, BETA_DEPLOY_PATH))
            run("cp -r %s/static/jar/* %s/static/jar/" % (today_backup, BETA_DEPLOY_PATH))
            # For locale..temporary
            run("rm -rf %s/locale" %BETA_DEPLOY_PATH)
            run("cp -r /home/webdev/locale_backup %s/locale" % BETA_DEPLOY_PATH)

            sudo("chown -R www-data.www-data %s" % BETA_DEPLOY_PATH)
            sudo("chmod -R 775 %s" % BETA_DEPLOY_PATH)
            sudo("/etc/init.d/apache2 restart")

if __name__ == "__main__":
    input = raw_input("Do you want to deploy to beta(1) or www(2)")
    if input == "1":
        beta_deploy()
    elif input == "2":
        deploy()
    else:
        "Try again"
