from django import http
from django.template import RequestContext
from django.template.loader import render_to_string

def server_error(request, template_name='500.html'):
    """
    500 error handler.

    Templates: `500.html`
    Context:
        MEDIA_URL
            Path of static media (e.g. "media.example.org")
    """
    return http.HttpResponseServerError(render_to_string(template_name, {},
                                                         context_instance=RequestContext(request)))
