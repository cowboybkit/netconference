{% load i18n %}{% blocktrans %}You have been sent a nuriche.tv netconference invitation by {{ user }}.

{{ user }} said:

{{ message }}

To view the details of this netconference, go to

{{ event_url }}

Netconferencing is provided for nuriche.tv by NetConference.tv - a great way to collaborate with others online, send video e-mail, and watch videos with others in real time.  If you have any questions about NetConference.tv, don't hesitate to contact us at http://netconference.tv/contact.
{% endblocktrans %}