{% extends "email_templates/base.html" %}
{% load i18n %}

{% block content %}
<div id="left_panel" align="left" style="width:100%; float:left; display:block;">
  <div class="txt" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#595959;line-height:15px;">
    <p>{% trans "Hello from NetConference" %}{% if name %} {% trans "and" %} {{ name }}{% endif %},</p>

    <div>{% if notes %}{{ notes }}{% endif %}</div>

    <p>{% if name %}{{ name }} has given you{% else %}You have been given{% endif %} the gift of a FREE premium subscription at {{ domain }} ($1200 value).  This subscription is valid for a full year, but you must use your signup link within 30 days to take advantage of this offer.</p>
    <br />

    <p>Please click the link below to sign up for your FREE NetConference account:</p>
    <p>http://{{ domain }}/account/signup/promo/?code={{ signup_code.code }}&utm_source=promo_code&utm_medium=email&utm_campaign=promo</p>
    <br />
    
    <p>NetConference is an exciting new Social Business Collaboration tool where you can:</p>
    <ul>
      <li>Receive a Telephone Conference line with up to 100 participants</li>
      <li>Hold an online meeting with video and VOIP conferencing</li>
      <li>Share your desktop with conference attendees</li>
      <li>Share HD video and other rich media in live sync with your friends</li>
      <li>Upload, store and share large files</li>
      <li>Collaborate in your business like never before!</li>
    </ul>

    <p>To see a full list of our features please click here: http://www.netconference.com/features/</p>
    <br />

    <p>Thanks</p>
    <p>TeamNC</p>
    <p>http://www.netconference.com/teamnc</p>
  </div>
</div>
{% endblock %}

