	function S4() {
	   return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
	}
	
	function get_guid() {
	   return (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4());
	}	 
	
    function getSSApplet(){
	 	return document.getElementById("VHScreenShare");
	 }
	 
{% load i18n %}
	function startRecordingDesktop(guid){
	 	var applet = getSSApplet();
		var urlhash = "{{ urlhash }}";
		try {
			applet.setRecordVideo(guid);
		}
		catch(exception){}
		
	 }
	
	function stopRecordingDeskTop(){
		var applet = getSSApplet();
		try {
			applet.stopRecordVideo();
		}
		catch(exception){}
	}


		$(function(){
			$("#startRecordButton").live("click", function(){
				recording_name = prompt("{% trans "What would you like to name this recording?" %}");
				if (recording_name.trim()) {
				    $("#startRecordButton").hide();
				    $("#stopRecordButton").show();
				    var player = document.getElementById('controller').getElementsByTagName('object')[0];
				    player.startRecord();
				    if(is_desktop_streaming){
					//startRecordingDesktop(recording_name);	
				    }
				    $("#recording_notice").show();
				} else {
				    alert("{% trans "Please provide a valid string." %}");
				}
			});
			$("#stopRecordButton").live("click", function(){
				$("#startRecordButton").show();
				$("#stopRecordButton").hide();
				var player = document.getElementById('controller').getElementsByTagName('object')[0];
				player.stopRecord();
				//stopRecordingDeskTop();
				$("#recording_notice").hide();
			});
			$("#stopRecordButton").hide();
			$("#recording_notice").hide();
		});
		
		should_record = true;
		if (should_record){
					$("#controller").each(function() {
                    flashembed(this, {
                        src: "{{ MEDIA_URL }}swf/controller.swf",
                        version: [10, 0],
                        wmode: 'opaque',
                        onFail: function() {
                            $('#wizard_panel').dialog({
                                width: 425,
                                height: 340,
                                title: '{% trans "Webcam and Microphone Setup Wizard" %}',
                                resizable: false
                            });
                            $("#wizard").each(function() {
                                flashembed(this, {
                                    src: "{{ MEDIA_URL }}swf/miccamwizard.swf",
                                    version: [10, 0],
                                    wmode: 'opaque',
                                    expressInstall: "{{ MEDIA_URL }}swf/expressinstall.swf"
                                }, {
                                    gid: '{{ gid }}'
        
                                });
                            });
                        }
                    }, 
					
					{
                        o: '{% ifequal host.username user.username %}j122l{% else %}{{ screen_name }}{% endifequal %}',
                        uid: '{{ screen_name }}',
                        cid: '{{ urlhash }}',
                        gid: '{{ gid }}',
			used_minutes: '{{ used_minutes }}',
			max_minutes: '{{ max_minutes }}'{% ifequal host.username user.username %},
                        max_users_in_conf_room: '{{ host_sub.guests_limit }}'{% endifequal %}
                    });
                });			
			
		}
