#!/bin/sh

WORKON_HOME=/home
PROJECT_ROOT=/home/netconference

# activate virtual environment
. $WORKON_HOME/pinax-env/bin/activate

cd $PROJECT_ROOT
python manage.py update_subscriptions >> $PROJECT_ROOT/logs/subscriptions.log 2>&1