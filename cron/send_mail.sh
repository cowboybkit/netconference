#!/bin/sh

WORKON_HOME=/home/esofthead
PROJECT_ROOT=/home/esofthead/netconference

# activate virtual environment
. $WORKON_HOME/pinax_env/bin/activate

cd $PROJECT_ROOT
#python manage.py send_mail >> $PROJECT_ROOT/logs/cron_mail.log 2>&1
python $PROJECT_ROOT/apps/vmail_new/cron.py  >> $PROJECT_ROOT/logs/cron_mail.log 2>&1
python $PROJECT_ROOT/apps/conferencing/cron.py  >> $PROJECT_ROOT/logs/cron_mail.log 2>&1
python $PROJECT_ROOT/manage.py send_mail >> $PROJECT_ROOT/logs/cron_mail1.log 2>&1