#!/bin/sh

WORKON_HOME=/home/esofthead
PROJECT_ROOT=/home/esofthead/netconference

. $WORKON_HOME/pinax_env/bin/activate

cd $PROJECT_ROOT

python $PROJECT_ROOT/manage.py autorespond >> $PROJECT_ROOT/logs/autorespond.log
