#!/bin/sh

WORKON_HOME=/home
PROJECT_ROOT=/home/netconference

# activate virtual environment
. $WORKON_HOME/pinax-env/bin/activate

cd $PROJECT_ROOT
python manage.py retry_deferred >> $PROJECT_ROOT/logs/cron_mail_deferred.log 2>&1
