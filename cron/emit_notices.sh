#!/bin/sh

WORKON_HOME=/home
PROJECT_ROOT=/home/netconference

# activate virtual environment
. $WORKON_HOME/pinax-env/bin/activate

cd $PROJECT_ROOT
python manage.py emit_notices >> $PROJECT_ROOT/logs/emit_notices.log 2>&1
