#!/bin/sh

WORKON_HOME=/home/esofthead
PROJECT_ROOT=/home/esofthead/netconference

# activate virtual environment
. $WORKON_HOME/pinax_env/bin/activate

cd $PROJECT_ROOT

python $PROJECT_ROOT/manage.py todays_users >> $PROJECT_ROOT/logs/todays_users.log 2>&1 
