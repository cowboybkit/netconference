from django.conf.urls.defaults import *
from django.conf import settings
from django.views.generic.simple import direct_to_template

from django.contrib import admin
admin.autodiscover()

handler500 = 'netconference.views.server_error'

from account.forms import SignupForm

from socialauth import urls as socialauth_urls

urlpatterns = patterns('',
    url(r'^traffic_geyser/', include('traffic_geyser.urls')),
    url(r'^amfservices/$', 'amfservices.gateway.gw'),
    url(r'^amfhttp/', include('amfservices.urls')),
    url(r'^vmail/', include('vmail_new.urls')),
    url(r'^wsdl/', include('amfservices.urls')),
    url(r'^60/$', "account.views.sixty_day_coupon", name="sixty_day_coupon"),
    url(r'^$', direct_to_template, {"template": "newer_homepage.html"}, name="home"),
    
	url(r'^landing/$', direct_to_template, {"template": "landing.html",
                                                'extra_context':{'form':SignupForm(),
                                                                 'INCLUDE_ANALYTICS': settings.INCLUDE_ANALYTICS}
                                                }, name="landing"),
	url(r'^landingvideo/$', direct_to_template, {"template": "landing_video.html",
                                                'extra_context':{'form':SignupForm()}
                                                }, name="landingvideo"), 												
       
    url(r'^admin/invite_user/generated_email/$', 'signup_codes.views.show_generated_email', name="generated_email"), 
    url(r'^admin/invite_user/$', 'signup_codes.views.admin_invite_user', name="admin_invite_user"),
    
    url(r'^robots.txt$', include('robots.urls')),
    
    url(r'^admin/(.*)', admin.site.root),
    
    url(r'^invest/', 'contact_form.views.invest', name="invest"),
    url(r'^meeting/(?P<username>[\w\._-]+)/$', 'conferencing.views.conference_session_quick', name='conference_session_quick'),
    url(r'^meeting/setx/(?P<urlhash>[-0-9A-Za-z_-]+)/$', 'conferencing.views.conference_session2', name='conference_session2'),
#    url(r'^meeting/setx/(?P<urlhash>[-0-9A-Za-z_-]+)/$', 'conferencing.views.conference_flex', name='conference_flex'),
    url(r'^meeting/set/(?P<urlhash>[-0-9A-Za-z_-]+)/$', 'conferencing.views.conference_flash', name='conference_flash'),
    url(r'^meeting/set2/(?P<urlhash>[-0-9A-Za-z_-]+)/$', 'conferencing.views.conference_session', name='conference_session'),
    url(r'^file/(?P<urlhash>[-0-9A-Za-z_-]+)/$', 'files.views.file_detail', name='file_detail'),
    
 
    
    
    url(r'^crossdomain.xml$', 'flashpolicies.views.simple', { 'domains': ['www.localhacks.com', 'netconference.tv', 'iftest.netconference.com', 'www.netconference.tv', 'netconference.com', 'www.netconference.com', 'dev.netconference.com', 'beta.netconference.com', 'nxtgenlabs.tv', 'www.nxtgenlabs.tv', 'nuriche.tv', 'www.nuriche.tv', 'efusjonmeeting.illusion.webfactional.com', 'efusjonmeeting.com', 'www.efusjonmeeting.com', 'nxtgendemo.tv', 'www.nxtgendemo.tv', '208.109.252.110', "*", ""] }),
    url(r'^my_admin/jsi18n', 'django.views.i18n.javascript_catalog'),

    
    url(r'^about/', include('about.urls')),
    url(r'^account/signup/promo/', 'signup_codes.views.signup', name='signup_promo'),
    url(r'^account/', include('account.urls')),
    url(r'^bbauth/', include('bbauth.urls')),
    url(r'^profiles/', include('profiles.urls')),
    url(r'^tags/', include('tag_app.urls')),
    url(r'^connections/', include('friends_app.urls')),
    url(r'^notices/', include('notification.urls')),
    url(r'^messages/', include('messages.urls')),
    url(r'^announcements/', include('announcements.urls')),
    url(r'^comments/', include('threadedcomments.urls')),
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^avatar/change/$', 'account.views.avatar_change', name='avatar_change'),
    url(r'^avatar/', include('avatar.urls')),
    url(r'^flag/', include('flag.urls')),
    url(r'^schedule/', include('schedule.urls')),
    url(r'^locations/', include('locations.urls')),
    url(r'^conferencing/', include('conferencing.urls')),
    url(r'^messaging/', include('vmail.urls')),
    url(r'^contact/', include('contact_form.urls')),
    url(r'^subscription/', include('subscription.urls')),
    url(r'^payment/auth/verify/fnord/selection/', include('paypal.standard.ipn.urls')),
    url(r'^features/', include('features.urls')),
    url(r'^files/', include('files.urls')),
    url(r'^cart/', include('cart.urls')),
    url(r'^checkout/', include('checkout.urls')),
    url(r'^billing/', include('billing.urls')),
    url(r'^netsign/', include('netsign.urls')),
    url(r'^affiliate/', include('affiliate.urls')),
    url(r'^comments/', include('django.contrib.comments.urls')),
    url(r'^activity/', include('activitystream.urls')),
    url(r'^rest/', include('restrictions.urls')),
    url(r'^projects/', include('project.urls')),
    url(r'^store/', include('store.urls')),
    url(r'^reporting/', include('reporting.urls')),
    url(r'^meter/', 'restrictions.views.show_context', name='usage_meter'),
    url(r'^total_usage/', 'restrictions.views.total_usage', name='total_usage'),
    url(r'^camtest/$', direct_to_template, {"template": "conferencing/camtest.html"}, name="camtest"),
    url(r'^googlehostedservice.html/$', 'googleapps.views.verify', name='googleverify'),
    url(r'^openid/', include('django_openid_auth.urls')),
    url(r'^integrate/', include('integrate.urls')),
    url(r'^chat/', include('chat.urls')),
    url(r'^android/', include('android.urls')),
    url(r'^workgroups/', include('tribes.urls')),
    url(r'^blog/', include('blogango.urls')),
    url(r'^rosetta/', include('rosetta.urls')),
    url(r'^comment/(?P<content_type>\d+)/(?P<object_id>\d+)/$', 'threadedcomments_extras.views.comment', name="tc_comment"),
    url(r'^comment/(?P<content_type>\d+)/(?P<object_id>\d+)/(?P<parent_id>\d+)/$', 'threadedcomments_extras.views.comment', name="tc_comment_parent"),
    url(r'^zendesk/login/$', 'zendesk.views.authorize', name="zendesk_authorize"),
    url(r'^oauth2callback/$', 'friends_app.views.oauth_callback', name="friends_oauth_callback"),
    url(r'^yahoo2callback/$', 'friends_app.views.yahoo_callback', name="yahoo_callback"),
    url(r'^recurly_notification/$', 'subscription.views.recurly_notification', name='recurly_notification'),
    url(r'^socialauth/', include(socialauth_urls)),
    url(r'^landingpages/', include('landingpages.urls')),
    url(r'^testsocialauth/$', direct_to_template, {"template":"socialauth/after_signup2.html"}, name="aftersignup2.html"),
    url(r'^followup/$', 'about.views.followup', name='follow_up'),
    url(r'^tutorial/$', direct_to_template, {"template":"newer_tutorial.html"}, name="tutorial"),
    url(r'^track_social_signup/$', direct_to_template, {"template":"newer_signup_complete.html"}, name="social_signup_complete"),
    url(r'^track_cc_signup/$', direct_to_template, {"template":"newer_signup_complete1.html"}, name="cc_signup_complete"),
    url(r'^autoresponder/', include('autoresponder.urls')),
    url(r'^create_account_intermediate/$', direct_to_template, {"template":"create_account_intermediate.html"}, name="create_account_intermediate"),
    url(r'^googledriver/', include('googledriver.urls')),
    #This should be the last as it is essentially a catchall thingie.
    url(r'^(?P<username>[\w\._-]+)/$', 'schedule.views.calendar', name="user_dashboard"),
)


if settings.SERVE_MEDIA:
    urlpatterns += patterns('',
        (r'^site_media/(?P<path>.*)$', 'staticfiles.views.serve'),

    )
