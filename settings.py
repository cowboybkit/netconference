# -*- coding: utf-8 -*-
# Django settings for social pinax project.
import os.path
import pinax
  
DEBUG = True
PINAX_ROOT = os.path.realpath(os.path.dirname(pinax.__file__))
PROJECT_ROOT = os.path.realpath(os.path.dirname(__file__))
# tells Pinax to use the default theme
PINAX_THEME = 'default'

ADMINS = (
   ("akshar raaj", "akshar@agiliq.com"), 
   ("hung nguyen", "hungnguyen@esofthead.com"),
)
  
INVEST_MAIL_RECIPIENT = 'brian@illusionfactory.com'
  
  
MANAGERS = (
    # ('Netconference Technical Support', 'help@netconference.com'),
   ('Support', 'support@netconference.com'),
)
  
# Local time zone for this installation. Choices can be found here:
# http://www.postgresql.org/docs/8.1/static/datetime-keywords.html#DATETIME-TIMEZONE-SET-TABLE
# although not all variations may be possible on all operating systems.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'America/Los_Angeles'
  
# Language code for this installation. All choices can be found here:
# http://www.w3.org/TR/REC-html40/struct/dirlang.html#langcodes
# http://blogs.law.harvard.edu/tech/stories/storyReader$15
LANGUAGE_CODE = 'en'
  
SITE_ID = 1
  
# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True
  
# URL that handles the media served from MEDIA_ROOT.
# Example: "http://media.lawrence.com"
MEDIA_URL = '/site_media/'
EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"
# URL prefix for admin media -- CSS, JavaScript and images. Make sure to use a
# trailing slash.
# Examples: "http://foo.com/media/", "/media/".
ADMIN_MEDIA_PREFIX = '/site_media/admin/'
  
# Make this unique, and don't share it with anybody.
SECRET_KEY = '3q98$ye*4cuf7eic#9)923vb3^p2l2i#c2yt$c4#6os!r+yl5l'
  
# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.load_template_source',
    'django.template.loaders.app_directories.load_template_source',
)
  
AUTHENTICATION_BACKENDS = (
    'integrate.auth.OpenIDBackend',
    'django.contrib.auth.backends.ModelBackend',
    'socialauth.auth_backends.OpenIdBackend',
    'socialauth.auth_backends.TwitterBackend',
    'socialauth.auth_backends.LinkedInBackend',
    'socialauth.auth_backends.FacebookBackend',
)
  
MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django_openid.consumer.SessionConsumer',
    'account.middleware.LocaleMiddleware',
    'django.middleware.doc.XViewMiddleware',
    'pagination.middleware.PaginationMiddleware',
    'django_sorting.middleware.SortingMiddleware',
    'djangodblog.middleware.DBLogMiddleware',
    'pinax.middleware.security.HideSensistiveFieldsMiddleware',
    'subscription.middleware.RefererTrackerMiddleware',
    'django.middleware.transaction.TransactionMiddleware',
    #'djangologging.middleware.LoggingMiddleware',
    'SSLMiddleware.SSLRedirect',
    'netconference.http.HttpErrorMiddleware',
    'django.contrib.flatpages.middleware.FlatpageFallbackMiddleware',
    'openid_consumer.middleware.OpenIDMiddleware',
)
  
ROOT_URLCONF = 'urls.py'
  
TEMPLATE_DIRS = (
    os.path.join(PROJECT_ROOT, "new_templates"),
    os.path.join(PROJECT_ROOT, "templates"),
    os.path.join(PINAX_ROOT, "templates", PINAX_THEME),
)
TEMPLATE_CONTEXT_PROCESSORS = (
    "django.core.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.request",
  
    "pinax.core.context_processors.pinax_settings",
  
    "notification.context_processors.notification",
    "announcements.context_processors.site_wide_announcements",
    "account.context_processors.openid",
    "account.context_processors.account",
    "messages.context_processors.inbox",
    "friends_app.context_processors.invitations",
    "netconference.context_processors.combined_inbox_count",
    "netconference.context_processors.user_subscription",
    "netconference.context_processors.user_theme",
    "netconference.context_processors.include_analytics",
    "netconference.context_processors.support_url",
)
  
COMBINED_INBOX_COUNT_SOURCES = (
    "messages.context_processors.inbox",
    "friends_app.context_processors.invitations",
    "notification.context_processors.notification",
)
  
INSTALLED_APPS = (
    # included
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.humanize',
    'django.contrib.markup',
    'django.contrib.comments',
    'pinax.templatetags',
    'django.contrib.flatpages',
     
    # external
    'notification', # must be first
    'django_openid',
    'emailconfirmation',
    'django_extensions',
    'robots',
    'netconference.apps.friends',
    'friends_app',
    'mailer',
    'messages',
    'announcements',
    'oembed',
    'djangodblog',
    'pagination',
    'storage',
  
    # 'gravatar',
    'threadedcomments',
    'threadedcomments_extras',
    'wiki',
    # 'swaps',
    'timezones',
    # 'voting',
    # 'voting_extras',
    'tagging',
    # 'bookmarks',
    # 'blog',
    'ajax_validation',
    # 'photologue',
    'avatar',
    'flag',
    'microblogging',
    # 'locations',
    'uni_form',
    'django_sorting',
    'django_markup',
    'projects',
    'project',
    'zendesk',
     
    # internal (for now)
    'analytics',
    'profiles',
    'staticfiles',
    'account',
    'signup_codes',
    'tribes',
    #'photos',
    'tag_app',
    'topics',
    'groups',
    'chat',
     
    # Custom Nxtgen apps
    'conferencing',
    'schedule',
    'bans',
    'about',
    'contact_form',
    'vmail',
    'paypal.standard',
    'paypal.pro',
    'paypal.standard.ipn',
    'cart',
    'checkout',
    'catalog',
    'subscription',
    'billing',
    'ccall_pins',
    'rep_accounts',
    'features',
    'user_theme',
    'files',
    'netsign',
    'activitystream',
     
    'netconf_utils',
    'restrictions',
    'migration',
    'affiliate',
    "store",
    "compressor",
    'backup',
    "reporting",
    "customer_service",
    #admin site
    'django.contrib.admin',
     
    #googleapps integration
    'django_openid_auth',
    'traffic_geyser',
    'south',
    'integrate',
    'amfservices',
    'googledriver',
  
    #blog
    'blogango',
    'taggit',
    'pingback',
  
    # Translations admin
    'rosetta',
  
    # sorting in contacts page
    'sorting' ,
    'vmail_new',
    'autoresponder',
    'socialauth',
    'landingpages',
    'openid_consumer',
)
  
OPENID_SSO_SERVER_URL = 'https://www.google.com/accounts/o8/site-xrds?hd=example.com'
OPENID_SSO_SERVER_URL2 = 'https://www.google.com/accounts/o8/id'
INTEGRATE_SERVER_DURATION = 20.0 # <=> 20 seconds
INTEGRATE_CLIENT_DURATION = 5 # <=> 5 seconds
DEFAULT_GOOGLE_DOCS_ACCOUNT = "default.netconference@gmail.com"
DEFAULT_GOOGLE_DOCS_PASSWORD = "esofthead321"
ABSOLUTE_URL_OVERRIDES = {
    "auth.user": lambda o: "/%s/" % o.username,
}
  
MARKUP_FILTER_FALLBACK = 'none'
MARKUP_CHOICES = (
    ('restructuredtext', u'reStructuredText'),
    ('textile', u'Textile'),
    ('markdown', u'Markdown'),
    ('creole', u'Creole'),
)
WIKI_MARKUP_CHOICES = MARKUP_CHOICES
  
AUTH_PROFILE_MODULE = 'profiles.Profile'
NOTIFICATION_LANGUAGE_MODULE = 'account.Account'
  
ACCOUNT_OPEN_SIGNUP = True
ACCOUNT_REQUIRED_EMAIL = True
ACCOUNT_EMAIL_VERIFICATION = True
  
OAUTH2_CLIENT_ID = ""
OAUTH2_CLIENT_SECRET = ""
  
INTERNAL_IPS = (
    '127.0.0.1',
)
  
SERVER_EMAIL = 'Server@Netconference.com'
  
ugettext = lambda s: s
LANGUAGES = (
  ('en', u'English'),
  ('de', u'Deutsch'),
  ('es', u'Español'),
  ('fr', u'Français'),
  ('sv', u'Svenska'),
  ('pt-br', u'Português brasileiro'),
  ('he', u'עברית'),
  ('ar', u'العربية'),
  ('it', u'Italiano'),
  ('zh', u'简体字'),
)
URCHIN_ID = "UA-229347-11"
  
class NullStream(object):
    def write(*args, **kwargs):
        pass
    writeline = write
    writelines = write
  
RESTRUCTUREDTEXT_FILTER_SETTINGS = {
    'cloak_email_addresses': True,
    'file_insertion_enabled': False,
    'raw_enabled': False,
    'warning_stream': NullStream(),
    'strip_comments': True,
}
  
# if Django is running behind a proxy, we need to do things like use
# HTTP_X_FORWARDED_FOR instead of REMOTE_ADDR. This setting is used
# to inform apps of this fact
BEHIND_PROXY = False
  
FORCE_LOWERCASE_TAGS = True
  
WIKI_REQUIRES_LOGIN = True
  
# Uncomment this line after signing up for a Yahoo Maps API key at the
# following URL: https://developer.yahoo.com/wsregapp/
# YAHOO_MAPS_API_KEY = ''
  
# Absolute path to the directory that holds static files like app media.
# Example: "/home/media/media.lawrence.com/apps/"
STATIC_ROOT = os.path.join(PROJECT_ROOT, 'static')
# URL that handles the static files like app media.
# Example: "http://media.lawrence.com"
STATIC_URL = '/site_media/'
  
# Additional directories which hold static files
STATICFILES_DIRS = (
    ('netconference', os.path.join(PROJECT_ROOT, 'static')),
    ('pinax', os.path.join(PINAX_ROOT, 'static', PINAX_THEME)),
)
  
# custom settings for netconference setup
  
BBAUTH_APP_ID = "WLFvTVTIkY7hPUbgPfa0YnpqdRdDXBS0c7cx3w--"
BBAUTH_SHARED_SECRET = "6d8fad3d22737d6808ef72dbfb6e9b25"

CONFERENCE_PHONENUMBER = '5124004871'
'''
0: Free conference
1: Go conference
'''
CONFERENCE_CALL_TYPE = 0


URL_SERVER_ENCODING = 'http://encoding.netconference.com/'
URL_GENERATE_VIDEOTHUMB_BYTIME =  URL_SERVER_ENCODING+'encoding/generatevideothumb' 
URL_GENERATE_VMAIL_THUMB = URL_SERVER_ENCODING+'encoding/drawimagethumb'

FILE_UPLOADER_URL = "http://encode1.media.netconf.nxtgencdn.com/uploader_amazons3/upload-amazons3.php"
FILE_UPLOADER_URL_V2 = "http://encode1.media.netconf.nxtgencdn.com/uploader_amazons3/upload-amazons3-v2.php"
FILE_UPLOADER_FLASH_URL = "http://encode1.media.netconf.nxtgencdn.com/swfs/media-upload.swf"
THUMBNAIL_URL = "http://static.netconf.nxtgencdn.com/thumbs/"
DOWNLOAD_URL = "http://static.netconf.nxtgencdn.com/files/"
PDF_DOWNLOAD_URL = "http://encode1.media.netconf.nxtgencdn.com/uploader/files/"
NETSIGN_DOWNLOAD_URL = "http://static.netconf.nxtgencdn.com/netsign/"
NETSIGN_ENCODER_URL = "http://encode1.media.netconf.nxtgencdn.com/uploader_amazons3/netsign-amazons3.php"
ITEMS_LIST_PAGINATE_BY = 10
PROFILE_PAGE_ITEMS_LIST_PAGINATE_BY = 25

FILE_UPLOAD_MAX_MEMORY_SIZE = 20000000
UPLOAD_FILE_DIR = os.path.join(PROJECT_ROOT, "temp/")
DEFAULT_FILE_STORAGE = "storages.backends.s3boto.S3BotoStorage"
# Change the below for branding.
#AWS_STORAGE_BUCKET_NAME = "netconference"



  
PAYPAL_RECEIVER_EMAIL = 'billing@nxtgen.tv'
PAYPAL_WPP_USER = "billing_api1.nxtgen.tv"
PAYPAL_WPP_PASSWORD = " C67HV597SJTHDCQR"
PAYPAL_WPP_SIGNATURE = "Afs3jZI1PBwxH5zM6D7NEDvtaW0TAKi7WXrkR7.znrnBMdhTxykh58JE"
  
GOOGLE_DRIVER_APP_CONSUMER_KEY = ''
GOOGLE_DRIVER_APP_SECRET = ''

ZENDESK_URL = ''
ZENDESK_TOKEN = ''
  
SUBSCRIPTION_PAYPAL_SETTINGS = {
    'business' : PAYPAL_RECEIVER_EMAIL,
    }
SUBSCRIPTION_PAYPAL_FORM = 'paypal.standard.forms.PayPalPaymentsForm'
SUBSCRIPTION_GRACE_PERIOD = 2

SUBSCRIPTION_APPLY = True
  
CACHE_TIMEOUT = 60 * 60
  
SESSION_COOKIE_DAYS = 90
SESSION_COOKIE_AGE = 60 * 60 * 24 * SESSION_COOKIE_DAYS
  
DEFAULT_MAX_COMMENT_LENGTH = 6000
  
ENABLE_SSL = True
BASE_FOLDER = PROJECT_ROOT
  
PRIMARY_AFFILIATE_COMMISION = 0.1
SECONDARY_AFFILIATE_COMMISSION = 0.05
  
NETCONFERENCE_USERNAME = 'netconference_corp'

SOUTH_LOGGING_ON = True

LOG_FOLDER = os.path.join(BASE_FOLDER, 'logs')

SOUTH_LOGGING_FILE = os.path.join(os.path.dirname(__file__),"south.log")
  
LOGIN_REDIRECT_URLNAME = "schedule.views.launcher"
  
LOGIN_REDIRECT_URL = '/conferencing/'
  
NC_STORE_SPLIT = 12 #What percentage of total is taken by NC after file sale.
  
NC_PAYPAL_EMAIL = "sales@netconference.com"
  
COMPRESS = True
  
INCLUDE_ANALYTICS = False
  
CID = "netconference"
  
#This is used by the django-openid
LOGIN_URL = '/openid/login/'
OPENID_CREATE_USERS = False
  
SHAREASALE_MERCHANT_ID = "28392"
SHAREASALE_MIN_DAYS = 34
  
AWS_ACCESS_KEY_ID = ''
AWS_SECRET_ACCESS_KEY = ''
DISALLOWED_USERNAMES = [
    'traffic_geyser',
    'landing',
    'landingvideo',
    'admininvite_user',
    'robots',
    'admin',
    'invest',
    'meeting',
    'meetingset',
    'meetingset2',
    'file',
    'crossdomain',
    'my_adminjsi18n',
    'about',
    'accountsignuppromo',
    'account',
    'bbauth',
    'authsublogin',
    'profiles',
    'tags',
    'connections',
    'notices',
    'messages',
    'announcements',
    'comments',
    'i18n',
    'avatar',
    'flag',
    'schedule',
    'locations',
    'conferencing',
    'messaging',
    'contact',
    'subscription',
    'paymentauthverifyfnordselection',
    'features',
    'files',
    'cart',
    'checkout',
    'billing',
    'netsign',
    'affiliate',
    'comments',
    'activity',
    'rest',
    'projects',
    'store',
    'reporting',
    'meter',
    'total_usage',
    'camtest',
    'googlehostedservice',
    'openid',
    'chat',
    'android',
]
# local_settings.py can be used to override environment-specific settings
# like database and email that differ between development and production.
try:
    from local_settings import *
except ImportError:
    pass
  
if locals().get('LOCAL_APPS', ''):
    INSTALLED_APPS += LOCAL_APPS
  
if locals().get('LOCAL_MIDDLEWARE', ''):
    MIDDLEWARE_CLASSES += LOCAL_MIDDLEWARE
  
  
####Please dont put anything after this
####As localsetings Can't override this.'
